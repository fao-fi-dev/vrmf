CREATE DATABASE  IF NOT EXISTS `vrmf_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `vrmf_db`;
-- MySQL dump 10.13  Distrib 5.7.11, for Win64 (x86_64)
--
-- Host: localhost    Database: vrmf_db
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorization_details`
--

DROP TABLE IF EXISTS `authorization_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorization_details` (
  `AUTH_ID` int(10) unsigned NOT NULL COMMENT 'The related authorization ID',
  `AUTHORIZED_ZONE_ID` int(10) unsigned DEFAULT NULL COMMENT 'The authorization zone ID',
  `AUTHORIZED_SPECIE_ID` int(10) unsigned DEFAULT NULL COMMENT 'The authorization specie ID',
  `AUTHORIZED_GEAR_ID` int(10) unsigned DEFAULT NULL COMMENT 'The authorization gear ID',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The authorization source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  KEY `DETAIL_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `DETAIL_TO_UPDATER` (`UPDATER_ID`),
  KEY `DETAIL_TO_ZONE` (`AUTHORIZED_ZONE_ID`),
  KEY `DETAIL_TO_AUTHORIZATION` (`AUTH_ID`) USING BTREE,
  KEY `DETAIL_TO_GEAR` (`AUTHORIZED_GEAR_ID`),
  KEY `DETAIL_TO_SPECIE` (`AUTHORIZED_SPECIE_ID`),
  CONSTRAINT `DETAIL_TO_AUTHORIZATION` FOREIGN KEY (`AUTH_ID`) REFERENCES `authorizations` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `DETAIL_TO_GEAR` FOREIGN KEY (`AUTHORIZED_GEAR_ID`) REFERENCES `s_gear_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `DETAIL_TO_SPECIE` FOREIGN KEY (`AUTHORIZED_SPECIE_ID`) REFERENCES `s_species` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `DETAIL_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `DETAIL_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `DETAIL_TO_ZONE` FOREIGN KEY (`AUTHORIZED_ZONE_ID`) REFERENCES `s_fishing_zones` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `authorizations`
--

DROP TABLE IF EXISTS `authorizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorizations` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The authorization internal ID',
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `AUTHORIZATION_HOLDER_ID` int(10) unsigned DEFAULT NULL,
  `AUTHORIZATION_ID` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'The authorization ID granted to the vessel',
  `TYPE_ID` varchar(10) COLLATE utf8_bin NOT NULL,
  `VALID_FROM` date DEFAULT NULL COMMENT 'The authorization starting validity date',
  `VALID_TO` date DEFAULT NULL COMMENT 'The authorization ending validity date',
  `REFERENCE_DATE` datetime DEFAULT NULL COMMENT 'The data reference date',
  `TERMINATION_REASON_CODE` int(10) unsigned DEFAULT NULL,
  `TERMINATION_REASON` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `TERMINATION_REFERENCE_DATE` datetime DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The authorization source system',
  `ISSUING_COUNTRY_ID` int(10) unsigned DEFAULT NULL COMMENT 'The authorization issuing country (when applicable)',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `AUTHORIZATION_TO_UPDATER` (`UPDATER_ID`),
  KEY `AUTHORIZATION_TO_VESSEL` (`VESSEL_ID`),
  KEY `AUTHORIZATION_TO_TERMINATION_REASON` (`TERMINATION_REASON_CODE`),
  KEY `AUTHORIZATION_ID_INDEX` (`AUTHORIZATION_ID`),
  KEY `AUTHORIZATION_TO_TYPE` (`TYPE_ID`,`SOURCE_SYSTEM`),
  KEY `AUTHORIZATION_TO_ISSUING_COUNTRY` (`ISSUING_COUNTRY_ID`),
  KEY `AUTH_START_INDEX` (`VALID_FROM`),
  KEY `AUTH_END_INDEX` (`VALID_TO`),
  KEY `TERMINATION_CODE_INDEX` (`TERMINATION_REASON_CODE`),
  KEY `AUTHORIZATION_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `AUTH_TYPE_INDEX` (`TYPE_ID`),
  KEY `AUTH_SOURCE_INDEX` (`SOURCE_SYSTEM`),
  CONSTRAINT `AUTHORIZATION_TO_ISSUING_COUNTRY` FOREIGN KEY (`ISSUING_COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `AUTHORIZATION_TO_SOURCE_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `AUTHORIZATION_TO_TERMINATION_REASON` FOREIGN KEY (`TERMINATION_REASON_CODE`) REFERENCES `s_authorization_termination_reasons` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `AUTHORIZATION_TO_TYPE` FOREIGN KEY (`TYPE_ID`, `SOURCE_SYSTEM`) REFERENCES `s_authorization_types` (`ID`, `SOURCE_SYSTEM`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `AUTHORIZATION_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `AUTHORIZATION_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1000023094 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='InnoDB free: 2117632 kB; (`ISSUING_COUNTRY_ID` `ISSUING_PORT';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`vrmf_root`@`%`*/ /*!50003 TRIGGER vrmf_db.AUTHORIZATION_HISTORY_TRIGGER
BEFORE UPDATE
ON AUTHORIZATIONS 
FOR EACH ROW
BEGIN
   IF @VRMF_TRIGGERS_DISABLED = 1 THEN
      SET @SKIPPED = 1;
   ELSE
          INSERT INTO AUTHORIZATIONS_HISTORY(UTIME, ID, VESSEL_ID, VESSEL_UID, AUTHORIZATION_HOLDER_ID, AUTHORIZATION_ID, TYPE_ID, VALID_FROM, VALID_TO, REFERENCE_DATE, TERMINATION_REASON_CODE, TERMINATION_REASON, TERMINATION_REFERENCE_DATE, SOURCE_SYSTEM, ISSUING_COUNTRY_ID, UPDATE_DATE, UPDATER_ID, COMMENT)

          VALUES (
            CURRENT_TIMESTAMP(),
            OLD.ID,
            OLD.VESSEL_ID,
            OLD.VESSEL_UID,
            OLD.AUTHORIZATION_HOLDER_ID,
            OLD.AUTHORIZATION_ID,
            OLD.TYPE_ID,
            OLD.VALID_FROM,
            OLD.VALID_TO,
            OLD.REFERENCE_DATE,
            OLD.TERMINATION_REASON_CODE,
            OLD.TERMINATION_REASON,
            OLD.TERMINATION_REFERENCE_DATE,
            OLD.SOURCE_SYSTEM,
            OLD.ISSUING_COUNTRY_ID,
            OLD.UPDATE_DATE,
            OLD.UPDATER_ID,
            OLD.COMMENT
          );
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `authorizations_bulk_terminations`
--

DROP TABLE IF EXISTS `authorizations_bulk_terminations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorizations_bulk_terminations` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ISSUING_COUNTRY_ID` int(10) unsigned NOT NULL,
  `AUTHORIZATION_ISSUER_ID` varchar(6) COLLATE utf8_bin NOT NULL,
  `AUTHORIZATION_TYPE_ID` varchar(10) COLLATE utf8_bin NOT NULL,
  `TERMINATION_REASON_CODE` int(10) unsigned NOT NULL,
  `TERMINATION_DATE` date NOT NULL,
  `TERMINATED_VESSELS` int(10) unsigned NOT NULL,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `ISSUING_COUNTRY_TO_COUNTRY` (`ISSUING_COUNTRY_ID`),
  KEY `ISSUER_ID_TO_SOURCE` (`AUTHORIZATION_ISSUER_ID`),
  KEY `TYPE_TO_AUTHORIZATION_TYPE` (`AUTHORIZATION_TYPE_ID`,`AUTHORIZATION_ISSUER_ID`),
  KEY `UPDATER_TO_USER` (`UPDATER_ID`),
  KEY `TERMINATION_TO_TERMINATION_CODE` (`TERMINATION_REASON_CODE`),
  CONSTRAINT `ISSUER_ID_TO_SOURCE` FOREIGN KEY (`AUTHORIZATION_ISSUER_ID`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `ISSUING_COUNTRY_TO_COUNTRY` FOREIGN KEY (`ISSUING_COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `TERMINATION_TO_TERMINATION_CODE` FOREIGN KEY (`TERMINATION_REASON_CODE`) REFERENCES `s_authorization_termination_reasons` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `TYPE_TO_AUTHORIZATION_TYPE` FOREIGN KEY (`AUTHORIZATION_TYPE_ID`, `AUTHORIZATION_ISSUER_ID`) REFERENCES `s_authorization_types` (`ID`, `SOURCE_SYSTEM`),
  CONSTRAINT `UPDATER_TO_USER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `authorizations_history`
--

DROP TABLE IF EXISTS `authorizations_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorizations_history` (
  `UID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'History row UID',
  `UTIME` datetime NOT NULL COMMENT 'The update time of the main table updated record',
  `ID` int(10) unsigned NOT NULL,
  `VESSEL_ID` int(10) unsigned NOT NULL,
  `VESSEL_UID` int(11) unsigned NOT NULL,
  `AUTHORIZATION_HOLDER_ID` int(10) unsigned DEFAULT NULL,
  `AUTHORIZATION_ID` varchar(45) COLLATE utf8_bin NOT NULL,
  `TYPE_ID` varchar(10) COLLATE utf8_bin NOT NULL,
  `VALID_FROM` date DEFAULT NULL,
  `VALID_TO` date DEFAULT NULL,
  `REFERENCE_DATE` datetime NOT NULL,
  `TERMINATION_REASON_CODE` int(10) unsigned DEFAULT NULL,
  `TERMINATION_REASON` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `TERMINATION_REFERENCE_DATE` datetime DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL,
  `ISSUING_COUNTRY_ID` int(10) unsigned NOT NULL,
  `UPDATE_DATE` datetime NOT NULL,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL,
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`UID`),
  KEY `HUTIME_INDEX` (`UTIME`),
  KEY `HVESSEL_IDS_INDEX` (`VESSEL_ID`,`VESSEL_UID`),
  KEY `HID_INDEX` (`ID`),
  KEY `HAUTH_ID_INDEX` (`AUTHORIZATION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=983393 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country_to_designated_ports`
--

DROP TABLE IF EXISTS `country_to_designated_ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_to_designated_ports` (
  `COUNTRY_ID` int(10) unsigned NOT NULL,
  `PORT_ID` int(10) unsigned NOT NULL,
  `SOURCE_SYSTEM` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `COMMENT` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`COUNTRY_ID`,`PORT_ID`),
  KEY `PORT_ID_TO_PORT` (`PORT_ID`),
  KEY `UPDATER_ID_TO_UPDATER` (`UPDATER_ID`),
  KEY `SOURCE_SYSTEM_TO_SYSTEM` (`SOURCE_SYSTEM`),
  CONSTRAINT `COUNTRY_ID_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `PORT_ID_TO_PORT` FOREIGN KEY (`PORT_ID`) REFERENCES `s_ports` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `SOURCE_SYSTEM_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `UPDATER_ID_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `entity`
--

DROP TABLE IF EXISTS `entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The entity unique ID',
  `UID` int(11) unsigned DEFAULT NULL,
  `ENTITY_TYPE_ID` char(3) COLLATE utf8_bin NOT NULL DEFAULT 'IND' COMMENT 'The entity type code',
  `NATIONALITY_ID` int(10) unsigned DEFAULT NULL,
  `NAME` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity name',
  `SIMPLIFIED_NAME` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity (simplified) name',
  `SIMPLIFIED_NAME_SOUNDEX` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity (simplified) name soundex',
  `SALUTATION` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity salutation (for INDIVIDUAL entities only)',
  `BRANCH` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity branch (for ORGANIZATION and COMPANY entities only)',
  `ROLE` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity role (for INDIVIDUAL entities only)',
  `COUNTRY_ID` int(10) unsigned DEFAULT NULL COMMENT 'The entity''s address country ID',
  `CITY` varchar(45) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity''s address city  (if available)',
  `ADDRESS` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity''s address  (if available)',
  `ZIP_CODE` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity''s address ZIP code  (if available)',
  `PHONE_NUMBER` varchar(96) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity''s phone number  (if available)',
  `MOBILE_NUMBER` varchar(96) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity''s mobile number (if available)',
  `FAX_NUMBER` varchar(96) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity''s fax number (if available)',
  `E_MAIL` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity''s e-mail (if available)',
  `WEBSITE` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'The entity website (if available)',
  `MAPS_TO` int(10) unsigned DEFAULT NULL COMMENT 'Links an entity to another one',
  `MAPPING_WEIGHT` double DEFAULT NULL,
  `MAPPING_DATE` datetime DEFAULT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(2048) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `ENTITY_TO_MAPPED_ENTITY` (`MAPS_TO`),
  KEY `CITY_INDEX` (`CITY`),
  KEY `ENTITY_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `ENTITY_TO_COUNTRY` (`COUNTRY_ID`),
  KEY `ENTITY_TO_TYPE` (`ENTITY_TYPE_ID`),
  KEY `ENTITY_TO_MAPPING_USER` (`MAPPING_USER`),
  KEY `ENTITY_TO_UPDATER` (`UPDATER_ID`),
  KEY `ENTITY_TO_NATIONALITY` (`NATIONALITY_ID`),
  CONSTRAINT `ENTITY_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `ENTITY_TO_MAPPED_ENTITY` FOREIGN KEY (`MAPS_TO`) REFERENCES `entity` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `ENTITY_TO_MAPPING_USER` FOREIGN KEY (`MAPPING_USER`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `ENTITY_TO_NATIONALITY` FOREIGN KEY (`NATIONALITY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `ENTITY_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `ENTITY_TO_TYPE` FOREIGN KEY (`ENTITY_TYPE_ID`) REFERENCES `s_entity_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `ENTITY_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=332232 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `remote_request_limit`
--

DROP TABLE IF EXISTS `remote_request_limit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remote_request_limit` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IP_FILTER` varchar(512) NOT NULL DEFAULT '.+',
  `APPLICATION_ID` varchar(16) NOT NULL,
  `MAX_PER_SECOND` int(10) unsigned DEFAULT NULL,
  `MAX_PER_MINUTE` int(10) unsigned DEFAULT NULL,
  `MAX_PER_HOUR` int(10) unsigned DEFAULT NULL,
  `MAX_PER_DAY` int(10) unsigned DEFAULT NULL,
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `remote_request_limit_to_updater` (`UPDATER_ID`),
  CONSTRAINT `remote_request_limit_to_updater` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `remote_request_metadata`
--

DROP TABLE IF EXISTS `remote_request_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remote_request_metadata` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The request unique identifier',
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The request timestamp',
  `TARGET_APP` varchar(16) NOT NULL COMMENT 'The request target application identifier',
  `REQUEST_SOURCE` varchar(45) NOT NULL COMMENT 'The request source IP address and port',
  `REQUEST_SOURCE_COUNTRY` char(2) DEFAULT NULL,
  `REQUEST_TARGET` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The request target IP address',
  `REQUEST_URL` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The request URL',
  `REQUEST_METHOD` varchar(16) NOT NULL DEFAULT 'GET' COMMENT 'The request method',
  `USER_AGENT` varchar(512) DEFAULT NULL,
  `USER_AGENT_TYPE` varchar(45) DEFAULT NULL,
  `IS_BOT` tinyint(1) NOT NULL DEFAULT '0',
  `REFERRER` varchar(2048) DEFAULT NULL,
  `LOGGED_USER` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'The logged user',
  `BLOCKED` tinyint(1) DEFAULT NULL,
  `SESSION_ID` varchar(32) DEFAULT NULL COMMENT 'The session ID',
  `ELAPSED` int(10) unsigned DEFAULT NULL,
  `STATUS_CODE` int(11) DEFAULT NULL,
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `LOGGED_USER_TO_USER2` (`LOGGED_USER`),
  KEY `REQUEST_URL_INDEX` (`REQUEST_URL`(255)),
  KEY `REQUEST_SOURCE_INDEX` (`REQUEST_SOURCE`),
  CONSTRAINT `LOGGED_USER_TO_USER2` FOREIGN KEY (`LOGGED_USER`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2129 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_age_categories`
--

DROP TABLE IF EXISTS `s_age_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_age_categories` (
  `RANGE_FROM` float NOT NULL,
  `RANGE_TO` float DEFAULT NULL,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`RANGE_FROM`) USING BTREE,
  KEY `AGE_CATEGORY_TO_UPDATER` (`UPDATER_ID`),
  KEY `FROM_INDEX` (`RANGE_FROM`) USING BTREE,
  KEY `TO_INDEX` (`RANGE_TO`) USING BTREE,
  CONSTRAINT `AGE_CATEGORY_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_agreement_contributing_parties`
--

DROP TABLE IF EXISTS `s_agreement_contributing_parties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_agreement_contributing_parties` (
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'HSVAR' COMMENT 'The source system / agreement identifier',
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The contributing country ID',
  `DATE_OF_ACCEPTANCE` date DEFAULT NULL COMMENT 'The date of acceptance of the agreement',
  `DATE_OF_LAST_REPORT` date DEFAULT NULL,
  `REPORTED_VESSELS` int(10) unsigned DEFAULT NULL,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`COUNTRY_ID`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `CONTRIBUTING_PARTIES_TO_UPDATER` (`UPDATER_ID`) USING BTREE,
  KEY `CONTRIBUTING_PARTIES_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `DATE_OF_ACCEPTANCE_INDEX` (`DATE_OF_ACCEPTANCE`),
  KEY `DATE_OF_LAST_REPORT_INDEX` (`DATE_OF_LAST_REPORT`),
  CONSTRAINT `CONTRIBUTING_PARTIES_TO_COUNTRIES` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `CONTRIBUTING_PARTIES_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `CONTRIBUTING_PARTIES_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_authority_role`
--

DROP TABLE IF EXISTS `s_authority_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_authority_role` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CODE` char(2) COLLATE utf8_bin NOT NULL COMMENT 'The hull material ID',
  `DESCRIPTION` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'The hull material description',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'FIGIS' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `AUTHORITY_ROLE_TO_UPDATER` (`UPDATER_ID`),
  KEY `AUTHORITY_ROLE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `UNIQUE` (`CODE`,`SOURCE_SYSTEM`),
  CONSTRAINT `AUTHORITY_ROLE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `AUTHORITY_ROLE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_authorization_holders`
--

DROP TABLE IF EXISTS `s_authorization_holders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_authorization_holders` (
  `ID` int(10) NOT NULL COMMENT 'The authorization type identifier',
  `DESCRIPTION` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The authorization type description',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `AUTHORIZATION_HOLDER_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `AUTHORIZATION_HOLDER_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `AUTHORIZATION_HOLDER_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `AUTHORIZATION_HOLDER_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_authorization_termination_reasons`
--

DROP TABLE IF EXISTS `s_authorization_termination_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_authorization_termination_reasons` (
  `ID` int(10) unsigned NOT NULL COMMENT 'The termination reason ID',
  `DESCRIPTION` varchar(256) COLLATE utf8_bin NOT NULL COMMENT 'The termination reason description',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'FIGIS' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `TERMINATION_REASON_TO_UPDATER` (`UPDATER_ID`),
  KEY `TERMINATION_REASON_TO_SYSTEM` (`SOURCE_SYSTEM`),
  CONSTRAINT `TERMINATION_REASON_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `TERMINATION_REASON_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_authorization_types`
--

DROP TABLE IF EXISTS `s_authorization_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_authorization_types` (
  `ID` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'The authorization type identifier',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The source system',
  `COUNTRY_DEPENDANT` tinyint(1) NOT NULL DEFAULT '0',
  `BY_SPECIES` tinyint(1) NOT NULL DEFAULT '0',
  `BY_GEAR` tinyint(1) NOT NULL DEFAULT '0',
  `BY_AREA` tinyint(1) NOT NULL DEFAULT '0',
  `DESCRIPTION` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The authorization type description',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `AUTHORIZATION_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `AUTHORIZATION_TYPE_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `AUTHORIZATION_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `AUTHORIZATION_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_authorization_types_i18n`
--

DROP TABLE IF EXISTS `s_authorization_types_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_authorization_types_i18n` (
  `ID` varchar(10) COLLATE utf8_bin NOT NULL,
  `LANG` char(2) COLLATE utf8_bin NOT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The source system',
  `DESCRIPTION` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The authorization type description',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The updater ID',
  `COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`,`SOURCE_SYSTEM`,`LANG`) USING BTREE,
  KEY `LOC_AUTHORIZATION_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `LOC_AUTHORIZATION_TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `LOC_AUTHORIZATION_TO_LANG_idx` (`LANG`),
  CONSTRAINT `LOC_AUTHORIZATION_TO_LANG` FOREIGN KEY (`LANG`) REFERENCES `s_languages` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_AUTHORIZATION_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `LOC_AUTHORIZATION_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_capability`
--

DROP TABLE IF EXISTS `s_capability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_capability` (
  `ID` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The capability ID',
  `PARAMETERS` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'The capability additional parameters (if needed)',
  `DESCRIPTION` varchar(256) NOT NULL COMMENT 'The capability description',
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The update user',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `CAPABILITY_TO_UPDATER` (`UPDATER_ID`) USING BTREE,
  CONSTRAINT `CAPABILITY_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_capability_group`
--

DROP TABLE IF EXISTS `s_capability_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_capability_group` (
  `ID` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The capability group ID',
  `DESCRIPTION` varchar(64) NOT NULL COMMENT 'The capabilities group description',
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updating user',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `GROUP_TO_UPDATER` (`UPDATER_ID`) USING BTREE,
  CONSTRAINT `GROUP_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_countries`
--

DROP TABLE IF EXISTS `s_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_countries` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The country ID (taken from FIGIS AREA table, when available)',
  `UID` int(11) unsigned DEFAULT NULL,
  `FAOSTAT_CODE` int(10) DEFAULT NULL COMMENT 'The FIGIS country code (when available)',
  `UN_CODE` int(10) unsigned DEFAULT NULL COMMENT 'The UN country code (when available)',
  `UNDP_CODE` char(3) COLLATE utf8_bin DEFAULT NULL,
  `AGROVOC_CODE` int(10) unsigned DEFAULT NULL,
  `GAUL_CODE` int(10) unsigned DEFAULT NULL,
  `ISO_2_CODE` char(2) COLLATE utf8_bin DEFAULT NULL COMMENT 'The ISO2 Country Code',
  `ISO_3_CODE` char(3) COLLATE utf8_bin DEFAULT NULL COMMENT 'The ISO3 Country Code',
  `NAME` varchar(96) CHARACTER SET utf8 NOT NULL COMMENT 'The country name',
  `SIMPLIFIED_NAME` varchar(96) COLLATE utf8_bin DEFAULT NULL COMMENT 'The simplified country name',
  `SIMPLIFIED_NAME_SOUNDEX` varchar(96) COLLATE utf8_bin DEFAULT NULL COMMENT 'The simplified country name soundex',
  `OFFICIAL_NAME` varchar(96) CHARACTER SET utf8 NOT NULL COMMENT 'The country official name',
  `SIMPLIFIED_OFFICIAL_NAME` varchar(96) COLLATE utf8_bin DEFAULT NULL COMMENT 'The simplified country official name',
  `SIMPLIFIED_OFFICIAL_NAME_SOUNDEX` varchar(96) COLLATE utf8_bin DEFAULT NULL COMMENT 'The simplified country official name soundex',
  `MAPS_TO` int(10) unsigned DEFAULT NULL COMMENT 'Links a country to another one',
  `MAPPING_WEIGHT` double DEFAULT NULL,
  `MAPPING_DATE` datetime DEFAULT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `OFFICIAL_NAME_INDEX` (`OFFICIAL_NAME`),
  KEY `SIMPLIFIED_OFFICIAL_NAME_INDEX` (`SIMPLIFIED_OFFICIAL_NAME`),
  KEY `SIMPLIFIED_OFFICIAL_NAME_SOUNDEX_INDEX` (`SIMPLIFIED_OFFICIAL_NAME_SOUNDEX`),
  KEY `COUNTRY_TO_SOURCE_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `COUNTRY_TO_MAPPED_COUNTRY` (`MAPS_TO`),
  KEY `NAME_INDEX` (`NAME`) USING BTREE,
  KEY `SIMPLIFIED_NAME_INDEX` (`SIMPLIFIED_NAME`) USING BTREE,
  KEY `SIMPLIFIED_NAME_SOUNDEX_INDEX` (`SIMPLIFIED_NAME_SOUNDEX`) USING BTREE,
  KEY `COUNTRY_TO_UPDATER` (`UPDATER_ID`) USING BTREE,
  KEY `COUNTRY_TO_MAPPING_USER` (`MAPPING_USER`),
  CONSTRAINT `COUNTRY_TO_MAPPED_COUNTRY` FOREIGN KEY (`MAPS_TO`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `COUNTRY_TO_MAPPING_USER` FOREIGN KEY (`MAPPING_USER`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `COUNTRY_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `COUNTRY_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=281 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`vrmf_root`@`%`*/ /*!50003 TRIGGER COUNTRY_HISTORY_TRIGGER BEFORE UPDATE ON S_COUNTRIES 
FOR EACH ROW
	BEGIN
		IF @VRMF_TRIGGERS_DISABLED = 1 THEN
			SET @SKIPPED = 1;
		ELSE
			INSERT INTO S_COUNTRIES_HISTORY (
				UTIME, 
				COUNTRY_ID, 
				COUNTRY_UID, 
				UNDP_CODE, 
				AGROVOC_CODE, 
				GAUL_CODE, 
				FAOSTAT_CODE, 
				UN_CODE, 
				ISO_2_CODE, 
				ISO_3_CODE, 
				NAME, 
				SIMPLIFIED_NAME, 
				SIMPLIFIED_NAME_SOUNDEX,
				OFFICIAL_NAME, 
				SIMPLIFIED_OFFICIAL_NAME, 
				SIMPLIFIED_OFFICIAL_NAME_SOUNDEX,
				MAPS_TO, 
				MAPPING_WEIGHT, 
				MAPPING_DATE, 
				MAPPING_USER, 
				MAPPING_COMMENT, 
				SOURCE_SYSTEM, 
				UPDATE_DATE, 
				UPDATER_ID, 
				COMMENT
			)
			VALUES (
				CURRENT_TIMESTAMP(),
				OLD.ID,
				OLD.UID,
				OLD.UNDP_CODE, 
				OLD.AGROVOC_CODE, 
				OLD.GAUL_CODE, 
				OLD.FAOSTAT_CODE, 
				OLD.UN_CODE, 
				OLD.ISO_2_CODE, 
				OLD.ISO_3_CODE, 
				OLD.NAME, 
				OLD.SIMPLIFIED_NAME, 
				OLD.SIMPLIFIED_NAME_SOUNDEX,
				OLD.OFFICIAL_NAME, 
				OLD.SIMPLIFIED_OFFICIAL_NAME, 
				OLD.SIMPLIFIED_OFFICIAL_NAME_SOUNDEX,
				OLD.MAPS_TO,
				OLD.MAPPING_WEIGHT,
				OLD.MAPPING_DATE,
				OLD.MAPPING_USER,
				OLD.MAPPING_COMMENT,
				OLD.SOURCE_SYSTEM,
				OLD.UPDATE_DATE,
				OLD.UPDATER_ID,
				OLD.COMMENT
			);
		END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `s_countries_group`
--

DROP TABLE IF EXISTS `s_countries_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_countries_group` (
  `ID` varchar(16) NOT NULL COMMENT 'The country group identifier',
  `DESCRIPTION` varchar(128) NOT NULL COMMENT 'The country group description',
  `VALID_FROM` date NOT NULL COMMENT 'The country group validity starting date',
  `VALID_TO` date DEFAULT NULL COMMENT 'The country group validity ending date',
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The updater identifier',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The updater date',
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `COUNTRY_GROUP_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `COUNTRY_GROUP_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_countries_group_i18n`
--

DROP TABLE IF EXISTS `s_countries_group_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_countries_group_i18n` (
  `ID` varchar(16) CHARACTER SET utf8 NOT NULL,
  `LANG` char(2) COLLATE utf8_bin NOT NULL,
  `DESCRIPTION` varchar(128) CHARACTER SET utf8 NOT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`,`LANG`),
  KEY `LOC_COUNTRY_GROUP_TO_UPDATER` (`UPDATER_ID`),
  KEY `LOC_COUNTRY_GROUP_TO_LANGUAGE_idx` (`LANG`),
  KEY `LOC_COUNTRY_GROUP_TO_SYSTEM_idx` (`SOURCE_SYSTEM`),
  CONSTRAINT `LOC_COUNTRY_GROUP_TO_COUNTRY_GROUP` FOREIGN KEY (`ID`) REFERENCES `s_countries_group` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_COUNTRY_GROUP_TO_LANGUAGE` FOREIGN KEY (`LANG`) REFERENCES `s_languages` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_COUNTRY_GROUP_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_COUNTRY_GROUP_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_countries_history`
--

DROP TABLE IF EXISTS `s_countries_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_countries_history` (
  `UID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UTIME` datetime NOT NULL,
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The country ID (taken from FIGIS AREA table, when available)',
  `COUNTRY_UID` int(11) unsigned DEFAULT NULL,
  `UNDP_CODE` char(3) COLLATE utf8_bin DEFAULT NULL,
  `AGROVOC_CODE` int(10) unsigned DEFAULT NULL,
  `GAUL_CODE` int(10) unsigned DEFAULT NULL,
  `FAOSTAT_CODE` int(10) DEFAULT NULL COMMENT 'The FIGIS country code (when available)',
  `UN_CODE` int(10) unsigned DEFAULT NULL COMMENT 'The UN country code (when available)',
  `ISO_2_CODE` char(2) COLLATE utf8_bin DEFAULT NULL COMMENT 'The ISO2 Country Code',
  `ISO_3_CODE` char(3) COLLATE utf8_bin DEFAULT NULL COMMENT 'The ISO3 Country Code',
  `NAME` varchar(96) CHARACTER SET utf8 NOT NULL COMMENT 'The country name',
  `SIMPLIFIED_NAME` varchar(96) COLLATE utf8_bin DEFAULT NULL COMMENT 'The simplified country name',
  `SIMPLIFIED_NAME_SOUNDEX` varchar(96) COLLATE utf8_bin DEFAULT NULL COMMENT 'The simplified country name soundex',
  `OFFICIAL_NAME` varchar(96) CHARACTER SET utf8 NOT NULL COMMENT 'The country official name',
  `SIMPLIFIED_OFFICIAL_NAME` varchar(96) COLLATE utf8_bin DEFAULT NULL COMMENT 'The simplified country official name',
  `SIMPLIFIED_OFFICIAL_NAME_SOUNDEX` varchar(96) COLLATE utf8_bin DEFAULT NULL COMMENT 'The simplified country official name soundex',
  `MAPS_TO` int(10) unsigned DEFAULT NULL COMMENT 'Links a country to another one',
  `MAPPING_WEIGHT` double DEFAULT NULL,
  `MAPPING_DATE` datetime DEFAULT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` datetime NOT NULL COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`UID`),
  KEY `COUNTRY_HISTORY_OFFICIAL_NAME_INDEX` (`OFFICIAL_NAME`),
  KEY `COUNTRY_HISTORY_SIMPLIFIED_OFFICIAL_NAME_INDEX` (`SIMPLIFIED_OFFICIAL_NAME`),
  KEY `COUNTRY_HISTORY_SIMPLIFIED_OFFICIAL_NAME_SOUNDEX_INDEX` (`SIMPLIFIED_OFFICIAL_NAME_SOUNDEX`),
  KEY `COUNTRY_HISTORY_COUNTRY_TO_SOURCE_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `COUNTRY_HISTORY_COUNTRY_TO_MAPPED_COUNTRY` (`MAPS_TO`),
  KEY `COUNTRY_HISTORY_NAME_INDEX` (`NAME`) USING BTREE,
  KEY `COUNTRY_HISTORY_SIMPLIFIED_NAME_INDEX` (`SIMPLIFIED_NAME`) USING BTREE,
  KEY `COUNTRY_HISTORY_SIMPLIFIED_NAME_SOUNDEX_INDEX` (`SIMPLIFIED_NAME_SOUNDEX`) USING BTREE,
  KEY `COUNTRY_HISTORY_COUNTRY_TO_UPDATER` (`UPDATER_ID`) USING BTREE,
  KEY `COUNTRY_HISTORY_COUNTRY_TO_MAPPING_USER` (`MAPPING_USER`),
  CONSTRAINT `COUNTRY_HISTORY_COUNTRY_TO_MAPPED_COUNTRY` FOREIGN KEY (`MAPS_TO`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `COUNTRY_HISTORY_COUNTRY_TO_MAPPING_USER` FOREIGN KEY (`MAPPING_USER`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `COUNTRY_HISTORY_COUNTRY_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `COUNTRY_HISTORY_COUNTRY_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_countries_i18n`
--

DROP TABLE IF EXISTS `s_countries_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_countries_i18n` (
  `ID` int(10) unsigned NOT NULL,
  `LANG` char(2) COLLATE utf8_bin NOT NULL,
  `NAME` varchar(96) CHARACTER SET utf8 NOT NULL,
  `SIMPLIFIED_NAME` varchar(96) COLLATE utf8_bin DEFAULT NULL,
  `SIMPLIFIED_NAME_SOUNDEX` varchar(96) COLLATE utf8_bin DEFAULT NULL,
  `OFFICIAL_NAME` varchar(96) CHARACTER SET utf8 NOT NULL,
  `SIMPLIFIED_OFFICIAL_NAME` varchar(96) COLLATE utf8_bin DEFAULT NULL,
  `SIMPLIFIED_OFFICIAL_NAME_SOUNDEX` varchar(96) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN',
  `COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`,`LANG`),
  KEY `LOC_OFFICIAL_NAME_INDEX` (`OFFICIAL_NAME`),
  KEY `LOC_SIMPLIFIED_OFFICIAL_NAME_INDEX` (`SIMPLIFIED_OFFICIAL_NAME`),
  KEY `LOC_SIMPLIFIED_OFFICIAL_NAME_SOUNDEX_INDEX` (`SIMPLIFIED_OFFICIAL_NAME_SOUNDEX`),
  KEY `LOC_COUNTRY_TO_SOURCE_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `LOC_COUNTRY_TO_COUNTRY` (`ID`),
  KEY `LOC_NAME_INDEX` (`NAME`) USING BTREE,
  KEY `LOC_SIMPLIFIED_NAME_INDEX` (`SIMPLIFIED_NAME`) USING BTREE,
  KEY `LOC_SIMPLIFIED_NAME_SOUNDEX_INDEX` (`SIMPLIFIED_NAME_SOUNDEX`) USING BTREE,
  KEY `LOC_COUNTRY_TO_UPDATER` (`UPDATER_ID`) USING BTREE,
  KEY `LOC_COUNTRY_TO_LANGUAGE_idx` (`LANG`),
  CONSTRAINT `LOC_COUNTRY_TO_COUNTRY` FOREIGN KEY (`ID`) REFERENCES `s_countries` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_COUNTRY_TO_LANGUAGE` FOREIGN KEY (`LANG`) REFERENCES `s_languages` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_COUNTRY_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_COUNTRY_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_countries_subdivision`
--

DROP TABLE IF EXISTS `s_countries_subdivision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_countries_subdivision` (
  `ID` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The subdivision original ID',
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The country ID',
  `NAME` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'The subdivision name',
  `ALTERNATE_NAME` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'The subdivision alternate name (when available)',
  `TYPE_ID` int(10) unsigned DEFAULT NULL COMMENT 'The subdivision type',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`,`COUNTRY_ID`) USING BTREE,
  KEY `SUBDIVISION_TO_COUNTRY` (`COUNTRY_ID`),
  KEY `SUBDIVISION_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `SUBDIVISION_TO_UPDATER` (`UPDATER_ID`),
  KEY `NAME_INDEX` (`NAME`),
  KEY `ALTERNATE_NAME_INDEX` (`ALTERNATE_NAME`) USING BTREE,
  KEY `SUBDIVISION_TO_TYPE` (`TYPE_ID`),
  CONSTRAINT `SUBDIVISION_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `SUBDIVISION_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `SUBDIVISION_TO_TYPE` FOREIGN KEY (`TYPE_ID`) REFERENCES `s_countries_subdivision_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `SUBDIVISION_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_countries_subdivision_types`
--

DROP TABLE IF EXISTS `s_countries_subdivision_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_countries_subdivision_types` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(128) COLLATE utf8_bin NOT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL,
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `DESCRIPTION_INDEX` (`DESCRIPTION`),
  KEY `SUBDIVISION_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `SUBDIVISION_TYPE_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `SUBDIVISION_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `SUBDIVISION_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_countries_to_callsigns`
--

DROP TABLE IF EXISTS `s_countries_to_callsigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_countries_to_callsigns` (
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The country ID',
  `CALLSIGN_FROM` char(3) COLLATE utf8_bin NOT NULL COMMENT 'The callsign range start',
  `CALLSIGN_TO` char(3) COLLATE utf8_bin NOT NULL COMMENT 'The callsign range end',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`COUNTRY_ID`,`CALLSIGN_FROM`,`CALLSIGN_TO`),
  KEY `CALLSIGN_RANGE_TO_UPDATER` (`UPDATER_ID`) USING BTREE,
  KEY `CALLSIGN_RANGE_TO_SYSTEM` (`SOURCE_SYSTEM`) USING BTREE,
  CONSTRAINT `CALLSIGN_RANGE_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `CALLSIGN_RANGE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `CALLSIGN_RANGE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_countries_to_group`
--

DROP TABLE IF EXISTS `s_countries_to_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_countries_to_group` (
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The country identifier',
  `GROUP_ID` varchar(16) NOT NULL COMMENT 'The group identifier',
  `VALID_FROM` date NOT NULL COMMENT 'The validity starting date',
  `VALID_TO` date DEFAULT NULL COMMENT 'The validity ending date',
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The updater identifier',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`COUNTRY_ID`,`GROUP_ID`,`VALID_FROM`),
  KEY `COUNTRIES_GROUP_TO_COUNTRIES_GROUP` (`GROUP_ID`),
  KEY `COUNTRIES_GROUP_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `COUNTRIES_GROUP_TO_COUNTRIES_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `s_countries_group` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `COUNTRIES_GROUP_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `COUNTRIES_GROUP_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_currencies`
--

DROP TABLE IF EXISTS `s_currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_currencies` (
  `ID` char(3) COLLATE utf8_bin NOT NULL,
  `DESCRIPTION` varchar(45) COLLATE utf8_bin NOT NULL,
  `SYMBOL` char(1) COLLATE utf8_bin NOT NULL,
  `UPDATE_USER` varchar(16) COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `CURRENCY_TO_UPDATER` (`UPDATE_USER`),
  CONSTRAINT `CURRENCY_TO_UPDATER` FOREIGN KEY (`UPDATE_USER`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_entity_types`
--

DROP TABLE IF EXISTS `s_entity_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_entity_types` (
  `ID` char(3) COLLATE utf8_bin NOT NULL COMMENT 'The entity type ID',
  `DESCRIPTION` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'The entity type description',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `ENTITY_TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `ENTITY_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  CONSTRAINT `ENTITY_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `ENTITY_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_fishing_zones`
--

DROP TABLE IF EXISTS `s_fishing_zones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_fishing_zones` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The FAO area ID',
  `UID` int(11) unsigned DEFAULT NULL,
  `ORIGINAL_ID` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The fishing zone original ID',
  `DESCRIPTION` varchar(512) COLLATE utf8_bin NOT NULL COMMENT 'The fishing zone description',
  `MAPS_TO` int(11) unsigned DEFAULT NULL COMMENT 'Links a fishing zone to another one',
  `MAPPING_WEIGHT` double DEFAULT NULL,
  `MAPPING_DATE` datetime DEFAULT NULL,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE KEY `ORIGINAL_ID_UNIQUENESS_INDEX` (`UID`),
  KEY `FISHING_ZONE_TO_UPDATER` (`UPDATER_ID`),
  KEY `FISHING_ZONE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `FISHING_ZONE_TO_MAPPED_ZONE` (`MAPS_TO`),
  KEY `MAPPED_ZONE_TO_MAPPING_USER` (`MAPPING_USER`),
  CONSTRAINT `FISHING_ZONE_TO_MAPPED_ZONE` FOREIGN KEY (`MAPS_TO`) REFERENCES `s_fishing_zones` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FISHING_ZONE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FISHING_ZONE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `MAPPED_ZONE_TO_MAPPING_USER` FOREIGN KEY (`MAPPING_USER`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=312 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_fishing_zones_containment`
--

DROP TABLE IF EXISTS `s_fishing_zones_containment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_fishing_zones_containment` (
  `SOURCE_ZONE_ID` int(10) unsigned NOT NULL,
  `TARGET_ZONE_ID` int(10) unsigned NOT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin NOT NULL,
  `MAPPING_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`SOURCE_ZONE_ID`,`TARGET_ZONE_ID`),
  KEY `CONTAINMENT_SOURCE_TO_AREA` (`SOURCE_ZONE_ID`),
  KEY `CONTAINMENT_TARGET_TO_AREA` (`TARGET_ZONE_ID`),
  KEY `CONTAINMENT_TO_UPDATER` (`MAPPING_USER`),
  CONSTRAINT `CONTAINMENT_SOURCE_TO_AREA` FOREIGN KEY (`SOURCE_ZONE_ID`) REFERENCES `s_fishing_zones` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `CONTAINMENT_TARGET_TO_AREA` FOREIGN KEY (`TARGET_ZONE_ID`) REFERENCES `s_fishing_zones` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `CONTAINMENT_TO_UPDATER` FOREIGN KEY (`MAPPING_USER`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_fishing_zones_intersections`
--

DROP TABLE IF EXISTS `s_fishing_zones_intersections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_fishing_zones_intersections` (
  `SOURCE_ZONE_ID` int(10) unsigned NOT NULL,
  `TARGET_ZONE_ID` int(10) unsigned NOT NULL,
  `INTERSECTION_VALUE` decimal(10,0) DEFAULT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin NOT NULL,
  `MAPPING_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`SOURCE_ZONE_ID`,`TARGET_ZONE_ID`),
  KEY `INTERSECTION_SOURCE_TO_AREA` (`SOURCE_ZONE_ID`),
  KEY `INTERSECTION_TARGET_TO_AREA` (`TARGET_ZONE_ID`),
  KEY `INTERSECTION_TO_UPDATER` (`MAPPING_USER`),
  CONSTRAINT `INTERSECTION_SOURCE_TO_AREA` FOREIGN KEY (`SOURCE_ZONE_ID`) REFERENCES `s_fishing_zones` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `INTERSECTION_TARGET_TO_AREA` FOREIGN KEY (`TARGET_ZONE_ID`) REFERENCES `s_fishing_zones` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `INTERSECTION_TO_UPDATER` FOREIGN KEY (`MAPPING_USER`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_gear_types`
--

DROP TABLE IF EXISTS `s_gear_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_gear_types` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The unqiue gear type code',
  `UID` int(11) unsigned DEFAULT NULL,
  `ORIGINAL_GEAR_TYPE_CODE` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The original gear type code',
  `ISSCFG_CODE` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT 'The ISSCFG gear type code',
  `META_CODE` int(11) DEFAULT NULL COMMENT 'The gear type meta code',
  `STANDARD_ABBREVIATION` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The standard abbreviation',
  `NAME` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'The english name',
  `DESCRIPTION` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT 'The english description',
  `MAPS_TO` int(10) unsigned DEFAULT NULL COMMENT 'Links a gear type to another',
  `MAPPING_WEIGHT` double DEFAULT NULL,
  `MAPPING_DATE` datetime DEFAULT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'FIGIS' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `GEAR_UNIQUE_INDEX` (`ORIGINAL_GEAR_TYPE_CODE`,`NAME`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `GEAR_TYPE_TO_MAPPED_GEAR_TYPE` (`MAPS_TO`),
  KEY `GEAR_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `GEAR_TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `GEAR_TYPES_TO_MAPPING_USER` (`MAPPING_USER`),
  KEY `ISSCFG_INDEX` (`ISSCFG_CODE`),
  KEY `ENGLISH_NAME_INDEX` (`NAME`),
  CONSTRAINT `GEAR_TYPES_TO_MAPPING_USER` FOREIGN KEY (`MAPPING_USER`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `GEAR_TYPE_TO_MAPPED_GEAR_TYPE` FOREIGN KEY (`MAPS_TO`) REFERENCES `s_gear_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `GEAR_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `GEAR_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=416 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='Taken from Oracle HSVAR - table FIGIS_REF_VESSEL_TYPE';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_gear_types_i18n`
--

DROP TABLE IF EXISTS `s_gear_types_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_gear_types_i18n` (
  `ID` int(10) unsigned NOT NULL,
  `LANG` char(2) COLLATE utf8_bin NOT NULL,
  `NAME` varchar(128) COLLATE utf8_bin NOT NULL,
  `DESCRIPTION` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'FIGIS',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN',
  `COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`,`LANG`),
  KEY `LOC_GEAR_TYPE_TO_GEAR_TYPE` (`ID`),
  KEY `LOC_GEAR_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `LOC_GEAR_TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `LOC_NAME_INDEX` (`NAME`),
  KEY `LOC_GEAR_TYPE_TO_LANGUAGE_idx` (`LANG`),
  CONSTRAINT `LOC_GEAR_TYPE_TO_GEAR_TYPE` FOREIGN KEY (`ID`) REFERENCES `s_gear_types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_GEAR_TYPE_TO_LANGUAGE` FOREIGN KEY (`LANG`) REFERENCES `s_languages` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_GEAR_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_GEAR_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_groups_to_capabilities`
--

DROP TABLE IF EXISTS `s_groups_to_capabilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_groups_to_capabilities` (
  `GROUP_ID` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The group ID',
  `CAPABILITY_ID` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The capability ID',
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updating user',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`GROUP_ID`,`CAPABILITY_ID`) USING BTREE,
  KEY `CAPABILITY_TO_CAPABILITY` (`CAPABILITY_ID`),
  KEY `CAPABILITY_GROUP_TO_UPDATER` (`UPDATER_ID`) USING BTREE,
  CONSTRAINT `CAPABILITY_GROUP_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `CAPABILITY_TO_CAPABILITY` FOREIGN KEY (`CAPABILITY_ID`) REFERENCES `s_capability` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `CAPABILITY_TO_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `s_capability_group` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_hull_material`
--

DROP TABLE IF EXISTS `s_hull_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_hull_material` (
  `ID` char(2) COLLATE utf8_bin NOT NULL COMMENT 'The hull material ID',
  `DESCRIPTION` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'The hull material description',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'FIGIS' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `HULL_MATERIAL_TO_UPDATER` (`UPDATER_ID`),
  KEY `HULL_MATERIAL_TO_SYSTEM` (`SOURCE_SYSTEM`),
  CONSTRAINT `HULL_MATERIAL_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `HULL_MATERIAL_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_identifier_types`
--

DROP TABLE IF EXISTS `s_identifier_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_identifier_types` (
  `ID` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'The vessel identifier type ID',
  `IS_UNIQUE` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'The identifier type uniqueness',
  `IS_PUBLIC` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'The identifier type publicness',
  `PRIORITY` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'The identifier priority by uniqueness',
  `DESCRIPTION` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The vessel identifier type description',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE KEY `PRIORITY_PUBLICNESS_INDEX` (`IS_PUBLIC`,`PRIORITY`) USING BTREE,
  KEY `IDENTIFIER_TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `IDENTIFIER_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  CONSTRAINT `IDENTIFIER_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `IDENTIFIER_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_inspection_infringement_type`
--

DROP TABLE IF EXISTS `s_inspection_infringement_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_inspection_infringement_type` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CODE` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'The non compliance infringement type identifier',
  `DESCRIPTION` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT 'The non compliance infringement type description',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQUE_INDEX` (`CODE`,`SOURCE_SYSTEM`),
  KEY `INSPECTION_INFRINGEMENT_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `INSPECTION_INFRINGEMENT_TYPE_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `INSPECTION_INFRINGEMENT_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `INSPECTION_INFRINGEMENT_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_inspection_outcome_type`
--

DROP TABLE IF EXISTS `s_inspection_outcome_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_inspection_outcome_type` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CODE` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'The non compliance outcome type identifier',
  `DESCRIPTION` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT 'The non compliance outcome type description',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQUE_INDEX` (`CODE`,`SOURCE_SYSTEM`),
  KEY `INSPECTION_OUTCOME_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `INSPECTION_OUTCOME_TYPE_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `INSPECTION_OUTCOME_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `INSPECTION_OUTCOME_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_inspection_report_type`
--

DROP TABLE IF EXISTS `s_inspection_report_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_inspection_report_type` (
  `ID` int(10) unsigned NOT NULL,
  `CODE` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'The inspection report type identifier',
  `DESCRIPTION` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The inspection report type description',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQUE_INDEX` (`CODE`,`SOURCE_SYSTEM`),
  KEY `INSPECTION_REPORT_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `INSPECTION_REPORT_TYPE_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `INSPECTION_REPORT_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `INSPECTION_REPORT_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_languages`
--

DROP TABLE IF EXISTS `s_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_languages` (
  `ID` char(2) COLLATE utf8_bin NOT NULL,
  `NAME` varchar(45) COLLATE utf8_bin NOT NULL,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LANGUAGES_TO_UPDATER_idx` (`UPDATER_ID`),
  CONSTRAINT `LANGUAGES_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_length_categories`
--

DROP TABLE IF EXISTS `s_length_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_length_categories` (
  `LENGTH_TYPE_ID` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `RANGE_FROM` float NOT NULL,
  `RANGE_TO` float NOT NULL,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`LENGTH_TYPE_ID`,`RANGE_FROM`,`RANGE_TO`) USING BTREE,
  KEY `LENGTH_CATEGORY_TO_UPDATER` (`UPDATER_ID`),
  KEY `FROM_INDEX` (`RANGE_FROM`) USING BTREE,
  KEY `TO_INDEX` (`RANGE_TO`) USING BTREE,
  CONSTRAINT `LENGTH_CATEGORY_TO_LENGTH` FOREIGN KEY (`LENGTH_TYPE_ID`) REFERENCES `s_length_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `LENGTH_CATEGORY_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_length_types`
--

DROP TABLE IF EXISTS `s_length_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_length_types` (
  `ID` varchar(3) COLLATE utf8_bin NOT NULL COMMENT 'The length type ID',
  `DESCRIPTION` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'The length type description',
  `DEFAULT_UNIT` varchar(3) COLLATE utf8_bin NOT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'FIGIS' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `LENGTH_TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `LENGTH_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `LENGTH_TYPE_TO_UNIT` (`DEFAULT_UNIT`),
  CONSTRAINT `LENGTH_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `LENGTH_TYPE_TO_UNIT` FOREIGN KEY (`DEFAULT_UNIT`) REFERENCES `s_measure_units` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `LENGTH_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_length_types_i18n`
--

DROP TABLE IF EXISTS `s_length_types_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_length_types_i18n` (
  `ID` varchar(3) COLLATE utf8_bin NOT NULL,
  `LANG` char(2) COLLATE utf8_bin NOT NULL,
  `DESCRIPTION` varchar(45) COLLATE utf8_bin NOT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT 'ADMIN',
  `COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`,`LANG`),
  UNIQUE KEY `LOC_LENGTH_UNIQUE` (`ID`,`LANG`),
  KEY `LOC_LENGTH_TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `LOC_LENGTH_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `LOC_LENGTH_TO_LANG_idx` (`LANG`),
  CONSTRAINT `LOC_LENGTH_TO_LANG` FOREIGN KEY (`LANG`) REFERENCES `s_languages` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_LENGTH_TO_TONNAGE` FOREIGN KEY (`ID`) REFERENCES `s_length_types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_LENGTH_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_LENGTH_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_measure_units`
--

DROP TABLE IF EXISTS `s_measure_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_measure_units` (
  `ID` varchar(3) COLLATE utf8_bin NOT NULL,
  `DESCRIPTION` varchar(45) COLLATE utf8_bin NOT NULL,
  `SYMBOL` varchar(6) COLLATE utf8_bin NOT NULL,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `UNIT_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `UNIT_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_non_compliance_infringement_type`
--

DROP TABLE IF EXISTS `s_non_compliance_infringement_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_non_compliance_infringement_type` (
  `ID` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'The non compliance infringement type identifier',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The source system',
  `DESCRIPTION` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The non compliance infringement type description',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `NON_COMPLIANCE_INFRINGEMENT_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `NON_COMPLIANCE_INFRINGEMENT_TYPE_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `NON_COMPLIANCE_INFRINGEMENT_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `NON_COMPLIANCE_INFRINGEMENT_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_non_compliance_outcome_type`
--

DROP TABLE IF EXISTS `s_non_compliance_outcome_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_non_compliance_outcome_type` (
  `ID` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'The non compliance outcome type identifier',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The source system',
  `DESCRIPTION` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The non compliance outcome type description',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `NON_COMPLIANCE_OUTCOME_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `NON_COMPLIANCE_OUTCOME_TYPE_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `NON_COMPLIANCE_OUTCOME_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `NON_COMPLIANCE_OUTCOME_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_non_compliance_source_type`
--

DROP TABLE IF EXISTS `s_non_compliance_source_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_non_compliance_source_type` (
  `ID` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'The non compliance source type identifier',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The source system',
  `DESCRIPTION` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The non compliance source type description',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `NON_COMPLIANCE_SOURCE_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `NON_COMPLIANCE_SOURCE_TYPE_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `NON_COMPLIANCE_SOURCE_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `NON_COMPLIANCE_SOURCE_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_port_entry_denial_reason`
--

DROP TABLE IF EXISTS `s_port_entry_denial_reason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_port_entry_denial_reason` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CODE` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'The port entry denial reason identifier',
  `DESCRIPTION` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT 'The port entry denial reason description',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQUE_INDEX` (`CODE`,`SOURCE_SYSTEM`),
  KEY `PORT_ENTRY_DENIAL_REASON_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `PORT_ENTRY_DENIAL_REASON_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `PORT_ENTRY_DENIAL_REASON_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PORT_ENTRY_DENIAL_REASON_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_ports`
--

DROP TABLE IF EXISTS `s_ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_ports` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The port unique ID',
  `UID` int(11) unsigned DEFAULT NULL,
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The port country ID',
  `ORIGINAL_PORT_ID` varchar(64) COLLATE utf8_bin NOT NULL COMMENT 'The original port ID (as retrieved from the data source)',
  `SUBDIVISION_ID` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The country subdivision the port belongs to',
  `NAME` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT 'The port name.',
  `SIMPLIFIED_NAME` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The port simplified name.',
  `SIMPLIFIED_NAME_SOUNDEX` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'The port simplified name soundex.',
  `MAPS_TO` int(10) unsigned DEFAULT NULL COMMENT 'Links a port to another one',
  `MAPPING_WEIGHT` double DEFAULT NULL,
  `MAPPING_DATE` datetime DEFAULT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'FIGIS' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PORT_CODE_UNIQUE_INDEX` (`COUNTRY_ID`,`ORIGINAL_PORT_ID`,`SOURCE_SYSTEM`,`NAME`) USING BTREE,
  KEY `PORT_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `PORT_TO_UPDATER` (`UPDATER_ID`),
  KEY `PORT_TO_SUBDIVISION` (`SUBDIVISION_ID`,`COUNTRY_ID`),
  KEY `PORT_NAME_INDEX` (`NAME`),
  KEY `PORT_SIMPLIFIED_NAME_INDEX` (`SIMPLIFIED_NAME`),
  KEY `PORT_SIMPLIFIED_NAME_SOUNDEX_INDEX` (`SIMPLIFIED_NAME_SOUNDEX`),
  KEY `PORTS_TO_MAPPING_USER` (`MAPPING_USER`),
  KEY `PORTS_TO_MAPPED_PORT` (`MAPS_TO`),
  CONSTRAINT `PORTS_TO_MAPPED_PORT` FOREIGN KEY (`MAPS_TO`) REFERENCES `s_ports` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PORTS_TO_MAPPING_USER` FOREIGN KEY (`MAPPING_USER`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `PORT_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `PORT_TO_SUBDIVISION` FOREIGN KEY (`SUBDIVISION_ID`, `COUNTRY_ID`) REFERENCES `s_countries_subdivision` (`ID`, `COUNTRY_ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `PORT_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `PORT_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=84875 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='Taken from Oracle HSVAR - table FIGIS_REF_PORT; InnoDB free:';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`vrmf_root`@`%`*/ /*!50003 TRIGGER `PORTS_HISTORY_TRIGGER` BEFORE UPDATE ON `s_ports` FOR EACH ROW BEGIN
    INSERT INTO `S_PORTS_HISTORY` (PORT_ID, PORT_UID, COUNTRY_ID, ORIGINAL_PORT_ID, SUBDIVISION_ID, NAME, SIMPLIFIED_NAME, SIMPLIFIED_NAME_SOUNDEX, MAPS_TO, MAPPING_WEIGHT, MAPPING_DATE, MAPPING_USER, MAPPING_COMMENT, SOURCE_SYSTEM, UPDATE_DATE, UPDATER_ID)
    VALUES (
      OLD.ID,
      OLD.UID,
      OLD.COUNTRY_ID,
      OLD.ORIGINAL_PORT_ID,
      OLD.SUBDIVISION_ID,
      OLD.NAME,
      OLD.SIMPLIFIED_NAME,
      OLD.SIMPLIFIED_NAME_SOUNDEX,
      OLD.MAPS_TO,
      OLD.MAPPING_WEIGHT,
      OLD.MAPPING_DATE,
      OLD.MAPPING_USER,
      OLD.MAPPING_COMMENT,
      OLD.SOURCE_SYSTEM,
      OLD.UPDATE_DATE,
      OLD.UPDATER_ID
    );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `s_ports_coordinates`
--

DROP TABLE IF EXISTS `s_ports_coordinates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_ports_coordinates` (
  `PORT_ID` int(11) unsigned NOT NULL COMMENT 'The referenced port ID',
  `PORT_UID` int(11) unsigned NOT NULL,
  `LATITUDE` varchar(12) COLLATE utf8_bin NOT NULL COMMENT 'The latitude (in degrees, minutes, qualifier). E.g. 85.37N',
  `LONGITUDE` varchar(12) COLLATE utf8_bin NOT NULL COMMENT 'The longitude (in degrees, minutes, qualifier). E.g. 85.37W',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The coordinates source',
  `REFERENCE_DATE` date NOT NULL,
  `CHECKSUM` char(32) COLLATE utf8_bin DEFAULT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update timestamp',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`PORT_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  UNIQUE KEY `CHECKSUM_INDEX` (`CHECKSUM`),
  KEY `PORT_COORDINATES_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `PORT_COORDINATES_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `PORTS_COORDINATES_TO_PORT` FOREIGN KEY (`PORT_ID`) REFERENCES `s_ports` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `PORT_COORDINATES_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `PORT_COORDINATES_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_ports_history`
--

DROP TABLE IF EXISTS `s_ports_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_ports_history` (
  `UID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UID for the row (needless?)',
  `PORT_ID` int(10) unsigned NOT NULL COMMENT 'The port unique ID',
  `PORT_UID` int(11) unsigned DEFAULT NULL,
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The port country ID',
  `ORIGINAL_PORT_ID` varchar(64) COLLATE utf8_bin NOT NULL COMMENT 'The original port ID (as retrieved from the data source)',
  `SUBDIVISION_ID` varchar(16) COLLATE utf8_bin DEFAULT NULL COMMENT 'The country subdivision the port belongs to',
  `NAME` varchar(64) COLLATE utf8_bin NOT NULL COMMENT 'The port name.',
  `LATITUDE` varchar(12) COLLATE utf8_bin DEFAULT NULL COMMENT 'The port longitude data (with the related emisphere)',
  `LONGITUDE` varchar(12) COLLATE utf8_bin DEFAULT NULL COMMENT 'The port latitude data (with the related emisphere)',
  `SIMPLIFIED_NAME` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'The port simplified name.',
  `SIMPLIFIED_NAME_SOUNDEX` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'The port simplified name soundex.',
  `MAPS_TO` int(10) unsigned DEFAULT NULL COMMENT 'Links a port to another one',
  `MAPPING_WEIGHT` double DEFAULT NULL,
  `MAPPING_DATE` datetime DEFAULT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'FIGIS' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  `GLAT` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `GLON` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`UID`),
  KEY `HPORT_TO_MAPPED_PORT` (`MAPS_TO`),
  KEY `HPORT_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `HPORT_TO_SUBDIVISION` (`SUBDIVISION_ID`,`COUNTRY_ID`),
  KEY `HPORT_TO_COUNTRY` (`COUNTRY_ID`),
  KEY `HPORTS_TO_MAPPING_USER` (`MAPPING_USER`)
) ENGINE=InnoDB AUTO_INCREMENT=91013 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='Taken from Oracle HSVAR - table FIGIS_REF_PORT; InnoDB free:';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_power_categories`
--

DROP TABLE IF EXISTS `s_power_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_power_categories` (
  `POWER_TYPE_ID` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `RANGE_FROM` float NOT NULL,
  `RANGE_TO` float NOT NULL,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`POWER_TYPE_ID`,`RANGE_FROM`,`RANGE_TO`) USING BTREE,
  KEY `POWER_CATEGORY_TO_UPDATER` (`UPDATER_ID`),
  KEY `FROM_INDEX` (`RANGE_FROM`) USING BTREE,
  KEY `TO_INDEX` (`RANGE_TO`) USING BTREE,
  CONSTRAINT `POWER_CATEGORY_TO_POWER` FOREIGN KEY (`POWER_TYPE_ID`) REFERENCES `s_power_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `POWER_CATEGORY_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_power_types`
--

DROP TABLE IF EXISTS `s_power_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_power_types` (
  `ID` varchar(3) COLLATE utf8_bin NOT NULL COMMENT 'The power type ID',
  `DESCRIPTION` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'The power type description',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `POWER_TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `POWER_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  CONSTRAINT `POWER_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `POWER_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_rfmo_iuu_lists`
--

DROP TABLE IF EXISTS `s_rfmo_iuu_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_rfmo_iuu_lists` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RFMO_ID` varchar(16) COLLATE utf8_bin NOT NULL,
  `CODE` varchar(45) COLLATE utf8_bin NOT NULL,
  `URL` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION` varchar(256) COLLATE utf8_bin NOT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQUE_INDEX` (`RFMO_ID`,`CODE`,`SOURCE_SYSTEM`),
  KEY `IUU_LIST_TO_SYSTEM_idx` (`SOURCE_SYSTEM`),
  KEY `RFMO_ID_UNIQUE` (`RFMO_ID`),
  CONSTRAINT `IUU_LIST_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `RFMO_TO_SYSTEM` FOREIGN KEY (`RFMO_ID`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_species`
--

DROP TABLE IF EXISTS `s_species`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_species` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The specie unique ID',
  `UID` int(11) unsigned DEFAULT NULL,
  `FIGIS_ID` int(10) unsigned DEFAULT NULL COMMENT 'The original FIGIS ID for the specie (when available)',
  `ISSCAAP_CODE` char(2) COLLATE utf8_bin NOT NULL COMMENT 'The specie ISSCAAP code',
  `TAXOCODE` varchar(13) COLLATE utf8_bin NOT NULL COMMENT 'The specie taxocode',
  `CODE_3A` char(3) COLLATE utf8_bin NOT NULL COMMENT 'The specie 3A code',
  `SCIENTIFIC_NAME` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'The specie scientific name',
  `SCIENTIFIC_NAME_SOUNDEX` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `NAME` varchar(45) COLLATE utf8_bin DEFAULT NULL COMMENT 'The specie english name',
  `SIMPLIFIED_NAME` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `SIMPLIFIED_NAME_SOUNDEX` varchar(90) COLLATE utf8_bin DEFAULT NULL,
  `FAMILY` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT 'The specie family',
  `SPECIE_ORDER` varchar(64) COLLATE utf8_bin NOT NULL COMMENT 'The specie family',
  `STATS_DATA` tinyint(1) NOT NULL COMMENT 'Set to 1 if capture or aquacolture stats are available',
  `MAPS_TO` int(10) unsigned DEFAULT NULL COMMENT 'Links a specie to another',
  `MAPPING_WEIGHT` double DEFAULT NULL,
  `MAPPING_DATE` datetime DEFAULT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'FIGIS' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TAXOCODE_UNIQUE_NDEX` (`TAXOCODE`),
  UNIQUE KEY `3A_CODE_UNIQUE_INDEX` (`CODE_3A`),
  KEY `SPECIE_TO_MAPPED_SPECIE` (`MAPS_TO`),
  KEY `SPECIE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `SPECIE_TO_UPDATER` (`UPDATER_ID`),
  KEY `SPECIES_TO_MAPPING_USER` (`MAPPING_USER`),
  CONSTRAINT `SPECIES_TO_MAPPING_USER` FOREIGN KEY (`MAPPING_USER`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `SPECIE_TO_MAPPED_SPECIE` FOREIGN KEY (`MAPS_TO`) REFERENCES `s_species` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `SPECIE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `SPECIE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10851 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_systems`
--

DROP TABLE IF EXISTS `s_systems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_systems` (
  `ID` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The Data Source System ID',
  `NAME` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'The Data Source System name',
  `WEBSITE` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT 'The source system website (if available)',
  `IS_PUBLIC` tinyint(1) DEFAULT '0',
  `VESSEL_SOURCE` tinyint(1) DEFAULT '0' COMMENT 'Tells whether this data source provides vessel details',
  `AUTH_SOURCE` tinyint(1) DEFAULT '0',
  `CROSS_SYSTEMS_DATA` tinyint(1) DEFAULT '0',
  `SYSTEM_OWNER` int(10) unsigned DEFAULT NULL COMMENT 'The Data Source System owner',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `SYSTEM_OWNER_TO_ENTITY` (`SYSTEM_OWNER`),
  KEY `SYSTEM_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `SYSTEM_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_systems_groups`
--

DROP TABLE IF EXISTS `s_systems_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_systems_groups` (
  `ID` int(10) unsigned NOT NULL COMMENT 'The systems group ID',
  `DESCRIPTION` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'The system group description',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `SYS_GROUPS_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `SYS_GROUPS_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_systems_i18n`
--

DROP TABLE IF EXISTS `s_systems_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_systems_i18n` (
  `ID` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The Data Source System ID',
  `LANG` char(2) COLLATE utf8_bin NOT NULL,
  `NAME` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'The Data Source System name',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-entry text',
  PRIMARY KEY (`ID`,`LANG`),
  KEY `LOC_SYSTEM_TO_UPDATER` (`UPDATER_ID`),
  KEY `LOC_SYSTEMS_TO_LANGUAGE_idx` (`LANG`),
  CONSTRAINT `LOC_SYSTEMS_TO_LANGUAGE` FOREIGN KEY (`LANG`) REFERENCES `s_languages` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_SYSTEMS_TO_S_SYSTEMS` FOREIGN KEY (`ID`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_SYSTEMS_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_systems_to_groups`
--

DROP TABLE IF EXISTS `s_systems_to_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_systems_to_groups` (
  `GROUP_ID` int(11) unsigned NOT NULL COMMENT 'The group ID',
  `SYSTEM_ID` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The system ID',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`GROUP_ID`,`SYSTEM_ID`) USING BTREE,
  KEY `SYS_GROUP_TO_UPDATER` (`UPDATER_ID`),
  KEY `SYS_GROUP_TO_SYSTEM` (`SYSTEM_ID`),
  CONSTRAINT `SYS_GROUP_TO_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `s_systems_groups` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `SYS_GROUP_TO_SYSTEM` FOREIGN KEY (`SYSTEM_ID`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SYS_GROUP_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_tonnage_categories`
--

DROP TABLE IF EXISTS `s_tonnage_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_tonnage_categories` (
  `TONNAGE_TYPE_ID` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `RANGE_FROM` float NOT NULL,
  `RANGE_TO` float NOT NULL,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`TONNAGE_TYPE_ID`,`RANGE_FROM`,`RANGE_TO`) USING BTREE,
  KEY `TONNAGE_CATEGORY_TO_UPDATER` (`UPDATER_ID`),
  KEY `FROM_INDEX` (`RANGE_FROM`) USING BTREE,
  KEY `TO_INDEX` (`RANGE_TO`) USING BTREE,
  CONSTRAINT `TONNAGE_CATEGORY_TO_TONNAGE` FOREIGN KEY (`TONNAGE_TYPE_ID`) REFERENCES `s_tonnage_types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `TONNAGE_CATEGORY_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_tonnage_types`
--

DROP TABLE IF EXISTS `s_tonnage_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_tonnage_types` (
  `ID` varchar(3) COLLATE utf8_bin NOT NULL COMMENT 'The tonnage type ID',
  `DESCRIPTION` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'The tonnage type description',
  `DEFAULT_UNIT` varchar(3) COLLATE utf8_bin NOT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `TONNAGE_TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `TONNAGE_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `TONNAGE_TO_UNIT` (`DEFAULT_UNIT`),
  CONSTRAINT `TONNAGE_TO_UNIT` FOREIGN KEY (`DEFAULT_UNIT`) REFERENCES `s_measure_units` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `TONNAGE_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `TONNAGE_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_tonnage_types_i18n`
--

DROP TABLE IF EXISTS `s_tonnage_types_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_tonnage_types_i18n` (
  `ID` varchar(3) COLLATE utf8_bin NOT NULL,
  `LANG` char(2) COLLATE utf8_bin NOT NULL,
  `DESCRIPTION` varchar(45) COLLATE utf8_bin NOT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT 'ADMIN',
  `COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`,`LANG`),
  UNIQUE KEY `LOC_TONNAGE_UNIQUE` (`ID`,`LANG`),
  KEY `LOC_TONNAGE_TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `LOC_TONNAGE_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `LOC_TONNAGE_TO_LANG_idx` (`LANG`),
  CONSTRAINT `LOC_TONNAGE_TO_LANG` FOREIGN KEY (`LANG`) REFERENCES `s_languages` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_TONNAGE_TO_TONNAGE` FOREIGN KEY (`ID`) REFERENCES `s_tonnage_types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_TONNAGE_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_TONNAGE_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_uid_ranges`
--

DROP TABLE IF EXISTS `s_uid_ranges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_uid_ranges` (
  `FROM` int(10) unsigned NOT NULL,
  `TO` int(10) unsigned DEFAULT NULL,
  `CURRENT` int(10) unsigned NOT NULL,
  `SOURCE_SYSTEM` varchar(16) NOT NULL,
  `UPDATER_ID` varchar(16) NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`FROM`,`SOURCE_SYSTEM`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_vessel_status`
--

DROP TABLE IF EXISTS `s_vessel_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_vessel_status` (
  `ID` char(2) COLLATE utf8_bin NOT NULL COMMENT 'The status ID',
  `DESCRIPTION` varchar(45) COLLATE utf8_bin NOT NULL COMMENT 'The status description',
  `IN_SERVICE` tinyint(1) DEFAULT NULL COMMENT 'Marks statuses that model the vessel as being in service',
  `TEMPORARILY_NOT_IN_SERVICE` tinyint(1) DEFAULT NULL COMMENT 'Marks statuses that model the vessel as being TEMPORARILY not in service',
  `NOT_IN_SERVICE` tinyint(1) DEFAULT NULL COMMENT 'Marks statuses that model the vessel as being not in service',
  `PERMANENTLY_NOT_IN_SERVICE` tinyint(1) DEFAULT NULL COMMENT 'Marks statuses that model the vessel as being PERMANENTLY not in service',
  `IS_UNRECOVERABLE` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'If true, marks transitions from this state to another unavailable',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'FIGIS' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `VESSEL_STATUS_TO_UPDATER` (`UPDATER_ID`),
  KEY `VESSEL_STATUS_TO_SYSTEM` (`SOURCE_SYSTEM`),
  CONSTRAINT `VESSEL_STATUS_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `VESSEL_STATUS_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_vessel_types`
--

DROP TABLE IF EXISTS `s_vessel_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_vessel_types` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The unique vessel type code',
  `UID` int(11) unsigned DEFAULT NULL,
  `ORIGINAL_VESSEL_TYPE_ID` varchar(64) COLLATE utf8_bin NOT NULL COMMENT 'The original vessel type code',
  `COUNTRY_ID` int(10) unsigned DEFAULT NULL COMMENT 'The country ID this vessel type is used by',
  `ISSCFV_CODE` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT 'The ISSCFV vessel type code, with ''UNKNOWN'', ''TO BE DETERMINED'', ''UNAVAILABLE'', ''NOT APPLICABLE'', ''NOT SPECIFIED'', and ''NON FISHING VESSELS'' as additions.',
  `META_CODE` int(11) unsigned DEFAULT NULL,
  `STANDARD_ABBREVIATION` varchar(6) COLLATE utf8_bin DEFAULT NULL COMMENT 'The standard abbreviation',
  `NAME` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'The english name',
  `DESCRIPTION` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT 'The english description',
  `LOA_FROM` int(10) unsigned DEFAULT NULL COMMENT 'Lenght Overall range start (NULL for unspecified)',
  `LOA_TO` int(10) unsigned DEFAULT NULL COMMENT 'Lenght Overall range end (NULL for unspecified)',
  `GRT_FROM` int(10) unsigned DEFAULT NULL COMMENT 'GRT range start (NULL for unspecified)',
  `GRT_TO` int(10) unsigned DEFAULT NULL COMMENT 'GRT range end (NULL for unspecified)',
  `KW_FROM` int(10) unsigned DEFAULT NULL COMMENT 'Power range start (NULL for unspecified)',
  `KW_TO` int(10) unsigned DEFAULT NULL COMMENT 'Power range end (NULL for unspecified)',
  `MAPS_TO` int(10) unsigned DEFAULT NULL COMMENT 'Links the vessel type to another one',
  `MAPPING_WEIGHT` double DEFAULT NULL,
  `MAPPING_DATE` datetime DEFAULT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'FIGIS' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `VESSEL_TYPE_UNIQUE_INDEX` (`ORIGINAL_VESSEL_TYPE_ID`,`COUNTRY_ID`,`SOURCE_SYSTEM`,`NAME`) USING BTREE,
  KEY `VESSEL_TYPE_TO_MAPPED_VESSEL_TYPE` (`MAPS_TO`),
  KEY `VESSEL_TYPE_TO_COUNTRY` (`COUNTRY_ID`),
  KEY `VESSEL_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `VESSEL_TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `VESSEL_TYPES_TO_MAPPING_USER` (`MAPPING_USER`),
  KEY `ISSCFV_INDEX` (`ISSCFV_CODE`),
  KEY `ENGLISH_NAME_INDEX` (`NAME`),
  CONSTRAINT `VESSEL_TYPES_TO_MAPPED_TYPE` FOREIGN KEY (`MAPS_TO`) REFERENCES `s_vessel_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `VESSEL_TYPES_TO_MAPPING_USER` FOREIGN KEY (`MAPPING_USER`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `VESSEL_TYPE_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `VESSEL_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `VESSEL_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8224 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='Taken from Oracle HSVAR - table FIGIS_REF_VESSEL_TYPE';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_vessel_types_i18n`
--

DROP TABLE IF EXISTS `s_vessel_types_i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_vessel_types_i18n` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LANG` char(2) COLLATE utf8_bin NOT NULL,
  `NAME` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'FIGIS',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN',
  `COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`,`LANG`),
  KEY `LOC_VESSEL_TYPE_TO_VESSEL_TYPE` (`ID`),
  KEY `LOC_VESSEL_TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `LOC_VESSEL_TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `LOC_NAME_INDEX` (`NAME`),
  KEY `LOC_VESSEL_TYPE_TO_LANGUAGE_idx` (`LANG`),
  CONSTRAINT `LOC_VESSEL_TYPE_TO_LANGUAGE` FOREIGN KEY (`LANG`) REFERENCES `s_languages` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_VESSEL_TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LOC_VESSEL_TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `LOC_VESSEL_TYPE_TO_VESSEL_TYPE` FOREIGN KEY (`ID`) REFERENCES `s_vessel_types` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1226 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='Taken from Oracle HSVAR - table FIGIS_REF_VESSEL_TYPE';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `s_vessel_types_isscfv_hierarchy`
--

DROP TABLE IF EXISTS `s_vessel_types_isscfv_hierarchy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_vessel_types_isscfv_hierarchy` (
  `PARENT_TYPE_ID` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The unique vessel type code',
  `TYPE_ID` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The unique vessel type code',
  PRIMARY KEY (`TYPE_ID`),
  KEY `PARENT_TYPE_ID_INDEX` (`PARENT_TYPE_ID`),
  KEY `TYPE_ID_INDEX` (`TYPE_ID`),
  CONSTRAINT `PARENT_TYPE_TO_TYPE_ID` FOREIGN KEY (`PARENT_TYPE_ID`) REFERENCES `s_vessel_types` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `TYPE_TO_TYPE_ID` FOREIGN KEY (`TYPE_ID`) REFERENCES `s_vessel_types` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `ID` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The user ID',
  `PASSWORD` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT 'The user (encrypted) password',
  `ENTITY_ID` int(10) unsigned DEFAULT NULL COMMENT 'The entity ID',
  `GRANTER_ID` int(10) unsigned DEFAULT NULL COMMENT 'The user granter entity ID',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID (if available)',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`),
  KEY `USERS_TO_ENTITY` (`ENTITY_ID`) USING BTREE,
  KEY `USERS_TO_GRANTER` (`GRANTER_ID`),
  KEY `USERS_TO_UPDATER` (`UPDATER_ID`),
  CONSTRAINT `USERS_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_to_capability_groups`
--

DROP TABLE IF EXISTS `users_to_capability_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_to_capability_groups` (
  `USER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `CAPABILITY_GROUP_ID` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALID_FROM` date NOT NULL,
  `VALID_TO` datetime DEFAULT NULL,
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT 'ADMIN',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`USER_ID`,`CAPABILITY_GROUP_ID`) USING BTREE,
  KEY `USER_GROUP_TO_UPDATER` (`UPDATER_ID`),
  KEY `USER_GROUP_TO_CAPABILITY_GROUP` (`CAPABILITY_GROUP_ID`),
  CONSTRAINT `USER_GROUP_TO_CAPABILITY_GROUP` FOREIGN KEY (`CAPABILITY_GROUP_ID`) REFERENCES `s_capability_group` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `USER_GROUP_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `USER_GROUP_TO_USER` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_to_managed_countries`
--

DROP TABLE IF EXISTS `users_to_managed_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_to_managed_countries` (
  `USER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `COUNTRY_ID` int(10) unsigned NOT NULL,
  `SOURCE_SYSTEM` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`USER_ID`,`COUNTRY_ID`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `MANAGED_COUNTRIES_TO_COUNTRY` (`COUNTRY_ID`),
  KEY `MANAGED_COUNTRIES_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `MANAGED_COUNTRIES_TO_UPDATER` (`UPDATER_ID`),
  KEY `MANAGED_COUNTRIES_TO_USERS` (`USER_ID`),
  CONSTRAINT `MANAGED_COUNTRIES_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `MANAGED_COUNTRIES_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `MANAGED_COUNTRIES_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `MANAGED_COUNTRIES_TO_USERS` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_to_managed_sources`
--

DROP TABLE IF EXISTS `users_to_managed_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_to_managed_sources` (
  `USER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SOURCE_SYSTEM` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `MANAGED_SOURCE` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `UPDATER_ID` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COMMENT` varchar(4096) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`USER_ID`,`SOURCE_SYSTEM`,`MANAGED_SOURCE`) USING BTREE,
  KEY `MANAGED_SOURCES_TO_UPDATER` (`UPDATER_ID`),
  KEY `MANAGED_SOURCES_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `MANAGED_SOURCES_TO_SOURCE` (`MANAGED_SOURCE`),
  CONSTRAINT `MANAGED_SOURCES_TO_SOURCE` FOREIGN KEY (`MANAGED_SOURCE`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `MANAGED_SOURCES_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `MANAGED_SOURCES_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `MANAGED_SOURCES_TO_USER` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `v_authorization_types`
--

DROP TABLE IF EXISTS `v_authorization_types`;
/*!50001 DROP VIEW IF EXISTS `v_authorization_types`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_authorization_types` AS SELECT 
 1 AS `IS_DEFAULT`,
 1 AS `ID`,
 1 AS `SOURCE_SYSTEM`,
 1 AS `COUNTRY_DEPENDANT`,
 1 AS `DESCRIPTION`,
 1 AS `UPDATE_DATE`,
 1 AS `UPDATER_ID`,
 1 AS `COMMENT`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_vessels_to_identifiers`
--

DROP TABLE IF EXISTS `v_vessels_to_identifiers`;
/*!50001 DROP VIEW IF EXISTS `v_vessels_to_identifiers`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_vessels_to_identifiers` AS SELECT 
 1 AS `VESSEL_ID`,
 1 AS `VESSEL_UID`,
 1 AS `TYPE_ID`,
 1 AS `IDENTIFIER`,
 1 AS `ALTERNATE_IDENTIFIER`,
 1 AS `REFERENCE_DATE`,
 1 AS `SOURCE_SYSTEM`,
 1 AS `UPDATE_DATE`,
 1 AS `UPDATER_ID`,
 1 AS `COMMENT`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `vessels`
--

DROP TABLE IF EXISTS `vessels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The vessel unique ID',
  `UID` int(11) unsigned NOT NULL COMMENT 'The vessels unique identifier',
  `MAPS_TO` int(10) unsigned DEFAULT NULL COMMENT 'Links a vessels to another one',
  `MAPPING_WEIGHT` double DEFAULT NULL,
  `MAPPING_DATE` datetime DEFAULT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID (if available)',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `VESSEL_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `VESSEL_TO_UPDATER` (`UPDATER_ID`),
  KEY `VESSEL_TO_MAPPED_VESSEL` (`MAPS_TO`),
  KEY `VESSELS_TO_MAPPING_USER` (`MAPPING_USER`),
  KEY `UID_INDEX` (`UID`) USING BTREE,
  CONSTRAINT `VESSELS_TO_MAPPING_USER` FOREIGN KEY (`MAPPING_USER`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `VESSEL_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `VESSEL_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1001643 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`vrmf_root`@`%`*/ /*!50003 TRIGGER VESSEL_UVI_TRIGGER
BEFORE UPDATE
ON VESSELS FOR EACH ROW
  BEGIN
    IF @VRMF_TRIGGERS_DISABLED = 1 THEN
      SET @SKIPPED = 1;
    ELSE
      UPDATE VESSELS_TO_BENEFICIAL_OWNERS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_BUILDING_YEAR SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_CALLSIGNS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_CREW SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_EXTERNAL_MARKINGS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_FISHING_LICENSE SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_FISHING_MASTERS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_FLAGS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_GEARS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_HULL_MATERIAL SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_IDENTIFIERS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_INSPECTIONS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID; 
      UPDATE VESSELS_TO_IUU_LIST SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID; 
      UPDATE VESSELS_TO_LENGTHS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_MASTERS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_MMSI SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_NAME SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_NON_COMPLIANCE SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_OPERATORS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_OWNERS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_POWER SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_PORT_ENTRY_DENIALS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_REGISTRATIONS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_SHIPBUILDERS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_STATUS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_TONNAGE SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_TYPES SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE VESSELS_TO_VMS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
      UPDATE AUTHORIZATIONS SET VESSEL_UID = NEW.UID WHERE VESSEL_ID = NEW.ID;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`vrmf_root`@`%`*/ /*!50003 TRIGGER VESSEL_HISTORY_TRIGGER
BEFORE UPDATE
ON VESSELS FOR EACH ROW
	BEGIN
		IF @VRMF_TRIGGERS_DISABLED = 1 THEN
			SET @SKIPPED = 1;
		ELSE
			INSERT INTO VESSELS_HISTORY (UTIME, VESSEL_ID, VESSEL_UID, MAPS_TO, MAPPING_WEIGHT, MAPPING_DATE, MAPPING_USER, MAPPING_COMMENT, SOURCE_SYSTEM, UPDATE_DATE, UPDATER_ID, COMMENT)
			VALUES (
				CURRENT_TIMESTAMP(),
				OLD.ID,
				OLD.UID,
				OLD.MAPS_TO,
				OLD.MAPPING_WEIGHT,
				OLD.MAPPING_DATE,
				OLD.MAPPING_USER,
				OLD.MAPPING_COMMENT,
				OLD.SOURCE_SYSTEM,
				OLD.UPDATE_DATE,
				OLD.UPDATER_ID,
				OLD.COMMENT
			);
		END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `vessels_history`
--

DROP TABLE IF EXISTS `vessels_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_history` (
  `UID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'UID for the row (needless?)',
  `UTIME` datetime NOT NULL COMMENT 'The update time of the main table updated record',
  `VESSEL_ID` int(11) unsigned NOT NULL COMMENT 'The vessel unique ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `MAPS_TO` int(10) unsigned DEFAULT NULL COMMENT 'Links a vessels to another one',
  `MAPPING_WEIGHT` double DEFAULT NULL,
  `MAPPING_DATE` datetime DEFAULT NULL,
  `MAPPING_USER` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `MAPPING_COMMENT` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID (if available)',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`UID`) USING BTREE,
  KEY `HUTIME_INDEX` (`UTIME`),
  KEY `HVESSEL_IDS_INDEX` (`VESSEL_ID`,`VESSEL_UID`)
) ENGINE=InnoDB AUTO_INCREMENT=678157 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_beneficial_owners`
--

DROP TABLE IF EXISTS `vessels_to_beneficial_owners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_beneficial_owners` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `ENTITY_ID` int(10) unsigned NOT NULL COMMENT 'The beneficial owner ID',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`ENTITY_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `BENEFICIAL_OWNER_TO_VESSEL` (`VESSEL_ID`),
  KEY `BENEFICIAL_OWNER_TO_UPDATER` (`UPDATER_ID`),
  KEY `BENEFICIAL_OWNER_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `BENEFICIAL_OWNERS_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `BENEFICIAL_OWNER_TO_ENTITY` (`ENTITY_ID`),
  CONSTRAINT `BENEFICIAL_OWNER_TO_ENTITY` FOREIGN KEY (`ENTITY_ID`) REFERENCES `entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `BENEFICIAL_OWNER_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `BENEFICIAL_OWNER_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `BENEFICIAL_OWNER_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_building_year`
--

DROP TABLE IF EXISTS `vessels_to_building_year`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_building_year` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `VALUE` int(10) unsigned NOT NULL COMMENT 'The vessel building year',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `BUILDING_YEAR_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `BUILDING_YEAR_TO_UPDATER` (`UPDATER_ID`),
  KEY `BUILDING_YEAR_TO_VESSEL` (`VESSEL_ID`),
  KEY `BUILDING_YEAR_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `BUILDING_YEAR_INDEX` (`VALUE`) USING BTREE,
  KEY `BUILDING_YEAR_BY_SYSTEM_INDEX` (`VALUE`,`SOURCE_SYSTEM`) USING BTREE,
  CONSTRAINT `BUILDING_YEAR_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `BUILDING_YEAR_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `BUILDING_YEAR_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_callsigns`
--

DROP TABLE IF EXISTS `vessels_to_callsigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_callsigns` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned NOT NULL,
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The callsign country flag. Should be included in the primary key if a vessel can have different callsigns valid at the same time under different flags (unlikely)',
  `CALLSIGN_ID` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT 'The callsign',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`REFERENCE_DATE`,`COUNTRY_ID`,`CALLSIGN_ID`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `CALLSIGN_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `CALLSIGN_TO_UPDATER` (`UPDATER_ID`),
  KEY `CALLSIGN_INDEX` (`CALLSIGN_ID`),
  KEY `CALLSIGN_TO_COUNTRY` (`COUNTRY_ID`),
  KEY `IRCS_TO_UVI` (`VESSEL_UID`) USING BTREE,
  CONSTRAINT `CALLSIGN_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `CALLSIGN_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `CALLSIGN_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `CALLSIGN_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_crew`
--

DROP TABLE IF EXISTS `vessels_to_crew`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_crew` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `VALUE` int(11) unsigned NOT NULL COMMENT 'The crew size',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `CREW_TO_VESSEL` (`VESSEL_ID`),
  KEY `CREW_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `CREW_TO_UPDATER` (`UPDATER_ID`),
  KEY `CREW_TO_UVI` (`VESSEL_UID`) USING BTREE,
  CONSTRAINT `CREW_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `CREW_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `CREW_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_external_markings`
--

DROP TABLE IF EXISTS `vessels_to_external_markings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_external_markings` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `EXTERNAL_MARKING` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT 'The vessel external marking',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`EXTERNAL_MARKING`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `EXT_MARK_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `EXT_MARK_TO_UPDATER` (`UPDATER_ID`),
  KEY `EXTERNAL_MARKINGS_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `EXT_MARK_INDEX` (`EXTERNAL_MARKING`) USING BTREE,
  CONSTRAINT `EXT_MARK_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `EXT_MARK_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `EXT_MARK_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_fishing_license`
--

DROP TABLE IF EXISTS `vessels_to_fishing_license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_fishing_license` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned NOT NULL,
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The fishing licence issuing country flag. Should be included in the primary key if a vessel can have different fishing licenses valid at the same time under different flags (unlikely)',
  `LICENSE_ID` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT 'The fishing license ID',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`REFERENCE_DATE`,`COUNTRY_ID`,`LICENSE_ID`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `FISHING_LICENSE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `FISHING_LICENSE_TO_UPDATER` (`UPDATER_ID`),
  KEY `FISHING_LICENSE_INDEX` (`LICENSE_ID`),
  KEY `FISHING_LICENSE_TO_COUNTRY` (`COUNTRY_ID`),
  KEY `FISHING_LICENSE_TO_UVI` (`VESSEL_UID`) USING BTREE,
  CONSTRAINT `FISHING_LICENSE_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FISHING_LICENSE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FISHING_LICENSE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FISHING_LICENSE_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_fishing_masters`
--

DROP TABLE IF EXISTS `vessels_to_fishing_masters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_fishing_masters` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `ENTITY_ID` int(10) unsigned NOT NULL COMMENT 'The fishing master ID',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`ENTITY_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `FISHING_MASTER_TO_VESSEL` (`VESSEL_ID`),
  KEY `FISHING_MASTER_TO_UPDATER` (`UPDATER_ID`),
  KEY `FISHING_MASTER_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `FISHING_MASTER_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `FISHING_MASTER_TO_ENTITY` (`ENTITY_ID`),
  CONSTRAINT `FISHING_MASTER_TO_ENTITY` FOREIGN KEY (`ENTITY_ID`) REFERENCES `entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FISHING_MASTER_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FISHING_MASTER_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FISHING_MASTER_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_flags`
--

DROP TABLE IF EXISTS `vessels_to_flags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_flags` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel unique ID',
  `VESSEL_UID` int(11) unsigned NOT NULL,
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The flag country ID',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`COUNTRY_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `FLAG_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `FLAG_TO_UPDATER` (`UPDATER_ID`),
  KEY `FLAG_TO_VESSEL` (`VESSEL_ID`),
  KEY `FLAG_TO_COUNTRY` (`COUNTRY_ID`),
  KEY `FLAG_INDEX` (`COUNTRY_ID`),
  KEY `FLAGS_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `FLAG_REFERENCE_DATE_INDEX` (`REFERENCE_DATE`) USING BTREE,
  CONSTRAINT `FLAG_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FLAG_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FLAG_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FLAG_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_gears`
--

DROP TABLE IF EXISTS `vessels_to_gears`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_gears` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `TYPE_ID` int(10) unsigned NOT NULL COMMENT 'The gear ID',
  `PRIMARY_GEAR` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Marks current data as referring to the primary gear type (as seen in the EU DB)',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`PRIMARY_GEAR`,`TYPE_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `GEAR_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `GEAR_TO_UPDATER` (`UPDATER_ID`),
  KEY `GEAR_TO_VESSEL` (`VESSEL_ID`),
  KEY `GEARS_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `PRIMARY_GEAR_INDEX` (`PRIMARY_GEAR`),
  KEY `GEAR_TO_TYPE` (`TYPE_ID`) USING BTREE,
  CONSTRAINT `GEAR_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `GEAR_TO_TYPE` FOREIGN KEY (`TYPE_ID`) REFERENCES `s_gear_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `GEAR_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `GEAR_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_hull_material`
--

DROP TABLE IF EXISTS `vessels_to_hull_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_hull_material` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `TYPE_ID` char(2) COLLATE utf8_bin NOT NULL COMMENT 'The vessel hull material ID',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `VESSEL_HULL_MATERIAL_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `VESSEL_HULL_MATERIAL_TO_UPDATER` (`UPDATER_ID`),
  KEY `VESSEL_HULL_MATERIAL_TO_VESSEL` (`VESSEL_ID`),
  KEY `VESSEL_HULL_MATERIAL_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `VESSEL_HULL_MATERIAL_TO_TYPE` (`TYPE_ID`) USING BTREE,
  KEY `VESSEL_HULL_MATERIAL_AND_VESSEL_INDEX` (`VESSEL_ID`,`TYPE_ID`) USING BTREE,
  CONSTRAINT `VESSEL_HULL_MATERIAL_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `VESSEL_HULL_MATERIAL_TO_TYPE` FOREIGN KEY (`TYPE_ID`) REFERENCES `s_hull_material` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `VESSEL_HULL_MATERIAL_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `VESSEL_HULL_MATERIAL_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_identifiers`
--

DROP TABLE IF EXISTS `vessels_to_identifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_identifiers` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `TYPE_ID` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'The vessel identifier type ID',
  `IDENTIFIER` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT 'The vessel identifier',
  `ALTERNATE_IDENTIFIER` varchar(32) CHARACTER SET utf8 NOT NULL,
  `REFERENCE_DATE` datetime NOT NULL,
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`TYPE_ID`,`IDENTIFIER`,`SOURCE_SYSTEM`,`REFERENCE_DATE`,`ALTERNATE_IDENTIFIER`) USING BTREE,
  KEY `IDENTIFIER_TO_SYSTEM` (`SOURCE_SYSTEM`) USING BTREE,
  KEY `IDENTIFIER_TO_UPDATER` (`UPDATER_ID`) USING BTREE,
  KEY `IDENTIFIER_INDEX` (`IDENTIFIER`) USING BTREE,
  KEY `IDENTIFIERS_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `IDENTIFIER_BY_TYPE_INDEX` (`TYPE_ID`,`IDENTIFIER`) USING BTREE,
  CONSTRAINT `IDENTIFIER_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `IDENTIFIER_TO_TYPE` FOREIGN KEY (`TYPE_ID`) REFERENCES `s_identifier_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `IDENTIFIER_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `IDENTIFIER_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_inspections`
--

DROP TABLE IF EXISTS `vessels_to_inspections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_inspections` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `REPORT_ID` varchar(32) COLLATE utf8_bin NOT NULL COMMENT 'The number of the inspection or sighting report',
  `REPORT_TYPE_ID` int(10) unsigned NOT NULL COMMENT 'The type of action/activity that generated the relevant report',
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The country carrying out the control activity and providing the information through the report',
  `ISSUING_AUTHORITY` varchar(64) COLLATE utf8_bin NOT NULL COMMENT 'The national agency or body carrying out the control activity and generating the report',
  `AUTHORITY_ROLE_ID` int(10) unsigned NOT NULL,
  `DATE` datetime NOT NULL COMMENT 'The effective date in which the report was generated/approved',
  `LOCATION` varchar(256) CHARACTER SET utf8 NOT NULL COMMENT 'The place in which the control action was carried out, depending on the type of action undertaken, as relevant (Statistical Area, Latitude/Longitude, or Port)',
  `INFRINGEMENT_TYPE_ID` int(10) unsigned NOT NULL COMMENT 'The type of action carried out in contravention of a national, regional or international fisheries law, regulation or agreement, which resulted in a formal administrative or criminal procedure',
  `DETAILS` varchar(2048) COLLATE utf8_bin DEFAULT NULL COMMENT 'Any further explanatory details that may support risk assessment, decision making, etc., including comments from the master',
  `CONTACT_DETAILS` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT 'The contact details of the issuing authority, flag State or vessel for further information',
  `OUTCOME_TYPE_ID` int(10) unsigned NOT NULL COMMENT 'The action taken by relevant control authorities in response to an infringement, or outcome of the formal procedure',
  `OUTCOME_DETAILS` varchar(2048) COLLATE utf8_bin NOT NULL COMMENT 'Any further explanatory details related to the outcome',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The authorization source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`OUTCOME_TYPE_ID`,`SOURCE_SYSTEM`,`DATE`,`INFRINGEMENT_TYPE_ID`,`REPORT_ID`,`REPORT_TYPE_ID`,`COUNTRY_ID`,`AUTHORITY_ROLE_ID`,`ISSUING_AUTHORITY`,`LOCATION`,`REFERENCE_DATE`) USING BTREE,
  KEY `INSPECTION_TO_TYPE` (`INFRINGEMENT_TYPE_ID`),
  KEY `INSPECTION_TO_OUTCOME` (`OUTCOME_TYPE_ID`),
  KEY `INSPECTION_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `INSPECTION_TO_UPDATER` (`UPDATER_ID`),
  KEY `INSPECTION_TO_VESSEL` (`VESSEL_ID`),
  KEY `INSPECTION_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `INSPECTION_TO_COUNTRY_idx` (`COUNTRY_ID`),
  KEY `INSPECTION_TO_AUTHORITY_ROLE_idx` (`AUTHORITY_ROLE_ID`),
  KEY `INSPECTION_TO_REPORT_TYPE_idx` (`REPORT_TYPE_ID`),
  CONSTRAINT `INSPECTION_TO_AUTHORITY_ROLE` FOREIGN KEY (`AUTHORITY_ROLE_ID`) REFERENCES `s_authority_role` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `INSPECTION_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `INSPECTION_TO_INFRINGEMENT_TYPE` FOREIGN KEY (`INFRINGEMENT_TYPE_ID`) REFERENCES `s_inspection_infringement_type` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `INSPECTION_TO_OUTCOME_TYPE` FOREIGN KEY (`OUTCOME_TYPE_ID`) REFERENCES `s_inspection_outcome_type` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `INSPECTION_TO_REPORT_TYPE` FOREIGN KEY (`REPORT_TYPE_ID`) REFERENCES `s_inspection_report_type` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `INSPECTION_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `INSPECTION_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `INSPECTION_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 2117632 kB; (`ISSUING_COUNTRY_ID` `ISSUING_PORT';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_iuu_list`
--

DROP TABLE IF EXISTS `vessels_to_iuu_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_iuu_list` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `RFMO_IUU_LIST_ID` int(10) unsigned NOT NULL COMMENT 'The Regional Fisheries Management Organization IUU or black list in which the vessel is included',
  `LISTING_DATE` datetime NOT NULL COMMENT 'The effective date in which the vessel was officially included in the IUU List of that RFMO',
  `DELISTING_DATE` datetime DEFAULT NULL COMMENT 'The effective date in which the vessel was removed from the RFMO IUU List (blank if still on the list)',
  `URL` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT 'The link to an online location where details of the listing and delisting are available',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The authorization source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`RFMO_IUU_LIST_ID`,`VESSEL_ID`,`LISTING_DATE`,`REFERENCE_DATE`,`SOURCE_SYSTEM`),
  KEY `IUU_LIST_TO_VESSEL_idx` (`VESSEL_ID`),
  KEY `IUU_LIST_TO_SOURCE_idx` (`SOURCE_SYSTEM`),
  KEY `IUU_LIST_TO_UPDATER_idx` (`UPDATER_ID`),
  CONSTRAINT `IUU_LIST_TO_RFMO` FOREIGN KEY (`RFMO_IUU_LIST_ID`) REFERENCES `s_rfmo_iuu_lists` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `IUU_LIST_TO_SOURCE` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `IUU_LIST_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `IUU_LIST_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 2117632 kB; (`ISSUING_COUNTRY_ID` `ISSUING_PORT';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_lengths`
--

DROP TABLE IF EXISTS `vessels_to_lengths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_lengths` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `TYPE_ID` varchar(3) COLLATE utf8_bin NOT NULL DEFAULT 'LOA' COMMENT 'The length type ID',
  `VALUE` float NOT NULL COMMENT 'The length value',
  `UNIT` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`TYPE_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `LENGTH_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `LENGTH_TO_UPDATER` (`UPDATER_ID`),
  KEY `LENGTH_TO_VESSEL` (`VESSEL_ID`),
  KEY `LENGTH_INDEX` (`VALUE`),
  KEY `LENGTHS_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `LENGTH_BY_TYPE_INDEX` (`TYPE_ID`,`VALUE`) USING BTREE,
  CONSTRAINT `LENGTH_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `LENGTH_TO_TYPE` FOREIGN KEY (`TYPE_ID`) REFERENCES `s_length_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `LENGTH_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `LENGTH_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_masters`
--

DROP TABLE IF EXISTS `vessels_to_masters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_masters` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `ENTITY_ID` int(10) unsigned NOT NULL COMMENT 'The master ID',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`ENTITY_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `MASTER_TO_VESSEL` (`VESSEL_ID`),
  KEY `MASTER_TO_UPDATER` (`UPDATER_ID`),
  KEY `MASTER_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `MASTER_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `MASTER_TO_ENTITY` (`ENTITY_ID`),
  CONSTRAINT `MASTER_TO_ENTITY` FOREIGN KEY (`ENTITY_ID`) REFERENCES `entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `MASTER_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `MASTER_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `MASTER_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_mmsi`
--

DROP TABLE IF EXISTS `vessels_to_mmsi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_mmsi` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `MMSI` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT 'The vessel MMSI',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `MMSI_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `MMSI_TO_UPDATER` (`UPDATER_ID`),
  KEY `MMSI_INDEX` (`MMSI`),
  KEY `MMSI_TO_UVI` (`VESSEL_UID`) USING BTREE,
  CONSTRAINT `MMSI_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `MMSI_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `MMSI_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_name`
--

DROP TABLE IF EXISTS `vessels_to_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_name` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID ',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `NAME` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT 'The vessel name',
  `SIMPLIFIED_NAME` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `SIMPLIFIED_NAME_SOUNDEX` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The source system',
  `VALID_FROM` date DEFAULT NULL,
  `VALID_TO` date DEFAULT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`,`NAME`) USING BTREE,
  KEY `NAME_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `NAME_TO_UPDATER` (`UPDATER_ID`),
  KEY `NAME_TO_VESSEL` (`VESSEL_ID`),
  KEY `NAME_INDEX` (`NAME`),
  KEY `SIMPLIFIED_NAME_INDEX` (`SIMPLIFIED_NAME`),
  KEY `SIMPLIFIED_NAME_SOUNDEX_INDEX` (`SIMPLIFIED_NAME_SOUNDEX`),
  KEY `NAME_REFERENCE_DATE_INDEX` (`REFERENCE_DATE`),
  KEY `NAME_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `VALID_FROM_INDEX` (`VALID_FROM`),
  KEY `VALID_TO_INDEX` (`VALID_TO`),
  KEY `VALIDITY_PERIOD_INDEX` (`VALID_FROM`,`VALID_TO`),
  CONSTRAINT `NAME_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `NAME_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `NAME_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_non_compliance`
--

DROP TABLE IF EXISTS `vessels_to_non_compliance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_non_compliance` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `REPORT_ID` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `SOURCE_TYPE_ID` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'The non compliance source type id',
  `DATE` datetime NOT NULL COMMENT 'The non compliance reference date',
  `LOCATION` varchar(512) COLLATE utf8_bin NOT NULL,
  `ISSUING_AUTHORITY_ID` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The non compliance issuing authority id',
  `TYPE_ID` varchar(10) COLLATE utf8_bin NOT NULL COMMENT 'The non compliance infringement/ apparent infringement type id',
  `DETAILS` varchar(2048) COLLATE utf8_bin DEFAULT NULL,
  `OUTCOME_TYPE_ID` varchar(10) COLLATE utf8_bin NOT NULL,
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The authorization source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`ISSUING_AUTHORITY_ID`,`OUTCOME_TYPE_ID`,`SOURCE_SYSTEM`,`DATE`,`TYPE_ID`) USING BTREE,
  KEY `NON_COMPLIANCE_TO_AUTHORITY` (`ISSUING_AUTHORITY_ID`),
  KEY `NON_COMPLIANCE_TO_TYPE` (`TYPE_ID`),
  KEY `NON_COMPLIANCE_TO_OUTCOME` (`OUTCOME_TYPE_ID`),
  KEY `NON_COMPLIANCE_TO_SOURCE_TYPE` (`SOURCE_TYPE_ID`),
  KEY `NON_COMPLIANCE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `NON_COMPLIANCE_TO_UPDATER` (`UPDATER_ID`),
  KEY `NON_COMPLIANCE_TO_VESSEL` (`VESSEL_ID`),
  KEY `NON_COMPLIANCE_TO_UVI` (`VESSEL_UID`) USING BTREE,
  CONSTRAINT `NON_COMPLIANCE_TO_AUTHORITY` FOREIGN KEY (`ISSUING_AUTHORITY_ID`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `NON_COMPLIANCE_TO_OUTCOME` FOREIGN KEY (`OUTCOME_TYPE_ID`) REFERENCES `s_non_compliance_outcome_type` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `NON_COMPLIANCE_TO_SOURCE_TYPE` FOREIGN KEY (`SOURCE_TYPE_ID`) REFERENCES `s_non_compliance_source_type` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `NON_COMPLIANCE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `NON_COMPLIANCE_TO_TYPE` FOREIGN KEY (`TYPE_ID`) REFERENCES `s_non_compliance_infringement_type` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `NON_COMPLIANCE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `NON_COMPLIANCE_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 2117632 kB; (`ISSUING_COUNTRY_ID` `ISSUING_PORT';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_operators`
--

DROP TABLE IF EXISTS `vessels_to_operators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_operators` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `ENTITY_ID` int(10) unsigned NOT NULL COMMENT 'The operator ID',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`ENTITY_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `OPERATOR_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `OPERATOR_TO_UPDATER` (`UPDATER_ID`),
  KEY `OPERATOR_TO_VESSEL` (`VESSEL_ID`),
  KEY `OPERATORS_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `OPERATOR_TO_ENTITY` (`ENTITY_ID`),
  CONSTRAINT `OPERATOR_TO_ENTITY` FOREIGN KEY (`ENTITY_ID`) REFERENCES `entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `OPERATOR_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `OPERATOR_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `OPERATOR_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_owners`
--

DROP TABLE IF EXISTS `vessels_to_owners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_owners` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `ENTITY_ID` int(10) unsigned NOT NULL COMMENT 'The owner ID',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  `VALID_FROM` date DEFAULT NULL,
  `VALID_TO` date DEFAULT NULL,
  PRIMARY KEY (`VESSEL_ID`,`ENTITY_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `OWNER_TO_VESSEL` (`VESSEL_ID`),
  KEY `OWNER_TO_UPDATER` (`UPDATER_ID`),
  KEY `OWNER_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `OWNERS_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `OWNER_TO_ENTITY` (`ENTITY_ID`),
  KEY `VALID_FROM_INDEX` (`VALID_FROM`),
  KEY `VALID_TO_INDEX` (`VALID_TO`),
  KEY `VALIDITY_PERIOD_INDEX` (`VALID_FROM`,`VALID_TO`),
  CONSTRAINT `OWNER_TO_ENTITY` FOREIGN KEY (`ENTITY_ID`) REFERENCES `entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `OWNER_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `OWNER_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `OWNER_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_port_entry_denials`
--

DROP TABLE IF EXISTS `vessels_to_port_entry_denials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_port_entry_denials` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `NOTIFICATION_NUMBER` varchar(32) COLLATE utf8_bin NOT NULL COMMENT 'The number or identifier of the notification of the port entry denial',
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The country denying port entry and issuing the notification',
  `PORT_ID` int(10) unsigned NOT NULL COMMENT 'The port to which the vessel requested entry and was denied',
  `DATE` datetime NOT NULL COMMENT 'The effective date in which the notification of port entry denial was issued',
  `PORT_ENTRY_DENIAL_REASON_ID` int(10) unsigned NOT NULL COMMENT 'The justification for denying port entry',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL COMMENT 'The authorization source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`SOURCE_SYSTEM`,`DATE`,`COUNTRY_ID`,`PORT_ID`,`NOTIFICATION_NUMBER`,`REFERENCE_DATE`,`PORT_ENTRY_DENIAL_REASON_ID`) USING BTREE,
  KEY `PORT_ENTRY_DENIAL_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `PORT_ENTRY_DENIAL_TO_UPDATER` (`UPDATER_ID`),
  KEY `PORT_ENTRY_DENIAL_TO_VESSEL` (`VESSEL_ID`),
  KEY `PORT_ENTRY_DENIAL_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `PORT_ENTRY_DENIAL_TO_COUNTRY_idx` (`COUNTRY_ID`),
  KEY `PORT_ENTRY_DENIAL_TO_PORT_idx` (`PORT_ID`),
  KEY `PORT_ENTRY_DENIAL_TO_PORT_AND_COUNTRY_idx` (`COUNTRY_ID`,`PORT_ID`),
  KEY `PORT_ENTRY_DENIAL_TO_REASON_idx` (`PORT_ENTRY_DENIAL_REASON_ID`),
  CONSTRAINT `PORT_ENTRY_DENIAL_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PORT_ENTRY_DENIAL_TO_PORT` FOREIGN KEY (`PORT_ID`) REFERENCES `s_ports` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PORT_ENTRY_DENIAL_TO_REASON` FOREIGN KEY (`PORT_ENTRY_DENIAL_REASON_ID`) REFERENCES `s_port_entry_denial_reason` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PORT_ENTRY_DENIAL_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `PORT_ENTRY_DENIAL_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `PORT_ENTRY_DENIAL_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 2117632 kB; (`ISSUING_COUNTRY_ID` `ISSUING_PORT';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_power`
--

DROP TABLE IF EXISTS `vessels_to_power`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_power` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `TYPE_ID` varchar(3) COLLATE utf8_bin NOT NULL DEFAULT 'KW' COMMENT 'The power type ID',
  `MAIN_ENGINE` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Marks current data as referring to the main engine (as seen in the EU DB)',
  `VALUE` float NOT NULL COMMENT 'The power value',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`TYPE_ID`,`MAIN_ENGINE`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `POWER_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `POWER_TO_UPDATER` (`UPDATER_ID`),
  KEY `POWER_TO_VESSEL` (`VESSEL_ID`),
  KEY `POWER_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `MAIN_ENGINE_INDEX` (`MAIN_ENGINE`),
  KEY `POWER_TO_TYPE` (`TYPE_ID`) USING BTREE,
  KEY `POWER_VALUE` (`TYPE_ID`) USING BTREE,
  KEY `POWER_VALUE_AND_TYPE` (`VALUE`,`TYPE_ID`) USING BTREE,
  KEY `POWER_VALUE_STATUS_AND_TYPE` (`VALUE`,`MAIN_ENGINE`,`TYPE_ID`) USING BTREE,
  CONSTRAINT `POWER_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `POWER_TO_TYPE` FOREIGN KEY (`TYPE_ID`) REFERENCES `s_power_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `POWER_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `POWER_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_registrations`
--

DROP TABLE IF EXISTS `vessels_to_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_registrations` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `REGISTRATION_NUMBER` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT 'The vessel registration number for given source system ',
  `COUNTRY_ID` int(10) unsigned NOT NULL COMMENT 'The registration country ID. This is mandatory, albeit redundant, and should always match the registration port country when set',
  `PORT_ID` int(10) unsigned DEFAULT '0' COMMENT 'The registration port ID. Can be NULL if the registration port is not known (as in the ICCAT registry)',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `VALID_FROM` date DEFAULT NULL,
  `VALID_TO` date DEFAULT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`COUNTRY_ID`,`REFERENCE_DATE`,`REGISTRATION_NUMBER`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `REGISTRATION_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `REGISTRATION_TO_UPDATER` (`UPDATER_ID`),
  KEY `REGISTRATION_TO_VESSEL` (`VESSEL_ID`),
  KEY `REGISTRATION_NUMBER_INDEX` (`REGISTRATION_NUMBER`),
  KEY `REGISTRATION_TO_PORT` (`PORT_ID`),
  KEY `REGISTRATION_TO_COUNTRY` (`COUNTRY_ID`),
  KEY `REGISTRATIONS_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `VALID_FROM_INDEX` (`VALID_FROM`),
  KEY `VALID_TO_INDEX` (`VALID_TO`),
  KEY `VALIDITY_PERIOD_INDEX` (`VALID_FROM`,`VALID_TO`),
  CONSTRAINT `REGISTRATION_TO_COUNTRY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `s_countries` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `REGISTRATION_TO_PORT` FOREIGN KEY (`PORT_ID`) REFERENCES `s_ports` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `REGISTRATION_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `REGISTRATION_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `REGISTRATION_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_shipbuilders`
--

DROP TABLE IF EXISTS `vessels_to_shipbuilders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_shipbuilders` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `ENTITY_ID` int(10) unsigned NOT NULL COMMENT 'The shipbuilder ID',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `SHIPBUILDER_TO_VESSEL` (`VESSEL_ID`),
  KEY `SHIPBUILDER_TO_UPDATER` (`UPDATER_ID`),
  KEY `SHIPBUILDER_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `SHIPBUILDER_TO_ENTITY` (`ENTITY_ID`),
  KEY `SHIPBUILDER_TO_UVI` (`VESSEL_UID`) USING BTREE,
  CONSTRAINT `SHIPBUILDER_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `SHIPBUILDER_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `SHIPBUILDER_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_status`
--

DROP TABLE IF EXISTS `vessels_to_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_status` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `TYPE_ID` char(2) COLLATE utf8_bin NOT NULL COMMENT 'The vessel status ID',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `STATUS_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `STATUS_TO_UPDATER` (`UPDATER_ID`),
  KEY `STATUS_TO_VESSEL` (`VESSEL_ID`),
  KEY `STATUS_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `STATUS_TO_TYPE` (`TYPE_ID`) USING BTREE,
  KEY `STATUS_AND_VESSEL_INDEX` (`VESSEL_ID`,`TYPE_ID`) USING BTREE,
  CONSTRAINT `STATUS_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `STATUS_TO_TYPE` FOREIGN KEY (`TYPE_ID`) REFERENCES `s_vessel_status` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `STATUS_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `STATUS_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_tonnage`
--

DROP TABLE IF EXISTS `vessels_to_tonnage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_tonnage` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `TYPE_ID` varchar(3) COLLATE utf8_bin NOT NULL DEFAULT 'GRT' COMMENT 'The tonnage type ID',
  `VALUE` float NOT NULL COMMENT 'The tonnage value',
  `UNIT` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`TYPE_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `TONNAGE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `TONNAGE_TO_UPDATER` (`UPDATER_ID`),
  KEY `TONNAGE_TO_VESSEL` (`VESSEL_ID`),
  KEY `TONNAGE_INDEX` (`VALUE`),
  KEY `TONNAGE_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `TONNAGE_TO_TYPE` (`TYPE_ID`) USING BTREE,
  KEY `TONNAGE_BY_TYPE_INDEX` (`TYPE_ID`,`VALUE`) USING BTREE,
  CONSTRAINT `TONNAGE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `TONNAGE_TO_TYPE` FOREIGN KEY (`TYPE_ID`) REFERENCES `s_tonnage_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `TONNAGE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `TONNAGE_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_types`
--

DROP TABLE IF EXISTS `vessels_to_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_types` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned DEFAULT NULL,
  `TYPE_ID` int(10) unsigned NOT NULL COMMENT 'The vessel type',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date (if available)',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`REFERENCE_DATE`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `TYPE_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `TYPE_TO_TYPE` (`TYPE_ID`),
  KEY `TYPE_TO_UPDATER` (`UPDATER_ID`),
  KEY `TYPE_TO_VESSEL` (`VESSEL_ID`),
  KEY `TYPES_TO_UVI` (`VESSEL_UID`) USING BTREE,
  KEY `TYPE_REFERENCE_DATE` (`REFERENCE_DATE`),
  CONSTRAINT `TYPE_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `TYPE_TO_TYPE` FOREIGN KEY (`TYPE_ID`) REFERENCES `s_vessel_types` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `TYPE_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `TYPE_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vessels_to_vms`
--

DROP TABLE IF EXISTS `vessels_to_vms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vessels_to_vms` (
  `VESSEL_ID` int(10) unsigned NOT NULL COMMENT 'The vessel ID',
  `VESSEL_UID` int(11) unsigned NOT NULL,
  `VMS_INDICATOR` tinyint(1) NOT NULL COMMENT 'The VMS indicator flag',
  `VMS_DETAILS` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT 'The VMS indicator details',
  `REFERENCE_DATE` datetime NOT NULL COMMENT 'The data reference date',
  `SOURCE_SYSTEM` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'OTHER' COMMENT 'The data source system',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The update date',
  `UPDATER_ID` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'ADMIN' COMMENT 'The updater ID',
  `COMMENT` varchar(4096) COLLATE utf8_bin DEFAULT NULL COMMENT 'A free-text entry',
  PRIMARY KEY (`VESSEL_ID`,`REFERENCE_DATE`,`VMS_INDICATOR`,`SOURCE_SYSTEM`) USING BTREE,
  KEY `VMS_TO_SYSTEM` (`SOURCE_SYSTEM`),
  KEY `VMS_TO_UPDATER` (`UPDATER_ID`),
  KEY `VMS_TO_UVI` (`VESSEL_UID`) USING BTREE,
  CONSTRAINT `VMS_TO_SYSTEM` FOREIGN KEY (`SOURCE_SYSTEM`) REFERENCES `s_systems` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `VMS_TO_UPDATER` FOREIGN KEY (`UPDATER_ID`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `VMS_TO_VESSEL` FOREIGN KEY (`VESSEL_ID`) REFERENCES `vessels` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'vrmf_db'
--
/*!50003 DROP FUNCTION IF EXISTS `GET_NEW_UID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` FUNCTION `GET_NEW_UID`(UID_SOURCE VARCHAR(16)) RETURNS int(11)
    DETERMINISTIC
BEGIN
    DECLARE NEW_UID INT;
    SELECT CURRENT+1 FROM S_UID_RANGES WHERE SOURCE_SYSTEM = UID_SOURCE INTO NEW_UID;
    UPDATE S_UID_RANGES SET CURRENT = NEW_UID WHERE SOURCE_SYSTEM = UID_SOURCE;
    
    RETURN NEW_UID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `LEVENSHTEIN` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` FUNCTION `LEVENSHTEIN`(s1 VARCHAR(255), s2 VARCHAR(255)) RETURNS int(11)
    DETERMINISTIC
BEGIN
        DECLARE s1_len, s2_len, i, j, c, c_temp, cost INT;
        DECLARE s1_char CHAR;
        DECLARE cv0, cv1 VARBINARY(256);
        SET s1_len = CHAR_LENGTH(s1), s2_len = CHAR_LENGTH(s2), cv1 = 0x00, j = 1, i = 1, c = 0;
        IF s1 = s2 THEN
          RETURN 0;
        ELSEIF s1_len = 0 THEN
          RETURN s2_len;
        ELSEIF s2_len = 0 THEN
          RETURN s1_len;
        ELSE
          WHILE j <= s2_len DO
            SET cv1 = CONCAT(cv1, UNHEX(HEX(j))), j = j + 1;
          END WHILE;
          WHILE i <= s1_len DO
            SET s1_char = SUBSTRING(s1, i, 1), c = i, cv0 = UNHEX(HEX(i)), j = 1;
            WHILE j <= s2_len DO
                SET c = c + 1;
                IF s1_char = SUBSTRING(s2, j, 1) THEN SET cost = 0; ELSE SET cost = 1; END IF;
                SET c_temp = CONV(HEX(SUBSTRING(cv1, j, 1)), 16, 10) + cost;
                IF c > c_temp THEN SET c = c_temp; END IF;
                SET c_temp = CONV(HEX(SUBSTRING(cv1, j+1, 1)), 16, 10) + 1;
                IF c > c_temp THEN SET c = c_temp; END IF;
                SET cv0 = CONCAT(cv0, UNHEX(HEX(c))), j = j + 1;
            END WHILE;
            SET cv1 = cv0, i = i + 1;
          END WHILE;
        END IF;
        RETURN c;
      END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `LEVENSHTEIN_RATIO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` FUNCTION `LEVENSHTEIN_RATIO`(s1 VARCHAR(255), s2 VARCHAR(255)) RETURNS int(11)
    DETERMINISTIC
BEGIN
        DECLARE s1_len, s2_len, max_len INT;
        IF s1 IS NULL OR s2 IS NULL
        THEN
          RETURN 0;
        ELSE
          SET s1_len = CHAR_LENGTH(s1), s2_len = CHAR_LENGTH(s2);

          IF s1_len > s2_len
          THEN
            SET max_len = s1_len;
          ELSE
            SET max_len = s2_len;
          END IF;

          RETURN ROUND((1 - LEVENSHTEIN(s1, s2) / max_len) * 100);
        END IF;
     END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `RAISE_ERROR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` FUNCTION `RAISE_ERROR`(MESSAGE VARCHAR(255)) RETURNS varchar(255) CHARSET utf8 COLLATE utf8_bin
    DETERMINISTIC
BEGIN
	DECLARE ERROR_MESSAGE VARCHAR(255);
    
    IF MESSAGE IS NULL THEN SET ERROR_MESSAGE = '<NOT SET>'; ELSE SET ERROR_MESSAGE = MESSAGE; END IF;
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = ERROR_MESSAGE;
    
    RETURN MESSAGE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `TO_BE_REMOVED_VESSEL_EXISTS_BY_TRFMO_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` FUNCTION `TO_BE_REMOVED_VESSEL_EXISTS_BY_TRFMO_ID`(VESSEL_SOURCE VARCHAR(16), VESSEL_IDENTIFIER VARCHAR(64)) RETURNS tinyint(1)
    DETERMINISTIC
BEGIN
	DECLARE VESSEL_EXISTS BOOLEAN;
    DECLARE VESSEL_ID INT;
    SELECT VESSEL_ID_BY_TRFMO_ID(VESSEL_SOURCE, VESSEL_IDENTIFIER) INTO VESSEL_ID;
    
    IF VESSEL_ID IS NULL
	THEN RETURN FALSE;
	ELSE RETURN TRUE;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `TO_BE_REMOVED_VESSEL_ID_BY_TRFMO_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` FUNCTION `TO_BE_REMOVED_VESSEL_ID_BY_TRFMO_ID`(VESSEL_SOURCE VARCHAR(16), VESSEL_IDENTIFIER VARCHAR(64)) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE VRMF_VESSEL_ID INT;
    
    SELECT VESSEL_ID
    FROM VESSELS_TO_IDENTIFIERS I
    WHERE 
		I.SOURCE_SYSTEM = VESSEL_SOURCE AND
        I.TYPE_ID = CONCAT(VESSEL_SOURCE, '_ID') AND
        I.IDENTIFIER = VESSEL_IDENTIFIER AND
        I.REFERENCE_DATE = (
			SELECT MAX(REFERENCE_DATE)
            FROM VESSELS_TO_IDENTIFIERS TEMP
            WHERE 
				TEMP.VESSEL_ID = I.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = I.SOURCE_SYSTEM AND
                TEMP.TYPE_ID = I.TYPE_ID
		)
	INTO VRMF_VESSEL_ID;
    
    IF VRMF_VESSEL_ID IS NOT NULL 
    THEN RETURN VRMF_VESSEL_ID;
    ELSE
		CALL RAISE_ERROR(CONCAT('Unable to find any vessel with ', VESSEL_SOURCE, ' ID set to ', VESSEL_IDENTIFIER));
    END IF;
RETURN 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `VESSEL_EXISTS_BY_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` FUNCTION `VESSEL_EXISTS_BY_ID`(VESSEL_ID INT) RETURNS tinyint(1)
    DETERMINISTIC
BEGIN
	DECLARE VESSEL_EXISTS BOOLEAN;
    SELECT CASE WHEN COUNT(ID) > 0 THEN TRUE ELSE FALSE END FROM VESSELS WHERE ID = VESSEL_ID INTO VESSEL_EXISTS;
	RETURN VESSEL_EXISTS;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `VESSEL_EXISTS_BY_RFMO_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` FUNCTION `VESSEL_EXISTS_BY_RFMO_ID`(VESSEL_SOURCE VARCHAR(16), VESSEL_IDENTIFIER VARCHAR(64)) RETURNS tinyint(1)
    DETERMINISTIC
BEGIN
	DECLARE VESSEL_EXISTS BOOLEAN;
    DECLARE VESSEL_ID INT;
    SELECT VESSEL_ID_BY_RFMO_ID(VESSEL_SOURCE, VESSEL_IDENTIFIER) INTO VESSEL_ID;
    
    IF VESSEL_ID IS NULL
	THEN RETURN FALSE;
	ELSE RETURN TRUE;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `VESSEL_ID_BY_RFMO_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` FUNCTION `VESSEL_ID_BY_RFMO_ID`(VESSEL_SOURCE VARCHAR(16), VESSEL_IDENTIFIER VARCHAR(64)) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE VRMF_VESSEL_ID INT;
    
    SELECT VESSEL_ID
    FROM VESSELS_TO_IDENTIFIERS I
    WHERE 
		I.SOURCE_SYSTEM = VESSEL_SOURCE AND
        I.TYPE_ID = CONCAT(VESSEL_SOURCE, '_ID') AND
        I.IDENTIFIER = VESSEL_IDENTIFIER AND
        I.REFERENCE_DATE = (
			SELECT MAX(REFERENCE_DATE)
            FROM VESSELS_TO_IDENTIFIERS TEMP
            WHERE 
				TEMP.VESSEL_ID = I.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = I.SOURCE_SYSTEM AND
                TEMP.TYPE_ID = I.TYPE_ID
		)
	INTO VRMF_VESSEL_ID;
    
    IF VRMF_VESSEL_ID IS NOT NULL 
    THEN RETURN VRMF_VESSEL_ID;
    ELSE
		CALL RAISE_ERROR(CONCAT('Unable to find any vessel with ', VESSEL_SOURCE, ' ID set to ', VESSEL_IDENTIFIER));
    END IF;
RETURN 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ARCHIVE_DATA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `ARCHIVE_DATA`()
BEGIN



  TRUNCATE TABLE EU_VESSEL_REGISTRY;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CALCULATE_REFERENCE_DATES` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `CALCULATE_REFERENCE_DATES`()
BEGIN
  DROP TABLE IF EXISTS T_REFERENCE_DATES;
  CREATE TABLE T_REFERENCE_DATES (
    VESSEL_ID INT(10) UNSIGNED NOT NULL,
    FIRST_REFERENCE_DATE DATETIME NOT NULL,
    LAST_REFERENCE_DATE DATETIME NOT NULL,
    FIRST_UPDATE_DATE DATETIME NOT NULL,
    LAST_UPDATE_DATE DATETIME NOT NULL
  );
  INSERT INTO T_REFERENCE_DATES (
    SELECT
      VESSEL_ID,
      MIN(U.REFERENCE_DATE) AS FIRST_REFERENCE_DATE,
      MAX(U.REFERENCE_DATE) AS LAST_REFERENCE_DATE,
      MIN(U.UPDATE_DATE) AS FIRST_UPDATE_DATE,
      MAX(U.UPDATE_DATE) AS LAST_UPDATE_DATE
    FROM (
      SELECT ID AS VESSEL_ID, UPDATE_DATE, LAST_REFERENCE_DATE AS REFERENCE_DATE FROM VESSELS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_BENEFICIAL_OWNERS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_BUILDING_YEAR UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_CALLSIGNS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_CREW UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_EXTERNAL_MARKINGS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_FISHING_LICENSE UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_FISHING_MASTERS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_FLAGS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_GEARS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_HULL_MATERIAL UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_IDENTIFIERS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_INSPECTIONS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_IUU_LIST UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_LENGTHS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_MASTERS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_MMSI UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_NAME UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_NON_COMPLIANCE UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_OPERATORS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_OWNERS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_PORT_ENTRY_DENIALS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_POWER UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_REGISTRATIONS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_SHIPBUILDERS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_STATUS UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_TONNAGE UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_TYPES UNION
      SELECT VESSEL_ID, UPDATE_DATE, REFERENCE_DATE FROM VESSELS_TO_VMS
    ) U
    GROUP BY U.VESSEL_ID
  );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CHECK_ID_UID_CONSISTENCY` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `CHECK_ID_UID_CONSISTENCY`()
BEGIN
  SELECT 'WRONG VESSEL MAPPINGS' AS ATTRIBUTE, B.ID, A.UID AS UID_OK, B.UID AS UID_KO, 1 AS OCCURRENCIES FROM VESSELS A INNER JOIN VESSELS B ON A.ID = B.MAPS_TO WHERE A.UID <> B.UID
  UNION
  SELECT 'DANGLING VESSEL MAPPINGS' AS ATTRIBUTE, A.ID, B.UID AS UID_OK, A.UID AS UID_KO, 1 AS OCCURRENCIES FROM VESSELS A LEFT JOIN VESSELS B ON A.MAPS_TO = B.ID WHERE A.MAPS_TO IS NOT NULL AND B.ID IS NULL
  UNION
  
  SELECT 'VESSEL MAPPINGS' AS ATTRIBUTE, B.ID, A.UID AS UID_OK, B.UID AS UID_KO, 1 AS OCCURRENCIES FROM VESSELS A INNER JOIN VESSELS B ON A.ID = B.MAPS_TO WHERE A.UID <> B.UID
  UNION
  SELECT 'AUTHORIZATIONS' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN AUTHORIZATIONS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'BENEFICIAL_OWNER' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_BENEFICIAL_OWNERS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'BUILDING_YEAR' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_BUILDING_YEAR A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'CALLSIGN' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_CALLSIGNS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'CREW' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_CREW A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'EXTERNAL_MARKING' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_EXTERNAL_MARKINGS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'FISHING_LICENSE' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_FISHING_LICENSE A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'FISHING_MASTER' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_FISHING_MASTERS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'FLAG' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_FLAGS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'IDENTIFIER' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_IDENTIFIERS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'INSPECTION' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_INSPECTIONS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'IUU_LIST' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_IUU_LIST A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'LENGTH' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_LENGTHS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'MASTER' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_MASTERS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'MMSI' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_MMSI A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'NAME' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_NAME A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'NON_COMPLIANCE' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_NON_COMPLIANCE A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'OPERATOR' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_OPERATORS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'OWNER' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_OWNERS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'PORT_ENTRY_DENIAL' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_PORT_ENTRY_DENIALS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'POWER' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_POWER A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'REGISTRATION' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_REGISTRATIONS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'SHIPBUILDER' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_SHIPBUILDERS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'STATUS' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_STATUS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'TONNAGE' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_TONNAGE A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'TYPE' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_TYPES A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID
  UNION
  SELECT 'VMS' AS ATTRIBUTE, V.ID, V.UID AS UID_OK, A.VESSEL_UID AS UID_KO, COUNT(*) AS OCCURRENCIES FROM VESSELS V LEFT JOIN VESSELS_TO_VMS A ON V.ID = A.VESSEL_ID WHERE V.UID <> A.VESSEL_UID GROUP BY V.ID, A.VESSEL_UID;

 END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DEFAULT_IDENTIFIER_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `DEFAULT_IDENTIFIER_SEARCH`(IN identifierType VARCHAR(16), IN identifierValue VARCHAR(64), IN sourceSystems VARCHAR(512), IN groupByUID BOOLEAN)
BEGIN
	SELECT
		IDENTIFIER AS IDENTIFIER,
		FILTERED.TYPE_ID AS IDENTIFIER_TYPE,
		COUNT(DISTINCT CONCAT(FILTERED.TYPE_ID, FILTERED.IDENTIFIER, FILTERED.SOURCE_SYSTEM, FILTERED.VESSEL_ID)) AS TOTAL_OCCURRENCIES,
		COUNT(DISTINCT FILTERED.VESSEL_ID) AS OCCURRENCIES,
		COUNT(DISTINCT FILTERED.VESSEL_UID) AS GROUPED_OCCURRENCIES
	FROM
  ( SELECT
      TYPE_ID, IDENTIFIER, VESSEL_ID, VESSEL_UID, SOURCE_SYSTEM
    FROM
      VESSELS_TO_IDENTIFIERS
    WHERE
      CASE WHEN identifierType IS NULL THEN
		    true
    	ELSE
		    VESSELS_TO_IDENTIFIERS.TYPE_ID = identifierType
      END AND
	  	CASE WHEN sourceSystems IS NULL THEN
		    true
  		ELSE
	  	  LOCATE(CONCAT('@', VESSELS_TO_IDENTIFIERS.SOURCE_SYSTEM, '@'), sourceSystems) >= 1
		  END AND
      LOCATE(identifierValue, VESSELS_TO_IDENTIFIERS.IDENTIFIER) >= 1
  ) FILTERED
  GROUP BY 2, 1
	ORDER BY 3 DESC, 4 DESC, 5 DESC, 1 ASC, 2 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DEFAULT_TUVI_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `DEFAULT_TUVI_SEARCH`(IN identifierValue VARCHAR(64), IN sourceSystems VARCHAR(512))
BEGIN
	SELECT
    UID AS IDENTIFIER,
    'TUVI' AS IDENTIFIER_TYPE,
		COUNT(DISTINCT VESSELS.ID) AS TOTAL_OCCURRENCIES,
		COUNT(DISTINCT VESSELS.ID) AS OCCURRENCIES,
		COUNT(DISTINCT VESSELS.UID) AS GROUPED_OCCURRENCIES
	FROM
		VESSELS
  INNER JOIN
    AUTHORIZATIONS
  ON
    VESSELS.ID = AUTHORIZATIONS.VESSEL_ID AND
    AUTHORIZATIONS.TYPE_ID = 'CLAV_DEF'
	WHERE
    CASE WHEN sourceSystems IS NULL THEN
      true
    ELSE
  	  LOCATE(CONCAT('@', VESSELS.SOURCE_SYSTEM, '@'), sourceSystems) >= 1
	  END AND
	  LOCATE(identifierValue, UID) >= 1
	GROUP BY 1
  ORDER BY 2 DESC, 3 DESC, 4 DESC, 1 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `DEFAULT_UID_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `DEFAULT_UID_SEARCH`(IN identifierValue VARCHAR(64), IN sourceSystems VARCHAR(512))
BEGIN
	SELECT
    UID AS IDENTIFIER,
    'VRMF_UID' AS IDENTIFIER_TYPE,
		COUNT(*) AS TOTAL_OCCURRENCIES,
		COUNT(DISTINCT VESSELS.ID) AS OCCURRENCIES,
		COUNT(DISTINCT VESSELS.UID) AS GROUPED_OCCURRENCIES
	FROM
		VESSELS
	WHERE
    CASE WHEN sourceSystems IS NULL THEN
		  true
		ELSE
		  LOCATE(CONCAT('@', VESSELS.SOURCE_SYSTEM, '@'), sourceSystems) >= 1
    END AND
	  LOCATE(identifierValue, UID) >= 1
	GROUP BY 1
  ORDER BY 2 DESC, 3 DESC, 4 DESC, 1 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `EXT_MARK_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `EXT_MARK_SEARCH`(IN externalMarking VARCHAR(64), IN sourceSystems VARCHAR(512), IN atDate DATETIME, IN groupByUID BOOLEAN)
BEGIN
  SELECT
    EXTERNAL_MARKING,
    COUNT(DISTINCT CONCAT(EXTERNAL_MARKING, REFERENCE_DATE, VESSEL_ID)) AS TOTAL_OCCURRENCIES,
    COUNT(DISTINCT VESSEL_ID) AS OCCURRENCIES,
    COUNT(DISTINCT VESSEL_UID) AS GROUPED_OCCURRENCIES
  FROM (
  SELECT * FROM
    VESSELS_TO_EXTERNAL_MARKINGS
  WHERE
	UCASE(EXTERNAL_MARKING) LIKE CONCAT('%', UCASE(externalMarking), '%') AND
    LOCATE(CONCAT('@', SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
    CASE WHEN groupByUID = true THEN
      CASE WHEN atDate IS NOT NULL THEN
        REFERENCE_DATE = (
          SELECT
            MAX(REFERENCE_DATE)
          FROM
            VESSELS_TO_EXTERNAL_MARKINGS TEMP
          WHERE
            TEMP.VESSEL_UID = VESSELS_TO_EXTERNAL_MARKINGS.VESSEL_UID AND
            LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
            TEMP.REFERENCE_DATE <= atDate
        ) 
      ELSE
        TRUE
      END
    ELSE
      CASE WHEN atDate IS NOT NULL THEN
        REFERENCE_DATE = (
          SELECT
            MAX(REFERENCE_DATE)
          FROM
            VESSELS_TO_EXTERNAL_MARKINGS TEMP
          WHERE
            TEMP.VESSEL_ID = VESSELS_TO_EXTERNAL_MARKINGS.VESSEL_ID AND
            LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
            TEMP.REFERENCE_DATE <= atDate
        )
      ELSE
        TRUE
      END 
    END
  ) FILTERED
  GROUP BY 1
	ORDER BY 2 DESC, 3 DESC, 4 DESC, 1 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FISHING_LICENSE_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `FISHING_LICENSE_SEARCH`(IN fishingLicense VARCHAR(64), IN countryIDs VARCHAR(8192), IN sourceSystems VARCHAR(512), IN atDate DATETIME, IN groupByUID BOOLEAN)
BEGIN
  SELECT
    UCASE(LICENSE_ID) AS LICENSE_ID,
    COUNT(DISTINCT CONCAT(LICENSE_ID, REFERENCE_DATE, VESSEL_ID)) AS TOTAL_OCCURRENCIES,
    COUNT(DISTINCT VESSEL_ID) AS OCCURRENCIES,
    COUNT(DISTINCT VESSEL_UID) AS GROUPED_OCCURRENCIES
  FROM (
  SELECT
    *
  FROM
    VESSELS_TO_FISHING_LICENSE
  WHERE
    CASE WHEN countryIDs IS NULL THEN true
  	ELSE  LOCATE(CONCAT('@', COUNTRY_ID, '@'), countryIDs) >= 1
  	END AND LOCATE(fishingLicense, LICENSE_ID) >= 1 AND
    CASE WHEN sourceSystems IS NULL THEN true
    ELSE LOCATE(CONCAT('@', SOURCE_SYSTEM, '@'), sourceSystems) >= 1
    END AND
    CASE WHEN groupByUID = true THEN
      CASE WHEN atDate IS NOT NULL THEN
        REFERENCE_DATE = (
          SELECT
            MAX(REFERENCE_DATE)
          FROM
            VESSELS_TO_FISHING_LICENSE TEMP
          WHERE
            TEMP.VESSEL_UID = VESSELS_TO_FISHING_LICENSE.VESSEL_UID AND
            LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
            TEMP.REFERENCE_DATE <= atDate
        ) 
      ELSE
        TRUE
      END
    ELSE
      CASE WHEN atDate IS NOT NULL THEN
        REFERENCE_DATE = (
          SELECT
            MAX(REFERENCE_DATE)
          FROM
            VESSELS_TO_FISHING_LICENSE TEMP
          WHERE
            TEMP.VESSEL_ID = VESSELS_TO_FISHING_LICENSE.VESSEL_ID AND
            LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
            TEMP.REFERENCE_DATE <= atDate
        )
      ELSE
        TRUE
      END 
    END
  ) FILTERED GROUP BY 1
	ORDER BY 2 DESC, 3 DESC, 4 DESC, 1 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FLUSH_ALL_CONTENT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `FLUSH_ALL_CONTENT`()
BEGIN
	UPDATE VESSELS
    	SET MAPS_TO = NULL,
        	MAPPING_USER = NULL,
        	MAPPING_WEIGHT = NULL,
        	MAPPING_DATE = NULL,
        	MAPPING_COMMENT = NULL
  	WHERE
	    MAPS_TO IS NOT NULL;

	TRUNCATE TABLE VESSELS_TO_BENEFICIAL_OWNERS;
	TRUNCATE TABLE VESSELS_TO_BUILDING_YEAR;
	TRUNCATE TABLE VESSELS_TO_CALLSIGNS;
	TRUNCATE TABLE VESSELS_TO_CREW;
	TRUNCATE TABLE VESSELS_TO_EXTERNAL_MARKINGS;
	TRUNCATE TABLE VESSELS_TO_FISHING_LICENSE;
	TRUNCATE TABLE VESSELS_TO_FISHING_MASTERS;
	TRUNCATE TABLE VESSELS_TO_FLAGS;
	TRUNCATE TABLE VESSELS_TO_GEARS;
	TRUNCATE TABLE VESSELS_TO_HULL_MATERIAL;
	TRUNCATE TABLE VESSELS_TO_IDENTIFIERS;
	TRUNCATE TABLE VESSELS_TO_INSPECTIONS;
	TRUNCATE TABLE VESSELS_TO_IUU_LIST;
	TRUNCATE TABLE VESSELS_TO_LENGTHS;
	TRUNCATE TABLE VESSELS_TO_MASTERS;
	TRUNCATE TABLE VESSELS_TO_MMSI;
	TRUNCATE TABLE VESSELS_TO_NAME;
	TRUNCATE TABLE VESSELS_TO_NON_COMPLIANCE;
	TRUNCATE TABLE VESSELS_TO_OPERATORS;
	TRUNCATE TABLE VESSELS_TO_OWNERS;
	TRUNCATE TABLE VESSELS_TO_PORT_ENTRY_DENIALS;
	TRUNCATE TABLE VESSELS_TO_POWER;
	TRUNCATE TABLE VESSELS_TO_REGISTRATIONS;
	TRUNCATE TABLE VESSELS_TO_SHIPBUILDERS;
	TRUNCATE TABLE VESSELS_TO_STATUS;
	TRUNCATE TABLE VESSELS_TO_TONNAGE;
	TRUNCATE TABLE VESSELS_TO_TYPES;
	TRUNCATE TABLE VESSELS_TO_VMS;

	TRUNCATE TABLE AUTHORIZATION_DETAILS;
	TRUNCATE TABLE AUTHORIZATIONS_HISTORY;
	TRUNCATE TABLE AUTHORIZATIONS;

	TRUNCATE TABLE VESSELS_HISTORY;
	TRUNCATE TABLE VESSELS;

	DELETE FROM ENTITY WHERE UPDATER_ID LIKE '%_IMPORTER';

	OPTIMIZE TABLE ENTITY;
	OPTIMIZE TABLE VESSELS;
	OPTIMIZE TABLE VESSELS_HISTORY;
	OPTIMIZE TABLE AUTHORIZATIONS;
	OPTIMIZE TABLE AUTHORIZATIONS_DETAILS;
	OPTIMIZE TABLE AUTHORIZATIONS_HISTORY;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FLUSH_CONTENT_BY_SYSTEM` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `FLUSH_CONTENT_BY_SYSTEM`(IN sourceSystem VARCHAR(5) CHARACTER SET utf8)
BEGIN
    DECLARE CURRENT_STATUS INT;
    SET CURRENT_STATUS = @VRMF_TRIGGERS_ENABLED;
    SET @VRMF_TRIGGERS_ENABLED = 0;

	DELETE FROM vessels_to_beneficial_owners WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_building_year WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_callsigns WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_crew WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_external_markings WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_fishing_license WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_fishing_masters WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_flags WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_gears WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_hull_material WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_identifiers WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_inspections WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_iuu_list WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_lengths WHERE SOURCE_SYSTEM = sourceSystem;
    DELETE FROM vessels_to_masters WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_mmsi WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_name WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_non_compliance WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_operators WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_owners WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_port_entry_denials WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_power WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_registrations WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_shipbuilders WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_status WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_tonnage WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_types WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_to_vms WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM authorization_details WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM authorizations WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels_history WHERE SOURCE_SYSTEM = sourceSystem;
	DELETE FROM vessels WHERE SOURCE_SYSTEM = sourceSystem;
    DELETE FROM entity WHERE SOURCE_SYSTEM = sourceSystem;

    OPTIMIZE TABLE entity;
	OPTIMIZE TABLE vessels;

    SET @VRMF_TRIGGERS_ENABLED = CURRENT_STATUS;

	END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FLUSH_CONTENT_BY_SYSTEMS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `FLUSH_CONTENT_BY_SYSTEMS`(IN sourceSystems VARCHAR(512) CHARACTER SET utf8, IN INCLUDE_ENTITIES BOOLEAN)
BEGIN
    DECLARE CURRENT_STATUS INT;
    SET CURRENT_STATUS = @VRMF_TRIGGERS_ENABLED;
    SET @VRMF_TRIGGERS_ENABLED = 0;

	DELETE FROM vessels_to_beneficial_owners WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_building_year WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_callsigns WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_crew WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_external_markings WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_fishing_license WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_fishing_masters WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_flags WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_gears WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_hull_material WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_identifiers WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_inspections WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_iuu_list WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_lengths WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_masters WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_mmsi WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_name WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_non_compliance WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_operators WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_owners WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_power WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_port_entry_denials WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_registrations WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_shipbuilders WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_status WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_tonnage WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_types WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_to_vms WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM authorization_details WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM authorizations_history WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM authorizations WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels_history WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
	DELETE FROM vessels WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;

    IF INCLUDE_ENTITIES = true THEN
      	DELETE FROM entity WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;
		OPTIMIZE TABLE entity;
    END IF;
		OPTIMIZE TABLE vessels;
    SET @VRMF_TRIGGERS_ENABLED = CURRENT_STATUS;
	END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FUZZY_GEAR_TYPE_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `FUZZY_GEAR_TYPE_SEARCH`(IN gearType VARCHAR(512), IN sourceSystems VARCHAR(512))
BEGIN
SET @UTYPE = UCASE(gearType);
SELECT
  0 AS RESULT_WEIGHT,
  GT.ID,
  GT.ORIGINAL_GEAR_TYPE_CODE,
  GT.NAME,
  GT.STANDARD_ABBREVIATION,
  GT.SOURCE_SYSTEM
FROM
  s_gear_types GT
WHERE
    CASE WHEN sourceSystems IS NULL THEN
      true
    ELSE
      LOCATE(CONCAT('@', GT.SOURCE_SYSTEM, '@'), sourceSystems) >=1
    END 
ORDER BY
  GT.NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FUZZY_OPERATOR_NAME_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `FUZZY_OPERATOR_NAME_SEARCH`(IN name VARCHAR(512), IN simplifiedName VARCHAR(512), IN sourceSystems VARCHAR(512), IN atDate DATETIME, IN groupByUID BOOLEAN)
BEGIN
SET @UNAME = UCASE(name);
SET @SNAMESOUNDEX = SOUNDEX(UCASE(simplifiedName));
SET @SNAMESOUNDEX_R = CASE WHEN @SNAMESOUNDEX IS NULL THEN NULL ELSE SUBSTRING(@SNAMESOUNDEX, 1, 4) END;
SET @SNAMESOUNDEX_L = CASE WHEN @SNAMESOUNDEX IS NULL OR LENGTH(@SNAMESOUNDEX) < 5 THEN NULL ELSE SUBSTRING(@SNAMESOUNDEX, 2, 7) END;
SELECT
	ENTITY.ID,
	ENTITY.ENTITY_TYPE_ID,
	LEVENSHTEIN_RATIO(simplifiedName, ENTITY.SIMPLIFIED_NAME) AS RESULT_WEIGHT,
	ENTITY.NAME,
	ENTITY.SIMPLIFIED_NAME,
	ENTITY.SIMPLIFIED_NAME_SOUNDEX,
	ENTITY.BRANCH,
	ENTITY.SALUTATION,
	ENTITY.ROLE,
	ENTITY.COUNTRY_ID,
	ENTITY.ADDRESS,
	ENTITY.CITY,
	ENTITY.ZIP_CODE,
	FILTERED.TOTAL_OCCURRENCIES,
	FILTERED.OCCURRENCIES,
	FILTERED.GROUPED_OCCURRENCIES
FROM
(
	SELECT
		ENTITY.ID AS ENTITY_ID,
        COUNT(DISTINCT CONCAT(ENTITY.NAME, VESSELS_TO_OPERATORS.REFERENCE_DATE, VESSELS_TO_OPERATORS.VESSEL_ID)) AS TOTAL_OCCURRENCIES,
		COUNT(DISTINCT VESSELS_TO_OPERATORS.VESSEL_ID) AS OCCURRENCIES,
		COUNT(DISTINCT VESSELS_TO_OPERATORS.VESSEL_UID) AS GROUPED_OCCURRENCIES
	FROM
		ENTITY
	INNER JOIN
		VESSELS_TO_OPERATORS 
	ON
		ENTITY.ID = VESSELS_TO_OPERATORS.ENTITY_ID
	WHERE (
		( @SNAMESOUNDEX_L IS NOT NULL AND LOCATE(@SNAMESOUNDEX_L, SIMPLIFIED_NAME_SOUNDEX) >= 2 ) OR  
		LOCATE(simplifiedName, SIMPLIFIED_NAME) >= 1 OR
		LOCATE(@SNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX) >= 1 OR
		LOCATE(@SNAMESOUNDEX_R, SIMPLIFIED_NAME_SOUNDEX) >= 1 
	) AND
    LOCATE(CONCAT('@', VESSELS_TO_OPERATORS.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
    CASE WHEN groupByUID = true THEN
    	CASE WHEN atDate IS NOT NULL THEN
			REFERENCE_DATE = (
		        SELECT
		         	MAX(REFERENCE_DATE)
		        FROM
		          	VESSELS_TO_OPERATORS TEMP
		        WHERE
					TEMP.VESSEL_UID = VESSELS_TO_OPERATORS.VESSEL_UID AND
		          	LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
		          	TEMP.REFERENCE_DATE <= atDate
		    ) 
		ELSE
			TRUE
		END
	ELSE
		CASE WHEN atDate IS NOT NULL THEN
		  	REFERENCE_DATE = (
		        SELECT
		        	MAX(REFERENCE_DATE)
		        FROM
		        	VESSELS_TO_OPERATORS TEMP
		        WHERE
				    TEMP.VESSEL_ID = VESSELS_TO_OPERATORS.VESSEL_ID AND
		          	LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
		          	TEMP.REFERENCE_DATE <= atDate
		    )
		ELSE
			TRUE
		END 
	END
	GROUP BY
 		ENTITY.ID
) FILTERED
LEFT JOIN 
	ENTITY
ON
	FILTERED.ENTITY_ID = ENTITY.ID
ORDER BY
  RESULT_WEIGHT DESC, SIMPLIFIED_NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FUZZY_OWNER_NAME_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `FUZZY_OWNER_NAME_SEARCH`(IN name VARCHAR(512), IN simplifiedName VARCHAR(512), IN sourceSystems VARCHAR(512), IN atDate DATETIME, IN groupByUID BOOLEAN)
BEGIN
SET @UNAME = UCASE(name);
SET @SNAMESOUNDEX = SOUNDEX(UCASE(simplifiedName));
SET @SNAMESOUNDEX_R = CASE WHEN @SNAMESOUNDEX IS NULL THEN NULL ELSE SUBSTRING(@SNAMESOUNDEX, 1, 4) END;
SET @SNAMESOUNDEX_L = CASE WHEN @SNAMESOUNDEX IS NULL OR LENGTH(@SNAMESOUNDEX) < 5 THEN NULL ELSE SUBSTRING(@SNAMESOUNDEX, 2, 7) END;
SELECT
	ENTITY.ID,
	ENTITY.ENTITY_TYPE_ID,
	LEVENSHTEIN_RATIO(simplifiedName, ENTITY.SIMPLIFIED_NAME) AS RESULT_WEIGHT,
	ENTITY.NAME,
	ENTITY.SIMPLIFIED_NAME,
	ENTITY.SIMPLIFIED_NAME_SOUNDEX,
	ENTITY.BRANCH,
	ENTITY.SALUTATION,
	ENTITY.ROLE,
	ENTITY.COUNTRY_ID,
	ENTITY.ADDRESS,
	ENTITY.CITY,
	ENTITY.ZIP_CODE,
	FILTERED.TOTAL_OCCURRENCIES,
	FILTERED.OCCURRENCIES,
	FILTERED.GROUPED_OCCURRENCIES
FROM
(
	SELECT
		ENTITY.ID AS ENTITY_ID,
        COUNT(DISTINCT CONCAT(ENTITY.NAME, VESSELS_TO_OWNERS.REFERENCE_DATE, VESSELS_TO_OWNERS.VESSEL_ID)) AS TOTAL_OCCURRENCIES,
		COUNT(DISTINCT VESSELS_TO_OWNERS.VESSEL_ID) AS OCCURRENCIES,
		COUNT(DISTINCT VESSELS_TO_OWNERS.VESSEL_UID) AS GROUPED_OCCURRENCIES
	FROM
		ENTITY
	INNER JOIN
		VESSELS_TO_OWNERS 
	ON
		ENTITY.ID = VESSELS_TO_OWNERS.ENTITY_ID
	WHERE (
		( @SNAMESOUNDEX_L IS NOT NULL AND LOCATE(@SNAMESOUNDEX_L, SIMPLIFIED_NAME_SOUNDEX) >= 2 ) OR  
		LOCATE(simplifiedName, SIMPLIFIED_NAME) >= 1 OR
		LOCATE(@SNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX) >= 1 OR
		LOCATE(@SNAMESOUNDEX_R, SIMPLIFIED_NAME_SOUNDEX) >= 1 
	) AND
    LOCATE(CONCAT('@', VESSELS_TO_OWNERS.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
    CASE WHEN groupByUID = true THEN
    	CASE WHEN atDate IS NOT NULL THEN
			REFERENCE_DATE = (
		        SELECT
		         	MAX(REFERENCE_DATE)
		        FROM
		          	VESSELS_TO_OWNERS TEMP
		        WHERE
					TEMP.VESSEL_UID = VESSELS_TO_OWNERS.VESSEL_UID AND
		          	LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
		          	TEMP.REFERENCE_DATE <= atDate
		    ) 
		ELSE
			TRUE
		END
	ELSE
		CASE WHEN atDate IS NOT NULL THEN
		  	REFERENCE_DATE = (
		        SELECT
		        	MAX(REFERENCE_DATE)
		        FROM
		        	VESSELS_TO_OWNERS TEMP
		        WHERE
				    TEMP.VESSEL_ID = VESSELS_TO_OWNERS.VESSEL_ID AND
		          	LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
		          	TEMP.REFERENCE_DATE <= atDate
		    )
		ELSE
			TRUE
		END 
	END
	GROUP BY
 		ENTITY.ID
) FILTERED
LEFT JOIN 
	ENTITY
ON
	FILTERED.ENTITY_ID = ENTITY.ID
ORDER BY
  RESULT_WEIGHT DESC, SIMPLIFIED_NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FUZZY_PORT_NAME_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `FUZZY_PORT_NAME_SEARCH`(IN sourceSystems VARCHAR(512), IN countryIDs VARCHAR(512), IN portName VARCHAR(512))
BEGIN
SET @UNAME = UCASE(portName);
SET @UNAMESOUNDEX = SOUNDEX(@UNAME);
SELECT
  CASE
    WHEN UCASE(NAME) = @UNAME THEN 100
		WHEN SIMPLIFIED_NAME = @UNAME THEN 90
    WHEN LOCATE(@UNAME, UCASE(NAME)) = 1 THEN GREATEST(70, (LEVENSHTEIN_RATIO(@UNAME, NAME)))
    WHEN LOCATE(@UNAME, SIMPLIFIED_NAME) = 1 THEN GREATEST(60, (LEVENSHTEIN_RATIO(@UNAME, SIMPLIFIED_NAME)))
    WHEN LOCATE(@UNAMESOUNDEX, P.SIMPLIFIED_NAME_SOUNDEX) = 1 THEN 0.5 * LEVENSHTEIN_RATIO(@UNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX)
		WHEN LOCATE(@UNAME, UCASE(NAME)) > 1 THEN (LEVENSHTEIN_RATIO(@UNAME, NAME))
		WHEN LOCATE(@UNAME, SIMPLIFIED_NAME) > 1 THEN (LEVENSHTEIN_RATIO(@UNAME, SIMPLIFIED_NAME))
    WHEN LOCATE(@UNAMESOUNDEX, P.SIMPLIFIED_NAME_SOUNDEX) > 1 THEN 0.2 * (LEVENSHTEIN_RATIO(@UNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX))
  ELSE
    GREATEST(LEVENSHTEIN_RATIO(@UNAME, NAME), LEVENSHTEIN_RATIO(@UNAME, SIMPLIFIED_NAME))
  END AS RESULT_WEIGHT,
  P.ID, P.ORIGINAL_PORT_ID, P.COUNTRY_ID, P.SUBDIVISION_ID, P.MAPS_TO, P.NAME, P.SOURCE_SYSTEM
FROM
  S_PORTS P
WHERE
  CASE WHEN sourceSystems IS NULL THEN
    true
  ELSE
    LOCATE(CONCAT('@', P.SOURCE_SYSTEM, '@'), sourceSystems) >= 1
  END AND
  CASE WHEN countryIDs IS NULL THEN
    true
  ELSE
    LOCATE(CONCAT('@', P.COUNTRY_ID, '@'), countryIDs) >= 1
  END AND (
    LOCATE(@UNAME, UCASE(P.NAME)) >= 1 OR
    LOCATE(@UNAME, P.SIMPLIFIED_NAME) >= 1 OR
    LOCATE(@UNAMESOUNDEX, P.SIMPLIFIED_NAME_SOUNDEX) >= 1
  )
ORDER BY
  RESULT_WEIGHT DESC, P.SIMPLIFIED_NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FUZZY_SPECIES_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `FUZZY_SPECIES_SEARCH`(IN specieName VARCHAR(512), IN specieSimplifiedName VARCHAR(512))
BEGIN
SET @UNAME = UCASE(specieName);
SET @SNAME = UCASE(specieSimplifiedName);
SET @SNAMESOUNDEX = SOUNDEX(UCASE(specieSimplifiedName));
SELECT
   CASE 
    WHEN @UNAME = UCASE(NAME) THEN 100
    WHEN @UNAME = UCASE(SCIENTIFIC_NAME) THEN 100
    WHEN @UNAME = SIMPLIFIED_NAME THEN 100
    ELSE
    GREATEST(0,
            LEVENSHTEIN_RATIO(@UNAME, COALESCE(TAXOCODE, '')), 
            LEVENSHTEIN_RATIO(@UNAME, COALESCE(CODE_3A, '')),
            COALESCE(LEVENSHTEIN_RATIO(COALESCE(@SNAME, ''), COALESCE(UCASE(NAME), '')), 0), 
            COALESCE(LEVENSHTEIN_RATIO(COALESCE(@SNAME, ''), COALESCE(SIMPLIFIED_NAME, '')), 0), 
            COALESCE(LEVENSHTEIN_RATIO(COALESCE(@SNAME, ''), COALESCE(UCASE(SCIENTIFIC_NAME), '')), 0),
            COALESCE(LEVENSHTEIN_RATIO(COALESCE(@SNAME, ''), COALESCE(UCASE(FAMILY), '')), 0),
            COALESCE(LEVENSHTEIN_RATIO(COALESCE(@SNAME, ''), COALESCE(UCASE(SPECIE_ORDER), '')), 0)
    ) END AS RESULT_WEIGHT,
   ID,
   FIGIS_ID,
   SOURCE_SYSTEM,
   TAXOCODE,
   CODE_3A,
   SCIENTIFIC_NAME,   
   NAME,
   FAMILY,
   SPECIE_ORDER
FROM
	S_SPECIES
WHERE
  LOCATE(@UNAME, TAXOCODE) >= 1 OR
  LOCATE(@UNAME, CODE_3A) >=1 OR
  LOCATE(@UNAME, UCASE(NAME)) >=1 OR
  LOCATE(@UNAME, UCASE(SIMPLIFIED_NAME)) >= 1 OR
  LOCATE(@UNAME, UCASE(SCIENTIFIC_NAME)) >= 1 OR
  LOCATE(@UNAME, UCASE(FAMILY)) >= 1 OR
  LOCATE(@UNAME, UCASE(SPECIE_ORDER)) >= 1 OR
  ( @SNAME IS NOT NULL AND LOCATE(@SNAME, UCASE(NAME)) >= 1 ) OR
  ( @SNAME IS NOT NULL AND LOCATE(@SNAME, UCASE(SIMPLIFIED_NAME)) >= 1 ) OR
  ( @SNAME IS NOT NULL AND LOCATE(@SNAME, UCASE(SCIENTIFIC_NAME)) >= 1 ) OR    
  LOCATE(@SNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX) >= 1 OR
  LOCATE(@SNAMESOUNDEX, SCIENTIFIC_NAME_SOUNDEX) >= 1 OR
  ( @SNAME IS NOT NULL AND LOCATE(@SNAME, UCASE(FAMILY)) >= 1 ) OR
  ( @SNAME IS NOT NULL AND LOCATE(@SNAME, UCASE(SPECIE_ORDER)) >= 1 ) OR
  LOCATE(@SNAMESOUNDEX, SOUNDEX(UCASE(FAMILY))) >= 1 OR
  LOCATE(@SNAMESOUNDEX, SOUNDEX(UCASE(SPECIE_ORDER))) >= 1 OR
  
  FALSE
ORDER BY
  RESULT_WEIGHT DESC, SIMPLIFIED_NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FUZZY_VESSEL_NAME_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `FUZZY_VESSEL_NAME_SEARCH`(IN vesselName VARCHAR(512), IN simplifiedVesselName VARCHAR(512), IN sourceSystems VARCHAR(512), IN atDate DATETIME, IN groupByUID BOOLEAN)
BEGIN
SET @UNAME = UCASE(vesselName);
SET @SNAMESOUNDEX = SOUNDEX(UCASE(simplifiedVesselName));
SET @SNAMESOUNDEX_R = CASE WHEN @SNAMESOUNDEX IS NULL THEN NULL ELSE SUBSTRING(@SNAMESOUNDEX, 1, 4) END;
SET @SNAMESOUNDEX_L = CASE WHEN @SNAMESOUNDEX IS NULL OR LENGTH(@SNAMESOUNDEX) < 5 THEN NULL ELSE SUBSTRING(@SNAMESOUNDEX, 2, 7) END;
SELECT
	@SNAMESOUNDEX,
    @SNAMESOUNDEX_R,
    @SNAMESOUNDEX_L,
    LEVENSHTEIN_RATIO(simplifiedVesselName, FILTERED.SIMPLIFIED_NAME) AS RESULT_WEIGHT,
    NAME,
    SIMPLIFIED_NAME,
    SIMPLIFIED_NAME_SOUNDEX,
    COUNT(DISTINCT CONCAT(FILTERED.NAME, FILTERED.REFERENCE_DATE, FILTERED.VESSEL_ID)) AS TOTAL_OCCURRENCIES,
    COUNT(DISTINCT FILTERED.VESSEL_ID) AS OCCURRENCIES,
    COUNT(DISTINCT FILTERED.VESSEL_UID) AS GROUPED_OCCURRENCIES
FROM
(
	SELECT
        VESSELS_TO_NAME.NAME,
        VESSELS_TO_NAME.SIMPLIFIED_NAME,
        VESSELS_TO_NAME.SIMPLIFIED_NAME_SOUNDEX,
        VESSELS_TO_NAME.REFERENCE_DATE AS REFERENCE_DATE,
        VESSELS_TO_NAME.VESSEL_ID AS VESSEL_ID,
        VESSELS_TO_NAME.VESSEL_UID AS VESSEL_UID
	FROM
		vessels_to_name
	WHERE (
        ( @SNAMESOUNDEX_L IS NOT NULL AND LOCATE(@SNAMESOUNDEX_L, SIMPLIFIED_NAME_SOUNDEX) >= 2 ) OR    
        LOCATE(simplifiedVesselName, SIMPLIFIED_NAME) >= 1 OR
        LOCATE(@SNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX) >= 1 OR
        LOCATE(@SNAMESOUNDEX_R, SIMPLIFIED_NAME_SOUNDEX) >= 1 
    ) AND
    LOCATE(CONCAT('@', VESSELS_TO_NAME.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
        CASE WHEN groupByUID = true THEN
            CASE WHEN atDate IS NOT NULL THEN
            REFERENCE_DATE = (
                    SELECT
                        MAX(REFERENCE_DATE)
                    FROM
                            VESSELS_TO_NAME TEMP
                    WHERE
                            TEMP.VESSEL_UID = VESSELS_TO_NAME.VESSEL_UID AND
                            LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
                            TEMP.REFERENCE_DATE <= atDate
            ) 
            ELSE
                TRUE
            END
        ELSE
            CASE WHEN atDate IS NOT NULL THEN
                REFERENCE_DATE = (
                        SELECT
                            MAX(REFERENCE_DATE)
                        FROM
                            VESSELS_TO_NAME TEMP
                        WHERE
                        TEMP.VESSEL_ID = VESSELS_TO_NAME.VESSEL_ID AND
                                LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
                                TEMP.REFERENCE_DATE <= atDate
                )
            ELSE
                TRUE
            END 
        END
) FILTERED
GROUP BY
    NAME
ORDER BY
    RESULT_WEIGHT DESC, SIMPLIFIED_NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FUZZY_VESSEL_REG_PORT_NAME_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `FUZZY_VESSEL_REG_PORT_NAME_SEARCH`(IN sourceSystems VARCHAR(512), IN countryIDs VARCHAR(512), IN portName VARCHAR(512), IN atDate DATETIME, IN groupByUID BOOLEAN)
BEGIN
SET @UNAME = UCASE(portName);
SET @SNAME = @UNAME;
SET @SNAMESOUNDEX = SOUNDEX(@UNAME);
SET @SNAMESOUNDEX_R = CASE WHEN @SNAMESOUNDEX IS NULL THEN NULL ELSE SUBSTRING(@SNAMESOUNDEX, 1, 4) END;
SET @SNAMESOUNDEX_L = CASE WHEN @SNAMESOUNDEX IS NULL OR LENGTH(@SNAMESOUNDEX) < 5 THEN NULL ELSE SUBSTRING(@SNAMESOUNDEX, 2, 7) END;
SELECT DISTINCT
  PORT.ID,
  PORT.ORIGINAL_PORT_ID,
  PORT.COUNTRY_ID,
  LEVENSHTEIN_RATIO(@SNAME, PORT.SIMPLIFIED_NAME) AS RESULT_WEIGHT,
  PORT.NAME,
  PORT.SIMPLIFIED_NAME,
  PORT.SOURCE_SYSTEM
FROM (
	SELECT
		ID AS PORT_ID
  	FROM
   		S_PORTS
	INNER JOIN
    	VESSELS_TO_REGISTRATIONS
  	ON
	    VESSELS_TO_REGISTRATIONS.PORT_ID = S_PORTS.ID 
	WHERE (
		( @SNAMESOUNDEX_L IS NOT NULL AND LOCATE(@SNAMESOUNDEX_L, SIMPLIFIED_NAME_SOUNDEX) >= 2 ) OR  
		LOCATE(@SNAME, SIMPLIFIED_NAME) >= 1 OR
		LOCATE(@SNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX) >= 1 OR
		LOCATE(@SNAMESOUNDEX_R, SIMPLIFIED_NAME_SOUNDEX) >= 1 
	) AND
    LOCATE(CONCAT('@', VESSELS_TO_REGISTRATIONS.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
    ( 
    	CASE WHEN countryIDs IS NOT NULL 
    	THEN LOCATE(CONCAT('@', VESSELS_TO_REGISTRATIONS.COUNTRY_ID, '@'), countryIDs) >= 1
    	ELSE true
    	END
    ) AND
    CASE WHEN groupByUID = true THEN
    	CASE WHEN atDate IS NOT NULL THEN
			REFERENCE_DATE = (
		        SELECT
		         	MAX(REFERENCE_DATE)
		        FROM
		          	VESSELS_TO_REGISTRATIONS TEMP
		        WHERE
					TEMP.VESSEL_UID = VESSELS_TO_REGISTRATIONS.VESSEL_UID AND
		          	LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
		          	TEMP.REFERENCE_DATE <= atDate
		    ) 
		ELSE
			TRUE
		END
	ELSE
		CASE WHEN atDate IS NOT NULL THEN
		  	REFERENCE_DATE = (
		        SELECT
		        	MAX(REFERENCE_DATE)
		        FROM
		        	VESSELS_TO_REGISTRATIONS TEMP
		        WHERE
				    TEMP.VESSEL_ID = VESSELS_TO_REGISTRATIONS.VESSEL_ID AND
		          	LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
		          	TEMP.REFERENCE_DATE <= atDate
		    )
		ELSE
			TRUE
		END 
	END
	GROUP BY
 		S_PORTS.ID
) FILTERED
LEFT JOIN 
	S_PORTS PORT
ON
	FILTERED.PORT_ID = PORT.ID
ORDER BY
	RESULT_WEIGHT DESC, PORT.SIMPLIFIED_NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FUZZY_VESSEL_TYPE_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `FUZZY_VESSEL_TYPE_SEARCH`(IN vesselType VARCHAR(512), IN sourceSystems VARCHAR(512))
BEGIN
SET @UTYPE = UCASE(vesselType);
SELECT
  CASE WHEN VT.COUNTRY_ID IS NULL THEN 0 ELSE 1 END AS COUNTRY_DEPENDENT,
  0 AS RESULT_WEIGHT,
  VT.ID,
  VT.ORIGINAL_VESSEL_TYPE_ID,
  VT.NAME,
  VT.COUNTRY_ID,
  VT.STANDARD_ABBREVIATION,
  VT.SOURCE_SYSTEM
FROM
  s_vessel_types VT
WHERE
  CASE WHEN sourceSystems IS NULL THEN
    true
  ELSE
    LOCATE(CONCAT('@', VT.SOURCE_SYSTEM, '@'), sourceSystems) >= 1
  END

ORDER BY
  RESULT_WEIGHT DESC, COUNTRY_DEPENDENT ASC, VT.NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GEAR_TYPE_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `GEAR_TYPE_SEARCH`(IN sourceSystems VARCHAR(512), IN gearType VARCHAR(512))
BEGIN
SET @UTYPE = UCASE(gearType);
SELECT
  0 AS RESULT_WEIGHT,
  GT.ID,
  GT.ORIGINAL_GEAR_TYPE_CODE,
  GT.NAME,
  GT.STANDARD_ABBREVIATION,
  GT.SOURCE_SYSTEM
FROM
  s_gear_types GT
WHERE
    CASE WHEN sourceSystems IS NULL THEN
      true
    ELSE
      LOCATE(CONCAT('@', GT.SOURCE_SYSTEM, '@'), sourceSystems) >=1
    END 
ORDER BY
  GT.NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `IDENTIFIER_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `IDENTIFIER_SEARCH`(IN identifierType VARCHAR(16), IN identifierValue VARCHAR(64), IN sourceSystems VARCHAR(512), IN groupByUID BOOLEAN, IN authOnly BOOLEAN, IN authSource VARCHAR(16))
BEGIN
	SELECT
		IDENTIFIER AS IDENTIFIER,
    VESSELS_TO_IDENTIFIERS.TYPE_ID AS IDENTIFIER_TYPE,
		COUNT(DISTINCT CONCAT(VESSELS_TO_IDENTIFIERS.TYPE_ID, VESSELS_TO_IDENTIFIERS.IDENTIFIER, VESSELS_TO_IDENTIFIERS.SOURCE_SYSTEM, VESSELS_TO_IDENTIFIERS.VESSEL_ID)) AS TOTAL_OCCURRENCIES,
		COUNT(DISTINCT VESSELS_TO_IDENTIFIERS.VESSEL_ID) AS OCCURRENCIES,
		COUNT(DISTINCT VESSELS_TO_IDENTIFIERS.VESSEL_UID) AS GROUPED_OCCURRENCIES
	FROM
		VESSELS_TO_IDENTIFIERS
  INNER JOIN
    AUTHORIZATIONS
  ON
    VESSELS_TO_IDENTIFIERS.vessel_id = AUTHORIZATIONS.vessel_id
	WHERE
    VESSELS_TO_IDENTIFIERS.IDENTIFIER LIKE CONCAT('%', identifierValue, '%') AND
		CASE WHEN sourceSystems IS NULL THEN
		  true
		ELSE
		  LOCATE(CONCAT('@', VESSELS_TO_IDENTIFIERS.SOURCE_SYSTEM, '@'), sourceSystems) >= 1
		END AND
    CASE WHEN authOnly = true THEN
      CASE WHEN authSource IS NOT NULL THEN
    AUTHORIZATIONS.source_system = authSource
      ELSE
    TRUE
      END AND
    AUTHORIZATIONS.termination_reference_date IS NULL AND
    AUTHORIZATIONS.reference_date = (
      SELECT MAX(TEMP.reference_date)
      FROM AUTHORIZATIONS TEMP
      WHERE
          TEMP.vessel_id = AUTHORIZATIONS.vessel_id AND
          TEMP.source_system = AUTHORIZATIONS.source_system AND
          TEMP.type_id = AUTHORIZATIONS.type_id
      GROUP BY
        CASE WHEN groupByUID = true THEN TEMP.VESSEL_UID ELSE TEMP.VESSEL_ID END
    )
    ELSE
      TRUE
    END AND
    CASE WHEN identifierType IS NULL THEN
		  true
		ELSE
		  VESSELS_TO_IDENTIFIERS.TYPE_ID = identifierType
    END
  GROUP BY 2, 1
	ORDER BY 3 DESC, 4 DESC, 5 DESC, 1 ASC, 2 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `IDENTIFIER_SEARCH_TEST` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `IDENTIFIER_SEARCH_TEST`(IN identifierType VARCHAR(16), IN identifierValue VARCHAR(64), IN sourceSystems VARCHAR(512), IN groupByUID BOOLEAN, IN authOnly BOOLEAN, IN authSource VARCHAR(16))
BEGIN
	SELECT
		IDENTIFIER AS IDENTIFIER,
    VESSELS_TO_IDENTIFIERS.TYPE_ID AS IDENTIFIER_TYPE,
		COUNT(DISTINCT CONCAT(TYPE_ID, IDENTIFIER, SOURCE_SYSTEM, VESSEL_ID)) AS TOTAL_OCCURRENCIES,
		COUNT(DISTINCT VESSELS_TO_IDENTIFIERS.VESSEL_ID) AS OCCURRENCIES,
		COUNT(DISTINCT VESSELS_TO_IDENTIFIERS.VESSEL_UID) AS GROUPED_OCCURRENCIES
	FROM
		VESSELS_TO_IDENTIFIERS
	WHERE
    VESSELS_TO_IDENTIFIERS.IDENTIFIER LIKE CONCAT('%', identifierValue, '%') AND
		CASE WHEN sourceSystems IS NULL THEN
		  true
		ELSE
		  LOCATE(CONCAT('@', VESSELS_TO_IDENTIFIERS.SOURCE_SYSTEM, '@'), sourceSystems) >= 1
		END AND
    CASE WHEN identifierType IS NULL THEN
		  true
		ELSE
		  VESSELS_TO_IDENTIFIERS.TYPE_ID = identifierType
    END
  GROUP BY 2, 1
	ORDER BY 3 DESC, 4 DESC, 5 DESC, 1 ASC, 2 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ID_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `ID_SEARCH`(IN identifierValue VARCHAR(64), IN authorizedOnly BOOLEAN, IN authorizationSource VARCHAR(16))
BEGIN
	SELECT
    ID AS IDENTIFIER,
    'VRMF_ID' AS IDENTIFIER_TYPE,
		COUNT(*) AS TOTAL_OCCURRENCIES,
		COUNT(DISTINCT VESSELS.ID) AS OCCURRENCIES,
		COUNT(DISTINCT VESSELS.UID) AS GROUPED_OCCURRENCIES
	FROM
		VESSELS
  INNER JOIN
    AUTHORIZATIONS
  ON
    VESSELS.id = AUTHORIZATIONS.vessel_id
	WHERE
    CASE WHEN authorizedOnly = true THEN
      CASE WHEN authorizationSource IS NOT NULL THEN
    AUTHORIZATIONS.source_system = authorizationSource
      ELSE
    TRUE
      END AND
    AUTHORIZATIONS.termination_reference_date IS NULL AND
    AUTHORIZATIONS.reference_date = (
      SELECT MAX(TEMP.reference_date)
      FROM AUTHORIZATIONS TEMP
      WHERE
          TEMP.vessel_uid = AUTHORIZATIONS.vessel_uid AND
          TEMP.source_system = AUTHORIZATIONS.source_system AND
          TEMP.type_id = AUTHORIZATIONS.type_id
    )
    ELSE true END AND
	  ID LIKE CONCAT('%', identifierValue, '%')
	GROUP BY 1
  ORDER BY 2 DESC, 3 DESC, 4 DESC, 1 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `IRCS_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `IRCS_SEARCH`(IN IRCS VARCHAR(20), IN sourceSystems VARCHAR(512), IN atDate DATETIME, IN groupByUID BOOLEAN)
BEGIN
  SELECT
    CALLSIGN_ID,
    COUNT(DISTINCT CONCAT(CALLSIGN_ID, REFERENCE_DATE, VESSEL_ID)) AS TOTAL_OCCURRENCIES,
    COUNT(DISTINCT VESSEL_ID) AS OCCURRENCIES,
    COUNT(DISTINCT VESSEL_UID) AS GROUPED_OCCURRENCIES
  FROM (
    SELECT
      *
    FROM
      VESSELS_TO_CALLSIGNS
    WHERE
      UCASE(CALLSIGN_ID) LIKE CONCAT('%', UCASE(IRCS), '%') AND
      LOCATE(CONCAT('@', SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
      CASE WHEN groupByUID = true THEN
        CASE WHEN atDate IS NOT NULL THEN
          REFERENCE_DATE = (
            SELECT
              MAX(REFERENCE_DATE)
            FROM
              VESSELS_TO_CALLSIGNS TEMP
            WHERE
              TEMP.VESSEL_UID = VESSELS_TO_CALLSIGNS.VESSEL_UID AND
              LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
              TEMP.REFERENCE_DATE <= atDate
          ) 
        ELSE
          TRUE
        END
      ELSE
        CASE WHEN atDate IS NOT NULL THEN
          REFERENCE_DATE = (
            SELECT
              MAX(REFERENCE_DATE)
            FROM
              VESSELS_TO_CALLSIGNS TEMP
            WHERE
              TEMP.VESSEL_ID = VESSELS_TO_CALLSIGNS.VESSEL_ID AND
              LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
              TEMP.REFERENCE_DATE <= atDate
          )
        ELSE
          TRUE
        END 
      END
  ) FILTERED GROUP BY 1
  ORDER BY 2 DESC, 3 DESC, 4 DESC, 1 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `LINK_BY_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `LINK_BY_ID`(IN SOURCE_ID INT, IN TARGET_ID INT, IN NEW_MAPPING_WEIGHT DOUBLE, IN NEW_MAPPING_USER VARCHAR(16), IN NEW_MAPPING_DATE DATETIME, IN NEW_MAPPING_COMMENT VARCHAR(512))
BEGIN
	DECLARE SOURCE_UID INT;
    DECLARE SOURCE_MAPS_TO INT;
	DECLARE TARGET_UID INT;
    DECLARE SOURCE_IS_ROOT BOOLEAN;
    
    DECLARE UPDATED INT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	  BEGIN
	  GET DIAGNOSTICS CONDITION 1 @p1 = MESSAGE_TEXT;
	  ROLLBACK;
	  SELECT RAISE_ERROR(CONCAT('LINK_BY_ID ', SOURCE_ID, ' ', TARGET_ID, ' : ', @p1));
	END;

	START TRANSACTION;
     
    IF VESSEL_EXISTS_BY_ID(SOURCE_ID) = FALSE
    THEN SELECT RAISE_ERROR(CONCAT('No vessel can be identified by ID ', COALESCE(SOURCE_ID, '<UNKOWN SOURCE ID>')));
    END IF;
    
    IF VESSEL_EXISTS_BY_ID(TARGET_ID) = FALSE
    THEN SELECT RAISE_ERROR(CONCAT('No vessel can be identified by ID ', COALESCE(TARGET_ID, '<UNKOWN TARGET ID>')));
    END IF;
    
    SELECT CASE WHEN MAPS_TO IS NULL THEN TRUE ELSE FALSE END FROM VESSELS WHERE ID = SOURCE_ID INTO SOURCE_IS_ROOT;
		
	IF SOURCE_IS_ROOT = FALSE 
    THEN
		SELECT MAPS_TO FROM VESSELS WHERE ID = SOURCE_ID INTO SOURCE_MAPS_TO;
        
        IF SOURCE_MAPS_TO <> TARGET_ID 
        THEN SELECT RAISE_ERROR(CONCAT('Cannot link vessel ', COALESCE(SOURCE_ID, '<UNKOWN SOURCE ID>'), ' to vessel ', COALESCE(TARGET_ID, '<UNKOWN TARGET ID>'), ' as source vessel is already mapped'));
        END IF;
        
	END IF;
    
    SELECT UID FROM VESSELS WHERE ID = SOURCE_ID INTO SOURCE_UID;
    SELECT UID FROM VESSELS WHERE ID = TARGET_ID INTO TARGET_UID;
    
	UPDATE VESSELS 
    SET MAPS_TO = TARGET_ID,
        MAPPING_WEIGHT = NEW_MAPPING_WEIGHT,
        MAPPING_USER = NEW_MAPPING_USER,
        MAPPING_DATE = COALESCE(NEW_MAPPING_DATE, CURRENT_DATE),
        MAPPING_COMMENT = NEW_MAPPING_COMMENT
	WHERE
		ID = SOURCE_ID;
   
	CALL UPDATE_UID(SOURCE_ID, TARGET_UID);

	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `LINK_BY_RFMO_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `LINK_BY_RFMO_ID`(IN SOURCE_VESSEL_SOURCE VARCHAR(16), IN SOURCE_VESSEL_IDENTIFIER VARCHAR(64), IN TARGET_VESSEL_SOURCE VARCHAR(16), IN TARGET_VESSEL_ID VARCHAR(64),
                                                       IN NEW_MAPPING_WEIGHT DOUBLE, IN NEW_MAPPING_USER VARCHAR(16), IN NEW_MAPPING_DATE DATETIME, IN NEW_MAPPING_COMMENT VARCHAR(512))
BEGIN
CALL LINK_BY_ID(VESSEL_ID_BY_RFMO_ID(SOURCE_VESSEL_SOURCE, SOURCE_VESSEL_IDENTIFIER), 
                VESSEL_ID_BY_RFMO_ID(TARGET_VESSEL_SOURCE, TARGET_VESSEL_IDENTIFIER),
                NEW_MAPPING_WEIGHT,
                NEW_MAPPING_USER,
                NEW_MAPPING_DATE,
                NEW_MAPPING_COMMENT);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MERGE_BY_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `MERGE_BY_ID`(IN SOURCE_VESSEL_ID INTEGER, IN TARGET_VESSEL_ID INTEGER)
BEGIN
    DECLARE SOURCE_VESSEL_UID INT;
    DECLARE TARGET_VESSEL_UID INT;

    DECLARE SOURCE_MAPS_TO INT DEFAULT 0;
    DECLARE SOURCE_MAPPING_WEIGHT DOUBLE;
    DECLARE SOURCE_MAPPING_DATE DATETIME;
    DECLARE SOURCE_MAPPING_USER VARCHAR(16);
    DECLARE SOURCE_MAPPING_COMMENT VARCHAR(512);

    DECLARE TARGET_MAPS_TO INT DEFAULT 0;
    
    DECLARE TARGET_IDENTIFIER_REFERENCE_DATE DATETIME;

    DECLARE SOURCE_UPDATE_DATE TIMESTAMP;

    DECLARE TARGET_UPDATE_DATE TIMESTAMP;
    
    DECLARE v_finished INT DEFAULT 0;
    DECLARE v_mapped_id INT DEFAULT 0;
    
    DEClARE MAPPED_VESSEL_CURSOR CURSOR FOR 
        SELECT DISTINCT ID FROM VESSELS WHERE MAPS_TO = SOURCE_VESSEL_ID AND ID <> SOURCE_VESSEL_ID AND ID <> TARGET_VESSEL_ID;
 
    DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
      BEGIN
      GET DIAGNOSTICS CONDITION 1 @p1 = MESSAGE_TEXT;
      ROLLBACK;
      SELECT RAISE_ERROR(CONCAT('MERGE_BY_ID ', SOURCE_VESSEL_ID, ' ', TARGET_VESSEL_ID, ' : ', @p1));
    END;

    START TRANSACTION;

    IF VESSEL_EXISTS_BY_ID(SOURCE_VESSEL_ID) = FALSE
    THEN SELECT RAISE_ERROR(CONCAT('No vessel can be identified by ID ', COALESCE(SOURCE_VESSEL_ID, '<UNKOWN SOURCE ID>')));
    END IF;
    
    IF VESSEL_EXISTS_BY_ID(TARGET_VESSEL_ID) = FALSE
    THEN SELECT RAISE_ERROR(CONCAT('No vessel can be identified by ID ', COALESCE(TARGET_VESSEL_ID, '<UNKOWN SOURCE ID>')));
    END IF;
    
    SELECT REFERENCE_DATE 
    FROM VESSELS_TO_IDENTIFIERS I 
    WHERE 
        I.VESSEL_ID = TARGET_VESSEL_ID AND
        I.TYPE_ID = CONCAT(I.SOURCE_SYSTEM, '_ID') 
    AND REFERENCE_DATE = ( 
        SELECT MAX(REFERENCE_DATE) 
        FROM VESSELS_TO_IDENTIFIERS TEMP
        WHERE 
            I.VESSEL_ID = TEMP.VESSEL_ID AND
            I.TYPE_ID = TEMP.TYPE_ID
    ) INTO TARGET_IDENTIFIER_REFERENCE_DATE;        
    
    SELECT UID, UPDATE_DATE, MAPS_TO, MAPPING_WEIGHT, MAPPING_DATE, MAPPING_USER, MAPPING_COMMENT
    FROM VESSELS
    WHERE ID = SOURCE_VESSEL_ID
    INTO SOURCE_VESSEL_UID, SOURCE_UPDATE_DATE, SOURCE_MAPS_TO, SOURCE_MAPPING_WEIGHT, SOURCE_MAPPING_DATE, SOURCE_MAPPING_USER, SOURCE_MAPPING_COMMENT;

    SELECT UID, UPDATE_DATE, MAPS_TO
    FROM VESSELS
    WHERE ID = TARGET_VESSEL_ID
    INTO TARGET_VESSEL_UID, TARGET_UPDATE_DATE, TARGET_MAPS_TO;

    IF SOURCE_MAPS_TO IS NOT NULL AND TARGET_MAPS_TO IS NOT NULL AND SOURCE_MAPS_TO <> TARGET_MAPS_TO
    THEN    
        CALL RAISE_ERROR(CONCAT('Source vessel #', SOURCE_VESSEL_ID, ' maps to a different vessel (#', SOURCE_MAPS_TO, ') than target vessel #', TARGET_VESSEL_ID));
    END IF;

    INSERT IGNORE INTO AUTHORIZATIONS (
        SELECT DISTINCT
               S.AUTHORIZATION_ID, TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.AUTHORIZATION_ID, S.TYPE_ID, S.VALID_FROM, S.VALID_TO, S.REFERENCE_DATE,
               S.TERMINATION_REASON_CODE, S.TERMINATION_REASON, S.TERMINATION_REFERENCE_DATE, S.SOURCE_SYSTEM, S.ISSUING_COUNTRY_ID,
               S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM AUTHORIZATIONS S
        LEFT JOIN AUTHORIZATIONS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM AUTHORIZATIONS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.TYPE_ID = S.TYPE_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_BENEFICIAL_OWNERS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.ENTITY_ID, 
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_BENEFICIAL_OWNERS S
        LEFT JOIN VESSELS_TO_BENEFICIAL_OWNERS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_BENEFICIAL_OWNERS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    ); 

    INSERT IGNORE INTO VESSELS_TO_BUILDING_YEAR (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.VALUE,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_BUILDING_YEAR S
        LEFT JOIN VESSELS_TO_BUILDING_YEAR T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_BUILDING_YEAR TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    ); 
    
    INSERT IGNORE INTO VESSELS_TO_CALLSIGNS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.COUNTRY_ID, S.CALLSIGN_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_CALLSIGNS S
        LEFT JOIN VESSELS_TO_CALLSIGNS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_CALLSIGNS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.COUNTRY_ID = S.COUNTRY_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_CREW (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.VALUE,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_CREW S
        LEFT JOIN VESSELS_TO_CREW T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_CREW TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_EXTERNAL_MARKINGS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.EXTERNAL_MARKING,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_EXTERNAL_MARKINGS S
        LEFT JOIN VESSELS_TO_EXTERNAL_MARKINGS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_EXTERNAL_MARKINGS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_FISHING_LICENSE (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.COUNTRY_ID, S.LICENSE_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_FISHING_LICENSE S
        LEFT JOIN VESSELS_TO_FISHING_LICENSE T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_FISHING_LICENSE TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.COUNTRY_ID = S.COUNTRY_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_FISHING_MASTERS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.ENTITY_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_FISHING_MASTERS S
        LEFT JOIN VESSELS_TO_FISHING_MASTERS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_FISHING_MASTERS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );
    INSERT IGNORE INTO VESSELS_TO_FLAGS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.COUNTRY_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_FLAGS S
        LEFT JOIN VESSELS_TO_FLAGS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_FLAGS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_GEARS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.TYPE_ID, S.PRIMARY_GEAR,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_GEARS S
        LEFT JOIN VESSELS_TO_GEARS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_GEARS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.PRIMARY_GEAR = S.PRIMARY_GEAR AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_HULL_MATERIAL (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.TYPE_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_HULL_MATERIAL S
        LEFT JOIN VESSELS_TO_HULL_MATERIAL T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_HULL_MATERIAL TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_INSPECTIONS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.REPORT_ID, S.REPORT_TYPE_ID, S.COUNTRY_ID, S.ISSUING_AUTHORITY, S.AUTHORITY_ROLE_ID, S.DATE, S.LOCATION, S.INFRINGEMENT_TYPE_ID, S.DETAILS, S.CONTACT_DETAILS, S.OUTCOME_TYPE_ID, S.OUTCOME_DETAILS,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_INSPECTIONS S
        LEFT JOIN VESSELS_TO_INSPECTIONS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_INSPECTIONS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.REPORT_ID = S.REPORT_ID AND
                TEMP.REPORT_TYPE_ID = S.REPORT_TYPE_ID AND
                TEMP.COUNTRY_ID = S.COUNTRY_ID AND
                TEMP.AUTHORITY_ROLE_ID = S.AUTHORITY_ROLE_ID AND
                TEMP.INFRINGEMENT_TYPE_ID = S.INFRINGEMENT_TYPE_ID AND
                TEMP.OUTCOME_TYPE_ID = S.OUTCOME_TYPE_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_IUU_LIST (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.RFMO_IUU_LIST_ID, S.LISTING_DATE, S.DELISTING_DATE, S.URL,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_IUU_LIST S
        LEFT JOIN VESSELS_TO_IUU_LIST T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_IUU_LIST TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.RFMO_IUU_LIST_ID = S.RFMO_IUU_LIST_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_LENGTHS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.TYPE_ID, S.VALUE, S.UNIT,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_LENGTHS S
        LEFT JOIN VESSELS_TO_LENGTHS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_LENGTHS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.TYPE_ID = S.TYPE_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );  

    INSERT IGNORE INTO VESSELS_TO_MASTERS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.ENTITY_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_MASTERS S
        LEFT JOIN VESSELS_TO_MASTERS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_MASTERS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

     INSERT IGNORE INTO VESSELS_TO_MMSI (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.MMSI,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_MMSI S
        LEFT JOIN VESSELS_TO_MMSI T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_MMSI TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );
    
    INSERT IGNORE INTO VESSELS_TO_NAME (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.NAME, S.SIMPLIFIED_NAME, S.SIMPLIFIED_NAME_SOUNDEX,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.VALID_FROM, S.VALID_TO, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_NAME S
        LEFT JOIN VESSELS_TO_NAME T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_NAME TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );  

    INSERT IGNORE INTO VESSELS_TO_OPERATORS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.ENTITY_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_OPERATORS S
        LEFT JOIN VESSELS_TO_OPERATORS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_OPERATORS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_OWNERS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.ENTITY_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_OWNERS S
        LEFT JOIN VESSELS_TO_OWNERS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_OWNERS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_PORT_ENTRY_DENIALS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.NOTIFICATION_NUMBER, S.COUNTRY_ID, S.PORT_ID, S.DATE, S.PORT_ENTRY_DENIAL_REASON_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_PORT_ENTRY_DENIALS S
        LEFT JOIN VESSELS_TO_PORT_ENTRY_DENIALS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_PORT_ENTRY_DENIALS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.NOTIFICATION_NUMBER = S.NOTIFICATION_NUMBER AND
                TEMP.COUNTRY_ID = S.COUNTRY_ID AND
                TEMP.PORT_ID = S.PORT_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_POWER (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.TYPE_ID, S.MAIN_ENGINE,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_POWER S
        LEFT JOIN VESSELS_TO_POWER T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_POWER TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.MAIN_ENGINE = S.MAIN_ENGINE AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );
  
    INSERT IGNORE INTO VESSELS_TO_REGISTRATIONS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.REGISTRATION_NUMBER, S.COUNTRY_ID, S.PORT_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.VALID_FROM, S.VALID_TO, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_REGISTRATIONS S
        LEFT JOIN VESSELS_TO_REGISTRATIONS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_REGISTRATIONS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.COUNTRY_ID = S.COUNTRY_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );  

    INSERT IGNORE INTO VESSELS_TO_SHIPBUILDERS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.ENTITY_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_SHIPBUILDERS S
        LEFT JOIN VESSELS_TO_SHIPBUILDERS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_SHIPBUILDERS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_STATUS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.TYPE_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_STATUS S
        LEFT JOIN VESSELS_TO_STATUS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_STATUS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_TONNAGE (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.TYPE_ID, S.VALUE, S.UNIT,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_TONNAGE S
        LEFT JOIN VESSELS_TO_TONNAGE T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_TONNAGE TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.TYPE_ID = S.TYPE_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );  

    INSERT IGNORE INTO VESSELS_TO_TYPES (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.TYPE_ID,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_TYPES S
        LEFT JOIN VESSELS_TO_TYPES T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_TYPES TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );

    INSERT IGNORE INTO VESSELS_TO_VMS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.VMS_INDICATOR, S.VMS_DETAILS,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_VMS S
        LEFT JOIN VESSELS_TO_VMS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_VMS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );
    
    INSERT IGNORE INTO VESSELS_TO_IDENTIFIERS (
        SELECT DISTINCT
               TARGET_VESSEL_ID, TARGET_VESSEL_UID, S.TYPE_ID, S.IDENTIFIER, S.ALTERNATE_IDENTIFIER,
               S.REFERENCE_DATE, S.SOURCE_SYSTEM, S.UPDATE_DATE, S.UPDATER_ID, CONCAT(S.COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_IDENTIFIERS S
        LEFT JOIN VESSELS_TO_IDENTIFIERS T
        ON S.VESSEL_ID = SOURCE_VESSEL_ID AND T.VESSEL_ID = TARGET_VESSEL_ID
        WHERE 
            T.VESSEL_ID IS NOT NULL AND
            S.TYPE_ID <> CONCAT(S.SOURCE_SYSTEM, '_ID') AND
            T.TYPE_ID <> CONCAT(T.SOURCE_SYSTEM, '_ID') AND
            S.REFERENCE_DATE < (
                SELECT COALESCE(MAX(REFERENCE_DATE), '2999-12-31')
                FROM VESSELS_TO_IDENTIFIERS TEMP
                WHERE TEMP.VESSEL_ID = T.VESSEL_ID AND
                TEMP.TYPE_ID = S.TYPE_ID AND
                TEMP.SOURCE_SYSTEM = S.SOURCE_SYSTEM
            )
    );
        
    
    INSERT INTO VESSELS_TO_IDENTIFIERS ( 
        SELECT 
            TARGET_VESSEL_ID, 
            TARGET_VESSEL_UID, 
            TYPE_ID, 
            IDENTIFIER, 
            ALTERNATE_IDENTIFIER, 
            CASE 
                WHEN REFERENCE_DATE >= TARGET_IDENTIFIER_REFERENCE_DATE 
                THEN ( DATE_SUB(TARGET_IDENTIFIER_REFERENCE_DATE, INTERVAL 1 DAY )) 
                ELSE REFERENCE_DATE 
            END AS REFERENCE_DATE, 
            SOURCE_SYSTEM, 
            UPDATE_DATE, 
            UPDATER_ID, 
            CONCAT(COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        FROM VESSELS_TO_IDENTIFIERS
        WHERE VESSEL_ID = SOURCE_VESSEL_ID AND TYPE_ID = CONCAT(SOURCE_SYSTEM, '_ID')
    );
    
    
    UPDATE VESSELS 
        SET UPDATE_DATE = LEAST(SOURCE_UPDATE_DATE, TARGET_UPDATE_DATE)
    WHERE
        ID = TARGET_VESSEL_ID;
        
    
    UPDATE VESSELS
        SET MAPS_TO = TARGET_VESSEL_ID,
            MAPPING_COMMENT = CONCAT(MAPPING_COMMENT, ' - MERGED FROM PREVIOUS VESSEL #', SOURCE_VESSEL_ID)
        WHERE
            ID <> SOURCE_VESSEL_ID AND
            ID <> TARGET_VESSEL_ID AND
            MAPS_TO = SOURCE_VESSEL_ID;
            
    
    OPEN MAPPED_VESSEL_CURSOR;
 
    CYCLE: LOOP
     
        FETCH MAPPED_VESSEL_CURSOR INTO v_mapped_id;

        IF v_finished = 1 THEN LEAVE CYCLE; END IF;

        IF v_mapped_id IS NOT NULL
        THEN CALL UPDATE_UID(v_mapped_id, TARGET_VESSEL_UID);
        END IF;
        
        IF v_finished = 1 THEN LEAVE CYCLE; END IF;
        
    END LOOP CYCLE;
 
    CLOSE MAPPED_VESSEL_CURSOR;

    IF TARGET_MAPS_TO IS NOT NULL AND TARGET_MAPS_TO = SOURCE_VESSEL_ID
    THEN UPDATE VESSELS SET MAPS_TO = NULL, MAPPING_WEIGHT = NULL, MAPPING_USER = NULL, MAPPING_DATE = NULL, MAPPING_COMMENT = NULL WHERE ID = TARGET_VESSEL_ID;
    END IF;

    DELETE FROM VESSELS WHERE ID = SOURCE_VESSEL_ID;
    
    CALL UPDATE_VESSELS_TABLES_FOR_ID(SOURCE_VESSEL_ID);
    CALL UPDATE_VESSELS_TABLES_FOR_ID(TARGET_VESSEL_ID);

    COMMIT;
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MERGE_BY_RFMO_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `MERGE_BY_RFMO_ID`(IN SOURCE_VESSEL_SOURCE VARCHAR(16), IN SOURCE_VESSEL_IDENTIFIER VARCHAR(64), IN TARGET_VESSEL_SOURCE VARCHAR(16), IN TARGET_VESSEL_ID VARCHAR(64))
BEGIN
    CALL MERGE_BY_ID(VESSEL_ID_BY_RFMO_ID(SOURCE_VESSEL_SOURCE, SOURCE_VESSEL_IDENTIFIER), VESSEL_ID_BY_RFMO_ID(TARGET_VESSEL_SOURCE, TARGET_VESSEL_ID));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MMSI_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `MMSI_SEARCH`(IN MMSI_ID VARCHAR(20), IN sourceSystems VARCHAR(512), IN atDate DATETIME, IN groupByUID BOOLEAN)
BEGIN
  SELECT
    MMSI,
    COUNT(DISTINCT CONCAT(MMSI_ID, REFERENCE_DATE, VESSEL_ID)) AS TOTAL_OCCURRENCIES,
    COUNT(DISTINCT VESSEL_ID) AS OCCURRENCIES,
    COUNT(DISTINCT VESSEL_UID) AS GROUPED_OCCURRENCIES
  FROM (
    SELECT
      *
    FROM
      VESSELS_TO_MMSI
    WHERE
      MMSI LIKE CONCAT('%', MMSI_ID, '%') AND
      LOCATE(CONCAT('@', SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
      CASE WHEN groupByUID = true THEN
        CASE WHEN atDate IS NOT NULL THEN
          REFERENCE_DATE = (
            SELECT
              MAX(REFERENCE_DATE)
            FROM
              VESSELS_TO_MMSI TEMP
            WHERE
              TEMP.VESSEL_UID = VESSELS_TO_MMSI.VESSEL_UID AND
              LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
              TEMP.REFERENCE_DATE <= atDate
          ) 
        ELSE
          TRUE
        END
      ELSE
        CASE WHEN atDate IS NOT NULL THEN
          REFERENCE_DATE = (
            SELECT
              MAX(REFERENCE_DATE)
            FROM
              VESSELS_TO_MMSI TEMP
            WHERE
              TEMP.VESSEL_ID = VESSELS_TO_MMSI.VESSEL_ID AND
              LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
              TEMP.REFERENCE_DATE <= atDate
          )
        ELSE
          TRUE
        END 
    END
  ) FILTERED GROUP BY 1
	ORDER BY 2 DESC, 3 DESC, 4 DESC, 1 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `NEW_FLUSH_CONTENT_BY_SYSTEMS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `NEW_FLUSH_CONTENT_BY_SYSTEMS`(IN sourceSystems VARCHAR(512), IN INCLUDE_ENTITIES BOOLEAN)
BEGIN
	DECLARE CURRENT_STATUS INT;
	SET CURRENT_STATUS = @VRMF_TRIGGERS_DISABLED;

	SET @VRMF_TRIGGERS_DISABLED = 1;

	SET foreign_key_checks = 0;

	CREATE TEMPORARY TABLE V_TO_REMOVE_TEMP AS (
		SELECT ID FROM VESSELS
		WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0
	);
	
	ALTER TABLE V_TO_REMOVE_TEMP ADD PRIMARY KEY USING BTREE(ID);

	CREATE TEMPORARY TABLE V_TO_KEEP_TEMP AS (
		SELECT ID FROM VESSELS
		WHERE ID NOT IN ( SELECT ID FROM V_TO_REMOVE_TEMP )
	);
	
	ALTER TABLE V_TO_KEEP_TEMP ADD PRIMARY KEY USING BTREE(ID);

	CREATE TEMPORARY TABLE vessels_to_building_year_temp AS SELECT F.* FROM vessels_to_building_year F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_building_year;
	INSERT INTO vessels_to_building_year SELECT * FROM vessels_to_building_year_temp;

	CREATE TEMPORARY TABLE vessels_to_callsigns_temp AS SELECT F.* FROM vessels_to_callsigns F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_callsigns;
	INSERT INTO vessels_to_callsigns SELECT * FROM vessels_to_callsigns_temp;

	CREATE TEMPORARY TABLE vessels_to_crew_temp AS SELECT F.* FROM vessels_to_crew F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_crew;
	INSERT INTO vessels_to_crew SELECT * FROM vessels_to_crew_temp;

	CREATE TEMPORARY TABLE vessels_to_external_markings_temp AS SELECT F.* FROM vessels_to_external_markings F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_external_markings;
	INSERT INTO vessels_to_external_markings SELECT * FROM vessels_to_external_markings_temp;

	CREATE TEMPORARY TABLE vessels_to_fishing_license_temp AS SELECT F.* FROM vessels_to_fishing_license F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_fishing_license;
	INSERT INTO vessels_to_fishing_license SELECT * FROM vessels_to_fishing_license_temp;

	CREATE TEMPORARY TABLE vessels_to_flags_temp AS SELECT F.* FROM vessels_to_flags F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_flags;
	INSERT INTO vessels_to_flags SELECT * FROM vessels_to_flags_temp;

	CREATE TEMPORARY TABLE vessels_to_gears_temp AS SELECT F.* FROM vessels_to_gears F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_gears;
	INSERT INTO vessels_to_gears SELECT * FROM vessels_to_gears_temp;

	CREATE TEMPORARY TABLE vessels_to_identifiers_temp AS SELECT F.* FROM vessels_to_identifiers F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_identifiers;
	INSERT INTO vessels_to_identifiers SELECT * FROM vessels_to_identifiers_temp;

	CREATE TEMPORARY TABLE vessels_to_inspections_temp AS SELECT F.* FROM vessels_to_inspections F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_inspections;
	INSERT INTO vessels_to_building_year SELECT * FROM vessels_to_inspections_temp;

	CREATE TEMPORARY TABLE vessels_to_iuu_list_temp AS SELECT F.* FROM vessels_to_iuu_list F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_iuu_list;
	INSERT INTO vessels_to_building_year SELECT * FROM vessels_to_iuu_list_temp;

	CREATE TEMPORARY TABLE vessels_to_lengths_temp AS SELECT F.* FROM vessels_to_lengths F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_lengths;
	INSERT INTO vessels_to_lengths SELECT * FROM vessels_to_lengths_temp;

	CREATE TEMPORARY TABLE vessels_to_mmsi_temp AS SELECT F.* FROM vessels_to_mmsi F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_mmsi;
	INSERT INTO vessels_to_mmsi SELECT * FROM vessels_to_mmsi_temp;

	CREATE TEMPORARY TABLE vessels_to_name_temp AS SELECT F.* FROM vessels_to_name F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_name;
	INSERT INTO vessels_to_name SELECT * FROM vessels_to_name_temp;

	CREATE TEMPORARY TABLE vessels_to_non_compliance_temp AS SELECT F.* FROM vessels_to_non_compliance F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_non_compliance;
	INSERT INTO vessels_to_non_compliance SELECT * FROM vessels_to_non_compliance_temp;
	
	CREATE TEMPORARY TABLE vessels_to_operators_temp AS SELECT F.* FROM vessels_to_operators F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_operators;
	INSERT INTO vessels_to_operators SELECT * FROM vessels_to_operators_temp;

	CREATE TEMPORARY TABLE vessels_to_owners_temp AS SELECT F.* FROM vessels_to_owners F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_owners;
	INSERT INTO vessels_to_owners SELECT * FROM vessels_to_owners_temp;

	CREATE TEMPORARY TABLE vessels_to_port_entry_denials_temp AS SELECT F.* FROM vessels_to_port_entry_denials F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_port_entry_denials;
	INSERT INTO vessels_to_port_entry_denials SELECT * FROM vessels_to_port_entry_denials_temp;

	CREATE TEMPORARY TABLE vessels_to_power_temp AS SELECT F.* FROM vessels_to_power F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_power;
	INSERT INTO vessels_to_power SELECT * FROM vessels_to_power_temp;

	CREATE TEMPORARY TABLE vessels_to_registrations_temp AS SELECT F.* FROM vessels_to_registrations F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_registrations;
	INSERT INTO vessels_to_registrations SELECT * FROM vessels_to_registrations_temp;

	CREATE TEMPORARY TABLE vessels_to_shipbuilders_temp AS SELECT F.* FROM vessels_to_shipbuilders F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_shipbuilders;
	INSERT INTO vessels_to_shipbuilders SELECT * FROM vessels_to_shipbuilders_temp;

	CREATE TEMPORARY TABLE vessels_to_status_temp AS SELECT F.* FROM vessels_to_status F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_status;
	INSERT INTO vessels_to_status SELECT * FROM vessels_to_status_temp;

	CREATE TEMPORARY TABLE vessels_to_tonnage_temp AS SELECT F.* FROM vessels_to_tonnage F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_tonnage;
	INSERT INTO vessels_to_tonnage SELECT * FROM vessels_to_tonnage_temp;

	CREATE TEMPORARY TABLE vessels_to_types_temp AS SELECT F.* FROM vessels_to_types F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_to_types;
	INSERT INTO vessels_to_types SELECT * FROM vessels_to_types_temp;

	CREATE TEMPORARY TABLE authorization_details_temp AS
    SELECT F.* FROM authorization_details F
      INNER JOIN AUTHORIZATIONS A ON A.ID = F.AUTH_ID
        INNER JOIN V_TO_KEEP_TEMP ON A.VESSEL_ID = V_TO_KEEP_TEMP.ID;

	TRUNCATE TABLE authorization_details;
	INSERT INTO authorization_details SELECT * FROM authorization_details_temp;

	CREATE TEMPORARY TABLE authorizations_history_temp AS SELECT F.* FROM authorizations_history F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE authorizations_history;
	INSERT INTO authorizations_history SELECT * FROM authorizations_history_temp;

	CREATE TEMPORARY TABLE authorizations_temp AS SELECT F.* FROM authorizations F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE authorizations;
	INSERT INTO authorizations SELECT * FROM authorizations_temp;

	CREATE TEMPORARY TABLE vessels_history_temp AS SELECT F.* FROM vessels_history F INNER JOIN V_TO_KEEP_TEMP ON F.VESSEL_ID = V_TO_KEEP_TEMP.ID;
	TRUNCATE TABLE vessels_history;
	INSERT INTO vessels_history SELECT * FROM vessels_history_temp;

	UPDATE VESSELS
	SET
	  MAPS_TO = NULL,
	  MAPPING_WEIGHT = NULL,
	  MAPPING_USER = NULL,
	  MAPPING_DATE = NULL,
	  MAPPING_COMMENT = NULL
	WHERE
	  MAPS_TO IN (
		  SELECT ID FROM V_TO_REMOVE_TEMP
  	);

	DELETE FROM vessels WHERE ID IN ( SELECT * FROM V_TO_REMOVE_TEMP );

	IF INCLUDE_ENTITIES = true THEN
	  DELETE FROM entity WHERE LOCATE(SOURCE_SYSTEM, sourceSystems) > 0;

	  OPTIMIZE TABLE entity;
	END IF;

	OPTIMIZE TABLE vessels;

	SET @VRMF_TRIGGERS_DISABLED = CURRENT_STATUS;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `REG_NO_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `REG_NO_SEARCH`(IN registrationNumber VARCHAR(64), IN countryIDs VARCHAR(8192), IN portCodes VARCHAR(8192), IN sourceSystems VARCHAR(512), IN atDate DATETIME, IN groupByUID BOOLEAN)
BEGIN
  SELECT
    UCASE(REGISTRATION_NUMBER) AS REGISTRATION_NUMBER,
    COUNT(DISTINCT CONCAT(REGISTRATION_NUMBER, REFERENCE_DATE, VESSEL_ID)) AS TOTAL_OCCURRENCIES,
    COUNT(DISTINCT VESSEL_ID) AS OCCURRENCIES,
    COUNT(DISTINCT VESSEL_UID) AS GROUPED_OCCURRENCIES
  FROM (
  SELECT
    *
  FROM
    VESSELS_TO_REGISTRATIONS
  WHERE
    CASE WHEN countryIDs IS NULL THEN true
  	ELSE LOCATE(CONCAT('@', COUNTRY_ID, '@'), countryIDs) >= 1
  	END AND
    CASE WHEN portCodes IS NULL THEN true
  	ELSE LOCATE(CONCAT('@', PORT_ID, '@'), portCodes) >= 1
  	END AND
	  LOCATE(UCASE(registrationNumber), UCASE(REGISTRATION_NUMBER)) >= 1 AND
    CASE WHEN sourceSystems IS NULL THEN  true
    ELSE  LOCATE(CONCAT('@', SOURCE_SYSTEM, '@'), sourceSystems) >= 1
    END AND
      CASE WHEN groupByUID = true THEN
        CASE WHEN atDate IS NOT NULL THEN
          REFERENCE_DATE = (
            SELECT
              MAX(REFERENCE_DATE)
            FROM
              VESSELS_TO_REGISTRATIONS TEMP
            WHERE
              TEMP.VESSEL_UID = VESSELS_TO_REGISTRATIONS.VESSEL_UID AND
              LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
              TEMP.REFERENCE_DATE <= atDate
          ) 
        ELSE
          TRUE
        END
      ELSE
        CASE WHEN atDate IS NOT NULL THEN
          REFERENCE_DATE = (
            SELECT
              MAX(REFERENCE_DATE)
            FROM
              VESSELS_TO_REGISTRATIONS TEMP
            WHERE
              TEMP.VESSEL_ID = VESSELS_TO_REGISTRATIONS.VESSEL_ID AND
              LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
              TEMP.REFERENCE_DATE <= atDate
          )
        ELSE
          TRUE
        END 
      END
  ) FILTERED GROUP BY 1
	ORDER BY 2 DESC, 3 DESC, 4 DESC, 1 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `REPOPULATE_AUTHS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `REPOPULATE_AUTHS`()
BEGIN
  TRUNCATE TABLE AUTHORIZATIONS_HISTORY;
  OPTIMIZE TABLE AUTHORIZATIONS_HISTORY;

  TRUNCATE TABLE AUTHORIZATIONS_BULK_TERMINATIONS;
  OPTIMIZE TABLE AUTHORIZATIONS_BULK_TERMINATIONS;

  TRUNCATE TABLE AUTHORIZATIONS;
  OPTIMIZE TABLE AUTHORIZATIONS;

  INSERT INTO AUTHORIZATIONS SELECT * FROM TEMP_AUTHORIZATIONS;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SPECIES_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `SPECIES_SEARCH`(IN specieName VARCHAR(512), IN specieSimplifiedName VARCHAR(512))
BEGIN
SET @UNAME = UCASE(specieName);
SET @SNAME = UCASE(specieSimplifiedName);
SET @SNAMESOUNDEX = SOUNDEX(UCASE(specieSimplifiedName));
SELECT
   0 AS RESULT_WEIGHT,
   ID,
   FIGIS_ID,
   SOURCE_SYSTEM,
   TAXOCODE,
   CODE_3A,
   SCIENTIFIC_NAME,   
   NAME,
   SIMPLIFIED_NAME,
   FAMILY,
   SPECIE_ORDER
FROM
	S_SPECIES
WHERE
  LOCATE(@UNAME, TAXOCODE) >= 1 OR
  LOCATE(@UNAME, CODE_3A) >=1 OR
  LOCATE(@UNAME, UCASE(NAME)) >=1 OR
  LOCATE(@UNAME, UCASE(SIMPLIFIED_NAME)) >= 1 OR
  LOCATE(@UNAME, UCASE(SCIENTIFIC_NAME)) >= 1 OR
  LOCATE(@UNAME, UCASE(FAMILY)) >= 1 OR
  LOCATE(@UNAME, UCASE(SPECIE_ORDER)) >= 1 OR
  ( @SNAME IS NOT NULL AND LOCATE(@SNAME, UCASE(NAME)) >= 1 ) OR
  ( @SNAME IS NOT NULL AND LOCATE(@SNAME, UCASE(SIMPLIFIED_NAME)) >= 1 ) OR
  ( @SNAME IS NOT NULL AND LOCATE(@SNAME, UCASE(SCIENTIFIC_NAME)) >= 1 ) OR    
  LOCATE(@SNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX) >= 1 OR
  LOCATE(@SNAMESOUNDEX, SCIENTIFIC_NAME_SOUNDEX) >= 1 OR
  ( @SNAME IS NOT NULL AND LOCATE(@SNAME, UCASE(FAMILY)) >= 1 ) OR
  ( @SNAME IS NOT NULL AND LOCATE(@SNAME, UCASE(SPECIE_ORDER)) >= 1 ) OR
  LOCATE(@SNAMESOUNDEX, SOUNDEX(UCASE(FAMILY))) >= 1 OR
  LOCATE(@SNAMESOUNDEX, SOUNDEX(UCASE(SPECIE_ORDER))) >= 1 OR
  
  FALSE
ORDER BY
    SIMPLIFIED_NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TO_BE_REMOVED_EXT_MARK_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `TO_BE_REMOVED_EXT_MARK_SEARCH`(IN externalMarking VARCHAR(64), IN sourceSystems VARCHAR(512), IN atDate DATETIME, IN timeframe INT(11), IN groupByUID BOOLEAN)
BEGIN
  SELECT VARIABLE_VALUE FROM information_schema.SESSION_VARIABLES WHERE VARIABLE_NAME = 'CHARACTER_SET_CONNECTION' INTO @RESULT;
  SELECT
    EXTERNAL_MARKING,
    COUNT(DISTINCT CONCAT(EXTERNAL_MARKING, REFERENCE_DATE, VESSEL_ID)) AS TOTAL_OCCURRENCIES,
    COUNT(DISTINCT VESSEL_ID) AS OCCURRENCIES,
    COUNT(DISTINCT VESSEL_UID) AS GROUPED_OCCURRENCIES
  FROM (
  SELECT * FROM
    VESSELS_TO_EXTERNAL_MARKINGS
  WHERE
    LOCATE(CONCAT('@', SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
    LOCATE(externalMarking, EXTERNAL_MARKING) >= 1 AND
    CASE WHEN atDate IS NOT NULL THEN
      CASE WHEN groupByUID = true THEN
      REFERENCE_DATE = (
        SELECT
          MAX(REFERENCE_DATE)
        FROM
          VESSELS_TO_EXTERNAL_MARKINGS TEMP
        WHERE
          LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
		      TEMP.VESSEL_UID = VESSELS_TO_EXTERNAL_MARKINGS.VESSEL_UID AND
        CASE WHEN timeframe IS NOT NULL THEN
            ABS(DATEDIFF(atDate, TEMP.REFERENCE_DATE)) <= timeframe
        ELSE
            TEMP.REFERENCE_DATE <= atDate
        END
      )
      ELSE
      REFERENCE_DATE = (
        SELECT
          MAX(REFERENCE_DATE)
        FROM
          VESSELS_TO_EXTERNAL_MARKINGS TEMP
        WHERE
          LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
		      TEMP.VESSEL_ID = VESSELS_TO_EXTERNAL_MARKINGS.VESSEL_ID AND
        CASE WHEN timeframe IS NOT NULL THEN
            ABS(DATEDIFF(atDate, TEMP.REFERENCE_DATE)) <= timeframe
        ELSE
            TEMP.REFERENCE_DATE <= atDate
        END
      )
      END
    ELSE
      true
    END
  ) FILTERED
  GROUP BY 1
	ORDER BY 2 DESC, 3 DESC, 4 DESC, 1 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TO_BE_REMOVED_EXT_MARK_SEARCH_BY_AUTH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `TO_BE_REMOVED_EXT_MARK_SEARCH_BY_AUTH`(IN externalMarking VARCHAR(64), IN sourceSystems VARCHAR(512), IN atDate DATETIME, IN timeframe INT(11), IN groupByUID BOOLEAN, IN authOnly BOOLEAN, IN authSource VARCHAR(16))
BEGIN
  SELECT
    EXTERNAL_MARKING,
    COUNT(DISTINCT VESSELS_TO_EXTERNAL_MARKINGS.REFERENCE_DATE) AS TOTAL_OCCURRENCIES,
    COUNT(DISTINCT VESSELS_TO_EXTERNAL_MARKINGS.VESSEL_ID) AS OCCURRENCIES,
    COUNT(DISTINCT VESSELS_TO_EXTERNAL_MARKINGS.VESSEL_UID) AS GROUPED_OCCURRENCIES
  FROM
    VESSELS_TO_EXTERNAL_MARKINGS
  INNER JOIN
    AUTHORIZATIONS
  ON
    VESSELS_TO_MMSI.vessel_id = AUTHORIZATIONS.vessel_id
  WHERE
    EXTERNAL_MARKING LIKE CONCAT('%', externalMarking, '%') AND
    LOCATE(CONCAT('@', VESSELS_TO_EXTERNAL_MARKINGS.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
    CASE WHEN authOnly = true THEN
      CASE WHEN authSource IS NOT NULL THEN
    AUTHORIZATIONS.source_system = authSource
      ELSE
    TRUE
      END AND
    AUTHORIZATIONS.termination_reference_date IS NULL AND
    AUTHORIZATIONS.reference_date = (
      SELECT MAX(TEMP.reference_date)
      FROM AUTHORIZATIONS TEMP
      WHERE
          TEMP.vessel_id = AUTHORIZATIONS.vessel_id AND
          TEMP.source_system = AUTHORIZATIONS.source_system AND
          TEMP.type_id = AUTHORIZATIONS.type_id AND
          CASE WHEN atDate IS NOT NULL THEN
          TEMP.REFERENCE_DATE <= atDate
          ELSE TRUE END
      GROUP BY
        CASE WHEN groupByUID = true THEN TEMP.VESSEL_UID ELSE TEMP.VESSEL_ID END
    )
    ELSE
      TRUE
    END AND
    CASE WHEN atDate IS NOT NULL THEN
      VESSELS_TO_EXTERNAL_MARKINGS.REFERENCE_DATE = (
        SELECT
          MAX(TEMP.REFERENCE_DATE)
        FROM
          VESSELS_TO_MMSI TEMP
        WHERE
          TEMP.VESSEL_ID = VESSELS_TO_EXTERNAL_MARKINGS.VESSEL_ID AND
          TEMP.VESSEL_ID = AUTHORIZATIONS.VESSEL_ID AND
          LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
          CASE WHEN timeframe IS NOT NULL THEN
            ABS(DATEDIFF(atDate, TEMP.REFERENCE_DATE)) <= timeframe
          ELSE
            TEMP.REFERENCE_DATE <= atDate
          END
        GROUP BY
          CASE WHEN groupByUID = true THEN TEMP.VESSEL_UID ELSE TEMP.VESSEL_ID END
      )
    ELSE
      TRUE
    END
  GROUP BY 1
	ORDER BY 2 DESC, 3 DESC, 4 DESC, 1 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TO_BE_REMOVED_FUZZY_ENTITY_NAME_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `TO_BE_REMOVED_FUZZY_ENTITY_NAME_SEARCH`(IN entityName VARCHAR(512), IN simplifiedEntityName VARCHAR(512), IN entityType VARCHAR(16))
BEGIN
SET @UNAME = UCASE(entityName);
SET @SNAMESOUNDEX = SOUNDEX(UCASE(simplifiedEntityName));
SELECT
   CASE
    WHEN UCASE(NAME) = @UNAME THEN 100
		WHEN SIMPLIFIED_NAME = @UNAME THEN 90
    WHEN NPOS = 1 THEN GREATEST(70, (LEVENSHTEIN_RATIO(@UNAME, NAME)))
    WHEN SNPOS = 1 THEN GREATEST(60, (LEVENSHTEIN_RATIO(simplifiedEntityName, SIMPLIFIED_NAME)))
    WHEN SXPOS = 1 THEN 0.5 * LEVENSHTEIN_RATIO(@SNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX)
		WHEN NPOS > 1 THEN (LEVENSHTEIN_RATIO(@UNAME, NAME))
		WHEN SNPOS > 1 THEN (LEVENSHTEIN_RATIO(simplifiedEntityName, SIMPLIFIED_NAME))
		WHEN SXPOS > 1 THEN 0.2 * (LEVENSHTEIN_RATIO(@SNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX))
   ELSE
    GREATEST(LEVENSHTEIN_RATIO(@UNAME, NAME), LEVENSHTEIN_RATIO(simplifiedEntityName, SIMPLIFIED_NAME))
   END AS RESULT_WEIGHT,
   ID,
   ENTITY_TYPE_ID,
   NAME,
   BRANCH,
   SALUTATION,
   ROLE,
   COUNTRY_ID,
   ADDRESS,
   CITY,
   ZIP_CODE,
   SOURCE_SYSTEM
FROM
(
	SELECT
    ID,
    ENTITY_TYPE_ID,
		LOCATE(@UNAME, UCASE(NAME)) AS NPOS,
		LOCATE(@UNAME, SIMPLIFIED_NAME) AS SNPOS,
		LOCATE(@SNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX) AS SXPOS,
		NAME,
    SIMPLIFIED_NAME,
    SIMPLIFIED_NAME_SOUNDEX,
    BRANCH,
    SALUTATION,
    ROLE,
    COUNTRY_ID,
    ADDRESS,
    CITY,
    ZIP_CODE,
    SOURCE_SYSTEM
	FROM
		entity
  WHERE
    ENTITY_TYPE_ID = entityType 
  GROUP BY NAME
  HAVING ( NPOS >= 1 OR SNPOS >= 1 OR SXPOS >= 1 )
) FILTERED
ORDER BY
  RESULT_WEIGHT DESC, SIMPLIFIED_NAME ASC
LIMIT 100 OFFSET 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TO_BE_REMOVED_FUZZY_VESSEL_NAME_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `TO_BE_REMOVED_FUZZY_VESSEL_NAME_SEARCH`(IN vesselName VARCHAR(512), IN simplifiedVesselName VARCHAR(512), IN sourceSystems VARCHAR(512), IN atDate DATETIME, IN timeframe INT(11), IN groupByUID BOOLEAN)
BEGIN
SET @UNAME = UCASE(vesselName);
SET @SNAME = UCASE(simplifiedVesselName);
SET @SNAMESOUNDEX = SOUNDEX(UCASE(simplifiedVesselName));
SELECT
   CASE
    WHEN UCASE(NAME) = @UNAME THEN 100
		WHEN SIMPLIFIED_NAME = @UNAME THEN 90
    WHEN NPOS = 1 THEN 70
    WHEN SNPOS = 1 THEN 60
    WHEN SXPOS = 1 THEN 50
		WHEN SXPOS > 1 THEN 20
   ELSE
    10 
   END AS RESULT_WEIGHT,
   NAME,
   SIMPLIFIED_NAME,
   SIMPLIFIED_NAME_SOUNDEX,
   COUNT(DISTINCT CONCAT(FILTERED.NAME, FILTERED.REFERENCE_DATE, FILTERED.VESSEL_ID)) AS TOTAL_OCCURRENCIES,
   COUNT(DISTINCT FILTERED.VESSEL_ID) AS OCCURRENCIES,
   COUNT(DISTINCT FILTERED.VESSEL_UID) AS GROUPED_OCCURRENCIES
FROM
(
	SELECT
		LOCATE(@UNAME, UCASE(VESSELS_TO_NAME.NAME)) AS NPOS,
		LOCATE(@SNAME, VESSELS_TO_NAME.SIMPLIFIED_NAME) AS SNPOS,
		LOCATE(@SNAMESOUNDEX, VESSELS_TO_NAME.SIMPLIFIED_NAME_SOUNDEX) AS SXPOS,
    VESSELS_TO_NAME.NAME,
    VESSELS_TO_NAME.SIMPLIFIED_NAME,
    VESSELS_TO_NAME.SIMPLIFIED_NAME_SOUNDEX,
    VESSELS_TO_NAME.REFERENCE_DATE AS REFERENCE_DATE,
    VESSELS_TO_NAME.VESSEL_ID AS VESSEL_ID,
    VESSELS_TO_NAME.VESSEL_UID AS VESSEL_UID
	FROM
		vessels_to_name
	WHERE
    LOCATE(CONCAT('@', vessels_to_name.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
    (
      LOCATE(@UNAME, UCASE(VESSELS_TO_NAME.NAME)) >= 1 OR
  		CASE WHEN @SNAME IS NOT NULL
      THEN LOCATE(@SNAME, VESSELS_TO_NAME.SIMPLIFIED_NAME) >= 1
      ELSE FALSE END OR
  		CASE WHEN @SNAMESOUNDEX <> ''
      THEN LOCATE(@SNAMESOUNDEX, VESSELS_TO_NAME.SIMPLIFIED_NAME_SOUNDEX) >= 1
      ELSE FALSE END
    ) AND
    CASE WHEN atDate IS NOT NULL THEN
      CASE WHEN groupByUID = true THEN
      REFERENCE_DATE = (
        SELECT
          MAX(REFERENCE_DATE)
        FROM
          VESSELS_TO_NAME TEMP
        WHERE
		      TEMP.VESSEL_UID = VESSELS_TO_NAME.VESSEL_UID AND
          LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
          CASE WHEN timeframe IS NOT NULL THEN
            ABS(DATEDIFF(atDate, TEMP.REFERENCE_DATE)) <= timeframe
          ELSE
            TEMP.REFERENCE_DATE <= atDate
          END
      )
      ELSE
      REFERENCE_DATE = (
        SELECT
          MAX(REFERENCE_DATE)
        FROM
          VESSELS_TO_NAME TEMP
        WHERE
		      TEMP.VESSEL_ID = VESSELS_TO_NAME.VESSEL_ID AND
          LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
          CASE WHEN timeframe IS NOT NULL THEN
            ABS(DATEDIFF(atDate, TEMP.REFERENCE_DATE)) <= timeframe
          ELSE
            TEMP.REFERENCE_DATE <= atDate
          END
      )
      END
    ELSE
      true
    END
) FILTERED

GROUP BY
  NAME
ORDER BY
  RESULT_WEIGHT DESC, SIMPLIFIED_NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TO_BE_REMOVED_FUZZY_VESSEL_REG_PORT_NAME_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `TO_BE_REMOVED_FUZZY_VESSEL_REG_PORT_NAME_SEARCH`(IN sourceSystems VARCHAR(512), IN countryIDs VARCHAR(512), IN portName VARCHAR(512), IN atDate DATETIME, IN timeframe INT(11), IN groupByUID BOOLEAN)
BEGIN
SET @UNAME = UCASE(portName);
SET @UNAMESOUNDEX = SOUNDEX(@UNAME);
SELECT DISTINCT
  CASE
    WHEN UCASE(NAME) = @UNAME     THEN 100
		WHEN SIMPLIFIED_NAME = @UNAME THEN 90
    
    WHEN NPOS = 1  THEN 70
    WHEN SNPOS = 1 THEN 60
    WHEN NPOS > 1  THEN 50
		WHEN SNPOS > 1 THEN 40
    WHEN SXPOS = 1 THEN 30
		WHEN SXPOS > 1 THEN 20  ELSE
    0 
  END AS RESULT_WEIGHT,
  FILTERED.ID,
  FILTERED.ORIGINAL_PORT_ID,
  FILTERED.COUNTRY_ID,
  FILTERED.NAME,
  FILTERED.SIMPLIFIED_NAME,
  FILTERED.SOURCE_SYSTEM
FROM (
  SELECT
		LOCATE(@UNAME, UCASE(NAME)) AS NPOS,
    LOCATE(@UNAME, P.SIMPLIFIED_NAME) AS SNPOS,
		LOCATE(@UNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX) AS SXPOS,
		P.ID AS ID,
    P.ORIGINAL_PORT_ID AS ORIGINAL_PORT_ID,
    P.COUNTRY_ID AS COUNTRY_ID,
    P.NAME AS NAME,
    P.SIMPLIFIED_NAME AS SIMPLIFIED_NAME,
    P.SOURCE_SYSTEM AS SOURCE_SYSTEM
  FROM
    S_PORTS P
  WHERE
    CASE WHEN countryIDs IS NULL THEN
      true
    ELSE
      LOCATE(CONCAT('@', P.COUNTRY_ID, '@'), countryIDs) >= 1
    END AND
    ( LOCATE(@UNAME, UCASE(NAME)) >= 1 OR LOCATE(@UNAME, P.SIMPLIFIED_NAME) >= 1 OR LOCATE(@UNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX) >= 1 )
) FILTERED
INNER JOIN
    VESSELS_TO_REGISTRATIONS
  ON
    VESSELS_TO_REGISTRATIONS.COUNTRY_ID = FILTERED.COUNTRY_ID AND
    VESSELS_TO_REGISTRATIONS.PORT_ID = FILTERED.ID AND
    CASE
      WHEN sourceSystems IS NOT NULL
    THEN
      LOCATE(CONCAT('@', VESSELS_TO_REGISTRATIONS.SOURCE_SYSTEM, '@'), sourceSystems) >= 1
    ELSE
      true
    END
  WHERE
    CASE WHEN countryIDs IS NULL THEN
      true
    ELSE
      LOCATE(CONCAT('@', VESSELS_TO_REGISTRATIONS.COUNTRY_ID, '@'), countryIDs) >= 1
    END AND
    CASE WHEN atDate IS NOT NULL THEN
      CASE WHEN groupByUID = true THEN
      VESSELS_TO_REGISTRATIONS.REFERENCE_DATE = (
        SELECT
          MAX(TEMP.REFERENCE_DATE)
        FROM
          VESSELS_TO_REGISTRATIONS TEMP
        WHERE
          TEMP.VESSEL_UID = VESSELS_TO_REGISTRATIONS.VESSEL_UID AND
          LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
          CASE WHEN timeframe IS NOT NULL THEN
            ABS(DATEDIFF(atDate, TEMP.REFERENCE_DATE)) <= timeframe
          ELSE
            TEMP.REFERENCE_DATE <= atDate
          END
      )
      ELSE
      VESSELS_TO_REGISTRATIONS.REFERENCE_DATE = (
        SELECT
          MAX(TEMP.REFERENCE_DATE)
        FROM
          VESSELS_TO_REGISTRATIONS TEMP
        WHERE
          TEMP.VESSEL_ID = VESSELS_TO_REGISTRATIONS.VESSEL_ID AND
          LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
          CASE WHEN timeframe IS NOT NULL THEN
            ABS(DATEDIFF(atDate, TEMP.REFERENCE_DATE)) <= timeframe
          ELSE
            TEMP.REFERENCE_DATE <= atDate
          END
      )
      END
    ELSE
      TRUE
    END
ORDER BY
  RESULT_WEIGHT DESC, FILTERED.SIMPLIFIED_NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TO_BE_REMOVED_FUZZY_VESSEL_REG_PORT_NAME_SEARCH_BY_AUTH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `TO_BE_REMOVED_FUZZY_VESSEL_REG_PORT_NAME_SEARCH_BY_AUTH`(IN sourceSystems VARCHAR(512), IN countryIDs VARCHAR(512), IN portName VARCHAR(512), IN atDate DATETIME, IN timeframe INT(11), IN groupByUID BOOLEAN, IN authOnly BOOLEAN, IN authSource VARCHAR(16))
BEGIN
SET @UNAME = UCASE(portName);
SET @UNAMESOUNDEX = SOUNDEX(@UNAME);
SELECT
  CASE
    WHEN UCASE(NAME) = @UNAME THEN 100
		WHEN SIMPLIFIED_NAME = @UNAME THEN 90
    WHEN LOCATE(@UNAME, UCASE(NAME)) = 1 THEN GREATEST(70, (LEVENSHTEIN_RATIO(@UNAME, NAME)))
    WHEN LOCATE(@UNAME, SIMPLIFIED_NAME) = 1 THEN GREATEST(60, (LEVENSHTEIN_RATIO(@UNAME, SIMPLIFIED_NAME)))
    WHEN LOCATE(@UNAMESOUNDEX, P.SIMPLIFIED_NAME_SOUNDEX) = 1 THEN 0.5 * LEVENSHTEIN_RATIO(@UNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX)
		WHEN LOCATE(@UNAME, UCASE(NAME)) > 1 THEN (LEVENSHTEIN_RATIO(@UNAME, NAME))
		WHEN LOCATE(@UNAME, SIMPLIFIED_NAME) > 1 THEN (LEVENSHTEIN_RATIO(@UNAME, SIMPLIFIED_NAME))
    WHEN LOCATE(@UNAMESOUNDEX, P.SIMPLIFIED_NAME_SOUNDEX) > 1 THEN 0.2 * (LEVENSHTEIN_RATIO(@UNAMESOUNDEX, SIMPLIFIED_NAME_SOUNDEX))
  ELSE
    GREATEST(LEVENSHTEIN_RATIO(@UNAME, NAME), LEVENSHTEIN_RATIO(@UNAME, SIMPLIFIED_NAME))
  END AS RESULT_WEIGHT,
  P.ID, P.ORIGINAL_PORT_ID, P.COUNTRY_ID, P.NAME, P.SOURCE_SYSTEM
FROM
    VESSELS_TO_REGISTRATIONS
  INNER JOIN
    S_PORTS P
  ON
    VESSELS_TO_REGISTRATIONS.COUNTRY_ID = P.COUNTRY_ID AND
    VESSELS_TO_REGISTRATIONS.PORT_ID = P.ID AND
    CASE WHEN sourceSystems IS NOT NULL THEN
    LOCATE(CONCAT('@', P.SOURCE_SYSTEM, '@'), sourceSystems) >= 1
    ELSE true END
  INNER JOIN
    AUTHORIZATIONS
  ON
    AUTH_VESSELS.id = AUTHORIZATIONS.vessel_id
  WHERE
    CASE WHEN countryIDs IS NULL THEN
      true
    ELSE
      LOCATE(CONCAT('@', P.COUNTRY_ID, '@'), countryIDs) >= 1
    END AND
    CASE WHEN authOnly = true THEN
      CASE WHEN authSource IS NOT NULL THEN
      AUTHORIZATIONS.source_system = authSource
      ELSE
        TRUE
      END AND
      AUTHORIZATIONS.termination_reference_date IS NULL AND
      AUTHORIZATIONS.reference_date = (
        SELECT MAX(TEMP.reference_date)
        FROM AUTHORIZATIONS TEMP
        WHERE
            TEMP.vessel_id = AUTHORIZATIONS.vessel_id AND
            TEMP.source_system = AUTHORIZATIONS.source_system AND
            TEMP.type_id = AUTHORIZATIONS.type_id AND
            CASE WHEN atDate IS NOT NULL THEN
              TEMP.REFERENCE_DATE <= atDate
            ELSE TRUE END
        GROUP BY
          CASE WHEN groupByUID = true THEN TEMP.VESSEL_UID ELSE TEMP.VESSEL_ID END
      )
    END AND
    CASE WHEN atDate IS NOT NULL THEN
      VESSELS_TO_REGISTRATIONS.REFERENCE_DATE = (
        SELECT
          MAX(TEMP.REFERENCE_DATE)
        FROM
          VESSELS_TO_MMSI TEMP
        WHERE
          TEMP.VESSEL_ID = VESSELS_TO_REGISTRATIONS.VESSEL_ID AND
          TEMP.VESSEL_ID = AUTHORIZATIONS.VESSEL_ID AND
          LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
          CASE WHEN timeframe IS NOT NULL THEN
            ABS(DATEDIFF(atDate, TEMP.REFERENCE_DATE)) <= timeframe
          ELSE
            TEMP.REFERENCE_DATE <= atDate
          END
        GROUP BY
          CASE WHEN groupByUID = true THEN TEMP.VESSEL_UID ELSE TEMP.VESSEL_ID END
      )
    ELSE
      TRUE
    END AND (
      LOCATE(@UNAME, UCASE(P.NAME)) >= 1 OR
      LOCATE(@UNAME, P.SIMPLIFIED_NAME) >= 1 OR
      LOCATE(@UNAMESOUNDEX, P.SIMPLIFIED_NAME_SOUNDEX) >= 1
    )
GROUP BY P.ID
ORDER BY
  RESULT_WEIGHT DESC, P.SIMPLIFIED_NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TO_BE_REMOVED_LINK_BY_TRFMO_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `TO_BE_REMOVED_LINK_BY_TRFMO_ID`(IN SOURCE_VESSEL_SOURCE VARCHAR(16), IN SOURCE_VESSEL_IDENTIFIER VARCHAR(64), IN TARGET_VESSEL_SOURCE VARCHAR(16), IN TARGET_VESSEL_ID VARCHAR(64),
                                                       IN NEW_MAPPING_WEIGHT DOUBLE, IN NEW_MAPPING_USER VARCHAR(16), IN NEW_MAPPING_DATE DATETIME, IN NEW_MAPPING_COMMENT VARCHAR(512))
BEGIN
CALL LINK_BY_ID(VESSEL_ID_BY_TRFMO_ID(SOURCE_VESSEL_SOURCE, SOURCE_VESSEL_IDENTIFIER), 
                  VESSEL_ID_BY_TRFMO_ID(TARGET_VESSEL_SOURCE, TARGET_VESSEL_IDENTIFIER),
                  NEW_MAPPING_WEIGHT,
                  NEW_MAPPING_USER,
                  NEW_MAPPING_DATE,
                  NEW_MAPPING_COMMENT);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TO_BE_REMOVED_MERGE_VESSELS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `TO_BE_REMOVED_MERGE_VESSELS`(IN SOURCE VARCHAR(16), IN OLD_SOURCE_ID VARCHAR(64), IN NEW_SOURCE_ID VARCHAR(64))
BEGIN
    DECLARE OLD_ID INT DEFAULT 0;
    DECLARE NEW_ID INT DEFAULT 0;

    DECLARE OLD_UID INT DEFAULT 0;
    DECLARE NEW_UID INT DEFAULT 0;
    
    DECLARE OLD_MAPS_TO INT DEFAULT 0;
    DECLARE OLD_MAPPING_WEIGHT DOUBLE;
    DECLARE OLD_MAPPING_DATE DATETIME;
    DECLARE OLD_MAPPING_USER VARCHAR(16);
    DECLARE OLD_MAPPING_COMMENT VARCHAR(512);

    DECLARE NEW_MAPS_TO INT DEFAULT 0;

    DECLARE OLD_MAPPED_VESSELS INT DEFAULT 0;
    DECLARE NEW_MAPPED_VESSELS INT DEFAULT 0;

    DECLARE OLD_IDENTIFIER_REFERENCE_DATE DATETIME;
    DECLARE OLD_UPDATE_DATE TIMESTAMP;

    DECLARE NEW_IDENTIFIER_REFERENCE_DATE DATETIME;
    DECLARE NEW_UPDATE_DATE TIMESTAMP;

    SELECT VESSEL_ID, VESSEL_UID, REFERENCE_DATE
    FROM VESSELS_TO_IDENTIFIERS I 
    WHERE TYPE_ID = CONCAT(SOURCE, '_ID') 
    AND IDENTIFIER = OLD_SOURCE_ID 
    AND REFERENCE_DATE = ( 
        SELECT MAX(REFERENCE_DATE) 
        FROM VESSELS_TO_IDENTIFIERS TEMP
        WHERE I.VESSEL_ID = TEMP.VESSEL_ID AND
        I.TYPE_ID = TEMP.TYPE_ID
    ) INTO OLD_ID, OLD_UID, OLD_IDENTIFIER_REFERENCE_DATE;

    SELECT VESSEL_ID, VESSEL_UID, REFERENCE_DATE 
    FROM VESSELS_TO_IDENTIFIERS I 
    WHERE TYPE_ID = CONCAT(SOURCE, '_ID') 
    AND IDENTIFIER = NEW_SOURCE_ID 
    AND REFERENCE_DATE = ( 
        SELECT MAX(REFERENCE_DATE) 
        FROM VESSELS_TO_IDENTIFIERS TEMP
        WHERE I.VESSEL_ID = TEMP.VESSEL_ID AND
        I.TYPE_ID = TEMP.TYPE_ID
    ) INTO NEW_ID, NEW_UID, NEW_IDENTIFIER_REFERENCE_DATE;      

    IF OLD_ID = 0 THEN CALL RAISE_ERROR(CONCAT('Unable to find vessel ID for old ', SOURCE, ' vessel ', OLD_SOURCE_ID)); END IF;
    IF NEW_ID = 0 THEN CALL RAISE_ERROR(CONCAT('Unable to find vessel ID for new ', SOURCE, ' vessel ', NEW_SOURCE_ID)); END IF;

    SELECT MAPS_TO, MAPPING_WEIGHT, MAPPING_DATE, MAPPING_USER, MAPPING_COMMENT
    FROM VESSELS 
    WHERE ID = OLD_ID INTO OLD_MAPS_TO, OLD_MAPPING_WEIGHT, OLD_MAPPING_DATE, OLD_MAPPING_USER, OLD_MAPPING_COMMENT;

    SELECT MAPS_TO
    FROM VESSELS 
    WHERE ID = NEW_ID INTO NEW_MAPS_TO;

    IF OLD_MAPS_TO IS NOT NULL AND NEW_MAPS_TO IS NOT NULL AND OLD_MAPS_TO <> NEW_MAPS_TO
    THEN    
        SELECT OLD_ID, NEW_ID;
        CALL RAISE_ERROR(CONCAT('Old ', SOURCE, ' vessel ', OLD_SOURCE_ID, ' (', OLD_ID, ') maps to a different vessel (', OLD_MAPS_TO, ') than new ', SOURCE, ' vessel ', NEW_SOURCE_ID, ' (', NEW_ID , ' -> ', NEW_MAPS_TO, ')'));
    END IF;

    SELECT COUNT(DISTINCT ID) FROM VESSELS WHERE MAPS_TO = OLD_ID AND ID <> NEW_ID INTO OLD_MAPPED_VESSELS;

    IF OLD_MAPPED_VESSELS > 0
    THEN
        SELECT OLD_ID, OLD_UID, NEW_ID, NEW_UID;
        CALL RAISE_ERROR(CONCAT(OLD_MAPPED_VESSELS, ' vessels are currently mapped to old ', SOURCE, ' vessel ', OLD_SOURCE_ID, ' (', OLD_ID, ')'));
    END IF;

    SELECT COUNT(DISTINCT ID) FROM VESSELS WHERE MAPS_TO = NEW_ID AND ID <> OLD_ID INTO NEW_MAPPED_VESSELS;

    IF NEW_MAPPED_VESSELS > 0
    THEN
        SELECT OLD_ID, OLD_UID, NEW_ID, NEW_UID;
        CALL RAISE_ERROR(CONCAT(NEW_MAPPED_VESSELS, ' vessels are currently mapped to new ', SOURCE, ' vessel ', NEW_SOURCE_ID, ' (', NEW_ID, ')'));
    END IF;

    SELECT UPDATE_DATE FROM VESSELS WHERE ID = OLD_ID INTO OLD_UPDATE_DATE;
    SELECT UPDATE_DATE FROM VESSELS WHERE ID = NEW_ID INTO NEW_UPDATE_DATE;

    UPDATE IGNORE AUTHORIZATIONS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    
    UPDATE IGNORE VESSELS_TO_BENEFICIAL_OWNERS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_BUILDING_YEAR SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_CALLSIGNS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_CREW SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_EXTERNAL_MARKINGS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_FISHING_LICENSE SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_FISHING_MASTERS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_FLAGS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_GEARS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_HULL_MATERIAL SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    
    INSERT INTO VESSELS_TO_IDENTIFIERS ( 
        SELECT NEW_ID, NEW_UID, TYPE_ID, IDENTIFIER, ALTERNATE_IDENTIFIER, CASE WHEN TYPE_ID = CONCAT(SOURCE, '_ID') AND REFERENCE_DATE >= NEW_IDENTIFIER_REFERENCE_DATE THEN ( DATE_SUB(NEW_IDENTIFIER_REFERENCE_DATE, INTERVAL 1 DAY )) ELSE REFERENCE_DATE END AS REFERENCE_DATE, SOURCE_SYSTEM, UPDATE_DATE, UPDATER_ID, COMMENT
        FROM VESSELS_TO_IDENTIFIERS
        WHERE VESSEL_ID = OLD_ID
    );

    DELETE FROM VESSELS_TO_IDENTIFIERS WHERE VESSEL_ID = OLD_ID;

    UPDATE IGNORE VESSELS_TO_INSPECTIONS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_IUU_LIST SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_LENGTHS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_MASTERS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_MMSI SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_NAME SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_NON_COMPLIANCE SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_OPERATORS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_OWNERS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_PORT_ENTRY_DENIALS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_POWER SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_REGISTRATIONS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_SHIPBUILDERS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_STATUS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_TONNAGE SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_TYPES SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;
    UPDATE IGNORE VESSELS_TO_VMS SET VESSEL_ID = NEW_ID, VESSEL_UID = NEW_UID WHERE VESSEL_ID = OLD_ID;

    UPDATE VESSELS 
        SET UPDATE_DATE = LEAST(OLD_UPDATE_DATE, NEW_UPDATE_DATE)
    WHERE
        ID = NEW_ID;

    IF OLD_MAPS_TO IS NOT NULL AND NEW_MAPS_TO IS NULL THEN
        UPDATE VESSELS 
        SET MAPS_TO = OLD_MAPS_TO, 
            MAPPING_WEIGHT = OLD_MAPPING_WEIGHT, 
            MAPPING_DATE = OLD_MAPPING_DATE,
            MAPPING_USER = OLD_MAPPING_USER,
            MAPPING_COMMENT = OLD_MAPPING_COMMENT,
            UID = OLD_UID
        WHERE ID = NEW_ID;
    END IF;

    DELETE FROM VESSELS WHERE ID = OLD_ID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TO_BE_REMOVED_MMSI_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `TO_BE_REMOVED_MMSI_SEARCH`(IN MMSI_ID VARCHAR(20), IN sourceSystems VARCHAR(512), IN atDate DATETIME, IN timeframe INT(11), IN groupByUID BOOLEAN)
BEGIN
  SELECT
    MMSI,
    COUNT(DISTINCT CONCAT(MMSI_ID, REFERENCE_DATE, VESSEL_ID)) AS TOTAL_OCCURRENCIES,
    COUNT(DISTINCT VESSEL_ID) AS OCCURRENCIES,
    COUNT(DISTINCT VESSEL_UID) AS GROUPED_OCCURRENCIES
  FROM (
  SELECT
    *
  FROM
    VESSELS_TO_MMSI
  WHERE
    MMSI LIKE CONCAT('%', MMSI_ID, '%') AND
    LOCATE(CONCAT('@', SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
    CASE WHEN atDate IS NOT NULL THEN
      CASE WHEN groupByUID = true THEN
      REFERENCE_DATE = (
        SELECT
          MAX(REFERENCE_DATE)
        FROM
          VESSELS_TO_MMSI TEMP
        WHERE
          LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
		      TEMP.VESSEL_UID = VESSELS_TO_MMSI.VESSEL_UID AND
          CASE WHEN timeframe IS NOT NULL THEN
            ABS(DATEDIFF(atDate, TEMP.REFERENCE_DATE)) <= timeframe
          ELSE
            TEMP.REFERENCE_DATE <= atDate
          END
      )
      ELSE
      REFERENCE_DATE = (
        SELECT
          MAX(REFERENCE_DATE)
        FROM
          VESSELS_TO_MMSI TEMP
        WHERE
          LOCATE(CONCAT('@', TEMP.SOURCE_SYSTEM, '@'), sourceSystems) >= 1 AND
		      TEMP.VESSEL_ID = VESSELS_TO_MMSI.VESSEL_ID AND
          CASE WHEN timeframe IS NOT NULL THEN
            ABS(DATEDIFF(atDate, TEMP.REFERENCE_DATE)) <= timeframe
          ELSE
            TEMP.REFERENCE_DATE <= atDate
          END
      )
      END
    ELSE
      true
    END
  ) FILTERED GROUP BY 1
	ORDER BY 2 DESC, 3 DESC, 4 DESC, 1 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TO_BE_REMOVED_UNLINK_BY_TRFMO_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `TO_BE_REMOVED_UNLINK_BY_TRFMO_ID`(IN VESSEL_SOURCE VARCHAR(16), IN VESSEL_IDENTIFIER VARCHAR(64))
BEGIN
    CALL UNLINK_BY_ID(VESSEL_ID_BY_TRFMO_ID(VESSEL_SOURCE, VESSEL_IDENTIFIER));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TO_BE_REMOVED_UPDATE_UID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `TO_BE_REMOVED_UPDATE_UID`(IN SOURCE_ID INT(11), IN NEW_UID INT(11))
BEGIN
    DECLARE v_to_update INT DEFAULT 0;
    DECLARE v_finished INT DEFAULT 0;
    DECLARE v_id INT;
    DECLARE v_expectedUID INT;
   
    DEClARE VESSEL_CURSOR CURSOR FOR 
        SELECT DISTINCT ID FROM VESSELS WHERE MAPS_TO = SOURCE_ID;
 
    DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;

    DECLARE exit handler for sqlexception
      BEGIN
        
      ROLLBACK;
    END;

    DECLARE exit handler for sqlwarning
     BEGIN
        
     ROLLBACK;
    END;

    START TRANSACTION;
        
    IF VESSEL_EXISTS_BY_ID(SOURCE_ID) = false
    THEN SELECT RAISE_ERROR(CONCAT('Cannot find vessel with ID ', COALESCE(SOURCE_ID, '<UNKNOWN SOURCE ID>')));
    END IF;
    
    SELECT VT.UID FROM VESSELS VT LEFT JOIN VESSELS VS ON VT.ID = VS.MAPS_TO WHERE VS.ID = SOURCE_ID INTO v_expectedUID;

    IF v_expectedUID IS NOT NULL AND v_expectedUID <> NEW_UID
    THEN SELECT RAISE_ERROR(CONCAT('Target UID (', COALESCE(NEW_UID, '<UNKOWN NEW UID>'),') clashes with currently mapped vessel UID (', COALESCE(v_expectedUID, '<UNKNOWN EXPECTED UID>'), ') for vessel ', COALESCE(SOURCE_ID, '<UNKNOWN SOURCE ID>')));
    ELSE 
        UPDATE VESSELS SET UID = NEW_UID WHERE ID = SOURCE_ID;
        
        CALL UPDATE_VESSELS_TABLES_FOR_ID(SOURCE_ID);
        
        OPEN VESSEL_CURSOR;
     
        CYCLE: LOOP
         
            FETCH VESSEL_CURSOR INTO v_id;

            IF v_id IS NOT NULL
            THEN CALL UPDATE_UID(v_id, NEW_UID);
            END IF;
            
            IF v_finished = 1 THEN LEAVE CYCLE; END IF;
            
        END LOOP CYCLE;
     
        CLOSE VESSEL_CURSOR;
       
    END IF;

    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UID_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `UID_SEARCH`(IN identifierValue VARCHAR(64), IN authorizedOnly BOOLEAN, IN authorizationSource VARCHAR(16))
BEGIN
	SELECT
    UID AS IDENTIFIER,
    'VRMF_UID' AS IDENTIFIER_TYPE,
		COUNT(*) AS TOTAL_OCCURRENCIES,
		COUNT(DISTINCT VESSELS.ID) AS OCCURRENCIES,
		COUNT(DISTINCT VESSELS.UID) AS GROUPED_OCCURRENCIES
	FROM
		VESSELS
  INNER JOIN
    AUTHORIZATIONS
  ON
    VESSELS.uid = AUTHORIZATIONS.vessel_uid
	WHERE
    CASE WHEN authorizedOnly = true THEN
      CASE WHEN authorizationSource IS NOT NULL THEN
    AUTHORIZATIONS.source_system = authorizationSource
      ELSE
    TRUE
      END AND
    AUTHORIZATIONS.termination_reference_date IS NULL AND
    AUTHORIZATIONS.reference_date = (
      SELECT MAX(TEMP.reference_date)
      FROM AUTHORIZATIONS TEMP
      WHERE
          TEMP.vessel_uid = AUTHORIZATIONS.vessel_uid AND
          TEMP.source_system = AUTHORIZATIONS.source_system AND
          TEMP.type_id = AUTHORIZATIONS.type_id
    )
    ELSE true END AND
	  UID LIKE CONCAT('%', identifierValue, '%')
	GROUP BY 1
  ORDER BY 2 DESC, 3 DESC, 4 DESC, 1 ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UNLINK_BY_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `UNLINK_BY_ID`(IN VESSEL_ID INT)
BEGIN
    DECLARE NEW_UID INT;
    
    DECLARE CURRENT_UID INT;
    DECLARE IS_MAPPED BOOLEAN;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
      BEGIN
      GET DIAGNOSTICS CONDITION 1 @p1 = MESSAGE_TEXT;
      ROLLBACK;
      SELECT RAISE_ERROR(CONCAT('UNLINK_BY_ID ', VESSEL_ID, ' : ', @p1));
    END;

    START TRANSACTION;

    IF VESSEL_EXISTS_BY_ID(VESSEL_ID) = FALSE 
    THEN 
        SELECT RAISE_ERROR(CONCAT('No vessel can be identified by ID ', COALESCE(VESSEL_ID, '<UNKOWN VESSEL ID>')));
    ELSE
        SELECT UID, CASE WHEN MAPS_TO IS NOT NULL THEN TRUE ELSE FALSE END FROM VESSELS WHERE ID = VESSEL_ID INTO CURRENT_UID, IS_MAPPED;
        
        IF IS_MAPPED = FALSE
        THEN SELECT CURRENT_UID;
        ELSE
            SET NEW_UID = GET_NEW_UID('VRMF');
            UPDATE VESSELS SET MAPS_TO = NULL, MAPPING_WEIGHT = NULL, MAPPING_DATE = NULL, MAPPING_USER = NULL, MAPPING_COMMENT = NULL WHERE ID = VESSEL_ID;
            CALL UPDATE_UID(VESSEL_ID, NEW_UID);
            SELECT NEW_UID;
        END IF;
    END IF;

    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UNLINK_BY_RFMO_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `UNLINK_BY_RFMO_ID`(IN VESSEL_SOURCE VARCHAR(16), IN VESSEL_IDENTIFIER VARCHAR(64))
BEGIN
    CALL UNLINK_BY_ID(VESSEL_ID_BY_RFMO_ID(VESSEL_SOURCE, VESSEL_IDENTIFIER));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UPDATE_UID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `UPDATE_UID`(IN SOURCE_ID INT(11), IN NEW_UID INT(11))
BEGIN
    DECLARE v_to_update INT DEFAULT 0;
    DECLARE v_finished INT DEFAULT 0;
    DECLARE v_id INT;
    DECLARE v_expectedUID INT;
   
    DECLARE VESSEL_CURSOR CURSOR FOR 
        SELECT DISTINCT ID FROM VESSELS WHERE MAPS_TO = SOURCE_ID;
 
    DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;

    DECLARE exit handler for sqlexception
		BEGIN
        
		ROLLBACK;
    END;

    DECLARE exit handler for sqlwarning
		BEGIN
        
		ROLLBACK;
    END;

    START TRANSACTION;
        
		IF VESSEL_EXISTS_BY_ID(SOURCE_ID) = false
		THEN SELECT RAISE_ERROR(CONCAT('Cannot find vessel with ID ', COALESCE(SOURCE_ID, '<UNKNOWN SOURCE ID>')));
		END IF;
	 
		UPDATE VESSELS SET UID = NEW_UID WHERE ID = SOURCE_ID;
		
		CALL UPDATE_VESSELS_TABLES_FOR_ID(SOURCE_ID);
		
		OPEN VESSEL_CURSOR;
	 
		CYCLE: LOOP
			FETCH VESSEL_CURSOR INTO v_id;
			
			IF v_finished = 1 THEN LEAVE CYCLE; END IF;
			
			IF v_id IS NOT NULL THEN
				CALL UPDATE_UID(v_id, NEW_UID);
			END IF;
		END LOOP CYCLE;
	 
		CLOSE VESSEL_CURSOR;

    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UPDATE_VESSELS_TABLES` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `UPDATE_VESSELS_TABLES`()
BEGIN
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UPDATE_VESSELS_TABLES_FOR_ID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `UPDATE_VESSELS_TABLES_FOR_ID`(IN TARGET_VESSEL_ID INT)
BEGIN
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `VESSEL_TYPE_SEARCH` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`vrmf_root`@`%` PROCEDURE `VESSEL_TYPE_SEARCH`(IN sourceSystems VARCHAR(512), IN vesselType VARCHAR(512))
BEGIN
SET @UTYPE = UCASE(vesselType);
SELECT
  CASE WHEN VT.COUNTRY_ID IS NULL THEN 0 ELSE 1 END AS COUNTRY_DEPENDENT,
  0 AS RESULT_WEIGHT,
  VT.ID,
  VT.ORIGINAL_VESSEL_TYPE_ID,
  VT.NAME,
  VT.COUNTRY_ID,
  VT.STANDARD_ABBREVIATION,
  VT.SOURCE_SYSTEM
FROM
  s_vessel_types VT
WHERE
    CASE WHEN sourceSystems IS NULL THEN
      true
    ELSE
      LOCATE(CONCAT('@', VT.SOURCE_SYSTEM, '@'), sourceSystems) >= 1
    END
ORDER BY
  RESULT_WEIGHT DESC, COUNTRY_DEPENDENT ASC, VT.ENGLISH_NAME ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `v_authorization_types`
--

/*!50001 DROP VIEW IF EXISTS `v_authorization_types`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`vrmf_root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_authorization_types` AS (select (case when (`a`.`ID` = 'DEFAULT') then 1 else 0 end) AS `IS_DEFAULT`,`a`.`ID` AS `ID`,`a`.`SOURCE_SYSTEM` AS `SOURCE_SYSTEM`,`a`.`COUNTRY_DEPENDANT` AS `COUNTRY_DEPENDANT`,`a`.`DESCRIPTION` AS `DESCRIPTION`,`a`.`UPDATE_DATE` AS `UPDATE_DATE`,`a`.`UPDATER_ID` AS `UPDATER_ID`,`a`.`COMMENT` AS `COMMENT` from `s_authorization_types` `a` order by `a`.`SOURCE_SYSTEM`,(case when (`a`.`ID` = 'DEFAULT') then 1 else 0 end) desc,`a`.`ID`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_vessels_to_identifiers`
--

/*!50001 DROP VIEW IF EXISTS `v_vessels_to_identifiers`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`vrmf_root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_vessels_to_identifiers` AS (select `i`.`VESSEL_ID` AS `VESSEL_ID`,`i`.`VESSEL_UID` AS `VESSEL_UID`,`i`.`TYPE_ID` AS `TYPE_ID`,`i`.`IDENTIFIER` AS `IDENTIFIER`,`i`.`ALTERNATE_IDENTIFIER` AS `ALTERNATE_IDENTIFIER`,`i`.`REFERENCE_DATE` AS `REFERENCE_DATE`,`i`.`SOURCE_SYSTEM` AS `SOURCE_SYSTEM`,`i`.`UPDATE_DATE` AS `UPDATE_DATE`,`i`.`UPDATER_ID` AS `UPDATER_ID`,`i`.`COMMENT` AS `COMMENT` from `vessels_to_identifiers` `i` where (`i`.`REFERENCE_DATE` = (select max(`temp`.`REFERENCE_DATE`) from `vessels_to_identifiers` `temp` where ((`i`.`VESSEL_ID` = `temp`.`VESSEL_ID`) and (`i`.`TYPE_ID` = `temp`.`TYPE_ID`))))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-01 15:04:55
