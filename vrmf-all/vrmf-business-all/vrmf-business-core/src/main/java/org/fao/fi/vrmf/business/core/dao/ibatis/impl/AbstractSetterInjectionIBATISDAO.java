/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business-core)
 */
package org.fao.fi.vrmf.business.core.dao.ibatis.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.fao.fi.vrmf.business.core.dao.ibatis.SetterInjectionIBATISDAO;

import com.ibatis.sqlmap.client.SqlMapClient;

import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Mar 2011
 */
@NoArgsConstructor
abstract public class AbstractSetterInjectionIBATISDAO extends AbstractIBATISDAO implements SetterInjectionIBATISDAO {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.business.dao.ibatis.SetterInjectionIBATISDAO#setSqlMapClient(com.ibatis.sqlmap.client.SqlMapClient)
	 */
	@Override
	@Named(VRMF_DEFAULT_SQL_MAP_CLIENT_BEAN_ID)
	@Inject public void setSqlMapClient(SqlMapClient sqlMapClient) {
		this.sqlMapClient = sqlMapClient;
	}
}
