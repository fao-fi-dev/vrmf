/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business-core)
 */
package org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.plugins.clients.support;

import org.mybatis.generator.api.IntrospectedTable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Mar 2013
 */
public interface BeanIdValueGenerator {
	String generateBeanId(IntrospectedTable introspectedTable);
}
