/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business-core)
 */
package org.fao.fi.vrmf.business.core.utilities.orm.ibatis2;

import java.io.InputStream;
import java.nio.charset.Charset;

import org.fao.fi.sh.utility.core.helpers.singletons.AbstractSingletonHelper;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Jun 2011   faoadmin     Creation.
 *
 * @version 1.0
 * @since 16 Jun 2011
 */
public final class SqlMapClientBuilderUtilities extends AbstractSingletonHelper {
	static public SqlMapClient buildSqlMapClient(String resourcePath) {
		try {
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			
			AbstractSingletonHelper.getLogger(SqlMapClientBuilderUtilities.class).info("Building SqlMap client from resource at path {} (URL is: {})", resourcePath, loader.getResource(resourcePath));
	
			SqlMapClient client = SqlMapClientBuilder.buildSqlMapClient(loader.getResourceAsStream(resourcePath));
			
			StringBuilder serialized = new StringBuilder();
						
			InputStream is = loader.getResourceAsStream(resourcePath);
			
			byte[] buffer = new byte[2048];
			
			int len = -1;
			
			while((len = is.read(buffer)) != -1) {
				serialized.append(new String(buffer, 0, len, Charset.forName("UTF-8")));
			}
			
			AbstractSingletonHelper.getLogger(SqlMapClientBuilderUtilities.class).debug(serialized.toString());
						
			return client;
		} catch (Throwable t) {
			AbstractSingletonHelper.getLogger(SqlMapClientBuilderUtilities.class).error("Unable to build SqlMap client from resource at path {}", resourcePath, t);
			
			return null;
		}
	}
}
