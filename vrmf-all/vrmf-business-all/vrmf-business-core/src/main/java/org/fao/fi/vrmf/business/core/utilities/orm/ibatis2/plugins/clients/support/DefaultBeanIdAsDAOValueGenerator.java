/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business-core)
 */
package org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.plugins.clients.support;

import org.fao.fi.sh.utility.core.helpers.singletons.text.CamelCaseHelper;
import org.mybatis.generator.api.IntrospectedTable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Mar 2013
 */
public class DefaultBeanIdAsDAOValueGenerator implements BeanIdValueGenerator {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.business.utilities.orm.ibatis.plugins.clients.support.BeanIdValueGenerator#generateBeanId(org.mybatis.generator.api.IntrospectedTable)
	 */
	@Override
	public String generateBeanId(IntrospectedTable introspectedTable) {
		return "dao." + CamelCaseHelper.convertFromUnderscoresToCamelCase(introspectedTable.getFullyQualifiedTable().getIntrospectedTableName().replaceAll("\\\\", "").replaceAll("\\s", "_"));
	}
}