/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business-core)
 */
package org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.plugins.models;

import java.io.Serializable;
import java.util.List;
import java.util.Properties;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Based on: http://mybatis.googlecode.com/svn/sub-projects/generator/trunk/core/mybatis-generator-core/src/main/java/org/mybatis/generator/plugins/SerializablePlugin.java
 * 
 * This extended version allows specifying a generated serial version UID as part of the configuration
 * or as a random number. Furthermore, it doesn't add the 'java.io.Serializable' implementation if the base class
 * is already serializable.
 */
public class AddGeneratedSerialVersionUIDPlugin extends PluginAdapter {
	Logger _log = LoggerFactory.getLogger(this.getClass());
	
	private FullyQualifiedJavaType serializable;
	private boolean suppressJavaInterface;
	private Long serialVersionUID;

	public AddGeneratedSerialVersionUIDPlugin() {
		super();
		serializable = new FullyQualifiedJavaType("java.io.Serializable");
	}

	public boolean validate(List<String> warnings) {
		return true;
	}

	@Override
	public void setProperties(Properties properties) {
		super.setProperties(properties);
		suppressJavaInterface = Boolean.valueOf(properties.getProperty("suppressJavaInterface"));
		serialVersionUID = properties.containsKey("serialVersionUID") ? Long.valueOf(properties.getProperty("serialVersionUID")) : null;
	}

	@Override
	public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		makeSerializable(topLevelClass, introspectedTable);
		return true;
	}

	@Override
	public boolean modelPrimaryKeyClassGenerated(TopLevelClass topLevelClass,
			IntrospectedTable introspectedTable) {
		makeSerializable(topLevelClass, introspectedTable);
		return true;
	}

	@Override
	public boolean modelRecordWithBLOBsClassGenerated(
			TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		makeSerializable(topLevelClass, introspectedTable);
		return true;
	}

	protected void makeSerializable(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		if (!suppressJavaInterface) {
			boolean implementsSerializable = false;
			boolean isSerializable = false;
			
			try {
				Class<?> actual = Class.forName(topLevelClass.getSuperClass().toString());
				
				isSerializable = Serializable.class.isAssignableFrom(actual);
				
				for(Class<?> interfaze : actual.getInterfaces())
					implementsSerializable = interfaze.equals(Serializable.class);
			} catch(ClassNotFoundException CNFe) {
				this._log.warn("Unable to check whether top level class "  + topLevelClass.toString() + " is serializable: defaulting to false", CNFe);
			}
			
			if(implementsSerializable && !isSerializable) {
				topLevelClass.addImportedType(serializable);
				topLevelClass.addSuperInterface(serializable);
			}
			
			Field field = new Field();
			field.setFinal(true);

			if (serialVersionUID != null)
				field.setInitializationString(String.valueOf(serialVersionUID)+ "L" );
			else
				field.setInitializationString(
					( System.currentTimeMillis() % 2 == 0 ? "" : "-" ) +
					  String.valueOf(Math.round(System.currentTimeMillis() * Math.random())) + 
					  "L" 
				);

			field.setName("serialVersionUID");
			field.setStatic(true);
			field.setType(new FullyQualifiedJavaType("long"));
			field.setVisibility(JavaVisibility.PRIVATE);
			context.getCommentGenerator().addFieldComment(field, introspectedTable);

			topLevelClass.addField(field);
		}
	}
}