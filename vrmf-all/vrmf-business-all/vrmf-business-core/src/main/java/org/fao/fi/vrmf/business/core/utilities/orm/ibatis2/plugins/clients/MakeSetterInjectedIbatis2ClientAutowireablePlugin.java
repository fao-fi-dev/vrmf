/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business-core)
 */
package org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.plugins.clients;

import java.util.List;
import java.util.Properties;

import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractSetterInjectionIBATISDAO;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.plugins.clients.support.BeanIdValueGenerator;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.plugins.clients.support.DefaultBeanIdAsDAOValueGenerator;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.IntrospectedTable.TargetRuntime;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11/mar/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 11/mar/2013
 */
public class MakeSetterInjectedIbatis2ClientAutowireablePlugin extends PluginAdapter {
	Logger _log = LoggerFactory.getLogger(this.getClass());
	
	static final private String ANNOTATIONS_STYLE_JAVAX_INJECT	= "javax";
	static final private String ANNOTATIONS_STYLE_SPRING		= "spring";
	
	static final private String DEFAULT_ANNOTATIONS_STYLE		= ANNOTATIONS_STYLE_JAVAX_INJECT;
	
	static final private String DEFAULT_BEAN_ID_VALUE_GENERATOR_CLASS_NAME = DefaultBeanIdAsDAOValueGenerator.class.getName();
	
	static final private String DEFAULT_BASE_CLASS_NAME		   = AbstractSetterInjectionIBATISDAO.class.getName();
	static final private String DEFAULT_SETTER_METHOD_NAME	   = "setSqlMapClient";
	static final private String DEFAULT_FIELD_NAME	   		   = "sqlMapClient";
	static final private String DEFAULT_SQL_MAP_CLIENT_BEAN_ID = "ibatis.default.sqlMapClient";
	
	static final public String ANNOTATIONS_STYLE_PROPERTY_NAME = "annotationsStyle";
	static final public String ADD_BEAN_ANNOTATION_PROPERTY_NAME = "addBeanAnnotation";
	static final public String ADD_BEAN_ID_PROPERTY_NAME = "addBeanId";
	static final public String BEAN_ID_VALUE_GENERATOR_TYPE_PROPERTY_NAME = "beanIdValueGeneratorType";
	static final public String ADD_SINGLETON_ANNOTATION_PROPERTY_NAME = "addSingletonAnnotation";
	static final public String ADD_AUTOWIRED_SQL_MAP_CLIENT_BEAN_ID_ANNOTATION_PROPERTY_NAME = "addAutowiredSqlMapClientBeanIdAnnotation";
	static final public String SQL_MAP_CLIENT_BEAN_ID_PROPERTY_NAME = "sqlMapClientBeanId";
	static final public String EXTEND_BASE_CLASS_PROPERTY_NAME = "extendBaseClass";
	static final public String BASE_CLASS_NAME_PROPERTY_NAME = "baseClassName";
	static final public String AUTOWIRE_BY_SETTER_METHOD_PROPERTY_NAME = "autowireBySetterMethod";
	static final public String SQL_MAP_CLIENT_SETTER_NAME_PROPERTY_NAME = "sqlMapClientSetterName";
	static final public String AUTOWIRE_BY_FIELD_PROPERTY_NAME = "autowireByField";
	static final public String SQL_MAP_CLIENT_FIELD_NAME_PROPERTY_NAME = "sqlMapClientFieldName";
	
	private String _annotationsStyle;
	private Boolean _addBeanAnnotation;
	private Boolean _addBeanId;
	private String _beanIdValueGeneratorClassName;
	
	private Boolean _addSingletonAnnotation;
	
	private String _sqlMapClientBeanId;
	private Boolean _extendBaseClass;
	private String _baseClassName;
	
	private Boolean _addAutowiredSqlMapClientBeanIdAnnotation;
	
	private Boolean _autowireBySetterMethod;
	private String _sqlMapClientSetterMethodName;

	private Boolean _autowireByField;
	private String _sqlMapClientFieldName;

	private FullyQualifiedJavaType _baseClass;

	public MakeSetterInjectedIbatis2ClientAutowireablePlugin() {
		super();
	}

	@Override
	public boolean validate(List<String> warnings) {
		if(this._baseClassName != null) {
			try {
				Class.forName(this._baseClassName);
			} catch(ClassNotFoundException CNFe) {
				this._log.error("Base class '" + this._baseClassName + "' is not accessible. Generation will halt...");
				
				return false;
			}
			
			this._baseClass = new FullyQualifiedJavaType(this._baseClassName);
		}
		
		if(this._addBeanId && this._beanIdValueGeneratorClassName != null) {
			try {
				Class.forName(this._beanIdValueGeneratorClassName);
			} catch(ClassNotFoundException CNFe) {
				this._log.warn("Bean id value generator class '" + this._beanIdValueGeneratorClassName + "' is not accessible. Using default '" + DEFAULT_BEAN_ID_VALUE_GENERATOR_CLASS_NAME + "' to generate bean id...");
				
				this._beanIdValueGeneratorClassName = DEFAULT_BEAN_ID_VALUE_GENERATOR_CLASS_NAME;
			}
		}

		
		if(Boolean.TRUE.equals(this._addBeanAnnotation) || 
		   Boolean.TRUE.equals(this._addSingletonAnnotation))
			if(this._annotationsStyle != null)
				if(!ANNOTATIONS_STYLE_JAVAX_INJECT.equals(this._annotationsStyle) && 
				   !ANNOTATIONS_STYLE_SPRING.equals(this._annotationsStyle)) {
					this._log.error("Specified 'annotationsStyle' (" + this._annotationsStyle + ") is neither " + ANNOTATIONS_STYLE_JAVAX_INJECT + " nor " + ANNOTATIONS_STYLE_SPRING + ". Cannot continue!");
				}
				
		if(this._autowireByField && this._autowireBySetterMethod) {
			String message = "Both 'autowireByField' and 'autowireBySetterMethod' are set to TRUE. Cannot continue!";
			
			this._log.error(message);
			warnings.add(message);
			
			return false;
		}
		
		if(!this._autowireByField && !this._autowireBySetterMethod) {
			String message = "Neither 'autowireByField' nor 'autowireBySetterMethod' are set to TRUE. Generated client might not work as expected";
			
			this._log.warn(message);
			
			warnings.add(message);
		}

		return true;
	}
	
	@Override
	public void setProperties(Properties properties) {
		super.setProperties(properties);

		this._annotationsStyle = properties.getProperty(ANNOTATIONS_STYLE_PROPERTY_NAME);
		
		this._addBeanAnnotation = Boolean.valueOf(properties.getProperty(ADD_BEAN_ANNOTATION_PROPERTY_NAME));
		this._addBeanId = Boolean.valueOf(properties.getProperty(ADD_BEAN_ID_PROPERTY_NAME));
		this._beanIdValueGeneratorClassName = properties.getProperty(BEAN_ID_VALUE_GENERATOR_TYPE_PROPERTY_NAME);
		this._addSingletonAnnotation = Boolean.valueOf(properties.getProperty(ADD_SINGLETON_ANNOTATION_PROPERTY_NAME));
		
		if(this._addBeanAnnotation || this._addSingletonAnnotation)
			if(this._annotationsStyle == null) {
				this._log.warn("'" + ANNOTATIONS_STYLE_PROPERTY_NAME + "' is not specified: assuming default annotations style (" + DEFAULT_ANNOTATIONS_STYLE + ")");
				this._annotationsStyle = DEFAULT_ANNOTATIONS_STYLE;
			}
		
		if(this._addBeanId && this._beanIdValueGeneratorClassName == null) {
			this._beanIdValueGeneratorClassName = DEFAULT_BEAN_ID_VALUE_GENERATOR_CLASS_NAME;
			
			this._log.warn("'" + ADD_BEAN_ID_PROPERTY_NAME + "' is set to true but no bean id value generator type is specified via '" + BEAN_ID_VALUE_GENERATOR_TYPE_PROPERTY_NAME + "': using default generator type " + this._beanIdValueGeneratorClassName);
		}
			
		this._addAutowiredSqlMapClientBeanIdAnnotation = Boolean.valueOf(properties.getProperty(ADD_AUTOWIRED_SQL_MAP_CLIENT_BEAN_ID_ANNOTATION_PROPERTY_NAME));
		
		this._sqlMapClientBeanId = properties.getProperty(SQL_MAP_CLIENT_BEAN_ID_PROPERTY_NAME);
		
		if(this._sqlMapClientBeanId == null)
			this._sqlMapClientBeanId = DEFAULT_SQL_MAP_CLIENT_BEAN_ID;
		
		this._extendBaseClass = Boolean.valueOf(properties.getProperty(EXTEND_BASE_CLASS_PROPERTY_NAME));
		
		if(this._extendBaseClass) {
			this._baseClassName = properties.getProperty(BASE_CLASS_NAME_PROPERTY_NAME);
		
			if(this._baseClassName == null) {
				this._baseClassName = DEFAULT_BASE_CLASS_NAME;
				this._log.warn("'" + EXTEND_BASE_CLASS_PROPERTY_NAME + "' was set to TRUE but no base class name was specified through '" + BASE_CLASS_NAME_PROPERTY_NAME + "': assuming " + this._baseClassName);
				
			}
		}
		
		this._autowireBySetterMethod = Boolean.valueOf(properties.getProperty(AUTOWIRE_BY_SETTER_METHOD_PROPERTY_NAME));
		
		if(this._autowireBySetterMethod) {
			this._sqlMapClientSetterMethodName = properties.getProperty(SQL_MAP_CLIENT_SETTER_NAME_PROPERTY_NAME);
			
			if(this._sqlMapClientSetterMethodName == null) {
				this._sqlMapClientSetterMethodName = DEFAULT_SETTER_METHOD_NAME;
				
				this._log.warn("'" + AUTOWIRE_BY_SETTER_METHOD_PROPERTY_NAME + "' was set to TRUE but no setter method name was specified through '" + SQL_MAP_CLIENT_SETTER_NAME_PROPERTY_NAME + "': assuming " + this._sqlMapClientSetterMethodName);
			}
		}
		
		this._autowireByField = Boolean.valueOf(properties.getProperty(AUTOWIRE_BY_FIELD_PROPERTY_NAME));
		
		if(this._autowireByField) {
			this._sqlMapClientFieldName = properties.getProperty(SQL_MAP_CLIENT_FIELD_NAME_PROPERTY_NAME);
			
			if(this._sqlMapClientFieldName == null) {
				this._sqlMapClientFieldName = DEFAULT_FIELD_NAME;
				
				this._log.warn("'" + AUTOWIRE_BY_FIELD_PROPERTY_NAME + "' was set to TRUE but no field name was specified through '" + SQL_MAP_CLIENT_FIELD_NAME_PROPERTY_NAME + "': assuming " + this._sqlMapClientFieldName);
			}
		}
	}
	
	private String getActualBeanId(IntrospectedTable introspectedTable) {
		if(!this._addBeanId || this._beanIdValueGeneratorClassName == null)
			return null;
		
		String beanId = "client." + introspectedTable.getFullyQualifiedTable().getIntrospectedTableName();
		
		try {
			beanId = ((BeanIdValueGenerator)Class.forName(this._beanIdValueGeneratorClassName).newInstance()).generateBeanId(introspectedTable);
		} catch(ClassNotFoundException CNFe) {
			this._log.warn("Unable to find bean id value generator class '" + this._beanIdValueGeneratorClassName + "'. Using default bean id (" + beanId + ") for annotated client " + introspectedTable.getDAOImplementationType(), CNFe);
		} catch(InstantiationException Ie) {
			this._log.warn("Unable to instantiate bean id value generator class '" + this._beanIdValueGeneratorClassName + "'. Using default bean id (" + beanId + ") for annotated client " + introspectedTable.getDAOImplementationType(), Ie);
		} catch(IllegalAccessException IAe) {
			this._log.warn("Unable to access bean id value generator class '" + this._beanIdValueGeneratorClassName + "'. Using default bean id (" + beanId + ") for annotated client " + introspectedTable.getDAOImplementationType(), IAe);
		}
		
		return beanId;
	}
	
	private String getSanitizedBeanId(String beanId) {
		return beanId == null ? null : beanId.replaceAll("\\\"", "\\\\\"");
	}
	
	private String getActualNamedAnnotation(TopLevelClass topLevelClass, String beanId) {
		String sanitizedBeanId = this.getSanitizedBeanId(beanId);
		boolean isJavax = ANNOTATIONS_STYLE_JAVAX_INJECT.equals(this._annotationsStyle);
		
		String annotation = 
			isJavax ?
				"@Named" : 
				"@Qualifier";
		
		annotation += sanitizedBeanId == null ? "" : "(\"" + sanitizedBeanId + "\")";
		
		if(isJavax)
			topLevelClass.addImportedType(new FullyQualifiedJavaType("javax.inject.Named"));
		else
			topLevelClass.addImportedType(new FullyQualifiedJavaType("org.springframework.beans.factory.annotation.Qualifier"));
		
		return annotation;
	}
	
	private String getActualSingletonAnnotation(TopLevelClass topLevelClass) {
		boolean isJavax = ANNOTATIONS_STYLE_JAVAX_INJECT.equals(this._annotationsStyle);

		if(isJavax)
			topLevelClass.addImportedType(new FullyQualifiedJavaType("javax.inject.Singleton"));
		else {
			topLevelClass.addImportedType(new FullyQualifiedJavaType("org.springframework.context.annotation.Scope"));
			topLevelClass.addImportedType(new FullyQualifiedJavaType("org.springframework.beans.factory.config.ConfigurableBeanFactory"));
		}
		
		return isJavax ? "@Singleton" : "@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)";
	}
	
	private String getActualInjectAnnotation(TopLevelClass topLevelClass) {
		boolean isJavax = ANNOTATIONS_STYLE_JAVAX_INJECT.equals(this._annotationsStyle);

		if(isJavax)
			topLevelClass.addImportedType(new FullyQualifiedJavaType("javax.inject.Inject"));
		else
			topLevelClass.addImportedType(new FullyQualifiedJavaType("org.springframework.beans.factory.annotation.Autowired"));
		
		return isJavax ? "@Inject" : "@Autowired(required=true)";
	}

	@Override
	public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		super.clientGenerated(interfaze, topLevelClass, introspectedTable);

		boolean isIBATIS2  = introspectedTable.getTargetRuntime() == TargetRuntime.IBATIS2;
		boolean isSIClient = "GENERIC-SI".equals(this.getContext().getJavaClientGeneratorConfiguration().getConfigurationType()); 
		
		if(isIBATIS2) {
			if(isSIClient) {
				if(this._addBeanAnnotation) {
					if(ANNOTATIONS_STYLE_SPRING.equals(this._annotationsStyle)) {
						topLevelClass.addAnnotation("@Component");
						topLevelClass.addImportedType(new FullyQualifiedJavaType("org.springframework.stereotype.Component"));
					}
					
					topLevelClass.addAnnotation(this.getActualNamedAnnotation(topLevelClass, this.getActualBeanId(introspectedTable)));
				}
				
				if(this._addSingletonAnnotation) {
					topLevelClass.addAnnotation(this.getActualSingletonAnnotation(topLevelClass));
				}
				
				topLevelClass.addImportedType(_baseClass);
				topLevelClass.setSuperClass(_baseClass);
				
				if(this._autowireBySetterMethod) {
					for(Method method : topLevelClass.getMethods())
						if(method.getName().equals(this._sqlMapClientSetterMethodName)) { 
							method.addAnnotation(this.getActualInjectAnnotation(topLevelClass)); 
							
							if(this._addAutowiredSqlMapClientBeanIdAnnotation)
								method.addAnnotation(this.getActualNamedAnnotation(topLevelClass, this.getSanitizedBeanId(this._sqlMapClientBeanId)));
						}
				}
				
				if(this._autowireByField) {
					for(Field method : topLevelClass.getFields())
						if(method.getName().equals(this._sqlMapClientFieldName)) { 
							method.addAnnotation(this.getActualInjectAnnotation(topLevelClass)); 
							
							if(this._addAutowiredSqlMapClientBeanIdAnnotation)
								method.addAnnotation(this.getActualNamedAnnotation(topLevelClass, this.getSanitizedBeanId(this._sqlMapClientBeanId))); 
						}
				} 
				
				return true;
			} else {
				this._log.warn("Current client is not a GENERIC-SI client");
			}
		} else {
			this._log.warn("Current client is not an iBATIS2 client");
		}
		
		return false;
	}
}