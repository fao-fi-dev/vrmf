/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business-core)
 */
package org.fao.fi.vrmf.business.core.utilities.extensions.maps.impl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Arrays;

import org.fao.fi.sh.utility.caching.spi.CacheFacade;
import org.fao.fi.vrmf.common.legacy.extensions.maps.LazySerializableMap;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Dec 2010
 */
abstract public class LazySerializableDataAccessMap<KEY extends Serializable, VALUE extends Serializable> extends LazySerializableMap<KEY, VALUE> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1084114772127508185L;
	
	protected String _sqlMapClientConfigurationResource;
	protected String[] _sourceSystems;
	
	/**
	 * Class constructor
	 */
	protected LazySerializableDataAccessMap() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param sqlMapClientConfigurationResource
	 * @throws Throwable
	 */
	public LazySerializableDataAccessMap(CacheFacade<KEY, VALUE> backingCache, String sqlMapClientConfigurationResource) throws Throwable {
		super(backingCache);
		
		this._sqlMapClientConfigurationResource = sqlMapClientConfigurationResource;
		
		this.initializeDAO();
	}
	
	/**
	 * Class constructor
	 *
	 * @param sqlMapClientConfigurationResource
	 * @throws Throwable
	 */
	public LazySerializableDataAccessMap(String sqlMapClientConfigurationResource) throws Throwable {
		super();
		
		this._sqlMapClientConfigurationResource = sqlMapClientConfigurationResource;
		
		this.initializeDAO();
	}
	
	/**
	 * Class constructor
	 *
	 * @param sqlMapClientConfigurationResource
	 * @param sourceSystems
	 * @throws Throwable
	 */
	public LazySerializableDataAccessMap(String sqlMapClientConfigurationResource, String[] sourceSystems) throws Throwable {
		this(sqlMapClientConfigurationResource);

		this._sourceSystems = sourceSystems == null ? null : Arrays.copyOf(sourceSystems, sourceSystems.length);
	}

	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param sqlMapClientConfigurationResource
	 * @param sourceSystems
	 * @throws Throwable
	 */
	public LazySerializableDataAccessMap(CacheFacade<KEY, VALUE> backingCache, String sqlMapClientConfigurationResource, String[] sourceSystems) throws Throwable {
		this(backingCache, sqlMapClientConfigurationResource);

		this._sourceSystems = sourceSystems == null ? null : Arrays.copyOf(sourceSystems, sourceSystems.length);
	}
	
	abstract protected void initializeDAO() throws Throwable;
	
	/**
	 * A custom 'readObject' method that re-initializes the transient log on object deserialization
	 * 
	 * @param inputStream
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	private void readObject(ObjectInputStream inputStream) throws ClassNotFoundException, IOException {
		inputStream.defaultReadObject();

		try {
			this.initializeDAO();
		} catch (Throwable t) {
			throw new IOException("Unable to read serialized object and recostruct its non-serializable fields", t);
		}
	}
}