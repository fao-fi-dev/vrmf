/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business-core)
 */
package org.fao.fi.vrmf.business.core.utilities.extensions.collections.impl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Arrays;

import org.fao.fi.sh.utility.caching.spi.CacheFacade;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.SqlMapClientBuilderUtilities;
import org.fao.fi.vrmf.common.legacy.extensions.collections.LazySerializableCollection;

import com.ibatis.sqlmap.client.SqlMapClient;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Dec 2010
 */
abstract public class LazySerializableDataAccessCollection<ID extends Serializable, ENTRY extends Serializable> extends LazySerializableCollection<ID, ENTRY> {	
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8782085919754064152L;
		
	protected transient SqlMapClient _sqlMapClient;	
	protected String _sqlMapClientConfigurationResource;
	
	protected String[] _sourceSystems;

	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param sqlMapClientConfigurationResource
	 * @throws Throwable
	 */
	public LazySerializableDataAccessCollection(CacheFacade<ID, ENTRY> backingCache, String sqlMapClientConfigurationResource) throws Throwable {
		super(backingCache);
		
		this._sqlMapClientConfigurationResource = sqlMapClientConfigurationResource;
		this._sqlMapClient = this.getSqlMapClient();
		
		this.initializeDAOs();
	}
	
	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param providedSqlMapClient
	 * @throws Throwable
	 */
	public LazySerializableDataAccessCollection(CacheFacade<ID, ENTRY> backingCache, SqlMapClient providedSqlMapClient) throws Throwable {
		super(backingCache);
		
		this._sqlMapClientConfigurationResource = null;
		this._sqlMapClient = providedSqlMapClient;
		
		this.initializeDAOs(this._sqlMapClient);
	}
	
	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param sqlMapClientConfigurationResource
	 * @param sourceSystems
	 * @throws Throwable
	 */
	public LazySerializableDataAccessCollection(CacheFacade<ID, ENTRY> backingCache, String sqlMapClientConfigurationResource, String[] sourceSystems) throws Throwable {
		this(backingCache, sqlMapClientConfigurationResource);

		this._sourceSystems = sourceSystems == null ? null : Arrays.copyOf(sourceSystems, sourceSystems.length);
	}
	
	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param sqlMapClientConfigurationResource
	 * @param sourceSystems
	 * @throws Throwable
	 */
	public LazySerializableDataAccessCollection(CacheFacade<ID, ENTRY> backingCache, SqlMapClient providedSqlMapClient, String[] sourceSystems) throws Throwable {
		this(backingCache, providedSqlMapClient);

		this._sourceSystems = sourceSystems == null ? null : Arrays.copyOf(sourceSystems, sourceSystems.length);
	}
	
	protected SqlMapClient getSqlMapClient() throws Throwable {
		if(this._sqlMapClient == null)
			if(this._sqlMapClientConfigurationResource != null)
				this._sqlMapClient = SqlMapClientBuilderUtilities.buildSqlMapClient(this._sqlMapClientConfigurationResource); 
			
		return this._sqlMapClient;
	}
	
	/**
	 * Initializes the underlying DAOs that provide access to persistant data
	 * 
	 * @throws Throwable
	 */
	abstract protected void initializeDAOs() throws Throwable;
	
	/**
	 * @param providedSqlMapClient
	 * @throws Throwable
	 */
	abstract protected void initializeDAOs(SqlMapClient providedSqlMapClient) throws Throwable;
	
	/**
	 * A custom 'readObject' method that re-initializes the transient log on object deserialization
	 * 
	 * @param inputStream
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	private void readObject(ObjectInputStream inputStream) throws ClassNotFoundException, IOException {
		inputStream.defaultReadObject();

		try {
			this.initializeDAOs();
		} catch (Throwable t) {
			throw new IOException("Unable to read serialized object and recostruct its non-serializable fields", t);
		}
	}
}