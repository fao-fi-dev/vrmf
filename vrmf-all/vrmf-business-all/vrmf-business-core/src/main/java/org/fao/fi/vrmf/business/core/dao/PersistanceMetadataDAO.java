/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business-core)
 */
package org.fao.fi.vrmf.business.core.dao;

import java.io.Serializable;
import java.sql.SQLException;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 Nov 2012
 */
public interface PersistanceMetadataDAO<I extends Serializable> {
	I getLastInsertedID() throws SQLException;
	I getLastInsertedID(String IBATISNamespace) throws SQLException;
}
