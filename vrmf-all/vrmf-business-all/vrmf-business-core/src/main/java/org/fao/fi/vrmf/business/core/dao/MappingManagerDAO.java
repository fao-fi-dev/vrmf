/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business-core)
 */
package org.fao.fi.vrmf.business.core.dao;

import java.io.Serializable;

import org.fao.fi.sh.model.core.spi.Mapped;
import org.fao.fi.sh.model.core.spi.Updatable;
import org.fao.fi.vrmf.business.core.dao.ibatis.SetterInjectionIBATISDAO;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Jul 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Jul 2011
 */
public interface MappingManagerDAO<IDENTIFIER, MAPPED_DATA extends Mapped<IDENTIFIER, IDENTIFIER> & Updatable> extends SetterInjectionIBATISDAO, Serializable {
	IDENTIFIER linkEntities(MAPPED_DATA source, MAPPED_DATA target, Double mappingWeight, String mappingOwner, String mappingComment) throws Throwable;
	IDENTIFIER linkEntitiesByID(IDENTIFIER sourceID, IDENTIFIER targetID, Double mappingWeight, String mappingOwner, String mappingComment) throws Throwable;
}
