/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business-core)
 */
package org.fao.fi.vrmf.business.core.dao.ibatis.impl;

import java.sql.Connection;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.business.core.dao.AbstractDAO;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;

import com.ibatis.sqlmap.client.SqlMapClient;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Mar 2011
 */
@NoArgsConstructor
@Named @Singleton
abstract public class AbstractIBATISDAO extends AbstractDAO {
	/** To be used as a reference for autowiring the SqlMapClient bean via constructor injection */
	final static public String VRMF_DEFAULT_SQL_MAP_CLIENT_BEAN_ID = "vrmf.sqlMapClient";

	protected @Getter SqlMapClient sqlMapClient;
	
	final protected void logStatus() {
		if(this.sqlMapClient != null) {
			this._log.debug("Current thread: " + Thread.currentThread().getName());
			this._log.debug("Current client: " + this.sqlMapClient);

			try {
				Connection c = this.sqlMapClient.getCurrentConnection();
				
				if(c == null) {
					this._log.info("Current connection is unavailable... Trying with user connection...");
					
					c = this.sqlMapClient.getCurrentConnection();
				}
				
				if(c != null) {
					this._log.debug("Current connection: " + c);
					this._log.debug("Autocommit status : " + c.getAutoCommit());
					this._log.debug("Tx isolation      : " + c.getTransactionIsolation());
				} else {
					this._log.warn("No connection currently available");
				}
			} catch (Throwable t) {
				this._log.warn("Unable to retrieve current connection", t);
			}
		} else {
			this._log.warn("No client currently available");
		}
	}
	
	final protected String getIBATISNamespace(String defaultNamespace) {
		String currentNamespace = defaultNamespace;
		
		if(StringsHelper.trim(currentNamespace) == null) {
			if(ObjectsHelper.isAnnotationPresent(this.getClass(), IBATISNamespace.class))
				currentNamespace = ObjectsHelper.getFirstAnnotationOfType(this.getClass(), IBATISNamespace.class).defaultNamespacePrefix();
		}
						
		currentNamespace = StringsHelper.trim(currentNamespace);
		
		return currentNamespace;
	}
	
	final protected String getIBATISNamespace() {
		return this.getIBATISNamespace(null);
	}
	
	final protected String getQueryIDForNamespace(String namespace, String queryID) {
		String currentNamespace = StringsHelper.trim(namespace);
		
		if(currentNamespace == null)
			currentNamespace = this.getIBATISNamespace();
		
		return currentNamespace == null ? queryID : currentNamespace + "." + queryID;
	}
	
	final protected String getQueryIDForCurrentNamespace(String queryID) {
		return this.getQueryIDForNamespace(null, queryID);
	}
}