/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business-core)
 */
package org.fao.fi.vrmf.business.core.dao.ibatis.impl;

import java.io.Serializable;
import java.sql.SQLException;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.business.core.dao.PersistanceMetadataDAO;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;

import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 Nov 2012
 */
@NoArgsConstructor
@Named("dao.meta.persistance") @Singleton
@IBATISNamespace(defaultNamespacePrefix="persistanceMetadata")
public class PersistanceMetadataDAOImpl<I extends Serializable> extends AbstractSetterInjectionIBATISDAO implements PersistanceMetadataDAO<I> {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.PersistanceMetadataDAO#getLastInsertedID()
	 */
	@Override
	public I getLastInsertedID() throws SQLException {
		return this.getLastInsertedID(this.getIBATISNamespace());
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.PersistanceMetadataDAO#getLastInsertedID(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public I getLastInsertedID(String IBATISNamespace) throws SQLException {
		return (I)this.sqlMapClient.queryForObject(this.getQueryIDForNamespace(IBATISNamespace, "getLastInsertedID"));
	}
}