/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.helpers.countries;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.topology.WeightedGraph;
import org.fao.fi.sh.utility.topology.helpers.TransitiveMappedDataManager;
import org.fao.fi.vrmf.business.dao.generated.SCountriesDAO;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SCountriesExample;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class CountriesMappingManager extends TransitiveMappedDataManager<Integer, SCountries> {
	@Inject @Named("dao.sCountries") private @Getter @Setter SCountriesDAO countriesDAO;
	
	/**
	 * Class constructor
	 *
	 * @param graph
	 */
	public CountriesMappingManager(WeightedGraph<Integer> graph) {
		super(graph);
	}
	
	/**
	 * Class constructor
	 *
	 * @param data
	 */
	public CountriesMappingManager(Collection<SCountries> data) {
		super(data);
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@PostConstruct
	public void afterPropertiesSet() throws Exception {
		SCountriesExample cFilter = new SCountriesExample();
		
		this._log.info("Initializing countries mapping manager...");
		
		try {
			for(SCountries country : this.countriesDAO.selectByExample(cFilter)) {
				this.addMappedData(country);
			}
			
			this._log.info("Checking cycles...");
			
			if(this.getGraph().hasCycles(this._detector))
				throw new RuntimeException("Cycles detected in mapped countries data: " + this.getGraph().getCycles(this._detector));
			
			this._log.info("No cycles detected");
			
			this._log.info("Countries mapping manager has been initialized!");
		} catch (Throwable t) {
			throw new RuntimeException("Unable to initialize the countries mapping manager", t);
		}
	}
}
