/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.vrmf.business.dao.VesselExternalMarkingsAutocompletionDAO;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.ExternalMarkingSearchResult;
import org.springframework.cache.annotation.Cacheable;

import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class VesselExternalMarkingsAutocompletionDAOImpl extends AbstractAutocompletionDAOImpl implements VesselExternalMarkingsAutocompletionDAO {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.generic.AbstractAutocompletionDAOImpl#getProcedureName(boolean)
	 */
	@Override
	protected String getProcedureName(boolean authorizedVesselsOnly) {
		return "EXT_MARK_SEARCH" + ( authorizedVesselsOnly ? "_BY_AUTH" : "" );
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselMMSIAutocompletionDAO#searchVesselMMSI(java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "#externalMarking + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID")
	public Collection<ExternalMarkingSearchResult> searchVesselExternalMarkings(String externalMarking, Date atDate, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		return this.searchVesselExternalMarkings(this.getProcedureName(false), externalMarking, atDate, limitToMaxItems, sourceSystems, groupByUID);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselMMSIAutocompletionDAO#searchVesselMMSI(java.lang.String, java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "#procedureName + " +
															  "#externalMarking + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID")
	public Collection<ExternalMarkingSearchResult> searchVesselExternalMarkings(String procedureName, String externalMarking, Date atDate, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		long end, start = System.currentTimeMillis();
		
		this._log.info("Autocompleting external markings for '" + externalMarking + "' (procedure name: " + procedureName + ", sources: " + CollectionsHelper.serializeArray(sourceSystems) + ", at date: " + atDate + ", group by UID: " + groupByUID + ")");
		
		Connection c = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {			
			String CALL_TEMPLATE = "CALL " + ( procedureName == null ? this.getProcedureName(false) : procedureName ) + "(?, ?, ?, ?)";
			
			c = this.sqlMapClient.getDataSource().getConnection();
			
			cs = c.prepareCall(CALL_TEMPLATE);
			
			int counter = 1;
					
			this.setUnicodeString(cs, counter++, externalMarking);
			
			if(sourceSystems != null && sourceSystems.length > 0)
				cs.setString(counter++, "@" + CollectionsHelper.join(sourceSystems, "@") + "@");
			else 
				cs.setNull(counter++, Types.VARCHAR);
			
			if(atDate != null)
				cs.setDate(counter++, new java.sql.Date(atDate.getTime()));
			else
				cs.setNull(counter++, Types.DATE);
	
			cs.setBoolean(counter++, groupByUID);		
			
			List<ExternalMarkingSearchResult> vesselExternalMarkingData = new ArrayList<ExternalMarkingSearchResult>();
			
			if(cs.execute()) {
				rs = cs.getResultSet();
								
				ExternalMarkingSearchResult current = null;
				
				while(rs.next()) {
					current = new ExternalMarkingSearchResult();
					
					current.setExternalMarking(rs.getString("EXTERNAL_MARKING"));
					current.setOccurrencies(rs.getInt("OCCURRENCIES"));
					current.setTotalOccurrencies(rs.getInt("TOTAL_OCCURRENCIES"));
					current.setGroupedOccurrencies(rs.getInt("GROUPED_OCCURRENCIES"));
					
					vesselExternalMarkingData.add(current);
				}
			}
			
			return this.trim(vesselExternalMarkingData, limitToMaxItems);
		} catch (Throwable t) {
			this._log.error("Unable to find vessel External Marking", t);
			
			return new ArrayList<ExternalMarkingSearchResult>();
		} finally {
			this.cleanup(rs, cs, c);
			
			end = System.currentTimeMillis();
			
			this._log.info("Autocompleting external markings for '" + externalMarking + "' took " + ( end - start ) + " mSec.");
		}
	}
}