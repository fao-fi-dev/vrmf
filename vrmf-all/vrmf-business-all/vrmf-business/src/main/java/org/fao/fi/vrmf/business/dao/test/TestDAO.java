/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.test;

import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.SqlMapClientBuilderUtilities;

import com.ibatis.sqlmap.client.SqlMapClient;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Jul 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Jul 2011
 */
public class TestDAO {
	@SuppressWarnings("unused")
	private SqlMapClient _client;
	
	public TestDAO() throws Throwable {
		this._client = SqlMapClientBuilderUtilities.buildSqlMapClient("org/fao/vrmf/business/resources/test/sqlmap-config.xml");
	}
}
