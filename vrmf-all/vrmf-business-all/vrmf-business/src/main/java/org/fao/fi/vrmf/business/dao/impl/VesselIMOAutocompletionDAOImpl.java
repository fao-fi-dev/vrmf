/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.vrmf.business.dao.VesselIMOAutocompletionDAO;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.IdentifierSearchResult;
import org.springframework.cache.annotation.Cacheable;

import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class VesselIMOAutocompletionDAOImpl extends AbstractVesselIdentifierAutocompletionDAOImpl implements VesselIMOAutocompletionDAO {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.generic.AbstractAutocompletionDAOImpl#getProcedureName(boolean)
	 */
	public String getProcedureName(boolean authorizedOnly) {
		return "IDENTIFIER_SEARCH";
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselIMOAutocompletionDAO#searchVesselIMO(java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "#IMO + " +
															  "#limitToMaxItems + " +
															  "#groupByUID")
	public Collection<IdentifierSearchResult> searchVesselIMO(String IMO, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		return this.searchVesselIMO(this.getProcedureName(false), IMO, limitToMaxItems, sourceSystems, groupByUID);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselIRCSAutocompletionDAO#searchVesselIRCS(java.lang.String, java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "#procedureName + " + 
															  "#IMO + " +
															  "#limitToMaxItems + " +
															  "#groupByUID")
	public Collection<IdentifierSearchResult> searchVesselIMO(String procedureName, String IMO, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		Connection c = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {			
			String CALL_TEMPLATE = "CALL " + ( procedureName == null ? this.getProcedureName(false) : procedureName ) + "(?, ?, ?, ?)";
			
			c = this.sqlMapClient.getDataSource().getConnection();
			
			cs = c.prepareCall(CALL_TEMPLATE);
			
			int counter = 1;
					
			cs.setString(counter++, "IMO");
			cs.setString(counter++, IMO);
			
			if(sourceSystems != null && sourceSystems.length > 0)
				cs.setString(counter++, "@" + CollectionsHelper.join(sourceSystems, "@") + "@");
			else 
				cs.setNull(counter++, Types.VARCHAR);
		
			cs.setBoolean(counter++, groupByUID);			
//			cs.setBoolean(counter++, authorizedOnly);
//			
//			if(authorizationSource != null)
//				cs.setString(counter++, authorizationSource);
//			else
//				cs.setNull(counter++, Types.VARCHAR);
			
			List<IdentifierSearchResult> vesselIMOData = null;
			
			if(cs.execute()) {
				rs = cs.getResultSet();
				
				vesselIMOData = this.buildSearchResults(rs);
			}
			
			return this.trim(vesselIMOData, limitToMaxItems);
		} catch (Throwable t) {
			this._log.error("Unable to find vessel IMO", t);
			
			return new ArrayList<IdentifierSearchResult>();
		} finally {
			this.cleanup(rs, cs, c);
		}
	}
}