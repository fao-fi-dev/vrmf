/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.vrmf.business.dao.VesselIdentifierAutocompletionDAO;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.IdentifierSearchResult;
import org.springframework.cache.annotation.Cacheable;

import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class VesselIdentifierAutocompletionDAOImpl extends AbstractVesselIdentifierAutocompletionDAOImpl implements VesselIdentifierAutocompletionDAO {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.generic.AbstractAutocompletionDAOImpl#getProcedureName(boolean)
	 */
	public String getProcedureName(boolean authorizedOnly) {
		return "DEFAULT_IDENTIFIER_SEARCH"; //Procedure doesn't currently change if searching for authorized vs non-necesarily authorized vessels...
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselIdentifierAutocompletionDAO#searchVesselIdentifier(java.lang.String, java.lang.String, java.lang.Integer, java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "#identifierType + " +
															  "#identifier + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID")
	public Collection<IdentifierSearchResult> searchVesselIdentifier(String identifierType, String identifier, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		return this.searchVesselIdentifier(this.getProcedureName(false), identifierType, identifier, limitToMaxItems, sourceSystems, groupByUID);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselIdentifierAutocompletionDAO#searchVesselIdentifier(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "#procedureName + " +
															  "#identifierType + " +
															  "#identifier + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID")
	public Collection<IdentifierSearchResult> searchVesselIdentifier(String procedureName, String identifierType, String identifier, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) {
		Connection c = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {			
			String CALL_TEMPLATE = "CALL " + ( procedureName == null ? this.getProcedureName(false) : procedureName ) + "(?, ?, ?, ?)";
			
			c = this.sqlMapClient.getDataSource().getConnection();
			
			cs = c.prepareCall(CALL_TEMPLATE);
			
			int counter = 1;
					
			cs.setString(counter++, identifierType);
			cs.setString(counter++, identifier);
			
			if(sourceSystems != null && sourceSystems.length > 0)
				cs.setString(counter++, "@" + CollectionsHelper.join(sourceSystems, "@") + "@");
			else 
				cs.setNull(counter++, Types.VARCHAR);
		
			cs.setBoolean(counter++, groupByUID);			
			
			List<IdentifierSearchResult> vesselIMOData = null;
			
			if(cs.execute()) {
				rs = cs.getResultSet();
				
				vesselIMOData = this.buildSearchResults(rs);
			}
			
			return this.trim(vesselIMOData, limitToMaxItems);
		} catch (Throwable t) {
			this._log.error("Unable to find vessel identifier", t);
			
			return new ArrayList<IdentifierSearchResult>();
		} finally {
			this.cleanup(rs, cs, c);
		}
	}
}