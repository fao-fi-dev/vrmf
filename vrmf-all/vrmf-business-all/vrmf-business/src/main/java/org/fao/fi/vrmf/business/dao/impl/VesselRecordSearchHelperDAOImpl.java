/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;
import org.fao.fi.vrmf.business.dao.generated.AuthorizationsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToInspectionsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToIuuListDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToPortEntryDenialsDAO;
import org.fao.fi.vrmf.common.models.extended.ExtendedAuthorizations;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToInspection;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToIuuList;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToPortEntryDenial;
import org.fao.fi.vrmf.common.models.generated.AuthorizationsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToInspectionsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToIuuListExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToPortEntryDenialsExample;
import org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonClauses;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.springframework.cache.annotation.Cacheable;

import com.ibatis.sqlmap.client.SqlMapClient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Feb 2013
 */
@NoArgsConstructor
@Named("dao.vessel.search.helper")
@IBATISNamespace(defaultNamespacePrefix="vesselsSearch")
public class VesselRecordSearchHelperDAOImpl extends AbstractVesselRecordHelperDAOImpl {
	static final private String DEFAULT_FILTER_INVOCATION_CACHE_ID		 = "vrmf.business.vessels.search.filter";
	static final private String DEFAULT_GET_DATA_INVOCATION_CACHE_ID 	 = "vrmf.business.vessels.search.get.data";
	static final private String DEFAULT_GET_METADATA_INVOCATION_CACHE_ID = "vrmf.business.vessels.search.get.metadata";

	@Inject private @Getter @Setter AuthorizationsDAO authorizations;
	@Inject private @Getter @Setter VesselsToInspectionsDAO inspections;
	@Inject private @Getter @Setter VesselsToPortEntryDenialsDAO portEntryDenials;
	@Inject private @Getter @Setter VesselsToIuuListDAO iuuListings;

	@SuppressWarnings("unchecked")
	@Cacheable(key="#root.target.buildFilterVesselsKey(#filter)", value=DEFAULT_FILTER_INVOCATION_CACHE_ID)
	public List<VesselRecord> filterVessels(SqlMapClient client, String namespace, VesselRecordSearchFilter filter) throws Throwable {
		filter.switchToDataSelection();

		VesselRecordSearchFilter internalFilter = SerializationHelper.clone(filter);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("filter", internalFilter);

		long start = System.currentTimeMillis();

		String xml, key = (this.computeFilterKey(xml = this.filterToXML(internalFilter)));

		try {
			this._queryLogger.debug("Executing query [ key: {} ] {}", key, xml);

			return client.queryForList(this.getQueryIDForNamespace(namespace, "select"), parameters);
		} finally {
			this.trackQueryExecution(xml, key, start);
		}
	}

	@SuppressWarnings("unchecked")
	@Cacheable(key="#root.target.buildGetVesselsDataKey(#filter, #vesselIDs)", value=DEFAULT_GET_DATA_INVOCATION_CACHE_ID)
	public List<VesselRecord> getVesselsData(SqlMapClient client, String namespace, VesselRecordSearchFilter filter, Collection<Integer> vesselIDs) throws Throwable {
		filter.switchToMetadataRetrievement();

		VesselRecordSearchFilter internalFilter = SerializationHelper.clone(filter);

		internalFilter.clauses().and(CommonClauses.VRMF_IDS, vesselIDs == null || vesselIDs.isEmpty() ? null : vesselIDs.toArray(new Integer[vesselIDs.size()]));

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("filter", internalFilter);

		long start = System.currentTimeMillis();

		String xml, key = (this.computeFilterKey(xml = this.filterToXML(internalFilter)));

		try {
			this._queryLogger.debug("Executing query [ key: {} ] {}", key, xml);

			List<VesselRecord> data = client.queryForList(this.getQueryIDForNamespace(namespace, "getMetadata"), parameters);
			
			List<String> sourcesList = filter.getAvailableSourceSystems() == null || filter.getAvailableSourceSystems().length == 0 ? null : Arrays.asList(filter.getAvailableSourceSystems());
			
			List<Integer> vIDs = new ArrayList<Integer>();
			for(VesselRecord in : data) {
				vIDs.add(in.getId());
			}
			
			//Adding full authorizations (filtered by available sources if the client has explicitly requested so)
			if(filter.includeAuthorizations()) {
				AuthorizationsExample example = new AuthorizationsExample();
				AuthorizationsExample.Criteria criteria = example.createCriteria();
				
				criteria.andVesselIdIn(vIDs);
				
				if(sourcesList != null)
					criteria.andSourceSystemIn(sourcesList);
				
				example.setOrderByClause("VESSEL_ID, REFERENCE_DATE DESC, VALID_FROM DESC, UPDATE_DATE DESC");
				
				List<ExtendedAuthorizations> allAuths = decorator.buildAuthorizations(authorizations.selectByExample(example));
				
				Map<Integer, List<ExtendedAuthorizations>> byId = new HashMap<Integer, List<ExtendedAuthorizations>>();
				
				List<ExtendedAuthorizations> existing = null;

				for(ExtendedAuthorizations in : allAuths) {
					existing = byId.get(in.getVesselId()); 
					
					if(existing == null) {
						existing = new ArrayList<ExtendedAuthorizations>();
						byId.put(in.getVesselId(), existing);
					}
					
					existing.add(in);
				}
						
				for(VesselRecord in : data) {
					in.setAuthorizations(byId.get(in.getId()));
				}
			}
			
			//Adding full inspections (filtered by available sources if the client has explicitly requested so)
			if(filter.includeInspections()) {
				VesselsToInspectionsExample example = new VesselsToInspectionsExample();
				VesselsToInspectionsExample.Criteria criteria = example.createCriteria();
				
				criteria.andVesselIdIn(vIDs);
				
				if(sourcesList != null)
					criteria.andSourceSystemIn(sourcesList);
				
				example.setOrderByClause("DATE DESC, REFERENCE_DATE DESC, UPDATE_DATE DESC");
				
				List<ExtendedVesselToInspection> allInspections = decorator.buildInspections(inspections.selectByExample(example));
				
				Map<Integer, List<ExtendedVesselToInspection>> byId = new HashMap<Integer, List<ExtendedVesselToInspection>>();
				
				List<ExtendedVesselToInspection> existing = null;

				for(ExtendedVesselToInspection in : allInspections) {
					existing = byId.get(in.getVesselId()); 
					
					if(existing == null) {
						existing = new ArrayList<ExtendedVesselToInspection>();
						byId.put(in.getVesselId(), existing);
					}
					
					existing.add(in);
				}
						
				for(VesselRecord in : data) {
					in.setInspections(byId.get(in.getId()));
				}
			}
			
			//Adding full IUU listings (filtered by available sources if the client has explicitly requested so)
			if(filter.includeIUUListings()) {
				VesselsToIuuListExample example = new VesselsToIuuListExample();
				VesselsToIuuListExample.Criteria criteria = example.createCriteria();
				
				criteria.andVesselIdIn(vIDs);
				
				if(sourcesList != null)
					criteria.andSourceSystemIn(sourcesList);
				
				example.setOrderByClause("LISTING_DATE DESC, DELISTING_DATE DESC, REFERENCE_DATE DESC, UPDATE_DATE DESC");
				
				List<ExtendedVesselToIuuList> allIUUListings = decorator.buildIuuLists(iuuListings.selectByExample(example));
				
				Map<Integer, List<ExtendedVesselToIuuList>> byId = new HashMap<Integer, List<ExtendedVesselToIuuList>>();
				
				List<ExtendedVesselToIuuList> existing = null;

				for(ExtendedVesselToIuuList in : allIUUListings) {
					existing = byId.get(in.getVesselId()); 
					
					if(existing == null) {
						existing = new ArrayList<ExtendedVesselToIuuList>();
						byId.put(in.getVesselId(), existing);
					}
					
					existing.add(in);
				}
						
				for(VesselRecord in : data) {
					in.setIuuListings(byId.get(in.getId()));
				}
			}
			
			//Adding full port denials (filtered by available sources if the client has explicitly requested so)
			if(filter.includePortDenials()) {
				VesselsToPortEntryDenialsExample example = new VesselsToPortEntryDenialsExample();
				VesselsToPortEntryDenialsExample.Criteria criteria = example.createCriteria();
				
				criteria.andVesselIdIn(vIDs);
				
				if(sourcesList != null)
					criteria.andSourceSystemIn(sourcesList);
				
				example.setOrderByClause("DATE DESC, REFERENCE_DATE DESC, UPDATE_DATE DESC");
				
				List<ExtendedVesselToPortEntryDenial> allDenials = decorator.buildPortEntryDenials(portEntryDenials.selectByExample(example));
				
				Map<Integer, List<ExtendedVesselToPortEntryDenial>> byId = new HashMap<Integer, List<ExtendedVesselToPortEntryDenial>>();
				
				List<ExtendedVesselToPortEntryDenial> existing = null;

				for(ExtendedVesselToPortEntryDenial in : allDenials) {
					existing = byId.get(in.getVesselId()); 
					
					if(existing == null) {
						existing = new ArrayList<ExtendedVesselToPortEntryDenial>();
						byId.put(in.getVesselId(), existing);
					}
					
					existing.add(in);
				}
						
				for(VesselRecord in : data) {
					in.setPortDenials(byId.get(in.getId()));
				}
			}
			
			return data;
		} finally {
			this.trackQueryExecution(xml, key, start);
		}
	}

	@SuppressWarnings("unchecked")
	@Cacheable(key="#root.target.buildGetVesselsMetadataKey(#filter, #vesselIDs)", value=DEFAULT_GET_METADATA_INVOCATION_CACHE_ID)
	public List<VesselRecord> getVesselsMetadata(SqlMapClient client, String namespace, VesselRecordSearchFilter filter, Collection<Integer> vesselIDs) throws Throwable {
		filter.switchToAdditionalMetadataRetrievement();

		VesselRecordSearchFilter internalFilter = SerializationHelper.clone(filter);
		internalFilter.clauses().and(CommonClauses.VRMF_IDS, vesselIDs == null || vesselIDs.isEmpty() ? null : vesselIDs.toArray(new Integer[vesselIDs.size()]));

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("filter", internalFilter);

		long start = System.currentTimeMillis();

		String xml, key = (this.computeFilterKey(xml = this.filterToXML(internalFilter)));

		try {
			this._queryLogger.debug("Executing query [ key: {} ] {}", key, xml);

			return client.queryForList(this.getQueryIDForNamespace(namespace, "getAdditionalDataBy" + filter.getGroupBy()), parameters);
		} finally {
			this.trackQueryExecution(xml, key, start);
		}
	}
}