/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.common.helpers.singletons.text.StringDistanceHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.business.dao.VesselTypeFuzzyAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.generated.SVesselTypesDAO;
import org.fao.fi.vrmf.business.helpers.vessel.types.VesselTypesMappingManager;
import org.fao.fi.vrmf.common.models.generated.SVesselTypes;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.FuzzyVesselTypeSearchResult;
import org.springframework.cache.annotation.Cacheable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class VesselTypeFuzzyAutocompletionDAOImpl extends AbstractAutocompletionDAOImpl implements VesselTypeFuzzyAutocompletionDAO {
	@Inject private @Getter @Setter SVesselTypesDAO vesselTypesDAO;
	
	@Inject private @Getter @Setter VesselTypesMappingManager vesselTypesMappingManager;

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.generic.AbstractAutocompletionDAOImpl#getProcedureName(boolean)
	 */
	public String getProcedureName(boolean authorizedOnly) {
		return "FUZZY_VESSEL_TYPE_SEARCH" + ( authorizedOnly ? "_BY_AUTH" : "");
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselTypeFuzzyAutocompletionDAO#searchVesselTypes(java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.String[], java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#availableSources) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#publicSourceSystems) + " +
															  "#vesselTypeName + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID + " +
															  "#groupMappedTypes")	
	public Collection<FuzzyVesselTypeSearchResult> searchVesselTypes(String vesselTypeName, Date atDate, Integer limitToMaxItems, String[] availableSources, String[] sourceSystems, String[] publicSourceSystems, Boolean groupByUID, boolean groupMappedTypes) throws Throwable {
		return this.searchVesselTypes(this.getProcedureName(false), vesselTypeName, atDate, limitToMaxItems, availableSources, sourceSystems, publicSourceSystems, groupByUID, groupMappedTypes);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselTypeFuzzyAutocompletionDAO#searchVesselTypes(java.lang.String, java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.String[], java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#availableSources) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#publicSourceSystems) + " +
															  "#procedureName + " +
															  "#vesselTypeName + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID + " +
															  "#groupMappedTypes")
	public Collection<FuzzyVesselTypeSearchResult> searchVesselTypes(String procedureName, String vesselTypeName, Date atDate, Integer limitToMaxItems, String[] availableSources, String[] sourceSystems, String[] publicSourceSystems, Boolean groupByUID, boolean groupMappedTypes) throws Throwable {
		long end, start = System.currentTimeMillis();

		this._log.info("Autocompleting vessel type for '" + vesselTypeName + "' (procedure name: " + procedureName + ", sources: " + CollectionsHelper.serializeArray(sourceSystems) + ", at date: " + atDate + ", group by UID: " + groupByUID + ")");

		Set<String> publicSourceSystemsSet = new HashSet<String>(Arrays.asList(availableSources));
		publicSourceSystemsSet.addAll(Arrays.asList(publicSourceSystems));
		
		Set<String> sourceSystemsSet = new HashSet<String>();
		
		if(sourceSystems != null)
			sourceSystemsSet.addAll(Arrays.asList(sourceSystems));
		
		sourceSystemsSet.addAll(publicSourceSystemsSet);
		
		Connection c = null;
		CallableStatement cs = null;
		ResultSet rs = null;
			
		try {			
			String CALL_TEMPLATE = "CALL " + ( procedureName == null ? this.getProcedureName(false) : procedureName ) + "(?, ?)";
						
			c = this.sqlMapClient.getDataSource().getConnection();
			
			cs = c.prepareCall(CALL_TEMPLATE);
			
			int counter = 1;
			
			this.setUnicodeString(cs, counter++, vesselTypeName);
			
			if(sourceSystemsSet.isEmpty())
				cs.setString(counter++, "@" + CollectionsHelper.join(publicSourceSystemsSet, "@") + "@");
			else
				cs.setString(counter++, "@" + CollectionsHelper.join(sourceSystemsSet, "@") + "@");

			List<FuzzyVesselTypeSearchResult> returnedTypesData = new ArrayList<FuzzyVesselTypeSearchResult>();
			List<FuzzyVesselTypeSearchResult> updatedTypesData = new ArrayList<FuzzyVesselTypeSearchResult>();
			Map<Integer, FuzzyVesselTypeSearchResult> returnedTypesDataMap = new HashMap<Integer, FuzzyVesselTypeSearchResult>();
			List<Integer> returnedTypesDataId = new ListSet<Integer>();
					
			if(cs.execute()) {
				rs = cs.getResultSet();
					
				String countryIDAsString = null;
				FuzzyVesselTypeSearchResult current = null;
						
				while(rs.next()) {
					current = new FuzzyVesselTypeSearchResult();
										
					current.setId(rs.getInt("ID"));
					current.setOriginalVesselTypeId(rs.getString("ORIGINAL_VESSEL_TYPE_ID"));
					
					countryIDAsString = rs.getString("COUNTRY_ID");
					
					if(countryIDAsString != null)
						current.setCountryId(Integer.parseInt(countryIDAsString));
					else
						current.setCountryId(null);
					
					current.setName(rs.getString("NAME"));
					current.setStandardAbbreviation(rs.getString("STANDARD_ABBREVIATION"));
					current.setSourceSystem(rs.getString("SOURCE_SYSTEM"));
					
					current.setScore(this.computeWeight(current, vesselTypeName));
					
					returnedTypesData.add(current);
					returnedTypesDataMap.put(current.getId(), current);
					returnedTypesDataId.add(current.getId());
				}
			}
			
			Collections.sort(returnedTypesData, new Comparator<FuzzyVesselTypeSearchResult>() {
				public int compare(FuzzyVesselTypeSearchResult o1, FuzzyVesselTypeSearchResult o2) {
					return o2.getScore().compareTo(o1.getScore());
				}
			});
			
			if(!groupMappedTypes)
				return returnedTypesData.subList(0, limitToMaxItems == null ? returnedTypesData.size() : Math.min(limitToMaxItems, returnedTypesData.size()));
			
			Set<Integer> mappedTypes;
			Integer typeId;
			
			SVesselTypes newType = null;
			FuzzyVesselTypeSearchResult newResult = null;
			
			for(FuzzyVesselTypeSearchResult type : returnedTypesData) {
				typeId = type.getId();
				
				if(returnedTypesDataId.contains(typeId)) {
					updatedTypesData.add(type);
					
					mappedTypes = this.vesselTypesMappingManager.mappedNodes(typeId);
										
					//For each mapped type...
					if(mappedTypes != null)
						for(Integer mappedTypeID : mappedTypes) {
							if(mappedTypeID.equals(typeId))
								continue;
							
							if(returnedTypesDataMap.containsKey(mappedTypeID)) {
								type.getMappedTypes().add(returnedTypesDataMap.get(mappedTypeID));
								
								returnedTypesDataId.remove(mappedTypeID);
							} else {
								newType = this.vesselTypesDAO.selectByPrimaryKey(mappedTypeID);
								
								if(publicSourceSystemsSet.contains(newType.getSourceSystem())) {	
									newResult = new FuzzyVesselTypeSearchResult();
									newResult.setId(newType.getId());
									newResult.setName(newType.getName());
									newResult.setStandardAbbreviation(newType.getStandardAbbreviation());
									newResult.setCountryId(newType.getCountryId());
									newResult.setSourceSystem(newType.getSourceSystem());
									newResult.setOriginalVesselTypeId(newType.getOriginalVesselTypeId());
									newResult.setMapsTo(newType.getMapsTo());
									newResult.setScore(0D);
									
									type.getMappedTypes().add(newResult);
								}
							}
						}
					
					returnedTypesDataId.remove(typeId);
				}
			}
			
			return this.trim(updatedTypesData, limitToMaxItems);
		} catch (Throwable t) {
			this._log.error("Unable to find vessel types", t);
			
			return new ArrayList<FuzzyVesselTypeSearchResult>();
		} finally {
			this.cleanup(rs, cs, c);
			
			end = System.currentTimeMillis();
			
			this._log.info("Autocompleting vessel type for '" + vesselTypeName + "' took " + ( end - start ) + " mSec.");
		}
	}
	
	private double computeWeight(FuzzyVesselTypeSearchResult entry, String term) {
		String name = entry.getName();
		if(name == null)
			name = "";
		
		String standardAbbreviation = entry.getStandardAbbreviation();
		if(standardAbbreviation == null)
			standardAbbreviation = "";
		
		String originalTypeCode = entry.getOriginalVesselTypeId();
		if(originalTypeCode == null)
			originalTypeCode = "";
		
		String type = term.toUpperCase();
						
		double nameRatio = StringDistanceHelper.computeRelativeDistance(type, name.toUpperCase());
		double standardAbbreviationRatio = StringDistanceHelper.computeRelativeDistance(type, standardAbbreviation.toUpperCase());
		double originalTypeCodeRatio = StringDistanceHelper.computeRelativeDistance(type, originalTypeCode.toUpperCase());
		
		double[] values = { nameRatio, 
							standardAbbreviationRatio,
							originalTypeCodeRatio };

		double weight = 100;

		for(double current : values) {
			if(Double.compare(current, weight) < 0)
				weight = current;
		}

		return Math.round(100D - ( weight * 100D ));
	}
}