/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.business.dao.CommonMetadataDAO;
import org.fao.fi.vrmf.business.dao.SimpleVesselsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsDAOImpl;
import org.fao.fi.vrmf.business.dao.generated.VesselsToFlagsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToNameDAO;
import org.fao.fi.vrmf.common.models.extended.BasicVessel;
import org.fao.fi.vrmf.common.models.generated.Vessels;
import org.fao.fi.vrmf.common.models.generated.VesselsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsExample.Criteria;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlagsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;
import org.fao.fi.vrmf.common.models.generated.VesselsToNameExample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Oct 2010
 */
@NoArgsConstructor
@Named @Singleton
public class SimpleVesselsDAOImpl extends VesselsDAOImpl implements SimpleVesselsDAO {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1107816117984432971L;

	protected Logger _log = LoggerFactory.getLogger(this.getClass());
	
	@Inject protected @Named("dao.vessels") @Getter @Setter VesselsDAO vesselDAO;

	@Inject protected @Getter @Setter CommonMetadataDAO metadataDAO;
	@Inject protected @Getter @Setter VesselsToFlagsDAO flagsDAO;
	@Inject protected @Getter @Setter VesselsToNameDAO namesDAO;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fao.vrmf.utilities.business.dao.SimpleVesselsDAO#selectSimpleByPrimaryKey(java.lang.Integer)
	 */
	@Override
	public BasicVessel selectSimpleByPrimaryKey(Integer vesselID) throws Throwable {
		BasicVessel toReturn = null;

		try {
			Vessels vessel = this.vesselDAO.selectByPrimaryKey(vesselID);

			if (vessel == null)
				return null;

			// Main vessel data
			toReturn = new BasicVessel();
			toReturn.setId(vesselID);
			toReturn.setSourceSystem(vessel.getSourceSystem());
			toReturn.setUpdateDate(vessel.getUpdateDate());

			// Name management
			VesselsToNameExample nameFilter = new VesselsToNameExample();
			nameFilter.createCriteria().andVesselIdEqualTo(vesselID);
			nameFilter.setOrderByClause("REFERENCE_DATE DESC");

			List<VesselsToName> names = this.namesDAO.selectByExample(nameFilter);

			if (names != null && names.size() > 0) {
				toReturn.setCurrentName(names.get(0).getName());
			}

			// Flag management
			VesselsToFlagsExample flagsFilter = new VesselsToFlagsExample();
			flagsFilter.createCriteria().andVesselIdEqualTo(vesselID);
			flagsFilter.setOrderByClause("REFERENCE_DATE DESC");

			List<VesselsToFlags> flags = this.flagsDAO.selectByExample(flagsFilter);

			if (flags != null && flags.size() > 0) {
				toReturn.setCurrentFlagId(flags.get(0).getCountryId());
			}
		} catch (Throwable t) {
			this._log.error("Unable to retrieve extended vessel data", t);

			return null;
		}

		return toReturn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.fao.vrmf.utilities.business.dao.SimpleVesselsDAO#selectSimpleByDataSource(java.lang.String[])
	 */
	public List<BasicVessel> selectSimpleByDataSource(String[] dataSources) throws Throwable {
		VesselsExample filter = new VesselsExample();
		Criteria criteria = filter.createCriteria();

		if (dataSources != null && dataSources.length > 0)
			criteria.andSourceSystemIn(Arrays.asList(dataSources));

		return this.selectSimpleByExample(filter);
	}

	/**
	 * @param filter
	 * @return
	 * @throws Throwable
	 */
	public List<BasicVessel> selectSimpleByExample(VesselsExample filter) throws Throwable {
		List<Vessels> vessels = this.vesselDAO.selectByExample(filter);

		List<BasicVessel> toReturn = new ArrayList<BasicVessel>();

		this._log.info(vessels.size() + " vessels selected: converting to Extended Vessels...");

		int counter = 0;
		for (Vessels vessel : vessels) {
			toReturn.add(this.selectSimpleByPrimaryKey(vessel.getId()));

			counter++;

			if (counter % 1000 == 0)
				this._log.info(counter + " Vessels converted to Extended Vessels so far...");
		}

		this._log.info(counter + " Vessels converted to Extended Vessels so far...");

		return toReturn;
	}

	public Integer linkEntitiesByID(Integer sourceID, Integer targetID, Double mappingWeight, String mappingOwner, String mappingComment) throws Throwable {
		Integer uid = this.selectByPrimaryKey(targetID).getUid();

		Vessels source = new Vessels();
		source.setId(sourceID);
		source.setUid(uid);
		source.setMapsTo(targetID);
		source.setMappingWeight(mappingWeight);
		source.setMappingDate(new Date());
		source.setMappingUser(mappingOwner);
		source.setMappingComment(mappingComment);

		this.updateByPrimaryKeySelective(source);

		return uid;
	}

	public Integer linkEntities(Vessels source, Vessels target, Double mappingWeight, String mappingOwner, String mappingComment) throws Throwable {
		Integer uid = target.getUid();

		source.setUid(uid);
		source.setMapsTo(target.getId());
		source.setMappingWeight(mappingWeight);
		source.setMappingDate(new Date());
		source.setMappingUser(mappingOwner);
		source.setMappingComment(mappingComment);

		this.updateByPrimaryKeySelective(source);

		return uid;
	}
}