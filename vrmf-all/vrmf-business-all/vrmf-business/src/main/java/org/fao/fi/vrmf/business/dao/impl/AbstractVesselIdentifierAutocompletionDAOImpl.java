/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.fao.fi.vrmf.common.models.search.autocompletion.typed.IdentifierSearchResult;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
abstract public class AbstractVesselIdentifierAutocompletionDAOImpl extends AbstractAutocompletionDAOImpl {
	/**
	 * Class constructor
	 *
	 * @param client
	 */
	public AbstractVesselIdentifierAutocompletionDAOImpl() {
	}

	final protected List<IdentifierSearchResult> buildSearchResults(ResultSet rs) throws Throwable {
		List<IdentifierSearchResult> toReturn = new ArrayList<IdentifierSearchResult>();
		IdentifierSearchResult current = null;
		
		while(rs.next()) {
			current = new IdentifierSearchResult();
			
			current.setIdentifierType(rs.getString("IDENTIFIER_TYPE"));
			current.setIdentifier(rs.getString("IDENTIFIER"));
			current.setOccurrencies(rs.getInt("OCCURRENCIES"));
			current.setTotalOccurrencies(rs.getInt("TOTAL_OCCURRENCIES"));
			current.setGroupedOccurrencies(rs.getInt("GROUPED_OCCURRENCIES"));
			
			toReturn.add(current);
		}
		
		return toReturn;
	}
}