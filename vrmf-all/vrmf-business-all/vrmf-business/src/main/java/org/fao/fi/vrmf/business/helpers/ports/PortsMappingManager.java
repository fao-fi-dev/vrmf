/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.helpers.ports;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.topology.WeightedGraph;
import org.fao.fi.sh.utility.topology.helpers.TransitiveMappedDataManager;
import org.fao.fi.vrmf.business.dao.generated.SPortsDAO;
import org.fao.fi.vrmf.common.models.generated.SPorts;
import org.fao.fi.vrmf.common.models.generated.SPortsExample;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class PortsMappingManager extends TransitiveMappedDataManager<Integer, SPorts> {
	@Inject @Named("dao.sPorts") private @Getter @Setter SPortsDAO portsDAO;
	
	/**
	 * Class constructor
	 *
	 * @param data
	 */
	public PortsMappingManager(Collection<SPorts> data) {
		super(data);
	}
	
	/**
	 * Class constructor
	 *
	 * @param graph
	 */
	public PortsMappingManager(WeightedGraph<Integer> graph) {
		super(graph);
	}

	@PostConstruct
	public void afterPropertiesSet() throws Exception {
		SPortsExample pFilter = new SPortsExample();
		
		this._log.info("Initializing ports mapping manager...");
		
		try {
			for(SPorts port : this.portsDAO.selectByExample(pFilter)) {
				this.addMappedData(port);
			}
			
			this._log.info("Checking cycles...");
			
			if(this.getGraph().hasCycles(this._detector))
				throw new RuntimeException("Cycles detected in mapped ports data: " + this.getGraph().getCycles(this._detector));
			
			this._log.info("No cycles detected");
			
			this._log.info("Ports mapping manager has been initialized!");
		} catch (Throwable t) {
			throw new RuntimeException("Unable to initialize the ports mapping manager", t);
		}
	}
}
