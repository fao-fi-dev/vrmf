/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.vrmf.business.dao.VesselIRCSAutocompletionDAO;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.IRCSSearchResult;
import org.springframework.cache.annotation.Cacheable;

import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class VesselIRCSAutocompletionDAOImpl extends AbstractAutocompletionDAOImpl implements VesselIRCSAutocompletionDAO {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.generic.AbstractAutocompletionDAOImpl#getProcedureName(boolean)
	 */
	@Override
	protected String getProcedureName(boolean authorizedVesselsOnly) {
		return "IRCS_SEARCH" + ( authorizedVesselsOnly ? "_BY_AUTH" : "");
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselIRCSAutocompletionDAO#searchVesselIRCS(java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "#IRCS + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID")
	public Collection<IRCSSearchResult> searchVesselIRCS(String IRCS, Date atDate, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		return this.searchVesselIRCS(this.getProcedureName(false), IRCS, atDate, limitToMaxItems, sourceSystems, groupByUID);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselIRCSAutocompletionDAO#searchVesselIRCS(java.lang.String, java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "#procedureName + " +
															  "#IRCS + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID")
	public Collection<IRCSSearchResult> searchVesselIRCS(String procedureName, String IRCS, Date atDate, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		long end, start = System.currentTimeMillis();
		
		this._log.info("Autocompleting IRCS for '" + IRCS + "' (procedure name: " + procedureName + ", sources: " + CollectionsHelper.serializeArray(sourceSystems) + ", at date: " + atDate + ", group by UID: " + groupByUID + ")");

		Connection c = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {			
			String CALL_TEMPLATE = "CALL " + ( procedureName == null ? this.getProcedureName(false) : procedureName ) + "(?, ?, ?, ?)";
			
			c = this.sqlMapClient.getDataSource().getConnection();
			
			cs = c.prepareCall(CALL_TEMPLATE);
			
			int counter = 1;
					
			cs.setString(counter++, IRCS);
			
			if(sourceSystems != null && sourceSystems.length > 0)
				cs.setString(counter++, "@" + CollectionsHelper.join(sourceSystems, "@") + "@");
			else 
				cs.setNull(counter++, Types.VARCHAR);
			
			if(atDate != null)
				cs.setDate(counter++, new java.sql.Date(atDate.getTime()));
			else
				cs.setNull(counter++, Types.DATE);
				
			cs.setBoolean(counter++, groupByUID);	
						
			List<IRCSSearchResult> vesselIRCSData = new ArrayList<IRCSSearchResult>();
			
			if(cs.execute()) {
				rs = cs.getResultSet();
								
				IRCSSearchResult current = null;
				
				while(rs.next()) {
					current = new IRCSSearchResult();
					
					current.setIRCS(rs.getString("CALLSIGN_ID"));
					current.setOccurrencies(rs.getInt("OCCURRENCIES"));
					current.setTotalOccurrencies(rs.getInt("TOTAL_OCCURRENCIES"));
					current.setGroupedOccurrencies(rs.getInt("GROUPED_OCCURRENCIES"));
					
					vesselIRCSData.add(current);
				}
			}
			
			return this.trim(vesselIRCSData, limitToMaxItems);
		} catch (Throwable t) {
			this._log.error("Unable to find vessel IRCS", t);
			
			return new ArrayList<IRCSSearchResult>();
		} finally {			
			this.cleanup(rs, cs, c);
			
			end = System.currentTimeMillis();
			
			this._log.info("Autocompleting IRCS for '" + IRCS + "' took " + ( end - start ) + " mSec.");
		}
	}
}