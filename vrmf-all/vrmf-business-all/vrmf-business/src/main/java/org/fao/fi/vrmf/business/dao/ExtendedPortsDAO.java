/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.List;

import org.fao.fi.vrmf.business.dao.generated.SPortsDAO;
import org.fao.fi.vrmf.common.models.extended.ExtendedPort;
import org.fao.fi.vrmf.common.models.generated.SPortsExample;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Oct 2010
 */
public interface ExtendedPortsDAO extends SPortsDAO {
	ExtendedPort selectExtendedByPrimaryKey(Integer portID) throws Throwable;
	
	List<ExtendedPort> selectExtendedByDataSource(String[] dataSources) throws Throwable;
	List<ExtendedPort> selectExtendedByExample(SPortsExample filter) throws Throwable;
}
