/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.extensions.maps.lazy;

import java.util.HashSet;
import java.util.Set;

import org.fao.fi.vrmf.business.core.utilities.extensions.maps.impl.LazySerializableDataAccessMap;
import org.fao.fi.vrmf.business.dao.generated.SPortEntryDenialReasonDAO;
import org.fao.fi.vrmf.business.dao.generated.SPortEntryDenialReasonDAOImpl;
import org.fao.fi.vrmf.common.models.generated.SPortEntryDenialReason;
import org.fao.fi.vrmf.common.models.generated.SPortEntryDenialReasonExample;

import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
public class LazyPortEntryDenialReasonsMap extends LazySerializableDataAccessMap<Integer, SPortEntryDenialReason> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5709503895117678115L;

	transient private SPortEntryDenialReasonDAO _dao;
	
	/**
	 * Class constructor
	 *
	 * @param dao
	 */
	public LazyPortEntryDenialReasonsMap(SPortEntryDenialReasonDAO dao) {
		this._dao = dao;
	}
	
	/**
	 * Class constructor
	 *
	 * @param sqlMapConfigurationResource
	 * @param sourceSystems
	 * @throws Throwable
	 */
	public LazyPortEntryDenialReasonsMap(String sqlMapConfigurationResource, String[] sourceSystems) throws Throwable {
		super(sqlMapConfigurationResource, sourceSystems);
	}
			
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazySerializableDataAccessMap#initializeDAO()
	 */
	@Override
	protected void initializeDAO() throws Throwable {
		this._dao = new SPortEntryDenialReasonDAOImpl();
		this._dao.setSqlMapClient(SqlMapClientBuilder.buildSqlMapClient(Thread.currentThread().getContextClassLoader().getResourceAsStream(this._sqlMapClientConfigurationResource)));
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetElement(java.lang.Object)
	 */
	protected SPortEntryDenialReason doGetElement(Integer key) throws Throwable {
		return this._dao.selectByPrimaryKey(key);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetKeySet(java.lang.Object)
	 */
	@Override
	protected Set<Integer> doGetKeySet() throws Throwable {
		SPortEntryDenialReasonExample filter = new SPortEntryDenialReasonExample();
			
		Set<Integer> keySet = new HashSet<Integer>();
		
		for(SPortEntryDenialReason data : this._dao.selectByExample(filter)) {
			keySet.add(data.getId());
		}
		
		return keySet;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetSize()
	 */
	@Override
	protected int doGetSize() throws Throwable {
		return this._dao.countByExample(new SPortEntryDenialReasonExample());
	}
}