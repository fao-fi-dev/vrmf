/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;
import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableArrayList;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;
import org.fao.fi.vrmf.business.dao.VesselRecordSearchDAO;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.fao.fi.vrmf.common.search.dsl.support.services.response.vessels.VesselRecordSearchResponse;
import org.fao.fi.vrmf.common.services.search.response.PaginatedSearchResponse;

import com.ibatis.sqlmap.client.SqlMapClient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Feb 2013
 */
@NoArgsConstructor
@Named
@IBATISNamespace(defaultNamespacePrefix="vesselsSearch")
public class VesselRecordSearchDAOImpl extends AbstractVesselRecordHelperDAOImpl implements VesselRecordSearchDAO {
	@Inject @Named("dao.vessel.count.helper") private @Getter @Setter VesselRecordCountHelperDAOImpl countHelperDAO;
	@Inject @Named("dao.vessel.search.helper") private @Getter @Setter VesselRecordSearchHelperDAOImpl searchHelperDAO;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.CommonVesselRecordSearchDAO#countVessels(java.lang.String, org.fao.fi.vrmf.common.core.search.dsl.custom.vessels.common.implementations.extended.VesselRecordSearchFilter)
	 */
	@Override
	public Integer countVessels(String namespace, VesselRecordSearchFilter filter) throws Throwable {
		return this.countHelperDAO.countVessels(namespace, filter);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.CommonVesselRecordSearchDAO#countVessels(org.fao.fi.vrmf.common.core.search.dsl.custom.vessels.common.implementations.extended.VesselRecordSearchFilter)
	 */
	@Override
	public Integer countVessels(VesselRecordSearchFilter filter) throws Throwable {
		return this.countHelperDAO.countVessels(null, filter);
	}
	
	protected Integer countVessels(SqlMapClient client, String namespace, VesselRecordSearchFilter filter) throws Throwable {
		return this.countHelperDAO.countVessels(client, namespace, filter);
	}
	
	protected List<VesselRecord> filterVessels(SqlMapClient client, String namespace, VesselRecordSearchFilter filter) throws Throwable {
		return this.searchHelperDAO.filterVessels(client, namespace, filter);
	}
	
	protected List<VesselRecord> retrieveVesselsData(SqlMapClient client, String namespace, VesselRecordSearchFilter filter, Collection<Integer> vesselIDs) throws Throwable {
		long end, start = System.currentTimeMillis();
		
		this._log.info("##### Retrieving filtered vessels data...");
		
		//Fetches found vessels data
		List<VesselRecord> vesselData = this.searchHelperDAO.getVesselsData(client, namespace, filter, vesselIDs);
		
		end = System.currentTimeMillis();
		
		this._log.info("##### Filtered vessels data retrievement took  " + ( end - start ) + " mSec.");
		
		this._log.info("##### " + ( vesselData == null ? 0 : vesselData.size()) + " data records retrieved");
			
		return vesselData;
	}
	
	public Map<Integer, VesselRecord> getAdditionalDataForIdentifiedVessels(String namespace, VesselRecordSearchFilter filter, Collection<Integer> vesselIDs) throws Throwable {
		long end, start = System.currentTimeMillis();
		
		VesselRecordSearchFilter internalFilter = SerializationHelper.clone(filter);
		
		this._log.info("##### Retrieving metadata for " + vesselIDs.size() + " filtered vessels...");

		List<VesselRecord> basicRecords = this.searchHelperDAO.getVesselsMetadata(this.sqlMapClient, namespace, internalFilter, vesselIDs);

		end = System.currentTimeMillis();

		this._log.info("##### Retrieving metadata for " + vesselIDs.size() + " filtered vessels took " + ( end - start ) + " mSec.");

		Map<Integer, VesselRecord> result = new HashMap<Integer, VesselRecord>();

		Integer id;
		final boolean groupByUID = filter.getGroupBy().equals("UID"); 
		for(VesselRecord record : basicRecords) {
			id = groupByUID ? record.getUid() : record.getId();
			
			result.put(id, record);
		}
		
		end = System.currentTimeMillis();
		
		return result;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.CommonVesselRecordSearchDAO#searchVessels(java.lang.String, org.fao.fi.vrmf.common.core.services.request.search.vessels.VesselRecordSearchFilter)
	 */
	@Override
	public VesselRecordSearchResponse<VesselRecord> searchVessels(String namespace, VesselRecordSearchFilter filter) throws Throwable {
		//Unnecessary if we count vessels separately
		this.sqlMapClient.startTransaction();
		
		try {
			long searchEnd, searchStart = System.currentTimeMillis();
			
			int numResults = this.countVessels(this.sqlMapClient, namespace, filter);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("filter", filter);

			this._log.info("##### Filtering data...");
			
			long selectEnd, selectStart = System.currentTimeMillis();
			
			List<VesselRecord> basicVesselsData = numResults <= 0 ? null : this.filterVessels(this.sqlMapClient, namespace, filter);
						
			selectEnd = System.currentTimeMillis();
			
			this._log.info("##### Filtering took " + ( selectEnd - selectStart ) + " mSec.");
			
			List<VesselRecord> vesselData = null;

			this._log.info("##### " + ( basicVesselsData == null ? 0 : basicVesselsData.size()) + " vessels identified over " + numResults );
			
			boolean filterByUID = "UID".equalsIgnoreCase(filter.getGroupBy());
			
			if(basicVesselsData != null && !basicVesselsData.isEmpty()) {
				Map<Integer, VesselRecord> basicVesselsDataMap = new HashMap<Integer, VesselRecord>();
				
				Collection<Integer> vesselIDs = new ArrayList<Integer>();

				for(VesselRecord current : basicVesselsData) {
					vesselIDs.add(filterByUID ? current.getUid() : current.getId());
					basicVesselsDataMap.put(filterByUID ? current.getUid() : current.getId(), current);
				}
				
//				filter.clauses().and(CommonClauses.VRMF_IDS, vesselIDs);
				
				this._log.info("##### Number of identified vessel " + ( filterByUID ? "UID" : "ID" ) + "s is " + vesselIDs.size());
								
				//Fetches found vessels data
				vesselData = this.retrieveVesselsData(this.sqlMapClient, namespace, filter, vesselIDs);
							
				if(vesselData.size() != vesselIDs.size())
					this._log.warn("Retrieved " + vesselData.size() + " vessel data records against a filtered number of " + vesselIDs.size() + " vessels. Something went wrong... Check the vessel data retrievement process!!!");
				
				//Fetches found vessels additional data (number of sources, update date etc.)
				Map<Integer, VesselRecord> additionalDataMap = this.getAdditionalDataForIdentifiedVessels(namespace, filter, vesselIDs);
				
				Set<Integer> decoratedVesselIDs = additionalDataMap.keySet();
				
				if(decoratedVesselIDs.size() != vesselIDs.size())
					this._log.warn("Retrieved " + decoratedVesselIDs.size() + " vessel metadata records against a filtered number of " + vesselIDs.size() + " vessels. Something went wrong... Check the additional metadata extraction process!!!");
				
				VesselRecord additionalVesselData;
				
				long updateEnd, updateStart = System.currentTimeMillis();
				
				for(VesselRecord vessel : vesselData) {
					additionalVesselData = additionalDataMap.get(filterByUID ? vessel.getUid() : vessel.getId());
					
					vessel.setSourceSystem(additionalVesselData.getSourceSystem());
					vessel.setUpdateDate(additionalVesselData.getUpdateDate());

					if(filterByUID && filter.getRetrieveAdditionalMetadata()) {
						vessel.setSourceSystems(additionalDataMap.get(additionalVesselData.getUid()).getSourceSystems());
					} else
						vessel.setSourceSystems(Arrays.asList(new String[] { vessel.getSourceSystem() }));

					vessel.setNumberOfSources(additionalVesselData.getNumberOfSources());
					
					vesselIDs.remove(filterByUID ? vessel.getUid() : vessel.getId());
				}
				
				updateEnd = System.currentTimeMillis();
				
				this._log.info("##### Decorating " + vesselData.size() + " vessel data records took " + ( updateEnd - updateStart ) + " mSec.");
				
				if(vesselIDs.size() > 0) {
					this._log.warn(vesselIDs.size() + " vessel data records were LOST while attempting to retrieve their metadata. The " + ( filterByUID ? "UID" : "ID" ) + "s of the lacking vessels are:");
					for(Integer missing : vesselIDs)
						this._log.warn(String.valueOf(missing));
				}
			}	
			
			searchEnd = System.currentTimeMillis();
			
			this._log.info("##### Search process took " + ( searchEnd - searchStart ) + " mSec.");
			
			return new VesselRecordSearchResponse<VesselRecord>(filter.getOffset(), filter.getLimit(), numResults, vesselData);
		} finally {
			//Unnecessary if we count vessels separately 
			this.sqlMapClient.endTransaction();
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.CommonVesselRecordSearchDAO#searchVessels(org.fao.fi.vrmf.common.core.services.request.search.vessels.VesselRecordSearchFilter)
	 */
	@Override
	public VesselRecordSearchResponse<VesselRecord> searchVessels(VesselRecordSearchFilter filter) throws Throwable {
		return this.searchVessels(null, filter);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.CommonVesselRecordSearchDAO#searchVesselIDs(java.lang.String, org.fao.fi.vrmf.common.core.services.request.search.vessels.VesselRecordSearchFilter)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public PaginatedSearchResponse<Integer> searchVesselIDs(String namespace, VesselRecordSearchFilter filter) throws Throwable {
		this._log.debug("Available Source Systems: " + ( filter.getAvailableSourceSystems() == null ? null : Arrays.asList(filter.getAvailableSourceSystems())) );
		this._log.debug("Source Systems: " + ( filter.getSourceSystems() == null ? null : Arrays.asList(filter.getSourceSystems())) );

		this.sqlMapClient.startTransaction();
		
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("filter", filter);
						
			this._queryLogger.info("Executing query: " + this.toSanitizedXML(filter));
			
			List<VesselRecord> basicVesselsData = this.sqlMapClient.queryForList(this.getQueryIDForNamespace(namespace, "improvedSelectBasicVesselsData"), parameters);
		
			this._log.info("##### Matching vessels found: " + ( basicVesselsData == null ? 0 : basicVesselsData.size()) );
		
			Integer numResults = this.getFullSearchResultsSize();
		
			boolean filterByUID = "UID".equalsIgnoreCase(filter.getGroupBy());
		
			if(numResults != null && numResults > 0) {
				List<Integer> vesselIDs = new ArrayList<Integer>();
			
				for(VesselRecord current : basicVesselsData)
					vesselIDs.add(filterByUID ? current.getUid() : current.getId());
			
				return new PaginatedSearchResponse<Integer>(filter.getOffset(), filter.getLimit(), numResults, new SerializableArrayList<Integer>(vesselIDs));
			}
			
			return new PaginatedSearchResponse<Integer>(filter.getOffset(), filter.getLimit(), 0, new SerializableArrayList<Integer>());
		} finally {
			this.sqlMapClient.endTransaction();
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.CommonVesselRecordSearchDAO#searchVesselIDs(org.fao.fi.vrmf.common.core.services.request.search.vessels.VesselRecordSearchFilter)
	 */
	@Override
	public PaginatedSearchResponse<Integer> searchVesselIDs(VesselRecordSearchFilter filter) throws Throwable {
		return this.searchVesselIDs(null, filter);
	}
}