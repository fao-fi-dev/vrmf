/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Named;

import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.springframework.cache.annotation.Cacheable;

import com.ibatis.sqlmap.client.SqlMapClient;

import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Feb 2013
 */
@NoArgsConstructor
@Named("dao.vessel.count.helper")
@IBATISNamespace(defaultNamespacePrefix="vesselsSearch")
public class VesselRecordCountHelperDAOImpl extends AbstractVesselRecordHelperDAOImpl {
	static final private String DEFAULT_COUNT_INVOCATION_CACHE_ID = "vrmf.business.vessels.count";

	public Integer countVessels(String namespace, VesselRecordSearchFilter filter) throws Throwable {
		return this.countVessels(this.sqlMapClient, namespace, filter);
	}

	public Integer countVessels(VesselRecordSearchFilter filter) throws Throwable {
		return this.countVessels(null, filter);
	}

	@Cacheable(key="#root.target.buildCountVesselsKey(#filter)", value=DEFAULT_COUNT_INVOCATION_CACHE_ID)
	public Integer countVessels(SqlMapClient client, String namespace, VesselRecordSearchFilter filter) throws Throwable {
		filter.switchToDataCounting();

		VesselRecordSearchFilter internalFilter = SerializationHelper.clone(filter);

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("filter", internalFilter);

		//Switches to stage #1 (count)
		internalFilter.switchToDataCounting();

		long start = System.currentTimeMillis();

		String xml, key = (this.computeFilterKey(xml = this.filterToXML(internalFilter)));

		try {
			this._queryLogger.debug("Executing query [ key: {} ] {}", key, xml);

			Integer count = (Integer)client.queryForObject(this.getQueryIDForNamespace(namespace, "countBy" + filter.getGroupBy()), parameters);

			this._log.info(count + " vessel record have been identified according to set criteria");

			return count;
		} finally {
			this.trackQueryExecution(xml, key, start);
		}
	}
}