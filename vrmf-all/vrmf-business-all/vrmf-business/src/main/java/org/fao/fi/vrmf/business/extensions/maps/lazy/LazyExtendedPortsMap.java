/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.extensions.maps.lazy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.fao.fi.sh.utility.caching.spi.CacheFacade;
import org.fao.fi.vrmf.business.core.utilities.extensions.maps.impl.LazySerializableDataAccessMap;
import org.fao.fi.vrmf.business.dao.ExtendedPortsDAO;
import org.fao.fi.vrmf.business.dao.impl.ExtendedPortsDAOImpl;
import org.fao.fi.vrmf.common.models.extended.ExtendedPort;
import org.fao.fi.vrmf.common.models.generated.SPorts;
import org.fao.fi.vrmf.common.models.generated.SPortsExample;

import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
public class LazyExtendedPortsMap extends LazySerializableDataAccessMap<Integer, ExtendedPort> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7081698698959061568L;

	transient private ExtendedPortsDAO _dao;
	
	/**
	 * Class constructor
	 *
	 * @param dao
	 * @throws Throwable
	 */
	public LazyExtendedPortsMap(ExtendedPortsDAO dao) throws Throwable {
		super();
		
		this._dao = dao;
	}
	
	/**
	 * Class constructor
	 *
	 * @param dao
	 * @param sourceSystems
	 */
	public LazyExtendedPortsMap(CacheFacade<Integer, ExtendedPort> backingMap, String sqlMapConfigurationResource, String[] sourceSystems) throws Throwable {
		super(backingMap, sqlMapConfigurationResource, sourceSystems);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazySerializableDataAccessMap#initializeDAO()
	 */
	@Override
	protected void initializeDAO() throws Throwable {
		this._dao = new ExtendedPortsDAOImpl();
		this._dao.setSqlMapClient(SqlMapClientBuilder.buildSqlMapClient(Thread.currentThread().getContextClassLoader().getResourceAsStream(this._sqlMapClientConfigurationResource)));
	}
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetElement(java.lang.Object)
	 */
	protected ExtendedPort doGetElement(Integer key) throws Throwable {
		return this._dao.selectExtendedByPrimaryKey(key);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetKeySet(java.lang.Object)
	 */
	@Override
	protected Set<Integer> doGetKeySet() throws Throwable {
		SPortsExample filter = new SPortsExample();
			
		if(this._sourceSystems != null && this._sourceSystems.length > 0)
			filter.createCriteria().andSourceSystemIn(Arrays.asList(this._sourceSystems));
			
		Set<Integer> keySet = new HashSet<Integer>();
		
		for(SPorts port : this._dao.selectByExample(filter)) {
			keySet.add(port.getId());
		}
		
		return keySet;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetSize()
	 */
	@Override
	protected int doGetSize() throws Throwable {
		SPortsExample filter = new SPortsExample();
		
		if(this._sourceSystems != null && this._sourceSystems.length > 0)
			filter.createCriteria().andSourceSystemIn(Arrays.asList(this._sourceSystems));
		
		return this._dao.countByExample(filter);
	}
}