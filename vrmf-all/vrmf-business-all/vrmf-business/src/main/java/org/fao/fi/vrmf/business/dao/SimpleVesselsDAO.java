/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.List;

import org.fao.fi.vrmf.business.core.dao.MappingManagerDAO;
import org.fao.fi.vrmf.business.core.dao.ibatis.SetterInjectionIBATISDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsDAO;
import org.fao.fi.vrmf.common.models.extended.BasicVessel;
import org.fao.fi.vrmf.common.models.generated.Vessels;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Oct 2010
 */ 
public interface SimpleVesselsDAO extends VesselsDAO, MappingManagerDAO<Integer, Vessels>, SetterInjectionIBATISDAO  {
	BasicVessel selectSimpleByPrimaryKey(Integer vesselID) throws Throwable;
	
	List<BasicVessel> selectSimpleByDataSource(String[] dataSources) throws Throwable;
	
	Integer linkEntities(Vessels source, Vessels target, Double mappingWeight, String mappingOwner, String mappingComment) throws Throwable;
	Integer linkEntitiesByID(Integer sourceID, Integer targetID, Double mappingWeight, String mappingOwner, String mappingComment) throws Throwable;
}
