/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.extensions.maps.lazy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.fao.fi.vrmf.business.core.utilities.extensions.maps.impl.LazySerializableDataAccessMap;
import org.fao.fi.vrmf.business.dao.generated.SPowerTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SPowerTypesDAOImpl;
import org.fao.fi.vrmf.common.models.generated.SPowerTypes;
import org.fao.fi.vrmf.common.models.generated.SPowerTypesExample;

import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
public class LazyPowerTypesMap extends LazySerializableDataAccessMap<String, SPowerTypes> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2573020033008867392L;

	transient private SPowerTypesDAO _dao;
	
	public LazyPowerTypesMap(SPowerTypesDAO dao) throws Throwable {
		super();
		
		this._dao = dao;
	}
	
	/**
	 * Class constructor
	 *
	 * @param sqlMapConfigurationResource
	 * @param sourceSystems
	 * @throws Throwable
	 */
	public LazyPowerTypesMap(String sqlMapConfigurationResource, String[] sourceSystems) throws Throwable {
		super(sqlMapConfigurationResource, sourceSystems);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazySerializableDataAccessMap#initializeDAO()
	 */
	@Override
	protected void initializeDAO() throws Throwable {
		this._dao = new SPowerTypesDAOImpl();
		this._dao.setSqlMapClient(SqlMapClientBuilder.buildSqlMapClient(Thread.currentThread().getContextClassLoader().getResourceAsStream(this._sqlMapClientConfigurationResource)));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetElement(java.lang.Object)
	 */
	protected SPowerTypes doGetElement(String key) throws Throwable {
		return this._dao.selectByPrimaryKey(key);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetKeySet(java.lang.Object)
	 */
	@Override
	protected Set<String> doGetKeySet() throws Throwable {
		SPowerTypesExample filter = new SPowerTypesExample();
			
		if(this._sourceSystems != null && this._sourceSystems.length > 0)
			filter.createCriteria().andSourceSystemIn(Arrays.asList(this._sourceSystems));
			
		Set<String> keySet = new HashSet<String>();
		
		for(SPowerTypes powerType : this._dao.selectByExample(filter)) {
			keySet.add(powerType.getId());
		}
		
		return keySet;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetSize()
	 */
	@Override
	protected int doGetSize() throws Throwable {
		SPowerTypesExample filter = new SPowerTypesExample();
		
		if(this._sourceSystems != null && this._sourceSystems.length > 0)
			filter.createCriteria().andSourceSystemIn(Arrays.asList(this._sourceSystems));
		
		return this._dao.countByExample(filter);
	}
}