/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractSetterInjectionIBATISDAO;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;
import org.fao.fi.vrmf.business.dao.CommonMetadataDAO;
import org.fao.fi.vrmf.business.helpers.VesselsTableHelper;
import org.fao.fi.vrmf.common.models.generated.SSystems;
import org.fao.fi.vrmf.common.models.generated.SSystemsGroups;
import org.fao.fi.vrmf.common.models.stats.StatusReportBySource;
import org.springframework.cache.annotation.Cacheable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Jan 2011
 */
@Named("dao.common.metadata")
@Singleton
@IBATISNamespace(defaultNamespacePrefix="meta")
public class CommonMetadataDAOImpl extends AbstractSetterInjectionIBATISDAO implements CommonMetadataDAO {
	static final private String DEFAULT_METHODS_INVOCATION_CACHE_ID = "vrmf.business.vessels.stats.meta";
	
	public CommonMetadataDAOImpl() {
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#getAuthorizationsIssuerSystems()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SSystems> getAuthorizationsIssuerSystems(Collection<String> managedSystemsIDs) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("managedSystems", managedSystemsIDs);
		
		return (List<SSystems>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("getAuthorizationIssuersSystems"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#getAuthorizationsIssuerSystemsGroups()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SSystemsGroups> getAuthorizationsIssuerSystemsGroups(Collection<String> managedSystemsIDs) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("managedSystems", managedSystemsIDs);
		
		return (List<SSystemsGroups>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("getAuthorizationIssuersSystemsGroups"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#getLastID()
	 */
	@Override
	public Integer getLastID() throws Exception {
		return(Integer)this.sqlMapClient.queryForObject(this.getQueryIDForCurrentNamespace("getLastID"));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#lockTable(java.lang.String)
	 */
	@Override
	public void lockTable(String table) throws Exception {
		this._log.debug("Attempting to lock table '{}'", table);
		
		this.logStatus();
		
		this.sqlMapClient.queryForObject(this.getQueryIDForCurrentNamespace("lockTable"), table);
		this._log.debug("Lock on table '{}' acquired by Thread {}", table, Thread.currentThread().getName());
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#unlockTables()
	 */
	@Override
	public void unlockTables() throws Exception {
		this._log.debug("Attempting to unlock tables by Thread '{}'", Thread.currentThread().getName());
		
		this.logStatus();
		
		this.sqlMapClient.queryForObject(this.getQueryIDForCurrentNamespace("unlockTables"));
		this._log.debug("Successfully unlocked tables by Thread '{}'", Thread.currentThread().getName());
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#getNextUID(java.lang.String)
	 */
	@Override
	public Integer getNextUID(String table) throws Exception {
		return (Integer)this.sqlMapClient.queryForObject(this.getQueryIDForCurrentNamespace("getNextUID"), table);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#getPreviousVesselUID(java.lang.Integer)
	 */
	@Override
	public Integer getPreviousVesselUID(Integer vesselID) throws Exception {
		return (Integer)this.sqlMapClient.queryForObject(this.getQueryIDForCurrentNamespace("getPreviousVesselUID"), vesselID);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#getNextUIDForSystem(java.lang.String, java.lang.String)
	 */
	@Override
	public Integer getNextUIDForSystem(String system, String table) throws Exception {
		this._log.debug("Attempting to get next UID for system '{}' through table '{}' by thread {}", system, table, Thread.currentThread().getName());

		this.logStatus();
		
		Integer current = null; 

		if(system != null) {
			current = (Integer)this.sqlMapClient.queryForObject(this.getQueryIDForCurrentNamespace("readNextUIDForSystem"), system) + 1;
	
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("sourceSystem", system);
			params.put("uid", current);
		
			this.sqlMapClient.update(this.getQueryIDForCurrentNamespace("storeNextUIDForSystem"), params);
		} else {
			current = this.getNextUID(table);
		}
		
		return current;
		
//		this._log.debug("Attempting to get next UID by thread " + Thread.currentThread().getName());
//				
//		this.logStatus();
//		
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("system", system);
//		params.put("table", table);
//		
//		if(system != null)
//			return (Integer)this._client.queryForObject(this.getQueryIDForCurrentNamespace("getNextUIDForSystem"), params);
//		
//		return this.getNextUID(table);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#getNextUIDForAuthorizationSystem(java.lang.String, java.lang.String)
	 */
	@Override
	public Integer getNextUIDForAuthorizationSystem(String system, String table) throws Exception {
		this._log.debug("Attempting to get next UID by thread {}", Thread.currentThread().getName());
		
		this.logStatus();
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("system", system);
		params.put("table", VesselsTableHelper.vesselsTable(table));

		Integer next = null; 

		if(system != null) {
			next = (Integer)this.sqlMapClient.queryForObject(this.getQueryIDForCurrentNamespace("getNextUIDForAuthorizationSystem"), params);
	
			Map<String, Object> paramsForUpdate = new HashMap<String, Object>();
			paramsForUpdate.put("sourceSystem", system);
			paramsForUpdate.put("uid", next);
		
			this.sqlMapClient.update(this.getQueryIDForCurrentNamespace("storeNextUIDForSystem"), paramsForUpdate);
		} else {
			throw new RuntimeException("Authorization system is not set (provided vessel table is " + table + ")");
		}
		
		return next;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#getAuthorizedIDsByUID(java.lang.Integer, java.lang.String, java.lang.String, java.lang.Boolean)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getAuthorizedIDsByUID(Integer vesselUID, String authorizationIssuerID, String authorizationTypeID, Boolean excludeTerminatedVessels, String table) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("value", vesselUID);
		params.put("authorizationIssuerID", authorizationIssuerID);
		params.put("authorizationTypeID", authorizationTypeID);
		params.put("excludeTerminatedVessels", excludeTerminatedVessels);
		params.put("vesselsTable", VesselsTableHelper.vesselsTable(table));
		
		return (List<Integer>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("getIDsForUID"), params);
	}
	
	protected Date computeLastDate(String what, String groupBy, Integer id, String[] sourceSystems, String table) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("what", what);
		params.put("groupBy", groupBy);
		params.put("id", id);
		params.put("sourceSystems", sourceSystems);
		params.put("vesselsTable", VesselsTableHelper.vesselsTable(table));
		
		return (Date)this.sqlMapClient.queryForObject(this.getQueryIDForCurrentNamespace("computeLastDateByIDAndSystems"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#getLastUpdateDateByID(java.lang.Integer)
	 */
	@Override
	public Date computeLastUpdateDateByID(Integer vesselID) throws Exception {
		return this.computeLastDate("update", "id", vesselID, VesselsTableHelper.ALL_DATA_SOURCES, VesselsTableHelper.DEFAULT_VESSELS_TABLE);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#computeLastUpdateDateByIDAndSources(java.lang.Integer, java.lang.String[])
	 */
	@Override
	public Date computeLastUpdateDateByIDAndSources(Integer vesselID, String[] systems) throws Exception {
		return this.computeLastDate("update", "id", vesselID, systems, VesselsTableHelper.DEFAULT_VESSELS_TABLE);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#getLastUpdateDateByUID(java.lang.Integer)
	 */
	@Override
	public Date computeLastUpdateDateByUID(Integer vesselUID, String table) throws Exception {
		return this.computeLastDate("update", "uid", vesselUID, VesselsTableHelper.ALL_DATA_SOURCES, table);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#computeLastUpdateDateByUIDAndSources(java.lang.Integer, java.lang.String[])
	 */
	@Override
	public Date computeLastUpdateDateByUIDAndSources(Integer vesselUID, String[] systems, String table) throws Exception {
		return this.computeLastDate("update", "uid", vesselUID, systems, table);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#getLastUpdateDateByUIDAndSources(java.lang.Integer, java.lang.String[])
	 */
	@Override
	public Date getLastUpdateDateByUID(Integer vesselUID, String table) throws Exception {
		return this.getLastUpdateDateByUIDAndSources(vesselUID, VesselsTableHelper.ALL_DATA_SOURCES, table);
	}	
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.MetadataDAO#getLastUpdateDateByUIDAndSources(java.lang.Integer, java.lang.String[])
	 */
	@Override
	public Date getLastUpdateDateByUIDAndSources(Integer vesselUID, String[] systems, String table) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uid", vesselUID);
		params.put("sourceSystems", systems);
		params.put("vesselsTable", VesselsTableHelper.vesselsTable(table));
		
		return (Date)this.sqlMapClient.queryForObject(this.getQueryIDForCurrentNamespace("getLastUpdateDateByUIDAndSources"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.MetadataDAO#getStatusReportBySources(java.lang.String, java.lang.String[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#availableSources) + " +
															  "#vesselsTable")
	public List<StatusReportBySource> getStatusReportBySources(String[] availableSources, String table) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("vesselsTable", VesselsTableHelper.vesselsTable(table));
		
		if(availableSources != null && availableSources.length > 0)
			params.put("availableSources", availableSources);
		
		return (List<StatusReportBySource>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("getOverallStatsBySource"), params);
	}
}