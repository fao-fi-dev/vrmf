/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import org.fao.fi.vrmf.business.core.dao.ibatis.SetterInjectionIBATISDAO;
import org.fao.fi.vrmf.common.models.extended.FullVessel;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Dec 2010
 */
public interface FullVesselsDAO extends SetterInjectionIBATISDAO {
	FullVessel selectFullByPrimaryKey(Integer vesselID) throws Throwable;	
	FullVessel selectFullByPrimaryKeyAndDataSources(Integer vesselID, String[] dataSources) throws Throwable;
	
	FullVessel selectFullByPrimaryKey(Integer[] vesselIDs) throws Throwable;	
	FullVessel selectFullByPrimaryKeyAndDataSources(Integer[] vesselIDs, String[] dataSources) throws Throwable;
	
	FullVessel selectFullByUID(Integer vesselUID, String vesselsTable) throws Throwable;
	FullVessel selectFullByUIDAndDataSources(Integer vesselUID, String[] dataSources, String vesselsTable) throws Throwable;
	
	FullVessel selectFullByUIDAndAuthorization(Integer vesselUID, String authorizationIssuerID, String authorizationType, Boolean excludeTerminatedVessels, String vesselsTable) throws Throwable;	
	FullVessel selectFullByUIDAuthorizationAndDataSources(Integer vesselUID, String[] dataSources, String authorizationIssuerID, String authorizationTypeID, Boolean excludeTerminatedVessels, String vesselsTable) throws Throwable;	

	FullVessel buildFromExtended(ExtendedVessel extended) throws Throwable;
	FullVessel buildFromExtended(ExtendedVessel extended, boolean preserveOrder) throws Throwable;
	
	FullVessel localize(FullVessel data, String lang) throws Throwable;
}
