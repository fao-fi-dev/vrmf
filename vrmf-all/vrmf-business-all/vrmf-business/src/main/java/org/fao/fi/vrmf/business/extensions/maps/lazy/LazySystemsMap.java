/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.extensions.maps.lazy;

import java.util.HashSet;
import java.util.Set;

import org.fao.fi.vrmf.business.core.utilities.extensions.maps.impl.LazySerializableDataAccessMap;
import org.fao.fi.vrmf.business.dao.generated.SSystemsDAO;
import org.fao.fi.vrmf.business.dao.generated.SSystemsDAOImpl;
import org.fao.fi.vrmf.common.models.generated.SSystems;
import org.fao.fi.vrmf.common.models.generated.SSystemsExample;

import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
public class LazySystemsMap extends LazySerializableDataAccessMap<String, SSystems> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5709503895117678115L;

	transient private SSystemsDAO _dao;
	
	/**
	 * Class constructor
	 *
	 * @param dao
	 */
	public LazySystemsMap(SSystemsDAO dao) {
		this._dao = dao;
	}
	
	/**
	 * Class constructor
	 *
	 * @param sqlMapConfigurationResource
	 * @param sourceSystems
	 * @throws Throwable
	 */
	public LazySystemsMap(String sqlMapConfigurationResource, String[] sourceSystems) throws Throwable {
		super(sqlMapConfigurationResource, sourceSystems);
	}
			
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazySerializableDataAccessMap#initializeDAO()
	 */
	@Override
	protected void initializeDAO() throws Throwable {
		this._dao = new SSystemsDAOImpl();
		this._dao.setSqlMapClient(SqlMapClientBuilder.buildSqlMapClient(Thread.currentThread().getContextClassLoader().getResourceAsStream(this._sqlMapClientConfigurationResource)));
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetElement(java.lang.Object)
	 */
	protected SSystems doGetElement(String key) throws Throwable {
		return this._dao.selectByPrimaryKey(key);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetKeySet(java.lang.Object)
	 */
	@Override
	protected Set<String> doGetKeySet() throws Throwable {
		SSystemsExample filter = new SSystemsExample();
			
		Set<String> keySet = new HashSet<String>();
		
		for(SSystems country : this._dao.selectByExample(filter)) {
			keySet.add(country.getId());
		}
		
		return keySet;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetSize()
	 */
	@Override
	protected int doGetSize() throws Throwable {
		return this._dao.countByExample(new SSystemsExample());
	}
}