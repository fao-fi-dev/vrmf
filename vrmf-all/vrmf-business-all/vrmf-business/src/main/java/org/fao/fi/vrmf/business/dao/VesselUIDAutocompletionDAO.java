/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.Collection;

import org.fao.fi.vrmf.common.models.search.autocompletion.typed.IdentifierSearchResult;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
public interface VesselUIDAutocompletionDAO {
	Collection<IdentifierSearchResult> searchVesselUID(String UID, String[] sourceSystems, Integer limitToMaxItems) throws Throwable;
	Collection<IdentifierSearchResult> searchVesselUID(String procedureName, String UID, String[] sourceSystems, Integer limitToMaxItems) throws Throwable;
}
