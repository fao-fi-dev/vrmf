/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractSetterInjectionIBATISDAO;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;
import org.fao.fi.vrmf.business.dao.VesselIndexingDAO;
import org.fao.fi.vrmf.common.models.indexing.SourceUpdateMetadata;
import org.fao.fi.vrmf.common.models.indexing.VesselUpdateMetadata;

import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2013
 */
@NoArgsConstructor
@Named @Singleton
@IBATISNamespace(defaultNamespacePrefix="vesselsIndexing")
public class VesselIndexingDAOImpl extends AbstractSetterInjectionIBATISDAO implements VesselIndexingDAO {
	@Override
	public Integer countIMOSourcesData(String[] sources, String groupBy) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sources", sources);
		params.put("groupBy", groupBy);
	
		return (Integer)this.sqlMapClient.queryForObject(this.getQueryIDForNamespace(this.getIBATISNamespace(), "countSourcesIMOData"), params);
	}

	@Override
	public Integer countSourcesData(String[] sources, String groupBy) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sources", sources);
		params.put("groupBy", groupBy);
	
		return (Integer)this.sqlMapClient.queryForObject(this.getQueryIDForNamespace(this.getIBATISNamespace(), "countSourcesData"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.VesselRecordSearchDAO#getIDsForUID(java.lang.String, java.lang.Integer)
	 */
	@Override
	public List<Integer> getIDsForUID(String vesselsTable, Integer vesselUID) throws Throwable {
		throw new RuntimeException("TO BE IMPLEMENTED");
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.browser.business.dao.SearchVesselsDAO#getIMOSourceUpdateMetadata(java.lang.String[], java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SourceUpdateMetadata> getIMOSourceUpdateMetadata(String[] sources, String groupBy) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sources", sources);
		params.put("groupBy", groupBy);
	
		return (List<SourceUpdateMetadata>)this.sqlMapClient.queryForList(this.getQueryIDForNamespace(this.getIBATISNamespace(), "getIMOSourcesData"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.browser.business.dao.SearchVesselsDAO#getVesselIdentifiers(java.lang.String[], java.lang.String, java.lang.Integer, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<VesselUpdateMetadata> getIMOVesselIdentifiers(String[] sources, String groupBy, Integer limit, Integer offset) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sources", sources);
		params.put("groupBy", groupBy);
		params.put("limit", limit);
		params.put("offset", offset);
	
		return (List<VesselUpdateMetadata>)this.sqlMapClient.queryForList(this.getQueryIDForNamespace(this.getIBATISNamespace(), "getIMOVesselIdentifiers"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.VesselRecordSearchDAO#getRandomVesselIDs(java.lang.String[], java.lang.String, long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getRandomVesselIDs(String[] sources, String groupBy, long limit) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sources", sources);
		params.put("groupBy", groupBy);
		params.put("limit", limit);
	
		return (List<Integer>)this.sqlMapClient.queryForList(this.getQueryIDForNamespace(this.getIBATISNamespace(), "getRandomVesselIDs"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.browser.business.dao.SearchVesselsDAO#getSourceUpdateMetadata(java.lang.String[], java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SourceUpdateMetadata> getSourceUpdateMetadata(String[] sources, String groupBy) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sources", sources);
		params.put("groupBy", groupBy);
	
		return (List<SourceUpdateMetadata>)this.sqlMapClient.queryForList(this.getQueryIDForNamespace(this.getIBATISNamespace(), "getSourcesData"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.browser.business.dao.SearchVesselsDAO#getVesselIdentifiers(java.lang.String[], java.lang.String, java.lang.Integer, java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<VesselUpdateMetadata> getVesselIdentifiers(String[] sources, String groupBy, Integer limit, Integer offset) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sources", sources);
		params.put("groupBy", groupBy);
		params.put("limit", limit);
		params.put("offset", offset);
	
		return (List<VesselUpdateMetadata>)this.sqlMapClient.queryForList(this.getQueryIDForNamespace(this.getIBATISNamespace(), "getVesselIdentifiers"), params);
	}
}