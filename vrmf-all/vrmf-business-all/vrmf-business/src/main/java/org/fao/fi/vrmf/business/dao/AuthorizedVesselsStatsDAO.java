/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.Date;
import java.util.List;

import org.fao.fi.vrmf.common.models.generated.SAgreementContributingParties;
import org.fao.fi.vrmf.common.models.stats.CodedVesselStatsReport;
import org.fao.fi.vrmf.common.models.stats.RangedVesselStatsReport;
import org.fao.fi.vrmf.common.models.stats.SimpleVesselStatsReport;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Oct 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Oct 2011
 */
public interface AuthorizedVesselsStatsDAO {
	List<SAgreementContributingParties> getStats(String sourceSystem, 
										   		 String authorizationTypeID,
										   		 String authorizationStatus,
										   		 String vesselsTable, 
										   		 String groupBy,
										   		 Date atDate,
										   		 Boolean excludeTerminatedVessels) throws Throwable;
	
	List<RangedVesselStatsReport> getLengthStats(String sourceSystem,
												 String lengthTypeID,
												 String authorizationIssuerID,
												 String authorizationTypeID,
												 String vesselsTable, 
										   		 String groupBy) throws Throwable;
	
	List<RangedVesselStatsReport> getTonnageStats(String sourceSystem,
			 									  String tonnageTypeID,
			 									  String authorizationIssuerID,
			 									  String authorizationTypeID,
			 									  String vesselsTable, 
			 									  String groupBy) throws Throwable;
	
	List<RangedVesselStatsReport> getPowerStats(String sourceSystem,
												Boolean mainEngine,
			  									String powerTypeID,
			  									String authorizationIssuerID,
			  									String authorizationTypeID,
			  									String vesselsTable, 
			  									String groupBy) throws Throwable;
	
	List<RangedVesselStatsReport> getAgeStats(String sourceSystem,
											  String authorizationIssuerID,
											  String authorizationTypeID,
											  String vesselsTable, 
											  String groupBy) throws Throwable;
	
	List<CodedVesselStatsReport> getTypeStats(String sourceSystem,
											  Boolean categoriesOnly,
											  String categoryCode,
											  String authorizationIssuerID,
											  String authorizationTypeID,
											  String vesselsTable, 
											  String groupBy) throws Throwable;
	
	List<CodedVesselStatsReport> getGearStats(String sourceSystem,
											  Boolean categoriesOnly,
											  String categoryCode,
											  Boolean primaryGear,
											  String authorizationIssuerID,
											  String authorizationTypeID,
											  String vesselsTable, 
											  String groupBy) throws Throwable;	
		
	Integer countAuthorizedVessels(String authorizationIssuerID, 
								   String authorizationTypeID, 
								   String authorizationStatus, 
								   Date atDate, 
								   String vesselsTable, 
								   String groupBy,
								   Boolean excludeTerminatedVessels) throws Throwable;
	
	Integer countAuthorizedVesselsForCountry(String authorizationIssuerID, 
			   					   			 String authorizationTypeID, 
			   					   			 String authorizationStatus,
			   					   			 Integer issuingCountryID,
			   					   			 Date atDate, 
			   					   			 String vesselsTable, 
			   					   			 String groupBy,
			   					   			 Boolean excludeTerminatedVessels) throws Throwable;
	
	List<SimpleVesselStatsReport> getAuthorizedVesselsSummary(String authorizationIssuerID, 
			   												  String authorizationTypeID, 
			   												  String authorizationStatus, 
			   												  Date atDate, 
			   												  String vesselsTable, 
			   												  String groupBy) throws Throwable;	
}