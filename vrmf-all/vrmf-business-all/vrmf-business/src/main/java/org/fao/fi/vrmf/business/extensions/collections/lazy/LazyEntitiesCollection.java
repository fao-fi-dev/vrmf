/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.extensions.collections.lazy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.fao.fi.sh.utility.caching.spi.CacheFacade;
import org.fao.fi.vrmf.business.core.utilities.extensions.collections.impl.LazySerializableDataAccessCollection;
import org.fao.fi.vrmf.business.dao.generated.EntityDAO;
import org.fao.fi.vrmf.business.dao.generated.EntityDAOImpl;
import org.fao.fi.vrmf.common.models.generated.Entity;
import org.fao.fi.vrmf.common.models.generated.EntityExample;
import org.fao.fi.vrmf.common.models.generated.EntityExample.Criteria;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
public class LazyEntitiesCollection extends LazySerializableDataAccessCollection<Integer, Entity> {	
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3292850152365283905L;

	static final public int DEFAULT_ENTITIES_CACHE_SIZE = 200000;

	transient private EntityDAO _dao;
			
	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param sqlMapClientConfigurationResource
	 * @param sourceSystems
	 * @throws Throwable
	 */
	public LazyEntitiesCollection(CacheFacade<Integer, Entity> backingCache, String sqlMapClientConfigurationResource, String[] sourceSystems) throws Throwable {
		super(backingCache, sqlMapClientConfigurationResource, sourceSystems);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.utils.extensions.collections.impl.LazySerializableDataAccessCollection#initializeDAOs(com.ibatis.sqlmap.client.SqlMapClient)
	 */
	@Override
	protected void initializeDAOs(SqlMapClient providedSqlMapClient) throws Throwable {
		this._dao = new EntityDAOImpl();
		this._dao.setSqlMapClient(SqlMapClientBuilder.buildSqlMapClient(Thread.currentThread().getContextClassLoader().getResourceAsStream(this._sqlMapClientConfigurationResource)));
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.utils.extensions.collections.impl.LazySerializableDataAccessCollection#initializeDAO()
	 */
	@Override
	protected void initializeDAOs() throws Throwable {
		this._dao = new EntityDAOImpl();
		this._dao.setSqlMapClient(SqlMapClientBuilder.buildSqlMapClient(Thread.currentThread().getContextClassLoader().getResourceAsStream(this._sqlMapClientConfigurationResource)));
	}
		
	/**
	 * @param sources
	 * @return
	 */
	private EntityExample createFilterFromSourceSystems(String[] sources) {
		EntityExample filter = new EntityExample();
		Criteria criteria = filter.createCriteria();

		List<String> sourcesCollection = new ArrayList<String>();
		
		if(this._sourceSystems != null && this._sourceSystems.length > 0) {
			sourcesCollection.addAll(Arrays.asList(this._sourceSystems));
		
			criteria.andSourceSystemIn(sourcesCollection);
		}
		
		return filter;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.collections.impl.LazyCollection#getSize()
	 */
	@Override
	protected int getSize() {
		try {
			if(this._backingCache.size() == 0) {
				return this._dao.countByExample(this.createFilterFromSourceSystems(this._sourceSystems));
			} else {
				return this._backingCache.size();
			}
		} catch(Throwable t) {
			return 0;
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.collections.impl.LazyCollection#getIndexes()
	 */
	@Override
	protected Collection<Integer> getIndexes() {
		Collection<Integer> indexes = new ArrayList<Integer>();
		
		try {
			for(Entity entity : this._dao.selectByExample(this.createFilterFromSourceSystems(this._sourceSystems))) {
				indexes.add(entity.getId());
			}
		} catch(Throwable t) {
			//Suffocate
		}
		
		return indexes;
	}

	/* (non-Javadoc)
	 * @see org.fao.gvr2.common.utils.ext.LazyCollection#doGetElement(java.lang.Object)
	 */
	protected Entity doGetElement(Integer index) {
		try {
			return this._dao.selectByPrimaryKey(index);
		} catch(Throwable t) {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see java.util.Collection#add(java.lang.Object)
	 */
	@Override
	public boolean add(Entity e) {
		Entity current = this._backingCache.get($nN($nN(e, "The entity to add cannot be NULL").getId(), "The entity to add cannot have a NULL ID"));
		
		this._backingCache.put(e.getId(), e);
		
		return current != null;
	}
}