/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.extensions.collections.lazy;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.util.Collection;

import org.fao.fi.sh.utility.caching.spi.CacheFacade;
import org.fao.fi.vrmf.business.core.utilities.extensions.collections.impl.LazySerializableDataAccessCollection;
import org.fao.fi.vrmf.business.dao.ExtendedVesselsDAO;
import org.fao.fi.vrmf.business.dao.impl.ExtendedVesselsDAOImpl;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
final public class LazyIMOEUCFRVesselsCollection extends LazySerializableDataAccessCollection<Integer, ExtendedVessel> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3436720853618616675L;
	
	transient private Collection<Class<?>> _dataTypeSet;
	transient private ExtendedVesselsDAO _dao;
	
	private Collection<Integer> _indexes;

	private boolean _withIMOOnly = false;
	private boolean _withEUCFROnly = false;
	
	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param sqlMapClientConfigurationResource
	 * @param originalSourceSystems
	 * @throws Throwable
	 */
	public LazyIMOEUCFRVesselsCollection(CacheFacade<Integer, ExtendedVessel> backingCache, String sqlMapClientConfigurationResource, String[] sourceSystems, Collection<Class<?>> dataTypeSet, boolean withIMOOnly, boolean withEUCFROnly) throws Throwable {
		super(backingCache, sqlMapClientConfigurationResource, sourceSystems);

		this._dataTypeSet = dataTypeSet;
		
		this._withIMOOnly = withIMOOnly;
		this._withEUCFROnly = withEUCFROnly;
	}
		
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.utils.extensions.collections.impl.LazySerializableDataAccessCollection#initializeDAOs()
	 */
	@Override
	protected void initializeDAOs() throws Throwable {
		this.initializeDAOs(SqlMapClientBuilder.buildSqlMapClient(Thread.currentThread().getContextClassLoader().getResourceAsStream(this._sqlMapClientConfigurationResource)));
	}
		
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.utils.extensions.collections.impl.LazySerializableDataAccessCollection#initializeDAOs(com.ibatis.sqlmap.client.SqlMapClient)
	 */
	@Override
	protected void initializeDAOs(SqlMapClient providedSqlMapClient) throws Throwable {
		this._dao = new ExtendedVesselsDAOImpl();
		this._dao.setSqlMapClient(providedSqlMapClient);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.collections.impl.LazyCollection#getSize()
	 */
	@Override
	protected int getSize() {
		try {
			if(this._backingCache.size() == 0) {
				boolean withIMOAndEUCFR = this._withIMOOnly && this._withEUCFROnly;
				if(withIMOAndEUCFR)
					return this._dao.selectVesselsWithIMOAndEUCFRIDs(this._sourceSystems).size();
				else if(this._withIMOOnly)
					return this._dao.selectVesselsWithIMOIDs(this._sourceSystems).size();
				else if(this._withEUCFROnly)
					return this._dao.selectVesselsWithEUCFRIDs(this._sourceSystems).size();
				else
					return this._dao.selectVesselIDs(this._sourceSystems).size();
			} else {
				return this._backingCache.size();
			}
		} catch(Throwable t) {
			this._log.error("Unable to retrieve size for lazy collection " + this, t);
		}
		
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.collections.impl.LazyCollection#getIndexes()
	 */
	@Override
	protected Collection<Integer> getIndexes() {
		if(this._indexes == null) {
			try {
				boolean withIMOAndEUCFR = this._withIMOOnly && this._withEUCFROnly;
				if(withIMOAndEUCFR)
					this._indexes = this._dao.selectVesselsWithIMOAndEUCFRIDs(this._sourceSystems);
				else if(this._withIMOOnly)
					this._indexes = this._dao.selectVesselsWithIMOIDs(this._sourceSystems);
				else if(this._withEUCFROnly)
					this._indexes = this._dao.selectVesselsWithEUCFRIDs(this._sourceSystems);
				else
					this._indexes = this._dao.selectVesselIDs(this._sourceSystems);
			} catch (Throwable t) {
				this._log.error("Unable to retrieve indexes for lazy collection " + this, t);
			}
		}
		
		return this._indexes;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.collections.impl.LazyCollection#doGetElement(java.lang.Object)
	 */
	protected ExtendedVessel doGetElement(Integer index) {
		try {
			return this._dao.selectExtendedByPrimaryKey(index, this._sourceSystems, this._dataTypeSet);
		} catch(Throwable t) {
			this._log.error("Unable to retrieve element with index " + index + " for lazy collection " + this, t);
		}
		
		return null;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Collection#add(java.lang.Object)
	 */
	@Override
	public boolean add(ExtendedVessel e) {
		ExtendedVessel current = this._backingCache.get($nN($nN(e, "The vessel to add cannot be NULL").getId(), "The vessel to add cannot have a NULL ID"));
		
		this._backingCache.put(e.getId(), e);
		
		return current != null;
	}
}