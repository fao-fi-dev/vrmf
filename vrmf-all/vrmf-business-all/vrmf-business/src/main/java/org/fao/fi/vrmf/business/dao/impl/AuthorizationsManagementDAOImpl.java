/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractConstructorInjectionIBATISDAO;
import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractIBATISDAO;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;
import org.fao.fi.vrmf.business.dao.AuthorizationsManagementDAO;
import org.fao.fi.vrmf.common.models.generated.Authorizations;

import com.ibatis.sqlmap.client.SqlMapClient;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Oct 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Oct 2011
 */
@Named
@Singleton
@IBATISNamespace(defaultNamespacePrefix="authorizationsManagement")
public class AuthorizationsManagementDAOImpl extends AbstractConstructorInjectionIBATISDAO implements AuthorizationsManagementDAO {
	/**
	 * Class constructor
	 *
	 * @param client
	 */
	@Inject
	@Named(AbstractIBATISDAO.VRMF_DEFAULT_SQL_MAP_CLIENT_BEAN_ID)
	public AuthorizationsManagementDAOImpl(SqlMapClient client) {
		super(client);
	}
	
	@SuppressWarnings("unchecked")
	public List<Authorizations> selectAuthorizationsToTerminate(Integer issuingCountryID, String issuerId, String typeId, String vesselsTable, String groupBy) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("issuingCountryId", issuingCountryID);
		params.put("issuerId", issuerId);
		params.put("typeId", typeId);
		params.put("vesselsTable", vesselsTable);
		params.put("groupBy", groupBy);
		
		return (List<Authorizations>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("selectAuthorizationsToTerminate"), params);
	}
}