/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.common.helpers.singletons.text.StringDistanceHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.business.dao.GearTypeFuzzyAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.generated.SGearTypesDAO;
import org.fao.fi.vrmf.business.helpers.vessel.gears.GearTypesMappingManager;
import org.fao.fi.vrmf.common.models.generated.SGearTypes;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.FuzzyGearTypeSearchResult;
import org.springframework.cache.annotation.Cacheable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class GearTypeFuzzyAutocompletionDAOImpl extends AbstractAutocompletionDAOImpl implements GearTypeFuzzyAutocompletionDAO {
	@Inject private @Getter @Setter SGearTypesDAO gearTypesDAO;
	@Inject private @Getter @Setter GearTypesMappingManager gearTypesMappingManager;

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.generic.AbstractAutocompletionDAOImpl#getProcedureName(boolean)
	 */
	public String getProcedureName(boolean authorizedOnly) {
		return "FUZZY_GEAR_TYPE_SEARCH" + ( authorizedOnly ? "_BY_AUTH" : "");
	}
	

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.GearTypeFuzzyAutocompletionDAO#searchGearTypes(java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.String[], java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#availableSources) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#publicSourceSystems) + " +
															  "#gearTypeName + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID + " +
															  "#groupMappedTypes")
	public Collection<FuzzyGearTypeSearchResult> searchGearTypes(String gearTypeName, Date atDate, Integer limitToMaxItems, String[] availableSources, String[] sourceSystems, String[] publicSourceSystems, Boolean groupByUID, boolean groupMappedTypes) throws Throwable {
		return this.searchGearTypes(this.getProcedureName(false), gearTypeName, atDate, limitToMaxItems, availableSources, sourceSystems, publicSourceSystems, groupByUID, groupMappedTypes);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.GearTypeFuzzyAutocompletionDAO#searchGearTypes(java.lang.String, java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.String[], java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#availableSources) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#publicSourceSystems) + " +
															  "#procedureName + " +
															  "#gearTypeName + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID + " +
															  "#groupMappedTypes")
	public Collection<FuzzyGearTypeSearchResult> searchGearTypes(String procedureName, String gearTypeName, Date atDate, Integer limitToMaxItems, String[] availableSources, String[] sourceSystems, String[] publicSourceSystems, Boolean groupByUID, boolean groupMappedTypes) throws Throwable {
		long end, start = System.currentTimeMillis();

		this._log.info("Autocompleting gear type for '" + gearTypeName + "' (procedure name: " + procedureName + ", sources: " + CollectionsHelper.serializeArray(sourceSystems) + ", at date: " + atDate + ", group by UID: " + groupByUID + ")");

		Set<String> publicSourceSystemsSet = new HashSet<String>(Arrays.asList(availableSources));
		publicSourceSystemsSet.addAll(Arrays.asList(publicSourceSystems));
		
		Set<String> sourceSystemsSet = new HashSet<String>();
		
		if(sourceSystems != null)
			sourceSystemsSet.addAll(Arrays.asList(sourceSystems));
		
		sourceSystemsSet.addAll(publicSourceSystemsSet);
		
		Connection c = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {			
			String CALL_TEMPLATE = "CALL " + ( procedureName == null ? this.getProcedureName(false) : procedureName ) + "(?, ?)";
						
			c = this.sqlMapClient.getDataSource().getConnection();
			
			cs = c.prepareCall(CALL_TEMPLATE);
			
			int counter = 1;
			
			this.setUnicodeString(cs, counter++, gearTypeName);
	
			if(sourceSystemsSet.isEmpty())
				cs.setString(counter++, "@" + CollectionsHelper.join(publicSourceSystemsSet, "@") + "@");
			else
				cs.setString(counter++, "@" + CollectionsHelper.join(sourceSystemsSet, "@") + "@");

			List<FuzzyGearTypeSearchResult> returnedTypesData = new ArrayList<FuzzyGearTypeSearchResult>();
			List<FuzzyGearTypeSearchResult> updatedTypesData = new ArrayList<FuzzyGearTypeSearchResult>();
			Map<Integer, FuzzyGearTypeSearchResult> returnedTypesDataMap = new HashMap<Integer, FuzzyGearTypeSearchResult>();
			List<Integer> returnedTypesDataId = new ListSet<Integer>();
					
			if(cs.execute()) {
				rs = cs.getResultSet();
					
				FuzzyGearTypeSearchResult current = null;
						
				while(rs.next()) {
					current = new FuzzyGearTypeSearchResult();
										
					current.setId(rs.getInt("ID"));
					current.setOriginalGearTypeCode(rs.getString("ORIGINAL_GEAR_TYPE_CODE"));
										
					current.setName(rs.getString("NAME"));
					current.setStandardAbbreviation(rs.getString("STANDARD_ABBREVIATION"));
					current.setSourceSystem(rs.getString("SOURCE_SYSTEM"));
					
					current.setScore(this.computeWeight(current, gearTypeName));
					
					returnedTypesData.add(current);
					returnedTypesDataMap.put(current.getId(), current);
					returnedTypesDataId.add(current.getId());
				}
			}
			
			Collections.sort(returnedTypesData, new Comparator<FuzzyGearTypeSearchResult>() {
				public int compare(FuzzyGearTypeSearchResult o1, FuzzyGearTypeSearchResult o2) {
					return o2.getScore().compareTo(o1.getScore());
				}				
			});
			
			if(!groupMappedTypes)
				return returnedTypesData.subList(0, limitToMaxItems == null ? returnedTypesData.size() : Math.min(limitToMaxItems, returnedTypesData.size()));
			
			Set<Integer> mappedTypes;
			Integer typeId;
			
			SGearTypes newType = null;
			FuzzyGearTypeSearchResult newResult = null;
			
			for(FuzzyGearTypeSearchResult type : returnedTypesData) {
				typeId = type.getId();
				
				if(returnedTypesDataId.contains(typeId)) {
					updatedTypesData.add(type);
					
					mappedTypes = this.gearTypesMappingManager.mappedNodes(typeId);
										
					if(mappedTypes != null)
						//For each mapped type...
						for(Integer mappedTypeID : mappedTypes) {
							if(mappedTypeID.equals(typeId))
								continue;
							
							if(returnedTypesDataMap.containsKey(mappedTypeID)) {
								type.getMappedTypes().add(returnedTypesDataMap.get(mappedTypeID));
								
								returnedTypesDataId.remove(mappedTypeID);
							} else {
								newType = this.gearTypesDAO.selectByPrimaryKey(mappedTypeID);
								
								if(publicSourceSystemsSet.contains(newType.getSourceSystem())) {	
									newResult = new FuzzyGearTypeSearchResult();
									newResult.setId(newType.getId());
									newResult.setName(newType.getName());
									newResult.setStandardAbbreviation(newType.getStandardAbbreviation());
									newResult.setSourceSystem(newType.getSourceSystem());
									newResult.setOriginalGearTypeCode(newType.getOriginalGearTypeCode());
									newResult.setMapsTo(newType.getMapsTo());
									newResult.setScore(0D);
									
									type.getMappedTypes().add(newResult);
								}
							}
						}
					
					returnedTypesDataId.remove(typeId);
				}
			}
			
			return this.trim(updatedTypesData, limitToMaxItems);
		} catch (Throwable t) {
			this._log.error("Unable to find vessel types", t);
			
			return new ArrayList<FuzzyGearTypeSearchResult>();
		} finally {
			this.cleanup(rs, cs, c);
			
			end = System.currentTimeMillis();
			
			this._log.info("Autocompleting vessel type for '" + gearTypeName + "' took " + ( end - start ) + " mSec.");
		}
	}
	
	private double computeWeight(FuzzyGearTypeSearchResult entry, String term) {
		String name = entry.getName();
		if(name == null)
			name = "";
		
		String standardAbbreviation = entry.getStandardAbbreviation();
		if(standardAbbreviation == null)
			standardAbbreviation = "";
		
		String originalTypeCode = entry.getOriginalGearTypeCode();
		if(originalTypeCode == null)
			originalTypeCode = "";
		
		String type = term.toUpperCase();
						
		double nameRatio = StringDistanceHelper.computeRelativeDistance(type, name.toUpperCase());
		double standardAbbreviationRatio = StringDistanceHelper.computeRelativeDistance(type, standardAbbreviation.toUpperCase());
		double originalTypeCodeRatio = StringDistanceHelper.computeRelativeDistance(type, originalTypeCode.toUpperCase());
		
		double[] values = { nameRatio, 
							standardAbbreviationRatio,
							originalTypeCodeRatio };

		double weight = 100;

		for(double current : values) {
			if(Double.compare(current, weight) < 0)
				weight = current;
		}

		return Math.round(100D - ( weight * 100D ));
	}
}