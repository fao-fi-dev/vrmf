/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractSetterInjectionIBATISDAO;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 May 2012
 */
abstract public class AbstractAutocompletionDAOImpl extends AbstractSetterInjectionIBATISDAO {
	static final protected String DEFAULT_METHODS_INVOCATION_CACHE_ID = "vrmf.business.search.autocompletion";
		
	final protected byte[] getUnicodeBytes(String string) {
		if(string == null)
			return null;
		
		return string.getBytes(Charset.forName("utf-8"));
	}
	
	final protected void setUnicodeString(CallableStatement cs, int column, String value) throws Throwable {
		if(value == null)
			cs.setNull(column, Types.VARCHAR);
		else
			cs.setBytes(column, this.getUnicodeBytes(value));
	}
	
	final protected <S extends Serializable> List<S> trim(List<S> toTrim, Integer maxLength) {
		return toTrim == null || 
			   toTrim.isEmpty() || 
			   maxLength == null || 
			   maxLength > toTrim.size() ? toTrim : new ArrayList<S>(toTrim.subList(0, maxLength - 1));
	}
	
	abstract protected String getProcedureName(boolean authorizedVesselsOnly);
	
	final protected void cleanup(ResultSet rs, CallableStatement cs, Connection c) {
		if(rs != null) {
			try {
				rs.close();
			} catch (Throwable t) {
				this._log.error("Unable to close resultset", t);
			}
		}		
		
		if(cs != null) {
			try {
				cs.close();
			} catch (Throwable t) {
				this._log.error("Unable to close callable statement", t);
			}
		}
		
		if(c != null) {
			try {
				c.close();
			} catch (Throwable t) {
				this._log.error("Unable to close connection", t);
			}
		}
	}
}
