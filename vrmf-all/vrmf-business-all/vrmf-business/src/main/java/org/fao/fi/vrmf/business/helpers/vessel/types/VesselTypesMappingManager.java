/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.helpers.vessel.types;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.topology.WeightedGraph;
import org.fao.fi.sh.utility.topology.helpers.TransitiveMappedDataManager;
import org.fao.fi.vrmf.business.dao.generated.SVesselTypesDAO;
import org.fao.fi.vrmf.common.models.generated.SVesselTypes;
import org.fao.fi.vrmf.common.models.generated.SVesselTypesExample;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class VesselTypesMappingManager extends TransitiveMappedDataManager<Integer, SVesselTypes> {
	@Inject private @Getter @Setter SVesselTypesDAO vesselTypesDAO;
	
	/**
	 * Class constructor
	 *
	 * @param data
	 */
	public VesselTypesMappingManager(Collection<SVesselTypes> data) {
		super(data);
	}

	/**
	 * Class constructor
	 *
	 * @param graph
	 */
	public VesselTypesMappingManager(WeightedGraph<Integer> graph) {
		super(graph);
	}
	
	
	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@PostConstruct
	public void afterPropertiesSet() throws Exception {
		SVesselTypesExample filter = new SVesselTypesExample();
		
		this._log.info("Initializing vessel types mapping manager...");
		
		try {
			for(SVesselTypes type : this.vesselTypesDAO.selectByExample(filter)) {
				this.addMappedData(type);
			}
			
			this._log.info("Checking cycles...");

			if(this.getGraph().hasCycles(this._detector))
				throw new RuntimeException("Cycles detected in mapped vessel types data: " + this.getGraph().getCycles(this._detector));
			
			this._log.info("No cycles detected");
			
			this._log.info("Vessel types mapping manager has been initialized");
		} catch (Throwable t) {
			throw new RuntimeException("Unable to initialize the vessel types mapping manager", t);
		}	
	}
}