/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractConstructorInjectionIBATISDAO;
import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractIBATISDAO;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;
import org.fao.fi.vrmf.business.dao.StatsDAO;
import org.fao.fi.vrmf.common.models.stats.CountriesToFlag;

import com.ibatis.sqlmap.client.SqlMapClient;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2011
 */
@Named @Singleton
@IBATISNamespace(defaultNamespacePrefix="stats")
public final class StatsDAOImpl extends AbstractConstructorInjectionIBATISDAO implements StatsDAO {
	/**
	 * Class constructor
	 *
	 * @param client
	 */
	@Inject @Named(AbstractIBATISDAO.VRMF_DEFAULT_SQL_MAP_CLIENT_BEAN_ID)
	public StatsDAOImpl(SqlMapClient client) {
		super(client);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.StatsDAO#getAvailableYears()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getAvailableYears() throws Throwable {
		return (List<Integer>)this.sqlMapClient.queryForList(this.getIBATISNamespace() + ".availableYears");
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.StatsDAO#getFlagStatsByYear(int)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CountriesToFlag> getFlagStatsByYear(int year, boolean cumulative) throws Throwable {
		return (List<CountriesToFlag>)this.sqlMapClient.queryForList(this.getIBATISNamespace() + ".flagReports" + ( cumulative ? "Cumulative" : "" ), new Integer(year));
	}
}