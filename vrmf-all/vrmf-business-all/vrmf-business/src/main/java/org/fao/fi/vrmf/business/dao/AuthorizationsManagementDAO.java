/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.List;

import org.fao.fi.vrmf.common.models.generated.Authorizations;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Oct 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Oct 2011
 */
public interface AuthorizationsManagementDAO {
	List<Authorizations> selectAuthorizationsToTerminate(Integer issuingCountryID, String issuerId, String typeId, String vesselsTable, String groupBy) throws Throwable;
}