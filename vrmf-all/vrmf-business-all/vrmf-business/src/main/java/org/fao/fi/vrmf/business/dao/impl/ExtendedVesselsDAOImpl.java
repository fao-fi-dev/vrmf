/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.business.dao.ExtendedVesselsDAO;
import org.fao.fi.vrmf.business.dao.VesselRecordSearchDAO;
import org.fao.fi.vrmf.business.dao.generated.AuthorizationDetailsDAO;
import org.fao.fi.vrmf.business.dao.generated.AuthorizationsDAO;
import org.fao.fi.vrmf.business.dao.generated.EntityDAO;
import org.fao.fi.vrmf.business.dao.generated.SFishingZonesDAO;
import org.fao.fi.vrmf.business.dao.generated.SSpeciesDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsHistoryDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToBeneficialOwnersDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToBuildingYearDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToCallsignsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToCrewDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToExternalMarkingsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToFishingLicenseDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToFishingMastersDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToGearsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToHullMaterialDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToIdentifiersDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToInspectionsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToIuuListDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToLengthsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToMastersDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToMmsiDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToNonComplianceDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToOperatorsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToOwnersDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToPortEntryDenialsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToPowerDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToRegistrationsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToShipbuildersDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToStatusDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToTonnageDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsToVmsDAO;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.Authorizations;
import org.fao.fi.vrmf.common.models.generated.AuthorizationsExample;
import org.fao.fi.vrmf.common.models.generated.Vessels;
import org.fao.fi.vrmf.common.models.generated.VesselsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsExample.Criteria;
import org.fao.fi.vrmf.common.models.generated.VesselsToBeneficialOwners;
import org.fao.fi.vrmf.common.models.generated.VesselsToBeneficialOwnersExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToBuildingYear;
import org.fao.fi.vrmf.common.models.generated.VesselsToBuildingYearExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsignsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToCrew;
import org.fao.fi.vrmf.common.models.generated.VesselsToCrewExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToExternalMarkings;
import org.fao.fi.vrmf.common.models.generated.VesselsToExternalMarkingsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToFishingLicense;
import org.fao.fi.vrmf.common.models.generated.VesselsToFishingLicenseExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToFishingMasters;
import org.fao.fi.vrmf.common.models.generated.VesselsToFishingMastersExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlagsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToGears;
import org.fao.fi.vrmf.common.models.generated.VesselsToGearsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToHullMaterial;
import org.fao.fi.vrmf.common.models.generated.VesselsToHullMaterialExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiersExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToInspections;
import org.fao.fi.vrmf.common.models.generated.VesselsToInspectionsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToIuuList;
import org.fao.fi.vrmf.common.models.generated.VesselsToIuuListExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengthsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToMasters;
import org.fao.fi.vrmf.common.models.generated.VesselsToMastersExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToMmsi;
import org.fao.fi.vrmf.common.models.generated.VesselsToMmsiExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;
import org.fao.fi.vrmf.common.models.generated.VesselsToNameExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToNonCompliance;
import org.fao.fi.vrmf.common.models.generated.VesselsToNonComplianceExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToOperators;
import org.fao.fi.vrmf.common.models.generated.VesselsToOperatorsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToOwners;
import org.fao.fi.vrmf.common.models.generated.VesselsToOwnersExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToPortEntryDenials;
import org.fao.fi.vrmf.common.models.generated.VesselsToPortEntryDenialsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToPower;
import org.fao.fi.vrmf.common.models.generated.VesselsToPowerExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrationsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToShipbuilders;
import org.fao.fi.vrmf.common.models.generated.VesselsToShipbuildersExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToStatus;
import org.fao.fi.vrmf.common.models.generated.VesselsToStatusExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnageExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToTypes;
import org.fao.fi.vrmf.common.models.generated.VesselsToTypesExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToVms;
import org.fao.fi.vrmf.common.models.generated.VesselsToVmsExample;
import org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonClauses;
import org.fao.fi.vrmf.common.search.dsl.impl.meta.GroupableDataColumns;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.fao.fi.vrmf.common.search.dsl.support.services.response.vessels.VesselRecordSearchResponse;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Oct 2010
 */
@NoArgsConstructor
@Named("dao.vessels.extended")
public class ExtendedVesselsDAOImpl extends SimpleVesselsDAOImpl implements ExtendedVesselsDAO {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6043693696652963300L;

	@Inject protected @Getter @Setter VesselsHistoryDAO vesselHistoryDAO;

	@Inject protected @Getter @Setter VesselsToIdentifiersDAO identifiersDAO;
	
	@Inject protected @Getter @Setter VesselsToBuildingYearDAO buildingYearDAO;
	@Inject protected @Getter @Setter VesselsToShipbuildersDAO shipbuildersDAO;
	@Inject protected @Getter @Setter VesselsToHullMaterialDAO hullDAO;

	@Inject protected @Getter @Setter VesselsToStatusDAO statusDAO;
	
	@Inject protected @Getter @Setter VesselsToCallsignsDAO callsignsDAO;
	@Inject protected @Getter @Setter VesselsToMmsiDAO MMSIDAO;
	@Inject protected @Getter @Setter VesselsToVmsDAO VMSDAO;

	@Inject protected @Getter @Setter VesselsToExternalMarkingsDAO externalMarkingsDAO;
	
	@Inject protected @Getter @Setter VesselsToGearsDAO gearsDAO;
	@Inject protected @Getter @Setter VesselsToTypesDAO typesDAO;

	@Inject protected @Getter @Setter VesselsToMastersDAO mastersDAO;
	@Inject protected @Getter @Setter VesselsToFishingMastersDAO fishingMastersDAO;

	@Inject protected @Getter @Setter VesselsToOperatorsDAO operatorsDAO;
	@Inject protected @Getter @Setter VesselsToOwnersDAO ownersDAO;
	@Inject protected @Getter @Setter VesselsToBeneficialOwnersDAO beneficialOwnersDAO;

	@Inject protected @Getter @Setter VesselsToLengthsDAO lengthsDAO;
	@Inject protected @Getter @Setter VesselsToTonnageDAO tonnageDAO;
	@Inject protected @Getter @Setter VesselsToPowerDAO powerDAO;

	@Inject protected @Getter @Setter VesselsToRegistrationsDAO registrationsDAO;
	@Inject protected @Getter @Setter VesselsToFishingLicenseDAO fishingLicenseDAO;

	@Inject protected @Getter @Setter VesselsToCrewDAO crewDAO;

	@Inject protected @Getter @Setter VesselsToNonComplianceDAO nonComplianceDAO;
	
	@Inject protected @Getter @Setter AuthorizationsDAO authorizationsDAO;
	@Inject protected @Getter @Setter AuthorizationDetailsDAO authorizationDetailsDAO;
	
	@Inject protected @Getter @Setter EntityDAO entityDAO;

	@Inject protected @Getter @Setter SFishingZonesDAO fishingZonesDAO;
	@Inject protected @Getter @Setter SSpeciesDAO speciesDAO;

	@Inject protected @Getter @Setter VesselRecordSearchDAO vesselRecordSearchDAO;
	
	@Inject protected @Getter @Setter VesselsToInspectionsDAO inspectionsDAO;
	@Inject protected @Getter @Setter VesselsToIuuListDAO iuuListsDAO;
	@Inject protected @Getter @Setter VesselsToPortEntryDenialsDAO portEntryDenialsDAO;

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectExtendedByPrimaryKey(java.lang.Integer)
	 */
	@Override
	public ExtendedVessel selectExtendedByPrimaryKey(Integer vesselID) throws Throwable {
		return this.selectExtendedByPrimaryKey(vesselID, null, null);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectExtendedByPrimaryKey(java.lang.Integer, java.lang.String[])
	 */
	@Override
	public ExtendedVessel selectExtendedByPrimaryKey(Integer vesselID, String[] dataSourcesFilter) throws Throwable {
		return this.selectExtendedByPrimaryKey(vesselID, dataSourcesFilter, null);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectExtendedByPrimaryKey(java.lang.Integer, java.util.Collection)
	 */
	@Override
	public ExtendedVessel selectExtendedByPrimaryKey(Integer vesselID, Collection<Class<?>> dataTypeSet) throws Throwable {
		return this.selectExtendedByPrimaryKey(vesselID, null, dataTypeSet);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectExtendedByPrimaryKey(java.lang.Integer, java.lang.String[], java.util.Collection)
	 */
	@Override
	public ExtendedVessel selectExtendedByPrimaryKey(Integer vesselID, String[] dataSources, Collection<Class<?>> dataTypeSet) throws Throwable {
		ExtendedVessel toReturn = null;

		try {
			Vessels vessel = this.vesselDAO.selectByPrimaryKey(vesselID);

			if(vessel == null)
				return null;

			//Main vessel data
			toReturn = new ExtendedVessel(vessel);

			boolean filterDataType = dataTypeSet != null && dataTypeSet.size() > 0;

			List<String> dataSourcesList = null;

			if(dataSources != null && dataSources.length > 0)
				dataSourcesList = Arrays.asList(dataSources);

			//Building Year management
			if(!filterDataType || dataTypeSet.contains(VesselsToBuildingYear.class)) {
				VesselsToBuildingYearExample buildingYearFilter = new VesselsToBuildingYearExample();
				VesselsToBuildingYearExample.Criteria criteria = buildingYearFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				buildingYearFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setBuildingYearData(this.buildingYearDAO.selectByExample(buildingYearFilter));
			}

			//Shipbuilders management
			if(!filterDataType || dataTypeSet.contains(VesselsToShipbuilders.class)) {
				VesselsToShipbuildersExample shipbuilderFilter = new VesselsToShipbuildersExample();
				VesselsToShipbuildersExample.Criteria criteria = shipbuilderFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				shipbuilderFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setShipbuildersData(this.shipbuildersDAO.selectByExample(shipbuilderFilter));
			}

			//Identifiers management
			if(!filterDataType || dataTypeSet.contains(VesselsToIdentifiers.class)) {
				VesselsToIdentifiersExample identifierFilter = new VesselsToIdentifiersExample();
				VesselsToIdentifiersExample.Criteria criteria = identifierFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				identifierFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");
				
				toReturn.setIdentifiers(this.identifiersDAO.selectByExample(identifierFilter));
			}
			
			//Non Compliance management
			if(!filterDataType || dataTypeSet.contains(VesselsToNonCompliance.class)) {
				VesselsToNonComplianceExample nonComplianceFilter = new VesselsToNonComplianceExample();
				VesselsToNonComplianceExample.Criteria criteria = nonComplianceFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				nonComplianceFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setNonComplianceData(this.nonComplianceDAO.selectByExample(nonComplianceFilter));
			}

			//Authorizations management
			if(!filterDataType || dataTypeSet.contains(Authorizations.class)) {
				AuthorizationsExample authorizationFilter = new AuthorizationsExample();
				AuthorizationsExample.Criteria criteria = authorizationFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				authorizationFilter.setOrderByClause("REFERENCE_DATE DESC, VALID_FROM DESC, UPDATE_DATE DESC");

				toReturn.setAuthorizationsData(this.authorizationsDAO.selectByExample(authorizationFilter));
			}

			//BuildingYear management
			if(!filterDataType || dataTypeSet.contains(VesselsToStatus.class)) {
				VesselsToStatusExample statusFilter = new VesselsToStatusExample();
				VesselsToStatusExample.Criteria criteria = statusFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				statusFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setStatusData(this.statusDAO.selectByExample(statusFilter));
			}
			
			//HullMaterial management
			if(!filterDataType || dataTypeSet.contains(VesselsToHullMaterial.class)) {
				VesselsToHullMaterialExample hullFilter = new VesselsToHullMaterialExample();
				VesselsToHullMaterialExample.Criteria criteria = hullFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				hullFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setHullMaterialData(this.hullDAO.selectByExample(hullFilter));
			}

			//Type management
			if(!filterDataType || dataTypeSet.contains(VesselsToTypes.class)) {
				VesselsToTypesExample typeFilter = new VesselsToTypesExample();
				VesselsToTypesExample.Criteria criteria = typeFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				typeFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setTypeData(this.typesDAO.selectByExample(typeFilter));
			}

			//Name management
			if(!filterDataType || dataTypeSet.contains(VesselsToName.class)) {
				VesselsToNameExample nameFilter = new VesselsToNameExample();
				VesselsToNameExample.Criteria criteria = nameFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				nameFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setNameData(this.namesDAO.selectByExample(nameFilter));
			}

			//External Markings management
			if(!filterDataType || dataTypeSet.contains(VesselsToExternalMarkings.class)) {
				VesselsToExternalMarkingsExample externalMarkingsFilter = new VesselsToExternalMarkingsExample();
				VesselsToExternalMarkingsExample.Criteria criteria =  externalMarkingsFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				externalMarkingsFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setExternalMarkingsData(this.externalMarkingsDAO.selectByExample(externalMarkingsFilter));
			}

			//Flag management
			if(!filterDataType || dataTypeSet.contains(VesselsToFlags.class)) {
				VesselsToFlagsExample flagsFilter = new VesselsToFlagsExample();
				VesselsToFlagsExample.Criteria criteria = flagsFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				flagsFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setFlagData(this.flagsDAO.selectByExample(flagsFilter));
			}

			//Registration management
			if(!filterDataType || dataTypeSet.contains(VesselsToRegistrations.class)) {
				VesselsToRegistrationsExample registrationFilter = new VesselsToRegistrationsExample();
				VesselsToRegistrationsExample.Criteria criteria = registrationFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				registrationFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setRegistrationData(this.registrationsDAO.selectByExample(registrationFilter));
			}

			//Fishing license management
			if(!filterDataType || dataTypeSet.contains(VesselsToFishingLicense.class)) {
				VesselsToFishingLicenseExample fishingLicenseFilter = new VesselsToFishingLicenseExample();
				VesselsToFishingLicenseExample.Criteria criteria = fishingLicenseFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				fishingLicenseFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setFishingLicenseData(this.fishingLicenseDAO.selectByExample(fishingLicenseFilter));
			}

			//Callsign management
			if(!filterDataType || dataTypeSet.contains(VesselsToCallsigns.class)) {
				VesselsToCallsignsExample callsignFilter = new VesselsToCallsignsExample();
				VesselsToCallsignsExample.Criteria criteria = callsignFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				callsignFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setCallsignData(this.callsignsDAO.selectByExample(callsignFilter));
			}

			//MMSI management
			if(!filterDataType || dataTypeSet.contains(VesselsToMmsi.class)) {
				VesselsToMmsiExample MMSIFilter = new VesselsToMmsiExample();
				VesselsToMmsiExample.Criteria criteria = MMSIFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				MMSIFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setMMSIData(this.MMSIDAO.selectByExample(MMSIFilter));
			}
			
			//VMS management
			if(!filterDataType || dataTypeSet.contains(VesselsToVms.class)) {
				VesselsToVmsExample VMSFilter = new VesselsToVmsExample();
				VesselsToVmsExample.Criteria criteria = VMSFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				VMSFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setVMSData(this.VMSDAO.selectByExample(VMSFilter));
			}

			//Crew management
			if(!filterDataType || dataTypeSet.contains(VesselsToCrew.class)) {
				VesselsToCrewExample crewFilter = new VesselsToCrewExample();
				VesselsToCrewExample.Criteria criteria = crewFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				crewFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setCrewData(this.crewDAO.selectByExample(crewFilter));
			}

			//Gear management
			if(!filterDataType || dataTypeSet.contains(VesselsToGears.class)) {
				VesselsToGearsExample gearFilter = new VesselsToGearsExample();
				VesselsToGearsExample.Criteria criteria = gearFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				gearFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setGearData(this.gearsDAO.selectByExample(gearFilter));
			}

			//Length management
			if(!filterDataType || dataTypeSet.contains(VesselsToLengths.class)) {
				VesselsToLengthsExample lengthFilter = new VesselsToLengthsExample();
				VesselsToLengthsExample.Criteria criteria = lengthFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				lengthFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setLengthData(this.lengthsDAO.selectByExample(lengthFilter));
			}

			//Tonnage management
			if(!filterDataType || dataTypeSet.contains(VesselsToTonnage.class)) {
				VesselsToTonnageExample tonnageFilter = new VesselsToTonnageExample();
				VesselsToTonnageExample.Criteria criteria = tonnageFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				tonnageFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setTonnageData(this.tonnageDAO.selectByExample(tonnageFilter));
			}

			//Power management
			if(!filterDataType || dataTypeSet.contains(VesselsToPower.class)) {
				VesselsToPowerExample powerFilter = new VesselsToPowerExample();
				VesselsToPowerExample.Criteria criteria = powerFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				powerFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setPowerData(this.powerDAO.selectByExample(powerFilter));
			}
			
			//Masters management
			if(!filterDataType || dataTypeSet.contains(VesselsToMasters.class)) {
				VesselsToMastersExample mastersFilter = new VesselsToMastersExample();
				VesselsToMastersExample.Criteria criteria = mastersFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				mastersFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setMastersData(this.mastersDAO.selectByExample(mastersFilter));
			}
			
			//Fishing Masters management
			if(!filterDataType || dataTypeSet.contains(VesselsToFishingMasters.class)) {
				VesselsToFishingMastersExample fishingMastersFilter = new VesselsToFishingMastersExample();
				VesselsToFishingMastersExample.Criteria criteria = fishingMastersFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				fishingMastersFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setFishingMastersData(this.fishingMastersDAO.selectByExample(fishingMastersFilter));
			}

			//Beneficial Owners management
			if(!filterDataType || dataTypeSet.contains(VesselsToBeneficialOwners.class)) {
				VesselsToBeneficialOwnersExample beneficialOwnerFilter = new VesselsToBeneficialOwnersExample();
				VesselsToBeneficialOwnersExample.Criteria criteria = beneficialOwnerFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				beneficialOwnerFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setBeneficialOwnersData(this.beneficialOwnersDAO.selectByExample(beneficialOwnerFilter));
			}
			
			//Owners management
			if(!filterDataType || dataTypeSet.contains(VesselsToOwners.class)) {
				VesselsToOwnersExample ownerFilter = new VesselsToOwnersExample();
				VesselsToOwnersExample.Criteria criteria = ownerFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				ownerFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setOwnersData(this.ownersDAO.selectByExample(ownerFilter));
			}

			//Operators management
			if(!filterDataType || dataTypeSet.contains(VesselsToOperators.class)) {
				VesselsToOperatorsExample operatorFilter = new VesselsToOperatorsExample();
				VesselsToOperatorsExample.Criteria criteria = operatorFilter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				operatorFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setOperatorsData(this.operatorsDAO.selectByExample(operatorFilter));
			}
			
			//Inspections management
			if(!filterDataType || dataTypeSet.contains(VesselsToInspections.class)) {
				VesselsToInspectionsExample filter = new VesselsToInspectionsExample();
				VesselsToInspectionsExample.Criteria criteria = filter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				filter.setOrderByClause("DATE DESC, REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setInspectionsData(this.inspectionsDAO.selectByExample(filter));
			}
			
			//IUU listings management
			if(!filterDataType || dataTypeSet.contains(VesselsToIuuList.class)) {
				VesselsToIuuListExample filter = new VesselsToIuuListExample();
				VesselsToIuuListExample.Criteria criteria = filter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				filter.setOrderByClause("LISTING_DATE DESC, DELISTING_DATE DESC, REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setIuuListsData(this.iuuListsDAO.selectByExample(filter));
			}
			
			//Port entry denials management
			if(!filterDataType || dataTypeSet.contains(VesselsToPortEntryDenials.class)) {
				VesselsToPortEntryDenialsExample filter = new VesselsToPortEntryDenialsExample();
				VesselsToPortEntryDenialsExample.Criteria criteria = filter.createCriteria().andVesselIdEqualTo(vesselID);

				if(dataSourcesList != null)
					criteria.andSourceSystemIn(Arrays.asList(dataSources));

				filter.setOrderByClause("DATE DESC, REFERENCE_DATE DESC, UPDATE_DATE DESC");

				toReturn.setPortEntryDenialsData(this.portEntryDenialsDAO.selectByExample(filter));
			}
		} catch (Throwable t) {
			this._log.error("Unable to retrieve extended vessel data", t);

			return null;
		}

		return toReturn;
	}

	public ExtendedVessel selectExtendedByUID(Integer vesselUID, String vesselsTable) throws Throwable {
		return this.selectExtendedByUID(vesselUID, null, null, vesselsTable);
	}

	public ExtendedVessel selectExtendedByUID(Integer vesselUID, Collection<Class<?>> dataTypeSet, String vesselsTable) throws Throwable {
		return this.selectExtendedByUID(vesselUID, null, dataTypeSet, vesselsTable);
	}


	public ExtendedVessel selectExtendedByUID(Integer vesselUID, String[] dataSourcesFilter, String vesselsTable) throws Throwable {
		return this.selectExtendedByUID(vesselUID, dataSourcesFilter, null, vesselsTable);
	}

	public ExtendedVessel selectExtendedByUID(Integer vesselUID, String[] dataSources, Collection<Class<?>> dataTypeSet, String vesselsTable) throws Throwable {
		ExtendedVessel toReturn = null;

		VesselRecordSearchFilter filter = new VesselRecordSearchFilter();

		filter.
			FROM(vesselsTable).
//			SELECT(filter.columns().
//					include(CommonDataColumns.ID)).
			WHERE(filter.clauses().
				  and(CommonClauses.VRMF_UIDS, vesselUID)).
			GROUP_BY(GroupableDataColumns.ID);

		if(dataSources != null && dataSources.length > 0)
			filter.SOURCED_BY(dataSources);

		filter.switchToMetadataRetrievement();

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("filter", filter);

		VesselRecordSearchResponse<VesselRecord> identified = this.vesselRecordSearchDAO.searchVessels(filter);

		if(identified == null || identified.isEmpty())
			return null;

		List<Integer> vesselIDs = new ListSet<Integer>();

		for(VesselRecord record : identified.getDataSet())
			vesselIDs.add(record.getId());

		VesselsExample vesselsFilter = new VesselsExample();
		vesselsFilter.createCriteria().andIdIn(vesselIDs);

		List<Vessels> vessels = this.vesselDAO.selectByExample(vesselsFilter);

		//Main vessel data
		toReturn = new ExtendedVessel(vessels.get(0));

		List<Vessels> components = new ListSet<Vessels>();
		List<String> vesselSources = new ListSet<String>();

		for(Vessels current : vessels) {
			components.add(current);
			vesselIDs.add(current.getId());
			vesselSources.add(current.getSourceSystem());

			toReturn.getIds().add(current.getId());
		}

		toReturn.setComponentVessels(components);
		toReturn.setSourceSystems(vesselSources.toArray(new String[0]));
		toReturn.setNumberOfSystems(toReturn.getSourceSystems().length);

		boolean filterDataType = dataTypeSet != null && dataTypeSet.size() > 0;

		List<String> dataSourcesList = null;

		if(dataSources != null && dataSources.length > 0)
			dataSourcesList = Arrays.asList(dataSources);

		//Building Year management
		if(!filterDataType || dataTypeSet.contains(VesselsToBuildingYear.class)) {
			VesselsToBuildingYearExample buildingYearFilter = new VesselsToBuildingYearExample();
			VesselsToBuildingYearExample.Criteria criteria = buildingYearFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			buildingYearFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setBuildingYearData(this.buildingYearDAO.selectByExample(buildingYearFilter));
		}

		//Shipbuilders management
		if(!filterDataType || dataTypeSet.contains(VesselsToShipbuilders.class)) {
			VesselsToShipbuildersExample shipbuilderFilter = new VesselsToShipbuildersExample();
			VesselsToShipbuildersExample.Criteria criteria = shipbuilderFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			shipbuilderFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setShipbuildersData(this.shipbuildersDAO.selectByExample(shipbuilderFilter));
		}

		//Identifiers management
		if(!filterDataType || dataTypeSet.contains(VesselsToIdentifiers.class)) {
			VesselsToIdentifiersExample identifierFilter = new VesselsToIdentifiersExample();
			VesselsToIdentifiersExample.Criteria criteria = identifierFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			identifierFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");
			
			toReturn.setIdentifiers(this.identifiersDAO.selectByExample(identifierFilter));
		}
		
		//Non Compliance management
		if(!filterDataType || dataTypeSet.contains(VesselsToNonCompliance.class)) {
			VesselsToNonComplianceExample nonComplianceFilter = new VesselsToNonComplianceExample();
			VesselsToNonComplianceExample.Criteria criteria = nonComplianceFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			nonComplianceFilter.setOrderByClause("REFERENCE_DATE DESC, DATE DESC, UPDATE_DATE DESC");

			toReturn.setNonComplianceData(this.nonComplianceDAO.selectByExample(nonComplianceFilter));
		}

		//Authorizations management
		if(!filterDataType || dataTypeSet.contains(Authorizations.class)) {
			AuthorizationsExample authorizationFilter = new AuthorizationsExample();
			AuthorizationsExample.Criteria criteria = authorizationFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			authorizationFilter.setOrderByClause("REFERENCE_DATE DESC, VALID_FROM DESC, UPDATE_DATE DESC");

			toReturn.setAuthorizationsData(this.authorizationsDAO.selectByExample(authorizationFilter));
		}

		//BuildingYear management
		if(!filterDataType || dataTypeSet.contains(VesselsToStatus.class)) {
			VesselsToStatusExample statusFilter = new VesselsToStatusExample();
			VesselsToStatusExample.Criteria criteria = statusFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			statusFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setStatusData(this.statusDAO.selectByExample(statusFilter));
		}
		
		//HullMaterial management
		if(!filterDataType || dataTypeSet.contains(VesselsToHullMaterial.class)) {
			VesselsToHullMaterialExample hullFilter = new VesselsToHullMaterialExample();
			VesselsToHullMaterialExample.Criteria criteria = hullFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			hullFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setHullMaterialData(this.hullDAO.selectByExample(hullFilter));
		}

		//Type management
		if(!filterDataType || dataTypeSet.contains(VesselsToTypes.class)) {
			VesselsToTypesExample typeFilter = new VesselsToTypesExample();
			VesselsToTypesExample.Criteria criteria = typeFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			typeFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setTypeData(this.typesDAO.selectByExample(typeFilter));
		}

		//Name management
		if(!filterDataType || dataTypeSet.contains(VesselsToName.class)) {
			VesselsToNameExample nameFilter = new VesselsToNameExample();
			VesselsToNameExample.Criteria criteria = nameFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			nameFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setNameData(this.namesDAO.selectByExample(nameFilter));
		}

		//External Markings management
		if(!filterDataType || dataTypeSet.contains(VesselsToExternalMarkings.class)) {
			VesselsToExternalMarkingsExample externalMarkingsFilter = new VesselsToExternalMarkingsExample();
			VesselsToExternalMarkingsExample.Criteria criteria =  externalMarkingsFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			externalMarkingsFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setExternalMarkingsData(this.externalMarkingsDAO.selectByExample(externalMarkingsFilter));
		}

		//Flag management
		if(!filterDataType || dataTypeSet.contains(VesselsToFlags.class)) {
			VesselsToFlagsExample flagsFilter = new VesselsToFlagsExample();
			VesselsToFlagsExample.Criteria criteria = flagsFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			flagsFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setFlagData(this.flagsDAO.selectByExample(flagsFilter));
		}

		//Registration management
		if(!filterDataType || dataTypeSet.contains(VesselsToRegistrations.class)) {
			VesselsToRegistrationsExample registrationFilter = new VesselsToRegistrationsExample();
			VesselsToRegistrationsExample.Criteria criteria = registrationFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			registrationFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setRegistrationData(this.registrationsDAO.selectByExample(registrationFilter));
		}

		//Fishing license management
		if(!filterDataType || dataTypeSet.contains(VesselsToFishingLicense.class)) {
			VesselsToFishingLicenseExample fishingLicenseFilter = new VesselsToFishingLicenseExample();
			VesselsToFishingLicenseExample.Criteria criteria = fishingLicenseFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			fishingLicenseFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setFishingLicenseData(this.fishingLicenseDAO.selectByExample(fishingLicenseFilter));
		}

		//Callsign management
		if(!filterDataType || dataTypeSet.contains(VesselsToCallsigns.class)) {
			VesselsToCallsignsExample callsignFilter = new VesselsToCallsignsExample();
			VesselsToCallsignsExample.Criteria criteria = callsignFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			callsignFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setCallsignData(this.callsignsDAO.selectByExample(callsignFilter));
		}

		//MMSI management
		if(!filterDataType || dataTypeSet.contains(VesselsToMmsi.class)) {
			VesselsToMmsiExample MMSIFilter = new VesselsToMmsiExample();
			VesselsToMmsiExample.Criteria criteria = MMSIFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			MMSIFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setMMSIData(this.MMSIDAO.selectByExample(MMSIFilter));
		}
		
		//VMS management
		if(!filterDataType || dataTypeSet.contains(VesselsToVms.class)) {
			VesselsToVmsExample VMSFilter = new VesselsToVmsExample();
			VesselsToVmsExample.Criteria criteria = VMSFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			VMSFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setVMSData(this.VMSDAO.selectByExample(VMSFilter));
		}

		//Crew management
		if(!filterDataType || dataTypeSet.contains(VesselsToCrew.class)) {
			VesselsToCrewExample crewFilter = new VesselsToCrewExample();
			VesselsToCrewExample.Criteria criteria = crewFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			crewFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setCrewData(this.crewDAO.selectByExample(crewFilter));
		}

		//Gear management
		if(!filterDataType || dataTypeSet.contains(VesselsToGears.class)) {
			VesselsToGearsExample gearFilter = new VesselsToGearsExample();
			VesselsToGearsExample.Criteria criteria = gearFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			gearFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setGearData(this.gearsDAO.selectByExample(gearFilter));
		}

		//Length management
		if(!filterDataType || dataTypeSet.contains(VesselsToLengths.class)) {
			VesselsToLengthsExample lengthFilter = new VesselsToLengthsExample();
			VesselsToLengthsExample.Criteria criteria = lengthFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			lengthFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setLengthData(this.lengthsDAO.selectByExample(lengthFilter));
		}

		//Tonnage management
		if(!filterDataType || dataTypeSet.contains(VesselsToTonnage.class)) {
			VesselsToTonnageExample tonnageFilter = new VesselsToTonnageExample();
			VesselsToTonnageExample.Criteria criteria = tonnageFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			tonnageFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setTonnageData(this.tonnageDAO.selectByExample(tonnageFilter));
		}

		//Power management
		if(!filterDataType || dataTypeSet.contains(VesselsToPower.class)) {
			VesselsToPowerExample powerFilter = new VesselsToPowerExample();
			VesselsToPowerExample.Criteria criteria = powerFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			powerFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setPowerData(this.powerDAO.selectByExample(powerFilter));
		}
		
		//Masters management
		if(!filterDataType || dataTypeSet.contains(VesselsToMasters.class)) {
			VesselsToMastersExample mastersFilter = new VesselsToMastersExample();
			VesselsToMastersExample.Criteria criteria = mastersFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			mastersFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setMastersData(this.mastersDAO.selectByExample(mastersFilter));
		}
		
		//Fishing Masters management
		if(!filterDataType || dataTypeSet.contains(VesselsToFishingMasters.class)) {
			VesselsToFishingMastersExample fishingMastersFilter = new VesselsToFishingMastersExample();
			VesselsToFishingMastersExample.Criteria criteria = fishingMastersFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			fishingMastersFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setFishingMastersData(this.fishingMastersDAO.selectByExample(fishingMastersFilter));
		}

		//Beneficial Owners management
		if(!filterDataType || dataTypeSet.contains(VesselsToBeneficialOwners.class)) {
			VesselsToBeneficialOwnersExample beneficialOwnerFilter = new VesselsToBeneficialOwnersExample();
			VesselsToBeneficialOwnersExample.Criteria criteria = beneficialOwnerFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			beneficialOwnerFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setBeneficialOwnersData(this.beneficialOwnersDAO.selectByExample(beneficialOwnerFilter));
		}
		
		//Owners management
		if(!filterDataType || dataTypeSet.contains(VesselsToOwners.class)) {
			VesselsToOwnersExample ownerFilter = new VesselsToOwnersExample();
			VesselsToOwnersExample.Criteria criteria = ownerFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			ownerFilter.setOrderByClause("REFERENCE_DATE DESC, VALID_FROM DESC, UPDATE_DATE DESC");

			toReturn.setOwnersData(this.ownersDAO.selectByExample(ownerFilter));
		}

		//Operators management
		if(!filterDataType || dataTypeSet.contains(VesselsToOperators.class)) {
			VesselsToOperatorsExample operatorFilter = new VesselsToOperatorsExample();
			VesselsToOperatorsExample.Criteria criteria = operatorFilter.createCriteria().andVesselIdIn(vesselIDs);

			if(dataSourcesList != null)
				criteria.andSourceSystemIn(Arrays.asList(dataSources));

			operatorFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");

			toReturn.setOperatorsData(this.operatorsDAO.selectByExample(operatorFilter));
		}

		return toReturn;
	}

	public ExtendedVessel selectExtendedByUID(Integer vesselUID) throws Throwable {
		return this.selectExtendedByUID(vesselUID, null, null, null);
	}

	public ExtendedVessel selectExtendedByUID(Integer vesselUID, String[] dataSourcesFilter) throws Throwable {
		return this.selectExtendedByUID(vesselUID, dataSourcesFilter, null, null);
	}

	public ExtendedVessel selectExtendedByUID(Integer vesselUID, Collection<Class<?>> dataTypeSet) throws Throwable {
		return this.selectExtendedByUID(vesselUID, null, dataTypeSet, null);
	}

	public ExtendedVessel selectExtendedByUID(Integer vesselUID, String[] dataSources, Collection<Class<?>> dataTypeSet) throws Throwable {
		return this.selectExtendedByUID(vesselUID, dataSources, dataTypeSet, null);
	}


	@Override
	public List<ExtendedVessel> selectExtendedByDataSource(String[] dataSources) throws Throwable {
		return this.selectExtendedByDataSource(dataSources, null);
	}

	/* (non-Javadoc)
	 * @see org.fao.gvr2.business.dao.ExtendedVesselDAO#selectByDataSource(java.lang.String[])
	 */
	@Override
	public List<ExtendedVessel> selectExtendedByDataSource(String[] dataSources, Collection<Class<?>> dataTypeSet) throws Throwable {
		VesselsExample filter = new VesselsExample();
		Criteria criteria = filter.createCriteria();

		if(dataSources != null && dataSources.length > 0)
			criteria.andSourceSystemIn(Arrays.asList(dataSources));

		return this.selectExtendedByExample(filter);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectExtendedByExample(org.fao.vrmf.utilities.common.models.generated.VesselsExample)
	 */
	@Override
	public List<ExtendedVessel> selectExtendedByExample(VesselsExample filter) throws Throwable {
		return this.selectExtendedByExample(filter, null, null);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectExtendedByExample(org.fao.vrmf.utilities.common.models.generated.VesselsExample)
	 */
	@Override
	public List<ExtendedVessel> selectExtendedByExample(VesselsExample filter, Collection<Class<?>> dataTypeSet) throws Throwable {
		return this.selectExtendedByExample(filter, null, dataTypeSet);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectExtendedByExample(org.fao.vrmf.utilities.common.models.generated.VesselsExample, java.lang.String[])
	 */
	@Override
	public List<ExtendedVessel> selectExtendedByExample(VesselsExample filter, String[] dataSourcesFilter) throws Throwable {
		return this.selectExtendedByExample(filter, dataSourcesFilter, null);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectExtendedByExample(org.fao.vrmf.utilities.common.models.generated.VesselsExample, java.lang.String[], java.util.Collection)
	 */
	@Override
	public List<ExtendedVessel> selectExtendedByExample(VesselsExample filter, String[] dataSourcesFilter, Collection<Class<?>> dataTypeSet) throws Throwable {
		List<Vessels> vessels = this.vesselDAO.selectByExample(filter);

		List<ExtendedVessel> toReturn = new ArrayList<ExtendedVessel>();

		this._log.info(vessels.size() + " vessels selected: converting to Extended Vessels...");

		int counter = 0;
		for(Vessels vessel : vessels) {
			toReturn.add(this.selectExtendedByPrimaryKey(vessel.getId(), dataSourcesFilter, dataTypeSet));

			counter++;

			if(counter % 1000 == 0)
				this._log.info(counter + " Vessels converted to Extended Vessels so far...");
		}

		this._log.info(counter + " Vessels converted to Extended Vessels so far...");

		return toReturn;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectVesselIDs(java.lang.String[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> selectVesselIDs(String[] sources) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sourceSystems", sources);

		return this.getSqlMapClient().queryForList("searchVessels.selectVesselIDs", params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectMultipleSourcedVesselIDs(java.lang.String[], java.lang.String[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> selectMultipleSourcedVesselIDs(String[] originalSources, String[] additionalSources) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("originalSourceSystems", originalSources);
		params.put("additionalSourceSystems", additionalSources);

		return this.getSqlMapClient().queryForList("searchVessels.selectMultipleSourcedVesselIDs", params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectVesselsWithIMOOrEUCFRIDs(java.lang.String[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> selectVesselsWithIMOOrEUCFRIDs(String[] sources) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sourceSystems", sources);

		return this.getSqlMapClient().queryForList("searchVessels.selectVesselsWithIMOOrEUCFR", params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectVesselsWithIMOAndEUCFRIDs(java.lang.String[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> selectVesselsWithIMOAndEUCFRIDs(String[] sources) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sourceSystems", sources);

		return this.getSqlMapClient().queryForList("searchVessels.selectVesselsWithIMOAndEUCFR", params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectVesselsWithIMOIDs(java.lang.String[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> selectVesselsWithIMOIDs(String[] sources) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sourceSystems", sources);

		return this.getSqlMapClient().queryForList("searchVessels.selectVesselsWithIMO", params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedVesselsDAO#selectVesselsWithEUCFRIDs(java.lang.String[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> selectVesselsWithEUCFRIDs(String[] sources) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sourceSystems", sources);

		return this.getSqlMapClient().queryForList("searchVessels.selectVesselsWithEUCFR", params);
	}
}