/**
 * (c) 2016 FAO / UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.common.helpers.singletons.text.StringDistanceHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.vrmf.business.dao.VesselOwnerFuzzyAutocompletionDAO;
import org.fao.fi.vrmf.common.models.search.autocompletion.FuzzySmartEntityNamedSearchResult;
import org.fao.fi.vrmf.common.tools.lexical.processors.queue.impl.ICUEntityNameSimplifier;
import org.springframework.cache.annotation.Cacheable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Feb 12, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Feb 12, 2016
 */
@NoArgsConstructor
@Named @Singleton
public class VesselOwenerFuzzyAutocompletionDAOImpl extends AbstractAutocompletionDAOImpl implements VesselOwnerFuzzyAutocompletionDAO {
	@Inject private @Getter @Setter ICUEntityNameSimplifier entityNamesSimplifier;

	public String getProcedureName(boolean authorizedOnly) {
		return "FUZZY_OWNER_NAME_SEARCH" + ( authorizedOnly ? "_BY_AUTH" : "");
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.VesselOperatorFuzzyAutocompletionDAO#searchVesselNames(java.lang.String, java.util.Date, java.lang.Integer, java.lang.String[], java.lang.Boolean)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "#ownerName + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID")
	public Collection<FuzzySmartEntityNamedSearchResult> searchOwnerNames(String ownerName, Date atDate, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		return this.searchOwnerNames(this.getProcedureName(false), ownerName, atDate, limitToMaxItems, sourceSystems, groupByUID);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.VesselOperatorFuzzyAutocompletionDAO#searchVesselNames(java.lang.String, java.lang.String, java.util.Date, java.lang.Integer, java.lang.String[], java.lang.Boolean)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															   "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															   "#procedureName + " +
															   "#ownerName + " +
															   "#atDate + " +
															   "#limitToMaxItems + " + 
															   "#groupByUID")
	public Collection<FuzzySmartEntityNamedSearchResult> searchOwnerNames(String procedureName, String ownerName, Date atDate, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		long end, start = System.currentTimeMillis();
		
		final String simplifiedName = this.entityNamesSimplifier.process(ownerName);

		this._log.info("Autocompleting owner name for '" + ownerName + "' / '" + simplifiedName + "' (procedure name: " + procedureName + ", sources: " + CollectionsHelper.serializeArray(sourceSystems) + ", at date: " + atDate + ", group by UID: " + groupByUID + ")");

		Connection c = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {			
			String CALL_TEMPLATE = "CALL " + ( procedureName == null ? this.getProcedureName(false) : procedureName ) + "(?, ?, ?, ?, ?)";
						
			c = this.sqlMapClient.getDataSource().getConnection();
			
			cs = c.prepareCall(CALL_TEMPLATE);
			
			int counter = 1;
			
			this.setUnicodeString(cs, counter++, ownerName);
			this.setUnicodeString(cs, counter++, simplifiedName);
			
			if(sourceSystems != null && sourceSystems.length > 0)
				cs.setString(counter++, "@" + CollectionsHelper.join(sourceSystems, "@") + "@");
			else 
				cs.setNull(counter++, Types.VARCHAR);
			
			if(atDate != null)
				cs.setDate(counter++, new java.sql.Date(atDate.getTime()));
			else
				cs.setNull(counter++, Types.DATE);

			cs.setBoolean(counter++, groupByUID);

			List<FuzzySmartEntityNamedSearchResult> vesselNameData = new ArrayList<FuzzySmartEntityNamedSearchResult>();
			
			if(cs.execute()) {
				rs = cs.getResultSet();
								
				FuzzySmartEntityNamedSearchResult current = null;
				
				double currentWeight, updatedWeight;
				
				while(rs.next()) {
					current = new FuzzySmartEntityNamedSearchResult();

					current.setId(rs.getInt("ID"));
					current.setEntityTypeId(rs.getString("ENTITY_TYPE_ID"));
					
					current.setName(rs.getString("NAME"));
					current.setSimplifiedName(rs.getString("SIMPLIFIED_NAME"));
					current.setSimplifiedNameSoundex(rs.getString("SIMPLIFIED_NAME_SOUNDEX"));
					current.setAddress(rs.getString("ADDRESS"));
					current.setCity(rs.getString("CITY"));
					current.setZipCode(rs.getString("ZIP_CODE"));
					current.setCountryId(rs.getObject("COUNTRY_ID") == null ? null : rs.getInt("COUNTRY_ID"));
					current.setBranch(rs.getString("BRANCH"));
					current.setSalutation(rs.getString("SALUTATION"));
					current.setRole(rs.getString("ROLE"));
					current.setOccurrencies(rs.getInt("OCCURRENCIES"));
					current.setTotalOccurrencies(rs.getInt("TOTAL_OCCURRENCIES"));
					current.setGroupedOccurrencies(rs.getInt("GROUPED_OCCURRENCIES"));
					
					currentWeight = rs.getDouble("RESULT_WEIGHT");
					
					if(Double.compare(currentWeight, 100D) == 0)
						current.setScore(currentWeight);
					else {
						if(simplifiedName != null)
							updatedWeight = 100D * (1 - StringDistanceHelper.computeRelativeDistance(current.getSimplifiedName(), simplifiedName));
						else
							updatedWeight = 100D * (1 - StringDistanceHelper.computeRelativeDistance(current.getName().toUpperCase(), ownerName.toUpperCase()));
						
						current.setScore(updatedWeight);
					}
					
					vesselNameData.add(current);
				}
			}

			Collections.sort(vesselNameData, new Comparator<FuzzySmartEntityNamedSearchResult>() {
				@Override
				public int compare(FuzzySmartEntityNamedSearchResult o1, FuzzySmartEntityNamedSearchResult o2) {
					int score = o2.getScore().compareTo(o1.getScore());
					
					if(Double.compare(score, 0D) != 0)
						return score;
					
					score = o2.getTotalOccurrencies().compareTo(o1.getTotalOccurrencies());
					
					if(Double.compare(score, 0D) != 0)
						return score;
					
					if(o2.getName() != null)
						return o2.getName().compareTo(o1.getName());
					
					if(o1.getName() != null)
						return -o1.getName().compareTo(o2.getName());
					
					return 0;
				}
			});
			
			return this.trim(vesselNameData, limitToMaxItems);
		} catch (Throwable t) {
			this._log.error("Unable to find owner names", t);
			
			return new ArrayList<FuzzySmartEntityNamedSearchResult>();
		} finally {
			this.cleanup(rs, cs, c);
			
			end = System.currentTimeMillis();
			
			this._log.info("Autocompleting owner name for '" + ownerName + "' / '" + simplifiedName + "' took " + ( end - start ) + " mSec.");
		}
	}

}
