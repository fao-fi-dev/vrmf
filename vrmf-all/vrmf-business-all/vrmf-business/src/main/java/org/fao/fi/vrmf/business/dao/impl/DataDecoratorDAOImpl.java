/**
 * (c) 2016 FAO / UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.jws.WebMethod;

import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.business.dao.DataDecoratorDAO;
import org.fao.fi.vrmf.business.dao.ExtendedPortsDAO;
import org.fao.fi.vrmf.business.dao.generated.AuthorizationDetailsDAO;
import org.fao.fi.vrmf.business.dao.generated.EntityDAO;
import org.fao.fi.vrmf.business.dao.generated.SAuthorityRoleDAO;
import org.fao.fi.vrmf.business.dao.generated.SAuthorizationHoldersDAO;
import org.fao.fi.vrmf.business.dao.generated.SAuthorizationTerminationReasonsDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesDAO;
import org.fao.fi.vrmf.business.dao.generated.SFishingZonesDAO;
import org.fao.fi.vrmf.business.dao.generated.SGearTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SHullMaterialDAO;
import org.fao.fi.vrmf.business.dao.generated.SInspectionInfringementTypeDAO;
import org.fao.fi.vrmf.business.dao.generated.SInspectionOutcomeTypeDAO;
import org.fao.fi.vrmf.business.dao.generated.SInspectionReportTypeDAO;
import org.fao.fi.vrmf.business.dao.generated.SLengthTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SNonComplianceInfringementTypeDAO;
import org.fao.fi.vrmf.business.dao.generated.SNonComplianceOutcomeTypeDAO;
import org.fao.fi.vrmf.business.dao.generated.SNonComplianceSourceTypeDAO;
import org.fao.fi.vrmf.business.dao.generated.SPortEntryDenialReasonDAO;
import org.fao.fi.vrmf.business.dao.generated.SPowerTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SRfmoIuuListsDAO;
import org.fao.fi.vrmf.business.dao.generated.SSpeciesDAO;
import org.fao.fi.vrmf.business.dao.generated.SSystemsDAO;
import org.fao.fi.vrmf.business.dao.generated.STonnageTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SVesselStatusDAO;
import org.fao.fi.vrmf.business.dao.generated.SVesselTypesDAO;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyAuthorityRolesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyAuthorizationHoldersMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyAuthorizationTerminationReasonsMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyCountriesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyEntitiesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyExtendedPortsMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyFishingZonesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyGearTypesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyHullMaterialsMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyInspectionInfringementTypesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyInspectionOutcomeTypesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyInspectionReportTypesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyLengthTypesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyNonComplianceInfringementTypesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyNonComplianceOutcomeTypesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyNonComplianceSourceTypesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyPortEntryDenialReasonsMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyPowerTypesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyRFMOIUUListMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazySpeciesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazySystemsMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyTonnageTypesMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyVesselStatusMap;
import org.fao.fi.vrmf.business.extensions.maps.lazy.LazyVesselTypesMap;
import org.fao.fi.vrmf.common.models.extended.ExtendedAuthorizationDetails;
import org.fao.fi.vrmf.common.models.extended.ExtendedAuthorizations;
import org.fao.fi.vrmf.common.models.extended.ExtendedEntity;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToBeneficialOwner;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToCallsigns;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToFishingLicense;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToFishingMaster;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToFlags;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToGear;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToHullMaterial;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToInspection;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToIuuList;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToLengths;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToMaster;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToNonCompliance;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToOperator;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToOwner;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToPortEntryDenial;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToPower;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToRegistration;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToShipbuilder;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToStatus;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToTonnage;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToType;
import org.fao.fi.vrmf.common.models.generated.AuthorizationDetails;
import org.fao.fi.vrmf.common.models.generated.AuthorizationDetailsExample;
import org.fao.fi.vrmf.common.models.generated.Authorizations;
import org.fao.fi.vrmf.common.models.generated.Entity;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.VesselsToBeneficialOwners;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToFishingLicense;
import org.fao.fi.vrmf.common.models.generated.VesselsToFishingMasters;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToGears;
import org.fao.fi.vrmf.common.models.generated.VesselsToHullMaterial;
import org.fao.fi.vrmf.common.models.generated.VesselsToInspections;
import org.fao.fi.vrmf.common.models.generated.VesselsToIuuList;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToMasters;
import org.fao.fi.vrmf.common.models.generated.VesselsToNonCompliance;
import org.fao.fi.vrmf.common.models.generated.VesselsToOperators;
import org.fao.fi.vrmf.common.models.generated.VesselsToOwners;
import org.fao.fi.vrmf.common.models.generated.VesselsToPortEntryDenials;
import org.fao.fi.vrmf.common.models.generated.VesselsToPower;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;
import org.fao.fi.vrmf.common.models.generated.VesselsToShipbuilders;
import org.fao.fi.vrmf.common.models.generated.VesselsToStatus;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;
import org.fao.fi.vrmf.common.models.generated.VesselsToTypes;
import org.springframework.beans.factory.InitializingBean;

import com.ibatis.sqlmap.client.SqlMapClient;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Feb 9, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Feb 9, 2016
 */
@Singleton @Named("dao.decorator")
public class DataDecoratorDAOImpl extends AbstractLoggingAwareClient implements InitializingBean, DataDecoratorDAO {
	final static private SCountries NO_NATIONALITY_PROVIDED = null;
	
	@Inject protected @Getter @Setter EntityDAO entityDAO;
	@Inject protected @Getter @Setter AuthorizationDetailsDAO authorizationDetailsDAO;
	
	@Inject protected @Getter @Setter SAuthorityRoleDAO authorityRoleDAO;
	@Inject protected @Getter @Setter SAuthorizationHoldersDAO authorizationHoldersDAO;
	@Inject protected @Getter @Setter SAuthorizationTerminationReasonsDAO authorizationTerminationReasonsDAO;
	@Inject protected @Getter @Setter SCountriesDAO countriesDAO;
	@Inject protected @Getter @Setter SFishingZonesDAO fishingZonesDAO;
	@Inject protected @Getter @Setter SGearTypesDAO gearTypesDAO;
	@Inject protected @Getter @Setter SHullMaterialDAO hullMaterialsDAO;
	@Inject protected @Getter @Setter SInspectionInfringementTypeDAO inspectionInfringementTypeDAO;
	@Inject protected @Getter @Setter SInspectionOutcomeTypeDAO inspectionOutcomeTypeDAO;
	@Inject protected @Getter @Setter SInspectionReportTypeDAO inspectionReportTypeDAO;
	@Inject protected @Getter @Setter SLengthTypesDAO lengthTypesDAO;
	@Inject protected @Getter @Setter SNonComplianceInfringementTypeDAO nonComplianceInfringementTypeDAO;
	@Inject protected @Getter @Setter SNonComplianceOutcomeTypeDAO nonComplianceOutcomeTypeDAO;
	@Inject protected @Getter @Setter SNonComplianceSourceTypeDAO nonComplianceSourceTypeDAO;
	@Inject protected @Getter @Setter ExtendedPortsDAO portsDAO;
	@Inject protected @Getter @Setter SPortEntryDenialReasonDAO portEntryDenialReasonDAO;
	@Inject protected @Getter @Setter SPowerTypesDAO powerTypesDAO;
	@Inject protected @Getter @Setter SRfmoIuuListsDAO rfmoIuuListsDAO;
	@Inject protected @Getter @Setter SSpeciesDAO speciesDAO;
	@Inject protected @Getter @Setter SSystemsDAO systemsDAO;
	@Inject protected @Getter @Setter STonnageTypesDAO tonnageTypesDAO;
	@Inject protected @Getter @Setter SVesselStatusDAO statusTypesDAO;
	@Inject protected @Getter @Setter SVesselTypesDAO vesselTypesDAO;
	
	private LazyAuthorityRolesMap _authorityRolesMap;
	private LazyAuthorizationHoldersMap _authorizationHoldersMap;
	private LazyAuthorizationTerminationReasonsMap _authorizationTerminationReasonsMap;
	private LazyCountriesMap _countriesMap;
	private LazyEntitiesMap _entitiesMap;
	private LazyExtendedPortsMap _extendedPortsMap;
	private LazyFishingZonesMap _fishingZonesMap;
	private LazyGearTypesMap _gearTypesMap;
	private LazyHullMaterialsMap _hullMaterialsMap;
	private LazyInspectionInfringementTypesMap _inspectionInfringementTypesMap;
	private LazyInspectionOutcomeTypesMap _inspectionOutcomeTypesMap;
	private LazyInspectionReportTypesMap _inspectionReportTypesMap;
	private LazyLengthTypesMap _lengthTypesMap;
	private LazyNonComplianceInfringementTypesMap _nonComplianceInfringementTypesMap;
	private LazyNonComplianceOutcomeTypesMap _nonComplianceOutcomeTypesMap;
	private LazyNonComplianceSourceTypesMap _nonComplianceSourceTypesMap;
	private LazyPortEntryDenialReasonsMap _denialReasonsMap;
	private LazyPowerTypesMap _powerTypesMap;
	private LazyRFMOIUUListMap _rfmoIuuListsMap;
	private LazySpeciesMap _speciesMap;
	private LazyVesselStatusMap _statusMap;
	private LazySystemsMap _systemsMap;
	private LazyTonnageTypesMap _tonnageTypesMap;
	private LazyVesselTypesMap _vesselTypesMap;

	private SqlMapClient sqlMapClient;

	@Inject
	@Named("vrmf.sqlMapClient")
	public void setSqlMapClient(SqlMapClient sqlMapClient) {
		this.sqlMapClient = sqlMapClient;
	}

	public SqlMapClient getSqlMapClient() {
		return sqlMapClient;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	@WebMethod(exclude = true)
	public void afterPropertiesSet() throws Exception {
		try {
			this._systemsMap = new LazySystemsMap(this.systemsDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy systems map", t);
		}
		
		try {
			this._countriesMap = new LazyCountriesMap(this.countriesDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy countries map", t);
		}
	
		try {
			this._lengthTypesMap = new LazyLengthTypesMap(this.lengthTypesDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy length types map", t);
		}
	
		try {
			this._tonnageTypesMap = new LazyTonnageTypesMap(this.tonnageTypesDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy tonnage types map", t);
		}
	
		try {
			this._powerTypesMap = new LazyPowerTypesMap(this.powerTypesDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy power types map", t);
		}
	
		try {
			this._statusMap = new LazyVesselStatusMap(this.statusTypesDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy status map", t);
		}
	
		try {
			this._extendedPortsMap = new LazyExtendedPortsMap(this.portsDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy ports map", t);
		}
	
		try {
			this._gearTypesMap = new LazyGearTypesMap(this.gearTypesDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy gear types map", t);
		}
		
		try {
			this._hullMaterialsMap = new LazyHullMaterialsMap(this.hullMaterialsDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy hull materials map", t);
		}
	
		try {
			this._vesselTypesMap = new LazyVesselTypesMap(this.vesselTypesDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy vessel types map", t);
		}
	
		try {
			this._entitiesMap = new LazyEntitiesMap(this.entityDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy entities map", t);
		}
	
		try {
			this._fishingZonesMap = new LazyFishingZonesMap(this.fishingZonesDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy fishing zones map", t);
		}
	
		try {
			this._speciesMap = new LazySpeciesMap(this.speciesDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy species map", t);
		}
		
		try {
			this._authorityRolesMap = new LazyAuthorityRolesMap(this.authorityRoleDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy authority roles map", t);
		}
	
		try {
			this._authorizationHoldersMap = new LazyAuthorizationHoldersMap(this.authorizationHoldersDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy authorization holders map", t);
		}
		
		try {
			this._authorizationTerminationReasonsMap = new LazyAuthorizationTerminationReasonsMap(this.authorizationTerminationReasonsDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy authorization types map", t);
		}
		
		try {
			this._inspectionInfringementTypesMap = new LazyInspectionInfringementTypesMap(this.inspectionInfringementTypeDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy inspection infringement types map", t);
		}
		
		try {
			this._inspectionReportTypesMap = new LazyInspectionReportTypesMap(this.inspectionReportTypeDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy inspection source types map", t);
		}
		
		try {
			this._inspectionOutcomeTypesMap = new LazyInspectionOutcomeTypesMap(this.inspectionOutcomeTypeDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy inspection outcome types map", t);
		}
		
		try {
			this._rfmoIuuListsMap = new LazyRFMOIUUListMap(this.rfmoIuuListsDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy RFMO IUU lists map", t);
		}
		
		try {
			this._denialReasonsMap = new LazyPortEntryDenialReasonsMap(this.portEntryDenialReasonDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy port entry denial reasons map", t);
		}
		
		try {
			this._nonComplianceInfringementTypesMap = new LazyNonComplianceInfringementTypesMap(this.nonComplianceInfringementTypeDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy non compliance infringement types map", t);
		}
		
		try {
			this._nonComplianceSourceTypesMap = new LazyNonComplianceSourceTypesMap(this.nonComplianceSourceTypeDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy non compliance source types map", t);
		}
		
		try {
			this._nonComplianceOutcomeTypesMap = new LazyNonComplianceOutcomeTypesMap(this.nonComplianceOutcomeTypeDAO);
		} catch (Throwable t) {
			this._log.error("Unable to initialize lazy non compliance outcome types map", t);
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildAuthorizations(java.util.Collection)
	 */
	@Override
	public List<ExtendedAuthorizations> buildAuthorizations(Collection<Authorizations> authorizationsData) throws Throwable {
		if(authorizationsData == null)
			return null;
	
		List<ExtendedAuthorizations> authorizations = new ListSet<ExtendedAuthorizations>();
		List<AuthorizationDetails> details;
		List<ExtendedAuthorizationDetails> extendedDetails;
		AuthorizationDetailsExample detailFilter;
	
		for(Authorizations current : authorizationsData) {
			extendedDetails = null;
	
			if(current.getId() != null) {
				detailFilter = new AuthorizationDetailsExample();
				detailFilter.createCriteria().andAuthIdEqualTo(current.getId());
	
				details = authorizationDetailsDAO.selectByExample(detailFilter);
	
				extendedDetails = new ArrayList<ExtendedAuthorizationDetails>();
	
				for(AuthorizationDetails detail : details) {
					extendedDetails.add(new ExtendedAuthorizationDetails(detail, 
																		 detail.getAuthorizedGearId() == null ? null : this._gearTypesMap.get(detail.getAuthorizedGearId()),
																		 detail.getAuthorizedZoneId() == null ? null : this._fishingZonesMap.get(detail.getAuthorizedZoneId()), 
																		 detail.getAuthorizedSpecieId() == null ? null : this._speciesMap.get(detail.getAuthorizedSpecieId())));
				}
			}
	
			authorizations.add(
				new ExtendedAuthorizations(
					current, 
					this._countriesMap.get(current.getIssuingCountryId()), 
					current.getAuthorizationHolderId() == null ? null : this._authorizationHoldersMap.get(current.getAuthorizationHolderId()),
					current.getTerminationReasonCode() == null ? null : this._authorizationTerminationReasonsMap.get(current.getTerminationReasonCode()), 
					extendedDetails
				)
			);
		}
	
		return authorizations;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildAuthorizationsOnly(java.util.Collection)
	 */
	@Override
	public List<ExtendedAuthorizations> buildAuthorizationsOnly(Collection<Authorizations> authorizationsData) throws Throwable {
		if(authorizationsData == null)
			return null;
	
		List<ExtendedAuthorizations> authorizations = new ListSet<ExtendedAuthorizations>();
		List<AuthorizationDetails> details;
		List<ExtendedAuthorizationDetails> extendedDetails;
		AuthorizationDetailsExample detailFilter;
	
		for(Authorizations current : authorizationsData) {
			extendedDetails = null;
	
			if(current.getId() != null) {
				detailFilter = new AuthorizationDetailsExample();
				detailFilter.createCriteria().andAuthIdEqualTo(current.getId());
	
				details = authorizationDetailsDAO.selectByExample(detailFilter);
	
				extendedDetails = new ArrayList<ExtendedAuthorizationDetails>();
	
				for(AuthorizationDetails detail : details) {
					extendedDetails.add(new ExtendedAuthorizationDetails(detail, 
																		 this._gearTypesMap.get(detail.getAuthorizedGearId()),
																		 this._fishingZonesMap.get(detail.getAuthorizedZoneId()), 
																		 this._speciesMap.get(detail.getAuthorizedSpecieId())));
				}
			}
	
			authorizations.add(
				new ExtendedAuthorizations(
					current, 
					this._countriesMap.get(current.getIssuingCountryId()), 
					this._authorizationHoldersMap.get(current.getAuthorizationHolderId()), 
					this._authorizationTerminationReasonsMap.get(current.getTerminationReasonCode()), 
					extendedDetails
				)
			);
		}
	
		return authorizations;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildBeneficialOwners(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToBeneficialOwner> buildBeneficialOwners(Collection<VesselsToBeneficialOwners> beneficialOwnersData) throws Throwable {
		if(beneficialOwnersData == null)
			return null;
	
		List<ExtendedVesselToBeneficialOwner> beneficialOwners = new ListSet<ExtendedVesselToBeneficialOwner>();
	
		Entity entity = null;
	
		for(VesselsToBeneficialOwners current : beneficialOwnersData) {
			entity = current.getEntityId() == null ? null : this._entitiesMap.get(current.getEntityId());
	
			if(entity != null)
				beneficialOwners.add(new ExtendedVesselToBeneficialOwner(current, new ExtendedEntity(entity, entity.getNationalityId() == null ? NO_NATIONALITY_PROVIDED : this._countriesMap.get(entity.getNationalityId()), entity.getCountryId() == null ? null : this._countriesMap.get(entity.getCountryId()))));
		}
	
		return beneficialOwners;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildCallsigns(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToCallsigns> buildCallsigns(Collection<VesselsToCallsigns> callsignData) throws Throwable {
		if(callsignData == null)
			return null;
	
		List<ExtendedVesselToCallsigns> callsigns = new ListSet<ExtendedVesselToCallsigns>();
	
		for(VesselsToCallsigns current : callsignData) {
			callsigns.add(new ExtendedVesselToCallsigns(current, current.getCountryId() == null ? null : this._countriesMap.get(current.getCountryId())));
		}
	
		return callsigns;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildFishingLicenses(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToFishingLicense> buildFishingLicenses(Collection<VesselsToFishingLicense> fishingLicenseData) throws Throwable {
		if(fishingLicenseData == null)
			return null;
	
		List<ExtendedVesselToFishingLicense> fishingLicenses = new ListSet<ExtendedVesselToFishingLicense>();
	
		for(VesselsToFishingLicense current : fishingLicenseData) {
			fishingLicenses.add(new ExtendedVesselToFishingLicense(current, current.getCountryId() == null ? null : this._countriesMap.get(current.getCountryId())));
		}
	
		return fishingLicenses;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildFishingMasters(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToFishingMaster> buildFishingMasters(Collection<VesselsToFishingMasters> fishingMastersData) throws Throwable {
		if(fishingMastersData == null)
			return null;
	
		List<ExtendedVesselToFishingMaster> fishingMasters = new ListSet<ExtendedVesselToFishingMaster>();
	
		Entity entity = null;
	
		for(VesselsToFishingMasters current : fishingMastersData) {
			entity = current.getEntityId() == null ? null : this._entitiesMap.get(current.getEntityId());
	
			if(entity != null)
				fishingMasters.add(new ExtendedVesselToFishingMaster(current, new ExtendedEntity(entity, entity.getNationalityId() == null ? NO_NATIONALITY_PROVIDED : this._countriesMap.get(entity.getNationalityId()), entity.getCountryId() == null ? null : this._countriesMap.get(entity.getCountryId()))));
		}
	
		return fishingMasters;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildHullMaterials(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToHullMaterial> buildHullMaterials(Collection<VesselsToHullMaterial> vesselHullMaterialData) throws Throwable {
		if(vesselHullMaterialData == null)
			return null;
	
		List<ExtendedVesselToHullMaterial> vesselHullMaterials = new ListSet<ExtendedVesselToHullMaterial>();
	
		for(VesselsToHullMaterial current : vesselHullMaterialData) {
			vesselHullMaterials.add(new ExtendedVesselToHullMaterial(current, this._hullMaterialsMap.get(current.getTypeId())));
		}
	
		return vesselHullMaterials;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildInspections(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToInspection> buildInspections(Collection<VesselsToInspections> inspectionData) throws Throwable {
		if(inspectionData == null)
			return null;
	
		List<ExtendedVesselToInspection> vesselInspection = new ListSet<ExtendedVesselToInspection>();
	
		for(VesselsToInspections current : inspectionData) {
			vesselInspection.add(new ExtendedVesselToInspection(current, 
																this._inspectionReportTypesMap.get(current.getReportTypeId()),
																this._countriesMap.get(current.getCountryId()),
																this._authorityRolesMap.get(current.getAuthorityRoleId()),
																this._inspectionInfringementTypesMap.get(current.getInfringementTypeId()),
																this._inspectionOutcomeTypesMap.get(current.getOutcomeTypeId())));
		}
	
		return vesselInspection;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildIuuLists(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToIuuList> buildIuuLists(Collection<VesselsToIuuList> iuuListData) throws Throwable {
		if(iuuListData == null)
			return null;
	
		List<ExtendedVesselToIuuList> vesselInspection = new ListSet<ExtendedVesselToIuuList>();
	
		for(VesselsToIuuList current : iuuListData) {
			vesselInspection.add(new ExtendedVesselToIuuList(current, 
															 this._rfmoIuuListsMap.get(current.getRfmoIuuListId())));
		}
	
		return vesselInspection;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildMasters(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToMaster> buildMasters(Collection<VesselsToMasters> mastersData) throws Throwable {
		if(mastersData == null)
			return null;
	
		List<ExtendedVesselToMaster> masters = new ListSet<ExtendedVesselToMaster>();
	
		Entity entity = null;
	
		for(VesselsToMasters current : mastersData) {
			entity = current.getEntityId() == null ? null : this._entitiesMap.get(current.getEntityId());
	
			if(entity != null)
				masters.add(new ExtendedVesselToMaster(current, new ExtendedEntity(entity, entity.getNationalityId() == null ? NO_NATIONALITY_PROVIDED : this._countriesMap.get(entity.getNationalityId()), entity.getCountryId() == null ? null : this._countriesMap.get(entity.getCountryId()))));
		}
	
		return masters;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildNonCompliances(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToNonCompliance> buildNonCompliances(Collection<VesselsToNonCompliance> nonComplianceData) throws Throwable {
		if(nonComplianceData == null)
			return null;
	
		List<ExtendedVesselToNonCompliance> vesselNonCompliance = new ListSet<ExtendedVesselToNonCompliance>();
	
		for(VesselsToNonCompliance current : nonComplianceData) {
			vesselNonCompliance.add(new ExtendedVesselToNonCompliance(current, 
																	  this._nonComplianceSourceTypesMap.get(current.getSourceTypeId()),
																	  this._systemsMap.get(current.getIssuingAuthorityId()),
																	  this._nonComplianceInfringementTypesMap.get(current.getTypeId()),
																	  this._nonComplianceOutcomeTypesMap.get(current.getOutcomeTypeId())));
		}
	
		return vesselNonCompliance;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildOperators(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToOperator> buildOperators(Collection<VesselsToOperators> operatorsData) throws Throwable {
		if(operatorsData == null)
			return null;
	
		List<ExtendedVesselToOperator> operators = new ListSet<ExtendedVesselToOperator>();
	
		Entity entity = null;
	
		for(VesselsToOperators current : operatorsData) {
			entity = current.getEntityId() == null ? null : this._entitiesMap.get(current.getEntityId());
	
			if(entity != null)
				operators.add(new ExtendedVesselToOperator(current, new ExtendedEntity(entity, entity.getNationalityId() == null ? NO_NATIONALITY_PROVIDED : this._countriesMap.get(entity.getNationalityId()), entity.getCountryId() == null ? null : this._countriesMap.get(entity.getCountryId()))));
		}
	
		return operators;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildOwners(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToOwner> buildOwners(Collection<VesselsToOwners> ownersData) throws Throwable {
		if(ownersData == null)
			return null;
	
		List<ExtendedVesselToOwner> owners = new ListSet<ExtendedVesselToOwner>();
	
		Entity entity = null;
	
		for(VesselsToOwners current : ownersData) {
			entity = current.getEntityId() == null ? null : this._entitiesMap.get(current.getEntityId());
	
			if(entity != null)
				owners.add(new ExtendedVesselToOwner(current, new ExtendedEntity(entity, entity.getNationalityId() == null ? NO_NATIONALITY_PROVIDED : this._countriesMap.get(entity.getNationalityId()), entity.getCountryId() == null ? null : this._countriesMap.get(entity.getCountryId()))));
		}
	
		return owners;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildPortEntryDenials(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToPortEntryDenial> buildPortEntryDenials(Collection<VesselsToPortEntryDenials> denialsData) throws Throwable {
		if(denialsData == null)
			return null;
	
		List<ExtendedVesselToPortEntryDenial> vesselDenials = new ListSet<ExtendedVesselToPortEntryDenial>();
	
		for(VesselsToPortEntryDenials current : denialsData) {
			vesselDenials.add(new ExtendedVesselToPortEntryDenial(current, 
																  this._denialReasonsMap.get(current.getPortEntryDenialReasonId()),
																  this._countriesMap.get(current.getCountryId()),
																  this._extendedPortsMap.get(current.getPortId())));
		}
	
		return vesselDenials;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildRegistrations(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToRegistration> buildRegistrations(Collection<VesselsToRegistrations> registrationsData) throws Throwable {
		if(registrationsData == null)
			return null;
	
		List<ExtendedVesselToRegistration> registrations = new ListSet<ExtendedVesselToRegistration>();
	
		for(VesselsToRegistrations current : registrationsData) {
			registrations.add(new ExtendedVesselToRegistration(current, current.getCountryId() == null ? null : this._countriesMap.get(current.getCountryId()), current.getPortId() == null ? null : this._extendedPortsMap.get(current.getPortId())));
		}
	
		return registrations;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildShipbuilders(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToShipbuilder> buildShipbuilders(Collection<VesselsToShipbuilders> shipbuildersData) throws Throwable {
		if(shipbuildersData == null)
			return null;
	
		List<ExtendedVesselToShipbuilder> shipbuilders = new ListSet<ExtendedVesselToShipbuilder>();
	
		Entity entity = null;
	
		for(VesselsToShipbuilders current : shipbuildersData) {
			entity = current.getEntityId() == null ? null : this._entitiesMap.get(current.getEntityId());
	
			if(entity != null)
				shipbuilders.add(new ExtendedVesselToShipbuilder(current, new ExtendedEntity(entity, NO_NATIONALITY_PROVIDED, entity.getCountryId() == null ? null : this._countriesMap.get(entity.getCountryId()))));
		}
	
		return shipbuilders;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildVesselFlags(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToFlags> buildVesselFlags(Collection<VesselsToFlags> vesselFlagData) throws Throwable {
		if(vesselFlagData == null)
			return null;
	
		List<ExtendedVesselToFlags> vesselFlags = new ListSet<ExtendedVesselToFlags>();
	
		for(VesselsToFlags current : vesselFlagData) {
			vesselFlags.add(new ExtendedVesselToFlags(current, this._countriesMap.get(current.getCountryId())));
		}
	
		return vesselFlags;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildVesselGears(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToGear> buildVesselGears(Collection<VesselsToGears> gearTypesData) throws Throwable {
		if(gearTypesData == null)
			return null;
	
		List<ExtendedVesselToGear> gearTypes = new ListSet<ExtendedVesselToGear>();
	
		for(VesselsToGears current : gearTypesData) {
			gearTypes.add(new ExtendedVesselToGear(current, this._gearTypesMap.get(current.getTypeId())));
		}
	
		return gearTypes;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildVesselLengths(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToLengths> buildVesselLengths(Collection<VesselsToLengths> vesselLengthData) throws Throwable {
		if(vesselLengthData == null)
			return null;
	
		List<ExtendedVesselToLengths> vesselLengths = new ListSet<ExtendedVesselToLengths>();
	
		for(VesselsToLengths current : vesselLengthData) {
			vesselLengths.add(
				new ExtendedVesselToLengths(
					current, 
					this._lengthTypesMap.get(current.getTypeId())
				)
			);
		}
	
		return vesselLengths;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildVesselPowers(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToPower> buildVesselPowers(Collection<VesselsToPower> vesselPowerData) throws Throwable {
		if(vesselPowerData == null)
			return null;
	
		List<ExtendedVesselToPower> vesselPowers = new ListSet<ExtendedVesselToPower>();
	
		for(VesselsToPower current : vesselPowerData) {
			vesselPowers.add(new ExtendedVesselToPower(current, this._powerTypesMap.get(current.getTypeId())));
		}
	
		return vesselPowers;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildVesselStatus(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToStatus> buildVesselStatus(Collection<VesselsToStatus> vesselStatusData) throws Throwable {
		if(vesselStatusData == null)
			return null;
	
		List<ExtendedVesselToStatus> vesselStatus = new ListSet<ExtendedVesselToStatus>();
	
		for(VesselsToStatus current : vesselStatusData) {
			vesselStatus.add(new ExtendedVesselToStatus(current, this._statusMap.get(current.getTypeId())));
		}
	
		return vesselStatus;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildVesselTonnages(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToTonnage> buildVesselTonnages(Collection<VesselsToTonnage> vesselTonnageData) throws Throwable {
		if(vesselTonnageData == null)
			return null;
	
		List<ExtendedVesselToTonnage> vesselTonnages = new ListSet<ExtendedVesselToTonnage>();
	
		for(VesselsToTonnage current : vesselTonnageData) {
			vesselTonnages.add(new ExtendedVesselToTonnage(current, this._tonnageTypesMap.get(current.getTypeId())));
		}
	
		return vesselTonnages;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.dao.impl.Foo#buildVesselTypes(java.util.Collection)
	 */
	@Override
	public List<ExtendedVesselToType> buildVesselTypes(Collection<VesselsToTypes> vesselTypesData) throws Throwable {
		if(vesselTypesData == null)
			return null;
	
		List<ExtendedVesselToType> vesselTypes = new ListSet<ExtendedVesselToType>();
	
		Integer owningCountry;
		for(VesselsToTypes current : vesselTypesData) {
			owningCountry = this._vesselTypesMap.get(current.getTypeId()).getCountryId();
			
			vesselTypes.add(new ExtendedVesselToType(current, this._vesselTypesMap.get(current.getTypeId()), owningCountry == null ? null :  this._countriesMap.get(owningCountry)));
		}
	
		return vesselTypes;
	}

}
