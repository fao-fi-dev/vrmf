/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.vrmf.business.dao.VesselRegNoAutocompletionDAO;
import org.fao.fi.vrmf.business.helpers.ports.PortsMappingManager;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.RegistrationNumberSearchResult;
import org.springframework.cache.annotation.Cacheable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class VesselRegNoAutocompletionDAOImpl extends AbstractAutocompletionDAOImpl implements VesselRegNoAutocompletionDAO {
	@Inject private @Getter @Setter PortsMappingManager portsMappingManager;

	public String getProcedureName(boolean authorizedOnly) {
		return "REG_NO_SEARCH" + ( authorizedOnly ? "_BY_AUTH" : "" );
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselRegNoAutocompletionDAO#searchVesselRegNo(java.lang.Integer[], java.lang.Integer, java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#registrationCountries) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#registrationPortCode) + " +
															  "#regNo + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID")
	public Collection<RegistrationNumberSearchResult> searchVesselRegNo(Integer[] registrationCountries, Integer[] registrationPortCode, String regNo, Date atDate, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		return this.searchVesselRegNo(this.getProcedureName(false), registrationCountries, registrationPortCode, regNo, atDate, limitToMaxItems, sourceSystems, groupByUID);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselRegNoAutocompletionDAO#searchVesselRegNo(java.lang.String, java.lang.Integer[], java.lang.Integer[], java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#registrationCountries) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#registrationPortCode) + " +
															  "#procedureName + " +
															  "#regNo + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID")
	public Collection<RegistrationNumberSearchResult> searchVesselRegNo(String procedureName, Integer[] registrationCountries, Integer[] registrationPortCode, String regNo, Date atDate, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		long end, start = System.currentTimeMillis();
		
		this._log.info("Autocompleting reg. no. for '{}' / {} / {} (procedure name: {}, sources: {}, at date: {}, timeframe: {}, group by UID: {}, authorized only: {}, authorization source: {})",
						regNo,
						CollectionsHelper.serializeArray(registrationCountries),
						CollectionsHelper.serializeArray(registrationPortCode),
						procedureName,
						CollectionsHelper.serializeArray(sourceSystems),
						atDate,
						groupByUID);
		
		Connection c = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		Collection<Integer> mappedPorts = new HashSet<Integer>();
		
//		if(registrationPortCode != null)
//			mappedPorts = this._portsMappingManager.mappedNodes(registrationPortCode);
		
		if(registrationPortCode != null)
			for(Integer portCode : registrationPortCode)
				mappedPorts.add(portCode);
		
		try {			
			String CALL_TEMPLATE = "CALL " + ( procedureName == null ? this.getProcedureName(false) : procedureName ) + "(?, ?, ?, ?, ?, ?)";
			
			c = this.sqlMapClient.getDataSource().getConnection();
			
			cs = c.prepareCall(CALL_TEMPLATE);
			
			int counter = 1;
			
			this.setUnicodeString(cs, counter++, regNo);
			
			if(registrationCountries != null)
				cs.setString(counter++, "@" + CollectionsHelper.join(registrationCountries, "@") + "@");
			else
				cs.setNull(counter++, Types.VARCHAR);
			
			if(mappedPorts != null && !mappedPorts.isEmpty())
				cs.setString(counter++, "@" + CollectionsHelper.join(mappedPorts.toArray(new Object[mappedPorts.size()]), "@") + "@");
			else
				cs.setNull(counter++, Types.VARCHAR);
			
			if(sourceSystems != null)
				cs.setString(counter++, "@" + CollectionsHelper.join(sourceSystems, "@") + "@");
			else 
				cs.setNull(counter++, Types.VARCHAR);
						
			if(atDate != null)
				cs.setDate(counter++, new java.sql.Date(atDate.getTime()));
			else
				cs.setNull(counter++, Types.DATE);
	
			cs.setBoolean(counter++, groupByUID);	
			
			List<RegistrationNumberSearchResult> vesselRegNoData = new ArrayList<RegistrationNumberSearchResult>();
			
			if(cs.execute()) {
				rs = cs.getResultSet();
								
				RegistrationNumberSearchResult current = null;
				
				while(rs.next()) {
					current = new RegistrationNumberSearchResult();
					
					current.setRegistrationNumber(rs.getString("REGISTRATION_NUMBER"));
					current.setOccurrencies(rs.getInt("OCCURRENCIES"));
					current.setTotalOccurrencies(rs.getInt("TOTAL_OCCURRENCIES"));
					current.setGroupedOccurrencies(rs.getInt("GROUPED_OCCURRENCIES"));
					
					vesselRegNoData.add(current);
				}
			}
			
			return this.trim(vesselRegNoData, limitToMaxItems);
		} catch (Throwable t) {
			this._log.error("Unable to find vessel registration number", t);
			
			return new ArrayList<RegistrationNumberSearchResult>();
		} finally {
			this.cleanup(rs, cs, c);
			
			end = System.currentTimeMillis();

			this._log.info("Autocompleting reg. no. for '{}' / {}  / {} took {}mSec.", regNo, CollectionsHelper.serializeArray(registrationCountries), registrationPortCode, end - start);
		}
	}
}