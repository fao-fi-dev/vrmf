/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.extensions.maps.lazy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.fao.fi.vrmf.business.core.utilities.extensions.maps.impl.LazySerializableDataAccessMap;
import org.fao.fi.vrmf.business.dao.generated.SHullMaterialDAO;
import org.fao.fi.vrmf.business.dao.generated.SHullMaterialDAOImpl;
import org.fao.fi.vrmf.common.models.generated.SHullMaterial;
import org.fao.fi.vrmf.common.models.generated.SHullMaterialExample;

import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
public class LazyHullMaterialsMap extends LazySerializableDataAccessMap<String, SHullMaterial> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8602579370194965528L;

	transient private SHullMaterialDAO _dao;
	
	/**
	 * Class constructor
	 *
	 * @param dao
	 * @throws Throwable
	 */
	public LazyHullMaterialsMap(SHullMaterialDAO dao) throws Throwable {
		super();
		
		this._dao = dao;
	}
	
	/**
	 * Class constructor
	 *
	 * @param dao
	 * @param sourceSystems
	 */
	public LazyHullMaterialsMap(String sqlMapConfigurationResource, String[] sourceSystems) throws Throwable {
		super(sqlMapConfigurationResource, sourceSystems);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazySerializableDataAccessMap#initializeDAO()
	 */
	@Override
	protected void initializeDAO() throws Throwable {
		this._dao = new SHullMaterialDAOImpl();
		this._dao.setSqlMapClient(SqlMapClientBuilder.buildSqlMapClient(Thread.currentThread().getContextClassLoader().getResourceAsStream(this._sqlMapClientConfigurationResource)));
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetElement(java.lang.Object)
	 */
	protected SHullMaterial doGetElement(String key) throws Throwable {
		return this._dao.selectByPrimaryKey(key);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetKeySet(java.lang.Object)
	 */
	@Override
	protected Set<String> doGetKeySet() throws Throwable {
		SHullMaterialExample filter = new SHullMaterialExample();
			
		if(this._sourceSystems != null && this._sourceSystems.length > 0)
			filter.createCriteria().andSourceSystemIn(Arrays.asList(this._sourceSystems));
			
		Set<String> keySet = new HashSet<String>();
		
		for(SHullMaterial hullMaterial : this._dao.selectByExample(filter)) {
			keySet.add(hullMaterial.getId());
		}
		
		return keySet;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetSize()
	 */
	@Override
	protected int doGetSize() throws Throwable {
		SHullMaterialExample filter = new SHullMaterialExample();
		
		if(this._sourceSystems != null && this._sourceSystems.length > 0)
			filter.createCriteria().andSourceSystemIn(Arrays.asList(this._sourceSystems));
		
		return this._dao.countByExample(filter);
	}
}