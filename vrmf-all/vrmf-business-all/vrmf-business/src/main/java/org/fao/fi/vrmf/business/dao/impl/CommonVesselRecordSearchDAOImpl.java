/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractSetterInjectionIBATISDAO;
import org.fao.fi.vrmf.business.dao.DataDecoratorDAO;
import org.fao.fi.vrmf.common.models.extended.BasicTimeframedAuthorizedVessel;
import org.fao.fi.vrmf.common.models.search.SourceByUID;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Oct 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Oct 2012
 */
abstract public class CommonVesselRecordSearchDAOImpl extends AbstractSetterInjectionIBATISDAO {
	static final protected String DEFAULT_METHODS_INVOCATION_CACHE_ID = "vrmf.business.search";
	static final protected String COMMON_SEARCH_VESSELS_NAMESPACE = "commonSearchVessels";
	
	static final protected Boolean KEEP_ONLY_DISTINCT_SOURCES = Boolean.TRUE;
	static final protected Boolean KEEP_ALL_SOURCES 		  = Boolean.FALSE;
	
	@Inject protected @Getter @Setter DataDecoratorDAO decorator;

	protected List<SourceByUID> getSourceSystemsForVesselUIDs(Collection<Integer> IDs, String vesselsTable, String[] availableSourceSystems) throws Throwable {
		return this.getSourceSystemsForVesselUIDs(COMMON_SEARCH_VESSELS_NAMESPACE, IDs, vesselsTable, availableSourceSystems);
	}
	
	protected Integer getFullSearchResultsSize() throws Throwable {
		return this.getFullSearchResultsSize(COMMON_SEARCH_VESSELS_NAMESPACE);
	}
	
	protected Integer getFullSearchResultsSize(String IBATISNamespace) throws Throwable {
		return (Integer)this.sqlMapClient.queryForObject(this.getQueryIDForNamespace(IBATISNamespace, "fullSearchResultSize"));
	}
	
	@SuppressWarnings("unchecked")
	protected List<SourceByUID> getSourceSystemsForVesselUIDs(String IBATISNamespace, Collection<Integer> IDs, String vesselsTable, String[] availableSourceSystems) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("UIDs", IDs);
		params.put("vesselsTable", vesselsTable);
		params.put("availableSourceSystems", availableSourceSystems);
		
		return (List<SourceByUID>)this.sqlMapClient.queryForList(this.getQueryIDForNamespace(IBATISNamespace, "getSystemsForUIDs"), params);
	}

	protected void updateSources(boolean groupByUID, Collection<? extends BasicTimeframedAuthorizedVessel> vessels, String vesselsTable, String[] availableSourceSystems, boolean skipDuplicates) throws Throwable {
		List<SourceByUID> sourcesByUID = null;
		Map<Integer, Collection<String>> sourcesByUIDMap = new HashMap<Integer, Collection<String>>();
		Map<Integer, String> sourceByUIDMap = new HashMap<Integer, String>();
		
		Collection<Integer> IDs = new ArrayList<Integer>();
		
		if(groupByUID) {
			for(BasicTimeframedAuthorizedVessel vessel : vessels) {
				sourceByUIDMap.put(vessel.getUid(), vessel.getSourceSystem());
				IDs.add(vessel.getUid());
			}

			sourcesByUID = this.getSourceSystemsForVesselUIDs(IDs, vesselsTable, availableSourceSystems);
				
			for(SourceByUID data : sourcesByUID) {
				if(!sourcesByUIDMap.containsKey(data.getUid()))
					sourcesByUIDMap.put(data.getUid(), new ArrayList<String>());
					
				sourcesByUIDMap.get(data.getUid()).add(data.getSourceSystem());
			}
		}
		
		for(BasicTimeframedAuthorizedVessel vessel : vessels) {
			if(groupByUID) {
				Collection<String> filteredSources = skipDuplicates ? new ListSet<String>() : new ArrayList<String>(); 
					
				for(String currentSource : sourcesByUIDMap.get(vessel.getUid()).toArray(new String[0])) {
					filteredSources.add(currentSource);
				}
				
				vessel.setSourceSystems(filteredSources.toArray(new String[filteredSources.size()]));
				vessel.setNumberOfSystems(vessel.getSourceSystems().length);
			} else {
				vessel.setSourceSystems(new String[] { vessel.getSourceSystem() });
				vessel.setNumberOfSystems(1);
			}
		}
	}
}
