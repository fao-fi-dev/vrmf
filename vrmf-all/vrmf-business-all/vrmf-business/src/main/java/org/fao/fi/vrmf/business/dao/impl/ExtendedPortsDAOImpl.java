/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.business.dao.ExtendedPortsDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesDAOImpl;
import org.fao.fi.vrmf.business.dao.generated.SCountriesSubdivisionDAOImpl;
import org.fao.fi.vrmf.business.dao.generated.SPortsCoordinatesDAO;
import org.fao.fi.vrmf.business.dao.generated.SPortsDAOImpl;
import org.fao.fi.vrmf.common.models.extended.ExtendedPort;
import org.fao.fi.vrmf.common.models.generated.SPorts;
import org.fao.fi.vrmf.common.models.generated.SPortsCoordinatesExample;
import org.fao.fi.vrmf.common.models.generated.SPortsExample;
import org.fao.fi.vrmf.common.models.generated.SPortsExample.Criteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Oct 2010
 */
@NoArgsConstructor
@Named @Singleton
public class ExtendedPortsDAOImpl extends SPortsDAOImpl implements ExtendedPortsDAO {
	private Logger _log = LoggerFactory.getLogger(this.getClass());

	@Inject protected @Getter @Setter SCountriesDAOImpl countriesDAO;
	@Inject protected @Getter @Setter SCountriesSubdivisionDAOImpl subdivisionsDAO;
	
	@Inject protected @Getter @Setter SPortsDAOImpl portsDAO;
	@Inject protected @Getter @Setter SPortsCoordinatesDAO coordinatesDAO;


	/* (non-Javadoc)
	 * @see org.fao.gvr2.business.dao.ExtendedPortsDAO#selectExtendedByPrimaryKey(java.lang.Integer)
	 */
	public ExtendedPort selectExtendedByPrimaryKey(Integer portID) throws Throwable {
		ExtendedPort toReturn = null;
		
		try {
			SPorts port = this.portsDAO.selectByPrimaryKey(portID);
			
			if(port == null)
				return null;
			
			//Main vessel data
			toReturn = new ExtendedPort();
			toReturn.setId(portID);
			toReturn.setUid(port.getUid());
			toReturn.setOriginalPortId(port.getOriginalPortId());
			toReturn.setMapsTo(port.getMapsTo());
			toReturn.setMappingUser(port.getMappingUser());
			toReturn.setMappingWeight(port.getMappingWeight());
			toReturn.setMappingDate(port.getMappingDate());
			toReturn.setMappingComment(port.getMappingComment());
			toReturn.setSubdivisionId(port.getSubdivisionId());
			toReturn.setCountryId(port.getCountryId());
			toReturn.setName(port.getName());
			toReturn.setSimplifiedName(port.getSimplifiedName());
			toReturn.setSimplifiedNameSoundex(port.getSimplifiedNameSoundex());
			toReturn.setSourceSystem(port.getSourceSystem());
			toReturn.setUpdaterId(port.getUpdaterId());
			toReturn.setUpdateDate(port.getUpdateDate());
			toReturn.setComment(port.getComment());
			
			if(port.getSubdivisionId() != null) {
				toReturn.setSubdivision(this.subdivisionsDAO.selectByPrimaryKey(port.getCountryId(), port.getSubdivisionId()));
			}
			
			SPortsCoordinatesExample coordinatesFilter = new SPortsCoordinatesExample();
			coordinatesFilter.createCriteria().andPortIdEqualTo(port.getId());

			coordinatesFilter.setOrderByClause("REFERENCE_DATE DESC, UPDATE_DATE DESC");
			
			toReturn.setCoordinates(this.coordinatesDAO.selectByExample(coordinatesFilter));
			
			toReturn.setCountry(this.countriesDAO.selectByPrimaryKey(port.getCountryId()));
			
		} catch (Throwable t) {
			this._log.error("Unable to retrieve extended port data", t);
			
			return null;
		}
		
		return toReturn;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.ExtendedPortsDAO#selectExtendedByExample(org.fao.vrmf.utilities.common.models.generated.SPortsExample)
	 */
	@Override
	public List<ExtendedPort> selectExtendedByExample(SPortsExample filter) throws Throwable {
		List<SPorts> ports = this.portsDAO.selectByExample(filter);
		
		List<ExtendedPort> toReturn = new ArrayList<ExtendedPort>();
		
		this._log.info(ports.size() + " ports selected: converting to Extended Ports...");
		
		int counter = 0;
		for(SPorts port : ports) {
			toReturn.add(this.selectExtendedByPrimaryKey(port.getId()));
			
			counter++;
			
			if(counter % 1000 == 0)
				this._log.info(counter + " Ports converted to Extended Ports so far...");
		}
		
		this._log.info(counter + " Ports converted to Extended Ports so far...");
		
		return toReturn;
	}

	/* (non-Javadoc)
	 * @see org.fao.gvr2.business.dao.ExtendedPortsDAO#selectExtendedByDataSource(java.lang.String[])
	 */
	public List<ExtendedPort> selectExtendedByDataSource(String[] dataSources) throws Throwable {
		SPortsExample filter = new SPortsExample();
		Criteria criteria = filter.createCriteria();
		
		if(dataSources != null && dataSources.length > 0)
			criteria.andSourceSystemIn(Arrays.asList(dataSources));
				
		List<SPorts> ports = this.portsDAO.selectByExample(filter);
		
		List<ExtendedPort> toReturn = new ArrayList<ExtendedPort>();
		
		this._log.info(ports.size() + " ports selected: converting to Extended Ports...");
		
		int counter = 0;
		for(SPorts port : ports) {
			toReturn.add(this.selectExtendedByPrimaryKey(port.getId()));
			
			counter++;
			
			if(counter % 1000 == 0)
				this._log.info(counter + " Ports converted to Extended Ports so far...");
		}
		
		this._log.info(counter + " Ports converted to Extended Ports so far...");
		
		return toReturn;
	}
}