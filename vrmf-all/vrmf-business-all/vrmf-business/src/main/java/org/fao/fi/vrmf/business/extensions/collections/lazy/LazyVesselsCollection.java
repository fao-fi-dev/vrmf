/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.extensions.collections.lazy;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.fao.fi.sh.utility.caching.spi.CacheFacade;
import org.fao.fi.vrmf.business.core.utilities.extensions.collections.impl.LazySerializableDataAccessCollection;
import org.fao.fi.vrmf.business.dao.ExtendedVesselsDAO;
import org.fao.fi.vrmf.business.dao.impl.ExtendedVesselsDAOImpl;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.Vessels;
import org.fao.fi.vrmf.common.models.generated.VesselsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsExample.Criteria;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
final public class LazyVesselsCollection extends LazySerializableDataAccessCollection<Integer, ExtendedVessel> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3436720853618616675L;
	
	transient private Collection<Class<?>> _dataTypeSet;
	transient private VesselsExample _filter;
	
	transient private ExtendedVesselsDAO _dao;
		
	private Collection<Integer> _indexes;

	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param sqlMapClientConfigurationResource
	 * @param sourceSystems
	 * @param updateDateFrom
	 * @param updateDateTo
	 * @param lastReferenceDateFrom
	 * @param lastReferenceDateTo
	 * @param dataTypeSet
	 * @throws Throwable
	 */
	public LazyVesselsCollection(CacheFacade<Integer, ExtendedVessel> backingCache, String sqlMapClientConfigurationResource, String[] sourceSystems, Date updateDateFrom, Date updateDateTo, Collection<Class<?>> dataTypeSet) throws Throwable {
		super(backingCache, sqlMapClientConfigurationResource, sourceSystems);
		
		this._dataTypeSet = dataTypeSet;
		this._filter = this.createFilter(sourceSystems, updateDateFrom, updateDateTo);
	}

	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param providedSqlMapClient
	 * @param sourceSystems
	 * @param updateDateFrom
	 * @param updateDateTo
	 * @param lastReferenceDateFrom
	 * @param lastReferenceDateTo
	 * @param dataTypeSet
	 * @throws Throwable
	 */
	public LazyVesselsCollection(CacheFacade<Integer, ExtendedVessel> backingCache, SqlMapClient providedSqlMapClient, String[] sourceSystems, Date updateDateFrom, Date updateDateTo, Collection<Class<?>> dataTypeSet) throws Throwable {
		super(backingCache, providedSqlMapClient, sourceSystems);
		
		this._dataTypeSet = dataTypeSet;
		this._filter = this.createFilter(sourceSystems, updateDateFrom, updateDateTo);
	}

	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param sqlMapClientConfigurationResource
	 * @param sourceSystems
	 * @param dataTypeSet
	 * @throws Throwable
	 */
	public LazyVesselsCollection(CacheFacade<Integer, ExtendedVessel> backingCache, String sqlMapClientConfigurationResource, String[] sourceSystems, Collection<Class<?>> dataTypeSet) throws Throwable {
		this(backingCache, sqlMapClientConfigurationResource, sourceSystems, null, null, dataTypeSet);
	}
	
	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param providedSqlMapClient
	 * @param sourceSystems
	 * @param dataTypeSet
	 * @throws Throwable
	 */
	public LazyVesselsCollection(CacheFacade<Integer, ExtendedVessel> backingCache, SqlMapClient providedSqlMapClient, String[] sourceSystems, Collection<Class<?>> dataTypeSet) throws Throwable {
		this(backingCache, providedSqlMapClient, sourceSystems, null, null, dataTypeSet);
	}

	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param sqlMapClientConfigurationResource
	 * @param filter
	 * @param dataTypeSet
	 * @throws Throwable
	 */
	public LazyVesselsCollection(CacheFacade<Integer, ExtendedVessel> backingCache, String sqlMapClientConfigurationResource, VesselsExample filter, Collection<Class<?>> dataTypeSet) throws Throwable {
		super(backingCache, sqlMapClientConfigurationResource, null);
		
		this._dataTypeSet = dataTypeSet;
		this._filter = filter;
	}

	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param providedSqlMapClient
	 * @param filter
	 * @param dataTypeSet
	 * @throws Throwable
	 */
	public LazyVesselsCollection(CacheFacade<Integer, ExtendedVessel> backingCache, SqlMapClient providedSqlMapClient, VesselsExample filter, Collection<Class<?>> dataTypeSet) throws Throwable {
		super(backingCache, providedSqlMapClient, null);
		
		this._dataTypeSet = dataTypeSet;
		this._filter = filter;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.utils.extensions.collections.impl.LazySerializableDataAccessCollection#initializeDAOs()
	 */
	@Override
	protected void initializeDAOs() throws Throwable {
		this.initializeDAOs(SqlMapClientBuilder.buildSqlMapClient(Thread.currentThread().getContextClassLoader().getResourceAsStream(this._sqlMapClientConfigurationResource)));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.utils.extensions.collections.impl.LazySerializableDataAccessCollection#initializeDAOs(com.ibatis.sqlmap.client.SqlMapClient)
	 */
	@Override
	protected void initializeDAOs(SqlMapClient providedSqlMapClient) throws Throwable {
		this._dao = new ExtendedVesselsDAOImpl();
		this._dao.setSqlMapClient(providedSqlMapClient);
	}

	/**
	 * @param sourceSystems
	 * @return
	 */
	private VesselsExample createFilter(String[] sourceSystems, Date updateDateFrom, Date updateDateTo) {
		List<String> sourcesCollection = new ArrayList<String>();
		
		VesselsExample filter = new VesselsExample();
		Criteria criteria = filter.createCriteria();
		
		if(sourceSystems != null && sourceSystems.length > 0) {
			sourcesCollection.addAll(Arrays.asList(sourceSystems));

			criteria.andSourceSystemIn(sourcesCollection);
		}
		
		if(updateDateFrom != null)
			criteria.andUpdateDateGreaterThanOrEqualTo(updateDateFrom);
		
		if(updateDateTo != null)
			criteria.andUpdateDateLessThanOrEqualTo(updateDateTo);

		filter.setOrderByClause("ID ASC");
		
		return filter;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.collections.impl.LazyCollection#getSize()
	 */
	@Override
	protected int getSize() {
		try {
//			if(this._backingCache.size() == 0) {
			if(this._indexes == null || this._indexes.size() == 0) {
				int size = this._dao.countByExample(this._filter);
				
				this._log.debug("CALCULATED Lazy Vessels Collection size for " + this._backingCache.getCacheID() + " is " + size);
				
				return size;
			} else {
				int size = this._indexes.size();
				
				this._log.debug("CURRENT Lazy Vessels Collection size for " + this._backingCache.getCacheID() + " is " + size);
				
				return size;
			}
		} catch(Throwable t) {
			this._log.error("Unable to retrieve size for lazy collection " + this, t);
			
			return 0;
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.collections.impl.LazyCollection#getIndexes()
	 */
	@Override
	protected Collection<Integer> getIndexes() {
		if(this._indexes == null) {
			this._indexes = new ArrayList<Integer>();
			try {
				for(Vessels vessel : this._dao.selectByExample(this._filter)) {
					this._indexes.add(vessel.getId());
				}
			} catch(Throwable t) {
				this._log.error("Unable to retrieve indexes for lazy collection " + this, t);
			}
		}
		
		return this._indexes;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.collections.impl.LazyCollection#doGetElement(java.lang.Object)
	 */
	protected ExtendedVessel doGetElement(Integer index) {
		try {
			return this._dao.selectExtendedByPrimaryKey(index, this._dataTypeSet);
		} catch(Throwable t) {
			this._log.error("Unable to retrieve element with index " + index + " for lazy collection " + this, t);
		}
		
		return null;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Collection#add(java.lang.Object)
	 */
	@Override
	public boolean add(ExtendedVessel e) {
		ExtendedVessel current = this._backingCache.get($nN($nN(e, "The vessel to add cannot be NULL").getId(), "The vessel to add cannot have a NULL ID"));
		
		this._backingCache.put(e.getId(), e);
		
		return current != null;
	}
}