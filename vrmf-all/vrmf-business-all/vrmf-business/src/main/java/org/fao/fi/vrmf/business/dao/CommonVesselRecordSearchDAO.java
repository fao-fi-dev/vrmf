/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import org.fao.fi.vrmf.business.core.dao.ibatis.SetterInjectionIBATISDAO;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.fao.fi.vrmf.common.search.dsl.support.services.response.vessels.VesselRecordSearchResponse;
import org.fao.fi.vrmf.common.services.search.response.PaginatedSearchResponse;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Mar 2013
 */
public interface CommonVesselRecordSearchDAO<V extends VesselRecord, F extends VesselRecordSearchFilter> extends SetterInjectionIBATISDAO {
	/**
	 * @param namespace
	 * @param filter
	 * @return
	 * @throws Throwable
	 */
	VesselRecordSearchResponse<V> searchVessels(String namespace, F filter) throws Throwable;
	
	/**
	 * Same as {@link #searchVessels(String, VesselsSearchRequest)} with a <code>NULL</code> (i.e. revert to default) namespace provided
	 * @param filter
	 * @return
	 * @throws Throwable
	 */
	VesselRecordSearchResponse<V> searchVessels(F filter) throws Throwable;
	
	/**
	 * @param namespace
	 * @param filter
	 * @return
	 * @throws Throwable
	 */
	Integer countVessels(String namespace, F filter) throws Throwable;
	
	/**
	 * Same as {@link #countVessels(String, VesselsSearchRequest)} with a <code>NULL</code> (i.e. revert to default) namespace provided
	 * @param filter
	 * @return
	 * @throws Throwable
	 */
	Integer countVessels(F filter) throws Throwable;
	
	/**
	 * @param namespace
	 * @param filter
	 * @return
	 * @throws Throwable
	 */
	PaginatedSearchResponse<Integer> searchVesselIDs(String namespace, F filter) throws Throwable;

	/**
	 * @param namespace
	 * @param filter
	 * @return
	 * @throws Throwable
	 */
	PaginatedSearchResponse<Integer> searchVesselIDs(F filter) throws Throwable;
}
