/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.vrmf.business.dao.VesselUIDAutocompletionDAO;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.IdentifierSearchResult;
import org.springframework.cache.annotation.Cacheable;

import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class VesselUIDAutocompletionDAOImpl extends AbstractVesselIdentifierAutocompletionDAOImpl implements VesselUIDAutocompletionDAO {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.generic.AbstractAutocompletionDAOImpl#getProcedureName(boolean)
	 */
	public String getProcedureName(boolean authorizedOnly) {
		return "DEFAULT_UID_SEARCH"; //Procedure doesn't currently change if searching for authorized vs non-necesarily authorized vessels...
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselUIDAutocompletionDAO#searchVesselUID(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "#UID + " +
															  "#limitToMaxItems")
	public Collection<IdentifierSearchResult> searchVesselUID(String UID, String[] sourceSystems, Integer limitToMaxItems) throws Throwable {
		return this.searchVesselUID(this.getProcedureName(false), UID, sourceSystems, limitToMaxItems);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselUIDAutocompletionDAO#searchVesselUID(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "#procedureName + " +
															  "#UID + " +
															  "#limitToMaxItems")
	public Collection<IdentifierSearchResult> searchVesselUID(String procedureName, String UID, String[] sourceSystems, Integer limitToMaxItems) throws Throwable {
		Connection c = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {			
			String CALL_TEMPLATE = "CALL " + ( procedureName == null ? this.getProcedureName(false) : procedureName ) + "(?, ?)";
			
			c = this.sqlMapClient.getDataSource().getConnection();
			
			cs = c.prepareCall(CALL_TEMPLATE);
			
			int counter = 1;
					
			cs.setString(counter++, UID);
					
			if(sourceSystems != null && sourceSystems.length > 0)
				cs.setString(counter++, "@" + CollectionsHelper.join(sourceSystems, "@") + "@");
			else 
				cs.setNull(counter++, Types.VARCHAR);
			
			List<IdentifierSearchResult> vesselUIDData = null;
			
			if(cs.execute()) {
				rs = cs.getResultSet();
								
				vesselUIDData = this.buildSearchResults(rs);
			}
			
			return this.trim(vesselUIDData, limitToMaxItems);
		} catch (Throwable t) {
			this._log.error("Unable to find vessel UID", t);
			
			return new ArrayList<IdentifierSearchResult>();
		} finally {
			this.cleanup(rs, cs, c);
		}
	}
}