/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.common.helpers.singletons.text.StringDistanceHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.lexical.processors.queue.impl.NameMainPartSimplifierProcessor;
import org.fao.fi.sh.utility.lexical.soundex.impl.ExtendedPhraseSoundexGenerator;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.business.dao.ExtendedPortsDAO;
import org.fao.fi.vrmf.business.dao.RegistrationPortAutocompletionDAO;
import org.fao.fi.vrmf.business.helpers.ports.PortsMappingManager;
import org.fao.fi.vrmf.common.models.generated.SPorts;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.FuzzyPortSearchResult;
import org.springframework.cache.annotation.Cacheable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class RegistrationPortAutocompletionDAOImpl extends AbstractAutocompletionDAOImpl implements RegistrationPortAutocompletionDAO {
	@Inject private @Getter @Setter ExtendedPortsDAO portsDAO;
	@Inject private @Getter @Setter PortsMappingManager portsMappingManager;
	@Inject private @Getter @Setter ExtendedPhraseSoundexGenerator soundexHelper;

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.generic.AbstractAutocompletionDAOImpl#getProcedureName(boolean)
	 */
	@Override
	protected String getProcedureName(boolean authorizedVesselsOnly) {
		return "FUZZY_VESSEL_REG_PORT_NAME_SEARCH" + ( authorizedVesselsOnly ? "_BY_AUTH" : "");
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.RegistrationPortAutocompletionDAO#searchRegistrationPorts(java.lang.Integer[], java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.String[], java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#availableSources) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#publicSourceSystems) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#countryIDs) + " +
															  "#portName + " +
															  "#atDate + " +
															  "#limitToMaxItems + " +
															  "#groupByUID + " +
															  "#groupMappedTypes")
	public Collection<FuzzyPortSearchResult> searchRegistrationPorts(Integer[] countryIDs, String portName, Date atDate, Integer limitToMaxItems, String[] availableSources, String[] sourceSystems, String[] publicSourceSystems, Boolean groupByUID, boolean groupMappedTypes) throws Throwable {
		return this.searchRegistrationPorts(this.getProcedureName(false), countryIDs, portName, atDate, limitToMaxItems, availableSources, sourceSystems, publicSourceSystems, groupByUID, groupMappedTypes);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.RegistrationPortAutocompletionDAO#searchRegistrationPorts(java.lang.String, java.lang.Integer[], java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.String[], java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#availableSources) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#publicSourceSystems) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#countryIDs) + " +
															  "#procedureName + " +
															  "#portName + " +
															  "#atDate + " +
															  "#limitToMaxItems + " +
															  "#groupByUID + " +
															  "#groupMappedTypes")
	public Collection<FuzzyPortSearchResult> searchRegistrationPorts(String procedureName, Integer[] countryIDs, String portName, Date atDate, Integer limitToMaxItems, String[] availableSources, String[] sourceSystems, String[] publicSourceSystems, Boolean groupByUID, boolean groupMappedTypes) throws Throwable {
		Connection c = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		final String simplifiedName = new NameMainPartSimplifierProcessor().process(portName);
		
		Set<String> publicSourceSystemsSet = new HashSet<String>(Arrays.asList(availableSources));
		publicSourceSystemsSet.addAll(Arrays.asList(publicSourceSystems));
		
		Set<String> sourceSystemsSet = new HashSet<String>();
		
		if(sourceSystems != null)
			sourceSystemsSet.addAll(Arrays.asList(sourceSystems));
		
		try {			
			String CALL_TEMPLATE = "CALL " + ( procedureName == null ? this.getProcedureName(false) : procedureName ) + "(?, ?, ?, ?, ?)";
			
			c = this.sqlMapClient.getDataSource().getConnection();
			
			cs = c.prepareCall(CALL_TEMPLATE);
			
			int counter = 1;
		
			if(sourceSystemsSet.isEmpty())
				cs.setString(counter++, "@" + CollectionsHelper.join(publicSourceSystemsSet, "@") + "@");
			else
				cs.setString(counter++, "@" + CollectionsHelper.join(sourceSystemsSet, "@") + "@");

			if(countryIDs != null)
				cs.setString(counter++, "@" + CollectionsHelper.join(countryIDs, "@") + "@");
			else 
				cs.setNull(counter++, Types.VARCHAR);
			
			this.setUnicodeString(cs, counter++, portName);
			
			if(atDate != null)
				cs.setDate(counter++, new java.sql.Date(atDate.getTime()));
			else
				cs.setNull(counter++, Types.DATE);
	
			cs.setBoolean(counter++, groupByUID);	
			
			List<FuzzyPortSearchResult> portNameData = new ArrayList<FuzzyPortSearchResult>();
			List<FuzzyPortSearchResult> updatedPortsData = new ArrayList<FuzzyPortSearchResult>();
			Map<Integer, FuzzyPortSearchResult> returnedPortsDataMap = new HashMap<Integer, FuzzyPortSearchResult>();
			List<Integer> returnedPortsDataId = new ListSet<Integer>();
			
			if(cs.execute()) {
				rs = cs.getResultSet();
								
				FuzzyPortSearchResult current = null;
				
				double currentWeight, updatedWeight;
				
				while(rs.next()) {
					current = new FuzzyPortSearchResult();
					
					current.setId(rs.getInt("ID"));
					current.setOriginalId(rs.getString("ORIGINAL_PORT_ID"));
					current.setName(rs.getString("NAME"));
					current.setSimplifiedName(rs.getString("SIMPLIFIED_NAME"));
					current.setCountryId(rs.getInt("COUNTRY_ID"));
					current.setSourceSystem(rs.getString("SOURCE_SYSTEM"));
					
					currentWeight = rs.getDouble("RESULT_WEIGHT");
					
					if(Double.compare(currentWeight, 100D) == 0)
						current.setScore(currentWeight);
					else {
						updatedWeight = 100D - 100D * StringDistanceHelper.computeRelativeDistance(current.getSimplifiedName(), simplifiedName);
						
						current.setScore(updatedWeight);
					}
					
					portNameData.add(current);
					
					returnedPortsDataMap.put(current.getId(), current);
					returnedPortsDataId.add(current.getId());
				}
			}
			
			Collections.sort(portNameData, new Comparator<FuzzyPortSearchResult>() {
				@Override
				public int compare(FuzzyPortSearchResult o1, FuzzyPortSearchResult o2) {
					int score = o2.getScore().compareTo(o1.getScore());
					
					if(Double.compare(score, 0D) != 0)
						return score;
					
					if(o2.getName() != null)
						return o2.getName().compareTo(o1.getName());
					
					if(o1.getName() != null)
						return -o1.getName().compareTo(o2.getName());
					
					return 0;
				}
			});
			
			if(!groupMappedTypes)
				return this.trim(portNameData, limitToMaxItems);
			
			Set<Integer> mappedPorts;
			Integer portId;
			
			SPorts newPort = null;
			FuzzyPortSearchResult newResult = null;
			
			for(FuzzyPortSearchResult port : portNameData) {
				portId = port.getId();
				
				if(returnedPortsDataId.contains(portId)) {
					updatedPortsData.add(port);
					
					mappedPorts = this.portsMappingManager.mappedNodes(portId);
										
					//For each mapped port...
					if(mappedPorts != null)
						for(Integer mappedPortID : mappedPorts) {
							if(mappedPortID.equals(portId))
								continue;
							
							if(returnedPortsDataMap.containsKey(mappedPortID)) {
								port.getMappedPorts().add(returnedPortsDataMap.get(mappedPortID));
								
								returnedPortsDataId.remove(mappedPortID);
							} else {
								newPort = this.portsDAO.selectByPrimaryKey(mappedPortID);
								
								if(publicSourceSystemsSet.contains(newPort.getSourceSystem())) {	
									newResult = new FuzzyPortSearchResult();
									newResult.setId(newPort.getId());
									newResult.setOriginalId(newPort.getOriginalPortId());	
									newResult.setName(newPort.getName());
									newResult.setSubdivisionId(newPort.getSubdivisionId());
									newResult.setCountryId(newPort.getCountryId());
									newResult.setSourceSystem(newPort.getSourceSystem());
									newResult.setIsMapped(newPort.getMapsTo() != null);
									newResult.setScore(0D);
									
									port.getMappedPorts().add(newResult);
								}
							}
						}
					
					returnedPortsDataId.remove(portId);
				}
			}
			
			return this.trim(updatedPortsData, limitToMaxItems);
		} catch (Throwable t) {
			this._log.error("Unable to find registration port", t);
			
			return new ArrayList<FuzzyPortSearchResult>();
		} finally {
			this.cleanup(rs, cs, c);
		}
	}
}