/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.extensions.collections.lazy;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.util.Collection;

import org.fao.fi.sh.utility.caching.spi.CacheFacade;
import org.fao.fi.vrmf.business.core.utilities.extensions.collections.impl.LazySerializableDataAccessCollection;
import org.fao.fi.vrmf.business.dao.ExtendedVesselsDAO;
import org.fao.fi.vrmf.business.dao.impl.ExtendedVesselsDAOImpl;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
final public class LazyMultipleSourcedVesselsCollection extends LazySerializableDataAccessCollection<Integer, ExtendedVessel> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3436720853618616675L;
	
	transient private Collection<Class<?>> _dataTypeSet;
	transient private ExtendedVesselsDAO _dao;
		
	private String[] _additionalSources;
	
	private Collection<Integer> _indexes;
	
	/**
	 * Class constructor
	 *
	 * @param backingCache
	 * @param sqlMapClientConfigurationResource
	 * @param originalSourceSystems
	 * @throws Throwable
	 */
	public LazyMultipleSourcedVesselsCollection(CacheFacade<Integer, ExtendedVessel> backingCache, String sqlMapClientConfigurationResource, String[] originalSourceSystems, String[] additionalSourceSystems, Collection<Class<?>> dataTypeSet) throws Throwable {
		super(backingCache, sqlMapClientConfigurationResource, originalSourceSystems);

		this._additionalSources = additionalSourceSystems;
		this._dataTypeSet = dataTypeSet;
	}
		
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.utils.extensions.collections.impl.LazySerializableDataAccessCollection#initializeDAOs()
	 */
	@Override
	protected void initializeDAOs() throws Throwable {
		this.initializeDAOs(SqlMapClientBuilder.buildSqlMapClient(Thread.currentThread().getContextClassLoader().getResourceAsStream(this._sqlMapClientConfigurationResource)));
	}
		
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.utils.extensions.collections.impl.LazySerializableDataAccessCollection#initializeDAOs(com.ibatis.sqlmap.client.SqlMapClient)
	 */
	@Override
	protected void initializeDAOs(SqlMapClient providedSqlMapClient) throws Throwable {
		this._dao = new ExtendedVesselsDAOImpl();
		this._dao.setSqlMapClient(providedSqlMapClient);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.collections.impl.LazyCollection#getSize()
	 */
	@Override
	protected int getSize() {
		try {
			if(this._backingCache.size() == 0) {
				return this._dao.selectMultipleSourcedVesselIDs(this._sourceSystems, this._additionalSources).size();
			} else {
				return this._backingCache.size();
			}
		} catch(Throwable t) {
			this._log.error("Unable to retrieve size for lazy collection " + this, t);
		}
		
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.collections.impl.LazyCollection#getIndexes()
	 */
	@Override
	protected Collection<Integer> getIndexes() {
		if(this._indexes == null) {
			try {
				this._indexes = this._dao.selectMultipleSourcedVesselIDs(this._sourceSystems, this._additionalSources);
			} catch (Throwable t) {
				this._log.error("Unable to retrieve indexes for lazy collection " + this, t);
			}
		}
		
		return this._indexes;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.collections.impl.LazyCollection#doGetElement(java.lang.Object)
	 */
	protected ExtendedVessel doGetElement(Integer index) {
		try {
			return this._dao.selectExtendedByPrimaryKey(index, this._additionalSources, this._dataTypeSet);
		} catch(Throwable t) {
			this._log.error("Unable to retrieve element with index " + index + " for lazy collection " + this, t);
		}
		
		return null;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Collection#add(java.lang.Object)
	 */
	@Override
	public boolean add(ExtendedVessel e) {
		ExtendedVessel current = this._backingCache.get($nN($nN(e, "The vessel to add cannot be NULL").getId(), "The vessel to add cannot have a NULL ID"));
		
		this._backingCache.put(e.getId(), e);
		
		return current != null;
	}
}