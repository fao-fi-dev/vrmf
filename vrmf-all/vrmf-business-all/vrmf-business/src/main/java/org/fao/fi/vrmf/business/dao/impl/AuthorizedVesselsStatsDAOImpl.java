/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractConstructorInjectionIBATISDAO;
import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractIBATISDAO;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;
import org.fao.fi.vrmf.business.dao.AuthorizedVesselsStatsDAO;
import org.fao.fi.vrmf.common.models.generated.SAgreementContributingParties;
import org.fao.fi.vrmf.common.models.stats.CodedVesselStatsReport;
import org.fao.fi.vrmf.common.models.stats.RangedVesselStatsReport;
import org.fao.fi.vrmf.common.models.stats.SimpleVesselStatsReport;

import com.ibatis.sqlmap.client.SqlMapClient;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Oct 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Oct 2011
 */
@Named
@Singleton
@IBATISNamespace(defaultNamespacePrefix="authorizedVesselStats")
public class AuthorizedVesselsStatsDAOImpl extends AbstractConstructorInjectionIBATISDAO implements AuthorizedVesselsStatsDAO {
	/**
	 * Class constructor
	 *
	 * @param client
	 */
	@Inject
	@Named(AbstractIBATISDAO.VRMF_DEFAULT_SQL_MAP_CLIENT_BEAN_ID)
	public AuthorizedVesselsStatsDAOImpl(SqlMapClient client) {
		super(client);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.AuthorizedVesselsStatsDAO#getStats(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Date)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SAgreementContributingParties> getStats(String sourceSystem, String authorizationTypeID, String authorizationStatus, String vesselsTable, String groupBy, Date atDate, Boolean excludeTerminatedVessels) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sourceSystem", sourceSystem);
		params.put("authorizationTypeId", authorizationTypeID);
		params.put("authorizationStatus", authorizationStatus);
		params.put("vesselsTable", vesselsTable);
		params.put("groupBy", groupBy);
		params.put("atDate", atDate);
		params.put("excludeTerminatedVessels", excludeTerminatedVessels);
		
		return (List<SAgreementContributingParties>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("getAuthorizedVesselsReport"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.AuthorizedVesselsStatsDAO#getLengthStats(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RangedVesselStatsReport> getLengthStats(String sourceSystem, String lengthTypeID, String authorizationIssuerID, String authorizationTypeID, String vesselsTable, String groupBy) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		
		//Pass this as part of the method signature...
		params.put("atDate", new Date());
		
		params.put("sourceSystem", sourceSystem);
		params.put("unitTypeId", lengthTypeID);
		params.put("authorizationTypeId", authorizationTypeID);
		params.put("authorizationIssuer", authorizationIssuerID);
		params.put("vesselsTable", vesselsTable);
		params.put("groupBy", groupBy);
		
		return (List<RangedVesselStatsReport>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("getAuthorizedVesselsLengthReport"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.AuthorizedVesselsStatsDAO#getTonnageStats(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RangedVesselStatsReport> getTonnageStats(String sourceSystem, String tonnageTypeID, String authorizationIssuerID, String authorizationTypeID, String vesselsTable, String groupBy) throws Throwable  {
		Map<String, Object> params = new HashMap<String, Object>();
		
		//Pass this as part of the method signature...
		params.put("atDate", new Date());
		
		params.put("sourceSystem", sourceSystem);
		params.put("unitTypeId", tonnageTypeID);
		params.put("authorizationTypeId", authorizationTypeID);
		params.put("authorizationIssuer", authorizationIssuerID);
		params.put("vesselsTable", vesselsTable);
		params.put("groupBy", groupBy);
		
		return (List<RangedVesselStatsReport>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("getAuthorizedVesselsTonnageReport"), params);		
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.AuthorizedVesselsStatsDAO#getPowerStats(java.lang.String, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RangedVesselStatsReport> getPowerStats(String sourceSystem, Boolean mainEngine, String powerTypeID, String authorizationIssuerID, String authorizationTypeID, String vesselsTable, String groupBy) throws Throwable  {
		Map<String, Object> params = new HashMap<String, Object>();
		
		//Pass this as part of the method signature...
		params.put("atDate", new Date());
		
		params.put("sourceSystem", sourceSystem);
		params.put("mainEngine", mainEngine);
		params.put("unitTypeId", powerTypeID);
		params.put("authorizationTypeId", authorizationTypeID);
		params.put("authorizationIssuer", authorizationIssuerID);
		params.put("vesselsTable", vesselsTable);
		params.put("groupBy", groupBy);
		
		return (List<RangedVesselStatsReport>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("getAuthorizedVesselsPowerReport"), params);	
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.AuthorizedVesselsStatsDAO#getAgeStats(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RangedVesselStatsReport> getAgeStats(String sourceSystem, String authorizationIssuerID, String authorizationTypeID, String vesselsTable, String groupBy) throws Throwable  {
		Map<String, Object> params = new HashMap<String, Object>();
		
		//Pass this as part of the method signature...
		params.put("atDate", new Date());
				
		params.put("sourceSystem", sourceSystem);
		params.put("authorizationTypeId", authorizationTypeID);
		params.put("authorizationIssuer", authorizationIssuerID);
		params.put("vesselsTable", vesselsTable);
		params.put("groupBy", groupBy);
		
		return (List<RangedVesselStatsReport>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("getAuthorizedVesselsAgeReport"), params);	
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.AuthorizedVesselsStatsDAO#countAuthorizedVessels(java.lang.String, java.lang.String, java.lang.String, java.util.Date, java.lang.String, java.lang.String)
	 */
	@Override
	public Integer countAuthorizedVessels(String authorizationIssuerID, String authorizationTypeID, String authorizationStatus, Date atDate, String vesselsTable, String groupBy, Boolean excludeTerminatedVessels) throws Throwable {
		return this.countAuthorizedVesselsForCountry(authorizationIssuerID, authorizationTypeID, authorizationStatus, null, atDate, vesselsTable, groupBy, excludeTerminatedVessels);
	}
	
	

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.AuthorizedVesselsStatsDAO#countAuthorizedVesselsForCountry(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.util.Date, java.lang.String, java.lang.String, java.lang.Boolean)
	 */
	@Override
	public Integer countAuthorizedVesselsForCountry(String authorizationIssuerID, String authorizationTypeID, String authorizationStatus, Integer issuingCountryID, Date atDate, String vesselsTable, String groupBy, Boolean excludeTerminatedVessels) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();	
		
		//Pass this as part of the method signature...		
		params.put("sourceSystem", authorizationIssuerID);
		
		params.put("authorizationTypeId", authorizationTypeID);
		params.put("authorizationIssuer", authorizationIssuerID);
		params.put("authorizationStatus", authorizationStatus);
		params.put("issuingCountryId", issuingCountryID);
		
		params.put("vesselsTable", vesselsTable);
		params.put("groupBy", groupBy);
		params.put("atDate", atDate);
		params.put("excludeTerminatedVessels", excludeTerminatedVessels);
		
		return (Integer)this.sqlMapClient.queryForObject(this.getQueryIDForCurrentNamespace("countAuthorizedVessels"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.AuthorizedVesselsStatsDAO#getTypeStats(java.lang.String, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CodedVesselStatsReport> getTypeStats(String sourceSystem, Boolean categoriesOnly, String categoryCode, String authorizationIssuerID, String authorizationTypeID, String vesselsTable, String groupBy) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		
		//Pass this as part of the method signature...
		params.put("atDate", new Date());
		
		params.put("sourceSystem", sourceSystem);
		params.put("categoriesOnly", categoriesOnly);
		params.put("categoryCode", categoryCode);
		params.put("authorizationTypeId", authorizationTypeID);
		params.put("authorizationIssuer", authorizationIssuerID);
		params.put("vesselsTable", vesselsTable);
		params.put("groupBy", groupBy);
		
		return (List<CodedVesselStatsReport>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("getAuthorizedVesselsTypesReport"), params);			
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.AuthorizedVesselsStatsDAO#getGearStats(java.lang.String, java.lang.Boolean, java.lang.String, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CodedVesselStatsReport> getGearStats(String sourceSystem, Boolean categoriesOnly, String categoryCode, Boolean primaryGear, String authorizationIssuerID, String authorizationTypeID, String vesselsTable, String groupBy) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		
		//Pass this as part of the method signature...
		params.put("atDate", new Date());
		
		params.put("sourceSystem", sourceSystem);
		params.put("categoriesOnly", categoriesOnly);
		params.put("categoryCode", categoryCode);
		params.put("primaryGear", primaryGear);
		params.put("authorizationTypeId", authorizationTypeID);
		params.put("authorizationIssuer", authorizationIssuerID);
		params.put("vesselsTable", vesselsTable);
		params.put("groupBy", groupBy);
		
		return (List<CodedVesselStatsReport>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("getAuthorizedVesselsGearsReport"), params);	
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.AuthorizedVesselsStatsDAO#getAuthorizedVesselsSumamry(java.lang.String, java.lang.String, java.lang.String, java.util.Date, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SimpleVesselStatsReport> getAuthorizedVesselsSummary(String authorizationIssuerID, String authorizationTypeID, String authorizationStatus, Date atDate, String vesselsTable, String groupBy) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		
		//Pass this as part of the method signature...
		params.put("atDate", new Date());
				
		//Pass this as part of the method signature...
		params.put("sourceSystem", authorizationIssuerID);
		
		params.put("authorizationTypeId", authorizationTypeID);
		params.put("authorizationIssuer", authorizationIssuerID);
		params.put("authorizationStatus", authorizationStatus);
		params.put("vesselsTable", vesselsTable);
		params.put("groupBy", groupBy);
		
		return (List<SimpleVesselStatsReport>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("getAuthorizedVesselsSummaryReport"), params);	
	}	
}