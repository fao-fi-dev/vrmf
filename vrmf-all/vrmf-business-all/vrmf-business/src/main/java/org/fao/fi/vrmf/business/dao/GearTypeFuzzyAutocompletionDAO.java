/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.Collection;
import java.util.Date;

import org.fao.fi.vrmf.common.models.search.autocompletion.typed.FuzzyGearTypeSearchResult;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
public interface GearTypeFuzzyAutocompletionDAO {
	Collection<FuzzyGearTypeSearchResult> searchGearTypes(String vesselName, Date atDate, Integer limitToMaxItems, String[] availableSources, String[] sourceSystems, String[] publicSourceSystems, Boolean groupByUID, boolean groupMappedTypes) throws Throwable;
	Collection<FuzzyGearTypeSearchResult> searchGearTypes(String procedureName, String vesselName, Date atDate, Integer limitToMaxItems, String[] availableSources, String[] sourceSystems, String[] publicSourceSystems, Boolean groupByUID, boolean groupMappedTypes) throws Throwable;
}
