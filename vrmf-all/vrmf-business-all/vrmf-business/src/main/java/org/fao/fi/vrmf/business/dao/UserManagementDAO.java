/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.Collection;
import java.util.Set;

import org.fao.fi.vrmf.business.core.dao.ibatis.SetterInjectionIBATISDAO;
import org.fao.fi.vrmf.common.models.generated.Entity;
import org.fao.fi.vrmf.common.models.generated.Users;
import org.fao.fi.vrmf.common.models.security.UserCapability;
import org.fao.fi.vrmf.common.models.security.UserRole;
import org.fao.fi.vrmf.common.models.security.VRMFUser;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Jan 2011
 */
public interface UserManagementDAO extends SetterInjectionIBATISDAO {
	VRMFUser login(String username, String password) throws Throwable;
	VRMFUser changePassword(String username, String password, String newPassword) throws Throwable;
	
	Users getUser(String username) throws Throwable;
	Entity getEntityForUser(Users user) throws Throwable;
	Entity getGranterEntityForUser(Users user) throws Throwable;
	
	Set<UserCapability> getCapabilitiesForUser(String username) throws Throwable;
	Set<UserRole> getRolesForUser(String username) throws Throwable;
	
	VRMFUser getAllUserDetails(String username) throws Throwable;
	Collection<VRMFUser> listApplicativeUserDetails(String applicationID, boolean realUser, boolean countryManagersOnly) throws Throwable;
}
