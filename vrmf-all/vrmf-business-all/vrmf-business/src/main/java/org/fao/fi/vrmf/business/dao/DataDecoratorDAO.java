/**
 * (c) 2016 FAO / UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.Collection;
import java.util.List;

import org.fao.fi.vrmf.business.core.dao.ibatis.SetterInjectionIBATISDAO;
import org.fao.fi.vrmf.common.models.extended.ExtendedAuthorizations;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToBeneficialOwner;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToCallsigns;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToFishingLicense;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToFishingMaster;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToFlags;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToGear;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToHullMaterial;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToInspection;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToIuuList;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToLengths;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToMaster;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToNonCompliance;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToOperator;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToOwner;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToPortEntryDenial;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToPower;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToRegistration;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToShipbuilder;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToStatus;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToTonnage;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToType;
import org.fao.fi.vrmf.common.models.generated.Authorizations;
import org.fao.fi.vrmf.common.models.generated.VesselsToBeneficialOwners;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToFishingLicense;
import org.fao.fi.vrmf.common.models.generated.VesselsToFishingMasters;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToGears;
import org.fao.fi.vrmf.common.models.generated.VesselsToHullMaterial;
import org.fao.fi.vrmf.common.models.generated.VesselsToInspections;
import org.fao.fi.vrmf.common.models.generated.VesselsToIuuList;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToMasters;
import org.fao.fi.vrmf.common.models.generated.VesselsToNonCompliance;
import org.fao.fi.vrmf.common.models.generated.VesselsToOperators;
import org.fao.fi.vrmf.common.models.generated.VesselsToOwners;
import org.fao.fi.vrmf.common.models.generated.VesselsToPortEntryDenials;
import org.fao.fi.vrmf.common.models.generated.VesselsToPower;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;
import org.fao.fi.vrmf.common.models.generated.VesselsToShipbuilders;
import org.fao.fi.vrmf.common.models.generated.VesselsToStatus;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;
import org.fao.fi.vrmf.common.models.generated.VesselsToTypes;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Feb 9, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Feb 9, 2016
 */
public interface DataDecoratorDAO extends SetterInjectionIBATISDAO {
	List<ExtendedAuthorizations> buildAuthorizations(Collection<Authorizations> authorizationsData) throws Throwable;
	List<ExtendedAuthorizations> buildAuthorizationsOnly(Collection<Authorizations> authorizationsData) throws Throwable;
	List<ExtendedVesselToBeneficialOwner> buildBeneficialOwners(Collection<VesselsToBeneficialOwners> beneficialOwnersData) throws Throwable;
	List<ExtendedVesselToCallsigns> buildCallsigns(Collection<VesselsToCallsigns> callsignData) throws Throwable;
	List<ExtendedVesselToFishingLicense> buildFishingLicenses(Collection<VesselsToFishingLicense> fishingLicenseData) throws Throwable;
	List<ExtendedVesselToFishingMaster> buildFishingMasters(Collection<VesselsToFishingMasters> fishingMastersData) throws Throwable;
	List<ExtendedVesselToHullMaterial> buildHullMaterials(Collection<VesselsToHullMaterial> vesselHullMaterialData) throws Throwable;
	List<ExtendedVesselToInspection> buildInspections(Collection<VesselsToInspections> inspectionData) throws Throwable;
	List<ExtendedVesselToIuuList> buildIuuLists(Collection<VesselsToIuuList> iuuListData) throws Throwable;
	List<ExtendedVesselToMaster> buildMasters(Collection<VesselsToMasters> mastersData) throws Throwable;
	List<ExtendedVesselToNonCompliance> buildNonCompliances(Collection<VesselsToNonCompliance> nonComplianceData) throws Throwable;
	List<ExtendedVesselToOperator> buildOperators(Collection<VesselsToOperators> operatorsData) throws Throwable;
	List<ExtendedVesselToOwner> buildOwners(Collection<VesselsToOwners> ownersData) throws Throwable;
	List<ExtendedVesselToPortEntryDenial> buildPortEntryDenials(Collection<VesselsToPortEntryDenials> denialsData) throws Throwable;
	List<ExtendedVesselToRegistration> buildRegistrations(Collection<VesselsToRegistrations> registrationsData) throws Throwable;
	List<ExtendedVesselToShipbuilder> buildShipbuilders(Collection<VesselsToShipbuilders> shipbuildersData) throws Throwable;
	List<ExtendedVesselToFlags> buildVesselFlags(Collection<VesselsToFlags> vesselFlagData) throws Throwable;
	List<ExtendedVesselToGear> buildVesselGears(Collection<VesselsToGears> gearTypesData) throws Throwable;
	List<ExtendedVesselToLengths> buildVesselLengths(Collection<VesselsToLengths> vesselLengthData) throws Throwable;
	List<ExtendedVesselToPower> buildVesselPowers(Collection<VesselsToPower> vesselPowerData) throws Throwable;
	List<ExtendedVesselToStatus> buildVesselStatus(Collection<VesselsToStatus> vesselStatusData) throws Throwable;
	List<ExtendedVesselToTonnage> buildVesselTonnages(Collection<VesselsToTonnage> vesselTonnageData) throws Throwable;
	List<ExtendedVesselToType> buildVesselTypes(Collection<VesselsToTypes> vesselTypesData) throws Throwable;
}