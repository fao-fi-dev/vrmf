/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.Collection;

import org.fao.fi.vrmf.common.models.search.autocompletion.typed.IdentifierSearchResult;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
public interface VesselIdentifierAutocompletionDAO {
	Collection<IdentifierSearchResult> searchVesselIdentifier(String identifierType, String identifier, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable;
	Collection<IdentifierSearchResult> searchVesselIdentifier(String procedureName, String identifierType, String identifier, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable;
}
