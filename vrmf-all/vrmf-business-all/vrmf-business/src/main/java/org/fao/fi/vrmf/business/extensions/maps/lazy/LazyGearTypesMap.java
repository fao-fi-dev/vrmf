/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.extensions.maps.lazy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.fao.fi.vrmf.business.core.utilities.extensions.maps.impl.LazySerializableDataAccessMap;
import org.fao.fi.vrmf.business.dao.generated.SGearTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SGearTypesDAOImpl;
import org.fao.fi.vrmf.common.models.generated.SGearTypes;
import org.fao.fi.vrmf.common.models.generated.SGearTypesExample;

import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
public class LazyGearTypesMap extends LazySerializableDataAccessMap<Integer, SGearTypes> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8602579370194965528L;

	transient private SGearTypesDAO _dao;
	
	/**
	 * Class constructor
	 *
	 * @param dao
	 * @throws Throwable
	 */
	public LazyGearTypesMap(SGearTypesDAO dao) throws Throwable {
		super();
		
		this._dao = dao;
	}
	
	/**
	 * Class constructor
	 *
	 * @param dao
	 * @param sourceSystems
	 */
	public LazyGearTypesMap(String sqlMapConfigurationResource, String[] sourceSystems) throws Throwable {
		super(sqlMapConfigurationResource, sourceSystems);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazySerializableDataAccessMap#initializeDAO()
	 */
	@Override
	protected void initializeDAO() throws Throwable {
		this._dao = new SGearTypesDAOImpl();
		this._dao.setSqlMapClient(SqlMapClientBuilder.buildSqlMapClient(Thread.currentThread().getContextClassLoader().getResourceAsStream(this._sqlMapClientConfigurationResource)));
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetElement(java.lang.Object)
	 */
	protected SGearTypes doGetElement(Integer key) throws Throwable {
		return this._dao.selectByPrimaryKey(key);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetKeySet(java.lang.Object)
	 */
	@Override
	protected Set<Integer> doGetKeySet() throws Throwable {
		SGearTypesExample filter = new SGearTypesExample();
			
		if(this._sourceSystems != null && this._sourceSystems.length > 0)
			filter.createCriteria().andSourceSystemIn(Arrays.asList(this._sourceSystems));
			
		Set<Integer> keySet = new HashSet<Integer>();
		
		for(SGearTypes gearType : this._dao.selectByExample(filter)) {
			keySet.add(gearType.getId());
		}
		
		return keySet;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetSize()
	 */
	@Override
	protected int doGetSize() throws Throwable {
		SGearTypesExample filter = new SGearTypesExample();
		
		if(this._sourceSystems != null && this._sourceSystems.length > 0)
			filter.createCriteria().andSourceSystemIn(Arrays.asList(this._sourceSystems));
		
		return this._dao.countByExample(filter);
	}
}