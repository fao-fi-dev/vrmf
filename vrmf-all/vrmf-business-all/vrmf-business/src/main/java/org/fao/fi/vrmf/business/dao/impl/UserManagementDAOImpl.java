/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.helpers.singletons.io.MD5Helper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractSetterInjectionIBATISDAO;
import org.fao.fi.vrmf.business.dao.UserManagementDAO;
import org.fao.fi.vrmf.business.dao.generated.EntityDAO;
import org.fao.fi.vrmf.business.dao.generated.UsersDAO;
import org.fao.fi.vrmf.business.dao.generated.UsersToCapabilityGroupsDAO;
import org.fao.fi.vrmf.business.dao.generated.UsersToManagedCountriesDAO;
import org.fao.fi.vrmf.business.dao.generated.UsersToManagedSourcesDAO;
import org.fao.fi.vrmf.common.models.generated.Entity;
import org.fao.fi.vrmf.common.models.generated.SCapability;
import org.fao.fi.vrmf.common.models.generated.Users;
import org.fao.fi.vrmf.common.models.generated.UsersExample;
import org.fao.fi.vrmf.common.models.generated.UsersToCapabilityGroups;
import org.fao.fi.vrmf.common.models.generated.UsersToCapabilityGroupsExample;
import org.fao.fi.vrmf.common.models.generated.UsersToManagedCountriesExample;
import org.fao.fi.vrmf.common.models.generated.UsersToManagedSourcesExample;
import org.fao.fi.vrmf.common.models.security.UserCapability;
import org.fao.fi.vrmf.common.models.security.UserRole;
import org.fao.fi.vrmf.common.models.security.VRMFUser;
import org.springframework.cache.annotation.Cacheable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Mar 2011
 */
@NoArgsConstructor
@Named @Singleton
public class UserManagementDAOImpl extends AbstractSetterInjectionIBATISDAO implements UserManagementDAO {
	static final private String DEFAULT_METHODS_INVOCATION_CACHE_ID = "vrmf.business.user.management";

	@Inject private @Getter @Setter EntityDAO entityDAO;
	@Inject private @Getter @Setter UsersDAO usersDAO;
	@Inject	private @Getter @Setter UsersToCapabilityGroupsDAO usersToCapabilityGroupsDAO;
	@Inject private @Getter @Setter UsersToManagedCountriesDAO usersToManagedCountriesDAO;
	@Inject private @Getter @Setter UsersToManagedSourcesDAO usersToManagedSourcesDAO;
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.UserManagementDAO#login(java.lang.String, java.lang.String)
	 */
	@Override
	public VRMFUser login(String username, String password) throws Throwable {
		if(StringsHelper.trim(username) == null)
			return null;
				
		VRMFUser user = new VRMFUser(username);
		
		Users logged = this.updateUserDetails(user, username, password);
		
		return logged == null ? null : user;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.UserManagementDAO#getEntityForUser(org.fao.vrmf.utilities.common.models.generated.Users)
	 */
	@Override
	public Entity getEntityForUser(Users user) throws Throwable {
		if(user == null || user.getEntityId() == null)
			return null;
		
		return this.entityDAO.selectByPrimaryKey(user.getEntityId());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.UserManagementDAO#getGranterEntityForUser(org.fao.vrmf.utilities.common.models.generated.Users)
	 */
	@Override
	public Entity getGranterEntityForUser(Users user) throws Throwable {
		if(user == null || user.getGranterId() == null)
			return null;
		
		return this.entityDAO.selectByPrimaryKey(user.getGranterId());
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.UserManagementDAO#changePassword(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public VRMFUser changePassword(String username, String password, String newPassword) throws Throwable {
		if(StringsHelper.trim(username) == null)
			return null;
		
		UsersToManagedCountriesExample filter = new UsersToManagedCountriesExample();
		filter.createCriteria().andUserIdEqualTo(username);
		
		VRMFUser user = new VRMFUser(username);

		Users userDetails = this.updateUserDetails(user, username, password);
		
		if(userDetails == null)
			return null;
		
		this.sqlMapClient.startTransaction();
		
		try {
			userDetails.setPassword(MD5Helper.digest(newPassword == null ? "" : newPassword, 2));
			this.usersDAO.updateByPrimaryKey(userDetails);
			
			this.sqlMapClient.commitTransaction();
			
			this._log.info("User {} has successfully updated his password", username);
		} catch(Throwable t) {
			this._log.error("Unable to update password for user {}", username, t);
			
			user = null;
		} finally {
			this.sqlMapClient.endTransaction();
		}
		
		return user;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.UserManagementDAO#getCapabilitiesForUser(java.lang.String)
	 */
	@Override
	public Set<UserCapability> getCapabilitiesForUser(String username) throws Throwable {
		if(username == null)
			return null;

		@SuppressWarnings("unchecked")
		List<SCapability> userCapabilities = (List<SCapability>)this.sqlMapClient.queryForList("userManagement.getCapabilitiesForUser", username);
		
		if(userCapabilities == null || userCapabilities.isEmpty())
			return null;
		
		Set<UserCapability> capabilities = new HashSet<UserCapability>();
		
		for(SCapability capability : userCapabilities) {
			capabilities.add(new UserCapability(capability.getId(), capability.getParameters()));
		}
		
		return capabilities;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.UserManagementDAO#getRolesForUser(java.lang.String)
	 */	
	@SuppressWarnings("unchecked")
	@Override
	public Set<UserRole> getRolesForUser(String username) throws Throwable {
		if(username == null)
			return null;
		
		UsersToCapabilityGroupsExample groupsNullDateFilter = new UsersToCapabilityGroupsExample();
		groupsNullDateFilter.createCriteria().andUserIdEqualTo(username).andValidFromLessThanOrEqualTo(new Date()).andValidToIsNull();
		
		UsersToCapabilityGroupsExample groupsDateFilter = new UsersToCapabilityGroupsExample();
		groupsDateFilter.createCriteria().andUserIdEqualTo(username).andValidFromLessThanOrEqualTo(new Date()).andValidToIsNotNull().andValidToGreaterThanOrEqualTo(new Date());
		
		List<UsersToCapabilityGroups> userGroups = this.usersToCapabilityGroupsDAO.selectByExample(groupsNullDateFilter);
		userGroups.addAll(this.usersToCapabilityGroupsDAO.selectByExample(groupsDateFilter));
		
		if(userGroups.isEmpty())
			return null;

		Set<UserRole> roles = new HashSet<UserRole>();
		UserRole role;
		Set<UserCapability> capabilitiesForRole;
		
		for(UsersToCapabilityGroups utcg : userGroups) {
			role = new UserRole(utcg.getCapabilityGroupId());
			
			capabilitiesForRole = new HashSet<UserCapability>();
			
			for(SCapability capability : (List<SCapability>)this.sqlMapClient.queryForList("userManagement.getCapabilitiesForGroup", utcg.getCapabilityGroupId())) {
				capabilitiesForRole.add(new UserCapability(capability.getId(), capability.getParameters()));
			}
			
			role.setCapabilities(capabilitiesForRole.toArray(new UserCapability[0]));

			roles.add(role);
		}
		
		return roles;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.UserManagementDAO#getUser(java.lang.String)
	 */
	@Override
	public Users getUser(String username) throws Throwable {
		if(username == null)
			return null;
		
		UsersExample userFilter = new UsersExample();
		userFilter.createCriteria().andIdEqualTo(username);
		
		List<Users> basicUsers = (List<Users>)this.usersDAO.selectByExample(userFilter);
		
		if(basicUsers == null || basicUsers.isEmpty()) {
			this._log.info("Missing user for '" + username + "'");
			
			return null;
		}
		
		//This will *NEVER* happen...
		if(basicUsers.size() > 1) {
			this._log.warn("Found multiple users valid for username '" + username + "'");
			
			return null;
		}

		return basicUsers.get(0);
	}

	private Users updateUserDetails(VRMFUser user, String username, String password) throws Throwable {
		if(user == null || StringsHelper.trim(username) == null)
			return null;
		
		UsersExample userFilter = new UsersExample();
		userFilter.createCriteria().andIdEqualTo(username).andPasswordEqualTo(MD5Helper.digest(password == null ? "" : password, 2));
		
		List<Users> basicUsers = (List<Users>)this.usersDAO.selectByExample(userFilter);
		
		if(basicUsers == null || basicUsers.isEmpty()) {
			this._log.info("Wrong credentials or missing user for '" + username + "'");
			
			return null;
		}
		
		if(basicUsers.size() > 1) {
			this._log.warn("Found multiple users valid for credentials '" + username + "' / '" + password + "'");
			
			return null;
		}
		
		Set<UserCapability> capabilities = this.getCapabilitiesForUser(username);
					
		if(capabilities == null || capabilities.isEmpty()) {
			this._log.error("User '" + username + "' has no current capabilities");
			
			return null;
		} 
		
		Set<UserRole> roles = this.getRolesForUser(username);
		
		if(roles != null && !roles.isEmpty())
			user.addRoles(roles);
				
		UsersToManagedCountriesExample managedCountriesFilter = new UsersToManagedCountriesExample();
		managedCountriesFilter.createCriteria().andUserIdEqualTo(username);
		
		user.setManagedCountries(this.usersToManagedCountriesDAO.selectByExample(managedCountriesFilter));		

		UsersToManagedSourcesExample managedSourcesFilter = new UsersToManagedSourcesExample();
		managedSourcesFilter.createCriteria().andUserIdEqualTo(username);
		
		user.setManagedSources(this.usersToManagedSourcesDAO.selectByExample(managedSourcesFilter));		

		Users toReturn = basicUsers.get(0);
		
		user.setUserEntity(this.getEntityForUser(toReturn));
		user.setGranterEntity(this.getGranterEntityForUser(toReturn));
		
		return toReturn;
	}

	public String buildAllUserDetailsCacheKey(String username) {
		String key = DEFAULT_METHODS_INVOCATION_CACHE_ID + "_getAllUserDetails_" + username;
		
		this._log.debug("Cache key for " + ObjectsHelper.getCurrentMethod(this) + " is " + key);
		
		return key;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.UserManagementDAO#getAllUserDetails(java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.target.buildAllUserDetailsCacheKey(#username)")
	public VRMFUser getAllUserDetails(String username) throws Throwable {
		this._log.debug("Getting all user details for " + username);
		
		long end, start = System.currentTimeMillis();
		
		if(username == null)
			return null;
		
		VRMFUser toReturn = new VRMFUser(username);
		Users found = null;
		
		UsersExample userFilter = new UsersExample();
		userFilter.createCriteria().andIdEqualTo(username);
		
		List<Users> basicUsers = (List<Users>)this.usersDAO.selectByExample(userFilter);
		
		if(basicUsers == null || basicUsers.isEmpty()) {
			this._log.info("Missing user details for '" + username + "'");
			
			return null;
		}
		
		if(basicUsers.size() > 1) {
			this._log.warn("Found multiple users valid for '" + username + "'");
			
			return null;
		}
		
		found = basicUsers.get(0);
		
		Set<UserCapability> capabilities = this.getCapabilitiesForUser(username);
		
		if(capabilities == null || capabilities.isEmpty()) {
			this._log.error("User '" + username + "' has no current capabilities");
			
			return null;
		} else {
			
		}
		
		Set<UserRole> roles = this.getRolesForUser(username);
		
		if(roles != null && !roles.isEmpty())
			toReturn.addRoles(roles);
		
		UsersToManagedCountriesExample managedCountriesFilter = new UsersToManagedCountriesExample();
		managedCountriesFilter.createCriteria().andUserIdEqualTo(username);
		
		toReturn.setManagedCountries(this.usersToManagedCountriesDAO.selectByExample(managedCountriesFilter));		

		UsersToManagedSourcesExample managedSourcesFilter = new UsersToManagedSourcesExample();
		managedSourcesFilter.createCriteria().andUserIdEqualTo(username);
		
		toReturn.setManagedSources(this.usersToManagedSourcesDAO.selectByExample(managedSourcesFilter));		
		
		if(found.getEntityId() != null)
			toReturn.setUserEntity(this.entityDAO.selectByPrimaryKey(found.getEntityId()));

		if(found.getGranterId() != null)
			toReturn.setGranterEntity(this.entityDAO.selectByPrimaryKey(found.getGranterId()));
		
		end = System.currentTimeMillis();
		
		this._log.info("Getting all user details for " + username + " took " + ( end - start ) + " mSec.");
		
		return toReturn;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.UserManagementDAO#listApplicativeUserDetails(java.lang.String, boolean)
	 */
	@Override
	public Collection<VRMFUser> listApplicativeUserDetails(String applicationID, boolean realUser, boolean countryManagersOnly) throws Throwable {
		if(applicationID == null)
			return null;
		
		Date now = new Date();
		
		UsersToCapabilityGroupsExample filter = new UsersToCapabilityGroupsExample();
		
		filter.createCriteria().andCapabilityGroupIdEqualTo(applicationID + "_USER").
								andValidFromLessThanOrEqualTo(now).andValidToIsNull();
								
		filter.or(filter.createCriteria().andCapabilityGroupIdEqualTo(applicationID + "_USER").
										  andValidFromLessThanOrEqualTo(now).
										  andValidToGreaterThanOrEqualTo(now));
		
		List<UsersToCapabilityGroups> filtered = this.usersToCapabilityGroupsDAO.selectByExample(filter);
		
		if(filtered == null || filtered.isEmpty())
			return null;
		
		Set<String> users = new TreeSet<String>();
		
		boolean keep = false;
		
		UsersToCapabilityGroupsExample realUserFilter = null;
		
		for(UsersToCapabilityGroups current : filtered) {
			keep = !realUser;
			 
			if(realUser) {
				realUserFilter = new UsersToCapabilityGroupsExample();
				realUserFilter.createCriteria().andUserIdEqualTo(current.getUserId()).andCapabilityGroupIdEqualTo("REAL_USER");
				
				keep = this.usersToCapabilityGroupsDAO.countByExample(realUserFilter) > 0;
			}
			
			if(keep)
				users.add(current.getUserId());
		}
		
		Collection<VRMFUser> toReturn = new ArrayList<VRMFUser>();
		
		VRMFUser retrieved = null;
		for(String userID : users) {
			retrieved = this.getAllUserDetails(userID);
			
			if(retrieved != null && ( !countryManagersOnly || !retrieved.getManagedCountriesBySystem(applicationID).isEmpty() ))
				toReturn.add(retrieved);
		}
		
		return toReturn;
	}
}