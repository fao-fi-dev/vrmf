/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.fao.fi.vrmf.business.core.dao.ibatis.SetterInjectionIBATISDAO;
import org.fao.fi.vrmf.common.models.generated.SSystems;
import org.fao.fi.vrmf.common.models.generated.SSystemsGroups;
import org.fao.fi.vrmf.common.models.stats.StatusReportBySource;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Jan 2011
 */
public interface CommonMetadataDAO extends SetterInjectionIBATISDAO {
	List<SSystems> getAuthorizationsIssuerSystems(Collection<String> managedSystemsIDs) throws Exception;
	List<SSystemsGroups> getAuthorizationsIssuerSystemsGroups(Collection<String> managedSystemsIDs) throws Exception;
	
	Integer getLastID() throws Exception;
	Integer getNextUID(String table) throws Exception;
	
	Integer getPreviousVesselUID(Integer vesselID) throws Exception;
	
	void lockTable(String table) throws Exception;
	void unlockTables() throws Exception;
	
//	Integer getNextUIDForSystem(String system) throws Exception;
	Integer getNextUIDForSystem(String system, String table) throws Exception;
	
//	Integer getNextUIDForAuthorizationSystem(String system) throws Exception;
	Integer getNextUIDForAuthorizationSystem(String system, String table) throws Exception;

	List<Integer> getAuthorizedIDsByUID(Integer vesselUID, String authorizationIssuerID, String authorizationTypeID, Boolean excludeTerminatedVessels, String table) throws Exception;
	
	Date computeLastUpdateDateByID(Integer vesselID) throws Exception;
	Date computeLastUpdateDateByUID(Integer vesselUID, String table) throws Exception;
	
	Date computeLastUpdateDateByIDAndSources(Integer vesselID, String[] systems) throws Exception;
	Date computeLastUpdateDateByUIDAndSources(Integer vesselUID, String[] systems, String table) throws Exception;
		
	Date getLastUpdateDateByUID(Integer vesselUID, String table) throws Exception;
	Date getLastUpdateDateByUIDAndSources(Integer vesselUID, String[] system, String table) throws Exception;
	
	List<StatusReportBySource> getStatusReportBySources(String[] availableSources, String table) throws Exception;
}