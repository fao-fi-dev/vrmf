/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.Collection;

import org.fao.fi.sh.utility.core.helpers.singletons.io.MD5Helper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.model.extensions.dates.SmartDate;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Mar 2013
 */
abstract public class AbstractVesselRecordHelperDAOImpl extends CommonVesselRecordSearchDAOImpl {
	protected Logger _queryLogger = LoggerFactory.getLogger("org.fao.vrmf.vessels.search.queries");
	
	protected String toSanitizedXML(VesselRecordSearchFilter filter) {
		return this.toSanitizedXML(filter, true);
	}
	
	protected String toSanitizedXML(VesselRecordSearchFilter filter, boolean compact) {
		String xml = filter == null ? "" : filter.toXML();
		
		return !compact ? ( "\n" + xml ) : xml.replaceAll("\\r|\\n|\\t", "").replaceAll("\\>\\s{1,}\\<", "><");
	}
	
	public String buildExplicitCountVesselsKey(VesselRecordSearchFilter filter) {
		String key = filter.getFrom();
		key += "_" + CollectionsHelper.join(CollectionsHelper.sortArray(filter.getAvailableSourceSystems()), "|");
		key += "_" + CollectionsHelper.join(CollectionsHelper.sortArray(filter.getSourceSystems()), "|");
		key += "_" + filter.getGroupBy();
		key += "_" + filter.getSearchDuplicatesOnly();
		key += "_" + ( filter.getAtDate() == null ? null : new SmartDate(filter.getAtDate()).roundToDay() );
		key += "_" + filter.getMinSources();
		key += "_" + filter.getMaxSources();
		key += "_" + ( filter.getUpdatedFrom() == null ? null : new SmartDate(filter.getUpdatedFrom()).roundToDay());
		key += "_" + ( filter.getUpdatedTo() == null ? null : new SmartDate(filter.getUpdatedTo()).roundToDay());
		key += "_" + filter.clauses().asString();
		
		this._log.debug("'count vessels' explicit cache key is: " + key);
		
		return key;
	}
	
	public String buildCountVesselsKey(VesselRecordSearchFilter filter) {
		String key = this.buildExplicitCountVesselsKey(filter);
		
		try {
			key = MD5Helper.digest(key);
		} catch(Throwable t) {
			//Suffocate
		}
		
		this._log.debug("'count vessels' cache key is: " + key);
		
		return key;
	}
	
	public String buildExplicitFilterVesselsKey(VesselRecordSearchFilter filter) {
		String key = this.buildExplicitCountVesselsKey(filter);
		key += "_" + filter.getLinkMetadataToSearchCriteria();
		key += "_" + filter.getOffset();
		key += "_" + filter.getLimit();
		key += "_" + filter.sortableColumns().asString();
		
		this._log.debug("'filter vessels' explicit cache key is: " + key);
		
		return key;
	}
	
	public String buildFilterVesselsKey(VesselRecordSearchFilter filter) {
		String key = this.buildExplicitFilterVesselsKey(filter);
		try {
			key = MD5Helper.digest(key);
		} catch(Throwable t) {
			//Suffocate
		}
		
		this._log.debug("'filter vessels' cache key is: " + key);
		
		return key;
	}
	
	public String buildExplicitGetVesselsDataKey(VesselRecordSearchFilter filter, Collection<Integer> IDs) {
		String key = this.buildExplicitFilterVesselsKey(filter);
		key += "_" + CollectionsHelper.join(CollectionsHelper.sortCollection(IDs), "|");
		
		this._log.debug("'get vessels' data' explicit cache key is: " + key);
		
		return key;
	}
	
	public String buildGetVesselsDataKey(VesselRecordSearchFilter filter, Collection<Integer> IDs) {
		String key = this.buildExplicitGetVesselsDataKey(filter, IDs);
		try {
			key = MD5Helper.digest(key);
		} catch(Throwable t) {
			//Suffocate
		}
		
		this._log.debug("'get vessels' data' cache key is: " + key);
		
		return key;
	}
	
	public String buildExplicitGetVesselsMetadataKey(VesselRecordSearchFilter filter, Collection<Integer> IDs) {
		String key = this.buildExplicitFilterVesselsKey(filter);
		key += "_" + CollectionsHelper.join(CollectionsHelper.sortCollection(IDs), "|");
		
		this._log.debug("'get vessels' metadata' explicit cache key is: " + key);
		
		return key;
	}
	
	public String buildGetVesselsMetadataKey(VesselRecordSearchFilter filter, Collection<Integer> IDs) {
		String key = this.buildExplicitGetVesselsMetadataKey(filter, IDs);
		try {
			key = MD5Helper.digest(key);
		} catch(Throwable t) {
			//Suffocate
		}
		
		this._log.debug("'get vessels' metadata' cache key is: " + key);
		
		return key;
	}
	
	protected String filterToXML(VesselRecordSearchFilter filter) {
		return filter == null ? "<nil/>" : this.toSanitizedXML(filter);
	}
	
	protected String computeFilterKey(VesselRecordSearchFilter filter) {
		try {
			return filter == null ? "0" : MD5Helper.digest(this.toSanitizedXML(filter));
		} catch(Throwable t) {
			return String.valueOf(filter.hashCode());
		}
	}
	
	protected String computeFilterKey(String filterXML) {
		try {
			return filterXML == null ? "0" : MD5Helper.digest(filterXML);
		} catch(Throwable t) {
			return String.valueOf(filterXML.hashCode());
		}
	}
	
	protected void trackQueryExecution(String queryAsXML, String key, long start) {
		long end = System.currentTimeMillis();
		
		long elapsed = end - start;
		
		if(elapsed < 1000)
			this._queryLogger.debug("Query execution took {} mSec. [ key: {} ] {}", elapsed, key, queryAsXML);
		else if(elapsed < 2000) 
			this._queryLogger.info("Query execution took {} mSec. [ key: {} ] {}", elapsed, key, queryAsXML);
		else 
			this._queryLogger.warn("Query execution took {} mSec. [ key: {} ] {}", elapsed, key, queryAsXML);
	}
}