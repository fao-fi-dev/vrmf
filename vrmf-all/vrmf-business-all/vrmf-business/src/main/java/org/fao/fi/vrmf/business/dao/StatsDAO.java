/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.List;

import org.fao.fi.vrmf.common.models.stats.CountriesToFlag;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2011
 */
public interface StatsDAO {
	List<Integer> getAvailableYears() throws Throwable;
	
	List<CountriesToFlag> getFlagStatsByYear(int year, boolean cumulative) throws Throwable;
}
