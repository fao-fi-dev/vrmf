/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.List;

import org.fao.fi.vrmf.common.models.indexing.SourceUpdateMetadata;
import org.fao.fi.vrmf.common.models.indexing.VesselUpdateMetadata;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2013
 */
public interface VesselIndexingDAO {
	List<Integer> getIDsForUID(String vesselsTable, Integer vesselUID) throws Throwable;
	
	List<Integer> getRandomVesselIDs(String[] sources, String groupBy, long limit) throws Throwable;
	
	Integer countSourcesData(String[] sources, String groupBy) throws Throwable;
	Integer countIMOSourcesData(String[] sources, String groupBy) throws Throwable;
	
	List<VesselUpdateMetadata> getVesselIdentifiers(String[] sources, String groupBy, Integer limit, Integer offset) throws Throwable;
	List<VesselUpdateMetadata> getIMOVesselIdentifiers(String[] sources, String groupBy, Integer limit, Integer offset) throws Throwable;
	
	List<SourceUpdateMetadata> getSourceUpdateMetadata(String[] sources, String groupBy) throws Throwable;
	List<SourceUpdateMetadata> getIMOSourceUpdateMetadata(String[] sources, String groupBy) throws Throwable;
}
