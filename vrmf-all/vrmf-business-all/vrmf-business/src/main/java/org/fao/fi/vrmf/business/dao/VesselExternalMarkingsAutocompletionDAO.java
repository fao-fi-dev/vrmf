/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.Collection;
import java.util.Date;

import org.fao.fi.vrmf.common.models.search.autocompletion.typed.ExternalMarkingSearchResult;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
public interface VesselExternalMarkingsAutocompletionDAO {
	Collection<ExternalMarkingSearchResult> searchVesselExternalMarkings(String externalMarking, Date atDate, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable;
	Collection<ExternalMarkingSearchResult> searchVesselExternalMarkings(String procedureName, String externalMarking, Date atDate, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable;
}
