/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.vrmf.business.dao.VesselFishingLicenseAutocompletionDAO;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.FishingLicenseSearchResult;
import org.springframework.cache.annotation.Cacheable;

import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class VesselFishingLicenseAutocompletionDAOImpl extends AbstractAutocompletionDAOImpl implements VesselFishingLicenseAutocompletionDAO {
	public String getProcedureName(boolean authorizedOnly) {
		return "FISHING_LICENSE_SEARCH" + ( authorizedOnly ? "_BY_AUTH" : "" );
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselFishingLicenseAutocompletionDAO#searchVesselRegNo(java.lang.Integer[], java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#issuingCountries) + " +
															  "#fishingLicense + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID")
	public Collection<FishingLicenseSearchResult> searchVesselFishingLicense(Integer[] issuingCountries, String fishingLicense, Date atDate, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		return this.searchVesselFishingLicense(this.getProcedureName(false), issuingCountries, fishingLicense, atDate, limitToMaxItems, sourceSystems, groupByUID);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.VesselFishingLicenseAutocompletionDAO#searchVesselRegNo(java.lang.String, java.lang.Integer[], java.lang.String, java.util.Date, java.lang.Integer, java.lang.Integer, java.lang.String[], java.lang.Boolean, java.lang.Boolean, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sourceSystems) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#issuingCountries) + " +
															  "#procedureName + " +
															  "#fishingLicense + " +
															  "#atDate + " +
															  "#limitToMaxItems + " + 
															  "#groupByUID")
	public Collection<FishingLicenseSearchResult> searchVesselFishingLicense(String procedureName, Integer[] issuingCountries, String fishingLicense, Date atDate, Integer limitToMaxItems, String[] sourceSystems, Boolean groupByUID) throws Throwable {
		long end, start = System.currentTimeMillis();
		
		this._log.info("Autocompleting fishing license for '" + fishingLicense + "' / " + CollectionsHelper.serializeArray(issuingCountries) + " (procedure name: " + procedureName + ", sources: " + CollectionsHelper.serializeArray(sourceSystems) + ", at date: " + atDate + ", group by UID: " + groupByUID + ")");

		Connection c = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		
		try {			
			String CALL_TEMPLATE = "CALL " + ( procedureName == null ? this.getProcedureName(false) : procedureName ) + "(?, ?, ?, ?, ?)";
			
			c = this.sqlMapClient.getDataSource().getConnection();
			
			cs = c.prepareCall(CALL_TEMPLATE);
			
			int counter = 1;
			
			this.setUnicodeString(cs, counter++, fishingLicense);
			
			if(issuingCountries != null)
				cs.setString(counter++, "@" + CollectionsHelper.join(issuingCountries, "@") + "@");
			else
				cs.setNull(counter++, Types.VARCHAR);
			
			if(sourceSystems != null)
				cs.setString(counter++, "@" + CollectionsHelper.join(sourceSystems, "@") + "@");
			else 
				cs.setNull(counter++, Types.VARCHAR);
						
			if(atDate != null)
				cs.setDate(counter++, new java.sql.Date(atDate.getTime()));
			else
				cs.setNull(counter++, Types.DATE);

			cs.setBoolean(counter++, groupByUID);	
				
			List<FishingLicenseSearchResult> vesselFishingLicenseData = new ArrayList<FishingLicenseSearchResult>();
			
			if(cs.execute()) {
				rs = cs.getResultSet();
								
				FishingLicenseSearchResult current = null;
				
				while(rs.next()) {
					current = new FishingLicenseSearchResult();
					
					current.setLicenseId(rs.getString("LICENSE_ID"));
					current.setOccurrencies(rs.getInt("OCCURRENCIES"));
					current.setTotalOccurrencies(rs.getInt("TOTAL_OCCURRENCIES"));
					current.setGroupedOccurrencies(rs.getInt("GROUPED_OCCURRENCIES"));
					
					vesselFishingLicenseData.add(current);
				}
			}
			
			return this.trim(vesselFishingLicenseData, limitToMaxItems);
		} catch (Throwable t) {
			this._log.error("Unable to find vessel registration number", t);
			
			return new ArrayList<FishingLicenseSearchResult>();
		} finally {
			this.cleanup(rs, cs, c);
			
			end = System.currentTimeMillis();
			
			this._log.info("Autocompleting fishing license for '" + fishingLicense + "' / " + CollectionsHelper.serializeArray(issuingCountries) + " took " + ( end - start ) + " mSec.");
		}
	}
}