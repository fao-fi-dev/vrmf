/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Feb 2013
 */
public interface VesselSearchDAO<V extends VesselRecord, F extends VesselRecordSearchFilter> extends CommonVesselRecordSearchDAO<V, F> {
}