/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.Collection;
import java.util.Date;

import org.fao.fi.vrmf.common.models.search.autocompletion.typed.FuzzyPortSearchResult;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Jun 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Jun 2012
 */
public interface RegistrationPortAutocompletionDAO {
	Collection<FuzzyPortSearchResult> searchRegistrationPorts(Integer[] countryIDs, String portName, Date atDate, Integer limitToMaxItems, String[] availableSources, String[] sourceSystems, String[] publicSourceSystems, Boolean groupByUID, boolean groupMappedTypes) throws Throwable;
	Collection<FuzzyPortSearchResult> searchRegistrationPorts(String procedureName, Integer[] countryIDs, String portName, Date atDate, Integer limitToMaxItems, String[] availableSources, String[] sourceSystems, String[] publicSourceSystems, Boolean groupByUID, boolean groupMappedTypes) throws Throwable;
}
