/**
 * (c) 2016 FAO / UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.helpers;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 21, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 21, 2016
 */
public final class VesselsTableHelper {
	static public String DEFAULT_VESSELS_TABLE = "vessels";
	static public String[] ALL_DATA_SOURCES = null;
	
	private VesselsTableHelper() { super(); }
	
	static public String vesselsTable(String table) {
		return table == null ? DEFAULT_VESSELS_TABLE : table;
	}
}
