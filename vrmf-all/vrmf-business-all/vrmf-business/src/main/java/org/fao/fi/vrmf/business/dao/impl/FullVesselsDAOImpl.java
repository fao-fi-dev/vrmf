/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.impl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import org.fao.fi.vrmf.business.dao.DataDecoratorDAO;
import org.fao.fi.vrmf.business.dao.FullVesselsDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesI18nDAO;
import org.fao.fi.vrmf.business.dao.generated.SGearTypesI18nDAO;
import org.fao.fi.vrmf.business.dao.generated.SVesselTypesI18nDAO;
import org.fao.fi.vrmf.common.models.extended.FullVessel;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToFlags;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToGear;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToType;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SCountriesI18n;
import org.fao.fi.vrmf.common.models.generated.SGearTypes;
import org.fao.fi.vrmf.common.models.generated.SGearTypesI18n;
import org.fao.fi.vrmf.common.models.generated.SVesselTypes;
import org.fao.fi.vrmf.common.models.generated.SVesselTypesI18n;
import org.springframework.cache.annotation.Cacheable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Dec 2010
 */
@NoArgsConstructor
@Singleton @Named("dao.vessels.full")
public class FullVesselsDAOImpl extends ExtendedVesselsDAOImpl implements FullVesselsDAO {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7296879120379124048L;
	
	@Inject private @Getter @Setter DataDecoratorDAO decorator;

	@Inject private SCountriesI18nDAO _countriesI18nDAO;
	@Inject private SGearTypesI18nDAO _gearTypesI18nDAO;
	@Inject private SVesselTypesI18nDAO _vesselTypesI18nDAO;
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.FullVesselsDAO#selectFullByPrimaryKey(java.lang.Integer)
	 */
	@Override
	@Cacheable(value="vrmf.business.data.vessels.full", key="#root.method.name + #vesselID")
	public FullVessel selectFullByPrimaryKey(Integer vesselID) throws Throwable {
		return this.selectFullByPrimaryKeyAndDataSources(vesselID, null);
	}
 
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.FullVesselsDAO#selectFullByPrimaryKeyAndDataSources(java.lang.Integer, java.lang.String[])
	 */
	@Override
	@Cacheable(value="vrmf.business.data.vessels.full", key="#root.method.name + #vesselID + T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#dataSources)")
	public FullVessel selectFullByPrimaryKeyAndDataSources(Integer vesselID, String[] dataSources) throws Throwable {
		return this.selectFullByPrimaryKeyAndDataSources(new Integer[] { vesselID }, dataSources);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.FullVesselsDAO#selectFullByPrimaryKey(java.lang.Integer[])
	 */
	@Override
	@Cacheable(value="vrmf.business.data.vessels.full", key="#root.method.name + T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#vesselIDs)")
	public FullVessel selectFullByPrimaryKey(Integer[] vesselIDs) throws Throwable {
		return this.selectFullByPrimaryKeyAndDataSources(vesselIDs, null);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.FullVesselsDAO#selectFullByPrimaryKeyAndDataSources(java.lang.Integer[], java.lang.String[])
	 */
	@Override
	@Cacheable(value="vrmf.business.data.vessels.full", key="#root.method.name + " +
															"T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#vesselIDs) + " +
															"T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#dataSources)")
	public FullVessel selectFullByPrimaryKeyAndDataSources(Integer[] vesselIDs, String[] dataSources) throws Throwable {
		FullVessel result, full;
		ExtendedVessel extended = null;

		result = null;
		full = null;
		for(Integer ID : vesselIDs) {
			extended = this.selectExtendedByPrimaryKey(ID, dataSources);

			if(extended != null) {
				full = new FullVessel(extended, false);

				full.setLengths(decorator.buildVesselLengths(extended.getLengthData()));
				full.setTonnages(decorator.buildVesselTonnages(extended.getTonnageData()));
				full.setPowers(decorator.buildVesselPowers(extended.getPowerData()));
				full.setFlags(decorator.buildVesselFlags(extended.getFlagData()));
				full.setStatus(decorator.buildVesselStatus(extended.getStatusData()));
				full.setTypes(decorator.buildVesselTypes(extended.getTypeData()));
				full.setGears(decorator.buildVesselGears(extended.getGearData()));
				full.setCallsigns(decorator.buildCallsigns(extended.getCallsignData()));
				full.setFishingLicenses(decorator.buildFishingLicenses(extended.getFishingLicenseData()));
				full.setRegistrations(decorator.buildRegistrations(extended.getRegistrationData()));
				full.setShipbuilders(decorator.buildShipbuilders(extended.getShipbuildersData()));
				full.setOwners(decorator.buildOwners(extended.getOwnersData()));
				full.setOperators(decorator.buildOperators(extended.getOperatorsData()));
				full.setAuthorizations(decorator.buildAuthorizations(extended.getAuthorizationsData()));

				full.setHull(decorator.buildHullMaterials(extended.getHullMaterialData()));
				full.setBeneficialOwners(decorator.buildBeneficialOwners(extended.getBeneficialOwnersData()));
				full.setMasters(decorator.buildMasters(extended.getMastersData()));
				full.setFishingMasters(decorator.buildFishingMasters(extended.getFishingMastersData()));

				full.setInspections(decorator.buildInspections(extended.getInspectionsData()));
				full.setNonCompliance(decorator.buildNonCompliances(extended.getNonComplianceData()));
				full.setIuuLists(decorator.buildIuuLists(extended.getIuuListsData()));
				full.setPortEntryDenials(decorator.buildPortEntryDenials(extended.getPortEntryDenialsData()));
				
				if(result == null)
					result = full;
				else
					result = result.join(full);
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.FullVesselsDAO#selectFullByUID(java.lang.Integer, java.lang.String)
	 */
	@Override
	@Cacheable(value="vrmf.business.data.vessels.full", key="#root.method.name + #vesselUID + #vesselsTable")
	public FullVessel selectFullByUID(Integer vesselUID, String vesselsTable) throws Throwable {
		return this.selectFullByUIDAndDataSources(vesselUID, null, vesselsTable);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.FullVesselsDAO#selectFullByUIDAndDataSources(java.lang.Integer, java.lang.String[], java.lang.String)
	 */
	@Override
	@Cacheable(value="vrmf.business.data.vessels.full", key="#root.method.name + " +
															"#vesselUID + " +
															"T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#dataSources) + " +
															"#vesselsTable")
	public FullVessel selectFullByUIDAndDataSources(Integer vesselUID, String[] dataSources, String vesselsTable) throws Throwable {
		this._log.debug("Selecting full vessel data for vessel with UID #" + vesselUID + " " + 
					   ( dataSources != null && dataSources.length > 0 ? " limited to " + Arrays.asList(dataSources) : "" ) + " " + 
					    "from table " + vesselsTable);


		return this.buildFromExtended(this.selectExtendedByUID(vesselUID, dataSources, vesselsTable));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.FullVesselsDAO#selectFullByUIDAndAuthorization(java.lang.Integer, java.lang.String, java.lang.String, java.lang.Boolean)
	 */
	@Override
	@Cacheable(value="vrmf.business.data.vessels.full", key="#root.method.name + " +
														    "#vesselUID + " +
															"#authorizationIssuerID + " +
															"#authorizationTypeID + " +
															"#excludeTerminatedVessels + " +
															"#vesselsTable")
	public FullVessel selectFullByUIDAndAuthorization(Integer vesselUID, String authorizationIssuerID, String authorizationTypeID, Boolean excludeTerminatedVessels, String vesselsTable) throws Throwable {
		return this.selectFullByUIDAuthorizationAndDataSources(vesselUID, null, authorizationIssuerID, authorizationTypeID, excludeTerminatedVessels, vesselsTable);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.business.dao.FullVesselsDAO#selectFullByUIDAuthorizationAndDataSources(java.lang.Integer, java.lang.String[], java.lang.String, java.lang.String, java.lang.Boolean)
	 */
	@Override
	@Cacheable(value="vrmf.business.data.vessels.full", key="#root.method.name + " +
														    "#vesselUID + " +
														    "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#dataSources) + " +
															"#authorizationIssuerID + " +
															"#authorizationTypeID + " +
															"#excludeTerminatedVessels + " +
															"#vesselsTable")
	public FullVessel selectFullByUIDAuthorizationAndDataSources(Integer vesselUID, String[] dataSources, String authorizationIssuerID, String authorizationTypeID, Boolean excludeTerminatedVessels, String vesselsTable) throws Throwable {
		List<Integer> IDs = this.metadataDAO.getAuthorizedIDsByUID(vesselUID, authorizationIssuerID, authorizationTypeID, excludeTerminatedVessels, vesselsTable);

		if(IDs == null || IDs.isEmpty())
			return null;

		return this.selectFullByPrimaryKeyAndDataSources(IDs.toArray(new Integer[IDs.size()]), dataSources);
	}

	public FullVessel buildFromExtended(ExtendedVessel extended) throws Throwable {
		return this.buildFromExtended(extended, false);
	}

	public FullVessel buildFromExtended(ExtendedVessel extended, boolean preserveOrder) throws Throwable {
		if(extended == null)
			return null;

		FullVessel full = new FullVessel(extended, preserveOrder);

		full.setFlags(decorator.buildVesselFlags(extended.getFlagData()), preserveOrder);
		full.setCallsigns(decorator.buildCallsigns(extended.getCallsignData()), preserveOrder);
		full.setFishingLicenses(decorator.buildFishingLicenses(extended.getFishingLicenseData()), preserveOrder);
		full.setLengths(decorator.buildVesselLengths(extended.getLengthData()), preserveOrder);
		full.setTonnages(decorator.buildVesselTonnages(extended.getTonnageData()), preserveOrder);
		full.setPowers(decorator.buildVesselPowers(extended.getPowerData()), preserveOrder);
		full.setStatus(decorator.buildVesselStatus(extended.getStatusData()), preserveOrder);
		full.setTypes(decorator.buildVesselTypes(extended.getTypeData()), preserveOrder);
		full.setGears(decorator.buildVesselGears(extended.getGearData()), preserveOrder);
		full.setRegistrations(decorator.buildRegistrations(extended.getRegistrationData()), preserveOrder);
		full.setShipbuilders(decorator.buildShipbuilders(extended.getShipbuildersData()), preserveOrder);
		full.setOwners(decorator.buildOwners(extended.getOwnersData()), preserveOrder);
		full.setOperators(decorator.buildOperators(extended.getOperatorsData()), preserveOrder);
		full.setAuthorizations(decorator.buildAuthorizations(extended.getAuthorizationsData()), preserveOrder);

		full.setHull(decorator.buildHullMaterials(extended.getHullMaterialData()), preserveOrder);
		full.setBeneficialOwners(decorator.buildBeneficialOwners(extended.getBeneficialOwnersData()), preserveOrder);
		full.setMasters(decorator.buildMasters(extended.getMastersData()), preserveOrder);
		full.setFishingMasters(decorator.buildFishingMasters(extended.getFishingMastersData()), preserveOrder);

		full.setIuuLists(decorator.buildIuuLists(extended.getIuuListsData()), preserveOrder);
		full.setInspections(decorator.buildInspections(extended.getInspectionsData()), preserveOrder);
		full.setNonCompliance(decorator.buildNonCompliances(extended.getNonComplianceData()), preserveOrder);
		
		return full;
	}
	
	public FullVessel localize(FullVessel data, String lang) throws Exception {
		if(data == null)
			return null;
		
		FullVessel cloned = SerializationHelper.clone(data);
		
		if(lang != null && cloned.getFlags() != null)
			for(ExtendedVesselToFlags in : cloned.getFlags())
				if(in != null)
					in.setVesselFlag(localize(in.getVesselFlag(), lang));
		
		if(lang != null && cloned.getGears() != null)
			for(ExtendedVesselToGear in : cloned.getGears())
				if(in != null)
					in.setGearType(localize(in.getGearType(), lang));
		
		if(lang != null && cloned.getTypes() != null)
			for(ExtendedVesselToType in : cloned.getTypes())
				if(in != null)
					in.setVesselType(localize(in.getVesselType(), lang));
		
		return cloned;	
	}

	
	private SCountries localize(SCountries data, String lang) throws Exception {
		if(data == null)
			return null;
				
		SCountriesI18n localized = _countriesI18nDAO.selectByPrimaryKey(data.getId(), lang);
		
		SCountries cloned = SerializationHelper.clone(data);
		
		if(localized != null) {
			cloned.setName(ObjectsHelper.coalesce(localized.getName(), data.getName()));
			cloned.setSimplifiedName(ObjectsHelper.coalesce(localized.getSimplifiedName(), data.getSimplifiedName()));
			cloned.setSimplifiedNameSoundex(ObjectsHelper.coalesce(localized.getSimplifiedNameSoundex(), data.getSimplifiedNameSoundex()));
			cloned.setOfficialName(ObjectsHelper.coalesce(localized.getOfficialName(), data.getOfficialName()));
			cloned.setSimplifiedOfficialName(ObjectsHelper.coalesce(localized.getSimplifiedOfficialName(), data.getSimplifiedOfficialName()));
			cloned.setSimplifiedOfficialNameSoundex(ObjectsHelper.coalesce(localized.getSimplifiedOfficialNameSoundex(), data.getSimplifiedOfficialNameSoundex()));
		}
		
		return cloned;
	}
	
	private SGearTypes localize(SGearTypes data, String lang) throws Exception {
		if(data == null)
			return null;
				
		SGearTypesI18n localized = _gearTypesI18nDAO.selectByPrimaryKey(data.getId(), lang);
		
		SGearTypes cloned = SerializationHelper.clone(data);
		
		if(localized != null) {
			cloned.setName(ObjectsHelper.coalesce(localized.getName(), data.getName()));
			cloned.setDescription(ObjectsHelper.coalesce(localized.getDescription(), data.getDescription()));
		}
		
		return cloned;
	}
	
	private SVesselTypes localize(SVesselTypes data, String lang) throws Exception {
		if(data == null)
			return null;
				
		SVesselTypesI18n localized = _vesselTypesI18nDAO.selectByPrimaryKey(data.getId(), lang);
		
		SVesselTypes cloned = SerializationHelper.clone(data);

		if(localized != null) {
			cloned.setName(ObjectsHelper.coalesce(localized.getName(), data.getName()));
			cloned.setDescription(ObjectsHelper.coalesce(localized.getDescription(), data.getDescription()));
		}
		
		return cloned;
	}
}