/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.helpers.vessel.gears;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.topology.WeightedGraph;
import org.fao.fi.sh.utility.topology.helpers.TransitiveMappedDataManager;
import org.fao.fi.vrmf.business.dao.generated.SGearTypesDAO;
import org.fao.fi.vrmf.common.models.generated.SGearTypes;
import org.fao.fi.vrmf.common.models.generated.SGearTypesExample;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 May 2012
 */
@NoArgsConstructor
@Named @Singleton
public class GearTypesMappingManager extends TransitiveMappedDataManager<Integer, SGearTypes> {
	@Inject private @Getter @Setter SGearTypesDAO gearTypesDAO;
	
	/**
	 * Class constructor
	 *
	 * @param data
	 */
	public GearTypesMappingManager(Collection<SGearTypes> data) {
		super(data);
	}

	/**
	 * Class constructor
	 *
	 * @param graph
	 */
	public GearTypesMappingManager(WeightedGraph<Integer> graph) {
		super(graph);
	}

	@PostConstruct
	public void afterPropertiesSet() throws Exception {
		SGearTypesExample filter = new SGearTypesExample();
		
		this._log.info("Initializing gear types mapping manager...");
		
		try {
			for(SGearTypes type : this.gearTypesDAO.selectByExample(filter)) {
				this.addMappedData(type);
			}
			
			this._log.info("Checking cycles...");

			if(this.getGraph().hasCycles(this._detector))
				throw new RuntimeException("Cycles detected in mapped gear types data: " + this.getGraph().getCycles(this._detector));
			
			this._log.info("No cycles detected");
			
			this._log.info("Gear types mapping manager has been initialized");
		} catch (Throwable t) {
			throw new RuntimeException("Unable to initialize the gear types mapping manager [" + t.getClass().getName() + ": " + t.getMessage() + " ]", t);
		}
	}
}