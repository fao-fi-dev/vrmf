/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao;

import java.util.Collection;
import java.util.List;

import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsExample;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Oct 2010
 */
public interface ExtendedVesselsDAO extends SimpleVesselsDAO {
	ExtendedVessel selectExtendedByPrimaryKey(Integer vesselID) throws Throwable;
	ExtendedVessel selectExtendedByPrimaryKey(Integer vesselID, String[] dataSourcesFilter) throws Throwable;
	ExtendedVessel selectExtendedByPrimaryKey(Integer vesselID, Collection<Class<?>> dataTypeSet) throws Throwable;
	ExtendedVessel selectExtendedByPrimaryKey(Integer vesselID, String[] dataSourcesFilter, Collection<Class<?>> dataTypeSet) throws Throwable;
	
	List<ExtendedVessel> selectExtendedByDataSource(String[] dataSources) throws Throwable;
	List<ExtendedVessel> selectExtendedByDataSource(String[] dataSources, Collection<Class<?>> dataTypeSet) throws Throwable;
	
	List<ExtendedVessel> selectExtendedByExample(VesselsExample filter) throws Throwable;
	List<ExtendedVessel> selectExtendedByExample(VesselsExample filter, String[] dataSourcesFilter) throws Throwable;
	List<ExtendedVessel> selectExtendedByExample(VesselsExample filter, Collection<Class<?>> dataTypeSet) throws Throwable;
	List<ExtendedVessel> selectExtendedByExample(VesselsExample filter, String[] dataSourcesFilter, Collection<Class<?>> dataTypeSet) throws Throwable;
	
	List<Integer> selectMultipleSourcedVesselIDs(String[] originalSources, String[] additionalSources) throws Throwable;
	
	List<Integer> selectVesselIDs(String[] sources) throws Throwable;
	List<Integer> selectVesselsWithEUCFRIDs(String[] sources) throws Throwable;
	List<Integer> selectVesselsWithIMOIDs(String[] sources) throws Throwable;
	List<Integer> selectVesselsWithIMOOrEUCFRIDs(String[] sources) throws Throwable;
	List<Integer> selectVesselsWithIMOAndEUCFRIDs(String[] sources) throws Throwable;
}