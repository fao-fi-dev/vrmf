/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.extensions.maps.lazy;

import java.util.HashSet;
import java.util.Set;

import org.fao.fi.vrmf.business.core.utilities.extensions.maps.impl.LazySerializableDataAccessMap;
import org.fao.fi.vrmf.business.dao.generated.SIdentifierTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SIdentifierTypesDAOImpl;
import org.fao.fi.vrmf.common.models.generated.SIdentifierTypes;
import org.fao.fi.vrmf.common.models.generated.SIdentifierTypesExample;

import com.ibatis.sqlmap.client.SqlMapClientBuilder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
public class LazyVesselIdentifierTypesMap extends LazySerializableDataAccessMap<String, SIdentifierTypes> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3381477689878259979L;
	
	transient private SIdentifierTypesDAO _dao;
		
	/**
	 * Class constructor
	 *
	 * @param sqlMapClientConfigurationResource
	 * @throws Throwable
	 */
	public LazyVesselIdentifierTypesMap(String sqlMapClientConfigurationResource) throws Throwable {
		super(sqlMapClientConfigurationResource);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazySerializableDataAccessMap#initializeDAO()
	 */
	@Override
	protected void initializeDAO() throws Throwable {
		this._dao = new SIdentifierTypesDAOImpl();
		this._dao.setSqlMapClient(SqlMapClientBuilder.buildSqlMapClient(Thread.currentThread().getContextClassLoader().getResourceAsStream(this._sqlMapClientConfigurationResource)));
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetElement(java.lang.Object)
	 */
	protected SIdentifierTypes doGetElement(String key) throws Throwable {
		return this._dao.selectByPrimaryKey(key);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetKeySet(java.lang.Object)
	 */
	@Override
	protected Set<String> doGetKeySet() throws Throwable {		
		Set<String> keySet = new HashSet<String>();
		
		for(SIdentifierTypes identifierType : this._dao.selectByExample(new SIdentifierTypesExample())) {
			keySet.add(identifierType.getId());
		}
		
		return keySet;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.extensions.maps.impl.LazyMap#doGetSize()
	 */
	@Override
	protected int doGetSize() throws Throwable {
		return this._dao.countByExample(new SIdentifierTypesExample());
	}
}