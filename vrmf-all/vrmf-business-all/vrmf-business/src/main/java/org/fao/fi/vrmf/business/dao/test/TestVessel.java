/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.business.dao.test;

import java.util.Collection;

import org.fao.fi.vrmf.common.core.model.GenericData;
import org.fao.fi.vrmf.common.models.extended.ExtendedAuthorizations;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToType;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Jul 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Jul 2011
 */
public class TestVessel extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3473435384450175835L;

	private Integer _id;
	private Integer _uid;
	private String _sourceSystem;
	private Collection<VesselsToFlags> _flags;
	private Collection<VesselsToName> _names;
	private Collection<ExtendedVesselToType> _types;
	private Collection<VesselsToIdentifiers> _identifiers;
	private Collection<ExtendedAuthorizations> _auths;
	
	/**
	 * Class constructor
	 *
	 */
	public TestVessel() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the 'iD' value
	 */
	public Integer getId() {
		return this._id;
	}
	/**
	 * @param iD the 'iD' value to set
	 */
	public void setId(Integer iD) {
		this._id = iD;
	}
	/**
	 * @return the 'uID' value
	 */
	public Integer getUid() {
		return this._uid;
	}
	/**
	 * @param uID the 'uID' value to set
	 */
	public void setUid(Integer uID) {
		this._uid = uID;
	}
	/**
	 * @return the 'sourceSystem' value
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}
	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}
	/**
	 * @return the 'flags' value
	 */
	public Collection<VesselsToFlags> getFlags() {
		return this._flags;
	}
	/**
	 * @param flags the 'flags' value to set
	 */
	public void setFlags(Collection<VesselsToFlags> flags) {
		this._flags = flags;
	}
	/**
	 * @return the 'names' value
	 */
	public Collection<VesselsToName> getNames() {
		return this._names;
	}
	/**
	 * @param names the 'names' value to set
	 */
	public void setNames(Collection<VesselsToName> names) {
		this._names = names;
	}
	/**
	 * @return the 'auths' value
	 */
	public Collection<ExtendedAuthorizations> getAuths() {
		return this._auths;
	}
	/**
	 * @param auths the 'auths' value to set
	 */
	public void setAuths(Collection<ExtendedAuthorizations> auths) {
		this._auths = auths;
	}
	/**
	 * @return the 'identifiers' value
	 */
	public Collection<VesselsToIdentifiers> getIdentifiers() {
		return this._identifiers;
	}
	/**
	 * @param identifiers the 'identifiers' value to set
	 */
	public void setIdentifiers(Collection<VesselsToIdentifiers> identifiers) {
		this._identifiers = identifiers;
	}
	/**
	 * @return the 'types' value
	 */
	public Collection<ExtendedVesselToType> getTypes() {
		return this._types;
	}
	/**
	 * @param types the 'types' value to set
	 */
	public void setTypes(Collection<ExtendedVesselToType> types) {
		this._types = types;
	}
}
