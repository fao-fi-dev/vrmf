/**
 * (c) 2013 FAO / UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.test.business.search;

import static org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns.ID;
import static org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns.SOURCE;
import static org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns.UID;
import static org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns.UPD_DATE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.FLAG;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.GEAR_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.GEAR_TYPE_ID;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.GT;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.IMO;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.LENGTH_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.LENGTH_VALUE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.LOA;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.NAME;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.TONNAGE_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.TONNAGE_VALUE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.VESSEL_TYPE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Inject;

import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import org.fao.fi.vrmf.business.dao.FullVesselsDAO;
import org.fao.fi.vrmf.business.dao.VesselRecordSearchDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesDAO;
import org.fao.fi.vrmf.common.models.extended.FullVessel;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SCountriesExample;
import org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonClauses;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.fao.fi.vrmf.common.search.dsl.support.services.response.vessels.VesselRecordSearchResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Mar 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
	"/test/config/spring/vrmf-business-test-spring-datasource.xml",
	"/test/config/spring/vrmf-business-test-spring-context-search.xml"
})
public class TestVesselRecordSearch {
	static final private String DEFAULT_VESSELS_TABLE = "VESSELS";
	
	private Logger _log = LoggerFactory.getLogger(this.getClass());
	
	@Inject private VesselRecordSearchDAO _vesselRecordSearchDao;
	@Inject private FullVesselsDAO _fullVesselsDao;
	@Inject private SCountriesDAO _countriesDao;
	
	private void display(VesselRecordSearchResponse<VesselRecord> results, boolean verbose) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		if(results.getDataSet() != null)
			for(VesselRecord record : results.getDataSet()) {
				if(!verbose)
					this._log.info(
							record.getId() + " " +
							record.getUid() + " " +
							record.getName() + " " +
							record.getVesselType() + " " +
							"[ " + record.getVesselTypeID() + " ] " +
							"LOA: " + record.getLOA() + " [ " +
							record.getCurrentLengthType() + ": " +
							record.getCurrentLengthValue() + " ] " +
							"GT: " + record.getGT() + " [ " +
							record.getCurrentTonnageType() + ": " +
							record.getCurrentTonnageValue() + " ] " +
						  ( record.getUpdateDate() == null ? "<NOT SET>" : formatter.format(record.getUpdateDate()) ) + " " +
							record.getSourceSystem() +
						  ( record.getNumberOfSources() > 1 ? "+" + ( record.getNumberOfSources() - 1 ) : "" ) + " " +
							CollectionsHelper.serializeCollection(record.getSourceSystems()) + " ");
				else
					this._log.info(ObjectsHelper.asString(record));
			}
	}

	private Date date(int year, int month, int day) {
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.DAY_OF_MONTH, day);
		
		return cal.getTime();
	}
		
	private VesselRecordSearchFilter getBasicFilter() {
		VesselRecordSearchFilter filter = new VesselRecordSearchFilter();
		filter.
			SELECT(
				filter.columns().
				include(ID,
						UID)).
			FROM(DEFAULT_VESSELS_TABLE).
			GROUP_DUPLICATES().
			LIMIT(30).
			OFFSET(0).
			AT(new Date());

		return filter;
	}

	private VesselRecordSearchResponse<VesselRecord> testFilter(VesselRecordSearchFilter filter) throws Throwable  {
		long end, start = System.currentTimeMillis();

		this._log.info("Searching vessels with filter:\n{}", filter.toXML());

		VesselRecordSearchResponse<VesselRecord> vessels = _vesselRecordSearchDao.searchVessels(filter);

		this.display(vessels, false);

		end = System.currentTimeMillis();

		this._log.info("OK, search took {} mSec. to warm up!", String.valueOf(end - start));

		List<Integer> UIDs = new ArrayList<Integer>();
		
		if(vessels != null && vessels.getDataSet() != null) {
			for(VesselRecord in : vessels.getDataSet())
				UIDs.add(in.getUid());

			System.out.println("UIDS: " + UIDs);
		}
		
		return vessels;
	}
	
	@Test
	public void testFilterByIMO() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.IMO, "5084180")
		).ORDER_BY(
			filter.sortableColumns().
				descending(NAME)
		).
		current().
		GROUP_DUPLICATES().
		LIMIT(10).
		OFFSET(0);

		long end, start = System.currentTimeMillis();
		VesselRecordSearchResponse<VesselRecord> response = null;
		
		response = this.testFilter(filter);
		
		end = System.currentTimeMillis();
		
		System.out.println("Elapsed: " + ( end - start ));
		System.out.println("Mean exec. time: " + ( end - start ) * 0.1 + " mSec.");
		Assert.assertNotNull(response.getDataSet());
		Assert.assertEquals(2, response.getDataSet().size());
	}
	
	@Test
	public void testFilterByUID() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.WHERE(filter.clauses().
			and(CommonClauses.VRMF_UIDS, 1, 2, 3 )
		);

		this.testFilter(filter);
	}

	@Test
	public void testFilterByFlag() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(NAME)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.FLAG, 110 )
		).ORDER_BY(
			filter.sortableColumns().
				descending(NAME)
		);

		this.testFilter(filter);
	}
	
	@Test
	public void testFilterByHullMaterial() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.HULL_MATERIAL, "FP" )
		).ORDER_BY(
			filter.sortableColumns().
				descending(NAME)
		).
		current().
		GROUP_DUPLICATES().
		LIMIT(10).
		OFFSET(0);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Set<Integer> expected = new TreeSet<Integer>();
		expected.add(30089902);
		expected.add(30091810);
		expected.add(30096737);
		expected.add(30097500);
		expected.add(30099674);
		expected.add(30107252);
		expected.add(30109140);
		expected.add(30114371);
		expected.add(30181220);
		expected.add(30183015);
		
		Set<Integer> found = new TreeSet<Integer>();

		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getDataSet());
		Assert.assertTrue(response.getDataSet().size() == 10);
		
		for(VesselRecord in : response.getDataSet())
			found.add(in.getUid());
		
		Assert.assertArrayEquals(expected.toArray(new Integer[expected.size()]), found.toArray(new Integer[found.size()]));
	}
	
	@Test
	public void testFilterByIdentifier() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.IDENTIFIER_TYPE, "EU_CFR").
				and(VesselRecordClauses.IDENTIFIERS, "DEU101000402" )
		).ORDER_BY(
			filter.sortableColumns().
				ascending(UID)
		).
		AT(this.date(1991, 1, 1)).
		GROUP_DUPLICATES().
		LIMIT(10).
		OFFSET(0);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getDataSet());
		Assert.assertTrue(response.getDataSet().size() == 1);
		
		Assert.assertEquals((Integer)30006944, (Integer)response.getDataSet().get(0).getUid());
		
		filter.AT(this.date(2013, 1, 1));
		
		response = this.testFilter(filter);
		
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getDataSet());
		Assert.assertTrue(response.getDataSet().size() == 2);
		
		Assert.assertEquals((Integer)14000257, (Integer)response.getDataSet().get(0).getUid());
		Assert.assertEquals((Integer)30006944, (Integer)response.getDataSet().get(1).getUid());
	}
	
	@Test
	public void testFilterByNonCompliance() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.NON_COMPLIANCE_INFRINGEMENT_TYPES, "REGE" ).
				and(VesselRecordClauses.NON_COMPLIANCE_ISSUERS, "ICCAT" ).
				and(VesselRecordClauses.NON_COMPLIANCE_DATE_RANGE_FROM, this.date(2013, 1, 1))
		).ORDER_BY(
			filter.sortableColumns().
				descending(NAME)
		).
		SOURCED_BY(new String[] { "EU", "GR" }).
		current().
		GROUP_DUPLICATES().
		LIMIT(10).
		OFFSET(0);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Assert.assertNotNull(response.getDataSet());
		Assert.assertEquals(1, response.getDataSet().size());
		
		Assert.assertEquals((Integer)30086960, response.getDataSet().get(0).getUid());
		
		FullVessel identified = _fullVesselsDao.selectFullByUID(response.getDataSet().get(0).getUid(), DEFAULT_VESSELS_TABLE);
		
		System.out.println(JAXBHelper.toXML(identified)); //identified.toXML(0));
	}
	
	@Test
	public void testFilterByNonComplianceInvalid() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.NON_COMPLIANCE_ISSUERS, "EU" ).
				and(VesselRecordClauses.NON_COMPLIANCE_DATE_RANGE_FROM, this.date(2013, 1, 1))
		).ORDER_BY(
			filter.sortableColumns().
				descending(NAME)
		).
		SOURCED_BY(new String[] { "EU", "GR" }).
		current().
		GROUP_DUPLICATES().
		LIMIT(10).
		OFFSET(0);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Assert.assertNull(response.getDataSet());
	}
	
	@Test
	public void testFilterByHullMaterialSourceAndRegistrationCountry() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.HULL_MATERIAL, "FP" ).
				and(VesselRecordClauses.REGISTRATION_COUNTRIES, 84 )
		).ORDER_BY(
			filter.sortableColumns().
				ascending(UID)
		).
		SOURCED_BY("EU").
		current().
		GROUP_DUPLICATES().
		LIMIT(10).
		OFFSET(0);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Set<Integer> expected = new TreeSet<Integer>();
		expected.add(30086974);
		expected.add(30086987);
		expected.add(30086994);
		expected.add(30086995);
		expected.add(30086996);
		expected.add(30086997);
		expected.add(30087026);
		expected.add(30087027);
		expected.add(30087038);
		expected.add(30087055);
		
		Set<Integer> found = new TreeSet<Integer>();

		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getDataSet());
		Assert.assertTrue(response.getDataSet().size() == 10);
		
		for(VesselRecord in : response.getDataSet())
			found.add(in.getUid());
		
		Assert.assertArrayEquals(expected.toArray(new Integer[expected.size()]), found.toArray(new Integer[found.size()]));
	}
	
	@Test
	public void testFilterByRegistrationPeriod() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.REGISTRATION_COUNTRIES, 84 ).
				and(VesselRecordClauses.REGISTRATION_NUMBERS, "Λ24567").
				and(VesselRecordClauses.REGISTRATION_COUNTRY_VALID_FROM, this.date(1992, 1, 1))
		).ORDER_BY(
			filter.sortableColumns().
				descending(NAME)
		).
		SOURCED_BY("EU").
		current().
		GROUP_DUPLICATES().
		LIMIT(10).
		OFFSET(0);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Assert.assertNull(response.getDataSet());
		
		filter = this.getBasicFilter();
		
		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.REGISTRATION_COUNTRIES, 84 ).
				and(VesselRecordClauses.REGISTRATION_NUMBERS, "Λ24567").
				and(VesselRecordClauses.REGISTRATION_COUNTRY_VALID_FROM, this.date(1989, 1, 1)).
				and(VesselRecordClauses.REGISTRATION_COUNTRY_VALID_TO, this.date(2015, 1, 1))
		).ORDER_BY(
			filter.sortableColumns().
				descending(NAME)
		).
		SOURCED_BY("EU").
		current().
		GROUP_DUPLICATES().
		LIMIT(10).
		OFFSET(0);
		response = this.testFilter(filter);
		
		Assert.assertNotNull(response.getDataSet());
		Assert.assertTrue(response.getDataSet().size() == 1);
		Assert.assertEquals((Integer)30086960, response.getDataSet().get(0).getUid());
	}
	
	@Test
	public void testFilterByVMSIndicatorTrue() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.HAS_VMS_INDICATOR, Boolean.TRUE)
		).
		GROUP_DUPLICATES().
		current().
		LIMIT(10).
		OFFSET(0);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Set<Integer> expected = new TreeSet<Integer>();
		expected.add(1);
		expected.add(2);
		expected.add(12);
		expected.add(15);
		expected.add(16);
		expected.add(17);
		expected.add(18);
		expected.add(21);
		expected.add(25);
		expected.add(26);
		
		Set<Integer> found = new TreeSet<Integer>();

		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getDataSet());
		Assert.assertTrue(response.getDataSet().size() == 10);
		
		for(VesselRecord in : response.getDataSet())
			found.add(in.getUid());
		
		Assert.assertArrayEquals(expected.toArray(new Integer[expected.size()]), found.toArray(new Integer[found.size()]));
	}
	
	@Test
	public void testFilterByVMSIndicatorFalse() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.HAS_VMS_INDICATOR, Boolean.FALSE)
		).
		GROUP_DUPLICATES().
		current().
		LIMIT(10).
		OFFSET(0);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Set<Integer> expected = new TreeSet<Integer>();
		expected.add(13);
		expected.add(826);
		expected.add(949);
		expected.add(1034);
		expected.add(1430);
		expected.add(1458);
		expected.add(1882);
		expected.add(2058);
		expected.add(8219);
		expected.add(8220);
		
		Set<Integer> found = new TreeSet<Integer>();

		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getDataSet());
		Assert.assertTrue(response.getDataSet().size() == 10);
		
		for(VesselRecord in : response.getDataSet())
			found.add(in.getUid());
		
		Assert.assertArrayEquals(expected.toArray(new Integer[expected.size()]), found.toArray(new Integer[found.size()]));
	}
	
	@Test
	public void testFilterByFlagLengthTypeAndSource() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		AMONG(new String[] { "LLOYD" }).
		SOURCED_BY("LLOYD").
		SELECT(
			filter.columns().
				include(NAME).
				include(SOURCE).
				include(FLAG).
				include(LENGTH_TYPE).
				include(LENGTH_VALUE).
				include(GT)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.FLAG, 110 ).
				and(VesselRecordClauses.LENGTH_TYPES,  "BP", "LBP" )
		).ORDER_BY(
			filter.sortableColumns().
				descending(NAME)
		).
		current().
		linkToSources();

		this.testFilter(filter);
	}
	
	@Test
	public void testFilterByHasInspections() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(NAME).
				include(SOURCE).
				include(FLAG).
				include(LENGTH_TYPE).
				include(LENGTH_VALUE).
				include(GT)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.HAS_INSPECTIONS, true )
		);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Assert.assertTrue(response.isEmpty());
	}
	
	@Test
	public void testFilterByHasNotInspections() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(NAME).
				include(SOURCE).
				include(FLAG).
				include(LENGTH_TYPE).
				include(LENGTH_VALUE).
				include(GT)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.HAS_INSPECTIONS, false)
		);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Assert.assertFalse(response.isEmpty());
	}

	@Test
	public void testFilterByHasPortEntryDenials() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(NAME).
				include(SOURCE).
				include(FLAG).
				include(LENGTH_TYPE).
				include(LENGTH_VALUE).
				include(GT)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.HAS_INSPECTIONS, true )
		);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Assert.assertTrue(response.isEmpty());
	}
	
	@Test
	public void testFilterByHasNotPortEntryDenials() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(NAME).
				include(SOURCE).
				include(FLAG).
				include(LENGTH_TYPE).
				include(LENGTH_VALUE).
				include(GT)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.HAS_PORT_ENTRY_DENIALS, false)
		);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Assert.assertFalse(response.isEmpty());
	}

	@Test
	public void testFilterByHasIUULists() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(NAME).
				include(SOURCE).
				include(FLAG).
				include(LENGTH_TYPE).
				include(LENGTH_VALUE).
				include(GT)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.HAS_IUU_LISTS, true )
		);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Assert.assertTrue(response.isEmpty());
	}
	
	@Test
	public void testFilterByHasNotIUULists() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(NAME).
				include(SOURCE).
				include(FLAG).
				include(LENGTH_TYPE).
				include(LENGTH_VALUE).
				include(GT)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.HAS_IUU_LISTS, false)
		);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Assert.assertFalse(response.isEmpty());
	}
	
	@Test
	public void testFilter() throws Throwable {
		Calendar authorizedAtDate = GregorianCalendar.getInstance();
		authorizedAtDate.set(Calendar.YEAR, 2011);
		authorizedAtDate.set(Calendar.MONTH, Calendar.DECEMBER);
		authorizedAtDate.set(Calendar.DAY_OF_MONTH, 31);

		VesselRecordSearchFilter filter = new VesselRecordSearchFilter();
		
		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
				filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
						//					include(VesselRecordDataColumns.AUTHORIZATION_ISSUER).
						//					include(VesselRecordDataColumns.AUTHORIZATION_START).
						//					include(VesselRecordDataColumns.AUTHORIZATION_END)
				).
				WHERE(
						filter.clauses().
						and(VesselRecordClauses.NO_LENGTH, Boolean.TRUE).
						//					and(VesselRecordClauses.TONNAGE_TYPES, new String[] { "NRT" })
						and(VesselRecordClauses.FLAG, 110, 117 ).
						and(VesselRecordClauses.NAME_LIKE, Boolean.TRUE ).
						and(VesselRecordClauses.NAMES, "K%" ).
						//					and(VesselRecordClauses.FLAG, new Integer[] { 230 } ).
						//					and(VesselRecordClauses.AUTHORIZATION_ISSUERS, new String[] { "ICCAT" }).
						//					and(VesselRecordClauses.AUTHORIZATION_STATUS, AuthorizationStatus.ENFORCED).
						//					and(VesselRecordClauses.AUTHORIZATION_AT_DATE, authorizedAtDate.getTime()).
						nop()
						//					and(VesselRecordClauses.CALLSIGNS_LIKE, Boolean.TRUE ).
						//					and(VesselRecordClauses.CALLSIGNS, new String[] { "WDC%" } )
						).
						/*
			ORDER_BY(
				filter.sortableColumns().
//					descending(CommonDataColumns.UID)
//					descending(VesselRecordDataColumns.FLAG).
//					descending(CommonDataColumns.NUM_SOURCES).
//					ascending(CommonDataColumns.SOURCE).
//					ascending(CommonDataColumns.REF_DATE)
					ascending(VesselRecordDataColumns.NAME)
//					ascending(VesselRecordDataColumns.LENGTH_VALUE).
//					ascending(VesselRecordDataColumns.LENGTH_TYPE)
			).
						 */
						//			GROUP_BY(GroupableDataColumns.UID).
						//			linkToSearch().
						//			linkToSources().
						GROUP_DUPLICATES().
						LIMIT(30).
						//			OFFSET(1000).
						//			current().
						//amongSources(new String[] { "CCSBT", "IATTC", "ICCAT", "IOTC", "WCPFC", "FFA", "SPRFMO", "ISSF", "CTMFM", "SICA", "UNGA61", "CCAMLR", "EU" }).
						retrieveAdditionalMetadata().
						//			duplicatesOnly().
						//			current().
						//			duplicatesOnly().
						//			numberOfSourcesBetween(6, 13).
						//			historical().
						nop()
						;

		long end, start = System.currentTimeMillis();

		this._log.info("Searching vessels with filter:\n{}", filter.toXML());

		this.display(_vesselRecordSearchDao.searchVessels(filter), true);

		end = System.currentTimeMillis();

		this._log.info("OK, search took {} mSec. to warm up!", String.valueOf(end - start));
	}
	
	@Test
	public void testStaticDataRetrieval() throws Throwable {
		for(SCountries country : _countriesDao.selectByExample(new SCountriesExample())) {
			System.out.println(country.getId() + " [ ISO2: " + country.getIso2Code() + " - ISO3: " + country.getIso3Code() + " - Name: " + country.getName() + " ]");
		}
	}
	
	@Test
	public void testGRQuery() throws Throwable {
		VesselRecordSearchFilter filter = new VesselRecordSearchFilter();

		String uvi = "9115664";
		String name = "ANYO MARU";
		String ircs = "JGNK";
		
		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
				filter.columns().
					include(ID,
							UID,
							IMO,
							NAME,
							FLAG)
				).
		WHERE(
				filter.clauses().
					and(VesselRecordClauses.IMO, uvi).
//					and(VesselRecordClauses.IDENTIFIER_TYPE, "IMO").
//					and(VesselRecordClauses.IDENTIFIER_LIKE, Boolean.TRUE).
//					and(VesselRecordClauses.IDENTIFIERS, new String[] { "%" + uvi + "%" }).
					and(VesselRecordClauses.NAME_LIKE, Boolean.TRUE ).
					and(VesselRecordClauses.NAMES, "%" + name + "%" ).
					and(VesselRecordClauses.CALLSIGNS_LIKE, Boolean.TRUE ).
					and(VesselRecordClauses.CALLSIGNS, "%" + ircs + "%" ).
					nop()).
		ORDER_BY(
				filter.sortableColumns().
					ascending(VesselRecordDataColumns.NAME)
				).
		GROUP_DUPLICATES().
		LIMIT(30).
		retrieveAdditionalMetadata().
		nop();
		
		VesselRecordSearchResponse<VesselRecord> response = _vesselRecordSearchDao.searchVessels(filter);
		
		System.out.println(response);
	}

	@Test
	public void testVesselTypeRetrievment() throws Throwable {
		VesselRecordSearchFilter filter = new VesselRecordSearchFilter();
		
		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
				filter.columns().
					include(ID,
							UID,
							IMO,
							NAME,
							FLAG,
							VESSEL_TYPE,
							GEAR_TYPE)
				).
		WHERE(
				filter.clauses().
					and(VesselRecordClauses.NAME_LIKE, Boolean.TRUE ).
					and(VesselRecordClauses.NAMES, "%" + "da grelo" + "%" ).
					nop()).
		ORDER_BY(
				filter.sortableColumns().
					ascending(VesselRecordDataColumns.NAME)
				).
		GROUP_DUPLICATES().
		LIMIT(30).
		retrieveAdditionalMetadata().
		nop();
		
		this._log.info("Searching vessels with filter:\n{}", filter.toXML());
		
		VesselRecordSearchResponse<VesselRecord> response = _vesselRecordSearchDao.searchVessels(filter);
		
		boolean found = false;
		if(response.getDataSet() != null)
			for(VesselRecord in : response.getDataSet()) {
				if(in.getUid().equals(new Integer(21))) {
					found = true;
					Assert.assertEquals("Tuna longliners", in.getVesselType());
				}
			}
		
		Assert.assertTrue("Couldn't find the expected vessel among those returned by the search", found);
		
		System.out.println(response);
	}
	
	@Test
	public void testFilterByOwners() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.OWNERS, 329598, 306288)
		).ORDER_BY(
			filter.sortableColumns().
				descending(NAME)
		).
		current().
		GROUP_DUPLICATES().
		LIMIT(10).
		OFFSET(0);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Set<Integer> expected = new TreeSet<Integer>();
		expected.add(40000417);
		expected.add(40102290);
		expected.add(40000452);
		expected.add(40101177);
		expected.add(40108781);
		expected.add(40102155);
		expected.add(40110870);
		expected.add(40003793);
		expected.add(40000442);
		expected.add(40011123);
		
		Set<Integer> found = new TreeSet<Integer>();

		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getDataSet());
		Assert.assertTrue(response.getDataSet().size() == 10);
		
		for(VesselRecord in : response.getDataSet())
			found.add(in.getUid());
		
		Assert.assertArrayEquals(expected.toArray(new Integer[expected.size()]), found.toArray(new Integer[found.size()]));
	}
	
	@Test
	public void testFilterByOwnersHistorical() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.OWNERS, 33113)
		).ORDER_BY(
			filter.sortableColumns().
				descending(NAME)
		).
		historical().
		AMONG(new String[] { "LLOYD" }).
		SOURCED_BY("LLOYD").
		GROUP_DUPLICATES().
		LIMIT(10).
		OFFSET(0);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Set<Integer> expected = new TreeSet<Integer>();
		expected.add(11016839);
		expected.add(11011941);
		expected.add(11012619);
		
		Set<Integer> found = new TreeSet<Integer>();

		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getDataSet());
		Assert.assertTrue(response.getDataSet().size() == 3);
		
		for(VesselRecord in : response.getDataSet())
			found.add(in.getUid());
		
		Assert.assertArrayEquals(expected.toArray(new Integer[expected.size()]), found.toArray(new Integer[found.size()]));
	}
	
	@Test
	public void testFilterByOwnersCurrentUngrouped() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.OWNERS, 329598, 306288)
		).ORDER_BY(
			filter.sortableColumns().
				descending(NAME)
		).
		current().
		LIMIT(10).
		OFFSET(0);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Set<Integer> expected = new TreeSet<Integer>();
		expected.add(40000417);
		expected.add(40102290);
		expected.add(40000452);
		expected.add(40101177);
		expected.add(40108781);
		expected.add(40102155);
		expected.add(40110870);
		expected.add(40003793);
		expected.add(40000442);
		expected.add(40011123);
		
		Set<Integer> found = new TreeSet<Integer>();

		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getDataSet());
		Assert.assertTrue(response.getDataSet().size() == 10);
		
		for(VesselRecord in : response.getDataSet())
			found.add(in.getUid());
		
		Assert.assertArrayEquals(expected.toArray(new Integer[expected.size()]), found.toArray(new Integer[found.size()]));
	}
	
	@Test
	public void testFilterByOwnersHistoricalUngrouped() throws Throwable {
		VesselRecordSearchFilter filter = this.getBasicFilter();

		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(ID,
						UID,
						SOURCE,
						IMO,
						NAME,
						FLAG,
						LOA,
						LENGTH_TYPE,
						LENGTH_VALUE,
						GT,
						TONNAGE_TYPE,
						TONNAGE_VALUE,
						VESSEL_TYPE,
						GEAR_TYPE,
						GEAR_TYPE_ID,
						UPD_DATE)
		).WHERE(
			filter.clauses().
				and(VesselRecordClauses.OWNERS, 33113)
		).ORDER_BY(
			filter.sortableColumns().
				descending(NAME)
		).
		historical().
		DONT_GROUP_DUPLICATES().
		AMONG(new String[] { "LLOYD" }).
		SOURCED_BY("LLOYD").
		LIMIT(10).
		OFFSET(0);

		VesselRecordSearchResponse<VesselRecord> response = this.testFilter(filter);
		
		Set<Integer> expected = new TreeSet<Integer>();
		expected.add(11016839);
		expected.add(11011941);
		expected.add(11012619);
		
		Set<Integer> found = new TreeSet<Integer>();

		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getDataSet());
		Assert.assertTrue(response.getDataSet().size() == 3);
		
		for(VesselRecord in : response.getDataSet())
			found.add(in.getUid());
		
		Assert.assertArrayEquals(expected.toArray(new Integer[expected.size()]), found.toArray(new Integer[found.size()]));
	}
}
