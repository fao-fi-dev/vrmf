/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.test.business.dao.ioc.support;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.business.dao.UserManagementDAO;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Mar 2013
 */
@Named
@Singleton
public class DAOConsumer {
	@Inject private UserManagementDAO _userManagementDAO;

	/**
	 * @return the 'userManagementDAO' value
	 */
	public UserManagementDAO getUserManagementDAO() {
		return this._userManagementDAO;
	}

	/**
	 * @param userManagementDAO the 'userManagementDAO' value to set
	 */
	public void setUserManagementDAO(UserManagementDAO userManagementDAO) {
		this._userManagementDAO = userManagementDAO;
	}
	
	public String getUserDetails(String userID) throws Throwable {
		return this._userManagementDAO.getAllUserDetails(userID).toString();
	}
	
	public String getUserDetails() throws Throwable {
		return this._userManagementDAO.getAllUserDetails("PUBLIC_GUEST").toString();
	}
}
