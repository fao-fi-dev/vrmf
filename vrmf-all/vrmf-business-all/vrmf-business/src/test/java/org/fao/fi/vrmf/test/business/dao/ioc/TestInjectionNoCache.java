/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.test.business.dao.ioc;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Mar 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
	"/test/config/spring/vrmf-business-test-spring-datasource.xml",
	"/test/config/spring/vrmf-business-test-spring-context-injection.xml",
	"/config/spring/caching/vrmf-business-spring-context-cache-disabled.xml"
})
public class TestInjectionNoCache extends AbstractTestInjection {
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.test.dao.ioc.AbstractTestInjection#postTestUserDetailsRetrievement()
	 */
	@Override
	protected void postTestUserDetailsRetrievement() throws Throwable {
		//DO NOTHING
	}
}
