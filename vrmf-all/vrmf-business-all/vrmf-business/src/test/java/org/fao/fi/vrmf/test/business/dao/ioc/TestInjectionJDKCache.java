/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.test.business.dao.ioc;


import java.util.concurrent.ConcurrentMap;

import javax.inject.Inject;
import javax.inject.Named;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Mar 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
	"/test/config/spring/vrmf-business-test-spring-datasource.xml",
	"/test/config/spring/vrmf-business-test-spring-context-injection.xml",
	"/config/spring/caching/vrmf-business-spring-context-cache-jdk.xml",
	"/config/spring/caching/vrmf-business-spring-context-cache-standalone.xml"
})
public class TestInjectionJDKCache extends AbstractTestInjection {
	@Inject protected @Named(CACHE_MANAGER_ID) CacheManager _cacheManager;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.business.test.dao.ioc.AbstractTestInjection#postTestUserDetailsRetrievement()
	 */
	@Override
	protected void postTestUserDetailsRetrievement() throws Throwable {
		Cache cache = _cacheManager.getCache(CACHE_ID);
		
		Assert.assertTrue(((ConcurrentMap<?, ?>)cache.getNativeCache()).size() > 0);
		
		cache.clear();
		
		Assert.assertTrue(((ConcurrentMap<?, ?>)cache.getNativeCache()).size() == 0);
	}
	
	@AfterClass
	static public void resetEnvironment() {
		System.clearProperty("vrmf.terracotta.server");
		System.clearProperty("catalina.base");
	}
}
