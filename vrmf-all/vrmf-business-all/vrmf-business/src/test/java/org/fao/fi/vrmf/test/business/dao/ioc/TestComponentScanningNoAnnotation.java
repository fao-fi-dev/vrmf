/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.test.business.dao.ioc;

import javax.inject.Inject;

import org.fao.fi.vrmf.business.dao.generated.SPortsDAO;
import org.fao.fi.vrmf.business.dao.generated.SPortsDAOImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Mar 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
	"/test/config/spring/vrmf-business-test-spring-datasource.xml",
	"/test/config/spring/vrmf-business-test-spring-context-ioc.xml",
	"/test/config/spring/vrmf-business-test-spring-datasource.xml",
	"/config/spring/dao/generated/vrmf-business-spring-context-dao-generated.xml"
})
public class TestComponentScanningNoAnnotation {
	@Inject protected SPortsDAO _dao;
	
	@Test
	public void testInitialization() {
		Assert.assertNotNull(_dao);
		Assert.assertEquals(_dao.getClass(), SPortsDAOImpl.class);
		Assert.assertNotNull(_dao.getSqlMapClient());
	}
}
