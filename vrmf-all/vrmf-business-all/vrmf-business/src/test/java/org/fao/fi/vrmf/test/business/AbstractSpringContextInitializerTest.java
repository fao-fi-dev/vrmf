/**
 * (c) 2015 FAO / UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.test.business;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 28, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 28, 2015
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 5 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 5 Mar 2013
 * 
 * @deprecated Useless, as we switched to SpringJUnit4ClassRunner?
 */
@Deprecated
abstract public class AbstractSpringContextInitializerTest {
	protected Logger _log = LoggerFactory.getLogger(this.getClass());
	
	protected static String CATALINA_BASE_PROPERTY_NAME 	= "catalina.base";
	protected static String TERRACOTTA_SERVER_PROPERTY_NAME = "vrmf.terracotta.server";
	
	protected static String CATALINA_BASE_PROPERTY_VALUE	 = "src/test/resources";
	protected static String TERRACOTTA_SERVER_PROPERTY_VALUE = "hqldvfigis1";
	
	protected static ApplicationContext _ctx;
	
	protected ApplicationContext getContext() {
		return AbstractSpringContextInitializerTest._ctx;
	}
	
	static protected void commonInitialize() {
		System.setProperty(CATALINA_BASE_PROPERTY_NAME, CATALINA_BASE_PROPERTY_VALUE);
		System.setProperty(TERRACOTTA_SERVER_PROPERTY_NAME, TERRACOTTA_SERVER_PROPERTY_VALUE);
	}
}
