/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-business)
 */
package org.fao.fi.vrmf.test.business.dao.ioc;

import javax.inject.Inject;

import org.fao.fi.vrmf.business.dao.UserManagementDAO;
import org.fao.fi.vrmf.test.business.dao.ioc.support.DAOConsumer;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Mar 2013
 */
abstract public class AbstractTestInjection {
	final static protected String CACHE_MANAGER_ID = "vrmf.business.cache.manager";
	final static protected String CACHE_ID = "vrmf.business.user.management";
	
	@Inject protected UserManagementDAO _dao;
	@Inject protected UserManagementDAO _anotherDao;
	@Inject protected DAOConsumer _daoConsumer;
			
	@Test
	public void testAutowiring() {
		Assert.assertNotNull(_dao);
		Assert.assertEquals(_dao, _anotherDao);
		Assert.assertNotNull(_daoConsumer);
		
		Assert.assertNotNull(_daoConsumer.getUserManagementDAO());
	}
	
	@Test
	public void testUserDetailsRetrievement() throws Throwable {
		for(int i=0; i<1; i++) {
			System.out.println("Iteration #" + ( i+1 ));
			Assert.assertNotNull(_daoConsumer.getUserDetails("PUBLIC_GUEST"));
		}
		
		this.postTestUserDetailsRetrievement();
	}
	
	abstract protected void postTestUserDetailsRetrievement() throws Throwable;
}
