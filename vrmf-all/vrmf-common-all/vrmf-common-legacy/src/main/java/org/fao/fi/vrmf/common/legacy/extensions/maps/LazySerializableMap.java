/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-legacy)
 */
package org.fao.fi.vrmf.common.legacy.extensions.maps;

import java.io.Serializable;

import org.fao.fi.sh.utility.caching.spi.CacheFacade;
import org.fao.fi.sh.utility.model.extensions.maps.SerializableMap;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Dec 2010
 */
abstract public class LazySerializableMap<KEY extends Serializable, VALUE extends Serializable> extends LazyMap<KEY, VALUE> implements SerializableMap<KEY, VALUE> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 768642766403309060L;

	/**
	 * Class constructor
	 *
	 */
	public LazySerializableMap() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param maxCacheSize
	 */
	public LazySerializableMap(CacheFacade<KEY, VALUE> backingCache) {
		super(backingCache);
	}
}