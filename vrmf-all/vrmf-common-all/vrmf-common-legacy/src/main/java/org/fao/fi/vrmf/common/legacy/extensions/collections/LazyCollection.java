/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-legacy)
 */
package org.fao.fi.vrmf.common.legacy.extensions.collections;

import java.util.Collection;
import java.util.Iterator;

import org.fao.fi.sh.utility.caching.impl.simple.SimpleCacheFacade;
import org.fao.fi.sh.utility.caching.spi.CacheFacade;
import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;


import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

/**
 * Abstract implementation for lazily-loaded (and cached) collections, with some limitations 
 * (see {@link #contains(Object)} throwing a RuntimeException on each invocation).
 * 
 * Requires the ehCache library.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
abstract public class LazyCollection<ID, ENTRY> extends AbstractLoggingAwareClient implements Collection<ENTRY> {
	protected CacheFacade<ID, ENTRY> _backingCache = null;
	
	public LazyCollection() {
		this._backingCache = new SimpleCacheFacade<ID, ENTRY>(ObjectsHelper.thi$(this));
	}
	
	/**
	 * Class constructor
	 */
	public LazyCollection(CacheFacade<ID, ENTRY> backingCache) {
		this._backingCache = $nN(backingCache, "The backing cache cannot be NULL");
	}
	
	/* (non-Javadoc)
	 * @see java.util.Collection#size()
	 */
	@Override
	public int size() {
		return this.getSize();
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(Object o) {
		throw new RuntimeException("Unimplemented");
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#iterator()
	 */
	@Override
	public Iterator<ENTRY> iterator() {		
		return new Iterator<ENTRY>() {
			private Iterator<ID> _indexIterator = getIndexes().iterator();
			
			/* (non-Javadoc)
			 * @see java.util.Iterator#hasNext()
			 */
			@Override
			public boolean hasNext() {				
				return this._indexIterator.hasNext();
			}

			/* (non-Javadoc)
			 * @see java.util.Iterator#next()
			 */
			@Override
			public ENTRY next() {
				return getElement(this._indexIterator.next());
			}

			/* (non-Javadoc)
			 * @see java.util.Iterator#remove()
			 */
			@Override
			public void remove() {
				throw new RuntimeException("Unimplemented");
			}			
		};
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#toArray()
	 */
	@Override
	public Object[] toArray() {
		throw new RuntimeException("Unimplemented");
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#toArray(T[])
	 */
	@Override
	public <T> T[] toArray(T[] a) {
		throw new RuntimeException("Unimplemented");
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#add(java.lang.Object)
	 */
	@Override
	public boolean add(ENTRY e) {
		throw new RuntimeException("Unimplemented");
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#remove(java.lang.Object)
	 */
	@Override
	public boolean remove(Object o) {
		throw new RuntimeException("Unimplemented");
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#containsAll(java.util.Collection)
	 */
	@Override
	public boolean containsAll(Collection<?> c) {
		throw new RuntimeException("Unimplemented");
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#addAll(java.util.Collection)
	 */
	@Override
	public boolean addAll(Collection<? extends ENTRY> c) {
		throw new RuntimeException("Unimplemented");
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#removeAll(java.util.Collection)
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		throw new RuntimeException("Unimplemented");
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#retainAll(java.util.Collection)
	 */
	@Override
	public boolean retainAll(Collection<?> c) {
		throw new RuntimeException("Unimplemented");
	}

	/* (non-Javadoc)
	 * @see java.util.Collection#clear()
	 */
	@Override
	public void clear() {
		throw new RuntimeException("Unimplemented");
	}
	
	/**
	 * @return the collection size (its current value depends on the backing data store for the concrete collection)
	 */
	abstract protected int getSize();
	
	/**
	 * @return the collection element indexes (whose values depend on the backing data store for the concrete collection)
	 */
	abstract protected Collection<ID> getIndexes();
	
	/**
	 * @param index an element index
	 * 
	 * @return the collection element with the given index (when available)
	 */
	abstract protected ENTRY doGetElement(ID index);
	
	/**
	 * Implements the lazy-loading (and caching) mechanism underneath this collection.
	 * 
	 * @param index an element index
	 * 
	 * @return the collection element with the given index (when available) 
	 */
	private ENTRY getElement(ID index) {
		ENTRY object = this._backingCache.get(index);
		
		if(object == null) {
			this._log.debug("MISS: Retrieving element with index " + index + " from persistency");
			
			object = this.doGetElement(index);
			
			if(object != null)
				this._backingCache.put(index, object);
			else
				this._log.warn("Element with index " + index + " cannot be retrieved from persistency (returns NULL)");
		} else {
			this._log.debug("HIT : Element with index " + index + " is already cached");
		}
		
		return object;
	}
}