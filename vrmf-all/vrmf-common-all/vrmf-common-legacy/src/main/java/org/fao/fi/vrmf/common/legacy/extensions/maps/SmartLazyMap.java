/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-legacy)
 */
package org.fao.fi.vrmf.common.legacy.extensions.maps;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.fao.fi.sh.utility.caching.impl.simple.SimpleCacheFacade;
import org.fao.fi.sh.utility.caching.spi.CacheFacade;
import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

/**
 * Abstract implementation for lazily-loaded (and cached) maps, with some limitations 
 * (see {@link #containsValue(Object)} throwing a RuntimeException on each invocation).
 * 
 * Requires the ehCache library.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
abstract public class SmartLazyMap<KEY, VALUE> extends AbstractLoggingAwareClient implements Map<KEY, VALUE> {
	abstract protected int doGetSize() throws Throwable;
	abstract protected VALUE doGetElement(KEY key) throws Throwable;
	abstract protected Set<KEY> doGetKeySet() throws Throwable;
	
	protected CacheFacade<KEY, VALUE> _backingMap = null;
	
	/**
	 * Class constructor
	 */
	public SmartLazyMap() {
		this(new SimpleCacheFacade<KEY, VALUE>());
	}
	
	public SmartLazyMap(CacheFacade<KEY, VALUE> backingMap) {
		$nN(backingMap, "The backing cache cannot be null");
		
		this._log.info("Initializing {} with backing map {}",  ObjectsHelper.thi$(this),  ObjectsHelper.thi$(backingMap));
		
		this._backingMap = backingMap;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Map#size()
	 */
	@Override
	public int size() {
		try {
			return this.doGetSize();
		} catch (Throwable t) {
			String message = "Unable to get size for lazy map " + this;
			
			this._log.error(message, t);
			
			throw new RuntimeException(message, t);
		}
	}

	/* (non-Javadoc)
	 * @see java.util.Map#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return this.size() > 0;
	}

	/* (non-Javadoc)
	 * @see java.util.Map#containsKey(java.lang.Object)
	 */
	@Override
	public boolean containsKey(Object key) {
		return this.get(key) != null;
	}

	/* (non-Javadoc)
	 * @see java.util.Map#containsValue(java.lang.Object)
	 */
	@Override
	public boolean containsValue(Object value) {
		throw new RuntimeException("Unimplemented");
	}

	/* (non-Javadoc)
	 * @see java.util.Map#get(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public VALUE get(Object key) {
		VALUE value = this._backingMap.get((KEY)key);
		
		if(value == null) {
			this._log.debug("Getting element with key " + key + " from cache " + this._backingMap.getCacheID());

			try {
				value = this.doGetElement((KEY)key);
							
				this._backingMap.put((KEY)key, value);
			} catch (Throwable t) {
				String message = "Unable to get element with key " + key + " from lazy map " + this;
				
				this._log.error(message, t);
				
				throw new RuntimeException(message, t);
			}
		} else {
			this._log.debug("Element with key " + key + " is already cached by cache " + this._backingMap.getCacheID());
		}
		
		return value;
	}

	/* (non-Javadoc)
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public VALUE put(KEY key, VALUE value) {
		return this._backingMap.put(key, value);
	}

	/* (non-Javadoc)
	 * @see java.util.Map#remove(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public VALUE remove(Object key) {
		return this._backingMap.remove((KEY)key);
	}

	/* (non-Javadoc)
	 * @see java.util.Map#putAll(java.util.Map)
	 */
	@Override
	public void putAll(Map<? extends KEY, ? extends VALUE> m) {
		for(Entry<? extends KEY, ? extends VALUE> entry : m.entrySet())
			this.put(entry.getKey(), entry.getValue());
	}

	/* (non-Javadoc)
	 * @see java.util.Map#clear()
	 */
	@Override
	public void clear() {
		this._backingMap.clear();
	}

	/* (non-Javadoc)
	 * @see java.util.Map#keySet()
	 */
	@Override
	public Set<KEY> keySet() {
		try {
			return this.doGetKeySet();
		} catch (Throwable t) {
			String message = "Unable to get keyset for lazy map " + this;
			
			this._log.error(message, t);
			
			throw new RuntimeException(message, t);
		}
	}

	/* (non-Javadoc)
	 * @see java.util.Map#values()
	 */
	@Override
	public Collection<VALUE> values() {
		throw new RuntimeException("Unimplemented");
	}

	/* (non-Javadoc)
	 * @see java.util.Map#entrySet()
	 */
	@Override
	public Set<Entry<KEY, VALUE>> entrySet() {
		throw new RuntimeException("Unimplemented");
	}
	
	/**
	 * @return
	 */
	final protected int getAvailableDataSize() {
		return this._backingMap.exactSize();
	}
	
	/**
	 * @return
	 */
	final protected Set<KEY> getAvailableDataKeySet() {
		return new HashSet<KEY>(this._backingMap.keySet());
	}
}