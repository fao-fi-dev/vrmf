/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-legacy)
 */
package org.fao.fi.vrmf.common.legacy.helpers;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.TreeSet;

import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.model.NameValuePair;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Nov 2010
 */
public class BundleManager extends AbstractLoggingAwareClient {
	private final Locale _locale;

	private final Collection<ResourceBundle> _bundles;

	static final private String DEFAULT_PLACEHOLDER = "{}";
	static final private String NULL_SUBSTITUTE		= "<NULL>";

	/** See: http://docs.oracle.com/javase/6/docs/api/java/util/ResourceBundle.Control.html */
	final private ResourceBundle.Control _UTF8PropertyResourceBundleController = new ResourceBundle.Control() {
		@Override
		public List<String> getFormats(String baseName) {
			if(baseName == null)
				throw new NullPointerException();

			return Arrays.asList("properties");
		}

		@Override
		public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload) throws IllegalAccessException, InstantiationException, IOException {
			if(baseName == null || locale == null || format == null || loader == null)
				throw new NullPointerException();

			ResourceBundle bundle = null;

			if(format.equals("properties")) {
				String bundleName = toBundleName(baseName, locale);
				String resourceName = toResourceName(bundleName, format);

				Reader r = new InputStreamReader(loader.getResourceAsStream(resourceName), "UTF-8");

				//If initialized through a reader, it can easily ingest property files
				//encoded with any charset...
				bundle = new PropertyResourceBundle(r);

				r.close();
			}

			return bundle;
		}
	};

	private BundleManager(Locale locale) {
		super();

		this._locale = locale == null ? Locale.getDefault() : locale;
		this._bundles = new ArrayList<ResourceBundle>();
	}

	/**
	 * Class constructor
	 *
	 * @param locale
	 * @param bundleName
	 */
	public BundleManager(Locale locale, String... bundleNames) {
		this(locale);

		for(String bundleName : $nEm(bundleNames, "Bundle names cannot be NULL or empty")) {
			try {
				this._bundles.add(ResourceBundle.getBundle(bundleName, this._locale, Thread.currentThread().getContextClassLoader(), this._UTF8PropertyResourceBundleController));
			} catch (MissingResourceException MRe) {
				this._log.error("Unable to initialize bundle manager for locale '{}' and bundle name '{}'. Unexpected {} caught [ {} ]", 
								locale.getLanguage(), 
								bundleName,
								MRe.getClass().getSimpleName(),
								MRe.getMessage());
				this._log.error("Unexpected exception caught", MRe);
			}
		}
	}

	/**
	 * Class constructor
	 *
	 * @param bundleName
	 */
	public BundleManager(Locale locale, String bundleName) {
		this(locale, bundleName == null ? null : new String[] { bundleName });
	}

	/**
	 * Class constructor
	 *
	 * @param bundleName
	 */
	public BundleManager(String bundleName) {
		this(Locale.getDefault(), bundleName);
	}

	private String wrapTextID(String textID) {
		return textID == null ? null : "!! " + textID + " !!";
	}

	public String getText(String textID) {
		if(this._bundles == null || this._bundles.isEmpty()) {
			this._log.warn("No bundle available: returning entry ID as value for '{}'", textID);

			return this.wrapTextID(textID);
		}

		for(ResourceBundle bundle : this._bundles) {
			try {
				return bundle.getString(textID);
			} catch (MissingResourceException MRe) {
				//No need to catch it
			}
		}

		this._log.warn("No data available for entry ID in any of the bundles: returning entry ID as value for '{}'", textID);

		return this.wrapTextID(textID);
	}

	/**
	 * @param textID
	 * @param parameters
	 * @return
	 */
	public <S extends Serializable> String getText(String textID, Collection<S> parameters) {
		return this.getTextWithCustomPlaceholder(textID, DEFAULT_PLACEHOLDER, parameters.toArray(new Serializable[parameters.size()]));
	}

	/**
	 * @param textID
	 * @param parameters
	 * @return
	 */
	public <S extends Serializable> String getText(String textID, @SuppressWarnings("unchecked") S... parameters) {
		return this.getTextWithCustomPlaceholder(textID, DEFAULT_PLACEHOLDER, parameters);
	}

	/**
	 * @param textID
	 * @param parameters
	 * @return
	 */
	public <S extends Serializable> String getTextWithCustomPlaceholder(String textID, String placeholder, @SuppressWarnings("unchecked") S... parameters) {
		String text = this.getText(textID);

		if(text == null || "".equals(text))
			return text;

		if(placeholder == null || "".equals(placeholder)) {
			this._log.warn("Invalid placeholder for parameter substitution (\"{}\"). Using \"{}\" instead...", placeholder, DEFAULT_PLACEHOLDER);

			placeholder = DEFAULT_PLACEHOLDER;
		}

		int numPlaceholders = 0;
		int index = 0;

		while((index = text.indexOf(placeholder, index)) != -1) {
			numPlaceholders++;
			index += placeholder.length();
		}

		int setParameters = 0;
		
		for(S parameter : parameters)
			if(parameter != null)
				setParameters++;
		
		if(numPlaceholders > setParameters)
			this._log.warn("Label: {} - {} placeholders vs. {} provided parameters: some placeholder will remain as it is...", textID, numPlaceholders, setParameters);
		else if(numPlaceholders < setParameters)
			this._log.warn("Label: {} - {} placeholders vs. {} provided parameters: some parameter will not be used...", textID, numPlaceholders, setParameters);

		index = 0;

		for(Serializable param : parameters) {
			index = text.indexOf(placeholder);

			if(index >= 0)
				text =
					text.substring(0, index) +
						( param == null ? NULL_SUBSTITUTE : param.toString() ) +
					text.substring(index + placeholder.length());
		}

		return text;
	}

	public String[] getLabels() {
		Collection<String> labels = new TreeSet<String>();

		for(ResourceBundle bundle : this._bundles) {
			labels.addAll(bundle.keySet());
		}

		return labels.toArray(new String[labels.size()]);
	}

	public NameValuePair[] getLabelsAndValues() {
		Collection<NameValuePair> labelsAndValues = new ArrayList<NameValuePair>();

		for(String label : this.getLabels())
			labelsAndValues.add(new NameValuePair(label, this.getText(label)));

		return labelsAndValues.toArray(new NameValuePair[labelsAndValues.size()]);
	}

	public String toJSON() {
		StringBuilder JSON = new StringBuilder();
		JSON.append("{\n");

		for(NameValuePair data : this.getLabelsAndValues()) {
			JSON.append("\t").append("\"").append(data.getName().replace("\"", "\\\"")).append("\"");
			JSON.append(": ").append("\"").append(((String)data.getValue()).replace("\"", "\\\"")).append("\"").append(",\n");
		}

		JSON.append("\t_padder: \"\" //TO BE REMOVED...\n");
		JSON.append("}");

		return JSON.toString();
	}
}