/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-legacy)
 */
package org.fao.fi.vrmf.common.legacy.extensions.collections;

import java.io.Serializable;

import org.fao.fi.sh.utility.caching.spi.CacheFacade;
import org.fao.fi.sh.utility.model.extensions.collections.SerializableCollection;

/**
 * Abstract implementation for lazily-loaded (and cached) collections, with some limitations 
 * (see {@link #contains(Object)} throwing a RuntimeException on each invocation).
 * 
 * Requires the ehCache library.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Oct 2010
 */
abstract public class LazySerializableCollection<ID extends Serializable, ENTRY extends Serializable> extends LazyCollection<ID, ENTRY> implements SerializableCollection<ENTRY> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3120861645838570874L;

	/**
	 * Class constructor
	 *
	 * @param backingCache
	 */
	public LazySerializableCollection(CacheFacade<ID, ENTRY> backingCache) {
		super(backingCache);
	}
}