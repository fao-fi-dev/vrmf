/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes;

import java.io.Serializable;

import org.fao.fi.sh.model.core.spi.Typed;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
abstract public class DateReferencedValuedTypeRelatedVesselAttribute<VALUE extends Number, TYPE_IDENTIFIER extends Serializable> extends DateReferencedValuedVesselAttribute<VALUE> implements Typed<TYPE_IDENTIFIER> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1614691634142209812L;
	
	public DateReferencedValuedTypeRelatedVesselAttribute() {
		super();
	}	
	
	public DateReferencedValuedTypeRelatedVesselAttribute(DateReferencedValuedTypeRelatedVesselAttribute<VALUE, TYPE_IDENTIFIER> another) {
		super(another);
		
		this.setTypeId(another.getTypeId());
	}
	
//	/* (non-Javadoc)
//	 * @see org.fao.vrmf.utilities.common.models.behaviours.vessel.composite.DateReferencedValuedVesselAttribute#isEmpty()
//	 */
//	public boolean isEmpty() {
//		return super.isEmpty() && this.getTypeId() == null;
//	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.getTypeId() == null) ? 0 : this.getTypeId().hashCode());
		return result;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(this.getClass().isAssignableFrom(obj.getClass())))
			return false;
		@SuppressWarnings("unchecked")
		DateReferencedValuedTypeRelatedVesselAttribute<VALUE, TYPE_IDENTIFIER> other = (DateReferencedValuedTypeRelatedVesselAttribute<VALUE, TYPE_IDENTIFIER>) obj;		
		if (this.getTypeId() == null) {
			if (other.getTypeId() != null)
				return false;
		} else if (!this.getTypeId().equals(other.getTypeId()))
			return false;
		return true;
	}		
}