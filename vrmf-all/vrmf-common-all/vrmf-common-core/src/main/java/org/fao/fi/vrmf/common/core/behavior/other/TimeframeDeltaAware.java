/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.behavior.other;

/**
 * Basic interface for data that should reporte a maximum 'time delta' (as the maximum difference between all of its date referenced attributes)
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Feb 2013
 */
public interface TimeframeDeltaAware {
	void setMaxTimeDelta(Long maxTimeDelta);
	Long getMaxTimeDelta();
}
