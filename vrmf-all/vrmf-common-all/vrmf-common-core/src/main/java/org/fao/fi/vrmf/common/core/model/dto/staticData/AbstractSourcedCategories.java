/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

import org.fao.fi.vrmf.common.core.behavior.Sourced;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Mar 2013
 */
abstract public class AbstractSourcedCategories extends AbstractCategories implements Sourced {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3604122844481597365L;
	
	private String _sourceSystem;

	/**
	 * @return the 'sourceSystem' value
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}

	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}
}
