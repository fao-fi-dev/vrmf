/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

import org.fao.fi.sh.model.core.spi.Identified;
import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableData;

abstract public class AbstractSAuthorizationTerminationReasons extends AbstractSourcedUpdatableData implements Identified<Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1811877102878491731L;
}