/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.other;

import java.util.Date;

import org.fao.fi.sh.model.core.spi.Commentable;
import org.fao.fi.sh.model.core.spi.Updatable;
import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Nov 2011
 */
public class Range extends GenericData implements Updatable, Commentable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4442748560453656558L;
	
	private String _unitTypeId;
	private String _description;
	private Float _rangeFrom;
	private Float _rangeTo;
	private String _updaterId;
	private Date _updateDate;
	private String _comment;
	
	/**
	 * Class constructor
	 */
	public Range() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param unitTypeId
	 * @param description
	 * @param rangeFrom
	 * @param rangeTo
	 * @param updaterId
	 * @param updateDate
	 * @param comment
	 */
	public Range(String unitTypeId, String description, Float rangeFrom, Float rangeTo, String updaterId, Date updateDate, String comment) {
		super();
		this._unitTypeId = unitTypeId;
		this._description = description;
		this._rangeFrom = rangeFrom;
		this._rangeTo = rangeTo;
		this._updaterId = updaterId;
		this._updateDate = updateDate;
		this._comment = comment;
	}

	/**
	 * @return the 'unitTypeId' value
	 */
	public String getUnitTypeId() {
		return this._unitTypeId;
	}

	/**
	 * @param unitTypeId the 'unitTypeId' value to set
	 */
	public void setUnitTypeId(String unitTypeId) {
		this._unitTypeId = unitTypeId;
	}

	/**
	 * @return the 'description' value
	 */
	public String getDescription() {
		return this._description;
	}

	/**
	 * @param description the 'description' value to set
	 */
	public void setDescription(String description) {
		this._description = description;
	}

	/**
	 * @return the 'rangeFrom' value
	 */
	public Float getRangeFrom() {
		return this._rangeFrom;
	}

	/**
	 * @param rangeFrom the 'rangeFrom' value to set
	 */
	public void setRangeFrom(Float rangeFrom) {
		this._rangeFrom = rangeFrom;
	}

	/**
	 * @return the 'rangeTo' value
	 */
	public Float getRangeTo() {
		return this._rangeTo;
	}

	/**
	 * @param rangeTo the 'rangeTo' value to set
	 */
	public void setRangeTo(Float rangeTo) {
		this._rangeTo = rangeTo;
	}

	/**
	 * @return the 'updaterId' value
	 */
	public String getUpdaterId() {
		return this._updaterId;
	}

	/**
	 * @param updaterId the 'updaterId' value to set
	 */
	public void setUpdaterId(String updaterId) {
		this._updaterId = updaterId;
	}

	/**
	 * @return the 'updateDate' value
	 */
	public Date getUpdateDate() {
		return this._updateDate;
	}

	/**
	 * @param updateDate the 'updateDate' value to set
	 */
	public void setUpdateDate(Date updateDate) {
		this._updateDate = updateDate;
	}

	/**
	 * @return the 'comment' value
	 */
	public String getComment() {
		return this._comment;
	}

	/**
	 * @param comment the 'comment' value to set
	 */
	public void setComment(String comment) {
		this._comment = comment;
	}
}