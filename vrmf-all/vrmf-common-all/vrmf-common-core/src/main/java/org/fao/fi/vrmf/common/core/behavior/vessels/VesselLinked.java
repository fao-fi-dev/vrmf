/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.behavior.vessels;

import org.fao.fi.sh.utility.core.annotations.data.MetadataFields;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
@MetadataFields({"vesselId", "vesselUid"})
public interface VesselLinked {
	Integer getVesselId();
	void setVesselId(Integer vesselId);
	
	Integer getVesselUid();
	void setVesselUid(Integer vesselUid);
}
