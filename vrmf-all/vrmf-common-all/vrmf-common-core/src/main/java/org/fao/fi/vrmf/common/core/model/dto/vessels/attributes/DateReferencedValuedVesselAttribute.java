/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes;

import org.fao.fi.sh.model.core.spi.DateReferencedValued;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
abstract public class DateReferencedValuedVesselAttribute<VALUE extends Number> extends AbstractDateReferencedVesselAttribute implements DateReferencedValued<VALUE> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1614691634142209812L;
	
	public DateReferencedValuedVesselAttribute(DateReferencedValuedVesselAttribute<VALUE> another) {
		super(another);
		
		this.setValue(another.getValue());
	}
}