/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

import org.fao.fi.sh.model.core.spi.ComplexNameAware;
import org.fao.fi.vrmf.common.core.behavior.MappedGroupable;
import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableFillableData;

abstract public class AbstractSCountries extends AbstractSourcedUpdatableFillableData implements ComplexNameAware, MappedGroupable<Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 267061059250663888L;
}