/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.behavior.other;

import org.fao.fi.sh.model.core.spi.Fillable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
public interface CountryAware<IDENTIFIER extends Comparable<? super IDENTIFIER>> extends Fillable {
	IDENTIFIER getCountryId();
	
	void setCountryId(IDENTIFIER countryId);
}