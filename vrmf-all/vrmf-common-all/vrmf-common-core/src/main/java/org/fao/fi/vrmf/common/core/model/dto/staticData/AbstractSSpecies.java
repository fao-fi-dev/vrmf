/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

import org.fao.fi.vrmf.common.core.behavior.MappedGroupable;
import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableData;

abstract public class AbstractSSpecies extends AbstractSourcedUpdatableData implements MappedGroupable<Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5383000320548250375L;
}