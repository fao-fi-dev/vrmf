/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.vrmf.common.core.behavior.SourcedUpdatable;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Mar 2013
 */
abstract public class AbstractSourcedUpdatableFillableData extends AbstractFillableData implements SourcedUpdatable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7029859230173127362L;
	
	@XmlAttribute
	@Getter @Setter private String sourceSystem;
	
	@XmlAttribute
	@Getter @Setter private Date updateDate;
	
	@XmlAttribute
	@Getter @Setter private String updaterId;
	
	@XmlElement
	@Getter @Setter private String comment;
}