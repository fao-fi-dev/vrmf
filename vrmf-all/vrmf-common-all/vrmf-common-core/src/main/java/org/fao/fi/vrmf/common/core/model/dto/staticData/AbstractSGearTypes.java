/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

import org.fao.fi.vrmf.common.core.behavior.MappedGroupable;
import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableData;

abstract public class AbstractSGearTypes extends AbstractSourcedUpdatableData implements MappedGroupable<Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8273890825166601585L;
}