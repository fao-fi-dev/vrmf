/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.authorizations;

import org.fao.fi.vrmf.common.core.model.dto.vessels.attributes.DateReferencedIdentifiedTimeframedVesselAttribute;

abstract public class AbstractAuthorizations extends DateReferencedIdentifiedTimeframedVesselAttribute {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6642061135911939670L;
}