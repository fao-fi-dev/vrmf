/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.support.comparators;

import java.io.Serializable;
import java.util.Comparator;

import org.fao.fi.sh.model.core.spi.DateReferenced;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/giu/2010   ffiorellato     Creation.
 *
 * @version 1.0
 * @since 28/giu/2010
 */
public class DateReferencedComparator implements Comparator<DateReferenced>, Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2212742346945617065L;
	
	private boolean _ascending = false;

	/**
	 * Class constructor.
	 *
	 * @param ascending
	 */
	public DateReferencedComparator(boolean ascending) {
		super();
		
		this._ascending = ascending;
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(DateReferenced first, DateReferenced second) {
		return ( this._ascending ? 1 : -1 ) * this.doCompare(first, second);	
	}
	
	/**
	 * Performs a descending comparison between two date referenced data objects.
	 *  
	 * @param first the first date referenced data object to compare
	 * @param second the second date referenced data object to compare
	 * @return 
	 * <ul>
	 * 	<li>0 if both data object are null or have a null reference date</li>
	 * 	<li>-1 if the first data object is null or has a null reference date</li>
	 * 	<li>1 if the second data object is null or has a null reference date</li>
	 *  <li><code>first.getReferenceDate().compareTo(second.getReferenceDate())</code> otherwise</li>
	 * </ul> 
	 */
	private int doCompare(DateReferenced first, DateReferenced second) {
		if(first == null || first.getReferenceDate() == null)
			return second == null || second.getReferenceDate() == null ? 0 : -1;
		
		if(second == null || second.getReferenceDate() == null)
			return 1;
		
		int toReturn = first.getReferenceDate().compareTo(second.getReferenceDate());
		
		return toReturn;
	}
}