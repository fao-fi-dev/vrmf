/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

abstract public class AbstractSLengthCategories extends AbstractSourcedCategories {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1821146177842852444L;
}