/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

import org.fao.fi.sh.model.core.spi.Commentable;
import org.fao.fi.vrmf.common.core.behavior.other.CountryAware;
import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableFillableData;

abstract public class AbstractSCountriesToCallsigns extends AbstractSourcedUpdatableFillableData implements CountryAware<Integer>, Commentable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6343640644676058150L;
}