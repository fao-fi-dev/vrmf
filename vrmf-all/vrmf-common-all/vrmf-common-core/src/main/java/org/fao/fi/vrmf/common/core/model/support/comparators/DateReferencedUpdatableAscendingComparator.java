/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.support.comparators;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/giu/2010   ffiorellato     Creation.
 *
 * @version 1.0
 * @since 28/giu/2010
 */
final public class DateReferencedUpdatableAscendingComparator extends DateReferencedUpdatableComparator {
	
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6065697291600011372L;

	/**
	 * Class constructor.
	 */
	public DateReferencedUpdatableAscendingComparator() {
		super(true);
	}
}