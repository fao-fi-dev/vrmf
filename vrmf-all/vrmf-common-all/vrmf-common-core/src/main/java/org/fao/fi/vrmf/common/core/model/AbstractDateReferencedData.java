/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;

import org.fao.fi.sh.model.core.spi.DateReferenced;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Jul 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Jul 2011
 */
abstract public class AbstractDateReferencedData extends GenericData implements DateReferenced {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1516986939799495546L;
	
	@XmlAttribute
	@Getter @Setter private Date referenceDate;
}
