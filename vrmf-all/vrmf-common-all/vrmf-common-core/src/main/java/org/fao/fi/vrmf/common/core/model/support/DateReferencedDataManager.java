/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.support;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.vrmf.common.core.model.support.comparators.DateReferencedDescendingComparator;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2010
 */
public class DateReferencedDataManager<DATA extends DateReferenced> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1972850699990973096L;

	public DATA getCurrentData(List<DATA> data) {
		if(data == null || data.size() == 0)
			return null;
		
		if(data.size() == 1)
			return data.get(0);
		
		List<DATA> sorted = new ArrayList<DATA>(data); 
		Collections.sort(sorted, new DateReferencedDescendingComparator());
		
		return sorted.get(0);
	}
	
	public List<DATA> itemsBefore(List<DATA> data, final Date date) {
		$nN(date, "Date filter cannot be NULL");
		
		return $nN(data, "Data to scan cannot be NULL").stream().filter(D -> D.getReferenceDate() != null && ( D.getReferenceDate().before(date) || D.getReferenceDate().equals(date) )).collect(Collectors.toList());
	}
	
	public List<DATA> itemsAfter(List<DATA> data, final Date date) {
		$nN(date, "Date filter cannot be NULL");
		
		return $nN(data, "Data to scan cannot be NULL").stream().filter(D -> D.getReferenceDate() != null && ( D.getReferenceDate().after(date) || D.getReferenceDate().equals(date) )).collect(Collectors.toList());
	}
	
	public List<DATA> itemsBetween(List<DATA> data, final Date from, final Date to) {
		$nN(from, "Date FROM filter cannot be NULL");
		$nN(to, "Date TO filter cannot be NULL");
		
		return $nN(data, "Data to scan cannot be NULL").stream().filter(
			D -> D.getReferenceDate() != null && 
			   ( D.getReferenceDate().after(from) || D.getReferenceDate().equals(from) ) &&
			   ( D.getReferenceDate().before(to) || D.getReferenceDate().equals(to) )).collect(Collectors.toList());
	}
}