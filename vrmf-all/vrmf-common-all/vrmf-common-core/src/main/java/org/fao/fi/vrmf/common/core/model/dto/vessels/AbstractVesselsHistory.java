/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels;

import org.fao.fi.sh.model.core.spi.Commentable;
import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
abstract public class AbstractVesselsHistory extends AbstractSourcedUpdatableData implements Commentable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5203894675705105337L;
}