/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.support.comparators;

import java.io.Serializable;
import java.util.Comparator;

import org.fao.fi.vrmf.common.core.behavior.DateReferencedUpdatable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/giu/2010   ffiorellato     Creation.
 *
 * @version 1.0
 * @since 28/giu/2010
 */
public class DateReferencedUpdatableComparator implements Comparator<DateReferencedUpdatable>, Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2212742346945617065L;
	
	private boolean _ascending = false;

	/**
	 * Class constructor.
	 *
	 * @param ascending
	 */
	public DateReferencedUpdatableComparator(boolean ascending) {
		super();
		
		this._ascending = ascending;
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(DateReferencedUpdatable first, DateReferencedUpdatable second) {
		return ( this._ascending ? 1 : -1 ) * this.doCompare(first, second);	
	}
	
	/**
	 * Performs a descending comparison between two date referenced, updatable data objects.
	 *  
	 * @param first the first date referenced, updatable data object to compare
	 * @param second the second date referenced, updatable data object to compare
	 * @return 
	 * <ul>
	 * 	<li>0 if both data object are <code>null</code> or have a <code>null</code> reference date</li>
	 * 	<li>-1 if the first data object is <code>null</code> or has a <code>null</code> reference date</li>
	 * 	<li>1 if the second data object is <code>null</code> or has a <code>null</code> reference date</li>
	 *  <li>
	 *  	<code>first.getReferenceDate().compareTo(second.getReferenceDate())</code> otherwise.
	 *  	If the result is again 0, then returns:
	 *  	<ul>
	 *  		<li>0 if both data object have a <code>null</code> update date</li>
	 * 			<li>-1 if the first data object has a <code>null</code> update date</li>
	 * 			<li>1 if the second data object has a <code>null</code> update date</li>
	 * 			<li>first.getUpdateDate().compareTo(second.getUpdateDate())
	 *  	</ul>
	 *  </li>
	 * </ul> 
	 */
	private int doCompare(DateReferencedUpdatable first, DateReferencedUpdatable second) {
		if(first == null || first.getReferenceDate() == null)
			return second == null || second.getReferenceDate() == null ? 0 : -1;
		
		if(second == null || second.getReferenceDate() == null)
			return 1;
		
		int toReturn = first.getReferenceDate().compareTo(second.getReferenceDate());
		
		if(toReturn == 0) {
			if(first.getUpdateDate() == null)
				toReturn = second == null || second.getUpdateDate() == null ? 0 : -1;
			else if(second.getUpdateDate() == null)
				toReturn = 1;
			else
				toReturn = first.getUpdateDate().compareTo(second.getUpdateDate());
		}
		
		return toReturn;
	}
}