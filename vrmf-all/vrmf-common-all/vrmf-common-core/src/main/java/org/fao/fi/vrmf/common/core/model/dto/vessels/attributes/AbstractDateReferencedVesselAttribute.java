/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;

import org.fao.fi.vrmf.common.core.behavior.DateReferencedUpdatable;
import org.fao.fi.vrmf.common.core.behavior.vessels.attributes.DateReferencedVesselAttribute;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true) @Getter @Setter 
abstract public class AbstractDateReferencedVesselAttribute extends AbstractVesselAttribute implements DateReferencedUpdatable, DateReferencedVesselAttribute {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3877308890200039083L;
	
	private @XmlAttribute Date referenceDate;
	
	public AbstractDateReferencedVesselAttribute(AbstractDateReferencedVesselAttribute another) {
		super(another);
		
		this.setReferenceDate(another.getReferenceDate());
	}
}