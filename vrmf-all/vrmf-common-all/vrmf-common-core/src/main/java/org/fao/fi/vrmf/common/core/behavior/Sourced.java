/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.behavior;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 26, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 26, 2015
 */
public interface Sourced {
	String getSourceSystem();
	void setSourceSystem(String sourceSystem);
}
