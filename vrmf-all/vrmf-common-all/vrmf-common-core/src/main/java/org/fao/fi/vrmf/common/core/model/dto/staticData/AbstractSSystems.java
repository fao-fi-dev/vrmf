/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

import org.fao.fi.sh.model.core.spi.Identified;
import org.fao.fi.sh.model.core.spi.Updatable;
import org.fao.fi.vrmf.common.core.model.GenericData;

abstract public class AbstractSSystems extends GenericData implements Updatable, Identified<String> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5492962273328306966L;
}