/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

import org.fao.fi.sh.model.core.spi.Commentable;
import org.fao.fi.vrmf.common.core.behavior.other.PortAndCountryAware;
import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableFillableData;


abstract public class AbstractSPortsHistory extends AbstractSourcedUpdatableFillableData implements PortAndCountryAware<Integer, Integer>, Commentable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -74927080317444141L;
}