/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.support;

import java.io.Serializable;

/**
 * TODO Insert class / interface description.
 * 
 * <pre>
 *   ######################### HISTORY ############################
 *   Date:        Author:         Action:
 *   ------------ --------------- ---------------------------------
 *   05/gen/2010      ffiorellato         Creation.
 *   ##############################################################
 * </pre>
 * 
 * @author ffiorellato
 * 
 * @since 05/gen/2010
 * 
 * @version 1.0
 */
public final class FieldDelta implements Serializable {
	/** Comment for <code>serialVersionUID</code> */
	private static final long serialVersionUID = -2877814600395398778L;
	
	private String _field;
	private Object _oldValue;
	private Object _newValue;
		
	/**
	 * Class constructor.
	 */
	public FieldDelta() {
		super();
	}

	/**
	 * Class constructor.
	 * @param field
	 * @param oldValue
	 * @param newValue
	 */
	public FieldDelta(String field, Object oldValue, Object newValue) {
		super();
		this._field = field;
		this._oldValue = oldValue;
		this._newValue = newValue;
	}

	/**
	 * Getter for 'field'.
	 *
	 * @return Returns the 'field' value.
	 */
	public String getField() {
		return this._field;
	}

	/**
	 * Setter for 'field'.
	 *
	 * @param field The 'field' value to set.
	 */
	public void setField(String field) {
		this._field = field;
	}

	/**
	 * Getter for 'oldValue'.
	 *
	 * @return Returns the 'oldValue' value.
	 */
	public Object getOldValue() {
		return this._oldValue;
	}

	/**
	 * Setter for 'oldValue'.
	 *
	 * @param oldValue The 'oldValue' value to set.
	 */
	public void setOldValue(Object oldValue) {
		this._oldValue = oldValue;
	}

	/**
	 * Getter for 'newValue'.
	 *
	 * @return Returns the 'newValue' value.
	 */
	public Object getNewValue() {
		return this._newValue;
	}

	/**
	 * Setter for 'newValue'.
	 *
	 * @param newValue The 'newValue' value to set.
	 */
	public void setNewValue(Object newValue) {
		this._newValue = newValue;
	}
	
	public String toString() {
		return this._field + " : " + this._oldValue + " -> " + this._newValue;
	}
}