/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes;

import org.fao.fi.vrmf.common.core.behavior.other.EntityAware;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
abstract public class DateReferencedEntityRelatedVesselAttribute extends AbstractDateReferencedVesselAttribute implements EntityAware<Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1614691634142209812L;
	
	public DateReferencedEntityRelatedVesselAttribute(DateReferencedEntityRelatedVesselAttribute another) {
		super(another);
		
		this.setEntityId(another.getEntityId());
	}
}