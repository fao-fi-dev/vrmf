/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.authorizations;

import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableData;

abstract public class AbstractAuthorizationDetails extends AbstractSourcedUpdatableData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2394132079061276852L;
}