/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.behavior.vessels.attributes;

import org.fao.fi.sh.model.core.spi.Commentable;
import org.fao.fi.sh.model.core.spi.Fillable;
import org.fao.fi.sh.utility.core.annotations.data.MetadataFields;
import org.fao.fi.vrmf.common.core.behavior.SourcedUpdatable;
import org.fao.fi.vrmf.common.core.behavior.vessels.VesselLinked;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Mar 2013
 */
@MetadataFields({"updaterId", "updateDate", "sourceSystem", "comment"})
public interface VesselAttribute extends VesselLinked, SourcedUpdatable, Commentable, Fillable {
}