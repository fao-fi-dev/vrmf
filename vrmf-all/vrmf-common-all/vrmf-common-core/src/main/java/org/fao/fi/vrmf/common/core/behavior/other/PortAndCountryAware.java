/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.behavior.other;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
public interface PortAndCountryAware<COUNTRY_IDENTIFIER extends Comparable<? super COUNTRY_IDENTIFIER>, PORT_IDENTIFIER extends Comparable<? super PORT_IDENTIFIER>> extends PortAware<PORT_IDENTIFIER>, CountryAware<COUNTRY_IDENTIFIER> {
}
