/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model;

import javax.xml.bind.annotation.XmlAccessType;

import javax.xml.bind.annotation.XmlAccessorType;

import org.fao.fi.sh.model.core.spi.Exportable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 26, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 26, 2015
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class GenericData implements Exportable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1890058866788709052L;
}
