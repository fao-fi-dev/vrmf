/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes;

import java.io.Serializable;

import org.fao.fi.sh.model.core.spi.Typed;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
abstract public class DateReferencedTypeRelatedVesselAttribute<TYPE_IDENTIFIER extends Serializable> extends AbstractDateReferencedVesselAttribute implements Typed<TYPE_IDENTIFIER> {
	private static final long serialVersionUID = 1614691634142209812L;
	
	public DateReferencedTypeRelatedVesselAttribute(DateReferencedTypeRelatedVesselAttribute<TYPE_IDENTIFIER> another) {
		super(another);
		
		this.setTypeId(another.getTypeId());
	}
}