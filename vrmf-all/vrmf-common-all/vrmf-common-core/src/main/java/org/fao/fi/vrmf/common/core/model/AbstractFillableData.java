/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model;

import org.fao.fi.sh.model.core.spi.Fillable;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Mar 2013
 */
abstract public class AbstractFillableData extends GenericData implements Fillable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7615445384510270657L;

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Fillable#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return ObjectsHelper.isEmpty(this);
	}
}
