/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.behavior.vessels;

import java.util.Date;

import org.fao.fi.sh.utility.core.annotations.data.MetadataFields;

/**
 * Basic authorization data for a vessel
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Feb 2013
 */
@MetadataFields("authorizationTerminationReason")
public interface AuthorizationAware {
	String getAuthorizationTypeID();
	String getAuthorizationIssuer();
	Integer getAuthorizationIssuingCountryID();
	Date getAuthorizationStartDate();
	Date getAuthorizationEndDate();
	Date getAuthorizationTerminationDate();
	Date getAuthorizationReferenceDate();
	Integer getAuthorizationTerminationReasonCode();
	String getAuthorizationTerminationReason();
	
	void setAuthorizationTypeID(String typeID);
	void setAuthorizationIssuer(String issuer);
	void setAuthorizationIssuingCountryID(Integer issuingCountryID);
	void setAuthorizationStartDate(Date date);
	void setAuthorizationEndDate(Date date);
	void setAuthorizationTerminationDate(Date date);
	void setAuthorizationReferenceDate(Date date);
	void setAuthorizationTerminationReasonCode(Integer reasonCode);
	void setAuthorizationTerminationReason(String reason);
}
