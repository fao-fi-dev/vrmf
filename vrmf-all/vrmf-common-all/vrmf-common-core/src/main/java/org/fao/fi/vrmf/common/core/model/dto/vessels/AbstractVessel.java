/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.sh.model.core.spi.Commentable;
import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.vrmf.common.core.behavior.MappedGroupable;
import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableData;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
abstract public class AbstractVessel extends AbstractSourcedUpdatableData implements Exportable, MappedGroupable<Integer>, Commentable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5203894675705105337L;
	
	@XmlAttribute 
	@Getter @Setter private Integer id;
	
	@XmlAttribute 
	@Getter @Setter private Integer uid;
	
	@XmlAttribute
	@Getter @Setter private Integer mapsTo;
	
	@XmlAttribute
	@Getter @Setter private Double mappingWeight;
	
	@XmlAttribute
	@Getter @Setter private String mappingUser;
	
	@XmlAttribute
	@Getter @Setter private Date mappingDate;
	
	@XmlElement
	@Getter @Setter private String mappingComment;
}