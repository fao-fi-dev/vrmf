/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes;

import org.fao.fi.sh.model.core.spi.Identified;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Mar 2013
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
abstract public class DateReferencedIdentifiedVesselAttribute extends AbstractDateReferencedVesselAttribute implements Identified<Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4324232855166882777L;
}
