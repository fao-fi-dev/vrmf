/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.entities;

import org.fao.fi.sh.model.core.spi.ComplexNameAware;
import org.fao.fi.sh.model.core.spi.Fillable;
import org.fao.fi.vrmf.common.core.behavior.MappedGroupable;
import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableFillableData;

//@NonSerializedFields(fieldNames={"mapsTo", "mappingWeight", "mappingUser", "mappingDate", "mappingComment"})
abstract public class AbstractEntity extends AbstractSourcedUpdatableFillableData implements MappedGroupable<Integer>, ComplexNameAware, Fillable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8051092659508681487L;
}