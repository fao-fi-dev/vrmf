/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes;

import org.fao.fi.sh.model.core.spi.Timeframed;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Mar 2013
 */
abstract public class DateReferencedIdentifiedTimeframedVesselAttribute extends DateReferencedIdentifiedVesselAttribute implements Timeframed {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4324232855166882777L;
}
