/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes.typed;

import org.fao.fi.vrmf.common.core.model.dto.vessels.attributes.DateReferencedValuedVesselAttribute;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
abstract public class DateReferencedIntegerValuedVesselAttribute extends DateReferencedValuedVesselAttribute<Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3876892758661720080L;

	public DateReferencedIntegerValuedVesselAttribute() {
		super();
	}	
	
	/**
	 * Class constructor
	 *
	 * @param another
	 */
	public DateReferencedIntegerValuedVesselAttribute(DateReferencedValuedVesselAttribute<Integer> another) {
		super(another);
	}
	
//	/* (non-Javadoc)
//	 * @see org.fao.vrmf.utilities.common.models.behaviours.vessel.VesselAttribute#isEmpty()
//	 */
//	@Override
//	public boolean isEmpty() {
//		return this.getValue() == null;
//	}
}