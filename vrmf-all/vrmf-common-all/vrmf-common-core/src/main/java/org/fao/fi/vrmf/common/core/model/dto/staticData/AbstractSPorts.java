/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

import org.fao.fi.sh.model.core.spi.Commentable;
import org.fao.fi.sh.model.core.spi.ComplexNameAware;
import org.fao.fi.vrmf.common.core.behavior.MappedGroupable;
import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableFillableData;

//@HiddenFields({"countryId", "subdivisionId"})
abstract public class AbstractSPorts extends AbstractSourcedUpdatableFillableData implements MappedGroupable<Integer>, ComplexNameAware, Commentable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3008994769539398742L;
}