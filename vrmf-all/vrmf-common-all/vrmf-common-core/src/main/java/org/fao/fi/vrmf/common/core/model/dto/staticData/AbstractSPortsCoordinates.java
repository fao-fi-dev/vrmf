/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

import org.fao.fi.vrmf.common.core.behavior.other.PortAware;
import org.fao.fi.vrmf.common.core.model.AbstractDateReferencedSourcedUpdatableFillableData;

abstract public class AbstractSPortsCoordinates extends AbstractDateReferencedSourcedUpdatableFillableData implements PortAware<Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4849284883858529181L;
}