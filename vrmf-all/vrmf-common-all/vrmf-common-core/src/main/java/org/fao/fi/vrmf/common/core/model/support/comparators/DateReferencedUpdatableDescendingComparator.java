/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.support.comparators;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/giu/2010   ffiorellato     Creation.
 *
 * @version 1.0
 * @since 28/giu/2010
 */
final public class DateReferencedUpdatableDescendingComparator extends DateReferencedUpdatableComparator {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1808635571262509854L;

	/**
	 * Class constructor.
	 */
	public DateReferencedUpdatableDescendingComparator() {
		super(false);
	}
}
