/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes;

import org.fao.fi.vrmf.common.core.behavior.other.PortAndCountryAware;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
abstract public class DateReferencedPortRelatedVesselAttribute extends DateReferencedCountryRelatedVesselAttribute implements PortAndCountryAware<Integer, Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1614691634142209812L;
	
	public DateReferencedPortRelatedVesselAttribute() {
		super();
	}	
	
	public DateReferencedPortRelatedVesselAttribute(DateReferencedPortRelatedVesselAttribute another) {
		super(another);
		
		this.setPortId(another.getPortId());
	}
	
//	/* (non-Javadoc)
//	 * @see org.fao.vrmf.utilities.common.models.behaviours.vessel.VesselAttribute#isEmpty()
//	 */
//	@Override
//	public boolean isEmpty() {
//		return super.isEmpty() && this.getPortId() == null;
//	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.behaviours.VesselAttribute#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.getPortId() == null) ? 0 : this.getPortId().hashCode());
		return result;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(this.getClass().isAssignableFrom(obj.getClass())))
			return false;
		DateReferencedPortRelatedVesselAttribute other = (DateReferencedPortRelatedVesselAttribute) obj;		
		if (this.getPortId() == null) {
			if (other.getPortId() != null)
				return false;
		} else if (!this.getPortId().equals(other.getPortId()))
			return false;
		return true;
	}			
}