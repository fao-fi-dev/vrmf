/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes;

import java.io.Serializable;

import org.fao.fi.sh.model.core.spi.Valued;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
abstract public class ValuedTypeRelatedVesselAttribute<VALUE extends Number, TYPE_IDENTIFIER extends Serializable> extends TypeRelatedVesselAttribute<TYPE_IDENTIFIER> implements Valued<VALUE> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1614691634142209812L;
	
	public ValuedTypeRelatedVesselAttribute() {
		super();
	}	
	
	public ValuedTypeRelatedVesselAttribute(ValuedTypeRelatedVesselAttribute<VALUE, TYPE_IDENTIFIER> another) {
		super(another);
		
		this.setValue(another.getValue());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.getValue() == null) ? 0 : this.getValue().hashCode());
		return result;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(this.getClass().isAssignableFrom(obj.getClass())))
			return false;
		@SuppressWarnings("unchecked")
		ValuedTypeRelatedVesselAttribute<VALUE, TYPE_IDENTIFIER> other = (ValuedTypeRelatedVesselAttribute<VALUE, TYPE_IDENTIFIER>) obj;		
		if (this.getValue() == null) {
			if (other.getValue() != null)
				return false;
		} else if (!this.getValue().equals(other.getValue()))
			return false;
		return true;
	}		
}