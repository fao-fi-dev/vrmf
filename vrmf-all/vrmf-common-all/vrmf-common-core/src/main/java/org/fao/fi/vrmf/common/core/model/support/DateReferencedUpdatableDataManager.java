/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.support;

import org.fao.fi.vrmf.common.core.behavior.DateReferencedUpdatable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2010
 */
public class DateReferencedUpdatableDataManager<DATA extends DateReferencedUpdatable> extends DateReferencedDataManager<DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1972850699990973096L;
}