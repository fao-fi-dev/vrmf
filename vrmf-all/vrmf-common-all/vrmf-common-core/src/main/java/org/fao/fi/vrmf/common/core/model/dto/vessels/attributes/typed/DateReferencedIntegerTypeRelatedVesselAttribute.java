/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes.typed;

import org.fao.fi.vrmf.common.core.model.dto.vessels.attributes.DateReferencedTypeRelatedVesselAttribute;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
abstract public class DateReferencedIntegerTypeRelatedVesselAttribute extends DateReferencedTypeRelatedVesselAttribute<Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1614691634142209812L;

	public DateReferencedIntegerTypeRelatedVesselAttribute() {
		super();
	}		
	
	/**
	 * Class constructor
	 *
	 * @param another
	 */
	public DateReferencedIntegerTypeRelatedVesselAttribute(DateReferencedTypeRelatedVesselAttribute<Integer> another) {
		super(another);
	}
}