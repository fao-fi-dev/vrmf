/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

import org.fao.fi.sh.model.core.spi.Identified;
import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableData;

abstract public class AbstractSVesselStatus extends AbstractSourcedUpdatableData implements Identified<String> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -9102444161129804945L;
}