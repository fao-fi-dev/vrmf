/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.staticData;

import org.fao.fi.sh.model.core.spi.Commentable;
import org.fao.fi.sh.model.core.spi.Identified;
import org.fao.fi.sh.model.core.spi.Named;
import org.fao.fi.sh.model.core.spi.Typed;
import org.fao.fi.vrmf.common.core.behavior.other.CountryAware;
import org.fao.fi.vrmf.common.core.model.AbstractSourcedUpdatableFillableData;

abstract public class AbstractSCountriesSubdivision extends AbstractSourcedUpdatableFillableData implements Identified<String>, Typed<Integer>, Named, CountryAware<Integer>, Commentable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -843105503072585278L;
}