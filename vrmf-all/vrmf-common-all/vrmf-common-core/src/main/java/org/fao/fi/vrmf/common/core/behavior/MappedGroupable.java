/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.behavior;

import java.util.Date;

import org.fao.fi.sh.model.core.spi.Groupable;
import org.fao.fi.sh.utility.topology.behaviours.MappedData;
import org.fao.fi.sh.utility.topology.behaviours.WeightValue;
import org.fao.fi.sh.utility.topology.impl.SimpleWeightValue;

/**
 * Interface listing the common methods to mapped data object.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/giu/2010   ffiorellato     Creation.
 *
 * @version 1.0
 * @since 28/giu/2010
 */
public interface MappedGroupable<ID> extends Groupable<ID>, MappedData<ID> {
	ID getMapsTo();
	void setMapsTo(ID mapsTo);
	
	Double getMappingWeight();
	void setMappingWeight(Double mappingWeight);
	
	String getMappingUser();
	void setMappingUser(String mappingUser);
	
	Date getMappingDate();
	void setMappingDate(Date mappingDate);
	
	String getMappingComment();
	void setMappingComment(String mappingComment);
	
	default ID getTargetId() { return getMapsTo(); }
	default WeightValue getWeight() { return new SimpleWeightValue(getMappingWeight()); }
}
