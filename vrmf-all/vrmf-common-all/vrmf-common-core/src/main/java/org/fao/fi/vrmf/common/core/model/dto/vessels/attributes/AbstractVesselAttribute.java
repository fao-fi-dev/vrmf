/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.vrmf.common.core.behavior.vessels.attributes.VesselAttribute;
import org.fao.fi.vrmf.common.core.model.AbstractFillableData;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
@XmlType @XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @EqualsAndHashCode(callSuper=true) @Getter @Setter 
abstract public class AbstractVesselAttribute extends AbstractFillableData implements VesselAttribute {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6574880118045132378L;
	
	private @XmlAttribute Integer vesselId;
	private @XmlAttribute Integer vesselUid;
	private @XmlAttribute Date updateDate;
	private @XmlAttribute String updaterId;
	private @XmlAttribute String sourceSystem;
	private @XmlElement String comment;
	
	public AbstractVesselAttribute(AbstractVesselAttribute another) {
		this.setVesselId(another.getVesselId());
		this.setVesselUid(another.getVesselUid());
		this.setUpdaterId(another.getUpdaterId());
		this.setUpdateDate(another.getUpdateDate());
		this.setComment(another.getComment());
		this.setSourceSystem(another.getSourceSystem());
	}
}