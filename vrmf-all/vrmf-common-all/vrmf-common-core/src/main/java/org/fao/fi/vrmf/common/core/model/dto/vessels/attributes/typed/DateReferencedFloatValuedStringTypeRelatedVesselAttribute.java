/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-core)
 */
package org.fao.fi.vrmf.common.core.model.dto.vessels.attributes.typed;

import org.fao.fi.vrmf.common.core.model.dto.vessels.attributes.DateReferencedValuedTypeRelatedVesselAttribute;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
abstract public class DateReferencedFloatValuedStringTypeRelatedVesselAttribute extends DateReferencedValuedTypeRelatedVesselAttribute<Float, String> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3876892758661720080L;

	public DateReferencedFloatValuedStringTypeRelatedVesselAttribute() {
		super();
	}		
	
	/**
	 * Class constructor
	 *
	 * @param another
	 */
	public DateReferencedFloatValuedStringTypeRelatedVesselAttribute(DateReferencedValuedTypeRelatedVesselAttribute<Float, String> another) {
		super(another);
	}
}