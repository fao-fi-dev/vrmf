/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import org.fao.fi.vrmf.common.models.generated.SPorts;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Oct 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Oct 2010
 */
@Deprecated
final public class PortOfRegistry extends SPorts {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4647892357340109529L;
	
	private Double coordinatesDistance;
	private int registeredVessels;

	/**
	 * Class constructor
	 *
	 */
	public PortOfRegistry() {
		super();
	}

	/**
	 * @return the 'registeredVessels' value
	 */
	public int getRegisteredVessels() {
		return this.registeredVessels;
	}

	/**
	 * @param registeredVessels the 'registeredVessels' value to set
	 */
	public void setRegisteredVessels(int registeredVessels) {
		this.registeredVessels = registeredVessels;
	}

	/**
	 * @return the 'coordinatesDistance' value
	 */
	public Double getCoordinatesDistance() {
		return this.coordinatesDistance;
	}

	/**
	 * @param coordinatesDistance the 'coordinatesDistance' value to set
	 */
	public void setCoordinatesDistance(Double coordinatesDistance) {
		this.coordinatesDistance = coordinatesDistance;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.coordinatesDistance == null) ? 0 : this.coordinatesDistance.hashCode());
		result = prime * result + this.registeredVessels;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PortOfRegistry other = (PortOfRegistry) obj;
		if (this.coordinatesDistance == null) {
			if (other.coordinatesDistance != null)
				return false;
		} else if (!this.coordinatesDistance.equals(other.coordinatesDistance))
			return false;
		if (this.registeredVessels != other.registeredVessels)
			return false;
		return true;
	}
}