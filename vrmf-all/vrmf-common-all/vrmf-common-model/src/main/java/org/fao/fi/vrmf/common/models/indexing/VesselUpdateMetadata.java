/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.indexing;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Oct 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Oct 2012
 */
public class VesselUpdateMetadata extends UpdateDateAndReferenceDateHolder {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8938243266120623293L;
	
	private Integer _identifier;

	/**
	 * @return the 'identifier' value
	 */
	public Integer getIdentifier() {
		return this._identifier;
	}

	/**
	 * @param identifier the 'identifier' value to set
	 */
	public void setIdentifier(Integer identifier) {
		this._identifier = identifier;
	}
}