/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.behavior.security;

import java.util.Collection;

import org.fao.fi.vrmf.common.models.generated.UsersToManagedCountries;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 Nov 2011
 */
public interface CountryManagerUser extends SupervisedUser {
	Collection<UsersToManagedCountries> getManagedCountries();

	boolean canManage(String[] sourceSystems, int countryId);
	
	Collection<Integer> getManagedCountriesBySystem(String sourceSystem);
	Collection<String> getManagedSystemsByCountry(int countryId);
	
	boolean managesCountriesBySystem(String sourceSystem);
}
