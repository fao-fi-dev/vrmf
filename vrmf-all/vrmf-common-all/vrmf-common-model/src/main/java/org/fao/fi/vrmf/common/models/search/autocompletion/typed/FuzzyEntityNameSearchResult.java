/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion.typed;

import org.fao.fi.vrmf.common.models.search.autocompletion.FuzzySmartNamedSearchResult;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Oct 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Oct 2011
 */
public class FuzzyEntityNameSearchResult extends FuzzySmartNamedSearchResult {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3398520370995516663L;
	
	private Integer _entityID;
	private String _entityTypeID;
	private String _branch;
	private String _salutation;
	private String _role;
	private Integer _countryID;
	private String _address;
	private String _city;
	private String _zipCode;

	/**
	 * Class constructor
	 *
	 */
	public FuzzyEntityNameSearchResult() {
		super();
	}
	
	/**
	 * @return the 'entityID' value
	 */
	public Integer getEntityID() {
		return this._entityID;
	}

	/**
	 * @param entityID the 'entityID' value to set
	 */
	public void setEntityID(Integer entityID) {
		this._entityID = entityID;
	}
 
	/**
	 * @return the 'entityTypeID' value
	 */
	public String getEntityTypeID() {
		return this._entityTypeID;
	}

	/**
	 * @param entityTypeID the 'entityTypeID' value to set
	 */
	public void setEntityTypeID(String entityTypeID) {
		this._entityTypeID = entityTypeID;
	}

	/**
	 * @return the 'branch' value
	 */
	public String getBranch() {
		return this._branch;
	}

	/**
	 * @param branch the 'branch' value to set
	 */
	public void setBranch(String branch) {
		this._branch = branch;
	}

	/**
	 * @return the 'salutation' value
	 */
	public String getSalutation() {
		return this._salutation;
	}

	/**
	 * @param salutation the 'salutation' value to set
	 */
	public void setSalutation(String salutation) {
		this._salutation = salutation;
	}

	/**
	 * @return the 'role' value
	 */
	public String getRole() {
		return this._role;
	}

	/**
	 * @param role the 'role' value to set
	 */
	public void setRole(String role) {
		this._role = role;
	}

	/**
	 * @return the 'countryID' value
	 */
	public Integer getCountryID() {
		return this._countryID;
	}

	/**
	 * @param countryID the 'countryID' value to set
	 */
	public void setCountryID(Integer countryID) {
		this._countryID = countryID;
	}

	/**
	 * @return the 'address' value
	 */
	public String getAddress() {
		return this._address;
	}

	/**
	 * @param address the 'address' value to set
	 */
	public void setAddress(String address) {
		this._address = address;
	}

	/**
	 * @return the 'city' value
	 */
	public String getCity() {
		return this._city;
	}

	/**
	 * @param city the 'city' value to set
	 */
	public void setCity(String city) {
		this._city = city;
	}

	/**
	 * @return the 'zipCode' value
	 */
	public String getZipCode() {
		return this._zipCode;
	}

	/**
	 * @param zipCode the 'zipCode' value to set
	 */
	public void setZipCode(String zipCode) {
		this._zipCode = zipCode;
	}
}