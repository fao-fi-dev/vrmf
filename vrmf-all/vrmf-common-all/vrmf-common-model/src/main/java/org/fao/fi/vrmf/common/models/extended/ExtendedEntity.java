/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.vrmf.common.models.generated.Entity;
import org.fao.fi.vrmf.common.models.generated.SCountries;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Nov 2011
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public class ExtendedEntity extends Entity {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7482943738965222173L;
	
	@XmlElement
	@Getter @Setter	private SCountries nationality;
	
	@XmlElement
	@Getter @Setter private SCountries country;
	
	/**
	 * Class constructor
	 *
	 * @param source
	 * @param shipbuilder
	 */
	public ExtendedEntity(Entity source, SCountries nationality, SCountries country) {
		this.nationality = nationality;
		this.country = country;

		this.setId(source.getId());
		this.setUid(source.getUid());
		
		this.setMapsTo(source.getMapsTo());
		this.setMappingWeight(source.getMappingWeight());
		this.setMappingDate(source.getMappingDate());
		this.setMappingUser(source.getMappingUser());
		this.setMappingComment(source.getMappingComment());
		
		this.setEntityTypeId(source.getEntityTypeId());
		
		this.setSalutation(source.getSalutation());
		this.setBranch(source.getBranch());
		this.setRole(source.getRole());

		this.setName(source.getName());
		this.setSimplifiedName(source.getSimplifiedName());
		this.setSimplifiedNameSoundex(source.getSimplifiedNameSoundex());
		
		this.setNationalityId(source.getNationalityId());
		
		this.setAddress(source.getAddress());
		this.setCity(source.getCity());
		this.setCountryId(source.getCountryId());
		this.setZipCode(source.getZipCode());
		this.setPhoneNumber(source.getPhoneNumber());
		this.setFaxNumber(source.getFaxNumber());
		this.setMobileNumber(source.getMobileNumber());
		this.seteMail(source.geteMail());
		this.setWebsite(source.getWebsite());
		
		this.setSourceSystem(source.getSourceSystem());
		this.setUpdaterId(source.getUpdaterId());
		this.setUpdateDate(source.getUpdateDate());
		this.setComment(source.getComment());
	}
}