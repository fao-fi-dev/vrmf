/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.stats;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Oct 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Oct 2011
 */
public class BasicVesselStatsReport extends GenericData implements VesselStatsReport {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3868716796920103725L;
	
	private Integer _vessels;
	
	/**
	 * Class constructor
	 *
	 */
	public BasicVesselStatsReport() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param vessels
	 */
	public BasicVesselStatsReport(Integer vessels) {
		super();
		this._vessels = vessels;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.stats.VesselStatsReport#getVessels()
	 */
	@Override
	public Integer getVessels() {
		return this._vessels;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.stats.VesselStatsReport#setVessels(java.lang.Integer)
	 */
	@Override
	public void setVessels(Integer vessels) {
		this._vessels = vessels;
	}
}