/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.stats;

import java.util.Date;

import org.fao.fi.vrmf.common.models.generated.SAgreementContributingParties;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Sep 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Sep 2011
 */
public class HSVARReport extends SAgreementContributingParties {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6300153403953978973L;
	
	private String _countryName;
	private Date _lastUpdate;
	private Integer _vessels;
	
	/**
	 * Class constructor
	 *
	 */
	public HSVARReport() {
		super();
	}
	
	/**
	 * @return the 'countryName' value
	 */
	public String getCountryName() {
		return this._countryName;
	}
	
	/**
	 * @param countryName the 'countryName' value to set
	 */
	public void setCountryName(String countryName) {
		this._countryName = countryName;
	}
		
	/**
	 * @return the 'lastUpdate' value
	 */
	public Date getLastUpdate() {
		return this._lastUpdate;
	}
	
	/**
	 * @param lastUpdate the 'lastUpdate' value to set
	 */
	public void setLastUpdate(Date lastUpdate) {
		this._lastUpdate = lastUpdate;
	}

	/**
	 * @return the 'vessels' value
	 */
	public Integer getVessels() {
		return this._vessels;
	}

	/**
	 * @param vessels the 'vessels' value to set
	 */
	public void setVessels(Integer vessels) {
		this._vessels = vessels;
	}	
}