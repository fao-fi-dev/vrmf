/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.indexing;

import java.util.Date;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Oct 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Oct 2012
 */
public class UpdateDateAndReferenceDateHolder extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2037711202997046907L;
	
	private Date _updateDate;
	private Date _referenceDate;
	
	/**
	 * @return the 'updateDate' value
	 */
	public Date getUpdateDate() {
		return this._updateDate;
	}
	
	/**
	 * @param updateDate the 'updateDate' value to set
	 */
	public void setUpdateDate(Date updateDate) {
		this._updateDate = updateDate;
	}
	
	/**
	 * @return the 'referenceDate' value
	 */
	public Date getReferenceDate() {
		return this._referenceDate;
	}
	
	/**
	 * @param referenceDate the 'referenceDate' value to set
	 */
	public void setReferenceDate(Date referenceDate) {
		this._referenceDate = referenceDate;
	}
	
	public Date getMaximumDate() {
		if(this._updateDate == null && this._referenceDate == null)
			return new Date();
		
		long now = System.currentTimeMillis();
		
		return this._updateDate == null 
				? new Date(Math.min(this._referenceDate.getTime(), now))  
				: this._referenceDate == null 
					? new Date(Math.min(this._updateDate.getTime(), now)) 
					: new Date(Math.min(Math.max(this._referenceDate.getTime(), this._updateDate.getTime()), now)); 
	}
}