/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2010
 */
@XmlType(name="vessel")
public class BasicHSVARVessel extends BasicTimeframedAuthorizedVessel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7801806468193779051L;

	@XmlElement(name="authorizationIssuingCountry")
	private Integer _authIssuingCountry;
	
	@XmlElement(name="authorizationStartDate")
	private Date _authFrom;
	
	@XmlElement(name="authorizationEndDate")
	private Date _authTo;
	
	@XmlElement(name="authorizationTerminationDate")
	private Date _authTerm;
	
	/**
	 * Class constructor
	 */
	public BasicHSVARVessel() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param vessel
	 */
	public BasicHSVARVessel(BasicHSVARVessel vessel) {
		super(vessel);
	}
	
	/**
	 * Class constructor
	 *
	 * @param vessel
	 */
	public BasicHSVARVessel(BasicVessel vessel) {
		super(vessel);
	}
	
	/**
	 * Class constructor
	 *
	 * @param vessel
	 */
	public BasicHSVARVessel(ExtendedVessel vessel) {
		super(vessel);
	}

	/**
	 * @return the 'authFrom' value
	 */
	public Date getAuthFrom() {
		return this._authFrom;
	}

	/**
	 * @param authFrom the 'authFrom' value to set
	 */
	public void setAuthFrom(Date authFrom) {
		this._authFrom = authFrom;
	}

	/**
	 * @return the 'authTo' value
	 */
	public Date getAuthTo() {
		return this._authTo;
	}

	/**
	 * @param authTo the 'authTo' value to set
	 */
	public void setAuthTo(Date authTo) {
		this._authTo = authTo;
	}

	/**
	 * @return the 'authTerm' value
	 */
	public Date getAuthTerm() {
		return this._authTerm;
	}

	/**
	 * @param authTerm the 'authTerm' value to set
	 */
	public void setAuthTerm(Date authTerm) {
		this._authTerm = authTerm;
	}

	/**
	 * @return the 'authIssuingCountry' value
	 */
	public Integer getAuthIssuingCountry() {
		return this._authIssuingCountry;
	}

	/**
	 * @param authIssuingCountry the 'authIssuingCountry' value to set
	 */
	public void setAuthIssuingCountry(Integer authIssuingCountry) {
		this._authIssuingCountry = authIssuingCountry;
	}
}