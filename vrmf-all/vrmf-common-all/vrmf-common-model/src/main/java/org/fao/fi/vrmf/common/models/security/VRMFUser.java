/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.behavior.security.ManagerUser;
import org.fao.fi.vrmf.common.models.generated.Entity;
import org.fao.fi.vrmf.common.models.generated.UsersToManagedCountries;
import org.fao.fi.vrmf.common.models.generated.UsersToManagedSources;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 Nov 2011
 */
public class VRMFUser extends BasicUser implements ManagerUser {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6884804907449985495L;
	
	private Collection<UsersToManagedCountries> _managedCountries;
	private Map<String, List<Integer>> _managedCountriesBySystem;
	private Map<Integer, List<String>> _managedSystemsByCountry;
	
	private Collection<UsersToManagedSources> _managedSources;
	private Map<String, List<String>> _managedSourcesBySystem;
	private Map<String, List<String>> _managedSystemsBySource;
	
	private Entity _userEntity;
	private Entity _granterEntity;
	
	/**
	 * Class constructor
	 *
	 * @param id
	 */
	public VRMFUser(String id) {
		this(id, null, null, null, null);
	}
		
	/**
	 * Class constructor
	 *
	 * @param id
	 * @param roles
	 * @param capabilities
	 */
	public VRMFUser(String id, Collection<UserRole> roles, Collection<UserCapability> capabilities, Collection<UsersToManagedCountries> managedCountries, Collection<UsersToManagedSources> managedSources) {
		super(id, roles, capabilities);
		
		this._managedCountries = new ListSet<UsersToManagedCountries>(managedCountries);
		this._managedSources = new ListSet<UsersToManagedSources>(managedSources);
		this.initializeMaps();
	}
	
	private void initializeMaps() {
		this._managedCountriesBySystem = new HashMap<String, List<Integer>>();
		this._managedSystemsByCountry = new HashMap<Integer, List<String>>();
		this._managedSourcesBySystem = new HashMap<String, List<String>>();
		this._managedSystemsBySource = new HashMap<String, List<String>>();
		
		if(this._managedCountries != null) {
			List<Integer> countries;
			List<String> systems;
			for(UsersToManagedCountries current : this._managedCountries) {
				systems = this._managedSystemsByCountry.get(current.getCountryId());
				countries = this._managedCountriesBySystem.get(current.getSourceSystem());
				
				if(systems == null) {
					systems = new ListSet<String>();
					this._managedSystemsByCountry.put(current.getCountryId(), systems);
				}
				
				if(countries == null) {
					countries = new ListSet<Integer>();
					this._managedCountriesBySystem.put(current.getSourceSystem(), countries);
				}
				
				systems.add(current.getSourceSystem());
				countries.add(current.getCountryId());
			}
		}
		
		if(this._managedSources != null) {
			List<String> sources;
			List<String> systems;
			for(UsersToManagedSources current : this._managedSources) {
				systems = this._managedSystemsBySource.get(current.getManagedSource());
				sources = this._managedSourcesBySystem.get(current.getSourceSystem());
				
				if(systems == null) {
					systems = new ListSet<String>();
					this._managedSystemsBySource.put(current.getManagedSource(), systems);
				}
				
				if(sources == null) {
					sources = new ListSet<String>();
					this._managedSourcesBySystem.put(current.getSourceSystem(), sources);
				}
				
				systems.add(current.getSourceSystem());
				sources.add(current.getManagedSource());
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.CountryManagerUser#getUserEntity()
	 */
	public Entity getUserEntity() {
		return this._userEntity;
	}

	/**
	 * @param userEntity the 'userEntity' value to set
	 */
	public void setUserEntity(Entity userEntity) {
		this._userEntity = userEntity;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.CountryManagerUser#getGranterEntity()
	 */
	public Entity getGranterEntity() {
		return this._granterEntity;
	}

	/**
	 * @param granterEntity the 'granterEntity' value to set
	 */
	public void setGranterEntity(Entity granterEntity) {
		this._granterEntity = granterEntity;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.CountryManagerUser#getManagedCountries()
	 */
	@Override
	public Collection<UsersToManagedCountries> getManagedCountries() {
		return this._managedCountries;
	}

	/**
	 * @param managedCountries the 'managedCountries' value to set
	 */
	public void setManagedCountries(Collection<UsersToManagedCountries> managedCountries) {
		this._managedCountries = new ListSet<UsersToManagedCountries>(managedCountries);
		
		this.initializeMaps();
	}
		
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.CountryManagerUser#canManage(java.lang.String[], int)
	 */
	public boolean canManage(String[] sourceSystems, int countryId) {
		List<Integer> slot = null;
		
		for(String sourceSystem : sourceSystems) {
			slot = this._managedCountriesBySystem.get(sourceSystem);
			
			if(slot != null && slot.contains(countryId))
				return true;
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.CountryManagerUser#managedCountries(java.lang.String)
	 */
	@Override
	public List<Integer> getManagedCountriesBySystem(String sourceSystem) {
		List<Integer> slot = this._managedCountriesBySystem.get(sourceSystem);
		
		return slot == null ? new ArrayList<Integer>() : slot;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.CountryManagerUser#managedSystemsByCountry(int)
	 */
	@Override
	public List<String> getManagedSystemsByCountry(int countryId) {
		List<String> slot = this._managedSystemsByCountry.get(countryId);
		
		return slot == null ? new ArrayList<String>() : slot;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.CountryManagerUser#managesCountriesBySystem(java.lang.String)
	 */
	@Override
	public boolean managesCountriesBySystem(String sourceSystem) {
		return !this.getManagedCountriesBySystem(sourceSystem).isEmpty();
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.SourcesManagerUser#getManagedSources()
	 */
	@Override
	public Collection<UsersToManagedSources> getManagedSources() {
		return this._managedSources;
	}
	
	/**
	 * @param managedSources the 'managedSources' value to set
	 */
	public void setManagedSources(Collection<UsersToManagedSources> managedSources) {
		this._managedSources = managedSources;
		
		this.initializeMaps();
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.SourcesManagerUser#canManage(java.lang.String[], java.lang.String)
	 */
	@Override
	public boolean canManage(String[] sourceSystems, String sourceId) {
		List<String> slot = null;
		
		for(String sourceSystem : sourceSystems) {
			slot = this._managedSourcesBySystem.get(sourceSystem);
			
			if(slot != null && slot.contains(sourceId))
				return true;
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.SourcesManagerUser#getManagedSourcesBySystem(java.lang.String)
	 */
	@Override
	public Collection<String> getManagedSourcesBySystem(String sourceSystem) {
		List<String> slot = this._managedSourcesBySystem.get(sourceSystem);
		
		return slot == null ? new ArrayList<String>() : slot;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.SourcesManagerUser#getManagedSystemsBySource(java.lang.String)
	 */
	@Override
	public Collection<String> getManagedSystemsBySource(String sourceId) {
		List<String> slot = this._managedSystemsBySource.get(sourceId);
		
		return slot == null ? new ArrayList<String>() : slot;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.SourcesManagerUser#managesSourcesBySystem(java.lang.String)
	 */
	@Override
	public boolean managesSourcesBySystem(String sourceSystem) {
		return !this.getManagedSourcesBySystem(sourceSystem).isEmpty();
	}
}