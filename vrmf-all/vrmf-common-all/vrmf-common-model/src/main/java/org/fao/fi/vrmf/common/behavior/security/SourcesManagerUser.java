/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.behavior.security;

import java.util.Collection;

import org.fao.fi.vrmf.common.models.generated.UsersToManagedSources;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 Nov 2011
 */
public interface SourcesManagerUser extends SupervisedUser {
	Collection<UsersToManagedSources> getManagedSources();

	boolean canManage(String[] sourceSystems, String sourceId);
	
	Collection<String> getManagedSourcesBySystem(String sourceSystem);
	Collection<String> getManagedSystemsBySource(String sourceId);
	
	boolean managesSourcesBySystem(String sourceSystem);
}
