/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.stats;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment 
 * ------------- --------------- -----------------------
 * 19 Oct 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Oct 2011
 */
public class CodedVesselStatsReport extends SimpleVesselStatsReport {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6620598005851832786L;

	private Integer _typeId;
	private String _originalId;
	private String _code;
	private String _abbreviation;
	private String _name;
	
	/**
	 * Class constructor
	 *
	 */
	public CodedVesselStatsReport() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param typeId
	 * @param originalId
	 * @param code
	 * @param abbreviation
	 * @param name
	 * @param description
	 * @param vessels
	 */
	public CodedVesselStatsReport(Integer typeId, String originalId, String code, String abbreviation, String name, String description, Integer vessels) {
		super(description, vessels);
		this._typeId = typeId;
		this._originalId = originalId;
		this._code = code;
		this._abbreviation = abbreviation;
		this._name = name;
	}

	/**
	 * @return the 'typeId' value
	 */
	public Integer getTypeId() {
		return this._typeId;
	}

	/**
	 * @param typeId the 'typeId' value to set
	 */
	public void setTypeId(Integer typeId) {
		this._typeId = typeId;
	}

	/**
	 * @return the 'code' value
	 */
	public String getCode() {
		return this._code;
	}

	/**
	 * @param code the 'code' value to set
	 */
	public void setCode(String code) {
		this._code = code;
	}

	/**
	 * @return the 'abbreviation' value
	 */
	public String getAbbreviation() {
		return this._abbreviation;
	}

	/**
	 * @param abbreviation the 'abbreviation' value to set
	 */
	public void setAbbreviation(String abbreviation) {
		this._abbreviation = abbreviation;
	}

	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return this._name;
	}

	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		this._name = name;
	}

	/**
	 * @return the 'originalId' value
	 */
	public String getOriginalId() {
		return this._originalId;
	}

	/**
	 * @param originalId the 'originalId' value to set
	 */
	public void setOriginalId(String originalId) {
		this._originalId = originalId;
	}
}