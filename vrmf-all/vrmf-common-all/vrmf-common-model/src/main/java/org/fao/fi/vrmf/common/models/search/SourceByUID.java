/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Jun 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Jun 2012
 */
public class SourceByUID extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8379236254112629055L;
	
	private Integer _uid;
	private String _sourceSystem;
	
	/**
	 * @return the 'uid' value
	 */
	public Integer getUid() {
		return this._uid;
	}
	
	/**
	 * @param uid the 'uid' value to set
	 */
	public void setUid(Integer uid) {
		this._uid = uid;
	}
	
	/**
	 * @return the 'sourceSystem' value
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}
	
	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}
}