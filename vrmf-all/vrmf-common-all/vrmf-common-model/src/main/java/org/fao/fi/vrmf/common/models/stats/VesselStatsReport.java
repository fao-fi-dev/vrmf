/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.stats;

import java.io.Serializable;

import org.fao.fi.sh.model.core.spi.Exportable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Oct 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Oct 2011
 */
public interface VesselStatsReport extends Serializable, Exportable {
	/**
	 * @return the 'vessels' value
	 */
	Integer getVessels();

	/**
	 * @param vessels the 'vessels' value to set
	 */
	void setVessels(Integer vessels);
}