/**
 * (c) 2015 FAO / UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 4, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 4, 2015
 */
@XmlType(name="valueRange")
abstract public class AbstractValueRange<TYPE extends Number> extends GenericData implements ValueRange<TYPE> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 27338177729940140L;
	
	@XmlAttribute(name="min") private TYPE _min;
	@XmlAttribute(name="max") private TYPE _max;
	
	/**
	 * Class constructor
	 *
	 */
	public AbstractValueRange() {
		super(); // TODO Auto-generated constructor block
	}
	
	/**
	 * Class constructor
	 *
	 * @param min
	 * @param max
	 */
	public AbstractValueRange(TYPE min, TYPE max) {
		super();
		
		_min = min;
		_max = max;
	}

	/**
	 * @return the 'min' value
	 */
	public TYPE getMin() {
		return _min;
	}

	/**
	 * @param min the 'min' value to set
	 */
	public void setMin(TYPE min) {
		_min = min;
	}

	/**
	 * @return the 'max' value
	 */
	public TYPE getMax() {
		return _max;
	}

	/**
	 * @param max the 'max' value to set
	 */
	public void setMax(TYPE max) {
		_max = max;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _max == null ) ? 0 : _max.hashCode() );
		result = prime * result + ( ( _min == null ) ? 0 : _min.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		AbstractValueRange other = (AbstractValueRange) obj;
		if(_max == null) {
			if(other._max != null) return false;
		} else if(!_max.equals(other._max)) return false;
		if(_min == null) {
			if(other._min != null) return false;
		} else if(!_min.equals(other._min)) return false;
		return true;
	}
}
