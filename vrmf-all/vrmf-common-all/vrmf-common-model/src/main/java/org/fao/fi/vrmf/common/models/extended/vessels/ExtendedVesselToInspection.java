/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended.vessels;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.sh.utility.common.helpers.singletons.lang.BeansHelper;
import org.fao.fi.vrmf.common.models.generated.SAuthorityRole;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SInspectionInfringementType;
import org.fao.fi.vrmf.common.models.generated.SInspectionOutcomeType;
import org.fao.fi.vrmf.common.models.generated.SInspectionReportType;
import org.fao.fi.vrmf.common.models.generated.VesselsToInspections;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Dec 2010
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public class ExtendedVesselToInspection extends VesselsToInspections {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8087583632648739615L;
	
	@XmlElement
	@Getter @Setter private SInspectionReportType reportType;
	
	@XmlElement
	@Getter @Setter private SCountries informingCountry;
	
	@XmlElement
	@Getter @Setter private SAuthorityRole authorityRole;
	
	@XmlElement
	@Getter @Setter private SInspectionInfringementType infringementType;
	
	@XmlElement
	@Getter @Setter private SInspectionOutcomeType outcomeType;
	
	/**
	 * Class constructor
	 *
	 * @param source
	 * @param owner
	 */
	public ExtendedVesselToInspection(VesselsToInspections source, 
									  SInspectionReportType reportType, 
									  SCountries informingCountry, 
									  SAuthorityRole authorityRole,
									  SInspectionInfringementType infringementType, 
									  SInspectionOutcomeType outcomeType) {
		super();
		
		BeansHelper.transferBean(source, this);
		
		this.reportType = reportType;
		this.informingCountry = informingCountry;
		this.authorityRole = authorityRole;
		this.infringementType = infringementType;
		this.outcomeType = outcomeType;
	}
}