/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion.typed;

import org.fao.fi.vrmf.common.models.search.autocompletion.SimpleGroupedSearchResult;



/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Jan 2011
 */
public final class IRCSSearchResult extends SimpleGroupedSearchResult {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5487439384483848917L;

	/**
	 * Class constructor
	 *
	 */
	public IRCSSearchResult() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param IRCS
	 * @param totalOccurrencies
	 * @param occurrencies
	 */
	public IRCSSearchResult(String IRCS, int totalOccurrencies, int occurrencies, int groupedOccurrencies) {
		super(IRCS, totalOccurrencies, occurrencies, groupedOccurrencies);
	}
	
	/**
	 * @return the 'IRCS' value
	 */
	public String getIRCS() {
		return super.getTerm();
	}
	
	/**
	 * @param IRCS the 'IRCS' value to set
	 */
	public void setIRCS(String IRCS) {
		super.setTerm(IRCS);
	}	
}