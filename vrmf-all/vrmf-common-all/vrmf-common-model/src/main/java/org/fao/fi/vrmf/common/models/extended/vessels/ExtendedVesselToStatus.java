/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended.vessels;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.sh.utility.common.helpers.singletons.lang.BeansHelper;
import org.fao.fi.vrmf.common.models.generated.SVesselStatus;
import org.fao.fi.vrmf.common.models.generated.VesselsToStatus;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Dec 2010
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public class ExtendedVesselToStatus extends VesselsToStatus {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3849406546807126599L;
	
	@XmlElement
	@Getter @Setter private SVesselStatus vesselStatus;
	
	/**
	 * Class constructor
	 *
	 * @param source
	 * @param vesselStatus
	 */
	public ExtendedVesselToStatus(VesselsToStatus source, SVesselStatus vesselStatus) {
		super();
		
		BeansHelper.transferBean(source, this);
		
		this.vesselStatus = vesselStatus;
	}
}