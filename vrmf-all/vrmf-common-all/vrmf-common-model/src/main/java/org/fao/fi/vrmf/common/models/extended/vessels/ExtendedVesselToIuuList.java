/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended.vessels;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.sh.utility.common.helpers.singletons.lang.BeansHelper;
import org.fao.fi.vrmf.common.models.generated.SRfmoIuuLists;
import org.fao.fi.vrmf.common.models.generated.VesselsToIuuList;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Dec 2010
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public class ExtendedVesselToIuuList extends VesselsToIuuList {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8087583632648739615L;
	
	@XmlElement
	@Getter @Setter	private SRfmoIuuLists iuuList;
	
	/**
	 * Class constructor
	 *
	 * @param source
	 * @param owner
	 */
	public ExtendedVesselToIuuList(VesselsToIuuList source, 
								   SRfmoIuuLists iuuList) {
		super();
		
		BeansHelper.transferBean(source, this);
		
		this.iuuList = iuuList;
	}
}