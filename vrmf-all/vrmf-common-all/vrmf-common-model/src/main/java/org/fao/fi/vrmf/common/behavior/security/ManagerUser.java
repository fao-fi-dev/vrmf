/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.behavior.security;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 May 2012
 */
public interface ManagerUser extends CountryManagerUser, SourcesManagerUser {

}
