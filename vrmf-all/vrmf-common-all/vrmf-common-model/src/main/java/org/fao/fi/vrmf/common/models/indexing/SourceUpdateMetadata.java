/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.indexing;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Oct 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Oct 2012
 */
public class SourceUpdateMetadata extends UpdateDateAndReferenceDateHolder {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8938243266120623293L;
	
	private String _sourceSystem;

	/**
	 * @return the 'sourceSystem' value
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}

	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}
}