/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.vrmf.common.models.generated.AuthorizationDetails;
import org.fao.fi.vrmf.common.models.generated.SFishingZones;
import org.fao.fi.vrmf.common.models.generated.SGearTypes;
import org.fao.fi.vrmf.common.models.generated.SSpecies;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Jan 2011
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public class ExtendedAuthorizationDetails extends AuthorizationDetails {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6027140315356804484L;
	
	@XmlElement
	@Getter @Setter private SGearTypes gearType;
	
	@XmlElement
	@Getter @Setter private SFishingZones fishingZone;

	@XmlElement
	@Getter @Setter private SSpecies species;

	public ExtendedAuthorizationDetails(AuthorizationDetails source, SGearTypes gearType, SFishingZones fishingZone, SSpecies species) {
		this.gearType = gearType;
		this.fishingZone = fishingZone;
		this.species = species;
		
		this.setAuthId(source.getAuthId());
		this.setAuthorizedGearId(source.getAuthorizedGearId());
		this.setAuthorizedSpecieId(source.getAuthorizedSpecieId());
		this.setAuthorizedZoneId(source.getAuthorizedZoneId());
		this.setComment(source.getComment());
		this.setUpdateDate(source.getUpdateDate());
		this.setUpdaterId(source.getUpdaterId());
		this.setSourceSystem(source.getSourceSystem());
	}
}
