/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion;

import org.fao.fi.sh.model.core.spi.ComplexNameAware;

public class SmartNamedDataSearchResult extends SimpleGroupedSearchResult implements ComplexNameAware {
	private static final long serialVersionUID = 4516129490822811326L;
	
	//Fields required by the 'SmartNamedData' interface
	protected String _simplifiedName;
	protected String _simplifiedNameSoundex;
	
	//Other fields
	private String _sourceSystem;
	
	public SmartNamedDataSearchResult() {
		super();
	}

	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return super.getTerm();
	}

	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		super.setTerm(name);
	}

	/**
	 * @return the 'simplifiedName' value
	 */
	public String getSimplifiedName() {
		return this._simplifiedName;
	}

	/**
	 * @param simplifiedName the 'simplifiedName' value to set
	 */
	public void setSimplifiedName(String simplifiedName) {
		this._simplifiedName = simplifiedName;
	}

	/**
	 * @return the 'simplifiedNameSoundex' value
	 */
	public String getSimplifiedNameSoundex() {
		return this._simplifiedNameSoundex;
	}

	/**
	 * @param simplifiedNameSoundex the 'simplifiedNameSoundex' value to set
	 */
	public void setSimplifiedNameSoundex(String simplifiedNameSoundex) {
		this._simplifiedNameSoundex = simplifiedNameSoundex;
	}

	/**
	 * @return the 'sourceSystem' value
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}

	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.models.behaviours.FillableData#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return this._simplifiedName == null;
	}
}