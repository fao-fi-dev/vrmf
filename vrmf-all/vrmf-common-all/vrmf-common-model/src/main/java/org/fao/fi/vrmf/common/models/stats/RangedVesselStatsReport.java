/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.stats;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Oct 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Oct 2011
 */
public class RangedVesselStatsReport extends SimpleVesselStatsReport {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6620598005851832786L;
	
	private Float _from;
	private Float _to;
	
	/**
	 * Class constructor
	 *
	 */
	public RangedVesselStatsReport() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param from
	 * @param to
	 * @param description
	 * @param vessels
	 */
	public RangedVesselStatsReport(Float from, Float to, String description, Integer vessels) {
		super(description, vessels);
		this._from = from;
		this._to = to;
	}

	/**
	 * @return the 'from' value
	 */
	public Float getFrom() {
		return this._from;
	}

	/**
	 * @param from the 'from' value to set
	 */
	public void setFrom(Float from) {
		this._from = from;
	}

	/**
	 * @return the 'to' value
	 */
	public Float getTo() {
		return this._to;
	}

	/**
	 * @param to the 'to' value to set
	 */
	public void setTo(Float to) {
		this._to = to;
	}
}