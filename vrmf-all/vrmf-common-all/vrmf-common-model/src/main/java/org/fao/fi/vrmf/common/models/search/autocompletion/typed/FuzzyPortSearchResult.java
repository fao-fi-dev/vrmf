/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion.typed;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.fao.fi.vrmf.common.models.search.autocompletion.FuzzySmartNamedSearchResult;
import org.fao.fi.vrmf.common.models.search.autocompletion.support.FuzzySearchResultDataComparator;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Nov 2010
 */
	/** Field serialVersionUID */
public class FuzzyPortSearchResult extends FuzzySmartNamedSearchResult {
	private static final long serialVersionUID = -8563623785578262107L;
	
	private Integer _id;
	private String _sourceSystem;
	private String _originalId;
	private Integer _countryId;
	private String _subdivisionId;
	private Boolean _isMapped;
	
	private List<FuzzyPortSearchResult> _mappedPorts = new ArrayList<FuzzyPortSearchResult>();
	
	/**
	 * Class constructor
	 *
	 */
	public FuzzyPortSearchResult() {
		super();
	}

	/**
	 * @return the 'countryId' value
	 */
	public Integer getCountryId() {
		return this._countryId;
	}

	/**
	 * @param countryId the 'countryId' value to set
	 */
	public void setCountryId(Integer countryId) {
		this._countryId = countryId;
	}

	/**
	 * @return the 'portId' value
	 */
	public Integer getId() {
		return this._id;
	}

	/**
	 * @param portId the 'portId' value to set
	 */
	public void setId(Integer portId) {
		this._id = portId;
	}

	/**
	 * @return the 'sourceSystem' value
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}

	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}

	/**
	 * @return the 'mappedTo' value
	 */
	public Boolean getIsMapped() {
		return this._isMapped;
	}

	/**
	 * @param isMapped the 'isMapped' value to set
	 */
	public void setIsMapped(Boolean isMapped) {
		this._isMapped = isMapped;
	}

	/**
	 * @return the 'originalPortId' value
	 */
	public String getOriginalId() {
		return this._originalId;
	}

	/**
	 * @param originalPortId the 'originalPortId' value to set
	 */
	public void setOriginalId(String originalPortId) {
		this._originalId = originalPortId;
	}

	/**
	 * @return the 'subdivisionId' value
	 */
	public String getSubdivisionId() {
		return this._subdivisionId;
	}

	/**
	 * @param subdivisionId the 'subdivisionId' value to set
	 */
	public void setSubdivisionId(String subdivisionId) {
		this._subdivisionId = subdivisionId;
	}

	/**
	 * @return the 'mappedPorts' value
	 */
	public List<FuzzyPortSearchResult> getMappedPorts() {
		Collections.sort(this._mappedPorts, new FuzzySearchResultDataComparator());
		
		return this._mappedPorts;
	}
}