/**
 * (c) 2015 FAO / UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 4, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 4, 2015
 */
public interface ValueRange<TYPE extends Number> {
	TYPE getMin();
	TYPE getMax();
}
