/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion.support;
import java.io.Serializable;
import java.util.Comparator;

import org.fao.fi.vrmf.common.models.search.autocompletion.FuzzySearchResult;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Jan 2011
 */
public class FuzzySearchResultDataComparator implements Comparator<FuzzySearchResult>, Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1904636826365426533L;

	private boolean _descending = true;
	
	/**
	 * Class constructor
	 *
	 */
	public FuzzySearchResultDataComparator() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param descending
	 */
	public FuzzySearchResultDataComparator(boolean descending) {
		super();
		
		this._descending = descending;
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(FuzzySearchResult first, FuzzySearchResult second) {
		return (this._descending ? -1 : 1) * first.getScore().compareTo(second.getScore());
	}	
}