/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion.typed;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.fao.fi.vrmf.common.models.generated.SVesselTypes;
import org.fao.fi.vrmf.common.models.search.autocompletion.FuzzySearchResult;
import org.fao.fi.vrmf.common.models.search.autocompletion.support.FuzzySearchResultDataComparator;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Nov 2010
 */
public class FuzzyVesselTypeSearchResult extends SVesselTypes implements FuzzySearchResult {
	private static final long serialVersionUID = 4516129490822811326L;
	
	//Fields required by the 'FuzzySearchResult' interface
	private Double _score;

	private List<FuzzyVesselTypeSearchResult> _mappedTypes = new ArrayList<FuzzyVesselTypeSearchResult>();
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.models.search.autocompletion.FuzzySearchResult#getScore()
	 */
	public Double getScore() {
		return this._score;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.models.search.autocompletion.FuzzySearchResult#setScore(java.lang.Double)
	 */
	public void setScore(Double score) {
		this._score = score;
	}

	/**
	 * @return the 'mappedTypes' value
	 */
	public List<FuzzyVesselTypeSearchResult> getMappedTypes() {
		Collections.sort(this._mappedTypes, new FuzzySearchResultDataComparator());	

		return this._mappedTypes;
	}	
}