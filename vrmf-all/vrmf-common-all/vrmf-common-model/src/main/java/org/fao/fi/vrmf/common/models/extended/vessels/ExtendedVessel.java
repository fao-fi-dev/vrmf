/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended.vessels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;

import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.core.model.support.DateReferencedUpdatableDataManager;
import org.fao.fi.vrmf.common.core.model.support.comparators.DateReferencedUpdatableDescendingComparator;
import org.fao.fi.vrmf.common.models.generated.Authorizations;
import org.fao.fi.vrmf.common.models.generated.Vessels;
import org.fao.fi.vrmf.common.models.generated.VesselsToBeneficialOwners;
import org.fao.fi.vrmf.common.models.generated.VesselsToBuildingYear;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToCrew;
import org.fao.fi.vrmf.common.models.generated.VesselsToExternalMarkings;
import org.fao.fi.vrmf.common.models.generated.VesselsToFishingLicense;
import org.fao.fi.vrmf.common.models.generated.VesselsToFishingMasters;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToGears;
import org.fao.fi.vrmf.common.models.generated.VesselsToHullMaterial;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToInspections;
import org.fao.fi.vrmf.common.models.generated.VesselsToIuuList;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToMasters;
import org.fao.fi.vrmf.common.models.generated.VesselsToMmsi;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;
import org.fao.fi.vrmf.common.models.generated.VesselsToNonCompliance;
import org.fao.fi.vrmf.common.models.generated.VesselsToOperators;
import org.fao.fi.vrmf.common.models.generated.VesselsToOwners;
import org.fao.fi.vrmf.common.models.generated.VesselsToPortEntryDenials;
import org.fao.fi.vrmf.common.models.generated.VesselsToPower;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;
import org.fao.fi.vrmf.common.models.generated.VesselsToShipbuilders;
import org.fao.fi.vrmf.common.models.generated.VesselsToStatus;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;
import org.fao.fi.vrmf.common.models.generated.VesselsToTypes;
import org.fao.fi.vrmf.common.models.generated.VesselsToVms;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * The 'extended' vessel value object. 
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/giu/2010   ffiorellato     Creation.
 *
 * @version 1.0
 * @since 28/giu/2010
 */
@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode(callSuper=true)
public class ExtendedVessel extends Vessels {
	/** Field 'serialVersionUID' */
	private static final long serialVersionUID = 3636711514651883473L;

	@XmlElementWrapper(name="ComponentVessels")
	@XmlElement(name="ComponentVessel")
	@Getter @Setter private List<Vessels> componentVessels;
	
	@XmlElementWrapper(name="BuildingYears")
	@XmlElement(name="BuildingYear")
	@Getter private List<VesselsToBuildingYear> buildingYearData;

	@XmlTransient
	@Getter private List<VesselsToShipbuilders> shipbuildersData;

	@XmlTransient
	@Getter private List<VesselsToHullMaterial> hullMaterialData;

	@XmlTransient
	@Getter private List<VesselsToStatus> statusData;

	@XmlTransient
	@Getter private List<VesselsToTypes> typeData;
	
	@XmlTransient
	@Getter private List<VesselsToGears> gearData;
	
	@XmlTransient
	@Getter private List<VesselsToFlags> flagData;

	@XmlElementWrapper(name="Names")
	@XmlElement(name="Name")
	@Getter private List<VesselsToName> nameData;
	
	@XmlTransient
	@Getter private List<VesselsToExternalMarkings> externalMarkingsData;
	
	@XmlTransient
	@Getter private List<VesselsToRegistrations> registrationData;

	@XmlTransient
	@Getter private List<VesselsToCallsigns> callsignData;
	
	@XmlTransient
	@Getter private List<VesselsToFishingLicense> fishingLicenseData;
	
	@XmlElementWrapper(name="MMSIs")
	@XmlElement(name="MMSI")
	@Getter private List<VesselsToMmsi> MMSIData;

	@XmlElementWrapper(name="VMSes")
	@XmlElement(name="VMS")
	@Getter private List<VesselsToVms> VMSData;
	
	@XmlElementWrapper(name="CrewSizes")
	@XmlElement(name="CrewSize")
	@Getter private List<VesselsToCrew> crewData;
	
	@XmlTransient
	@Getter private List<VesselsToPower> powerData;
	
	@XmlTransient
	@Getter private List<VesselsToLengths> lengthData;
	
	@XmlTransient
	@Getter private List<VesselsToTonnage> tonnageData;
	
	@XmlTransient
	@Getter private List<VesselsToMasters> mastersData;
	
	@XmlTransient
	@Getter private List<VesselsToFishingMasters> fishingMastersData;
	
	@XmlTransient
	@Getter private List<VesselsToOperators> operatorsData;
	
	@XmlTransient
	@Getter private List<VesselsToBeneficialOwners> beneficialOwnersData;
	
	@XmlTransient
	@Getter private List<VesselsToOwners> ownersData;
	
	@XmlTransient
	@Getter private List<VesselsToNonCompliance> nonComplianceData;
	
	@XmlTransient
	@Getter private List<VesselsToInspections> inspectionsData;
	
	@XmlTransient
	@Getter private List<VesselsToIuuList> iuuListsData;
	
	@XmlTransient
	@Getter private List<VesselsToPortEntryDenials> portEntryDenialsData;

	@XmlTransient
	@Getter private List<Authorizations> authorizationsData;
	
	@XmlTransient
	@Getter @Setter private List<Integer> ids;
	
	@XmlElementWrapper(name="Identifiers")
	@XmlElement(name="Identifier")
	@Getter private List<VesselsToIdentifiers> identifiers;
	
	@XmlAttribute(name="numberOfSources")
	@Getter @Setter private Integer numberOfSystems;
	
	@XmlElementWrapper(name="SourceSystems")
	@XmlElement(name="SourceSystem")
	@Getter @Setter private String[] sourceSystems;

	/**
	 * Class constructor.
	 */
	public ExtendedVessel() {
		super();
		
		this.componentVessels = new ListSet<Vessels>();
		this.buildingYearData = new ListSet<VesselsToBuildingYear>();
		this.shipbuildersData = new ListSet<VesselsToShipbuilders>();
		
		this.hullMaterialData = new ListSet<VesselsToHullMaterial>();
		this.statusData = new ListSet<VesselsToStatus>();
		this.typeData = new ListSet<VesselsToTypes>();
		this.flagData = new ListSet<VesselsToFlags>();

		this.nameData = new ListSet<VesselsToName>();
		this.externalMarkingsData = new ListSet<VesselsToExternalMarkings>();
		this.registrationData = new ListSet<VesselsToRegistrations>();
		this.callsignData = new ListSet<VesselsToCallsigns>();
		this.fishingLicenseData = new ListSet<VesselsToFishingLicense>();
		this.MMSIData = new ListSet<VesselsToMmsi>();
		this.VMSData = new ListSet<VesselsToVms>();
		
		this.gearData = new ListSet<VesselsToGears>();

		this.crewData = new ListSet<VesselsToCrew>();
		this.powerData = new ListSet<VesselsToPower>();
		this.lengthData = new ListSet<VesselsToLengths>();	
		this.tonnageData = new ListSet<VesselsToTonnage>();
		
		this.mastersData = new ListSet<VesselsToMasters>();
		this.fishingMastersData = new ListSet<VesselsToFishingMasters>();
		
		this.operatorsData = new ListSet<VesselsToOperators>();
		this.beneficialOwnersData = new ListSet<VesselsToBeneficialOwners>();
		this.ownersData = new ListSet<VesselsToOwners>();
		
		this.inspectionsData = new ListSet<VesselsToInspections>();
		this.nonComplianceData = new ListSet<VesselsToNonCompliance>();
		this.iuuListsData = new ListSet<VesselsToIuuList>();
		this.portEntryDenialsData = new ListSet<VesselsToPortEntryDenials>();
		
		this.authorizationsData = new ListSet<Authorizations>();
		
		this.ids = new ListSet<Integer>();
		this.identifiers = new ListSet<VesselsToIdentifiers>();
	}
	
	/**
	 * Class constructor.
	 */
	public ExtendedVessel(Vessels source) {
		this();
		
		if(source != null) {
			this.setComponentVessels(Arrays.asList(new Vessels[] { source }));
			this.setId(source.getId());
			this.setUid(source.getUid());
			this.setMapsTo(source.getMapsTo());
			this.setMappingUser(source.getMappingUser()); 
			this.setMappingWeight(source.getMappingWeight());
			this.setMappingDate(source.getMappingDate());
			this.setMappingComment(source.getMappingComment());
			this.setSourceSystem(source.getSourceSystem());
			this.setUpdaterId(source.getUpdaterId());
			this.setUpdateDate(source.getUpdateDate());
			this.setComment(source.getComment());
	
			this.ids = new ListSet<Integer>();
			this.ids.add(source.getId());
		}
	}

	public void setBuildingYearData(List<VesselsToBuildingYear> buildingYearData) {
		this.setBuildingYearData(buildingYearData, false);
	}
	
	/**
	 * @param buildingYearData the 'buildingYearData' value to set
	 */
	public void setBuildingYearData(List<VesselsToBuildingYear> buildingYearData, boolean preserveOrder) {
		this.buildingYearData.clear();
		
		if(buildingYearData != null) {
			this.buildingYearData.addAll(buildingYearData);
		
			if(!preserveOrder)
				Collections.sort(this.buildingYearData, new DateReferencedUpdatableDescendingComparator());
		}
	}

	public void setShipbuildersData(List<VesselsToShipbuilders> shipbuildersData) {
		this.setShipbuildersData(shipbuildersData, false);
	}
	
	/**
	 * @param shipbuildersData the 'shipbuildersData' value to set
	 */
	public void setShipbuildersData(List<VesselsToShipbuilders> shipbuildersData, boolean preserveOrder) {
		this.shipbuildersData.clear();
		
		if(shipbuildersData != null) {
			this.shipbuildersData.addAll(shipbuildersData);
		
			if(!preserveOrder)
				Collections.sort(this.shipbuildersData, new DateReferencedUpdatableDescendingComparator());
		}
	}
	
	public void setOperatorsData(List<VesselsToOperators> operatorsData) {
		this.setOperatorsData(operatorsData, false);
	}
	
	/**
	 * Setter for operatorsData
	 *
	 * @param operatorsData the value to set for operatorsData
	 */
	public void setOperatorsData(List<VesselsToOperators> operatorsData, boolean preserveOrder) {
		this.operatorsData.clear();
		
		if(operatorsData != null) {
			this.operatorsData.addAll(operatorsData);
		
			if(!preserveOrder)
				Collections.sort(this.operatorsData, new DateReferencedUpdatableDescendingComparator());
		}
	}

	public void setOwnersData(List<VesselsToOwners> ownersData) {
		this.setOwnersData(ownersData, false);
	}
	
	/**
	 * Setter for ownersData
	 *
	 * @param ownersData the value to set for ownersData
	 */
	public void setOwnersData(List<VesselsToOwners> ownersData, boolean preserveOrder) {
		this.ownersData.clear();
		
		if(ownersData != null) {
			this.ownersData.addAll(ownersData);
		
			if(!preserveOrder)
				Collections.sort(this.ownersData, new DateReferencedUpdatableDescendingComparator());
		}
	}
	
	/**
	 * @param beneficialOwnersData
	 */
	public void setBeneficialOwnersData(List<VesselsToBeneficialOwners> beneficialOwnersData) {
		this.setBeneficialOwnersData(beneficialOwnersData, false);
	}
	
	/**
	 * @param beneficialOwnersData
	 * @param preserveOrder
	 */
	public void setBeneficialOwnersData(List<VesselsToBeneficialOwners> beneficialOwnersData, boolean preserveOrder) {
		this.beneficialOwnersData.clear();
		
		if(beneficialOwnersData != null) {
			this.beneficialOwnersData.addAll(beneficialOwnersData);
		
			if(!preserveOrder)
				Collections.sort(this.beneficialOwnersData, new DateReferencedUpdatableDescendingComparator());
		}
	}

	/**
	 * @param mastersData
	 */
	public void setMastersData(List<VesselsToMasters> mastersData) {
		this.setMastersData(mastersData, false);
	}
	
	/**
	 * @param mastersData
	 * @param preserveOrder
	 */
	public void setMastersData(List<VesselsToMasters> mastersData, boolean preserveOrder) {
		this.mastersData.clear();
		
		if(mastersData != null) {
			this.mastersData.addAll(mastersData);
		
			if(!preserveOrder)
				Collections.sort(this.mastersData, new DateReferencedUpdatableDescendingComparator());
		}
	}
	
	/**
	 * @param fishingMastersData
	 */
	public void setFishingMastersData(List<VesselsToFishingMasters> fishingMastersData) {
		this.setFishingMastersData(fishingMastersData, false);
	}
	
	/**
	 * @param fishingMastersData
	 * @param preserveOrder
	 */
	public void setFishingMastersData(List<VesselsToFishingMasters> fishingMastersData, boolean preserveOrder) {
		this.fishingMastersData.clear();
		
		if(fishingMastersData != null) {
			this.fishingMastersData.addAll(fishingMastersData);
		
			if(!preserveOrder)
				Collections.sort(this.fishingMastersData, new DateReferencedUpdatableDescendingComparator());
		}
	}

	
	public void setStatusData(List<VesselsToStatus> statusData) {
		this.setStatusData(statusData, false);
	}
	
	/**
	 * Setter for statusData
	 *
	 * @param statusData the value to set for statusData
	 */
	public void setStatusData(List<VesselsToStatus> statusData, boolean preserveOrder) {
		this.statusData.clear();
		
		if(statusData != null) {
			this.statusData.addAll(statusData);
		
			if(!preserveOrder)
				Collections.sort(this.statusData, new DateReferencedUpdatableDescendingComparator());
		}
	}

	public void setTypeData(List<VesselsToTypes> typeData) {
		this.setTypeData(typeData, false);
	}
	
	/**
	 * Setter for typeData
	 *
	 * @param typeData the value to set for typeData
	 */
	public void setTypeData(List<VesselsToTypes> typeData, boolean preserveOrder) {
		this.typeData.clear();
		
		if(typeData != null) {
			this.typeData.addAll(typeData);
		
			if(!preserveOrder)
				Collections.sort(this.typeData, new DateReferencedUpdatableDescendingComparator());
		}
	}

	public void setGearData(List<VesselsToGears> gearData) {
		this.setGearData(gearData, false);
	}
	
	/**
	 * Setter for gearData
	 *
	 * @param gearData the value to set for gearData
	 */
	public void setGearData(List<VesselsToGears> gearData, boolean preserveOrder) {
		this.gearData.clear();
		
		if(gearData != null) {
			this.gearData.addAll(gearData);
		
			if(!preserveOrder)
				Collections.sort(this.gearData, new DateReferencedUpdatableDescendingComparator());
		}		
	}

	/**
	 * Setter for nameData
	 *
	 * @param nameData the value to set for nameData
	 */
	public void setNameData(List<VesselsToName> nameData) {
		this.setNameData(nameData, false);		
	}
	
	public void setNameData(List<VesselsToName> nameData, boolean preserveOrder) {
		this.nameData.clear();
		
		if(nameData != null) {
			this.nameData.addAll(nameData);
		
			if(!preserveOrder)
				Collections.sort(this.nameData, new DateReferencedUpdatableDescendingComparator());
		}		
	}

	public void setExternalMarkingsData(List<VesselsToExternalMarkings> externalMarkingsData) {
		this.setExternalMarkingsData(externalMarkingsData, false);
	}

	/**
	 * @param externalMarkingsData the 'externalMarkingsData' value to set
	 */
	public void setExternalMarkingsData(List<VesselsToExternalMarkings> externalMarkingsData, boolean preserveOrder) {
		if(externalMarkingsData != null) {
			this.externalMarkingsData.addAll(externalMarkingsData);
		
			if(!preserveOrder)
				Collections.sort(this.externalMarkingsData, new DateReferencedUpdatableDescendingComparator());
		}
	}

	public void setRegistrationData(List<VesselsToRegistrations> registrationData) {
		this.setRegistrationData(registrationData, false);
	}
	
	/**
	 * Setter for registrationData
	 *
	 * @param registrationData the value to set for registrationData
	 */
	public void setRegistrationData(List<VesselsToRegistrations> registrationData, boolean preserveOrder) {
		this.registrationData.clear();
		
		if(registrationData != null) {
			this.registrationData.addAll(registrationData);
		
			if(!preserveOrder)
				Collections.sort(this.registrationData, new DateReferencedUpdatableDescendingComparator());
		}			
	}

	/**
	 * Setter for flagData
	 *
	 * @param flagData the value to set for flagData
	 */
	public void setFlagData(List<VesselsToFlags> flagData) {
		this.setFlagData(flagData, false);			
	}
	
	public void setFlagData(List<VesselsToFlags> flagData, boolean preserveOrder) {
		this.flagData.clear();
		
		if(flagData != null) {
			this.flagData.addAll(flagData);
		
			if(!preserveOrder)
				Collections.sort(this.flagData, new DateReferencedUpdatableDescendingComparator());
		}			
	}

	public void setCallsignData(List<VesselsToCallsigns> callsignData) {
		this.setCallsignData(callsignData, false);
	}
	
	/**
	 * Setter for callsignData
	 *
	 * @param callsignData the value to set for callsignData
	 */
	public void setCallsignData(List<VesselsToCallsigns> callsignData, boolean preserveOrder) {
		this.callsignData.clear();
		
		if(callsignData != null) {
			this.callsignData.addAll(callsignData);
		
			if(!preserveOrder)
				Collections.sort(this.callsignData, new DateReferencedUpdatableDescendingComparator());
		}			
	}
	
	/**
	 * @param fishingLicenseData the 'fishingLicenseData' value to set
	 */
	public void setFishingLicenseData(List<VesselsToFishingLicense> fishingLicenseData) {
		this.setFishingLicenseData(fishingLicenseData, false);
	}
	
	/**
	 * Setter for callsignData
	 *
	 * @param callsignData the value to set for callsignData
	 */
	public void setFishingLicenseData(List<VesselsToFishingLicense> fishingLicenseData, boolean preserveOrder) {
		this.fishingLicenseData.clear();
		
		if(fishingLicenseData != null) {
			this.fishingLicenseData.addAll(fishingLicenseData);
		
			if(!preserveOrder)
				Collections.sort(this.fishingLicenseData, new DateReferencedUpdatableDescendingComparator());
		}			
	}

	public void setMMSIData(List<VesselsToMmsi> MMSIData) {
		this.setMMSIData(MMSIData, false);
	}
	
	/**
	 * @param MMSIData the 'MMSIData' value to set
	 */
	public void setMMSIData(List<VesselsToMmsi> MMSIData, boolean preserveOrder) {
		this.MMSIData.clear();
		
		if(MMSIData != null) {
			this.MMSIData.addAll(MMSIData);
		
			if(!preserveOrder)
				Collections.sort(this.MMSIData, new DateReferencedUpdatableDescendingComparator());
		}
	}

	public void setCrewData(List<VesselsToCrew> crewData) {
		this.setCrewData(crewData, false);
	}

	/**
	 * Setter for crewData
	 *
	 * @param crewData the value to set for crewData
	 */
	public void setCrewData(List<VesselsToCrew> crewData, boolean preserveOrder) {
		this.crewData.clear();
		
		if(crewData != null) {
			this.crewData.addAll(crewData);
		
			if(!preserveOrder)
				Collections.sort(this.crewData, new DateReferencedUpdatableDescendingComparator());
		}	
	}

	public void setPowerData(List<VesselsToPower> powerData) {
		this.setPowerData(powerData, false);
	}
	
	/**
	 * Setter for powerData
	 *
	 * @param powerData the value to set for powerData
	 */
	public void setPowerData(List<VesselsToPower> powerData, boolean preserveOrder) {
		this.powerData.clear();
		
		if(powerData != null) {
			this.powerData.addAll(powerData);
		
			if(!preserveOrder)
				Collections.sort(this.powerData, new DateReferencedUpdatableDescendingComparator());
		}	
	}

	public void setLengthData(List<VesselsToLengths> lengthData) {
		this.setLengthData(lengthData, false);
	}
	
	/**
	 * Setter for lengthData
	 *
	 * @param lengthData the value to set for lengthData
	 */
	public void setLengthData(List<VesselsToLengths> lengthData, boolean preserveOrder) {
		this.lengthData.clear();
		
		if(lengthData != null) {
			this.lengthData.addAll(lengthData);
		
			if(!preserveOrder)
				Collections.sort(this.lengthData, new DateReferencedUpdatableDescendingComparator());
		}		
	}

	public void setTonnageData(List<VesselsToTonnage> tonnageData) {
		this.setTonnageData(tonnageData, false);
	}
	
	/**
	 * Setter for tonnageData
	 *
	 * @param tonnageData the value to set for tonnageData
	 */
	public void setTonnageData(List<VesselsToTonnage> tonnageData, boolean preserveOrder) {
		this.tonnageData.clear();
		
		if(tonnageData != null) {
			this.tonnageData.addAll(tonnageData);
			
			if(!preserveOrder)
				Collections.sort(this.tonnageData, new DateReferencedUpdatableDescendingComparator());
		}
	}

	public void setAuthorizationsData(List<Authorizations> authorizations) {
		this.setAuthorizationsData(authorizations, false);
	}
	
	/**
	 * Setter for authorizations
	 *
	 * @param authorizations the value to set for authorizations
	 */
	public void setAuthorizationsData(List<Authorizations> authorizations, boolean preserveOrder) {
		this.authorizationsData.clear();
		
		if(authorizations != null) {
			this.authorizationsData.addAll(authorizations);
			
			if(!preserveOrder)
				Collections.sort(this.authorizationsData, new DateReferencedUpdatableDescendingComparator());
		}
	}

	/**
	 * @param identifiers the 'identifiers' value to set
	 */
	public void setIdentifiers(List<VesselsToIdentifiers> identifiers) {
		this.setIdentifiers(identifiers, false);
	}
	
	/**
	 * @param identifiers the 'identifiers' value to set
	 */
	public void setIdentifiers(List<VesselsToIdentifiers> identifiers, boolean preserveOrder) {
		this.identifiers.clear();
		
		if(identifiers != null) {
			this.identifiers.addAll(identifiers);
			
			if(!preserveOrder)
				Collections.sort(this.identifiers, new DateReferencedUpdatableDescendingComparator());
		}
	}
	
	/**
	 * @param hullMaterialData the 'hullMaterialData' value to set
	 */
	public void setHullMaterialData(List<VesselsToHullMaterial> hullMaterialData) {
		this.setHullMaterialData(hullMaterialData, false);
	}

	/**
	 * @param hullMaterialData
	 * @param preserveOrder
	 */
	public void setHullMaterialData(List<VesselsToHullMaterial> hullMaterialData, boolean preserveOrder) {
		this.hullMaterialData.clear();
		
		if(hullMaterialData != null) {
			this.hullMaterialData.addAll(hullMaterialData);
			
			if(!preserveOrder)
				Collections.sort(this.hullMaterialData, new DateReferencedUpdatableDescendingComparator());
		}
	}

	/**
	 * @param vmsData the 'hullMaterialData' value to set
	 */
	public void setVMSData(List<VesselsToVms> vmsData) {
		this.setVMSData(vmsData, false);
	}

	/**
	 * @param vmsData
	 * @param preserveOrder
	 */
	public void setVMSData(List<VesselsToVms> vmsData, boolean preserveOrder) {
		this.VMSData.clear();
		
		if(vmsData != null) {
			this.VMSData.addAll(vmsData);
			
			if(!preserveOrder)
				Collections.sort(this.VMSData, new DateReferencedUpdatableDescendingComparator());
		}
	}
	
	/**
	 * @param nonComplianceData the 'nonComplianceData' value to set
	 */
	public void setNonComplianceData(List<VesselsToNonCompliance> nonComplianceData) {
		this.setNonComplianceData(nonComplianceData, false);
	}

	/**
	 * @param nonComplianceData the 'nonComplianceData' value to set
	 */
	public void setNonComplianceData(List<VesselsToNonCompliance> nonComplianceData, boolean preserveOrder) {
		this.nonComplianceData.clear();
		
		if(nonComplianceData != null) {
			this.nonComplianceData.addAll(nonComplianceData);
			
			if(!preserveOrder)
				Collections.sort(this.nonComplianceData, new DateReferencedUpdatableDescendingComparator());
		}
	}

	/**
	 * @param inspectionsData the 'inspectionsData' value to set
	 */
	public void setInspectionsData(List<VesselsToInspections> inspectionsData) {
		this.setInspectionsData(inspectionsData, false);
	}
	
	/**
	 * @param inspectionsData the 'inspectionsData' value to set
	 */
	public void setInspectionsData(List<VesselsToInspections> inspectionsData, boolean preserveOrder) {
		this.inspectionsData.clear();
		
		if(inspectionsData != null) {
			this.inspectionsData.addAll(inspectionsData);
			
			if(!preserveOrder)
				Collections.sort(this.inspectionsData, new DateReferencedUpdatableDescendingComparator());
		}
	}
	
	

	/**
	 * @param iuuListsData the 'iuuListsData' value to set
	 */
	public void setIuuListsData(List<VesselsToIuuList> iuuListsData) {
		this.setIuuListsData(iuuListsData, false);
	}
	
	/**
	 * @param iuuListsData the 'iuuListsData' value to set
	 */
	public void setIuuListsData(List<VesselsToIuuList> iuuListsData, boolean preserveOrder) {
		this.iuuListsData.clear();
		
		if(iuuListsData != null) {
			this.iuuListsData.addAll(iuuListsData);
			
			if(!preserveOrder)
				Collections.sort(this.iuuListsData, new DateReferencedUpdatableDescendingComparator());
		}
	}
	
	

	/**
	 * @param portEntryDenials the 'portEntryDenials' value to set
	 */
	public void setPortEntryDenialsData(List<VesselsToPortEntryDenials> portEntryDenials) {
		this.setPortEntryDenialsData(portEntryDenials, false);
	}

	/**
	 * @param portEntryDenials the 'portEntryDenials' value to set
	 */
	public void setPortEntryDenialsData(List<VesselsToPortEntryDenials> portEntryDenials, boolean preserveOrder) {
		this.portEntryDenialsData.clear();
		
		if(portEntryDenials != null) {
			this.portEntryDenialsData.addAll(portEntryDenials);
			
			if(!preserveOrder)
				Collections.sort(this.portEntryDenialsData, new DateReferencedUpdatableDescendingComparator());
		}
	}
	
	/**
	 * @return
	 */
	public VesselsToFlags getCurrentFlag() {
		return new DateReferencedUpdatableDataManager<VesselsToFlags>().getCurrentData(flagData);
	}
	
	/**
	 * @return
	 */
	public Integer getCurrentFlagId() {
		VesselsToFlags result = this.getCurrentFlag();
		
		if(result == null)
			return null;
		
		return result.getCountryId();
	}
	
	/**
	 * @return
	 */
	public String getCurrentName() {
		VesselsToName result = new DateReferencedUpdatableDataManager<VesselsToName>().getCurrentData(nameData);
		
		if(result == null)
			return "<NAME UNKNOWN>";
		
		return result.getName();
	}
	
	/**
	 * @return
	 */
	public String getCurrentSimplifiedName() {
		VesselsToName result = new DateReferencedUpdatableDataManager<VesselsToName>().getCurrentData(nameData);
		
		if(result == null)
			return "<NAME UNKNOWN>";
		
		return result.getSimplifiedName();
	}
	
	public VesselsToLengths getCurrentLength() {
		return new DateReferencedUpdatableDataManager<VesselsToLengths>().getCurrentData(lengthData);
	}
	
	public VesselsToTonnage getCurrentTonnage() {
		return new DateReferencedUpdatableDataManager<VesselsToTonnage>().getCurrentData(tonnageData);
	}
	
	/**
	 * @return the 'publicIdentifiers' value
	 */
	public List<VesselsToIdentifiers> getPublicIdentifiers() {
		List<VesselsToIdentifiers> toReturn = new ArrayList<VesselsToIdentifiers>(); 
		
		if(identifiers == null || identifiers.isEmpty())
			return toReturn;
		
		String idType;
		for(VesselsToIdentifiers current : identifiers) {
			idType =  current.getTypeId();
			 
			//TODO: IMPROVE!!!
			if("GR_ID".equalsIgnoreCase(idType) ||
			   "TUVI".equalsIgnoreCase(idType) ||
			   "EU_CFR".equalsIgnoreCase(idType) ||
			   "IMO".equalsIgnoreCase(idType) ||
			   "ICCAT_ID".equalsIgnoreCase(idType) ||
			   "CCSBT_ID".equalsIgnoreCase(idType) ||
			   "CCSBT_SID".equalsIgnoreCase(idType) ||
			   "IOTC_ID".equalsIgnoreCase(idType) ||
			   "IATTC_ID".equalsIgnoreCase(idType) ||
			   "WCPFC_ID".equalsIgnoreCase(idType) ||
			   "FFA_ID".equalsIgnoreCase(idType) ||
			   "CCAMLR_ID".equalsIgnoreCase(idType) ||
			   "ADFG_ID".equalsIgnoreCase(idType) ||
			   "TCGC_ID".equalsIgnoreCase(idType))
				toReturn.add(current);
		}
		
		return toReturn;
	}

	public String findIMO() {
		if(this.identifiers == null || this.identifiers.isEmpty())
			return null;
		
		for(VesselsToIdentifiers current : this.identifiers) {
			if("IMO".equals(current.getTypeId()))
				return current.getIdentifier();
		}
		
		return null;
	}
	
	public String findEUCFR() {
		if(this.identifiers == null || this.identifiers.isEmpty())
			return null;
		
		for(VesselsToIdentifiers current : this.identifiers) {
			if("EUCFR".equals(current.getTypeId()))
				return current.getIdentifier();
		}
		
		return null;
	}
	
	public ExtendedVessel join(ExtendedVessel another) {
		if(another == null || this.getId().equals(another.getId()))
			return this;
		
		if(another.componentVessels != null)
			this.componentVessels.addAll(another.componentVessels);
		
		if(another.authorizationsData != null)
			this.authorizationsData.addAll(another.authorizationsData);
		
		if(another.beneficialOwnersData != null)
			this.beneficialOwnersData.addAll(another.beneficialOwnersData);
		
		if(another.buildingYearData != null)
			this.buildingYearData.addAll(another.buildingYearData);
		
		if(another.callsignData != null)
			this.callsignData.addAll(another.callsignData);

		if(another.crewData != null)
			this.crewData.addAll(another.crewData);
		
		if(another.externalMarkingsData != null)
			this.externalMarkingsData.addAll(another.externalMarkingsData);
		
		if(another.fishingLicenseData != null)
			this.fishingLicenseData.addAll(another.fishingLicenseData);

		if(another.fishingMastersData != null)
			this.fishingMastersData.addAll(another.fishingMastersData);
	
		if(another.flagData != null)
			this.flagData.addAll(another.flagData);
		
		if(another.gearData != null)
			this.gearData.addAll(another.gearData);
		
		if(another.hullMaterialData != null)
			this.hullMaterialData.addAll(another.hullMaterialData);
		
		if(another.identifiers != null)
			this.identifiers.addAll(another.identifiers);
		
		if(another.lengthData != null)
			this.lengthData.addAll(another.lengthData);
		
		if(another.nonComplianceData != null)
			this.nonComplianceData.addAll(another.nonComplianceData);
		
		if(another.mastersData != null)
			this.mastersData.addAll(another.mastersData);
		
		if(another.MMSIData != null)
			this.MMSIData.addAll(another.MMSIData);
		
		if(another.nameData != null)
			this.nameData.addAll(another.nameData);
		
		if(another.operatorsData != null)
			this.operatorsData.addAll(another.operatorsData);
		
		if(another.ownersData != null)
			this.ownersData.addAll(another.ownersData);
		
		if(another.powerData != null)
			this.powerData.addAll(another.powerData);
		
		if(another.registrationData != null)
			this.registrationData.addAll(another.registrationData);
		
		if(another.shipbuildersData != null)
			this.shipbuildersData.addAll(another.shipbuildersData);
		
		if(another.statusData != null)
			this.statusData.addAll(another.statusData);
		
		if(another.tonnageData != null)
			this.tonnageData.addAll(another.tonnageData);
		
		if(another.typeData != null)
			this.typeData.addAll(another.typeData);
		
		if(another.VMSData != null)
			this.VMSData.addAll(another.VMSData);
		
		return this;
	}
	
	public boolean hasData() {
		return 	(this.getComponentVessels() != null && !this.getComponentVessels().isEmpty()) ||
				(this.getIdentifiers() != null && !this.getIdentifiers().isEmpty()) ||
				(this.getNameData() != null && !this.getNameData().isEmpty()) ||
				(this.getExternalMarkingsData() != null && !this.getExternalMarkingsData().isEmpty()) ||
				(this.getFlagData() != null && !this.getFlagData().isEmpty()) ||
				(this.getFlagData() != null && !this.getFlagData().isEmpty()) ||
				(this.getShipbuildersData() != null && !this.getShipbuildersData().isEmpty()) ||
				(this.getStatusData() != null && !this.getStatusData().isEmpty()) ||
				(this.getHullMaterialData() != null && !this.getHullMaterialData().isEmpty()) ||
				(this.getBuildingYearData() != null && !this.getBuildingYearData().isEmpty()) ||
				(this.getTypeData() != null && !this.getTypeData().isEmpty()) ||
				(this.getGearData() != null && !this.getGearData().isEmpty()) ||
				(this.getCallsignData() != null && !this.getCallsignData().isEmpty()) ||
				(this.getFishingLicenseData() != null && !this.getFishingLicenseData().isEmpty()) ||
				(this.getMMSIData() != null && !this.getMMSIData().isEmpty()) ||
				(this.getVMSData() != null && !this.getVMSData().isEmpty()) ||
				(this.getRegistrationData() != null && !this.getRegistrationData().isEmpty()) ||
				(this.getLengthData() != null && !this.getLengthData().isEmpty()) ||
				(this.getTonnageData() != null && !this.getTonnageData().isEmpty()) ||
				(this.getPowerData() != null && !this.getPowerData().isEmpty()) ||
				(this.getCrewData() != null && !this.getCrewData().isEmpty()) ||
				(this.getBeneficialOwnersData() != null && !this.getBeneficialOwnersData().isEmpty()) ||
				(this.getOwnersData() != null && !this.getOwnersData().isEmpty()) ||
				(this.getOperatorsData() != null && !this.getOperatorsData().isEmpty()) ||
				(this.getMastersData() != null && !this.getMastersData().isEmpty()) ||
				(this.getFishingMastersData() != null && !this.getFishingMastersData().isEmpty()) ||
				(this.getAuthorizationsData() != null && !this.getAuthorizationsData().isEmpty());		
	}

}