/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.services.search.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.utility.model.extensions.collections.SerializableList;
import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Nov 2011
 */
@XmlType(name="SearchResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchResponse<ROW extends Serializable> extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5207894329984798014L;
	
	@XmlElementWrapper(name="dataSet")
	@XmlElement(name="data")
	private SerializableList<ROW> _dataSet;
	
	/**
	 * Class constructor
	 *
	 */
	public SearchResponse() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param dataSet
	 */
	public SearchResponse(SerializableList<ROW> dataSet) {
		super();
		
		this._dataSet = dataSet;
	}

	/**
	 * @return the 'report' value
	 */
	public SerializableList<ROW> getDataSet() {
		return this._dataSet;
	}

	/**
	 * @param dataSet the 'dataSet' value to set
	 */
	public void setDataSet(SerializableList<ROW> dataSet) {
		this._dataSet = dataSet;
	}
}