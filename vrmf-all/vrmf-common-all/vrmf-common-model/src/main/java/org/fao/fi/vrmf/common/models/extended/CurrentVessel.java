/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0 
 * @since 1 Dec 2010
 */
@Deprecated
public class CurrentVessel extends GenericCurrentVessel<FullVessel> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2240056356770070341L;
	
	public CurrentVessel(FullVessel source) {
		super(source);
	}
}