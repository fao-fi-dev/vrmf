/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended.vessels;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.sh.utility.common.helpers.singletons.lang.BeansHelper;
import org.fao.fi.vrmf.common.models.generated.SNonComplianceInfringementType;
import org.fao.fi.vrmf.common.models.generated.SNonComplianceOutcomeType;
import org.fao.fi.vrmf.common.models.generated.SNonComplianceSourceType;
import org.fao.fi.vrmf.common.models.generated.SSystems;
import org.fao.fi.vrmf.common.models.generated.VesselsToNonCompliance;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Dec 2010
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public class ExtendedVesselToNonCompliance extends VesselsToNonCompliance {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8087583632648739615L;
	
	@XmlElement
	@Getter @Setter private SNonComplianceSourceType sourceType;
	
	@XmlElement
	@Getter @Setter private SSystems issuingAuthority;
	
	@XmlElement
	@Getter @Setter private SNonComplianceInfringementType infringementType;
	
	@XmlElement
	@Getter @Setter private SNonComplianceOutcomeType outcomeType;
	
	/**
	 * Class constructor
	 *
	 * @param source
	 * @param owner
	 */
	public ExtendedVesselToNonCompliance(VesselsToNonCompliance source, SNonComplianceSourceType sourceType, SSystems issuingAuthority, SNonComplianceInfringementType infringementType, SNonComplianceOutcomeType outcomeType) {
		super();
		
		BeansHelper.transferBean(source, this);
		
		this.sourceType = sourceType;
		this.issuingAuthority = issuingAuthority;
		this.infringementType = infringementType;
		this.outcomeType = outcomeType;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.behaviours.vessel.composite.DateReferencedEntityRelatedVesselAttribute#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return super.isEmpty() && 
			( this.sourceType == null || this.sourceType.getId() == null ) &&
			( this.infringementType == null || this.infringementType.getId() == null ) &&
			( this.outcomeType == null || this.outcomeType.getId() == null ) &&
			( this.issuingAuthority == null || this.issuingAuthority.getId() == null );
	}
}