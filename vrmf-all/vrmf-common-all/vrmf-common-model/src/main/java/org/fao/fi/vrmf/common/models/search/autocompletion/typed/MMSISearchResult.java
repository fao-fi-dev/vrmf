/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion.typed;

import org.fao.fi.vrmf.common.models.search.autocompletion.SimpleGroupedSearchResult;



/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Jan 2011
 */
public final class MMSISearchResult extends SimpleGroupedSearchResult {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5487439384483848917L;

	/**
	 * Class constructor
	 *
	 */
	public MMSISearchResult() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param MMSI
	 * @param totalOccurrencies
	 * @param occurrencies
	 * @param groupedOccurrencies
	 */
	public MMSISearchResult(String MMSI, int totalOccurrencies, int occurrencies, int groupedOccurrencies) {
		super(MMSI, totalOccurrencies, occurrencies, groupedOccurrencies);
	}
	
	/**
	 * @return the 'MMSI' value
	 */
	public String getMMSI() {
		return super.getTerm();
	}
	
	/**
	 * @param MMSI the 'MMSI' value to set
	 */
	public void setMMSI(String MMSI) {
		super.setTerm(MMSI);
	}
}