/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SCountriesSubdivision;
import org.fao.fi.vrmf.common.models.generated.SPorts;
import org.fao.fi.vrmf.common.models.generated.SPortsCoordinates;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
final public class ExtendedPort extends SPorts {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2204179038881651043L;
	
	@XmlElement
	@Getter @Setter private SCountries country;
	
	@XmlElement
	@Getter @Setter private SCountriesSubdivision subdivision;
	
	@XmlElement
	@Getter @Setter private List<SPortsCoordinates> coordinates;
}