/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.stats;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Aug 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Aug 2012
 */
public class TypesStatsData extends BasicVesselStatsReport {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8425189258197709543L;
	
	private Integer _typeId;

	/**
	 * @return the 'countryId' value
	 */
	public Integer getTypeId() {
		return this._typeId;
	}

	/**
	 * @param countryId the 'countryId' value to set
	 */
	public void setTypeId(Integer countryId) {
		this._typeId = countryId;
	}
}
