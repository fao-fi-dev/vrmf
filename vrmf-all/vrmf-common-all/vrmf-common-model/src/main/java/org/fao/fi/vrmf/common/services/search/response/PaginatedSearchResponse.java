/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.services.search.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.utility.model.extensions.collections.SerializableList;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2012
 */
@XmlType(name="paginatedSearchResponse")
public class PaginatedSearchResponse<ROW extends Serializable> extends SearchResponse<ROW> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1957941055986535052L;

	@XmlAttribute(name="offset")
	private Integer _offset;
	
	@XmlAttribute(name="pageSize")
	private Integer _pageSize;
	
	@XmlAttribute(name="currentResultPageSize")
	private Integer _currentResultPageSize;
	
	@XmlAttribute(name="fullResultsSize")
	private Integer _fullResultsSize;

	/**
	 * Class constructor
	 *
	 */
	public PaginatedSearchResponse() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param dataSet
	 */
	public PaginatedSearchResponse(SerializableList<ROW> dataSet) {
		super(dataSet);
	}

	/**
	 * Class constructor
	 *
	 * @param offset
	 * @param pageSize
	 * @param currentResultPageSize
	 * @param fullResultsSize
	 */
	public PaginatedSearchResponse(Integer offset, Integer pageSize, Integer fullResultsSize) {
		this(offset, pageSize, fullResultsSize, null);
	}	
	
	/**
	 * Class constructor
	 *
	 * @param offset
	 * @param pageSize
	 * @param currentResultPageSize
	 * @param fullResultsSize
	 */
	public PaginatedSearchResponse(Integer offset, Integer pageSize, Integer fullResultsSize, SerializableList<ROW> dataSet) {
		super(dataSet);
		this._offset = offset;
		this._pageSize = pageSize;
		this._currentResultPageSize = dataSet == null ? 0 : dataSet.size();
		this._fullResultsSize = fullResultsSize;
	}	
	
	final public boolean isEmpty() {
		return super.getDataSet() == null || super.getDataSet().isEmpty();
	}
	
	final public boolean isNotEmpty() {
		return !this.isEmpty();
	}
	
	/**
	 * @return the 'offset' value
	 */
	final public Integer getOffset() {
		return this._offset;
	}

	/**
	 * @param offset the 'offset' value to set
	 */
	final public void setOffset(Integer offset) {
		this._offset = offset;
	}

	/**
	 * @return the 'pageSize' value
	 */
	final public Integer getPageSize() {
		return this._pageSize;
	}

	/**
	 * @param pageSize the 'pageSize' value to set
	 */
	final public void setPageSize(Integer pageSize) {
		this._pageSize = pageSize;
	}

	/**
	 * @return the 'currentResultPageSize' value
	 */
	final public Integer getCurrentResultPageSize() {
		return this._currentResultPageSize;
	}

	/**
	 * @param currentResultPageSize the 'currentResultPageSize' value to set
	 */
	final public void setCurrentResultPageSize(Integer currentResultPageSize) {
		this._currentResultPageSize = currentResultPageSize;
	}

	/**
	 * @return the 'fullResultsSize' value
	 */
	final public Integer getFullResultsSize() {
		return this._fullResultsSize;
	}

	/**
	 * @param fullResultsSize the 'fullResultsSize' value to set
	 */
	final public void setFullResultsSize(Integer fullResultsSize) {
		this._fullResultsSize = fullResultsSize;
	}
}