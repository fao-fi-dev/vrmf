/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.security;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.auth.UserModel;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableTreeSet;
import org.fao.fi.vrmf.common.core.model.GenericData;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Mar 2011
 */
@XmlRootElement(name="BasicUser")
@XmlAccessorType(XmlAccessType.FIELD)
public class BasicUser extends GenericData implements UserModel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4604614209712279587L;

	@XmlElement(name="id")
	private String _id = null;

	@XmlElementWrapper(name="UserRoles")
	@XmlElement(name="UserRole")
	private Set<UserRole> _roles = new SerializableTreeSet<UserRole>(new UserRoleComparator());

	@XmlElementWrapper(name="UserCapabilities")
	@XmlElement(name="UserCapability")
	private Set<UserCapability> _capabilities = new SerializableTreeSet<UserCapability>(new UserCapabilityComparator());

	static final private class UserRoleComparator implements Comparator<UserRole>, Serializable {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 3977840366677543533L;

		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(UserRole o1, UserRole o2) {
			return o1.getRole().compareTo(o2.getRole());
		}
	};

	static final private class UserCapabilityComparator implements Comparator<UserCapability>, Serializable {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 2999586710288981653L;

		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(UserCapability o1, UserCapability o2) {
			return o1.getCapabilityName().compareTo(o2.getCapabilityName());
		}
	};

	public BasicUser() {
		super();
	}

	public BasicUser(String id) {
		super();

		this._id = id;
	}

	public BasicUser(String id, Collection<UserRole> roles, Collection<UserCapability> capabilities) {
		this._id = id;

		if(roles != null)
			this._roles = new SerializableTreeSet<UserRole>(roles);

		if(capabilities != null)
			this._capabilities = new SerializableTreeSet<UserCapability>(capabilities);
	}

	@Override
	final public String getId() { return this._id; }
	final public void setId(String id) { this._id = id; }

	final public void addRole(UserRole role) { if(role != null) { this._roles.add(role); this.addCapabilities(role.getCapabilities()); } }
	final public void addRoles(UserRole... roles) { if(roles !=null) { for(UserRole role : roles) { this.addRole(role); } } }
	final public void addRoles(Collection<UserRole> roles) { if(roles !=null) { this.addRoles(roles.toArray(new UserRole[roles.size()])); } }

	final public void addCapability(UserCapability capability) { if(capability != null) this._capabilities.add(capability); }
	final public void addCapabilities(UserCapability... capabilities) { if(capabilities !=null) { for(UserCapability capability : capabilities) { this.addCapability(capability); } } }
	final public void addCapabilities(Collection<UserCapability> capabilities) { if(capabilities !=null) { this.addCapabilities(capabilities.toArray(new UserCapability[capabilities.size()])); } }

	/**
	 * @return the 'roles' value
	 */
	public Set<UserRole> getRoles() { return this._roles;	}

	/**
	 * @param roles
	 */
	public void setRoles(Set<UserRole> roles) { this._roles = roles; }

	/**
	 * @return the 'capabilities' value
	 */
	public Set<UserCapability> getCapabilities() { return this._capabilities; }

	/**
	 * @param capabilities
	 */
	public void setCapabilities(Set<UserCapability> capabilities) { this._capabilities = capabilities; }

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.User#is(java.lang.String[])
	 */
	@Override
	final public boolean is(String... roles) {
		boolean is = this._roles.size() > 0;

		if(is) {
			boolean currentIs;

			for(String role : roles) {
				currentIs = false;

				for(UserRole userRole : this._roles) {
					currentIs |= userRole.getRole().equals(role) ||
								 this.check(userRole.getRole(), role) ||
								 this.check(role, userRole.getRole());

					if(currentIs)
						break;
				}

				is &= currentIs;

				if(!is)
					break;
			}
		}

		return is;
	}

	final public boolean is(UserRole... roles) {
		String[] rolesArray = new String[roles.length];

		int counter = 0;

		for(UserRole role : roles)
			rolesArray[counter++] = role.getRole();

		return this.is(rolesArray);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.security.models.User#can(java.lang.String[])
	 */
	@Override
	final public boolean can(String... capabilities) {
		boolean can = true;

		for(String capability : capabilities) {
			can &= this.lazyCan(capability);

			if(!can)
				break;
		}

		return can;
	}

	final public boolean can(UserCapability... capabilities) {
		String[] capabilitiesArray = new String[capabilities.length];

		int counter = 0;

		for(UserCapability capability : capabilities)
			capabilitiesArray[counter++] = capability.getCapabilityName();

		return this.can(capabilitiesArray);
	}

	final public UserCapability getCapability(String capabilityID) {
		$true(StringsHelper.isNotNullAndNonEmpty(capabilityID), "The capability ID cannot be null or null-equivalent");

		if(this._capabilities != null && !this._capabilities.isEmpty())
			for(UserCapability current : this._capabilities)
				if(capabilityID.equals(current.getCapabilityName()))
					return current;

		return null;
	}

	final public UserCapability getCapability(UserCapability capability) {
		$true(StringsHelper.isNotNullAndNonEmpty($nN(capability, "The user capability cannot be null").getCapabilityName()), "The user capability name cannot be null or null-equivalent");

		return this.getCapability(capability.getCapabilityName());
	}

	final public Collection<UserCapability> getCapabilities(String capabilityPattern) {
		$true(StringsHelper.isNotNullAndNonEmpty(capabilityPattern), "The capability pattern cannot be null or null-equivalent");

		Collection<UserCapability> capabilities = new HashSet<UserCapability>();

		if(this._capabilities != null && !this._capabilities.isEmpty())
			for(UserCapability current : this._capabilities)
				if(this.check(capabilityPattern, current.getCapabilityName()) || this.check(current.getCapabilityName(), capabilityPattern))
					capabilities.add(current);

		return capabilities;
	}

	private boolean lazyCan(String capability) {
		boolean can = false;

		if(this._capabilities.size() > 0)
			for(UserCapability userCapability : this._capabilities) {
				can |= this.check(capability, userCapability.getCapabilityName()) || this.check(userCapability.getCapabilityName(), capability);

				if(can)
					break;
			}

		return can;
	}

	private boolean check(String pattern, String value) {
		return Pattern.matches(value.replaceAll("\\.", "\\\\.").replaceAll("\\?", ".").replaceAll("\\*", ".*"), pattern);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._capabilities == null) ? 0 : this._capabilities.hashCode());
		result = prime * result + ((this._id == null) ? 0 : this._id.hashCode());
		result = prime * result + ((this._roles == null) ? 0 : this._roles.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicUser other = (BasicUser) obj;
		if (this._capabilities == null) {
			if (other._capabilities != null)
				return false;
		} else if (!this._capabilities.equals(other._capabilities))
			return false;
		if (this._id == null) {
			if (other._id != null)
				return false;
		} else if (!this._id.equals(other._id))
			return false;
		if (this._roles == null) {
			if (other._roles != null)
				return false;
		} else if (!this._roles.equals(other._roles))
			return false;
		return true;
	}
}