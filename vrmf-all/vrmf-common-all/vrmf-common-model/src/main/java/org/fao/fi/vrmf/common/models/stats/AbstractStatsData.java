/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.stats;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Aug 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Aug 2012
 */
abstract public class AbstractStatsData extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8202645341025261865L;
	
	private Integer _numberOfVessels;

	/**
	 * @return the 'numberOfVessels' value
	 */
	public Integer getNumberOfVessels() {
		return this._numberOfVessels;
	}

	/**
	 * @param numberOfVessels the 'numberOfVessels' value to set
	 */
	public void setNumberOfVessels(Integer numberOfVessels) {
		this._numberOfVessels = numberOfVessels;
	}
}
