/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Jan 2011
 */
abstract public class SimpleGroupedSearchResult extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6579870813179259268L;
	
	private String _term;
	private Integer _totalOccurrencies;	
	private Integer _occurrencies;	
	private Integer _groupedOccurrencies;
	
	/**
	 * Class constructor
	 *
	 */
	public SimpleGroupedSearchResult() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param term
	 * @param totalOccurrencies
	 * @param occurrencies
	 * @param groupedOccurrencies
	 */
	public SimpleGroupedSearchResult(String term, Integer totalOccurrencies, Integer occurrencies, Integer groupedOccurrencies) {
		super();
		this._term = term;
		this._totalOccurrencies = totalOccurrencies;
		this._groupedOccurrencies = groupedOccurrencies;
		this._occurrencies = occurrencies;
	}

	/**
	 * @return the 'term' value
	 */
	public String getTerm() {
		return this._term;
	}

	/**
	 * @param term the 'term' value to set
	 */
	public void setTerm(String term) {
		this._term = term;
	}
	
	/**
	 * @return the 'totalOccurrencies' value
	 */
	public Integer getTotalOccurrencies() {
		return this._totalOccurrencies;
	}

	/**
	 * @param totalOccurrencies the 'totalOccurrencies' value to set
	 */
	public void setTotalOccurrencies(Integer totalOccurrencies) {
		this._totalOccurrencies = totalOccurrencies;
	}	

	/**
	 * @return the 'occurrencies' value
	 */
	final public Integer getOccurrencies() {
		return this._occurrencies;
	}

	/**
	 * @param occurrencies the 'occurrencies' value to set
	 */
	final public void setOccurrencies(Integer occurrencies) {
		this._occurrencies = occurrencies;
	}

	/**
	 * @return the 'groupedOccurrencies' value
	 */
	public Integer getGroupedOccurrencies() {
		return this._groupedOccurrencies;
	}

	/**
	 * @param groupedOccurrencies the 'groupedOccurrencies' value to set
	 */
	public void setGroupedOccurrencies(Integer groupedOccurrencies) {
		this._groupedOccurrencies = groupedOccurrencies;
	}
}