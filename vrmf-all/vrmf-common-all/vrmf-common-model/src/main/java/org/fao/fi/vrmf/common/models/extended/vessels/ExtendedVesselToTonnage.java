/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended.vessels;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.sh.utility.common.helpers.singletons.lang.BeansHelper;
import org.fao.fi.vrmf.common.models.generated.STonnageTypes;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Dec 2010
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public class ExtendedVesselToTonnage extends VesselsToTonnage {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3849406546807126599L;
	
	@XmlElement
	@Getter @Setter private STonnageTypes tonnageType;
	
	/**
	 * Class constructor
	 *
	 * @param source
	 * @param tonnageType
	 */
	public ExtendedVesselToTonnage(VesselsToTonnage source, STonnageTypes tonnageType) {
		super();
		
		BeansHelper.transferBean(source, this);
		
		this.tonnageType = tonnageType;
	}
}