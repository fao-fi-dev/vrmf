/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.security;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Mar 2011
 */
@XmlType(name="UserCapability")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserCapability implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 9028506326798099373L;
	
	@XmlElement(name="CapabilityName")
	private String _capabilityName;
	
	@XmlElement(name="CapabilityParameters")
	private String _capabilityParameters;
	
	public UserCapability() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param capabilityName
	 */
	public UserCapability(String capabilityName) {
		super();
		this._capabilityName = capabilityName;
	}

	/**
	 * Class constructor
	 *
	 * @param capabilityName
	 * @param capabilityParameters
	 */
	public UserCapability(String capabilityName, String capabilityParameters) {
		super();
		this._capabilityName = capabilityName;
		this._capabilityParameters = capabilityParameters;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.models.security.Capability#getCapabilityName()
	 */
	public String getCapabilityName() { return this._capabilityName; }

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.models.security.Capability#getCapabilityParameters()
	 */
	public String getCapabilityParameters() { return this._capabilityParameters; }

	/**
	 * @param capabilityName the 'capabilityName' value to set
	 */
	public void setCapabilityName(String capabilityName) { this._capabilityName = capabilityName; }

	/**
	 * @param capabilityParameters the 'capabilityParameters' value to set
	 */
	public void setCapabilityParameters(String capabilityParameters) { this._capabilityParameters = capabilityParameters; }

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._capabilityName == null) ? 0 : this._capabilityName.hashCode());
		result = prime * result + ((this._capabilityParameters == null) ? 0 : this._capabilityParameters.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UserCapability))
			return false;
		UserCapability other = (UserCapability) obj;
		if (this._capabilityName == null) {
			if (other._capabilityName != null)
				return false;
		} else if (!this._capabilityName.equals(other._capabilityName))
			return false;
		if (this._capabilityParameters == null) {
			if (other._capabilityParameters != null)
				return false;
		} else if (!this._capabilityParameters.equals(other._capabilityParameters))
			return false;
		return true;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	final public String toString() {
		return this._capabilityName + ( StringsHelper.isNullOrEmpty(this._capabilityParameters) ? "" : " { " + this._capabilityParameters + " }" );
	}	
}