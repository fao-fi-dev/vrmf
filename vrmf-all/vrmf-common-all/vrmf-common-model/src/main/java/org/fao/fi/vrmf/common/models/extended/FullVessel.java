/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.model.BasicPair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.core.model.support.comparators.DateReferencedUpdatableAscendingComparator;
import org.fao.fi.vrmf.common.core.model.support.comparators.DateReferencedUpdatableComparator;
import org.fao.fi.vrmf.common.core.model.support.comparators.DateReferencedUpdatableDescendingComparator;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToBeneficialOwner;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToCallsigns;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToFishingLicense;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToFishingMaster;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToFlags;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToGear;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToHullMaterial;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToInspection;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToIuuList;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToLengths;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToMaster;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToNonCompliance;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToOperator;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToOwner;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToPortEntryDenial;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToPower;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToRegistration;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToShipbuilder;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToStatus;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToTonnage;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToType;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.VesselsToExternalMarkings;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToMmsi;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Dec 2010
 */
@XmlRootElement(name="Vessel")
@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode(callSuper=true) @NoArgsConstructor
public class FullVessel extends ExtendedVessel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2240056356770070341L;
	
	@XmlElementWrapper(name="Flags")
	@XmlElement(name="Flag")	
	@Getter private List<ExtendedVesselToFlags> flags;
	
	@XmlElementWrapper(name="Shipbuilders")
	@XmlElement(name="Shipbuilder")
	@Getter private List<ExtendedVesselToShipbuilder> shipbuilders;

	@XmlElementWrapper(name="Statuses")
	@XmlElement(name="Status")
	@Getter private List<ExtendedVesselToStatus> status;
	
	@XmlElementWrapper(name="HullMaterials")
	@XmlElement(name="HullMaterial")	
	@Getter private List<ExtendedVesselToHullMaterial> hull;
	
	@XmlElementWrapper(name="Types")
	@XmlElement(name="Type")
	@Getter private List<ExtendedVesselToType> types;

	@XmlElementWrapper(name="Gears")
	@XmlElement(name="Gear")
	@Getter private List<ExtendedVesselToGear> gears;
	
	@XmlElementWrapper(name="Callsigns")
	@XmlElement(name="Callsign")
	@Getter private List<ExtendedVesselToCallsigns> callsigns;

	@XmlElementWrapper(name="FishingLicenses")
	@XmlElement(name="FishingLicense")
	@Getter private List<ExtendedVesselToFishingLicense> fishingLicenses;
	
	@XmlElementWrapper(name="Registrations")
	@XmlElement(name="Registration")
	@Getter private List<ExtendedVesselToRegistration> registrations;
	
	@XmlElementWrapper(name="Lengths")
	@XmlElement(name="Length")
	@Getter private List<ExtendedVesselToLengths> lengths;
	
	@XmlElementWrapper(name="Tonnages")
	@XmlElement(name="Tonnage")
	@Getter private List<ExtendedVesselToTonnage> tonnages;
	
	@XmlElementWrapper(name="EnginePowers")
	@XmlElement(name="EnginePower")
	@Getter private List<ExtendedVesselToPower> powers;

	@XmlElementWrapper(name="BeneficialOwners")
	@XmlElement(name="BeneficialOwner")
	@Getter private List<ExtendedVesselToBeneficialOwner> beneficialOwners;
	
	@XmlElementWrapper(name="Owners")
	@XmlElement(name="Owner")
	@Getter private List<ExtendedVesselToOwner> owners;
	
	@XmlElementWrapper(name="Operators")
	@XmlElement(name="Operator")
	@Getter private List<ExtendedVesselToOperator> operators;
	
	@XmlElementWrapper(name="FishingMasters")
	@XmlElement(name="FishingMaster")
	@Getter private List<ExtendedVesselToFishingMaster> fishingMasters;
	
	@XmlElementWrapper(name="Masters")
	@XmlElement(name="Master")
	@Getter private List<ExtendedVesselToMaster> masters;
	
	@XmlElementWrapper(name="Inspections")
	@XmlElement(name="Inspection")
	@Getter private List<ExtendedVesselToInspection> inspections;
	
	@XmlElementWrapper(name="NonCompliances")
	@XmlElement(name="NonCompliance")
	@Getter private List<ExtendedVesselToNonCompliance> nonCompliance;
	
	@XmlElementWrapper(name="IUULists")
	@XmlElement(name="IUUList")
	@Getter private List<ExtendedVesselToIuuList> iuuLists;
	
	@XmlElementWrapper(name="PortEntryDenials")
	@XmlElement(name="PortEntryDenial")
	@Getter private List<ExtendedVesselToPortEntryDenial> portEntryDenials;
	
	@XmlElementWrapper(name="Authorizations")
	@XmlElement(name="Authorization")
	@Getter private List<ExtendedAuthorizations> authorizations;
	
	@XmlElementWrapper(name="AllAvailableSourceSystems")
	@XmlElement(name="SourceSystem")
	@Getter @Setter private List<String> allAvailableSourceSystemsForVessel;

	@SuppressWarnings("unused")
	static final private DateReferencedUpdatableComparator ASCENDING_COMPARATOR = new DateReferencedUpdatableAscendingComparator();
	static final private DateReferencedUpdatableComparator DESCENDING_COMPARATOR = new DateReferencedUpdatableDescendingComparator();
	
	public FullVessel(ExtendedVessel source) {
		this(source, false);
	}

	public FullVessel(ExtendedVessel source, boolean preserveOrder) {
		super(source);

		if(source != null) {
			this.setComponentVessels(source.getComponentVessels());
			this.setIds(source.getIds());
			this.setSourceSystems(source.getSourceSystems());
			this.setNumberOfSystems(source.getNumberOfSystems());

			this.setIdentifiers(source.getIdentifiers());

			this.setBuildingYearData(source.getBuildingYearData(), preserveOrder);
			this.setShipbuildersData(source.getShipbuildersData(), preserveOrder);

			this.setAuthorizationsData(source.getAuthorizationsData(), preserveOrder);
			this.setNonComplianceData(source.getNonComplianceData(), preserveOrder);
			this.setInspectionsData(source.getInspectionsData(), preserveOrder);
			this.setIuuListsData(source.getIuuListsData(), preserveOrder);
			this.setPortEntryDenialsData(source.getPortEntryDenialsData(), preserveOrder);
			
			this.setStatusData(source.getStatusData(), preserveOrder);
			this.setHullMaterialData(source.getHullMaterialData(), preserveOrder);

			this.setTypeData(source.getTypeData(), preserveOrder);

			this.setFlagData(source.getFlagData(), preserveOrder);

			this.setCallsignData(source.getCallsignData(), preserveOrder);
			this.setMMSIData(source.getMMSIData(), preserveOrder);
			this.setVMSData(source.getVMSData(), preserveOrder);
			
			this.setRegistrationData(source.getRegistrationData(), preserveOrder);
			this.setFishingLicenseData(source.getFishingLicenseData(), preserveOrder);

			this.setNameData(source.getNameData(), preserveOrder);
			this.setExternalMarkingsData(source.getExternalMarkingsData(), preserveOrder);

			this.setGearData(source.getGearData(), preserveOrder);

			this.setCrewData(source.getCrewData(), preserveOrder);

			this.setLengthData(source.getLengthData(), preserveOrder);
			this.setPowerData(source.getPowerData(), preserveOrder);
			this.setTonnageData(source.getTonnageData(), preserveOrder);

			this.setOperatorsData(source.getOperatorsData(), preserveOrder);
			this.setBeneficialOwnersData(source.getBeneficialOwnersData(), preserveOrder);
			this.setOwnersData(source.getOwnersData(), preserveOrder);
			
			this.setMastersData(source.getMastersData(), preserveOrder);
			this.setFishingMastersData(source.getFishingMastersData(), preserveOrder);
		}

		this.authorizations = new ListSet<ExtendedAuthorizations>();
		this.nonCompliance = new ListSet<ExtendedVesselToNonCompliance>();
		this.inspections = new ListSet<ExtendedVesselToInspection>();
		this.iuuLists = new ListSet<ExtendedVesselToIuuList>();
		this.portEntryDenials = new ListSet<ExtendedVesselToPortEntryDenial>();
		this.callsigns = new ListSet<ExtendedVesselToCallsigns>();
		this.fishingLicenses = new ListSet<ExtendedVesselToFishingLicense>();
		this.flags = new ListSet<ExtendedVesselToFlags>();
		this.gears = new ListSet<ExtendedVesselToGear>();
		this.lengths = new ListSet<ExtendedVesselToLengths>();
		this.operators = new ListSet<ExtendedVesselToOperator>();
		this.owners = new ListSet<ExtendedVesselToOwner>();
		this.powers = new ListSet<ExtendedVesselToPower>();
		this.registrations = new ListSet<ExtendedVesselToRegistration>();
		this.shipbuilders = new ListSet<ExtendedVesselToShipbuilder>();
		this.status = new ListSet<ExtendedVesselToStatus>();
		this.tonnages = new ListSet<ExtendedVesselToTonnage>();
		this.types = new ListSet<ExtendedVesselToType>();
		
		this.fishingMasters = new ListSet<ExtendedVesselToFishingMaster>();
		this.masters = new ListSet<ExtendedVesselToMaster>();
		
		this.beneficialOwners = new ListSet<ExtendedVesselToBeneficialOwner>();
		
		this.hull = new ListSet<ExtendedVesselToHullMaterial>();

		this.allAvailableSourceSystemsForVessel = new ListSet<String>();
	}

	public FullVessel(FullVessel source) {
		this(source, false);
	}

	public FullVessel(FullVessel source, boolean preserveOrder) {
		this((ExtendedVessel)source, preserveOrder);

		if(source != null) {
			this.setAuthorizations(source.getAuthorizations(), preserveOrder);
			this.setInspections(source.getInspections(), preserveOrder);
			this.setNonCompliance(source.getNonCompliance(), preserveOrder);
			this.setCallsigns(source.getCallsigns(), preserveOrder);
			this.setFlags(source.getFlags(), preserveOrder);
			this.setGears(source.getGears(), preserveOrder);
			this.setLengths(source.getLengths(), preserveOrder);
			this.setOperators(source.getOperators(), preserveOrder);
			this.setOwners(source.getOwners(), preserveOrder);
			this.setRegistrations(source.getRegistrations(), preserveOrder);
			this.setFishingLicenses(source.getFishingLicenses(), preserveOrder);
			this.setShipbuilders(source.getShipbuilders(), preserveOrder);
			this.setStatus(source.getStatus(), preserveOrder);
			this.setTonnages(source.getTonnages(), preserveOrder);
			this.setTypes(source.getTypes(), preserveOrder);
			this.allAvailableSourceSystemsForVessel = source.allAvailableSourceSystemsForVessel;
		}
	}

	public void setHull(List<ExtendedVesselToHullMaterial> hulls) {
		this.setHull(hulls, false);
	}
	
	public void setHull(List<ExtendedVesselToHullMaterial> hulls, boolean preserveOrder) {
		this.hull.clear();

		if(hulls != null) {
			this.hull.addAll(hulls);

			if(!preserveOrder)
				Collections.sort(this.hull, DESCENDING_COMPARATOR);
		}
	}

	/**
	 * @param beneficialOwners the 'beneficialOwners' value to set
	 */
	public void setBeneficialOwners(List<ExtendedVesselToBeneficialOwner> beneficialOwners) {
		this.setBeneficialOwners(beneficialOwners, false);
	}
	
	/**
	 * @param beneficialOwners the 'beneficialOwners' value to set
	 */
	public void setBeneficialOwners(List<ExtendedVesselToBeneficialOwner> beneficialOwners, boolean preserveOrder) {
		this.beneficialOwners.clear();

		if(beneficialOwners != null) {
			this.beneficialOwners.addAll(beneficialOwners);

			if(!preserveOrder)
				Collections.sort(this.beneficialOwners, DESCENDING_COMPARATOR);
		}
	}

	/**
	 * @param masters the 'masters' value to set
	 */
	public void setMasters(List<ExtendedVesselToMaster> masters) {
		this.setMasters(masters, false);
	}
	
	/**
	 * @param masters the 'masters' value to set
	 */
	public void setMasters(List<ExtendedVesselToMaster> masters, boolean preserveOrder) {
		this.masters.clear();

		if(masters != null) {
			this.masters.addAll(masters);

			if(!preserveOrder)
				Collections.sort(this.masters, DESCENDING_COMPARATOR);
		}
	}

	/**
	 * @param fishingMasters the 'fishingMasters' value to set
	 */
	public void setFishingMasters(List<ExtendedVesselToFishingMaster> fishingMasters) {
		this.setFishingMasters(fishingMasters, false);
	}

	/**
	 * @param masters the 'masters' value to set
	 */
	public void setFishingMasters(List<ExtendedVesselToFishingMaster> fishingMasters, boolean preserveOrder) {
		this.fishingMasters.clear();

		if(fishingMasters != null) {
			this.fishingMasters.addAll(fishingMasters);

			if(!preserveOrder)
				Collections.sort(this.fishingMasters, DESCENDING_COMPARATOR);
		}
	}

	public void setLengths(List<ExtendedVesselToLengths> lengths) {
		this.setLengths(lengths, false);
	}

	/**
	 * @param lengths the 'lengths' value to set
	 */
	public void setLengths(List<ExtendedVesselToLengths> lengths, boolean preserveOrder) {
		this.lengths.clear();

		if(lengths != null) {
			this.lengths.addAll(lengths);

			if(!preserveOrder)
				Collections.sort(this.lengths, DESCENDING_COMPARATOR);
		}
	}

	public void setTonnages(List<ExtendedVesselToTonnage> tonnages) {
		this.setTonnages(tonnages, false);
	}

	/**
	 * @param tonnages the 'tonnages' value to set
	 */
	public void setTonnages(List<ExtendedVesselToTonnage> tonnages, boolean preserveOrder) {
		this.tonnages.clear();

		if(tonnages != null) {
			this.tonnages.addAll(tonnages);

			if(!preserveOrder)
				Collections.sort(this.tonnages, DESCENDING_COMPARATOR);
		}
	}

	public void setPowers(List<ExtendedVesselToPower> powers) {
		this.setPowers(powers, false);
	}

	/**
	 * @param tonnages the 'tonnages' value to set
	 */
	public void setPowers(List<ExtendedVesselToPower> powers, boolean preserveOrder) {
		this.powers.clear();

		if(powers != null) {
			this.powers.addAll(powers);

			if(!preserveOrder)
				Collections.sort(this.powers, DESCENDING_COMPARATOR);
		}
	}

	public void setShipbuilders(List<ExtendedVesselToShipbuilder> shipbuilders) {
		this.setShipbuilders(shipbuilders, false);
	}

	/**
	 * @param shipbuilders the 'shipbuilders' value to set
	 */
	public void setShipbuilders(List<ExtendedVesselToShipbuilder> shipbuilders, boolean preserveOrder) {
		this.shipbuilders.clear();

		if(shipbuilders != null) {
			this.shipbuilders.addAll(shipbuilders);

			if(!preserveOrder)
				Collections.sort(this.shipbuilders, DESCENDING_COMPARATOR);
		}
	}

	public void setOwners(List<ExtendedVesselToOwner> owners) {
		this.setOwners(owners, false);
	}

	/**
	 * @param owners the 'owners' value to set
	 */
	public void setOwners(List<ExtendedVesselToOwner> owners, boolean preserveOrder) {
		this.owners.clear();

		if(owners != null) {
			this.owners.addAll(owners);

			if(!preserveOrder)
				Collections.sort(this.owners, DESCENDING_COMPARATOR);
		}
	}

	public void setOperators(List<ExtendedVesselToOperator> operators) {
		this.setOperators(operators, false);
	}

	/**
	 * @param operators the 'operators' value to set
	 */
	public void setOperators(List<ExtendedVesselToOperator> operators, boolean preserveOrder) {
		this.operators.clear();

		if(operators != null) {
			this.operators.addAll(operators);

			if(!preserveOrder)
				Collections.sort(this.operators, DESCENDING_COMPARATOR);
		}
	}

	public void setTypes(List<ExtendedVesselToType> types) {
		this.setTypes(types, false);
	}

	/**
	 * @param types the 'types' value to set
	 */
	public void setTypes(List<ExtendedVesselToType> types, boolean preserveOrder) {
		this.types.clear();

		if(types != null) {
			this.types.addAll(types);

			if(!preserveOrder)
				Collections.sort(this.types, DESCENDING_COMPARATOR);
		}
	}

	public void setGears(List<ExtendedVesselToGear> gears) {
		this.setGears(gears, false);
	}

	/**
	 * @param gears the 'gears' value to set
	 */
	public void setGears(List<ExtendedVesselToGear> gears, boolean preserveOrder) {
		this.gears.clear();

		if(gears != null) {
			this.gears.addAll(gears);

			if(!preserveOrder)
				Collections.sort(this.gears, DESCENDING_COMPARATOR);
		}
	}

	public void setRegistrations(List<ExtendedVesselToRegistration> registrations) {
		this.setRegistrations(registrations, false);
	}

	/**
	 * @param registrations the 'registrations' value to set
	 */
	public void setRegistrations(List<ExtendedVesselToRegistration> registrations, boolean preserveOrder) {
		this.registrations.clear();

		if(registrations != null) {
			this.registrations.addAll(registrations);

			if(!preserveOrder)
				Collections.sort(this.registrations, DESCENDING_COMPARATOR);
		}
	}

	public void setStatus(List<ExtendedVesselToStatus> status) {
		this.setStatus(status, false);
	}

	/**
	 * @param status the 'status' value to set
	 */
	public void setStatus(List<ExtendedVesselToStatus> status, boolean preserveOrder) {
		this.status.clear();

		if(status != null) {
			this.status.addAll(status);

			if(!preserveOrder)
				Collections.sort(this.status, DESCENDING_COMPARATOR);
		}
	}

	public void setFlags(List<ExtendedVesselToFlags> flags) {
		this.setFlags(flags, false);
	}

	/**
	 * @param flags the 'flags' value to set
	 */
	public void setFlags(List<ExtendedVesselToFlags> flags, boolean preserveOrder) {
		this.flags.clear();

		if(flags != null) {
			this.flags.addAll(flags);

			if(!preserveOrder)
				Collections.sort(this.flags, DESCENDING_COMPARATOR);
		}
	}

	public void setAuthorizations(List<ExtendedAuthorizations> authorizations) {
		this.setAuthorizations(authorizations, false);
	}

	/**
	 * @param authorizations the 'authorizations' value to set
	 */
	public void setAuthorizations(List<ExtendedAuthorizations> authorizations, boolean preserveOrder) {
		this.authorizations.clear();

		if(authorizations != null) {
			this.authorizations.addAll(authorizations);

			if(!preserveOrder)
				Collections.sort(this.authorizations, DESCENDING_COMPARATOR);
		}
	}
	
	/**
	 * @param nonCompliance the 'nonCompliance' value to set
	 */
	public void setNonCompliance(List<ExtendedVesselToNonCompliance> nonCompliance) {
		this.setNonCompliance(nonCompliance, false);
	}
	
	/**
	 * @param nonCompliance the 'nonCompliance' value to set
	 */
	public void setNonCompliance(List<ExtendedVesselToNonCompliance> nonCompliance, boolean preserveOrder) {
		this.nonCompliance.clear();

		if(nonCompliance != null) {
			this.nonCompliance.addAll(nonCompliance);

			if(!preserveOrder)
				Collections.sort(this.nonCompliance, DESCENDING_COMPARATOR);
		}
	}

	/**
	 * @param inspections the 'inspections' value to set
	 */
	public void setInspections(List<ExtendedVesselToInspection> inspections) {
		this.setInspections(inspections, false);
	}
	
	/**
	 * @param inspections the 'nonCompliance' value to set
	 */
	public void setInspections(List<ExtendedVesselToInspection> inspections, boolean preserveOrder) {
		this.inspections.clear();

		if(inspections != null) {
			this.inspections.addAll(inspections);

			if(!preserveOrder)
				Collections.sort(this.inspections, DESCENDING_COMPARATOR);
		}
	}

	/**
	 * @param iuuLists the 'iuuLists' value to set
	 */
	public void setIuuLists(List<ExtendedVesselToIuuList> iuuLists) {
		this.setIuuLists(iuuLists, false);
	}

	/**
	 * @param iuuLists the 'iuuLists' value to set
	 */
	public void setIuuLists(List<ExtendedVesselToIuuList> iuuLists, boolean preserveOrder) {
		this.iuuLists.clear();

		if(iuuLists != null) {
			this.iuuLists.addAll(iuuLists);

			if(!preserveOrder)
				Collections.sort(this.iuuLists, DESCENDING_COMPARATOR);
		}
	}
		
	/**
	 * @param portEntryDenials the 'portEntryDenials' value to set
	 */
	public void setPortEntryDenials(List<ExtendedVesselToPortEntryDenial> portEntryDenials) {
		this.setPortEntryDenials(portEntryDenials, false);
	}

	/**
	 * @param portEntryDenials the 'portEntryDenials' value to set
	 */
	public void setPortEntryDenials(List<ExtendedVesselToPortEntryDenial> portEntryDenials, boolean preserveOrder) {
		this.portEntryDenials.clear();

		if(portEntryDenials != null) {
			this.portEntryDenials.addAll(portEntryDenials);

			if(!preserveOrder)
				Collections.sort(this.portEntryDenials, DESCENDING_COMPARATOR);
		}
	}
	
	public void setCallsigns(List<ExtendedVesselToCallsigns> callsigns) {
		this.setCallsigns(callsigns, false);
	}

	/**
	 * @param callsigns the 'callsigns' value to set
	 */
	public void setCallsigns(List<ExtendedVesselToCallsigns> callsigns, boolean preserveOrder) {
		this.callsigns.clear();

		if(callsigns != null) {
			this.callsigns.addAll(callsigns);

			if(!preserveOrder)
				Collections.sort(this.callsigns, DESCENDING_COMPARATOR);
		}
	}

	public void setFishingLicenses(List<ExtendedVesselToFishingLicense> fishingLicenses) {
		this.setFishingLicenses(fishingLicenses, false);
	}

	/**
	 * @param fishingLicenses the 'fishingLicenses' value to set
	 */
	public void setFishingLicenses(List<ExtendedVesselToFishingLicense> fishingLicenses, boolean preserveOrder) {
		this.fishingLicenses.clear();

		if(fishingLicenses != null) {
			this.fishingLicenses.addAll(fishingLicenses);

			if(!preserveOrder)
				Collections.sort(this.fishingLicenses, DESCENDING_COMPARATOR);
		}
	}

	public FullVessel join(FullVessel another) {
		FullVessel combined = new FullVessel(this, false);

		if(another == null || this.getId().equals(another.getId()))
			return this;

		if(another.getAuthorizationsData() != null) {
			combined.getAuthorizationsData().addAll(another.getAuthorizationsData());
			combined.authorizations.addAll(another.authorizations);

			Collections.sort(combined.getAuthorizationsData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.authorizations, DESCENDING_COMPARATOR);
		}
		
		if(another.getBeneficialOwnersData() != null) {
			combined.getBeneficialOwnersData().addAll(another.getBeneficialOwnersData());
			combined.beneficialOwners.addAll(another.beneficialOwners);

			Collections.sort(combined.getBeneficialOwnersData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.beneficialOwners, DESCENDING_COMPARATOR);
		}

		if(another.getBuildingYearData() != null) {
			combined.getBuildingYearData().addAll(another.getBuildingYearData());

			Collections.sort(combined.getBuildingYearData(), DESCENDING_COMPARATOR);
		}

		if(another.getCallsignData() != null) {
			combined.getCallsignData().addAll(another.getCallsignData());
			combined.callsigns.addAll(another.callsigns);

			Collections.sort(combined.getCallsignData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.callsigns, DESCENDING_COMPARATOR);
		}

		if(another.getCrewData() != null) {
			combined.getCrewData().addAll(another.getCrewData());
			Collections.sort(combined.getCrewData(), DESCENDING_COMPARATOR);
		}

		if(another.getExternalMarkingsData() != null) {
			combined.getExternalMarkingsData().addAll(another.getExternalMarkingsData());
			Collections.sort(combined.getExternalMarkingsData(), DESCENDING_COMPARATOR);
		}
		
		if(another.getFishingLicenseData() != null) {
			combined.getFishingLicenseData().addAll(another.getFishingLicenseData());
			combined.fishingLicenses.addAll(another.fishingLicenses);

			Collections.sort(combined.getFishingLicenseData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.fishingLicenses, DESCENDING_COMPARATOR);
		}
		
		if(another.getFishingMastersData() != null) {
			combined.getFishingMastersData().addAll(another.getFishingMastersData());
			combined.fishingMasters.addAll(another.fishingMasters);

			Collections.sort(combined.getFishingMastersData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.fishingMasters, DESCENDING_COMPARATOR);
		}

		if(another.getFlagData() != null) {
			combined.getFlagData().addAll(another.getFlagData());
			combined.flags.addAll(another.flags);

			Collections.sort(combined.getFlagData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.flags, DESCENDING_COMPARATOR);
		}

		if(another.getGearData() != null) {
			combined.getGearData().addAll(another.getGearData());
			combined.gears.addAll(another.gears);

			Collections.sort(combined.getGearData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.gears, DESCENDING_COMPARATOR);
		}
		
		if(another.getHullMaterialData() != null) {
			combined.getHullMaterialData().addAll(another.getHullMaterialData());
			combined.hull.addAll(another.hull);

			Collections.sort(combined.getHullMaterialData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.hull, DESCENDING_COMPARATOR);
		}

		if(another.getIdentifiers() != null) {
			combined.getIdentifiers().addAll(another.getIdentifiers());
			Collections.sort(combined.getIdentifiers(), DESCENDING_COMPARATOR);
		}

		if(another.getLengthData() != null) {
			combined.getLengthData().addAll(another.getLengthData());
			combined.lengths.addAll(another.lengths);

			Collections.sort(combined.getLengthData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.lengths, DESCENDING_COMPARATOR);
		}
		
		if(another.getMastersData() != null) {
			combined.getMastersData().addAll(another.getMastersData());
			combined.masters.addAll(another.masters);

			Collections.sort(combined.getMastersData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.masters, DESCENDING_COMPARATOR);
		}

		if(another.getMMSIData() != null) {
			combined.getMMSIData().addAll(another.getMMSIData());

			Collections.sort(combined.getMMSIData(), DESCENDING_COMPARATOR);
		}

		if(another.getNameData() != null) {
			combined.getNameData().addAll(another.getNameData());

			Collections.sort(combined.getNameData(), DESCENDING_COMPARATOR);
		}

		if(another.getOperatorsData() != null) {
			combined.getOperatorsData().addAll(another.getOperatorsData());
			combined.operators.addAll(another.operators);

			Collections.sort(combined.getOperatorsData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.operators, DESCENDING_COMPARATOR);
		}

		if(another.getOwnersData() != null) {
			combined.getOwnersData().addAll(another.getOwnersData());
			combined.owners.addAll(another.owners);

			Collections.sort(combined.getOwnersData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.owners, DESCENDING_COMPARATOR);
		}

		if(another.getPowerData() != null) {
			combined.getPowerData().addAll(another.getPowerData());
			combined.powers.addAll(another.powers);

			Collections.sort(combined.getPowerData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.powers, DESCENDING_COMPARATOR);
		}

		if(another.getRegistrationData() != null) {
			combined.getRegistrationData().addAll(another.getRegistrationData());
			combined.registrations.addAll(another.registrations);

			Collections.sort(combined.getRegistrationData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.registrations, DESCENDING_COMPARATOR);
		}

		if(another.getShipbuildersData() != null) {
			combined.getShipbuildersData().addAll(another.getShipbuildersData());
			combined.shipbuilders.addAll(another.shipbuilders);

			Collections.sort(combined.getShipbuildersData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.shipbuilders, DESCENDING_COMPARATOR);
		}

		if(another.getStatusData() != null) {
			combined.getStatusData().addAll(another.getStatusData());
			combined.status.addAll(another.status);

			Collections.sort(combined.getStatusData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.status, DESCENDING_COMPARATOR);
		}

		if(another.getTonnageData() != null) {
			combined.getTonnageData().addAll(another.getTonnageData());
			combined.tonnages.addAll(another.tonnages);

			Collections.sort(combined.getTonnageData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.tonnages, DESCENDING_COMPARATOR);
		}

		if(another.getTypeData() != null) {
			combined.getTypeData().addAll(another.getTypeData());
			combined.types.addAll(another.types);

			Collections.sort(combined.getTypeData(), DESCENDING_COMPARATOR);
			Collections.sort(combined.types, DESCENDING_COMPARATOR);
		}
		
		if(another.getVMSData() != null) {
			combined.getVMSData().addAll(another.getVMSData());

			Collections.sort(combined.getVMSData(), DESCENDING_COMPARATOR);
		}

		return combined;
	}

	public boolean hasData() {
		return super.hasData() ||
				(this.getFlags() != null && !this.getFlags().isEmpty()) ||
				(this.getShipbuilders() != null && !this.getShipbuilders().isEmpty()) ||
				(this.getStatus() != null && !this.getStatus().isEmpty()) ||
				(this.getTypes() != null && !this.getTypes().isEmpty()) ||
				(this.getGears() != null && !this.getGears().isEmpty()) ||
				(this.getCallsigns() != null && !this.getCallsigns().isEmpty()) ||
				(this.getFishingLicenses() != null && !this.getFishingLicenses().isEmpty()) ||
				(this.getRegistrations() != null && !this.getRegistrations().isEmpty()) ||
				(this.getLengths() != null && !this.getLengths().isEmpty()) ||
				(this.getTonnages() != null && !this.getTonnages().isEmpty()) ||
				(this.getPowers() != null && !this.getPowers().isEmpty()) ||
				(this.getOwners() != null && !this.getOwners().isEmpty()) ||
				(this.getOperators() != null && !this.getOperators().isEmpty()) ||
				(this.getAuthorizations() != null && !this.getAuthorizations().isEmpty());
	}

	private void addCountryData(Collection<String> target, SCountries country) {
		if(target != null && country != null) {
			target.add(country.getName() + ( country.getIso2Code() != null ? " [ " + country.getIso2Code() + ", " + country.getIso3Code() + " ]" : "" ) );
		}
	}
	
	public Collection<BasicPair<String, Set<String>>> getKeywords() {
		Collection<BasicPair<String, Set<String>>> keywords = new ArrayList<BasicPair<String, Set<String>>>();

		if(this.getPublicIdentifiers() != null) {
			ListSet<String> data = new ListSet<String>();

			for(VesselsToIdentifiers identifier : this.getPublicIdentifiers()) {
				data.add(identifier.getTypeId().replaceAll("\\_", " ").replaceAll("TUVI", "CLAV") + ": " + identifier.getIdentifier());
			}

			if(!data.isEmpty())
				keywords.add(new BasicPair<String, Set<String>>("Identifier" + ( data.size() == 1 ? "": "s" ), data));
		}

		if(this.getNameData() != null) {
			ListSet<String> data = new ListSet<String>();

			for(VesselsToName name : this.getNameData()) {
				data.add(name.getName());

				if(!name.getName().equalsIgnoreCase(name.getSimplifiedName()))
					data.add(name.getSimplifiedName());
			}

			if(!data.isEmpty())
				keywords.add(new BasicPair<String, Set<String>>("Name" + ( data.size() == 1 ? "": "s" ), data));
		}

		ListSet<String> countries = new ListSet<String>();
		ListSet<String> flags = new ListSet<String>();

		if(this.getFlags() != null) {
			for(ExtendedVesselToFlags flag : this.getFlags()) {
				this.addCountryData(flags, flag.getVesselFlag());
				this.addCountryData(countries, flag.getVesselFlag());
			}

			if(!flags.isEmpty())
				keywords.add(new BasicPair<String, Set<String>>("Flag" + ( flags.size() == 1 ? "": "s" ), flags));
		}

		if(this.getShipbuilders() != null) {
			ListSet<String> data = new ListSet<String>();

			ExtendedEntity entity;
			for(ExtendedVesselToShipbuilder shipbuilder : this.getShipbuilders()) {
				entity = shipbuilder.getShipbuilder();

				if(entity.getName() != null) {
					data.add(entity.getName());

					if(entity.getSimplifiedName() != null &&
					   !entity.getSimplifiedName().equalsIgnoreCase(entity.getName()))
					   data.add(entity.getSimplifiedName());
				}

				if(entity.getCity() != null)
					data.add(entity.getCity());

				if(entity.getAddress() != null)
					data.add(entity.getAddress());

				if(entity.getCountry() != null) {
					this.addCountryData(flags, entity.getCountry());
					this.addCountryData(countries, entity.getCountry());
				}
			}

			if(!data.isEmpty())
				keywords.add(new BasicPair<String, Set<String>>("Shipbuilder" + ( data.size() == 1 ? "": "s" ), data));
		}

		if(this.getCallsignData() != null) {
			ListSet<String> data = new ListSet<String>();

			for(ExtendedVesselToCallsigns IRCS : this.getCallsigns()) {
				data.add(IRCS.getCallsignId());

				if(IRCS.getCallsignCountry() != null) {
					this.addCountryData(countries, IRCS.getCallsignCountry()); 
				}
			}

			if(!data.isEmpty())
				keywords.add(new BasicPair<String, Set<String>>("Callsign" + ( data.size() == 1 ? "": "s" ), data));
		}

		if(this.getMMSIData() != null) {
			ListSet<String> data = new ListSet<String>();

			for(VesselsToMmsi MMSI : this.getMMSIData()) {
				data.add(MMSI.getMmsi());
			}

			if(!data.isEmpty())
				keywords.add(new BasicPair<String, Set<String>>("MMSI" + ( data.size() == 1 ? "": "s" ), data));
		}

		if(this.getExternalMarkingsData() != null) {
			ListSet<String> data = new ListSet<String>();

			for(VesselsToExternalMarkings extMark : this.getExternalMarkingsData()) {
				data.add(extMark.getExternalMarking());
			}

			if(!data.isEmpty())
				keywords.add(new BasicPair<String, Set<String>>("External marking" + ( data.size() == 1 ? "": "s" ), data));
		}

		if(this.getFishingLicenseData() != null) {
			ListSet<String> data = new ListSet<String>();

			for(ExtendedVesselToFishingLicense fishingLicense : this.getFishingLicenses()) {
				data.add(fishingLicense.getLicenseId());

				if(fishingLicense.getFishingLicenseCountry() != null) {
					this.addCountryData(countries, fishingLicense.getFishingLicenseCountry()); 
				}
			}

			if(!data.isEmpty())
				keywords.add(new BasicPair<String, Set<String>>("Fishing license" + ( data.size() == 1 ? "": "s" ), data));
		}

		if(this.getRegistrations() != null) {
			ListSet<String> data = new ListSet<String>();

			String fullRegistration;
			
			for(ExtendedVesselToRegistration registration : this.getRegistrations()) {
				fullRegistration = registration.getRegistrationNumber();

				if(registration.getRegistrationPort() != null || registration.getRegistrationCountry() != null) {
					fullRegistration += " @ ";
				}
				
				if(registration.getRegistrationPort() != null)
					fullRegistration += registration.getRegistrationPort().getName();

				if(registration.getRegistrationCountry() != null) {
					fullRegistration += ( registration.getRegistrationPort() != null ? ", " : "" ) + registration.getRegistrationCountry().getName() + ( registration.getRegistrationCountry().getIso2Code() != null ? " [ " + registration.getRegistrationCountry().getIso2Code() + ", " + registration.getRegistrationCountry().getIso3Code() + " ]" : "" ); 
				}
				
				data.add(fullRegistration);
			}

			if(!data.isEmpty())
				keywords.add(new BasicPair<String, Set<String>>("Registration" + ( data.size() == 1 ? "": "s" ), data));
		}

		if(this.getTypes() != null) {
			ListSet<String> data = new ListSet<String>();

			ListSet<String> codes;
			
			for(ExtendedVesselToType type : this.getTypes()) {
				codes = new ListSet<String>();

				if(type.getVesselType().getStandardAbbreviation() != null) {
					codes.add(type.getVesselType().getStandardAbbreviation());
				}

				if(type.getVesselType().getIsscfvCode() != null) {
					codes.add(type.getVesselType().getIsscfvCode());
				}
				
				data.add(type.getVesselType().getName() + ( codes.isEmpty() ? "" : " [ " + CollectionsHelper.join(codes, ", ") + " ]" ));
			}

			if(!data.isEmpty())
				keywords.add(new BasicPair<String, Set<String>>("Vessel type" + ( data.size() == 1 ? "": "s" ), data));
		}

		if(this.getGears() != null) {
			ListSet<String> data = new ListSet<String>();

			ListSet<String> codes;
			
			for(ExtendedVesselToGear gear : this.getGears()) {
				codes = new ListSet<String>();
				
				if(gear.getGearType().getStandardAbbreviation() != null) {
					codes.add(gear.getGearType().getStandardAbbreviation());
				}

				if(gear.getGearType().getIsscfgCode() != null) {
					codes.add(gear.getGearType().getStandardAbbreviation());
				}

				data.add(gear.getGearType().getName() + ( codes.isEmpty() ? "" : " [ " + CollectionsHelper.join(codes, ", ") + " ]" ));
			}

			if(!data.isEmpty())
				keywords.add(new BasicPair<String, Set<String>>("Gear type" + ( data.size() == 1 ? "": "s" ), data));
		}

		if(!countries.isEmpty() && countries.size() != flags.size())
			keywords.add(new BasicPair<String, Set<String>>("Countr" + ( countries.size() == 1 ? "y" : "ies"), countries));
		
		return keywords;
	}
	
	public String getSerializedVesselKeywords(boolean withLabel) {
		Set<String> toSerialize = new ListSet<String>();

		StringBuilder serialized;
		String label;
		Set<String> data;
		for(BasicPair<String, Set<String>> keyword : this.getKeywords()) {
			label = keyword.getLeftPart();
			data = keyword.getRightPart();
			
			serialized = new StringBuilder();

			if(data != null && !data.isEmpty()) {
				serialized.append("{ ");
				if(withLabel && label != null)
					serialized.append(label).append(": ");
				
				serialized.append(CollectionsHelper.join(data, ", "));
				serialized.append(" }");
			}
			
			if(serialized.length() > 0)
				toSerialize.add(serialized.toString());
		}
		
		return CollectionsHelper.join(toSerialize, ", ");
	}
}