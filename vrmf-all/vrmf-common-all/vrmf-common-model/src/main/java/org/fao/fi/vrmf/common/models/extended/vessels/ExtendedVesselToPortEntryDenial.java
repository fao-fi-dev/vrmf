/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended.vessels;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.sh.utility.common.helpers.singletons.lang.BeansHelper;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SPortEntryDenialReason;
import org.fao.fi.vrmf.common.models.generated.SPorts;
import org.fao.fi.vrmf.common.models.generated.VesselsToPortEntryDenials;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Dec 2010
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public class ExtendedVesselToPortEntryDenial extends VesselsToPortEntryDenials {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8087583632648739615L;
	
	@XmlElement
	@Getter @Setter private SPortEntryDenialReason denialReason;
	
	@XmlElement
	@Getter @Setter private SCountries country;
	
	@XmlElement
	@Getter @Setter private SPorts port;
	
	/**
	 * Class constructor
	 *
	 * @param source
	 * @param owner
	 */
	public ExtendedVesselToPortEntryDenial(VesselsToPortEntryDenials source, 
										   SPortEntryDenialReason denialReason,
										   SCountries country,
										   SPorts port) {
		super();
		
		BeansHelper.transferBean(source, this);
		
		this.denialReason = denialReason;
		this.country = country;
		this.port = port;
	}
}