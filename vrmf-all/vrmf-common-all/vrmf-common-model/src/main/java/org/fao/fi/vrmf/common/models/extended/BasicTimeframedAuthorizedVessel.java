/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2010
 */
@XmlType(name="vessel")
public class BasicTimeframedAuthorizedVessel extends BasicVessel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7801806468193779051L;

	@XmlTransient
	private Long _maxTimeDelta;
	
	@XmlElement(name="authorizationType")
	private String _authTypeId;
	
	@XmlElement(name="authorizationIssuer")
	private String _authIssuer;
	
	@XmlElement(name="authorizationIssuingCountry")
	private Integer _authIssuingCountry;
	
	@XmlElement(name="authorizationStartDate")
	private Date _authFrom;
	
	@XmlElement(name="authorizationEndDate")
	private Date _authTo;
	
	@XmlElement(name="authorizationTerminationDate")
	private Date _authTerm;

	@XmlElement(name="authorizationTerminationReasonCode")
	private Integer _authTermCode;
	
	@XmlElement(name="authorizationTerminationReason")
	private String _authTermReason;
	
	@XmlElement(name="authorizationReferenceDate")
	private Date _authRef;
	
	
	/**
	 * Class constructor
	 */
	public BasicTimeframedAuthorizedVessel() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param id
	 * @param uid
	 * @param IMO
	 * @param currentFlagId
	 * @param currentName
	 * @param currentCallsign
	 * @param currentCallsignCountry
	 * @param currentRegNo
	 * @param currentRegPort
	 * @param currentRegCountry
	 * @param sourceSystem
	 * @param updateDate
	 * @param maxTimeDelta
	 */
	public BasicTimeframedAuthorizedVessel(Integer id, Integer uid, String IMO, String EUCFR, Integer currentFlagId, String currentName, String currentSimplifiedName, String currentCallsign, Integer currentCallsignCountry, String currentRegNo, String currentRegPort, Integer currentRegCountry, String sourceSystem, Date updateDate, long maxTimeDelta) {
		super(id, uid, IMO, EUCFR, currentFlagId, currentName, currentSimplifiedName, currentCallsign, currentCallsignCountry, currentRegNo, currentRegPort, currentRegCountry, sourceSystem, updateDate);

		this._maxTimeDelta = maxTimeDelta;
	}
	
	/**
	 * Class constructor
	 *
	 * @param id
	 * @param uid
	 * @param IMO
	 * @param currentFlagId
	 * @param currentName
	 * @param currentCallsign
	 * @param currentCallsignCountry
	 * @param currentRegNo
	 * @param currentRegPort
	 * @param currentRegCountry
	 * @param sourceSystem
	 * @param updateDate
	 * @param maxTimeDelta
	 * @param authIssuingCountry
	 * @param authFrom
	 * @param authTo
	 * @param authTerm
	 */
	public BasicTimeframedAuthorizedVessel(Integer id, Integer uid, String IMO, String EUCFR, Integer currentFlagId, String currentName, String currentSimplifiedName, String currentCallsign, Integer currentCallsignCountry, String currentRegNo, String currentRegPort, Integer currentRegCountry, String sourceSystem, Date updateDate, Integer maxTimeDelta, 
			 					 Integer authIssuingCountry, Date authFrom, Date authTo, Date authTerm) {
		this(id, uid, IMO, EUCFR, currentFlagId, currentName, currentSimplifiedName, currentCallsign, currentCallsignCountry, currentRegNo, currentRegPort, currentRegCountry, sourceSystem, updateDate, maxTimeDelta);
		
		this._authIssuingCountry = authIssuingCountry;
		this._authFrom = authFrom;
		this._authTo = authTo;
		this._authTerm = authTerm;
	}
	
	/**
	 * Class constructor
	 *
	 * @param vessel
	 */
	public BasicTimeframedAuthorizedVessel(BasicTimeframedAuthorizedVessel vessel) {
		super(vessel);
		
		this._maxTimeDelta = vessel._maxTimeDelta;
		this._authIssuingCountry = vessel._authIssuingCountry;
		this._authFrom = vessel._authFrom;
		this._authTo = vessel._authTo;
		this._authTerm = vessel._authTerm;
	}
	
	/**
	 * Class constructor
	 *
	 * @param vessel
	 */
	public BasicTimeframedAuthorizedVessel(BasicVessel vessel) {
		super(vessel);
	}
	
	/**
	 * Class constructor
	 *
	 * @param vessel
	 */
	public BasicTimeframedAuthorizedVessel(ExtendedVessel vessel) {
		super(vessel);
	}

	/**
	 * @return the 'maxTimeDelta' value
	 */
	public Long getMaxTimeDelta() {
		return this._maxTimeDelta;
	}

	/**
	 * @param maxTimeDelta the 'maxTimeDelta' value to set
	 */
	public void setMaxTimeDelta(Long maxTimeDelta) {
		this._maxTimeDelta = maxTimeDelta;
	}

	/**
	 * @return the 'authTypeId' value
	 */
	public String getAuthTypeId() {
		return this._authTypeId;
	}

	/**
	 * @param authTypeId the 'authTypeId' value to set
	 */
	public void setAuthTypeId(String authTypeId) {
		this._authTypeId = authTypeId;
	}

	/**
	 * @return the 'authIssuer' value
	 */
	public String getAuthIssuer() {
		return this._authIssuer;
	}

	/**
	 * @param authIssuer the 'authIssuer' value to set
	 */
	public void setAuthIssuer(String authIssuer) {
		this._authIssuer = authIssuer;
	}

	/**
	 * @return the 'authIssuingCountry' value
	 */
	public Integer getAuthIssuingCountry() {
		return this._authIssuingCountry;
	}

	/**
	 * @param authIssuingCountry the 'authIssuingCountry' value to set
	 */
	public void setAuthIssuingCountry(Integer authIssuingCountry) {
		this._authIssuingCountry = authIssuingCountry;
	}

	/**
	 * @return the 'authFrom' value
	 */
	public Date getAuthFrom() {
		return this._authFrom;
	}

	/**
	 * @param authFrom the 'authFrom' value to set
	 */
	public void setAuthFrom(Date authFrom) {
		this._authFrom = authFrom;
	}

	/**
	 * @return the 'authTo' value
	 */
	public Date getAuthTo() {
		return this._authTo;
	}

	/**
	 * @param authTo the 'authTo' value to set
	 */
	public void setAuthTo(Date authTo) {
		this._authTo = authTo;
	}

	/**
	 * @return the 'authTerm' value
	 */
	public Date getAuthTerm() {
		return this._authTerm;
	}

	/**
	 * @param authTerm the 'authTerm' value to set
	 */
	public void setAuthTerm(Date authTerm) {
		this._authTerm = authTerm;
	}

	/**
	 * @return the 'authTermCode' value
	 */
	public Integer getAuthTermCode() {
		return this._authTermCode;
	}

	/**
	 * @param authTermCode the 'authTermCode' value to set
	 */
	public void setAuthTermCode(Integer authTermCode) {
		this._authTermCode = authTermCode;
	}

	/**
	 * @return the 'authTermReason' value
	 */
	public String getAuthTermReason() {
		return this._authTermReason;
	}

	/**
	 * @param authTermReason the 'authTermReason' value to set
	 */
	public void setAuthTermReason(String authTermReason) {
		this._authTermReason = authTermReason;
	}

	/**
	 * @return the 'authRef' value
	 */
	public Date getAuthRef() {
		return this._authRef;
	}

	/**
	 * @param authRef the 'authRef' value to set
	 */
	public void setAuthRef(Date authRef) {
		this._authRef = authRef;
	}
}