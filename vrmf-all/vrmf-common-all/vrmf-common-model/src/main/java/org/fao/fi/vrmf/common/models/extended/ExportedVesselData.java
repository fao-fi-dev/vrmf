/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import java.util.Collection;
import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.vrmf.common.core.model.GenericData;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Jun 2011   faoadmin     Creation.
 *
 * @version 1.0
 * @since 24 Jun 2011
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public class ExportedVesselData extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7841651995440316214L;

	@XmlAttribute @Getter @Setter private Integer id;
	@XmlAttribute @Getter @Setter private Integer uid;
	@XmlElement @Getter @Setter private Integer flagId;
	@XmlElement @Getter @Setter private String name;
	@XmlElement @Getter @Setter private Integer ircsCountryId;
	@XmlElement @Getter @Setter private String ircs;
	@XmlElement @Getter @Setter private Integer regNoCountryId;
	@XmlElement @Getter @Setter private String regNo;
	@XmlElement @Getter @Setter private String lengthId;
	@XmlElement @Getter @Setter private Double length;
	@XmlElement @Getter @Setter private String tonnageId;
	@XmlElement @Getter @Setter private Double tonnage;
	@XmlElement @Getter @Setter private String authorizationTypeId;
	@XmlElement @Getter @Setter private Date authorizedFrom;
	@XmlElement @Getter @Setter private Date authorizedTo;
	@XmlElement @Getter @Setter private Collection<VesselsToIdentifiers> identifiers;
	@XmlAttribute @Getter @Setter private String sourceSystem;
	@XmlAttribute @Getter @Setter private Date lastUpdate;
}
