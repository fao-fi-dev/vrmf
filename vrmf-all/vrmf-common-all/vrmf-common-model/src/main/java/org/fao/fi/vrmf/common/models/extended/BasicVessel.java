/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVessel;
import org.fao.fi.vrmf.common.models.generated.Vessels;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2010
 */
@XmlType(name="vessel")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public class BasicVessel extends Vessels {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7801806468193779051L;

	@XmlElement
	@Getter @Setter protected String imo;
	
	@XmlElement
	@Getter @Setter protected String euCfr;
	
	@XmlTransient
	@XmlElement(name="flag")
	@Getter @Setter protected Integer currentFlagId;
	
	@XmlElement(name="name")
	@Getter @Setter protected String currentName;
	
	@XmlElement(name="simplifiedName")
	@Getter @Setter protected String currentSimplifiedName;

	@XmlElement(name="callsignCountryId")
	@Getter @Setter protected Integer currentCallsignCountry;
	
	@XmlElement(name="callsign")
	@Getter @Setter protected String currentCallsign;
	
	@XmlElement(name="registrationNumber")
	@Getter @Setter protected String currentRegNo;
	
	@XmlElement(name="registrationPort")
	@Getter @Setter protected String currentRegPort;
	
	@XmlElement(name="registrationCountry")
	@Getter @Setter protected Integer currentRegCountry;
	
	@XmlElement(name="lengthType")
	@Getter @Setter protected String currentLengthType;
	
	@XmlElement(name="length")
	@Getter @Setter protected Float currentLengthValue;
	
	@XmlElement(name="tonnageType")
	@Getter @Setter protected String currentTonnageType;
	
	@XmlElement(name="tonnage")
	@Getter @Setter protected Float currentTonnageValue;
	
	@XmlElement
	@Getter @Setter protected String sourceSystem;
	
	@XmlElement
	@Getter @Setter protected Integer numberOfSystems;
	
	@XmlElementWrapper(name="sourceSystems")
	@XmlElement(name="sourceSystem")
	@Getter @Setter protected String[] sourceSystems;
	
	@XmlElement
	@Getter @Setter protected Date updateDate;
		
	/**
	 * Class constructor
	 *
	 * @param id
	 * @param uid
	 * @param IMO
	 * @param currentFlagId
	 * @param currentName
	 * @param currentCallsign
	 * @param currentCallsignCountry
	 * @param currentRegNo
	 * @param currentRegPort
	 * @param currentRegCountry
	 * @param sourceSystem
	 * @param updateDate
	 */
	public BasicVessel(Integer id, Integer uid, String IMO, String EUCFR, Integer currentFlagId, String currentName, String currentSimplifiedName, String currentCallsign, Integer currentCallsignCountry, String currentRegNo, String currentRegPort, Integer currentRegCountry, String sourceSystem, Date updateDate) {
		super();
		this.setId(id);
		this.setUid(uid);		
		this.imo = IMO;
		this.euCfr = EUCFR;
		this.currentFlagId = currentFlagId;
		this.currentName = currentName;
		this.currentSimplifiedName = currentSimplifiedName;
		this.currentCallsign = currentCallsign;
		this.currentCallsignCountry = currentCallsignCountry;
		this.currentRegNo = currentRegNo;
		this.currentRegPort = currentRegPort;
		this.currentRegCountry = currentRegCountry;
		this.sourceSystem = sourceSystem;
		this.updateDate = updateDate;
	}
	
	/**
	 * Class constructor
	 *
	 * @param id
	 * @param uid
	 * @param IMO
	 * @param currentFlagId
	 * @param currentName
	 * @param currentCallsign
	 * @param currentCallsignCountry
	 * @param currentRegNo
	 * @param currentRegPort
	 * @param currentRegCountry
	 * @param sourceSystem
	 * @param numberOfSystems
	 * @param sourceSystems
	 * @param updateDate
	 */
	public BasicVessel(Integer id, Integer uid, String IMO, String EUCFR, Integer currentFlagId, String currentName, String currentSimplifiedName, String currentCallsign, Integer currentCallsignCountry, String currentRegNo, String currentRegPort, Integer currentRegCountry, String sourceSystem, Integer numberOfSystems, String[] sourceSystems, Date updateDate) {
		this(id, uid, IMO, EUCFR, currentFlagId, currentName, currentSimplifiedName, currentCallsign, currentCallsignCountry, currentRegNo, currentRegPort, currentRegCountry, sourceSystem, updateDate);
		
		this.numberOfSystems = numberOfSystems;
		this.sourceSystems = sourceSystems;
	}
	
	/**
	 * Class constructor
	 *
	 * @param id
	 * @param uid
	 * @param IMO
	 * @param currentFlagId
	 * @param currentName
	 * @param currentCallsign
	 * @param currentCallsignCountry
	 * @param currentRegNo
	 * @param currentRegPort
	 * @param currentRegCountry
	 * @param currentLengthType
	 * @param currentLength
	 * @param currentTonnageType
	 * @param currentTonnage
	 * @param sourceSystem
	 * @param updateDate
	 * @param numberOfSystems
	 * @param sourceSystems
	 */
	public BasicVessel(Integer id, Integer uid, String IMO, String EUCFR, Integer currentFlagId, String currentName, String currentSimplifiedName, String currentCallsign, Integer currentCallsignCountry, String currentRegNo, String currentRegPort, Integer currentRegCountry, String currentLengthType, Float currentLength, String currentTonnageType, Float currentTonnage, String sourceSystem, Date updateDate, Integer numberOfSystems, String[] sourceSystems) {
		this(id, uid, IMO, EUCFR, currentFlagId, currentName, currentSimplifiedName, currentCallsign, currentCallsignCountry, currentRegNo, currentRegPort, currentRegCountry, sourceSystem, numberOfSystems, sourceSystems, updateDate);
		
		this.currentLengthType = currentLengthType;
		this.currentLengthValue = currentLength;
		
		this.currentTonnageType = currentTonnageType;
		this.currentTonnageValue = currentTonnage;
	}
	
	/**
	 * Class constructor
	 *
	 * @param vessel
	 */
	public BasicVessel(BasicVessel vessel) {
		this.setId(vessel.getId());
		this.setUid(vessel.getUid());

		this.imo = vessel.imo;
		
		this.currentFlagId = vessel.currentFlagId;
		this.currentName = vessel.currentName;
		this.currentSimplifiedName = vessel.currentSimplifiedName;

		this.currentLengthType = vessel.currentLengthType;
		this.currentLengthValue = vessel.currentLengthValue;
		this.currentTonnageType = vessel.currentTonnageType;
		this.currentTonnageValue = vessel.currentTonnageValue;
		
		this.sourceSystems = vessel.sourceSystems;
		this.updateDate = vessel.getUpdateDate();

		this.sourceSystem = vessel.sourceSystem;
		this.numberOfSystems = vessel.numberOfSystems;
	}
	
	/**
	 * Class constructor
	 *
	 * @param vessel
	 */
	public BasicVessel(ExtendedVessel vessel) {
		this.setId(vessel.getId());
		this.setUid(vessel.getUid());
		this.setImo(vessel.findIMO());
		
		this.currentFlagId = vessel.getCurrentFlagId();
		this.currentName = vessel.getCurrentName();
		this.currentSimplifiedName = vessel.getCurrentSimplifiedName();
		this.sourceSystem = vessel.getSourceSystem();
		this.numberOfSystems = vessel.getNumberOfSystems();
		this.sourceSystems = vessel.getSourceSystems();
		this.updateDate = vessel.getUpdateDate();
		
		VesselsToLengths currentLength = vessel.getCurrentLength();
		VesselsToTonnage currentTonnage = vessel.getCurrentTonnage();
		
		if(currentLength != null) {
			this.currentLengthType = currentLength.getTypeId();
			this.currentLengthValue = currentLength.getValue();
		}
		
		if(currentTonnage != null) {
			this.currentTonnageType = currentTonnage.getTypeId();
			this.currentTonnageValue = currentTonnage.getValue();
		}
		
		this.imo = vessel.findIMO();
	}
}