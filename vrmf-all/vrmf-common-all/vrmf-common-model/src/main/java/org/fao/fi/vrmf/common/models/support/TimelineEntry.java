/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class TimelineEntry implements Serializable, Comparable<TimelineEntry> {
	private static final long serialVersionUID = -6374249769871761924L;

	private Date _date;
	private Collection<String> _entries;
	
	public TimelineEntry(Date date) {
		this._date = date;
		
		this._entries = new ArrayList<String>();
	}
	
	public TimelineEntry(Date date, String entry) {
		this(date);
		this._entries.add(entry);
	}
	
	public void addEntry(String entry) {
		this._entries.add(entry);
	}

	public int compareTo(TimelineEntry o) {
		return -inverseCompareTo(o);
	}
	
	public int inverseCompareTo(TimelineEntry o) {
		if(o == null)
			return this._date.compareTo(new Date());
		
		if(this._date == null && o._date == null)
			return 0;
		
		if(this._date == null)
			return new Date().compareTo(o._date);

		return this._date.compareTo(o._date);
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return this._date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this._date = date;
	}

	/**
	 * @return the entries
	 */
	public Collection<String> getEntries() {
		return this._entries;
	}

	/**
	 * @param entries the entries to set
	 */
	public void setEntries(Collection<String> entries) {
		this._entries = entries;
	}	
}