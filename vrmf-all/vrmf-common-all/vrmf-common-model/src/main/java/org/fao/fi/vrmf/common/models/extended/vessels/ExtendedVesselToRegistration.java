/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended.vessels;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.sh.utility.common.helpers.singletons.lang.BeansHelper;
import org.fao.fi.vrmf.common.models.extended.ExtendedPort;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Dec 2010
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public class ExtendedVesselToRegistration extends VesselsToRegistrations {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1635927710226481655L;
	
	@XmlElement(name="registrationCountry")
	@Getter @Setter private SCountries registrationCountry;
	
	@XmlElement(name="registrationPort")
	@Getter @Setter private ExtendedPort registrationPort;

	/**
	 * Class constructor
	 *
	 * @param source
	 * @param shipbuilder
	 */
	public ExtendedVesselToRegistration(VesselsToRegistrations source, SCountries registrationCountry, ExtendedPort registrationPort) {
		super();
		
		BeansHelper.transferBean(source, this);

		this.registrationCountry = registrationCountry;
		this.registrationPort = registrationPort;
	}
}