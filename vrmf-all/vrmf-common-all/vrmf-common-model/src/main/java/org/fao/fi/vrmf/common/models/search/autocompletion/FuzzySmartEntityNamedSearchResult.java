/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2010
 */
@EqualsAndHashCode(callSuper=true) @NoArgsConstructor
public class FuzzySmartEntityNamedSearchResult extends FuzzySmartNamedSearchResult {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 552458677989023764L;

	@Getter @Setter private Integer id;
	@Getter @Setter private Integer uid;
	@Getter @Setter private String entityTypeId;
	@Getter @Setter private String salutation;
	@Getter @Setter private String role;
	@Getter @Setter private String branch;
	@Getter @Setter private String address;
	@Getter @Setter private String city;
	@Getter @Setter private Integer countryId;
	@Getter @Setter private String zipCode;
	@Getter @Setter private Integer nationalityId;
	@Getter @Setter private String phoneNumber;
	@Getter @Setter private String mobileNumber;
	@Getter @Setter private String faxNumber;
	@Getter @Setter private String eMail;
	@Getter @Setter private String website;
}