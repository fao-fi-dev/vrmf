/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.security;

import java.io.Serializable;
import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Mar 2011
 */
@XmlType(name="UserRole")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserRole implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 9209346436594565537L;
	
	@XmlElement(name="Role")
	private String _role;
	
	@XmlElementWrapper(name="RoleCapabilities")
	@XmlElement(name="RoleCapability")
	private UserCapability[] _capabilities;

	public UserRole() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param role
	 */
	public UserRole(String role) {
		super();
		this._role = role;
	}

	/**
	 * Class constructor
	 *
	 * @param role
	 * @param capabilities
	 */
	public UserRole(String role, UserCapability[] capabilities) {
		super();
		this._role = role;
		this._capabilities = SerializationHelper.clone(capabilities);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.models.security.Role#getRole()
	 */
	public String getRole() { return this._role; }

	/**
	 * @param role the 'role' value to set
	 */
	public void setRole(String role) { this._role = role; }

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.models.security.Role#getCapabilities()
	 */
	public UserCapability[] getCapabilities() { return SerializationHelper.clone(this._capabilities); }

	/**
	 * @param capabilities the 'capabilities' value to set
	 */
	public void setCapabilities(UserCapability[] capabilities) { this._capabilities = SerializationHelper.clone(capabilities); }
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(this._capabilities);
		result = prime * result + ((this._role == null) ? 0 : this._role.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UserRole))
			return false;
		UserRole other = (UserRole) obj;
		if (!Arrays.equals(this._capabilities, other._capabilities))
			return false;
		if (this._role == null) {
			if (other._role != null)
				return false;
		} else if (!this._role.equals(other._role))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	final public String toString() {
		return this._role + ( this._capabilities == null ? "" : " " + Arrays.asList(this._capabilities));
	}
}