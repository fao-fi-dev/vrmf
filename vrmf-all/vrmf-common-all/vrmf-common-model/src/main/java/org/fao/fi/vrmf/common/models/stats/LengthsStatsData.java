/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.stats;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Aug 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Aug 2012
 */
public class LengthsStatsData extends BasicVesselStatsReport {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8425189258197709543L;
	
	private Double _rangeFrom;
	private Double _rangeTo;
	
	/**
	 * @return the 'rangeFrom' value
	 */
	public Double getRangeFrom() {
		return this._rangeFrom;
	}
	
	/**
	 * @param rangeFrom the 'rangeFrom' value to set
	 */
	public void setRangeFrom(Double rangeFrom) {
		this._rangeFrom = rangeFrom;
	}
	
	/**
	 * @return the 'rangeTo' value
	 */
	public Double getRangeTo() {
		return this._rangeTo;
	}
	
	/**
	 * @param rangeTo the 'rangeTo' value to set
	 */
	public void setRangeTo(Double rangeTo) {
		this._rangeTo = rangeTo;
	}
}
