/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.stats;

import java.io.Serializable;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2011
 */
public final class CountriesToFlag extends GenericData implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 435853956713059121L;
	
	private Integer _countryId;
	private Integer _vessels;
	
	/**
	 * Class constructor
	 */
	public CountriesToFlag() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param countryId
	 * @param vessels
	 */
	public CountriesToFlag(Integer countryId, Integer vessels) {
		super();
		this._countryId = countryId;
		this._vessels = vessels;
	}

	/**
	 * @return the 'countryId' value
	 */
	public Integer getCountryId() {
		return this._countryId;
	}

	/**
	 * @param countryId the 'countryId' value to set
	 */
	public void setCountryId(Integer countryId) {
		this._countryId = countryId;
	}

	/**
	 * @return the 'vessels' value
	 */
	public Integer getVessels() {
		return this._vessels;
	}

	/**
	 * @param vessels the 'vessels' value to set
	 */
	public void setVessels(Integer vessels) {
		this._vessels = vessels;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._countryId == null) ? 0 : this._countryId.hashCode());
		result = prime * result + ((this._vessels == null) ? 0 : this._vessels.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CountriesToFlag))
			return false;
		CountriesToFlag other = (CountriesToFlag) obj;
		if (this._countryId == null) {
			if (other._countryId != null)
				return false;
		} else if (!this._countryId.equals(other._countryId))
			return false;
		if (this._vessels == null) {
			if (other._vessels != null)
				return false;
		} else if (!this._vessels.equals(other._vessels))
			return false;
		return true;
	}
}
