/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.utility.core.helpers.singletons.io.MD5Helper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.extensions.maps.impl.SortedKeysHashMap;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToFlags;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToGear;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToOperator;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToOwner;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToRegistration;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToShipbuilder;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToType;
import org.fao.fi.vrmf.common.models.generated.Authorizations;
import org.fao.fi.vrmf.common.models.generated.SGearTypes;
import org.fao.fi.vrmf.common.models.generated.SVesselTypes;
import org.fao.fi.vrmf.common.models.generated.Vessels;
import org.fao.fi.vrmf.common.models.generated.VesselsToBuildingYear;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToCrew;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;
import org.fao.fi.vrmf.common.models.generated.VesselsToPower;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Dec 2010
 */
abstract public class GenericCurrentVessel<VESSEL extends Vessels> extends FullVessel implements Exportable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2240056356770070341L;
	
	private FullVessel _source;
	
	public GenericCurrentVessel(FullVessel source) {
		super(source);
		
		this._source = source;
	}
	
	final protected <DATA extends Serializable> DATA getCurrentData(List<DATA> history) {
		if(history == null || history.isEmpty())
			return null;
		
		return history.get(0);
	}
	
	final protected <DATA extends Serializable> DATA getPreviousData(List<DATA> history) {
		if(history == null || history.isEmpty() || history.size() == 1)
			return null;
		
		return history.get(1);
	}
	
	final public String getIMO() {
		if(this.getIdentifiers() != null) {
			List<VesselsToIdentifiers> identifiers = this.getIdentifiers();
			
			Collections.sort(identifiers, new Comparator<VesselsToIdentifiers>() {
				/* (non-Javadoc)
				 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
				 */
				@Override
				public int compare(VesselsToIdentifiers o1, VesselsToIdentifiers o2) {
					return o1.getUpdateDate() == null ? -1 : o2.getUpdateDate() == null ? 1 : o1.getUpdateDate().getTime() >= o2.getUpdateDate().getTime() ? -1 : 1;
				}
			});
			
			for(VesselsToIdentifiers current : identifiers) {
				if("IMO".equals(current.getTypeId())) {
					return current.getIdentifier();
				}
			}
		}
		
		return null;
	}
	
	final public String getEU_CFR() {
		if(this.getIdentifiers() != null) {
			List<VesselsToIdentifiers> identifiers = this.getIdentifiers();
			
			Collections.sort(identifiers, new Comparator<VesselsToIdentifiers>() {
				/* (non-Javadoc)
				 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
				 */
				@Override
				public int compare(VesselsToIdentifiers o1, VesselsToIdentifiers o2) {
					return o1.getUpdateDate() == null ? -1 : o2.getUpdateDate() == null ? 1 : o1.getUpdateDate().getTime() >= o2.getUpdateDate().getTime() ? -1 : 1;
				}
			});
			
			for(VesselsToIdentifiers current : identifiers) {
				if("EU_CFR".equals(current.getTypeId())) {
					return current.getIdentifier();
				}
			}
		}
		
		return null;
	}
	
	final public Date getReportDate() {
		Authorizations lastAuthorization = this.getLastAuthorization();
				
		return lastAuthorization == null ? null : lastAuthorization.getReferenceDate();
	}
		
	final public VesselsToFlags getFlag() {
		return this.getCurrentData(this.getFlagData());
	}
	
	final public ExtendedVesselToFlags getExtendedFlag() {
		return this.getCurrentData(this.getFlags());
	}
	
	final public VesselsToFlags getPreviousFlag() {
		VesselsToFlags currentFlagEntry = this.getFlag();
		VesselsToFlags previousFlagEntry = this.getPreviousData(this.getFlagData());
		
		if(previousFlagEntry == null || currentFlagEntry == null || currentFlagEntry.getCountryId() == null)
			return null;
		
		Integer currentFlag = currentFlagEntry.getCountryId();
		
		Integer flag = null;
		for(VesselsToFlags current : this.getFlagData()) {
			flag = current.getCountryId();
			
			//No need to check for flag non-nullity as it is mandatory set!
			if(!flag.equals(currentFlag))
				return current;
		}
		
		return null;
	}
	
	final public ExtendedVesselToFlags getExtendedPreviousFlag() {
		ExtendedVesselToFlags currentFlagEntry = this.getExtendedFlag();
		ExtendedVesselToFlags previousFlagEntry = this.getPreviousData(this.getFlags());
		
		if(previousFlagEntry == null || currentFlagEntry == null || currentFlagEntry.getCountryId() == null)
			return null;
		
		Integer currentFlag = currentFlagEntry.getCountryId();
		
		Integer flag = null;
		for(ExtendedVesselToFlags current : this.getFlags()) {
			flag = current.getCountryId();
			
			//No need to check for flag non-nullity as it is mandatory set!
			if(!flag.equals(currentFlag))
				return current;
		}
		
		return null;
	}
	
	final public VesselsToName getName() {
		return this.getCurrentData(this.getNameData());
	}
	
	final public VesselsToName getPreviousName() {
		VesselsToName currentNameEntry = this.getName();
		VesselsToName previousNameEntry = this.getPreviousData(this.getNameData());
		
		if(previousNameEntry == null || currentNameEntry == null || currentNameEntry.getName() == null)
			return null;
		
		String currentName = currentNameEntry.getName();
		
		String name = null;
		for(VesselsToName current : this.getNameData()) {
			name = current.getName();
			
			if(currentName != null) {
				if(!name.equals(currentName))
					return current;
			} else if(name != null)
				return current;
		}
		
		return null;
	}
	
	final public ExtendedVesselToRegistration getRegistration() {
		return this.getCurrentData(super.getRegistrations());
	}
		
	final public VesselsToCallsigns getIRCS() {
		return this.getCurrentData(this.getCallsignData());
	}
	
	final public VesselsToBuildingYear getBuildingYear() {
		return this.getCurrentData(this.getBuildingYearData());
	}
	
	final public ExtendedEntity getShipbuilder() {
		ExtendedVesselToShipbuilder shipbuilder = this.getCurrentData(this.getShipbuilders());
		
		if(shipbuilder != null)
			return shipbuilder.getShipbuilder();
		
		return null;
	}
	
	final public SVesselTypes getVesselType() {
		ExtendedVesselToType type = this.getCurrentData(this.getTypes());
		
		if(type != null)
			return type.getVesselType();
		
		return null;
	}
	
	final public SGearTypes getPrimaryGearType() {
		if(this.getGears() != null) {
			for(ExtendedVesselToGear current : this.getGears()) {
				if(Boolean.TRUE.equals(current.getPrimaryGear()))
					return current.getGearType();
			}
		}
		
		return null;
	}
	
	final public SGearTypes getSecondaryGearType() {
		if(this.getGears() != null) {
			for(ExtendedVesselToGear current : this.getGears()) {
				if(Boolean.FALSE.equals(current.getPrimaryGear()))
					return current.getGearType();
			}
		}
		
		return null;
	}
	
	final public Map<String, Float> getLengthsMap() {
		Map<String, Float> lengthMap = new SortedKeysHashMap<String, Float>();
		
		if(this.getLengthData() != null) {
			for(VesselsToLengths current : this.getLengthData()) {
				if(!lengthMap.containsKey(current.getTypeId())) {
					lengthMap.put(current.getTypeId(), current.getValue());
				}
			}
		}
		
		return lengthMap;
	}
	
	final public Float getLengthOverall() {
		return this.getLengthsMap().get("LOA");
	}
	
	final public Float getLengthBetweenPerpendiculars() {
		return this.getLengthsMap().get("BP");
	}
	
	final public Float getRegisteredLength() {
		return this.getLengthsMap().get("REG");
	}
	
	final public Float getMouldedDepth() {
		return this.getLengthsMap().get("MOD");
	}
	
	final public Float getDraught() {
		return this.getLengthsMap().get("DRA");
	}
	
	final public Float getBeam() {
		return this.getLengthsMap().get("BEA");
	}
	 
	final public Map<String, Float> getTonnagesMap() {
		Map<String, Float> tonnageMap = new SortedKeysHashMap<String, Float>();
		
		if(this.getTonnageData() != null) {
			for(VesselsToTonnage current : this.getTonnageData()) {
				if(!tonnageMap.containsKey(current.getTypeId())) {
					tonnageMap.put(current.getTypeId(), current.getValue());
				}
			}
		}
		
		return tonnageMap;
	}
	
	final public Float getGrossTonnage() {
		return this.getTonnagesMap().get("GT");
	}
	
	final public Float getGrossRegisteredTonnage() {
		return this.getTonnagesMap().get("GRT");
	}
			
	final public VesselsToPower getMainEnginePower() {
		if(this.getPowerData() != null) {
			for(VesselsToPower current : this.getPowerData()) {
				if(Boolean.TRUE.equals(current.getMainEngine()))
					return current;
			}
		}
		
		return null;
	}
	
	final public VesselsToPower getAuxiliaryEnginesPower() {
		if(this.getPowerData() != null) {
			for(VesselsToPower current : this.getPowerData()) {
				if(Boolean.FALSE.equals(current.getMainEngine()))
					return current;
			}
		}
		
		return null;
	}
	
	final public VesselsToCrew getCrew() {
		return this.getCurrentData(this.getCrewData());
	}
	
	final public ExtendedEntity getOwner() {
		ExtendedVesselToOwner owner = this.getCurrentData(this.getOwners());
		
		if(owner != null)
			return owner.getOwner();
		
		return null;
	}
	
	final public ExtendedEntity getOperator() {
		ExtendedVesselToOperator operator = this.getCurrentData(this.getOperators());
		
		if(operator != null)
			return operator.getOperator();
		
		return null;
	}
	
	final public Map<String, List<ExtendedAuthorizations>> getAuthorizationsMap() {
		Map<String, List<ExtendedAuthorizations>> authorizationsMap = new HashMap<String, List<ExtendedAuthorizations>>();
		
		String key;
		List<ExtendedAuthorizations> currentAuths;
		if(this.getAuthorizations() != null) {
			for(ExtendedAuthorizations current : this.getAuthorizations()) {
				key = current.getSourceSystem() + "_" + current.getTypeId();
				
				currentAuths = authorizationsMap.get(key);
				
				if(currentAuths == null) {
					currentAuths = new ArrayList<ExtendedAuthorizations>();
					
					authorizationsMap.put(key, currentAuths);
				}

				currentAuths.add(current);
			}
		}
		
		return authorizationsMap;
	}
	
	final public ExtendedAuthorizations getLastAuthorization() {
		return this.getLastAuthorization(null, null);
	}
	
	final public ExtendedAuthorizations getLastAuthorization(String authorizationIssuerID, String authorizationTypeID) {
		List<ExtendedAuthorizations> currentAuths = this.getAuthorizations();
		
		if(currentAuths == null ||
		   currentAuths.isEmpty()) 
			return null;
		
		if(authorizationIssuerID == null && authorizationTypeID == null)
			return currentAuths.get(0);
		
		if(authorizationIssuerID != null && authorizationTypeID != null) {
			List<ExtendedAuthorizations> auths = this.getAuthorizationsMap().get(authorizationIssuerID + "_" + authorizationTypeID);
			
			if(auths != null && !auths.isEmpty())
				return auths.get(0);
			
			return null;
		}
		
		for(ExtendedAuthorizations current : currentAuths) {
			if(authorizationIssuerID != null) {
				if(authorizationIssuerID.equals(current.getSourceSystem()))
					return current;
			} else if(authorizationTypeID != null) {
				if(authorizationTypeID.equals(current.getTypeId()))
					return current;
			}
		}
		
		return null;
	}
	
	protected String getDataForChecksum(boolean includeUID) {
		StringBuilder data = new StringBuilder();
		
		if(includeUID)
			data.append(this.getUid());
		
		data.append(this.getBeam());
		data.append(this.getBuildingYear());
		data.append(this.getCrew());
		data.append(this.getDraught());
		data.append(this.getFlag().getCountryId());
		data.append(this.getGrossRegisteredTonnage());
		data.append(this.getGrossTonnage());
		data.append(this.getLastAuthorization());
		data.append(StringsHelper.trim(this.getIMO()));
		data.append(this.getIRCS());
		data.append(this.getLengthBetweenPerpendiculars());
		data.append(this.getLengthOverall());
		data.append(this.getMainEnginePower());
		data.append(this.getAuxiliaryEnginesPower());
		data.append(this.getMouldedDepth());
		data.append(this.getName());
		data.append(this.getOperator());
		data.append(this.getOwner());
		data.append(this.getPreviousFlag());
		data.append(this.getPreviousName());
		data.append(this.getPrimaryGearType());
		data.append(this.getRegisteredLength());
		data.append(this.getRegistration());
		data.append(this.getReportDate());
		data.append(this.getSecondaryGearType());
		data.append(this.getShipbuilder());
		data.append(this.getVesselType());
		
		return data.toString();
	}
	
	final public String getChecksum(boolean includeUID) throws Throwable{
		return MD5Helper.digest(this.getDataForChecksum(includeUID));
	}
	
	final public String getChecksum() throws Throwable{
		return MD5Helper.digest(this.getDataForChecksum(false));
	}
	
//	public DATA_EXCHANGE_MODEL toExchangeableForm(DATA_EXCHANGE_MODEL model, String authorizationIssuerId, String authorizationTypeId) {
//		String flag, previousFlag;
//		Date previousFlagReferenceDate = null;
//		
//		String name, previousName;
//		Date previousNameReferenceDate = null;
//		
//		flag = this.getFlags() == null || this.getFlags().isEmpty() ? null : this.getFlags().get(0).getVesselFlag().getIso2Code();
//		previousFlag = this.getFlags() == null || this.getFlags().size() < 2 ? null : this.getFlags().get(1).getVesselFlag().getIso2Code();
//
//		if(previousFlag != null)
//			previousFlagReferenceDate = this.getFlags().get(1).getReferenceDate();		
//
//		name = this.getNameData() == null || this.getNameData().isEmpty() ? null : this.getNameData().get(0).getName();
//		previousName = this.getNameData() == null || this.getNameData().size() < 2 ? null : this.getNameData().get(1).getName();
//
//		if(previousName != null)
//			previousNameReferenceDate = this.getNameData().get(1).getReferenceDate();
//		
//		model.setId(this.getId());
//		model.setUid(this.getUid());
//		model.setIMO(this.getIMO());
//		model.setFlag(flag);
//		model.setPreviousFlag(previousFlag);
//		model.setPreviousFlagReferenceDate(previousFlagReferenceDate);
//		model.setName(name);
//		model.setPreviousName(previousName);
//		model.setPreviousNameReferenceDate(previousNameReferenceDate);
//		
//		if(this.getRegistration() != null) {
//			model.setRegistrationNumber(this.getRegistration().getRegistrationNumber());
//			model.setRegistrationCountry(this.getRegistration().getRegistrationCountry() == null ? null : this.getRegistration().getRegistrationCountry().getIso2Code());
//			model.setRegistrationPortCode(this.getRegistration().getPortId());
//			model.setRegistrationPortName(model.getRegistrationPortCode() == null ? null : this.getRegistration().getRegistrationPort().getName());
//		}
//		
//		model.setCallsign(this.getCallsignData() == null || this.getCallsignData().isEmpty() ? null : this.getCallsignData().get(0).getCallsignId());
//		model.setBuildingYear(this.getBuildingYearData() == null || this.getBuildingYearData().isEmpty() ? null : this.getBuildingYearData().get(0).getValue());
//		
//		if(this.getShipbuilder() != null) {
//			model.setShipyard(this.getShipbuilder().getName());
//			model.setBuildingCountry(this.getShipbuilder().getCountry() == null ? null : this.getShipbuilder().getCountry().getIso2Code());
//			model.setBuildingPort(this.getShipbuilder().getCity());
//		}
//		
//		model.setVesselType(this.getVesselType() == null ? null : this.getVesselType().getIsscfvCode());
//		model.setPrimaryGearType(this.getPrimaryGearType() == null ? null : this.getPrimaryGearType().getIsscfgCode());
//		model.setSecondaryGearType(this.getSecondaryGearType() == null ? null : this.getSecondaryGearType().getIsscfgCode());
//		model.setLengths(this.getLengthsMap());
//		model.setTonnages(this.getTonnagesMap());
//		
//		if(this.getMainEnginePower() != null) {
//			model.setMainEngineUnit(this.getMainEnginePower().getTypeId());
//			model.setMainEnginePower(this.getMainEnginePower().getValue());
//		}
//		
//		if(this.getAuxiliaryEnginesPower() != null) {
//			model.setAuxiliaryEnginesUnit(this.getAuxiliaryEnginesPower().getTypeId());
//			model.setAuxiliaryEnginesPower(this.getAuxiliaryEnginesPower().getValue());
//		}
//		
//		model.setCrew(this.getCrew() == null ? null : this.getCrew().getValue());
//		
//		if(this.getOwner() != null) {
//			final ExtendedEntity owner = this.getOwner();
//			model.setOwnerName(owner.getName());
//			model.setOwnerCountry(owner.getCountry() == null ? null : owner.getCountry().getIso2Code());
//			model.setOwnerCity(owner.getCity());
//			model.setOwnerAddress(owner.getAddress());
//			model.setOwnerZipCode(owner.getZipCode());
//			model.setOwnerPhoneNumber(owner.getPhoneNumber());
//			model.setOwnerMobileNumber(owner.getMobileNumber());
//			model.setOwnerFaxNumber(owner.getFaxNumber());
//			model.setOwnerEMail(owner.geteMail());
//			model.setOwnerWebsite(owner.getWebsite());
//		}
//		
//		if(this.getOperator() != null) {
//			final ExtendedEntity operator = this.getOperator();
//			model.setOperatorName(operator.getName());
//			model.setOperatorCountry(operator.getCountry() == null ? null : operator.getCountry().getIso2Code());
//			model.setOperatorCity(operator.getCity());
//			model.setOperatorAddress(operator.getAddress());
//			model.setOperatorZipCode(operator.getZipCode());
//			model.setOperatorPhoneNumber(operator.getPhoneNumber());
//			model.setOperatorMobileNumber(operator.getMobileNumber());
//			model.setOperatorFaxNumber(operator.getFaxNumber());
//			model.setOperatorEMail(operator.geteMail());
//			model.setOperatorWebsite(operator.getWebsite());
//		}
//
//		ExtendedAuthorizations authorization = this.getLastAuthorization(authorizationIssuerId, authorizationTypeId);
//		
//		if(authorization != null) {			
//			model.setReportingFlag(authorization.getIssuingCountry() == null ? null : authorization.getIssuingCountry().getIso2Code());
//			model.setAuthorizationIssuerId(authorization.getSourceSystem());
//			model.setAuthorizationTypeId(authorization.getTypeId());
//			model.setAuthorizationId(authorization.getAuthorizationId());
//			model.setAuthorizationVesselId(authorization.getVesselId());
//			model.setAuthorizationStart(authorization.getValidFrom());
//			model.setAuthorizationEnd(authorization.getValidTo());
//			model.setAuthorizationTermination(authorization.getTerminationReferenceDate());
//			model.setAuthorizationTerminationCode(authorization.getTerminationReasonCode());
//			model.setAuthorizationTerminationReason(authorization.getTerminationReason());
//			model.setAuthorizationComment(authorization.getComment());
//			model.setReportingDate(authorization.getReferenceDate());
//		} 
//		
//		model.setChecksum(model.calculateChecksum());
//		
//		return model;
//	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._source == null) ? 0 : this._source.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof GenericCurrentVessel))
			return false;
		@SuppressWarnings("unchecked")
		GenericCurrentVessel<VESSEL> other = (GenericCurrentVessel<VESSEL>) obj;		
		if (this._source == null) {
			if (other._source != null)
				return false;
		} else if (!this._source.equals(other._source))
			return false;
		return true;
	}
}
