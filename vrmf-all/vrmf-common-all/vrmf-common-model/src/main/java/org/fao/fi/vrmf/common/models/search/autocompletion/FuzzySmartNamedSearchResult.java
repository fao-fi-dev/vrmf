/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2010
 */
public class FuzzySmartNamedSearchResult extends SmartNamedDataSearchResult implements FuzzySearchResult {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 552458677989023764L;
	
	//Fields required by the 'FuzzySearchResult' interface
	private Double _score;

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.models.search.autocompletion.FuzzySearchResult#getScore()
	 */
	public Double getScore() {
		return this._score;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.models.search.autocompletion.FuzzySearchResult#setScore(java.lang.Double)
	 */
	public void setScore(Double score) {
		this._score = score;
	}
}