/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion.typed;

import org.fao.fi.vrmf.common.models.search.autocompletion.SimpleGroupedSearchResult;



/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Jan 2011
 */
public final class RegistrationNumberSearchResult extends SimpleGroupedSearchResult {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5487439384483848917L;

	/**
	 * Class constructor
	 *
	 */
	public RegistrationNumberSearchResult() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param registrationNumber
	 * @param totalOccurrencies
	 * @param occurrencies
	 * @param groupedOccurrencies
	 */
	public RegistrationNumberSearchResult(String registrationNumber, int totalOccurrencies, int occurrencies, int groupedOccurrencies) {
		super(registrationNumber, totalOccurrencies, occurrencies, groupedOccurrencies);
	}
	
	/**
	 * @return the 'registrationNumber' value
	 */
	public String getRegistrationNumber() {
		return super.getTerm();
	}
	
	/**
	 * @param registrationNumber the 'registrationNumber' value to set
	 */
	public void setRegistrationNumber(String registrationNumber) {
		super.setTerm(registrationNumber);
	}
}