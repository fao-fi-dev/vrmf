/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import java.util.Date;

import org.fao.fi.vrmf.common.core.model.dto.vessels.attributes.AbstractVesselAttribute;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Nov 2010
 */
@Deprecated
public class VesselUniqueIdentifier extends AbstractVesselAttribute {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5187265324157194212L;
	
	private Integer _vesselId;
	private Integer _vesselUid;
	private String _identifier;
	private String _identifierTypeId;
	private String _sourceSystem;
	private String _updaterId;
	private Date _updateDate;
	private String _comment;
	
	/**
	 * @return the 'vesselId' value
	 */
	public Integer getVesselId() {
		return this._vesselId;
	}

	/**
	 * @param vesselId the 'vesselId' value to set
	 */
	public void setVesselId(Integer vesselId) {
		this._vesselId = vesselId;
	}
	
	/**
	 * @return the 'vesselUid' value
	 */
	public Integer getVesselUid() {
		return this._vesselUid;
	}

	/**
	 * @param vesselUid the 'vesselUid' value to set
	 */
	public void setVesselUid(Integer vesselUid) {
		this._vesselUid = vesselUid;
	}

	/**
	 * @return the 'identifier' value
	 */
	final public String getIdentifier() {
		return this._identifier;
	}
	
	/**
	 * @param identifier the 'identifier' value to set
	 */
	final public void setIdentifier(String identifier) {
		this._identifier = identifier;
	}
	
	/**
	 * @return the 'identifierTypeID' value
	 */
	public String getIdentifierTypeId() {
		return this._identifierTypeId;
	}

	/**
	 * @param identifierTypeID the 'identifierTypeID' value to set
	 */
	public void setIdentifierTypeId(String identifierTypeID) {
		this._identifierTypeId = identifierTypeID;
	}

	/**
	 * @return the 'sourceSystem' value
	 */
	final public String getSourceSystem() {
		return this._sourceSystem;
	}
	
	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	final public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}
	
	/**
	 * @return the 'updaterId' value
	 */
	final public String getUpdaterId() {
		return this._updaterId;
	}
	
	/**
	 * @param updaterId the 'updaterId' value to set
	 */
	final public void setUpdaterId(String updaterId) {
		this._updaterId = updaterId;
	}
	
	/**
	 * @return the 'updateDate' value
	 */
	final public Date getUpdateDate() {
		return this._updateDate;
	}
	
	/**
	 * @param updateDate the 'updateDate' value to set
	 */
	final public void setUpdateDate(Date updateDate) {
		this._updateDate = updateDate;
	}

	/**
	 * @return the 'comment' value
	 */
	public String getComment() {
		return this._comment;
	}

	/**
	 * @param comment the 'comment' value to set
	 */
	public void setComment(String comment) {
		this._comment = comment;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.behaviours.vessel.VesselAttribute#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return this._identifier == null && this._identifierTypeId == null;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._identifier == null) ? 0 : this._identifier.hashCode());
		result = prime * result + ((this._identifierTypeId == null) ? 0 : this._identifierTypeId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof VesselUniqueIdentifier))
			return false;
		VesselUniqueIdentifier other = (VesselUniqueIdentifier) obj;
		if (this._identifier == null) {
			if (other._identifier != null)
				return false;
		} else if (!this._identifier.equals(other._identifier))
			return false;
		if (this._identifierTypeId == null) {
			if (other._identifierTypeId != null)
				return false;
		} else if (!this._identifierTypeId.equals(other._identifierTypeId))
			return false;
		return true;
	}
}