/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.fao.fi.vrmf.common.models.generated.SSystems;
import org.fao.fi.vrmf.common.models.generated.SSystemsGroups;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Jan 2011
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public final class ExtendedSystemsGroups extends SSystemsGroups {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6805694217855636451L;

	@XmlElementWrapper(name="systems") @XmlElement(name="system")
	@Getter @Setter private List<SSystems> systems;

	/**
	 * Class constructor
	 *
	 * @param source
	 */
	public ExtendedSystemsGroups(SSystemsGroups source) {
		this();
		
		this.setId(source.getId());
		this.setDescription(source.getDescription());
		this.setUpdaterId(source.getUpdaterId());
		this.setUpdateDate(source.getUpdateDate());
		this.setComment(source.getComment());
		
		this.systems = new ArrayList<SSystems>();
	}
}
