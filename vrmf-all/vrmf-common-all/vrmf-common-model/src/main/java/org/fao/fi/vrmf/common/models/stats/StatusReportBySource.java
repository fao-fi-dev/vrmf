/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.stats;

import java.util.Date;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Dec 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Dec 2012
 */
public class StatusReportBySource extends BasicVesselStatsReport {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1021245286390192724L;
	
	private String _sourceSystem;
	private Integer _groupedVessels;
	private Date _lastUpdate;
	private Date _lastReporting;
	
	/**
	 * @return the 'sourceSystem' value
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}
	
	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}
	
	/**
	 * @return the 'numGroupedVessels' value
	 */
	public Integer getGroupedVessels() {
		return this._groupedVessels;
	}

	/**
	 * @param numGroupedVessels the 'numGroupedVessels' value to set
	 */
	public void setGroupedVessels(Integer numGroupedVessels) {
		this._groupedVessels = numGroupedVessels;
	}

	/**
	 * @return the 'lastUpdate' value
	 */
	public Date getLastUpdate() {
		return this._lastUpdate;
	}
	
	/**
	 * @param lastUpdate the 'lastUpdate' value to set
	 */
	public void setLastUpdate(Date lastUpdate) {
		this._lastUpdate = lastUpdate;
	}
	
	/**
	 * @return the 'lastReporting' value
	 */
	public Date getLastReporting() {
		return this._lastReporting;
	}
	
	/**
	 * @param lastReporting the 'lastReporting' value to set
	 */
	public void setLastReporting(Date lastReporting) {
		this._lastReporting = lastReporting;
	}
}