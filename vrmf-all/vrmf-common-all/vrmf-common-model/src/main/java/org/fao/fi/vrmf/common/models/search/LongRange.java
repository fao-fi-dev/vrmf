/**
 * (c) 2015 FAO / UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 4, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 4, 2015
 */
public class LongRange extends AbstractValueRange<Long> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5872327060050188466L;

	/**
	 * Class constructor
	 *
	 */
	public LongRange() {
		super(); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param min
	 * @param max
	 */
	public LongRange(Long min, Long max) {
		super(min, max); // TODO Auto-generated constructor block
	}
}
