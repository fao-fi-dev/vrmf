/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.extended;

import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.vrmf.common.models.generated.Authorizations;
import org.fao.fi.vrmf.common.models.generated.SAuthorizationHolders;
import org.fao.fi.vrmf.common.models.generated.SAuthorizationTerminationReasons;
import org.fao.fi.vrmf.common.models.generated.SCountries;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Dec 2010
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
public class ExtendedAuthorizations extends Authorizations {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3410337073177447648L;
	
	@XmlElement
	@Getter @Setter private SCountries issuingCountry;
	
	@XmlElement
	@Getter @Setter private SAuthorizationHolders authorizationsHolder;
	
	@XmlElement
	@Getter @Setter private SAuthorizationTerminationReasons authorizationTerminationReason;
	
	@XmlElement
	@Getter @Setter private Collection<ExtendedAuthorizationDetails> details;

	/**
	 * Class constructor
	 *
	 * @param source
	 * @param operator
	 */
	public ExtendedAuthorizations(Authorizations source, SCountries issuingCountry, SAuthorizationHolders holder,  SAuthorizationTerminationReasons terminationReason, Collection<ExtendedAuthorizationDetails> authorizationDetails) {
		this.details = authorizationDetails;

		this.setId(source.getId());
		this.setVesselId(source.getVesselId());
		this.setVesselUid(source.getVesselUid());
		this.setAuthorizationId(source.getAuthorizationId());
		this.setIssuingCountryId(source.getIssuingCountryId());
		this.setIssuingCountry(issuingCountry);
		this.setTypeId(source.getTypeId());
		this.setValidFrom(source.getValidFrom());
		this.setValidTo(source.getValidTo());
		this.setTerminationReasonCode(source.getTerminationReasonCode());
		this.setTerminationReferenceDate(source.getTerminationReferenceDate());
		this.setAuthorizationsHolder(holder);
		this.setAuthorizationTerminationReason(terminationReason);
		this.setTerminationReason(source.getTerminationReason());
		this.setReferenceDate(source.getReferenceDate());
		this.setSourceSystem(source.getSourceSystem());
		this.setUpdaterId(source.getUpdaterId());
		this.setUpdateDate(source.getUpdateDate());
		this.setComment(source.getComment());
	}
}