/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.stats;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment 
 * ------------- --------------- -----------------------
 * 19 Oct 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Oct 2011
 */
public class SimpleVesselStatsReport extends BasicVesselStatsReport {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6620598005851832786L;

	private String _description;
	
	/**
	 * Class constructor
	 *
	 */
	public SimpleVesselStatsReport() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param description
	 * @param vessels
	 */
	public SimpleVesselStatsReport(String description, Integer vessels) {
		super(vessels);		
		this._description = description;
	}

	/**
	 * @return the 'description' value
	 */
	public String getDescription() {
		return this._description;
	}

	/**
	 * @param description the 'description' value to set
	 */
	public void setDescription(String description) {
		this._description = description;
	}
}