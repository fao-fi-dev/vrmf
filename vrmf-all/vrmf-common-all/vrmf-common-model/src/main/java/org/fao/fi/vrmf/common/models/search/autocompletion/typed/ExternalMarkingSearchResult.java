/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion.typed;

import org.fao.fi.vrmf.common.models.search.autocompletion.SimpleGroupedSearchResult;



/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Jan 2011
 */
public final class ExternalMarkingSearchResult extends SimpleGroupedSearchResult {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5487439384483848917L;

	/**
	 * Class constructor
	 *
	 */
	public ExternalMarkingSearchResult() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param externalMarking
	 * @param totalOccurrencies
	 * @param occurrencies
	 * @param groupedOccurrencies
	 */
	public ExternalMarkingSearchResult(String externalMarking, int totalOccurrencies, int occurrencies, int groupedOccurrencies) {
		super(externalMarking, totalOccurrencies, occurrencies, groupedOccurrencies);
	}
	
	/**
	 * @return the 'externalMarking' value
	 */
	public String getExternalMarking() {
		return super.getTerm();
	}
	
	/**
	 * @param externalMarking the 'externalMarking' value to set
	 */
	public void setExternalMarking(String externalMarking) {
		super.setTerm(externalMarking);
	}
}