/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion.typed;

import org.fao.fi.vrmf.common.models.search.autocompletion.SimpleGroupedSearchResult;



/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Jan 2011
 */
public final class IdentifierSearchResult extends SimpleGroupedSearchResult {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5487439384483848917L;

	private String _identifierType;
	
	/**
	 * Class constructor
	 *
	 */
	public IdentifierSearchResult() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param identifier
	 * @param totalOccurrencies
	 * @param occurrencies
	 * @param groupedOccurrencies
	 */
	public IdentifierSearchResult(String identifierType, String identifier, int totalOccurrencies, int occurrencies, int groupedOccurrencies) {
		super(identifier, totalOccurrencies, occurrencies, groupedOccurrencies);
		
		this._identifierType = identifierType;
	}
	
	/**
	 * @return the 'identifierType' value
	 */
	public String getIdentifierType() {
		return this._identifierType;
	}

	/**
	 * @param identifierType the 'identifierType' value to set
	 */
	public void setIdentifierType(String identifierType) {
		this._identifierType = identifierType;
	}

	/**
	 * @return the 'identifier' value
	 */
	public String getIdentifier() {
		return super.getTerm();
	}
	
	/**
	 * @param identifier the 'registrationNumber' value to set
	 */
	public void setIdentifier(String identifier) {
		super.setTerm(identifier);
	}
}