/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.stats;

import java.util.Date;

import org.fao.fi.vrmf.common.models.generated.SAgreementContributingParties;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Oct 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Oct 2011
 */
public class AuthorizedVesselStatsReport extends SAgreementContributingParties implements VesselStatsReport, Comparable<AuthorizedVesselStatsReport> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3042348421902714306L;

	private String _countryIso2;
	private String _countryName;
	
	private Date _lastUpdate;
	
	private Integer _vessels;
	private Integer _authorizedVessels;
	private Integer _expiredVessels;
	private Integer _terminatedVessels;
	
	/**
	 * Class constructor
	 *
	 */
	public AuthorizedVesselStatsReport() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param countryIso2
	 * @param countryName
	 * @param lastUpdate
	 * @param vessels
	 * @param authorizedVessels
	 * @param expiredVessels
	 * @param terminatedVessels
	 */
	public AuthorizedVesselStatsReport(String countryIso2, String countryName, Date lastUpdate, Integer vessels, Integer authorizedVessels, Integer expiredVessels, Integer terminatedVessels) {
		super();
		this._countryIso2 = countryIso2;
		this._countryName = countryName;
		this._lastUpdate = lastUpdate;
		this._vessels = vessels;
		this._authorizedVessels = authorizedVessels;
		this._expiredVessels = expiredVessels;
		this._terminatedVessels = terminatedVessels;
	}

	/**
	 * @return the 'countryIso2' value
	 */
	public String getCountryIso2() {
		return this._countryIso2;
	}

	/**
	 * @param countryIso2 the 'countryIso2' value to set
	 */
	public void setCountryIso2(String countryIso2) {
		this._countryIso2 = countryIso2;
	}

	/**
	 * @return the 'countryName' value
	 */
	public String getCountryName() {
		return this._countryName;
	}
	
	/**
	 * @param countryName the 'countryName' value to set
	 */
	public void setCountryName(String countryName) {
		this._countryName = countryName;
	}
	
	/**
	 * @return the 'lastUpdate' value
	 */
	public Date getLastUpdate() {
		return this._lastUpdate;
	}
	
	/**
	 * @param lastUpdate the 'lastUpdate' value to set
	 */
	public void setLastUpdate(Date lastUpdate) {
		this._lastUpdate = lastUpdate;
	}

	/**
	 * @return the 'vessels' value
	 */
	public Integer getVessels() {
		return this._vessels;
	}

	/**
	 * @param vessels the 'vessels' value to set
	 */
	public void setVessels(Integer vessels) {
		this._vessels = vessels;
	}

	/**
	 * @return the 'authorizedVessels' value
	 */
	public Integer getAuthorizedVessels() {
		return this._authorizedVessels;
	}

	/**
	 * @param authorizedVessels the 'authorizedVessels' value to set
	 */
	public void setAuthorizedVessels(Integer authorizedVessels) {
		this._authorizedVessels = authorizedVessels;
	}

	/**
	 * @return the 'expiredVessels' value
	 */
	public Integer getExpiredVessels() {
		return this._expiredVessels;
	}

	/**
	 * @param expiredVessels the 'expiredVessels' value to set
	 */
	public void setExpiredVessels(Integer expiredVessels) {
		this._expiredVessels = expiredVessels;
	}

	/**
	 * @return the 'terminatedVessels' value
	 */
	public Integer getTerminatedVessels() {
		return this._terminatedVessels;
	}

	/**
	 * @param terminatedVessels the 'terminatedVessels' value to set
	 */
	public void setTerminatedVessels(Integer terminatedVessels) {
		this._terminatedVessels = terminatedVessels;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AuthorizedVesselStatsReport another) {
		try {
			return this.getCountryName().compareTo(another.getCountryName());
		} catch (Throwable t) {
			return 0;
		}
	}
}