/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-model)
 */
package org.fao.fi.vrmf.common.models.search.autocompletion.typed;

import org.fao.fi.vrmf.common.models.search.autocompletion.SimpleGroupedSearchResult;



/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Jan 2011
 */
public final class FishingLicenseSearchResult extends SimpleGroupedSearchResult {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5487439384483848917L;

	/**
	 * Class constructor
	 *
	 */
	public FishingLicenseSearchResult() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param fishingLicenseNumber
	 * @param totalOccurrencies
	 * @param occurrencies
	 * @param groupedOccurrencies
	 */
	public FishingLicenseSearchResult(String fishingLicenseNumber, int totalOccurrencies, int occurrencies, int groupedOccurrencies) {
		super(fishingLicenseNumber, totalOccurrencies, occurrencies, groupedOccurrencies);
	}
	
	/**
	 * @return the 'licenseId' value
	 */
	public String getLicenseId() {
		return super.getTerm();
	}
	
	/**
	 * @param licenseId the 'licenseId' value to set
	 */
	public void setLicenseId(String licenseId) {
		super.setTerm(licenseId);
	}
}