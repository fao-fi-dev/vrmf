/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.emitters.impl;

import java.io.OutputStream;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.model.core.spi.services.ProcessTracker;
import org.fao.fi.sh.utility.common.helpers.beans.text.XMLPrettyPrinter;
import org.fao.fi.sh.utility.core.helpers.singletons.text.DateFormatHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.AbstractDatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.DatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Jan 2012
 */
@Deprecated
public class XMLDatasetEmitter<DATA extends Exportable> extends AbstractDatasetEmitter<DATA> {
	static final public String XML_NAMESPACE_PROPERTY 	    = "namespace";
	static final public String XML_NAMESPACE_URI_PROPERTY   = "namespaceURI"; 
	static final public String XML_SCHEMA_LOCATION_PROPERTY = "schemaLocation";
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.export.emitters.DatasetEmitter#emit(java.io.OutputStream, org.fao.vrmf.utilities.common.utils.process.ProcessTracker, org.fao.vrmf.utilities.common.models.export.DataExportModel, java.util.Collection, java.util.Properties)
	 */
	@Override
	public void emit(OutputStream out, ProcessTracker tracker, DataExportModel<? super DATA> model, Collection<DATA> data, Properties parameters) throws Throwable {
		String namespace = (String)parameters.get(XML_NAMESPACE_PROPERTY);
		URI namespaceURI = (URI)parameters.get(XML_NAMESPACE_URI_PROPERTY);
		URL schemaLocation = (URL)parameters.get(XML_SCHEMA_LOCATION_PROPERTY);
		
		Date dateOfBuild = (Date)parameters.get(DatasetEmitter.DATE_OF_BUILD_PROPERTY);
		final String type = (String)parameters.get(DatasetEmitter.TYPE_PROPERTY);
		final String title = (String)parameters.get(DatasetEmitter.TITLE_PROPERTY);
		final Integer datasetBaseSize = (Integer)parameters.get(DatasetEmitter.DATASET_BASE_SIZE_PROPERTY);
		final Long elapsed = (Long)parameters.get(DatasetEmitter.ELAPSED_PROPERTY);
		
		if(dateOfBuild == null)
			dateOfBuild = new Date();
		
		if(namespace == null)
			namespace = "vrmfRef";
		
		if(namespaceURI == null)
			namespaceURI = new URI("http://www.fao.org/fi/vrmf/webservices");
		
		if(schemaLocation == null)
			schemaLocation = new URL("http://www.fao.org/fi/vrmf/webservices" + "/schema/" + namespace + ".xsd");
		
		XMLPrettyPrinter xmlEmitter = new XMLPrettyPrinter(namespace, namespaceURI, schemaLocation);
		
		xmlEmitter.open(type);
		
			xmlEmitter.open("meta");
				xmlEmitter.element("title", title);
				xmlEmitter.element("dateOfBuild", DateFormatHelper.formatDateAndTime(dateOfBuild),
												  new NameValuePair("timestamp", dateOfBuild.getTime()));
				xmlEmitter.element("elapsed", elapsed);
				xmlEmitter.element("datasetSize", data.size());
				xmlEmitter.element("datasetBaseSize", datasetBaseSize);
				
			xmlEmitter.close();
			
			xmlEmitter.open("items", new NameValuePair("totalNumber", data.size()));
			
			int counter = 0;
			
			for(DATA current : data) {
				xmlEmitter.open("item", 
								new NameValuePair("number", counter++),
								new NameValuePair("type", current.getClass().getSimpleName()));
				
				for(DataExportFieldModel<? super DATA> field : model.getFieldsModel()) {
					xmlEmitter.element("property",
									   this.getFieldValue(model, field, current),
									   new NameValuePair("type", field.getFieldClass().getSimpleName()),
									   new NameValuePair("name", field.getFieldExpression()),
									   new NameValuePair("label", field.getFieldLabel()));
				}
				
				xmlEmitter.close();
			}
			
			xmlEmitter.close();
		
		xmlEmitter.close();
		
		String content = xmlEmitter.toString();
		
		if(content != null)
			out.write(content.getBytes("UTF-8"));
	}
}
