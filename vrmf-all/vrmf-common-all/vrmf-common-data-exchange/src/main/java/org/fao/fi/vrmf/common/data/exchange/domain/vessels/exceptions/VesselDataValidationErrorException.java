/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.vessels.exceptions;

import org.fao.fi.vrmf.common.data.exchange.core.exceptions.ValidationErrorException;
import org.fao.fi.vrmf.common.data.exchange.domain.vessels.VesselDataExchangeModel;
import org.fao.fi.vrmf.common.models.extended.GenericCurrentVessel;
import org.fao.fi.vrmf.common.models.generated.Vessels;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Nov 2011
 */
public class VesselDataValidationErrorException extends ValidationErrorException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6963639350596073049L;

	private GenericCurrentVessel<?> _existingVessel;
	private VesselDataExchangeModel<?> _uploadedVessel;

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public VesselDataValidationErrorException(String message, GenericCurrentVessel<? extends Vessels> existingVessel, VesselDataExchangeModel<? extends Vessels> uploadedVessel) {
		this(message, null, existingVessel, uploadedVessel);
	}
	
	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public VesselDataValidationErrorException(String message, Throwable cause, GenericCurrentVessel<? extends Vessels> existingVessel, VesselDataExchangeModel<? extends Vessels> uploadedVessel) {
		super(message, cause);
		
		this._existingVessel = existingVessel;
		this._uploadedVessel = uploadedVessel;		
	}

	/**
	 * @return the 'existingVessel' value
	 */
	public GenericCurrentVessel<?> getExistingVessel() {
		return this._existingVessel;
	}

	/**
	 * @return the 'uploadedVessel' value
	 */
	public VesselDataExchangeModel<?> getUploadedVessel() {
		return this._uploadedVessel;
	}
}