/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.vessels.transformers;

import java.util.Date;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.utility.model.extensions.dates.SmartDate;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModelTransformer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Feb 2012
 */
abstract public class AbstractVesselAuthorizationRangeToAuthorizationStatusTransformer<DATA extends Exportable> implements DataExportModelTransformer<DATA> {
	final protected String doCalculateAuthorizationStatus(Date validFrom, Date validTo, Date terminationDate) {
		SmartDate now = new SmartDate().roundToDay();
		SmartDate from = validFrom == null ? null : new SmartDate(validFrom).roundToDay();
		SmartDate to = validTo == null ? null : new SmartDate(validTo).roundToDay();
		SmartDate terminated = terminationDate == null ? null : new SmartDate(terminationDate).roundToDay();
		
		String authorizationStatus = "UNKNOWN";
		
		boolean isTerminated = terminated != null;
		boolean isInvalid = !isTerminated && from != null && to != null && from.after(to);
		boolean isFuture = !isInvalid && from.after(now);
		boolean isValid = !isFuture && 
						  ( from == null || from.equals(now) || from.before(now) ) &&
						  ( to == null || to.equals(now) || to.after(now) );
		boolean isExpired = !isValid && 
						  ( from == null || from.equals(now) || from.before(now) ) &&
						  ( to == null || to.before(now) );

		if(isTerminated) {
			authorizationStatus = "TERMINATED";
		} else if(isExpired) {
			authorizationStatus = "EXPIRED";
		} else if(isValid) {
			authorizationStatus = "AUTHORIZED";
		} else if(isFuture) {
			authorizationStatus = "TO_BE_AUTHORIZED";
		} else if(isInvalid) {
			authorizationStatus = "INVALID";
		}
		
		return authorizationStatus;
	}
}
