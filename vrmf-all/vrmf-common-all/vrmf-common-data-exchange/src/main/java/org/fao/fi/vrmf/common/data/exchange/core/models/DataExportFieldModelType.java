/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.models;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Nov 2011
 */
public enum DataExportFieldModelType {
	MANDATORY(DataExportFieldModelType.MANDATORY_MASK),
	REQUIRED(DataExportFieldModelType.REQUIRED_MASK),
	OPTIONAL(DataExportFieldModelType.OPTIONAL_MASK),
	HIDDEN(DataExportFieldModelType.HIDDEN_MASK),
	READ_ONLY(DataExportFieldModelType.READ_ONLY_MASK),
	
	OPTIONAL_HIDDEN(DataExportFieldModelType.OPTIONAL_HIDDEN_MASK),					
	READ_ONLY_HIDDEN(DataExportFieldModelType.READ_ONLY_HIDDEN_MASK),	
	READ_ONLY_OPTIONAL(DataExportFieldModelType.READ_ONLY_OPTIONAL_MASK),
	READ_ONLY_OPTIONAL_HIDDEN(DataExportFieldModelType.READ_ONLY_OPTIONAL_HIDDEN_MASK),
	REQUIRED_HIDDEN(DataExportFieldModelType.REQUIRED_HIDDEN_MASK),
	MANDATORY_HIDDEN(DataExportFieldModelType.MANDATORY_HIDDEN_MASK);
	
	static final public int MANDATORY_MASK = ( 2 << 0 );
	static final public int REQUIRED_MASK  = ( 2 << 1 );
	static final public int OPTIONAL_MASK  = ( 2 << 2 );
	static final public int READ_ONLY_MASK = ( 2 << 3 );
	static final public int HIDDEN_MASK    = ( 2 << 4 );
	
	static final public int OPTIONAL_HIDDEN_MASK  		   = OPTIONAL_MASK | HIDDEN_MASK;
	static final public int READ_ONLY_HIDDEN_MASK		   = READ_ONLY_MASK | HIDDEN_MASK;
	static final public int READ_ONLY_OPTIONAL_MASK 	   = READ_ONLY_MASK | OPTIONAL_MASK;
	static final public int READ_ONLY_OPTIONAL_HIDDEN_MASK = READ_ONLY_OPTIONAL_MASK | HIDDEN_MASK;
	static final public int MANDATORY_HIDDEN_MASK 		   = MANDATORY_MASK | HIDDEN_MASK;	
	static final public int REQUIRED_HIDDEN_MASK 		   = REQUIRED_MASK | HIDDEN_MASK;
	
	private int _typeMask;
	
	private DataExportFieldModelType(int typeMask) {
		this._typeMask = typeMask;
	}

	/**
	 * @return the 'typeMask' value
	 */
	public int getTypeMask() {
		return this._typeMask;
	}
	
	public boolean is(int type) {
		return ( this._typeMask & type ) > 0;
	}
	
	public boolean is(DataExportFieldModelType type) {
		return ( this._typeMask & type._typeMask ) == type._typeMask;
	}
	
	public boolean isNot(int type) {
		return ( this._typeMask & type ) == 0;
	}
	
	public boolean isNot(DataExportFieldModelType type) {
		return ( this._typeMask & type._typeMask ) == 0;
	}
	
	static public DataExportFieldModelType fromTypeMask(int typeMask) {
		for(DataExportFieldModelType type : DataExportFieldModelType.values())
			if(type.getTypeMask() == typeMask)
				return type;
		
		return null;
	}
}