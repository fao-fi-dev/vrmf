/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModelValidator;
import org.fao.fi.vrmf.common.data.exchange.core.validators.NOPValidator;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Nov 2011
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DataExchangeMappedFieldModelMetadata {
	Class<?> keyType() default String.class;
	Class<?> mappedType();
	String label();
	DataExchangeMappedFieldKeyToLabelMapping[] keyToLabelMappings();
	Class<? extends DataExportModelValidator> validator() default NOPValidator.class;	
}
