/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.vessels.transformers;

import java.io.Serializable;
import java.util.Map;

import org.fao.fi.vrmf.common.data.exchange.core.transformers.IdentityDataModelTransformer;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.Vessels;
import org.fao.fi.vrmf.common.models.generated.VesselsToFlags;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Nov 2011
 */
public class VesselFlagIDToISO2DataModelTransformer<VESSEL extends Vessels> extends IdentityDataModelTransformer<VESSEL> {
	private Map<Integer, SCountries> _countriesMap;
	
	/**
	 * Class constructor
	 *
	 * @param countriesMap
	 */
	public VesselFlagIDToISO2DataModelTransformer(Map<Integer, SCountries> countriesMap) {
		super();
		
		this._countriesMap = countriesMap;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.models.export.DataModelTransformer#transform(java.lang.Object, java.io.Serializable)
	 */
	@Override
	public Serializable transform(Serializable value, VESSEL data) {
		if(value == null)
			return null;
		
		if(this._countriesMap == null ||
		   !VesselsToFlags.class.isAssignableFrom(value.getClass()))
			return value;
		
		SCountries country = this._countriesMap.get(((VesselsToFlags)value).getCountryId());
		
		if(country == null)
			return null;
		
		return country.getIso2Code();
	}
	
	
}