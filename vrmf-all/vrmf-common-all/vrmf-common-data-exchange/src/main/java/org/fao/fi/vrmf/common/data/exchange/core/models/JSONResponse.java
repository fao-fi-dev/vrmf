/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.models;

import java.io.Serializable;
import java.io.Writer;

import flexjson.JSONSerializer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
public class JSONResponse<DATA extends Serializable> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7478567973668600309L;
	
	private boolean _error;
	private String _errorMessage;
	private String _additionalErrorMessage;
	private String[] _trace;
	private long _elapsed;
	private DATA _data;
	
	final private static String[] DEFAULT_EXCLUSION_PATTERNS = {
		"class", 
		"*.class"
	};
	
	final private static String[] DEFAULT_INCLUSION_PATTERNS = {
		"error", 
		"errorMessage", 
		"additionalErrorMessage", 
		"trace", 
		"elapsed",
		"data"
	};
	
	/**
	 * Class constructor
	 *
	 */
	public JSONResponse() {
		super();
		
		this._error = false;
		this._errorMessage = null;
		this._additionalErrorMessage = null;
		this._trace = null;
	}

	/**
	 * Class constructor
	 *
	 * @param data
	 */
	public JSONResponse(DATA data) {
		this();
		
		this._data = data;
	}
	
	/**
	 * Class constructor
	 *
	 * @param data
	 * @param elapsed
	 */
	public JSONResponse(DATA data, long elapsed) {
		this(data);
		
		this._elapsed = elapsed;
	}

	/**
	 * @return the 'error' value
	 */
	public boolean isError() {
		return this._error;
	}
	
	/**
	 * @param error the 'error' value to set
	 */
	public void setError(boolean error) {
		this._error = error;
	}
	
	/**
	 * @return the 'errorMessage' value
	 */
	public String getErrorMessage() {
		return this._errorMessage;
	}
	
	/**
	 * @param errorMessage the 'errorMessage' value to set
	 */
	public void setErrorMessage(String errorMessage) {
		this._errorMessage = errorMessage;
	}
	
	/**
	 * @return the 'additionalErrorMessage' value
	 */
	public String getAdditionalErrorMessage() {
		return this._additionalErrorMessage;
	}
	
	/**
	 * @param additionalErrorMessage the 'additionalErrorMessage' value to set
	 */
	public void setAdditionalErrorMessage(String additionalErrorMessage) {
		this._additionalErrorMessage = additionalErrorMessage;
	}
	
	/**
	 * @return the 'trace' value
	 */
	public String[] getTrace() {
		return this._trace;
	}
	
	/**
	 * @param trace the 'trace' value to set
	 */
	public void setTrace(String[] trace) {
		this._trace = trace;
	}
	
	/**
	 * @return the 'elapsed' value
	 */
	public long getElapsed() {
		return this._elapsed;
	}
	
	/**
	 * @param elapsed the 'elapsed' value to set
	 */
	public void setElapsed(long elapsed) {
		this._elapsed = elapsed;
	}
	
	/**
	 * @return the 'data' value
	 */
	public DATA getData() {
		return this._data;
	}
	
	/**
	 * @param data the 'data' value to set
	 */
	public void setData(DATA data) {
		this._data = data;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.JSONify();
	}
	
	/**
	 * @return
	 */
	public String JSONify() {
		return this.JSONifyExcluding();
	}
	
	/**
	 * @return
	 */
	public void JSONify(Writer writer) {
		this.JSONifyExcluding(writer);
	}
	
	/**
	 * @return
	 */
	public String JSONify(String[] dataInclusionPatterns, String[] dataExclusionPatterns) {
		return new JSONSerializer().include(DEFAULT_INCLUSION_PATTERNS).exclude(DEFAULT_EXCLUSION_PATTERNS).include(dataInclusionPatterns).exclude(dataExclusionPatterns).deepSerialize(this);
	}

	public void JSONify(Writer writer, String[] dataInclusionPatterns, String[] dataExclusionPatterns) {
		new JSONSerializer().include(DEFAULT_INCLUSION_PATTERNS).exclude(DEFAULT_EXCLUSION_PATTERNS).include(dataInclusionPatterns).exclude(dataExclusionPatterns).deepSerialize(this, writer);
	}
	
	/**
	 * @param dataExclusionPatterns
	 * @return
	 */
	public String JSONifyExcluding(String... dataExclusionPatterns) {
		return new JSONSerializer().exclude(DEFAULT_EXCLUSION_PATTERNS).exclude(dataExclusionPatterns).deepSerialize(this);
	}
	
	/**
	 * @param dataExclusionPatterns
	 * @return
	 */
	public void JSONifyExcluding(Writer writer, String... dataExclusionPatterns) {
		new JSONSerializer().exclude(DEFAULT_EXCLUSION_PATTERNS).exclude(dataExclusionPatterns).deepSerialize(this, writer);
	}
	
	/**
	 * @param dataInclusionPatterns
	 * @return
	 */
	public String JSONifyOnly(String... dataInclusionPatterns) {
		return new JSONSerializer().include(DEFAULT_INCLUSION_PATTERNS).include(dataInclusionPatterns).exclude("*").deepSerialize(this);
	}
	
	/**
	 * @param dataInclusionPatterns
	 * @return
	 */
	public void JSONifyOnly(Writer writer, String... dataInclusionPatterns) {
		new JSONSerializer().include(DEFAULT_INCLUSION_PATTERNS).include(dataInclusionPatterns).exclude("*").deepSerialize(this, writer);
	}
}
