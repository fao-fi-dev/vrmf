/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.transformers;

import java.io.Serializable;
import java.net.URL;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExchangeModel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Nov 2011
 */
public class ValueToURLTransformer extends IdentityDataModelTransformer<Exportable> {
	private String _baseURL = null;
	
	public ValueToURLTransformer(String baseURL) {
		this._baseURL = baseURL;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.models.export.DataModelTransformer#transform(java.lang.Object, java.io.Serializable)
	 */
	@Override
	public Serializable transform(Serializable value, Exportable data) {
		if(value == null)
			return null;
		
		try {
			return new URL(this._baseURL + "/" + value.toString());
		} catch (Throwable t) {
			return new String(this._baseURL + "/" + value.toString());
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.export.DataExportModelTransformer#reverseTransform(java.io.Serializable, org.fao.vrmf.utilities.common.models.export.DataExchangeModel)
	 */
	@Override
	public Serializable reverseTransform(Serializable value, DataExchangeModel<Exportable> model) {
		return this.transform(value, null);
	}
}