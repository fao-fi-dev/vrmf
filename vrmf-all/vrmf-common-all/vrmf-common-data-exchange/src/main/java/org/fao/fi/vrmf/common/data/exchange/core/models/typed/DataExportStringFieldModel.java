/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.models.typed;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModelType;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModelTransformer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Nov 2011
 */
public class DataExportStringFieldModel<DATA extends Exportable> extends DataExportFieldModel<DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3059684874250587009L;
	
	/**
	 * Class constructor
	 */
	public DataExportStringFieldModel() {
		super(String.class);
	}

	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param type
	 * @param fieldExpression
	 * @param fieldClass
	 */
	public DataExportStringFieldModel(Class<DATA> owner, DataExportFieldModelType type, String fieldExpression) {
		super(owner, type, fieldExpression, String.class);
	}

	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param type
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportStringFieldModel(Class<DATA> owner, DataExportFieldModelType type, String fieldExpression, String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(owner, type, fieldExpression, fieldLabel, String.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param fieldExpression
	 * @param fieldClass
	 */
	public DataExportStringFieldModel(Class<DATA> owner, String fieldExpression) {
		super(owner, fieldExpression, String.class);
	}

	/**
	 * Class constructor
	 *
	 * @param type
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportStringFieldModel(DataExportFieldModelType type, String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(type, fieldLabel, String.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param type
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportStringFieldModel(DataExportFieldModelType type, String fieldExpression, String fieldLabel) {
		super(type, fieldExpression, fieldLabel, String.class);
	}

	/**
	 * Class constructor
	 *
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportStringFieldModel(String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(fieldLabel, String.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportStringFieldModel(String fieldExpression, String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(fieldExpression, fieldLabel, String.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportStringFieldModel(String fieldExpression, String fieldLabel) {
		super(fieldExpression, fieldLabel, String.class);
	}
}