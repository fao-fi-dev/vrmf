/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.emitters.impl;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.model.core.spi.services.ProcessTracker;
import org.fao.fi.sh.utility.common.helpers.beans.text.HTMLPrettyPrinter;
import org.fao.fi.sh.utility.core.helpers.singletons.text.DateFormatHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.AbstractDatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.DatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Jan 2012
 */
@Deprecated
public class HTMLDatasetEmitter<DATA extends Exportable> extends AbstractDatasetEmitter<DATA> {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.export.emitters.DatasetEmitter#emit(java.io.OutputStream, org.fao.vrmf.utilities.common.utils.process.ProcessTracker, org.fao.vrmf.utilities.common.models.export.DataExportModel, java.util.Collection, java.util.Properties)
	 */
	@Override
	public void emit(OutputStream out, ProcessTracker tracker, DataExportModel<? super DATA> model, Collection<DATA> data, Properties parameters) throws Throwable {
		final boolean track = tracker != null;
		
		HTMLPrettyPrinter emitter = new HTMLPrettyPrinter();
		
		OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");
			
		final String title = (String)this.getProperty(parameters, DatasetEmitter.TITLE_PROPERTY);
		
		if(track) tracker.start("Building HTML file...");
		
		String[] stylesheets = new String[] {
			"http://www.fao.org/figis/vrmf/base/styles/all.css",
			"http://www.fao.org/figis/vrmf/hsvar/styles/tables.css",
			"http://www.fao.org/figis/vrmf/hsvar/styles/tablesorter.css",
			"http://www.fao.org/figis/vrmf/hsvar/styles/reports.css"
		};
		
		writer.write(emitter.open("html"));		
			writer.write(emitter.open("head"));
//				writer.write(emitter.empty("meta", new NameValuePair("http-equiv", "Cache-Control"),
//												   new NameValuePair("content", "public")));
						
			for(String stylesheet : stylesheets) {
				writer.write(emitter.element("link", " ", new NameValuePair[] { 
					new NameValuePair("rel", "stylesheet"),
					new NameValuePair("type", "text/css"),
					new NameValuePair("href", stylesheet),
				}));
			}
			
				writer.write(emitter.open("title"));
				writer.write(emitter.text(title));
				writer.write(emitter.close());
			writer.write(emitter.close());
			
			writer.write(emitter.open("body"));
			
			this.addHTMLBody(writer, emitter, tracker, model, data, parameters);
		
			writer.write(emitter.close());
		
		writer.write(emitter.close());
		
		if(track) tracker.finish("Finished building XLS file: " + data.size() + " rows processed");
		
		writer.flush();
		writer.close();
	}
	
	protected void addHTMLBody(Writer writer, HTMLPrettyPrinter emitter, ProcessTracker tracker, DataExportModel<? super DATA> dataModel, Collection<DATA> data, Properties parameters) throws Throwable {
		final boolean track = tracker != null;
		
		final String title = (String)this.getProperty(parameters, DatasetEmitter.TITLE_PROPERTY);

		Date dateOfBuild = (Date)this.getProperty(parameters, DatasetEmitter.DATE_OF_BUILD_PROPERTY);
				
		if(dateOfBuild == null)
			dateOfBuild = new Date();
		
		final Long elapsed = (Long)this.getProperty(parameters, DatasetEmitter.ELAPSED_PROPERTY);

		final Integer datasetBaseSize = (Integer)this.getProperty(parameters, DatasetEmitter.DATASET_BASE_SIZE_PROPERTY);
		
		writer.write(emitter.open("table", new NameValuePair("id", (String)this.getProperty(parameters, DatasetEmitter.TYPE_PROPERTY)), 
										   new NameValuePair("class", "default reportTable")));

		if(track) tracker.setStatus("Building headers...");
		
			writer.write(emitter.open("thead"));

				writer.write(emitter.open("tr"));

					writer.write(emitter.open("th", new NameValuePair("colspan", dataModel.getFieldsModel().size())));
						writer.write(emitter.text(title));
					writer.write(emitter.close());

				writer.write(emitter.close());

				writer.write(emitter.open("tr"));
		
		for (DataExportFieldModel<? super DATA> field : dataModel.getFieldsModel()) {
					writer.write(emitter.open("th", new NameValuePair("class", emitter.convertAttributeValue(field.getFieldLabel()))));

						writer.write(emitter.text(field.getFieldLabel()));

					writer.write(emitter.close());
		}

		if(track) tracker.setStatus("Headers built");
		
//		if(this.getHTMLExtraHeaders() != null)
//			for(NameValuePair extraHeader : this.getHTMLExtraHeaders()) {
//					writer.write(emitter.open("th", new NameValuePair("class", extraHeader.getValue())));
//						writer.write(emitter.text(extraHeader.getName()));
//					writer.write(emitter.close());
//			}
		
				writer.write(emitter.close());

			writer.write(emitter.close());

		Object value = null;
		int row = 0;

		if(track) tracker.setStatus("Building actual content...");
		
			writer.write(emitter.open("tbody"));

		for (DATA report : data) {
				writer.write(emitter.open("tr", new NameValuePair("id", (String)this.getProperty(parameters, DatasetEmitter.TYPE_PROPERTY) + "_row_" + row++)));

			for (DataExportFieldModel<? super DATA> field : dataModel.getFieldsModel()) {
				value = this.getFieldValue(dataModel, field, report);
					writer.write(emitter.open("td", new NameValuePair("class", emitter.convertAttributeValue(field.getFieldLabel()))));

				if(this.isURL(value)) {
					NameValuePair URLData = this.getURLDetails((URL)value);
						writer.write(emitter.open("a", 
												  new NameValuePair("target", "_BLANK"),
												  new NameValuePair("href", URLData.getValue())));
							writer.write(emitter.text(URLData.getName()));
						writer.write(emitter.close());
				} else if(Number.class.isAssignableFrom(field.getFieldClass())) {
					if (value != null)
						writer.write(emitter.text(value.toString()));
					else
						writer.write(emitter.text(DatasetEmitter.DEFAULT_NULL_LABEL));
				} else if (Date.class.isAssignableFrom(field.getFieldClass())) {
					if (value != null)
						writer.write(emitter.text(DateFormatHelper.formatDate((Date) value)));
					else
						writer.write(emitter.text(DatasetEmitter.DEFAULT_NULL_LABEL));
				} else if (value != null) {
						writer.write(emitter.text(value.toString()));
				} else {
						writer.write(emitter.text(DatasetEmitter.DEFAULT_NULL_LABEL));
				}
				
					writer.write(emitter.close());
			}

//			this.addHTMLExtraDetails(writer, emitter, report, vesselBaseSize);

				writer.write(emitter.close());
				
				if(track) tracker.setCompletionStatus(this.percentage(row, data.size(), 2));
		}
					
			writer.write(emitter.close());

		writer.write(emitter.close());

		if(track) tracker.setStatus("Actual content built");
		
		if(track) tracker.setStatus("Building metadata section...");
		
		writer.write(emitter.open("p", new NameValuePair("id", (String)this.getProperty(parameters, DatasetEmitter.TYPE_PROPERTY) + "_creationTime"), new NameValuePair("class", "summaryReport")));

			writer.write(emitter.open("label"));
				writer.write(emitter.text("Creation time :"));
			writer.write(emitter.close());

			writer.write(emitter.open("span"));
				writer.write(emitter.text(DateFormatHelper.formatDateAndTime(dateOfBuild)));
			writer.write(emitter.close());
			
		writer.write(emitter.close());

		writer.write(emitter.open("p", new NameValuePair("id", (String)this.getProperty(parameters, DatasetEmitter.TYPE_PROPERTY) + "_elapsed"), new NameValuePair("class", "summaryReport")));

			writer.write(emitter.open("label"));
				writer.write(emitter.text("Elapsed (mSec) :"));
			writer.write(emitter.close());

			writer.write(emitter.open("span"));
				writer.write(emitter.text(String.valueOf(elapsed)));
			writer.write(emitter.close());
			
		writer.write(emitter.close());

		writer.write(emitter.open("p", new NameValuePair("id", (String)this.getProperty(parameters, DatasetEmitter.TYPE_PROPERTY) + "_datasetEntries"), new NameValuePair("class", "summaryReport")));

			writer.write(emitter.open("label"));
				writer.write(emitter.text("Dataset entries :"));
			writer.write(emitter.close());

			writer.write(emitter.open("span"));
				writer.write(emitter.text(String.valueOf(data.size())));
			writer.write(emitter.close());
			
		writer.write(emitter.close());
		
		writer.write(emitter.open("p", new NameValuePair("id", (String)this.getProperty(parameters, DatasetEmitter.TYPE_PROPERTY) + "_totalDatasetEntries"), new NameValuePair("class", "summaryReport")));

		writer.write(emitter.open("label"));
			writer.write(emitter.text("Total dataset entries :"));
		writer.write(emitter.close());

		writer.write(emitter.open("span"));
			writer.write(emitter.text(String.valueOf(datasetBaseSize)));
		writer.write(emitter.close());
		
		if(track) tracker.setStatus("Metadata section has been built");
		
		writer.write(emitter.close());
	}
}