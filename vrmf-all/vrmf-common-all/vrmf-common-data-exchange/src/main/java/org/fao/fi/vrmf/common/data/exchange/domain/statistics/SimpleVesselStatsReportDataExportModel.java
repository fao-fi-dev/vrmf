/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.statistics;

import java.util.ArrayList;
import java.util.List;

import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportIntegerFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportStringFieldModel;
import org.fao.fi.vrmf.common.models.stats.SimpleVesselStatsReport;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Nov 2011
 */
public class SimpleVesselStatsReportDataExportModel extends DataExportModel<SimpleVesselStatsReport> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2459798963796592050L;

	/**
	 * Class constructor
	 *
	 */
	public SimpleVesselStatsReportDataExportModel() {
		super();

		List<DataExportFieldModel<SimpleVesselStatsReport>> model = new ArrayList<DataExportFieldModel<SimpleVesselStatsReport>>();
		model.add(new DataExportStringFieldModel<SimpleVesselStatsReport>("description", "Information"));
		model.add(new DataExportIntegerFieldModel<SimpleVesselStatsReport>("vessels", "Total vessels"));
		
		this.setFieldsModel(model);
	}
}
