/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.validators.typed;

import java.util.Set;

import org.fao.fi.vrmf.common.data.exchange.core.validators.CodeListValidator;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2011
 */
public class StringCodeListValidator extends CodeListValidator<String> {
	/**
	 * Class constructor
	 *
	 * @param codes
	 */
	public StringCodeListValidator(Set<String> codes) {
		super(codes);
	}
}