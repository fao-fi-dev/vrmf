/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.models.typed;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModelType;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModelTransformer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Nov 2011
 */
public class DataExportIntegerFieldModel<DATA extends Exportable> extends DataExportFieldModel<DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3059684874250587009L;

	/**
	 * Class constructor
	 */
	public DataExportIntegerFieldModel() {
		super(Integer.class);
	}
	
	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param type
	 * @param fieldExpression
	 * @param fieldClass
	 */
	public DataExportIntegerFieldModel(Class<DATA> owner, DataExportFieldModelType type, String fieldExpression) {
		super(owner, type, fieldExpression, Integer.class);
	}

	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param type
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportIntegerFieldModel(Class<DATA> owner, DataExportFieldModelType type, String fieldExpression, String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(owner, type, fieldExpression, fieldLabel, Integer.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param fieldExpression
	 * @param fieldClass
	 */
	public DataExportIntegerFieldModel(Class<DATA> owner, String fieldExpression) {
		super(owner, fieldExpression, Integer.class);
	}

	/**
	 * Class constructor
	 *
	 * @param type
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportIntegerFieldModel(DataExportFieldModelType type, String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(type, fieldLabel, Integer.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param type
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportIntegerFieldModel(DataExportFieldModelType type, String fieldExpression, String fieldLabel) {
		super(type, fieldExpression, fieldLabel, Integer.class);
	}

	/**
	 * Class constructor
	 *
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportIntegerFieldModel(String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(fieldLabel, Integer.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportIntegerFieldModel(String fieldExpression, String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(fieldExpression, fieldLabel, Integer.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportIntegerFieldModel(String fieldExpression, String fieldLabel) {
		super(fieldExpression, fieldLabel, Integer.class);
	}
}