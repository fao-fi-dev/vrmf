/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Nov 2011
 */
public class FormatValidationException extends ValidationException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2142536934945426948L;

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public FormatValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public FormatValidationException(String message) {
		super(message);
	}
}