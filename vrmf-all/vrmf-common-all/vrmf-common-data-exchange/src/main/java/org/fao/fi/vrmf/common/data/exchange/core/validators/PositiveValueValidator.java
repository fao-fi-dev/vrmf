/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.validators;

import java.io.Serializable;

import org.fao.fi.vrmf.common.data.exchange.core.exceptions.ConstraintValidationException;
import org.fao.fi.vrmf.common.data.exchange.core.exceptions.ValidationException;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModelValidator;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2011
 */
public class PositiveValueValidator implements DataExportModelValidator {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.export.DataExportModelValidator#validate(java.io.Serializable, java.lang.String)
	 */
	@Override
	public void validate(Serializable value, String label) throws ValidationException {
		if(value == null)
		   	throw new ConstraintValidationException("Value must be non-null and greater than zero for field '" + label + "'");
		
		if(!Number.class.isAssignableFrom(value.getClass())) {
			throw new ConstraintValidationException("Value must be numeric and greater than zero for field '" + label + "' (currently: '" + value + "')");
		}
		
		double doubleValue = Double.NaN;
		
		try {
			doubleValue = Double.valueOf(value.toString());
		} catch (Throwable t) {
			throw new ConstraintValidationException("Value must be numeric and greater than zero for field '" + label + "' (currently: '" + value + "')");			
		}

		if(Double.compare(doubleValue, 0D) <= 0)
			throw new ConstraintValidationException("Value must be greater than zero for field '" + label + "' (currently: " + value + ")");
	}	
}
