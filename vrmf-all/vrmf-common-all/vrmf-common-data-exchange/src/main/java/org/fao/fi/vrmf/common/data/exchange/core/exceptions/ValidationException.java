/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Nov 2011
 */
abstract public class ValidationException extends Throwable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5367733652784751942L;
	
	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public ValidationException(String message) {
		super(message);
	}
	
	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public ValidationException(String message, Throwable cause) {
		super(message, cause);
	}		
}
