/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.vessels.transformers;

import java.io.Serializable;

import org.fao.fi.vrmf.common.data.exchange.core.models.DataExchangeModel;
import org.fao.fi.vrmf.common.data.exchange.domain.vessels.VesselDataExchangeModel;
import org.fao.fi.vrmf.common.models.extended.ExtendedAuthorizations;
import org.fao.fi.vrmf.common.models.extended.GenericCurrentVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Nov 2011
 */
public class CurrentVesselAuthorizationRangeToAuthorizationStatusTransformer<DATA extends GenericCurrentVessel<?>> extends AbstractVesselAuthorizationRangeToAuthorizationStatusTransformer<DATA> {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.models.export.DataModelTransformer#transform(java.lang.Object, java.io.Serializable)
	 */
	@Override
	public Serializable transform(Serializable value, DATA data) {
		ExtendedAuthorizations auth = data.getLastAuthorization();
		
		if(auth == null)
			return null;
		
		return this.doCalculateAuthorizationStatus(auth.getValidFrom(), auth.getValidTo(), auth.getTerminationReferenceDate());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.export.DataExportModelTransformer#reverseTransform(java.io.Serializable, org.fao.vrmf.utilities.common.models.export.DataExchangeModel)
	 */
	@Override
	public Serializable reverseTransform(Serializable value, DataExchangeModel<DATA> model) {
		VesselDataExchangeModel<DATA> asVesselExchangeModel = (VesselDataExchangeModel<DATA>)model;
		
		return this.doCalculateAuthorizationStatus(asVesselExchangeModel.getAuthorizationStart(), asVesselExchangeModel.getAuthorizationEnd(), asVesselExchangeModel.getAuthorizationTermination());
	}
}