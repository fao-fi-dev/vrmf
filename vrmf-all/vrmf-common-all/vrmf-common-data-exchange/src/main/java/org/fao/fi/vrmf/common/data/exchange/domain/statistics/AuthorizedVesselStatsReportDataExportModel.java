/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.statistics;

import java.util.ArrayList;
import java.util.List;

import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportDateFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportIntegerFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportStringFieldModel;
import org.fao.fi.vrmf.common.models.stats.AuthorizedVesselStatsReport;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Nov 2011
 */
public class AuthorizedVesselStatsReportDataExportModel extends DataExportModel<AuthorizedVesselStatsReport> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2086235474003172067L;

	/**
	 * Class constructor
	 *
	 */
	public AuthorizedVesselStatsReportDataExportModel() {
		super();

		List<DataExportFieldModel<AuthorizedVesselStatsReport>> model = new ArrayList<DataExportFieldModel<AuthorizedVesselStatsReport>>();
		model.add(new DataExportStringFieldModel<AuthorizedVesselStatsReport>("countryName", "Party / Nation"));
		model.add(new DataExportStringFieldModel<AuthorizedVesselStatsReport>("countryIso2", "ISO 2"));
		model.add(new DataExportDateFieldModel<AuthorizedVesselStatsReport>("dateOfAcceptance", "Date Of Acceptance"));
		model.add(new DataExportDateFieldModel<AuthorizedVesselStatsReport>("lastUpdate", "Date Of Last Update"));
		model.add(new DataExportIntegerFieldModel<AuthorizedVesselStatsReport>("vessels", "Total Vessels"));
		model.add(new DataExportIntegerFieldModel<AuthorizedVesselStatsReport>("authorizedVessels", "Authorized Vessels"));
		model.add(new DataExportIntegerFieldModel<AuthorizedVesselStatsReport>("expiredVessels", "Expired Vessels"));
		
		this.setFieldsModel(model);
	}	
}
