/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.vessels.transformers;

import java.io.Serializable;
import java.net.URL;
import java.net.URLEncoder;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.common.data.exchange.core.transformers.IdentityDataModelTransformer;
import org.fao.fi.vrmf.common.models.generated.Vessels;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Nov 2011
 */
public class VesselNameToURLTransformer<VESSEL extends Vessels> extends IdentityDataModelTransformer<VESSEL> {
	private String _baseURL = null;
	private String _emptyNameLabel = "<NOT SET>";
	
	/**
	 * Class constructor
	 *
	 * @param baseURL
	 */
	public VesselNameToURLTransformer(String baseURL) {
		this._baseURL = baseURL;
	}
	
	/**
	 * Class constructor
	 *
	 * @param baseURL
	 * @param emptyNameLabel
	 */
	public VesselNameToURLTransformer(String baseURL, String emptyNameLabel) {
		this(baseURL);
		this._emptyNameLabel = emptyNameLabel;
	}
	
	public void setBaseURL(String baseURL) {
		this._baseURL = baseURL;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.models.export.DataModelTransformer#transform(java.lang.Object, java.io.Serializable)
	 */
	@Override
	public Serializable transform(Serializable value, VESSEL data) {
		String name = value == null ? null : StringsHelper.trim(value.toString());
		
		if(name == null)
			name = this._emptyNameLabel;
		
		try {
			return new URL(this._baseURL + "/" + data.getUid() + URLEncoder.encode("#", "UTF-8") + name);
		} catch (Throwable t) {
			return name;
		}
	}
}