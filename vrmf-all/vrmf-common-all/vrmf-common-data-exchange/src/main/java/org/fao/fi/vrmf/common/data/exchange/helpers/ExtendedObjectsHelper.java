/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.helpers;

import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
public class ExtendedObjectsHelper {
	static final private JexlEngine JEXL_ENGINE = new JexlEngine();
	static final private String JEXL_CONTEXT_PREFIX = "obj";
	static final private Map<String, Expression> EXPRESSIONS_CACHE = new HashMap<String, Expression>();

	static {
		ExtendedObjectsHelper.JEXL_ENGINE.setSilent(true);
	}

	/**
	 * Class constructor (made private in order to avoid class instantiation)
	 */
	private ExtendedObjectsHelper() {
		super();
	}

	/**
	 * @param expression
	 * @return
	 */
	static private Expression getExpression(String expression) {
		Expression evaluated = ExtendedObjectsHelper.EXPRESSIONS_CACHE.get(expression);
		if(evaluated == null) {
			evaluated = JEXL_ENGINE.createExpression(expression);

			ExtendedObjectsHelper.EXPRESSIONS_CACHE.put(expression, evaluated);
		}

		return evaluated;
	}
	
	/**
	 * @param object
	 * @param fieldName
	 * @param traverseClasses
	 * @return
	 */
	static public Object getFieldValue(Object object, String fieldName, boolean traverseClasses) {
		try {
			Expression x = JEXL_ENGINE.createExpression(JEXL_CONTEXT_PREFIX + "." + fieldName);

			JexlContext c = new MapContext();
			c.set(JEXL_CONTEXT_PREFIX, object);

			return x.evaluate(c);
		} catch (Throwable t) {
			t.printStackTrace();

			return null;
		}
	}

	/**
	 * @param object
	 * @param fieldAccessorExpression
	 * @param value
	 * @param traverseClasses
	 */
	static public void setFieldValue(Object object, String fieldAccessorExpression, Object value, boolean traverseClasses) {
		try {
			if(object == null)
				return;

			String valueName = JEXL_CONTEXT_PREFIX + "_" + fieldAccessorExpression;
			Expression x = ExtendedObjectsHelper.getExpression(JEXL_CONTEXT_PREFIX + "." + fieldAccessorExpression + " = " + valueName);

			JexlContext c = new MapContext();
			c.set(valueName, value);
			c.set(JEXL_CONTEXT_PREFIX, object);

			x.evaluate(c);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
	/**
	 * @param object
	 * @param traverseClasses
	 * @return
	 */
	static public Collection<Object> getFieldValues(final Object object, final boolean traverseClasses) {
		$nN(object, "Provided object cannot be null");

		return AccessController.doPrivileged(new PrivilegedAction<Collection<Object>>() {
			public Collection<Object> run() {
				Collection<Object> values = new ArrayList<Object>();
				Collection<Field> fields = ObjectsHelper.listFields(object, traverseClasses);

				for(Field field : fields) {
					try {
						values.add(ExtendedObjectsHelper.getFieldValue(object, field.getName(), traverseClasses));
					} catch (Throwable t) {
							;// Suffocate
					}
				}

				return values;
			}
		});
	}

	/**
	 * @param object
	 * @return
	 */
	static public Collection<Object> getAllFieldValues(Object object) {
		return ExtendedObjectsHelper.getFieldValues(object, true);
	}
}
