/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.emitters.impl;

import java.io.OutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Properties;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.model.core.spi.services.ProcessTracker;
import org.fao.fi.sh.utility.model.extensions.collections.SerializableList;
import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableArrayList;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.AbstractDatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.DatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Jan 2012
 */
public class JSONDatasetEmitter<DATA extends Exportable & Serializable> extends AbstractDatasetEmitter<DATA> {
	static final public String JSONP_CALLBACK_PROPERTY	   = "jsonpCallback";
	static final public String EXCLUSION_PATTERNS_PROPERTY = "exclusionPatterns";
	static final public String INCLUSION_PATTERNS_PROPERTY = "inclusionPatterns";
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.export.emitters.DatasetEmitter#emit(java.io.OutputStream, org.fao.vrmf.utilities.common.utils.process.ProcessTracker, org.fao.vrmf.utilities.common.models.export.DataExportModel, java.util.Collection, java.util.Properties)
	 */
	@Override
	public void emit(OutputStream out, ProcessTracker tracker, DataExportModel<? super DATA> model, Collection<DATA> data, Properties parameters) throws Throwable {
		String jsonpCallback = (String)this.getProperty(parameters, JSONP_CALLBACK_PROPERTY);
		String[] exclusionPatterns = (String[])this.getProperty(parameters, EXCLUSION_PATTERNS_PROPERTY);
		String[] inclusionPatterns = (String[])this.getProperty(parameters, INCLUSION_PATTERNS_PROPERTY);
		Long elapsed = (Long)this.getProperty(parameters, DatasetEmitter.ELAPSED_PROPERTY);
		
		JSONResponse<SerializableList<DATA>> response = new JSONResponse<SerializableList<DATA>>(new SerializableArrayList<DATA>(data));
		
		if(elapsed != null)
			response.setElapsed(elapsed);
		
		String json = response.JSONify(inclusionPatterns, exclusionPatterns);

		if(jsonpCallback != null) {
			json = jsonpCallback + "(" + json + ");";
		}
		
		out.write(json.getBytes("UTF-8"));
	}
	
	protected JSONResponse<Serializable> getDefaultResponseJSONObject() {
		JSONResponse<Serializable> result = new JSONResponse<Serializable>();
		
		result.setError(false);
		
		return result;
	}
	
	protected JSONResponse<Serializable> getDefaultResponseJSONObject(long elapsed) {
		JSONResponse<Serializable> result = this.getDefaultResponseJSONObject();
		
		result.setElapsed(elapsed);
		
		return result;
	}
	
	protected JSONResponse<Serializable> getDefaultResponseJSONObject(Serializable toReturn)  {
		JSONResponse<Serializable> result = this.getDefaultResponseJSONObject();
		
		result.setData(toReturn);
		
		return result;
	}
	
	protected JSONResponse<Serializable> getDefaultResponseJSONObject(Serializable toReturn, long elapsed) {
		JSONResponse<Serializable> result = this.getDefaultResponseJSONObject(toReturn);
		
		result.setElapsed(elapsed);
		
		return result;
	}
		
	protected JSONResponse<Serializable> getErrorResponseJSONObject() {
		JSONResponse<Serializable> result = new JSONResponse<Serializable>();
		
		result.setError(true);
		
		return result;
	}
}