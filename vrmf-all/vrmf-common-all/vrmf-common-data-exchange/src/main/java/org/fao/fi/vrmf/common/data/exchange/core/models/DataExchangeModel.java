/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.models;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.utility.core.helpers.singletons.io.MD5Helper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.common.data.exchange.core.annotations.DataExchangeFieldModelMetadata;
import org.fao.fi.vrmf.common.data.exchange.core.annotations.DataExchangeMappedFieldKeyToLabelMapping;
import org.fao.fi.vrmf.common.data.exchange.core.annotations.DataExchangeMappedFieldModelMetadata;
import org.fao.fi.vrmf.common.data.exchange.core.exceptions.FormatValidationException;
import org.fao.fi.vrmf.common.data.exchange.core.exceptions.ValidationException;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportBooleanFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportDateFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportDoubleFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportFloatFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportIntegerFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportStringFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.transformers.IdentityDataModelTransformer;
import org.fao.fi.vrmf.common.data.exchange.core.validators.NOPValidator;
import org.fao.fi.vrmf.common.data.exchange.helpers.ExtendedObjectsHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Nov 2011
 */
//@NonSerializedFields(fieldNames={"_checksum"})
abstract public class DataExchangeModel<DATA extends Exportable> extends DataExportModel<DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3163914920316631703L;
	
//	@DoNotSerialize	
//	@Skip
	transient private Map<DATA, DataExchangeModel<DATA>> _dataToModelMap = null;
	
	/**
	 * Class constructor
	 *
	 */
	public DataExchangeModel(Class<DATA> owner) {
		super();
		
		this.setFieldsModel(this.toExportModel(owner, null, null).getFieldsModel());
	}
	
	/**
	 * Class constructor
	 *
	 */
	public DataExchangeModel(Class<DATA> owner, Map<String, DataExportModelTransformer<DATA>> transformers, Map<String, DataExportModelValidator> validators) {
		super();

		this.setFieldsModel(this.toExportModel(owner, transformers, validators).getFieldsModel());
	}

	@DataExchangeFieldModelMetadata(
		label="Checksum",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_HIDDEN_MASK
	)
	private String _checksum;
	
	/**
	 * @return the 'checksum' value
	 */
	public String getChecksum() {
		return this._checksum;
	}

	/**
	 * @param checksum the 'checksum' value to set
	 */
	public void setChecksum(String checksum) {
		this._checksum = checksum;
	}	
	
	/**
	 * @return
	 */
	public String calculateChecksum(String content) {
		try {
			return MD5Helper.digest(content);
		} catch(Throwable t) {
			return null;
		}
	}
	
	/**
	 * @return
	 */
	public String calculateModelChecksum() {
		try {
			return MD5Helper.digest(this.toString());
		} catch(Throwable t) {
			return null;
		}
	}
	
	final private Map<DATA, DataExchangeModel<DATA>> lazyGetDataToModelMap() {
		if(this._dataToModelMap == null)
			this._dataToModelMap = new HashMap<DATA, DataExchangeModel<DATA>>();
		
		return this._dataToModelMap;
	}
	
	final public DataExchangeModel<DATA> populateWithData(DATA data) {
		if(!this.lazyGetDataToModelMap().containsKey(data)) {
			this.lazyGetDataToModelMap().put(data, this.doPopulateWithData(data));			
		}
		
		return this.lazyGetDataToModelMap().get(data);
	}
	
	abstract protected DataExchangeModel<DATA> doPopulateWithData(DATA data);
			
	final public DataExportModel<DATA> toExportModel(Class<DATA> owner, 
													 Map<String, DataExportModelTransformer<DATA>> transformers,
													 Map<String, DataExportModelValidator> validators) {
		List<DataExportFieldModel<DATA>> fieldModels = new ArrayList<DataExportFieldModel<DATA>>();
		
		List<DataExportFieldModel<DATA>> mappedFieldsModel = null;
		DataExportFieldModel<DATA> fieldModel = null;
		
		DataExchangeFieldModelMetadata meta = null;
		for(Field field : ObjectsHelper.listAllFields(this)) {
			fieldModel = this.buildDataExportFieldModel(owner, field, transformers, validators);
			
			if(fieldModel != null) {
				meta = field.getAnnotation(DataExchangeFieldModelMetadata.class);
				
				if(StringsHelper.trim(meta.beforeFieldWithLabel()) != null) {
					int position = this.findFieldModelByFieldLabel(fieldModels, meta.beforeFieldWithLabel());
					
					if(position >= 0)
						fieldModels.add(position, fieldModel);
					else
						fieldModels.add(fieldModel);
				} else
					fieldModels.add(fieldModel);
			} else {
				mappedFieldsModel = this.buildDataExportMappedFieldModel(owner, field, validators);
				
				if(mappedFieldsModel != null)
					fieldModels.addAll(mappedFieldsModel);
			}
		}
				
		return new DataExportModel<DATA>(fieldModels);
	}
	
	final int findFieldModelByFieldLabel(List<DataExportFieldModel<DATA>> fieldModels, String label) {		
		int counter = 0;
		
		for(DataExportFieldModel<DATA> fieldModel : fieldModels) {
			if(label.equals(fieldModel.getFieldLabel()))
				return counter;
			
			counter++;
		}
		
		return -1;
	}
	
	final private List<DataExportFieldModel<DATA>> buildDataExportMappedFieldModel(Class<DATA> owner, Field field, Map<String, DataExportModelValidator> validators) {
		List<DataExportFieldModel<DATA>> models = new ArrayList<DataExportFieldModel<DATA>>();
		
		if(field.isAnnotationPresent(DataExchangeMappedFieldModelMetadata.class)) {
			DataExchangeMappedFieldModelMetadata meta = field.getAnnotation(DataExchangeMappedFieldModelMetadata.class);
			
			Class<?> fieldClass = meta.mappedType();
			
			DataExportMappedFieldModel<DATA> model = null;
						
			for(DataExchangeMappedFieldKeyToLabelMapping mapping : meta.keyToLabelMappings()) {
				model = new DataExportMappedFieldModel<DATA>();
				model.setFieldExpression(field.getName().replaceFirst("\\_", ""));
				model.setFieldClass(fieldClass);
				model.setFieldLabel(mapping.label());
				model.setKey(mapping.key());
				model.setType(DataExportFieldModelType.fromTypeMask(mapping.typeMask()));
				
				if(!meta.validator().isAssignableFrom(NOPValidator.class) && validators != null) {
					try {
						model.setValidator(validators.get(meta.validator().getSimpleName()));
					} catch (Throwable t) {
						t.printStackTrace();
					}
				}
				
				models.add(model);
			}		
		}
		
		return models;
	}
	
	final private DataExportFieldModel<DATA> buildDataExportFieldModel(Class<DATA> owner, 
																	   Field field, 
																	   Map<String, DataExportModelTransformer<DATA>> transformers,
																	   Map<String, DataExportModelValidator> validators) {
		if(field.isAnnotationPresent(DataExchangeFieldModelMetadata.class)) {
			DataExchangeFieldModelMetadata meta = field.getAnnotation(DataExchangeFieldModelMetadata.class);
			
			DataExportFieldModel<DATA> model = null;
			
			Class<?> fieldClass = field.getClass();
			
			if(String.class.isAssignableFrom(fieldClass))
				model = new DataExportStringFieldModel<DATA>();
			else if(Integer.class.isAssignableFrom(fieldClass))
				model = new DataExportIntegerFieldModel<DATA>();
			else if(Float.class.isAssignableFrom(fieldClass))
				model = new DataExportFloatFieldModel<DATA>();
			else if(Double.class.isAssignableFrom(fieldClass))
				model = new DataExportDoubleFieldModel<DATA>();
			else if(Date.class.isAssignableFrom(fieldClass))
				model = new DataExportDateFieldModel<DATA>();
			else if(Boolean.class.isAssignableFrom(fieldClass))
				model = new DataExportBooleanFieldModel<DATA>();
			
			if(model == null)
				model = new DataExportFieldModel<DATA>();
			
			model.setOwner(owner);
			
			DataExportFieldModelType type = DataExportFieldModelType.fromTypeMask(meta.fieldModelTypeMask());
			if(type != null)
				model.setType(type);
			
			model.setFieldLabel(meta.label());
			model.setFieldExpression(field.getName().replaceFirst("\\_", ""));
			
			if(model.getClass().isAssignableFrom(DataExportFieldModel.class))
				model.setFieldClass(field.getType());
			
			if(!meta.transformer().isAssignableFrom(IdentityDataModelTransformer.class) && transformers != null) {
				try {
					model.setTransformer(transformers.get(meta.transformer().getSimpleName()));
				} catch (Throwable t) {
					t.printStackTrace();
				}
			}
			
			if(!meta.validator().isAssignableFrom(NOPValidator.class) && validators != null) {
				try {
					model.setValidator(validators.get(meta.validator().getSimpleName()));
				} catch (Throwable t) {
					t.printStackTrace();
				}
			}
			
			return model;			
		}
		
		return null;
	}
	
	final public void setField(String fieldName, Object value) throws Throwable {
		if(value == null || fieldName == null)
			return;
		
		ExtendedObjectsHelper.setFieldValue(this, fieldName, value, true);
	}
		
	@SuppressWarnings("unchecked")
	final public void setMapField(String mapFieldName, Object key, Object value) throws Throwable {
		Field mapField = ObjectsHelper.getField(this, mapFieldName, true);		
		mapField.setAccessible(true);
		
		@SuppressWarnings("rawtypes")
		Map map = (Map)mapField.get(this);
		
		if(map == null)
			throw new NullPointerException("Access to map-type field by expression '" + mapFieldName + "' on an instance of '" + this.getClass().getSimpleName() + "' yield a NULL value. Ensure the class has a Map attribute reacheable by the given expression and initialized to a non-null concrete Map");
		
		map.put(key, value);
	}
	
	final public Object getField(String fieldExpression) throws Throwable {
		if(fieldExpression == null)
			return null;
		
		DataExportFieldModel<DATA> fieldModel = this.getFieldModelByFieldExpression(fieldExpression);
		
		if(DataExportMappedFieldModel.class.isAssignableFrom(fieldModel.getClass())) {
			DataExportMappedFieldModel<DATA> asMappedModel = (DataExportMappedFieldModel<DATA>)fieldModel;
			
			return this.getMapField(asMappedModel.getFieldExpression(), asMappedModel.getKey());
		} else {		
			return ExtendedObjectsHelper.getFieldValue(this, fieldExpression, true);
		}
	}
		
	final private Object getMapField(String mapFieldName, Object key) throws Throwable {
		Field mapField = ObjectsHelper.getField(this, mapFieldName, true);		
		mapField.setAccessible(true);
		
		@SuppressWarnings("rawtypes")
		Map map = (Map)mapField.get(this);
		
		if(map == null)
			throw new NullPointerException("Access to map-type field by expression '" + mapFieldName + "' on an instance of '" + this.getClass().getSimpleName() + "' yield a NULL value. Ensure the class has a Map attribute reacheable by the given expression and initialized to a non-null concrete Map");
		
		return map.get(key);
	}
	
//	final public Object getField(String fieldLabel) throws Throwable {
//		DataExportFieldModel<DATA> fieldModel = this.getFieldModelByLabel(fieldLabel); 
//		
//		if(fieldModel != null) {
//			if(fieldModel instanceof DataExportMappedFieldModel)
//				return ObjectsHelper.getFieldValue(this, fieldModel.getFieldAccessorExpression(), true);
//			else
//				return ObjectsHelper.getFieldValue(this, fieldModel.getFieldExpression(), true);
//		}
//	}
	
	final public String getFieldLabel(String fieldName) throws Throwable {
		Field field = ObjectsHelper.getField(this, fieldName, true);
		
		if(field == null)
			return null;
		
		return field.isAnnotationPresent(DataExchangeFieldModelMetadata.class) ? field.getAnnotation(DataExchangeFieldModelMetadata.class).label() : null;
	}	
		
	final public void checkMandatoryData() throws ValidationException {		
		Object value;
		
		for(DataExportFieldModel<DATA> current : this.getFieldsModel()) {
			try {
				value = this.getField(current.getFieldReadAccessorExpression());
			} catch (Throwable t) {
				throw new FormatValidationException("Unable to access field " + current.getFieldLabel(), t);
			}

			if(value == null && current.isMandatory())
				throw new FormatValidationException("No value set for mandatory field '" + current.getFieldLabel() + "'");
		}
	}
	
	final public void validateData() throws ValidationException {		
		Object value;
		
		for(DataExportFieldModel<DATA> current : this.getFieldsModel()) {
			try {
				value = this.getField(current.getFieldReadAccessorExpression());
			} catch (Throwable t) {
				throw new FormatValidationException("Unable to access field " + current.getFieldLabel(), t);
			}

			if(current.hasValidator())
				if(!current.isOptional() || value != null)
					current.getValidator().validate((Serializable)value, current.getFieldLabel());
		}
	}
}