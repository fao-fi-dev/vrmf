/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.vessels;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportDateFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportDoubleFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportIntegerFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportStringFieldModel;
import org.fao.fi.vrmf.common.models.extended.BasicTimeframedAuthorizedVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Nov 2011
 */
public class BasicTimeframedVesselsSearchDataExportModel extends DataExportModel<BasicTimeframedAuthorizedVessel> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1776829281371661285L;

	/**
	 * Class constructor
	 */
	public BasicTimeframedVesselsSearchDataExportModel() {
		super();
		
		List<DataExportFieldModel<BasicTimeframedAuthorizedVessel>> model = new ArrayList<DataExportFieldModel<BasicTimeframedAuthorizedVessel>>();
		model.add(new DataExportIntegerFieldModel<BasicTimeframedAuthorizedVessel>("id", "ID"));
		model.add(new DataExportIntegerFieldModel<BasicTimeframedAuthorizedVessel>("uid", "UID"));
		model.add(new DataExportStringFieldModel<BasicTimeframedAuthorizedVessel>("imo", "IMO number"));
		model.add(new DataExportIntegerFieldModel<BasicTimeframedAuthorizedVessel>("currentFlagId", "Current flag"));
		model.add(new DataExportStringFieldModel<BasicTimeframedAuthorizedVessel>("currentName", "Name"));
		model.add(new DataExportStringFieldModel<BasicTimeframedAuthorizedVessel>("currentCallsign", "IRCS"));
		model.add(new DataExportIntegerFieldModel<BasicTimeframedAuthorizedVessel>("currentCallsignCountry", "IRCS country"));
		model.add(new DataExportStringFieldModel<BasicTimeframedAuthorizedVessel>("currentRegNo", "Registration number"));
		model.add(new DataExportStringFieldModel<BasicTimeframedAuthorizedVessel>("currentRegCountry", "Registration country"));
		model.add(new DataExportStringFieldModel<BasicTimeframedAuthorizedVessel>("currentRegPort", "Registration port"));
		model.add(new DataExportStringFieldModel<BasicTimeframedAuthorizedVessel>("currentLengthType", "Length type"));
		model.add(new DataExportDoubleFieldModel<BasicTimeframedAuthorizedVessel>("currentLengthValue", "Length value"));
		model.add(new DataExportStringFieldModel<BasicTimeframedAuthorizedVessel>("currentTonnageType", "Tonnage type"));
		model.add(new DataExportDoubleFieldModel<BasicTimeframedAuthorizedVessel>("currentTonnageValue", "Tonnage value"));
		model.add(new DataExportStringFieldModel<BasicTimeframedAuthorizedVessel>("sourceSystem", "Source system"));
		model.add(new DataExportDateFieldModel<BasicTimeframedAuthorizedVessel>("updateDate", "Update date"));
		model.add(new DataExportDoubleFieldModel<BasicTimeframedAuthorizedVessel>("maxTimeDelta", "Max. time delta"));
		model.add(new DataExportIntegerFieldModel<BasicTimeframedAuthorizedVessel>("numberOfSystems", "Number of systems"));
		model.add(new DataExportFieldModel<BasicTimeframedAuthorizedVessel>("sourceSystems", "Source systems", Array.class));
		
		this.setFieldsModel(model);
	}
}
