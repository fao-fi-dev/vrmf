/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.models;

import org.fao.fi.sh.model.core.spi.Exportable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Nov 2011
 */
public class DataExportMappedFieldModel<DATA extends Exportable> extends DataExportFieldModel<DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3059684874250587009L;

	private Object _key;

	/**
	 * Class constructor
	 *
	 */
	public DataExportMappedFieldModel() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param type
	 * @param mapFieldExpression
	 * @param fieldClass
	 */
	public DataExportMappedFieldModel(Class<DATA> owner, DataExportFieldModelType type, String mapFieldExpression, Object key, Class<?> fieldClass) {
		this(owner, type, mapFieldExpression, key, null, fieldClass, null);
	}

	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param type
	 * @param mapFieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportMappedFieldModel(Class<DATA> owner, DataExportFieldModelType type, String mapFieldExpression, Object key, String fieldLabel, Class<?> fieldClass, DataExportModelTransformer<DATA> transformer) {
		super(owner, type, mapFieldExpression, fieldLabel, fieldClass, transformer);
		
		this._key = key;
	}

	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param mapFieldExpression
	 * @param fieldClass
	 */
	public DataExportMappedFieldModel(Class<DATA> owner, String mapFieldExpression, Object key, Class<?> fieldClass) {
		this(owner, DataExportFieldModelType.MANDATORY, mapFieldExpression, key, null, fieldClass, null);
	}

	/**
	 * Class constructor
	 *
	 * @param type
	 * @param mapFieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportMappedFieldModel(DataExportFieldModelType type, String mapFieldExpression, Object key, String fieldLabel, Class<?> fieldClass) {
		this(null, type, mapFieldExpression, key, fieldLabel, fieldClass, null);
	}

	/**
	 * Class constructor
	 *
	 * @param mapFieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportMappedFieldModel(String mapFieldExpression, Object key, String fieldLabel, Class<?> fieldClass, DataExportModelTransformer<DATA> transformer) {
		this(null, DataExportFieldModelType.MANDATORY, mapFieldExpression, key, fieldLabel, fieldClass, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param mapFieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportMappedFieldModel(String mapFieldExpression, Object key, String fieldLabel, Class<?> fieldClass) {
		this(null, DataExportFieldModelType.MANDATORY, mapFieldExpression, key, fieldLabel, fieldClass, null);
	}

	/**
	 * @return the 'key' value
	 */
	final public Object getKey() {
		return this._key;
	}

	/**
	 * @param key the 'key' value to set
	 */
	final public void setKey(Object key) {
		this._key = key;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.models.export.GenericExportFieldModel#getFieldAccessorExpression()
	 */
	@Override
	public String getFieldReadAccessorExpression() {
		final StringBuilder fieldExpression = new StringBuilder(this.getFieldExpression());
		
		if(this._key == null)
			return fieldExpression.toString();
		
		Class<?> keyType = this._key.getClass();
				
		boolean isString = String.class.isAssignableFrom(keyType);
		boolean isNumber = Number.class.isAssignableFrom(keyType);
		
		if(!isString && !isNumber)
			return fieldExpression.toString();
		
		return fieldExpression.append(".get(").append(isString ? "\"" : "").append(this._key.toString()).append(isString ? "\"" : "").append(")").toString();
	}		
	
//	/* (non-Javadoc)
//	 * @see org.fao.vrmf.web.models.export.GenericExportFieldModel#getFieldAccessorExpression()
//	 */
//	@Override
//	public String getFieldWriteAccessorExpression() {
//		final StringBuilder fieldExpression = new StringBuilder(this.getFieldExpression());
//		
//		if(this._key == null)
//			return fieldExpression.toString();
//		
//		Class<?> keyType = this._key.getClass();
//				
//		boolean isString = String.class.isAssignableFrom(keyType);
//		boolean isNumber = Number.class.isAssignableFrom(keyType);
//		
//		if(!isString && !isNumber)
//			return fieldExpression.toString();
//		
//		return fieldExpression.append(".put(").append(isString ? "\"" : "").append(this._key.toString()).append(isString ? "\"" : "").append(", $value)").toString();
//	}
	
//	@SuppressWarnings("unchecked")
//	final public void setField(DATA source, Object value) throws Throwable {
//		if(source == null)
//			return;
//		
//		Field mapField = ObjectsHelper.getField(source, this.getFieldExpression(), true);		
//		mapField.setAccessible(true);
//		
//		@SuppressWarnings("rawtypes")
//		Map map = (Map)mapField.get(this);
//		
//		if(map == null)
//			throw new NullPointerException("Access to map-type field by expression '" + this.getFieldExpression() + "' on an instance of '" + source.getClass().getSimpleName() + "' yield a NULL value. Ensure the class has a Map attribute reacheable by the given expression and initialized to a non-null concrete Map");
//		
//		map.put(this._key, value);
//	}
}