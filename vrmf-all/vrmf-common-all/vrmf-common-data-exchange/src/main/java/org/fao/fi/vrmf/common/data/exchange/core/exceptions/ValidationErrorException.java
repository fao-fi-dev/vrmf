/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Jan 2012
 */
public class ValidationErrorException extends ValidationException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1063461859911988424L;

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public ValidationErrorException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public ValidationErrorException(String message) {
		super(message);
	}
}
