/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.statistics;

import java.util.ArrayList;
import java.util.List;

import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportDoubleFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportIntegerFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportStringFieldModel;
import org.fao.fi.vrmf.common.models.stats.RangedVesselStatsReport;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Nov 2011
 */
public class RangedVesselStatsReportDataExportModel extends DataExportModel<RangedVesselStatsReport> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7202064379381156710L;

	/**
	 * Class constructor
	 *
	 */
	public RangedVesselStatsReportDataExportModel() {
		super();
		
		List<DataExportFieldModel<RangedVesselStatsReport>> model = new ArrayList<DataExportFieldModel<RangedVesselStatsReport>>();
		model.add(new DataExportDoubleFieldModel<RangedVesselStatsReport>("from", "From"));
		model.add(new DataExportDoubleFieldModel<RangedVesselStatsReport>("to", "To"));
		model.add(new DataExportStringFieldModel<RangedVesselStatsReport>("description", "Range class"));
		model.add(new DataExportIntegerFieldModel<RangedVesselStatsReport>("vessels", "Vessels"));

		this.setFieldsModel(model);
	}	
}