/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.core.model.GenericData;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Nov 2011
 */
//@NonSerializedFields(fieldNames={"mapsTo", "mappingWeight", "mappingUser", "mappingDate", "mappingComment", "sourceSystem", "updateDate", "lastUpdateDate"})
public class DataExportModel<DATA extends Exportable> extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3664173211182242374L;
			
//	@DoNotSerialize
	transient private List<DataExportFieldModel<DATA>> _fieldsModel = new ListSet<DataExportFieldModel<DATA>>();
//	@DoNotSerialize
	transient private Map<String, DataExportFieldModel<DATA>> _fieldsModelByLabel = new HashMap<String, DataExportFieldModel<DATA>>();
//	@DoNotSerialize
	transient private Map<String, DataExportFieldModel<DATA>> _fieldsModelByFieldExpression = new HashMap<String, DataExportFieldModel<DATA>>();
	
	/**
	 * Class constructor
	 *
	 */
	public DataExportModel() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param fieldsModel
	 */
	public DataExportModel(List<DataExportFieldModel<DATA>> fieldsModel) {
		super();
		
		this._fieldsModel = $nEm(fieldsModel, "The fields model cannot be null or empty");
		
		this.updateMaps();
	}

	/**
	 * @return the 'fieldsModel' value
	 */
	public List<DataExportFieldModel<DATA>> getFieldsModel() {
		return this._fieldsModel;
	}

	/**
	 * @param fieldsModel the 'fieldsModel' value to set
	 */
	public void setFieldsModel(List<DataExportFieldModel<DATA>> fieldsModel) {
		this._fieldsModel = $nEm(fieldsModel, "The fields model cannot be null or empty");
		
		this.updateMaps();
	}
	
	public DataExportFieldModel<DATA> getFieldModelByLabel(String label) {
		return this._fieldsModelByLabel.get(label);
	}
	
	public DataExportFieldModel<DATA> getFieldModelByFieldExpression(String fieldExpression) {
		return this._fieldsModelByFieldExpression.get(fieldExpression);
	}
	
	final protected void add(DataExportFieldModel<DATA> toAdd, boolean updateMap) {
		this._fieldsModel.add(toAdd);
		
		if(updateMap)
			this.updateMaps();
	}
	
	final protected void fastAdd(DataExportFieldModel<DATA> toAdd) {
		this.add(toAdd, false);
	}
	
	final protected void add(DataExportFieldModel<DATA> toAdd) {
		this.add(toAdd, true);
	}
	
	final protected void updateMaps() {
		this._fieldsModelByLabel.clear();
		this._fieldsModelByFieldExpression.clear();
		
		for(DataExportFieldModel<DATA> fieldModel : this._fieldsModel) {
			if(fieldModel.mustMap()) {
				this._fieldsModelByLabel.put(fieldModel.getFieldLabel(), fieldModel);
				this._fieldsModelByFieldExpression.put(fieldModel.getFieldReadAccessorExpression(), fieldModel);
			}
		}
	}

	final public List<DataExportFieldModel<DATA>> getFieldsByType(DataExportFieldModelType[] ofTypes, DataExportFieldModelType[] notOfTypes) {
		List<DataExportFieldModel<DATA>> filteredFields = new ArrayList<DataExportFieldModel<DATA>>();
		
		boolean checkOfType = ofTypes != null && ofTypes.length > 0;
		boolean checkNotOfType = notOfTypes != null && notOfTypes.length > 0;
		
		boolean include;
		for(DataExportFieldModel<DATA> current : this.getFieldsModel()) {
			include = true;
			
			if(checkOfType) {
				for(DataExportFieldModelType toCheck : ofTypes)
					include &= current.getType().is(toCheck);
			}
			
			if(!include) 
				continue;
			
			if(checkNotOfType) {
				for(DataExportFieldModelType toCheck : notOfTypes)
					include &= current.getType().isNot(toCheck);
			}
			
			if(include) 
				filteredFields.add(current);
		}
		
		return filteredFields;
	}
	
	final public List<DataExportFieldModel<DATA>> getMandatoryFields() {
		return this.getFieldsByType(new DataExportFieldModelType[] { DataExportFieldModelType.MANDATORY }, null);
	}
	
	final public List<DataExportFieldModel<DATA>> getOptionalFields() {
		return this.getFieldsByType(new DataExportFieldModelType[] { DataExportFieldModelType.OPTIONAL }, null);
	}
	
	final public List<DataExportFieldModel<DATA>> getHiddenFields() {
		return this.getFieldsByType(new DataExportFieldModelType[] { DataExportFieldModelType.HIDDEN }, null);
	}

	
	final public boolean isModeled(String fieldLabel) {
		return this._fieldsModelByLabel.get(fieldLabel) != null;
	}
	
	final public boolean isMandatory(String fieldLabel) {
		return this.isModeled(fieldLabel) && 
			   this._fieldsModelByLabel.get(fieldLabel).getType().is(DataExportFieldModelType.MANDATORY);
	}	
}