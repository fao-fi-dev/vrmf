/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.countries.validators;

import java.io.Serializable;
import java.util.Set;

import org.fao.fi.vrmf.common.data.exchange.core.exceptions.ConstraintValidationException;
import org.fao.fi.vrmf.common.data.exchange.core.exceptions.ValidationException;
import org.fao.fi.vrmf.common.data.exchange.core.validators.CodeListValidator;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2011
 */
public class ISO2CountryCodeValidator extends CodeListValidator<String> {
	/**
	 * Class constructor
	 *
	 * @param iso2CountryCodes
	 */
	public ISO2CountryCodeValidator(Set<String> iso2CountryCodes) {
		super(iso2CountryCodes);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.data.exchange.core.validators.CodeListValidator#validate(java.io.Serializable, java.lang.String)
	 */
	@Override
	public void validate(Serializable value, String label) throws ValidationException {
		if(this._codes == null || value == null || !this._codes.contains(value.toString()))
		   	throw new ConstraintValidationException("Value '" + value + "' is not a valid ISO 2 country code for field '" + label + "'");
	}
}
