/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.countries.transformers;

import java.io.Serializable;
import java.util.Map;

import org.fao.fi.vrmf.common.data.exchange.core.models.DataExchangeModel;
import org.fao.fi.vrmf.common.data.exchange.core.transformers.IdentityDataModelTransformer;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.Vessels;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Nov 2011
 */
public class CountryIDToNameDataModelTransformer<VESSEL extends Vessels> extends IdentityDataModelTransformer<VESSEL>  {
	private Map<Integer, SCountries> _countriesByIDMap;
	
	@SuppressWarnings("unused")
	private Map<String, SCountries> _countriesByISO2Map;
	
	/**
	 * Class constructor
	 *
	 * @param countriesByIDMap
	 */
	public CountryIDToNameDataModelTransformer(Map<Integer, SCountries> countriesByIDMap, Map<String, SCountries> countriesByISO2Map) {
		super();
		
		this._countriesByIDMap = countriesByIDMap;
		this._countriesByISO2Map = countriesByISO2Map;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.models.export.DataModelTransformer#transform(java.lang.Object, java.io.Serializable)
	 */
	@Override
	public Serializable transform(Serializable value, VESSEL data) {
		if(value == null)
			return null;
		
		if(this._countriesByIDMap == null ||
		   !Integer.class.isAssignableFrom(value.getClass()))
			return value;
		
		SCountries country = this._countriesByIDMap.get((Integer)value);
		
		if(country == null)
			return null;
		
		return country.getName();
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.export.transformers.IdentityDataModelTransformer#reverseTransform(java.io.Serializable, org.fao.vrmf.utilities.common.models.export.DataExchangeModel)
	 */
	@Override
	public Serializable reverseTransform(Serializable value, DataExchangeModel<VESSEL> model) {
		return super.reverseTransform(value, model);
	}
}