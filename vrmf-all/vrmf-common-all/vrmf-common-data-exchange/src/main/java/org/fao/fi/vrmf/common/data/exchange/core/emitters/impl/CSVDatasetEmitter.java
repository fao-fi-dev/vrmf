/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.emitters.impl;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.model.core.spi.services.ProcessTracker;
import org.fao.fi.sh.utility.core.helpers.singletons.text.DateFormatHelper;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.AbstractDatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.DatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Jan 2012
 */
@Deprecated
public class CSVDatasetEmitter<DATA extends Exportable> extends AbstractDatasetEmitter<DATA> {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.export.emitters.DataSetEmitter#emit(java.io.OutputStream, org.fao.vrmf.utilities.common.utils.process.ProcessTracker, org.fao.vrmf.utilities.common.models.export.DataExportModel, java.util.Collection, java.util.Properties)
	 */
	@Override
	public void emit(OutputStream out, ProcessTracker tracker, DataExportModel<? super DATA> model, Collection<DATA> data, Properties parameters) throws Throwable {
		final boolean track = tracker != null;
		
		final String title = (String)this.getProperty(parameters, DatasetEmitter.TITLE_PROPERTY);

		final Long elapsed = (Long)this.getProperty(parameters, DatasetEmitter.ELAPSED_PROPERTY);

		final Integer datasetBaseSize = (Integer)this.getProperty(parameters, DatasetEmitter.DATASET_BASE_SIZE_PROPERTY);
		final Integer datasetSize = data == null ? 0 : data.size();
		
		final Boolean emitFooter = (Boolean)this.getProperty(parameters, DatasetEmitter.EMIT_FOOTER_PROPERTY);
		
		if(track) tracker.start("Building CSV file...");
		
		OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");
		
		this.addCSVHeader(writer, model);
		
		int row = 1;
		
		for(DATA report : data) {
			this._log.debug("Processing results row #" + row++);
			
			this.addCSVRowDetails(writer, model, report, datasetBaseSize, row++);
		}
		
		writer.write("\r\n");
		
		if(Boolean.TRUE.equals(emitFooter)) {
			if(track) tracker.setStatus("Building XLS footer...");
			
			this.addCSVFooter(writer, model, title);
			this.addCSVFooter(writer, model, "Creation time : ", DateFormatHelper.formatDateAndTime());
			this.addCSVFooter(writer, model, "Elapsed (mSec): ", elapsed);
			this.addCSVFooter(writer, model, "Current data size: ", datasetSize);
			this.addCSVFooter(writer, model, "Total data size: ", datasetBaseSize);
			
			if(track) tracker.setStatus("XLS footer built");
		}
		
		if(track) tracker.finish("Finished building CSV file: " + datasetSize + " rows processed");
		
		writer.flush();
		writer.close();
	}
	
	private void addCSVHeader(OutputStreamWriter writer, DataExportModel<? super DATA> dataModel) throws Throwable {
		StringBuilder header = new StringBuilder();
		
		for(DataExportFieldModel<? super DATA> field : dataModel.getFieldsModel()) {
			this.setCSVCellContent(header, field.getFieldLabel());
		}
		
		if(header.length() > 0)
			header = new StringBuilder(header.subSequence(0, header.length() - 1));
		
		header.append("\r\n");
		
		writer.write(header.toString());
	}
	
	private void addCSVFooter(OutputStreamWriter writer, DataExportModel<? super DATA> dataModel, Object... values) throws Throwable {
		//Footer row
		StringBuilder footer = new StringBuilder();
		
		for(Object value : values)
			this.setCSVCellContent(footer, value);
		
		int toPad = dataModel.getFieldsModel().size(); // + ( this.getCSVExtraHeaders() == null ? 0 : this.getCSVExtraHeaders().length) - values.length;
		
		if(toPad > 0)
			for(int p=0; p<toPad; p++)
				this.setCSVCellContent(footer, "");
		
		footer.append("\r\n");
		
		writer.write(footer.toString());
	}
	
	private void addCSVRowDetails(Writer writer, DataExportModel<? super DATA> dataModel, DATA report, Integer vesselBaseSize, int rowNumber) throws Throwable {
		StringBuilder result = new StringBuilder();
		
		Object value = null;
		
		for(DataExportFieldModel<? super DATA> field : dataModel.getFieldsModel()) {
			value = this.getFieldValue(dataModel, field, report);
			
			this.setCSVCellContent(result, value);
		}
		
//		this.addCSVExtraDetails(result, report, vesselBaseSize);
		
		if(result.length() > 0)
			result = new StringBuilder(result.subSequence(0, result.length() - 1));
		
		result.append("\n");
		
		writer.write(result.toString());
	}
	
//	abstract protected Object[] getCSVExtraDetails(DATA report, Integer vesselBaseSize);
	
//	protected void addCSVExtraDetails(StringBuilder row, DATA report, Integer vesselBaseSize) {
//		for(Object data : this.getCSVExtraDetails(report, vesselBaseSize))
//			this.setCSVCellContent(row, data);
//	}
	
	private StringBuilder setCSVCellContent(StringBuilder result, Object value) {	
		result.append("\"");
		
		StringBuilder currentResult = new StringBuilder();
		
		if(value != null) {
			if(Date.class.isAssignableFrom(value.getClass()))
				currentResult.append(this.getValue(DateFormatHelper.formatDate((Date)value)));
			else if(Integer.class.isAssignableFrom(value.getClass()))
				currentResult.append(this.getValue((Integer)value));
			else if(Number.class.isAssignableFrom(value.getClass()))
				currentResult.append(this.getValue((Number)value));
			else 
				currentResult.append(this.getValue(value.toString()));
		} else
			currentResult.append(this.getValue((String)null));
		
		currentResult.append("\"");
		
		result.append(currentResult.toString().replaceAll("\\,", "\\,")).append(",");
		
		return result;
	}
}