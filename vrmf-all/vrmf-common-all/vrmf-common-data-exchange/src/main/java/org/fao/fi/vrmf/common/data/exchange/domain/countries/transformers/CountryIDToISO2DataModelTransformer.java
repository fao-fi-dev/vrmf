/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.countries.transformers;

import java.io.Serializable;
import java.util.Map;

import org.fao.fi.vrmf.common.data.exchange.core.transformers.IdentityDataModelTransformer;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.Vessels;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Nov 2011
 */
public class CountryIDToISO2DataModelTransformer<VESSEL extends Vessels> extends IdentityDataModelTransformer<VESSEL> {
	private Map<Integer, SCountries> _countriesByIDMap;
	
	/**
	 * Class constructor
	 *
	 * @param countriesByIDMap
	 */
	public CountryIDToISO2DataModelTransformer(Map<Integer, SCountries> countriesByIDMap) {
		super();
		
		this._countriesByIDMap = countriesByIDMap;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.models.export.DataModelTransformer#transform(java.lang.Object, java.io.Serializable)
	 */
	@Override
	public Serializable transform(Serializable value, VESSEL data) {
		if(value == null)
			return null;
		
		if(this._countriesByIDMap == null ||
		   !Integer.class.isAssignableFrom(value.getClass()))
			return value;
		
		SCountries country = this._countriesByIDMap.get((Integer)value);
		
		if(country == null)
			return null;
		
		return country.getIso2Code();
	}
}