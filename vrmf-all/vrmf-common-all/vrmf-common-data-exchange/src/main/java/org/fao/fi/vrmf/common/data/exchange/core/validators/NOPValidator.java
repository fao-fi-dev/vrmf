/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.validators;

import java.io.Serializable;

import org.fao.fi.vrmf.common.data.exchange.core.exceptions.ValidationException;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModelValidator;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2011
 */
public class NOPValidator implements DataExportModelValidator {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.export.DataExportModelValidator#validate(java.io.Serializable, org.fao.vrmf.utilities.common.utils.models.ExportableData)
	 */
	@Override
	public void validate(Serializable value, String label) throws ValidationException {
		//NOP validator. It actually does nothing, and can be used in-place wherever a validator is needed although non necessary
	}
}
