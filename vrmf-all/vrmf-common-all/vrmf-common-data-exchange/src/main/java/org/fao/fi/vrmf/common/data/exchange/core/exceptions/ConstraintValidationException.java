/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2011
 */
public class ConstraintValidationException extends ValidationException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -9223000358565359075L;

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public ConstraintValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public ConstraintValidationException(String message) {
		super(message);
	}
}