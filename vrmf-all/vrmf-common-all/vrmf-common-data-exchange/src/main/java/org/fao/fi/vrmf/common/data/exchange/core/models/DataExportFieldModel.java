/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.models;

import java.lang.reflect.Field;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import org.fao.fi.vrmf.common.core.model.GenericData;
import org.fao.fi.vrmf.common.data.exchange.core.annotations.DataExchangeFieldModelMetadata;
import org.fao.fi.vrmf.common.data.exchange.core.transformers.IdentityDataModelTransformer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Oct 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Oct 2011
 */
public class DataExportFieldModel<DATA extends Exportable> extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7046215545917182419L;
		
	private DataExportFieldModelType _type = DataExportFieldModelType.MANDATORY;
	
	private Class<DATA> _owner;
	private String _fieldExpression;
	private String _fieldLabel;
	private Class<?> _fieldClass;
	private DataExportModelTransformer<DATA> _transformer;
	private DataExportModelValidator _validator;
	
	/**
	 * Class constructor
	 *
	 */
	public DataExportFieldModel() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 */
	public DataExportFieldModel(Class<?> fieldClass) {
		super();
		
		this._fieldClass = fieldClass;
	}
	
	/**
	 * Class constructor
	 *
	 * @param type
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportFieldModel(DataExportFieldModelType type, String fieldExpression, String fieldLabel, Class<?> fieldClass) {
		this(null, type, fieldExpression, fieldLabel, fieldClass, new IdentityDataModelTransformer<DATA>());
	}
			
	/**
	 * Class constructor
	 *
	 * @param type
	 * @param fieldExpression
	 * @param fieldClass
	 */
	public DataExportFieldModel(Class<DATA> owner, DataExportFieldModelType type, String fieldExpression, Class<?> fieldClass) {
		this(owner, type, fieldExpression, null, fieldClass, new IdentityDataModelTransformer<DATA>());
	}
	
	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportFieldModel(String fieldExpression, String fieldLabel, Class<?> fieldClass) {
		this(null, DataExportFieldModelType.MANDATORY, fieldExpression, fieldLabel, fieldClass, new IdentityDataModelTransformer<DATA>());
	}
	
	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldClass
	 */
	public DataExportFieldModel(Class<DATA> owner, String fieldExpression, Class<?> fieldClass) {
		this(owner, DataExportFieldModelType.MANDATORY, fieldExpression, null, fieldClass, new IdentityDataModelTransformer<DATA>());
	}
	
	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportFieldModel(DataExportFieldModelType type, String fieldLabel, Class<?> fieldClass, DataExportModelTransformer<DATA> transformer) {
		this(null, type, null, fieldLabel, fieldClass, transformer);
	}
	
	/**
	 * Class constructor
	 *
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportFieldModel(String fieldLabel, Class<?> fieldClass, DataExportModelTransformer<DATA> transformer) {
		this(null, DataExportFieldModelType.MANDATORY, null, fieldLabel, fieldClass, transformer);
	}
	
	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportFieldModel(String fieldExpression, String fieldLabel, Class<?> fieldClass, DataExportModelTransformer<DATA> transformer) {
		this(null, DataExportFieldModelType.MANDATORY, fieldExpression, fieldLabel, fieldClass, transformer);
	}
	
	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportFieldModel(Class<DATA> owner, DataExportFieldModelType type, String fieldExpression, String fieldLabel, Class<?> fieldClass, DataExportModelTransformer<DATA> transformer) {
		super();
		
		if(fieldExpression == null && transformer == null)
			throw new RuntimeException("Field name and transformer cannot be both null");
		
		if(owner == null && fieldLabel == null)
			throw new RuntimeException("Owner class and field label cannot be both null");
		
		this._owner = owner;		
		this._type = type;
		this._fieldExpression = fieldExpression;
		this._fieldLabel = fieldLabel;
		this._fieldClass = fieldClass;
		this._transformer = transformer;		
	}
	
	/**
	 * @return <code>true</code> if the field is mandatory
	 */
	final public boolean isMandatory() {
		return this._type.is(DataExportFieldModelType.MANDATORY);
	}
	
	/**
	 * @return <code>true</code> if the field is required
	 */
	final public boolean isRequired() {
		return this._type.is(DataExportFieldModelType.REQUIRED);
	}
	
	/**
	 * @return <code>true</code> if the field is optional
	 */
	final public boolean isOptional() {
		return !this.isMandatory();
	}

	/**
	 * @return <code>true</code> if the field is read-only
	 */
	final public boolean isReadOnly() {
		return this._type.is(DataExportFieldModelType.READ_ONLY);
	}

	/**
	 * @return <code>true</code> if the field is hidden
	 */
	final public boolean isHidden() {
		return this._type.is(DataExportFieldModelType.HIDDEN);
	}
	
	/**
	 * @return the 'type' value
	 */
	public DataExportFieldModelType getType() {
		return this._type;
	}

	/**
	 * @param type the 'type' value to set
	 */
	public void setType(DataExportFieldModelType type) {
		this._type = type;
	}

	/**
	 * @return the 'owner' value
	 */
	public Class<DATA> getOwner() {
		return this._owner;
	}

	/**
	 * @param owner the 'owner' value to set
	 */
	public void setOwner(Class<DATA> owner) {
		this._owner = owner;
	}

	/**
	 * @return the 'fieldExpression' value
	 */
	final public String getFieldExpression() {
		return this._fieldExpression;
	}

	/**
	 * @param fieldExpression the 'fieldExpression' value to set
	 */
	final public void setFieldExpression(String fieldExpression) {
		this._fieldExpression = fieldExpression;
	}
	
	/**
	 * @return
	 */
	public String getFieldReadAccessorExpression() {
		return this._fieldExpression;
	}
	
	/**
	 * @return
	 */
	public String getFieldWriteAccessorExpression() {
		return this.getFieldReadAccessorExpression();
	}

	/**
	 * @return the 'fieldLabel' value
	 */
	final public String getFieldLabel() {
		if(this._fieldLabel != null || 
		   this._fieldExpression == null || 
		   this._owner == null)
			return this._fieldLabel;
		
		Field target = ObjectsHelper.getField(this._owner, this._fieldExpression, true);
		
		if(target != null) {
			if(target.isAnnotationPresent(DataExchangeFieldModelMetadata.class))
				return target.getAnnotation(DataExchangeFieldModelMetadata.class).label();
		}
		
		return null;
	}

	/**
	 * @param fieldLabel the 'fieldLabel' value to set
	 */
	final public void setFieldLabel(String fieldLabel) {
		this._fieldLabel = fieldLabel;
	}

	/**
	 * @return the 'fieldClass' value
	 */
	final public Class<?> getFieldClass() {
		return this._fieldClass;
	}

	/**
	 * @param fieldClass the 'fieldClass' value to set
	 */
	final public void setFieldClass(Class<?> fieldClass) {
		this._fieldClass = fieldClass;
	}

	/**
	 * @return the 'transformer' value
	 */
	final public DataExportModelTransformer<DATA> getTransformer() {
		return this._transformer;
	}

	/**
	 * @param transformer the 'transformer' value to set
	 */
	final public void setTransformer(DataExportModelTransformer<DATA> transformer) {
		this._transformer = transformer;
	}
	
	/**
	 * @return the 'validator' value
	 */
	public DataExportModelValidator getValidator() {
		return this._validator;
	}

	/**
	 * @param validator the 'validator' value to set
	 */
	public void setValidator(DataExportModelValidator validator) {
		this._validator = validator;
	}
	
//	final public Object getValue(DATA source) {
//		if(source == null)
//			return null;
//
//		Object value = null;
//		
//		final String expression = this.getFieldReadAccessorExpression();
//		
//		if(expression != null) {
//			final String JEXL_CONTEXT_PREFIX = "JEXL_" + source.hashCode();
//			
//			try {
//				Expression x = JEXL_ENGINE.createExpression(JEXL_CONTEXT_PREFIX + "." + expression);
//		        
//				JexlContext c = new MapContext();
//				
//				if(DataExchangeModel.class.isAssignableFrom(source.getClass())) {
//					c.set(JEXL_CONTEXT_PREFIX, ((DataExchangeModel<DATA>)source).fromData(source));
//				} else {
//					c.set(JEXL_CONTEXT_PREFIX, source);	
//				}
//		        		        
//		        value = x.evaluate(c);
//			} catch (Throwable t) {
//				t.printStackTrace();
//				//this._log.warn(ErrorManagementHelper.appendCommonThrowableReportPart("Unable to evaluate expression '" + expression + "' on " + source, t), t);				
//			}
//		}
//        
//		if(this.hasTransformer())
//			return this.getTransformer().transform((Serializable)value, source);
//			
//		return value;
//	}
	
//	public void setValue(DATA source, Object value) {
//		if(source == null)
//			return;
//		
//		ObjectsHelper.setFieldValue(source, this.getFieldExpression(), value, true);
//	}
		
	final public boolean hasTransformer() {
		return this._transformer != null;
	}
	
	final public boolean hasValidator() {
		return this._validator != null;
	}	
	
	final public boolean hasOwner() {
		return this._owner != null;
	}
	
	final public boolean hasLabel() {
		return this.getFieldLabel() != null;
	}
	
	final public boolean hasFieldExpression() {
		return this._fieldExpression != null;
	}
	
	final public boolean mustMap() {
		return this.hasLabel() &&
			   this.hasFieldExpression();
	}	
}