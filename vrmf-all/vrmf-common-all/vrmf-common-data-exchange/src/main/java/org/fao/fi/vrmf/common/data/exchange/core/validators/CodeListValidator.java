/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.validators;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

import org.fao.fi.vrmf.common.data.exchange.core.exceptions.ConstraintValidationException;
import org.fao.fi.vrmf.common.data.exchange.core.exceptions.ValidationException;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModelValidator;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2011
 */
abstract public class CodeListValidator<CODE_TYPE extends Serializable> implements DataExportModelValidator {
	protected Set<CODE_TYPE> _codes = new TreeSet<CODE_TYPE>();
	
	/**
	 * Class constructor
	 *
	 * @param codes
	 */
	public CodeListValidator(Set<CODE_TYPE> codes) {
		super();
		
		this._codes = codes;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.export.DataExportModelValidator#validate(java.io.Serializable, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void validate(Serializable value, String label) throws ValidationException {
		if(value == null)
		   	throw new ConstraintValidationException("Code must be non-null for field '" + label + "'");
		
		CODE_TYPE code = null;
		
		try {
			code = (CODE_TYPE)value;
			
			if(!this._codes.contains(code))
				throw new ConstraintValidationException("Invalid code '" + code + "' for field '" + label + "'");
		} catch (ClassCastException CCe) {
			throw new ConstraintValidationException("Code '" + code + "' is of wrong type for field '" + label + "' (currently: " + value.getClass().getSimpleName() + ")");
		}
	}
}