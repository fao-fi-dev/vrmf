/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.vessels.transformers;

import java.io.Serializable;

import org.fao.fi.vrmf.common.data.exchange.core.models.DataExchangeModel;
import org.fao.fi.vrmf.common.data.exchange.domain.vessels.VesselDataExchangeModel;
import org.fao.fi.vrmf.common.models.extended.BasicTimeframedAuthorizedVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Nov 2011
 */
public class BasicVesselAuthorizationRangeToAuthorizationStatusTransformer<DATA extends BasicTimeframedAuthorizedVessel> extends AbstractVesselAuthorizationRangeToAuthorizationStatusTransformer<DATA> {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.models.export.DataModelTransformer#transform(java.lang.Object, java.io.Serializable)
	 */
	@Override
	public Serializable transform(Serializable value, DATA data) {
		return this.doCalculateAuthorizationStatus(data.getAuthFrom(), data.getAuthTo(), data.getAuthTerm());
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.export.DataExportModelTransformer#reverseTransform(java.io.Serializable, org.fao.vrmf.utilities.common.models.export.DataExchangeModel)
	 */
	@Override
	public Serializable reverseTransform(Serializable value, DataExchangeModel<DATA> model) {
		VesselDataExchangeModel<DATA> asVesselExchangeModel = (VesselDataExchangeModel<DATA>)model;
		
		return this.doCalculateAuthorizationStatus(asVesselExchangeModel.getAuthorizationStart(), asVesselExchangeModel.getAuthorizationEnd(), asVesselExchangeModel.getAuthorizationTermination());
	}
}