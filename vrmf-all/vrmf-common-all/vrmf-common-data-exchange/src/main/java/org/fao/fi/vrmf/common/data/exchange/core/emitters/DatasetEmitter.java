/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.emitters;

import java.io.OutputStream;
import java.util.Collection;
import java.util.Properties;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.model.core.spi.services.ProcessTracker;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Jan 2012
 */
public interface DatasetEmitter<DATA extends Exportable> {
	String DATE_OF_BUILD_PROPERTY 		= "dateOfBuild";
	String ELAPSED_PROPERTY 	  		= "elapsed";
	String TITLE_PROPERTY 				= "title";
	String TYPE_PROPERTY				= "type";
	String DATASET_BASE_SIZE_PROPERTY 	= "datasetBaseSize";
	String EMIT_FOOTER_PROPERTY 		= "emitFooter";
	
	String DEFAULT_NULL_LABEL = "----";
	
	Boolean ADD_PERCENTAGE_SIGN 	 = Boolean.TRUE;
	Boolean DONT_ADD_PERCENTAGE_SIGN = Boolean.FALSE;
	
	String EMPTY_MDB_RESOURCE = "org/fao/vrmf/web/resources/export/templates/mdb/empty.mdb";
	
	void emit(OutputStream out, ProcessTracker tracker, DataExportModel<? super DATA> model, Collection<DATA> data, Properties parameters) throws Throwable;
	
//	void manageError(OutputStream out, ProcessTracker tracker, DataExportModel<? super DATA> model, Collection<DATA> data, Throwable error, Properties parameters) throws Throwable;
}