/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.models.typed;

import java.util.Date;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModelType;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModelTransformer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Nov 2011
 */
public class DataExportDateFieldModel<DATA extends Exportable> extends DataExportFieldModel<DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3059684874250587009L;

	/**
	 * Class constructor
	 */
	public DataExportDateFieldModel() {
		super(Date.class);
	}
	
	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param type
	 * @param fieldExpression
	 * @param fieldClass
	 */
	public DataExportDateFieldModel(Class<DATA> owner, DataExportFieldModelType type, String fieldExpression) {
		super(owner, type, fieldExpression, Date.class);
	}

	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param type
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportDateFieldModel(Class<DATA> owner, DataExportFieldModelType type, String fieldExpression, String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(owner, type, fieldExpression, fieldLabel, Date.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param fieldExpression
	 * @param fieldClass
	 */
	public DataExportDateFieldModel(Class<DATA> owner, String fieldExpression) {
		super(owner, fieldExpression, Date.class);
	}

	/**
	 * Class constructor
	 *
	 * @param type
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportDateFieldModel(DataExportFieldModelType type, String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(type, fieldLabel, Date.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param type
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportDateFieldModel(DataExportFieldModelType type, String fieldExpression, String fieldLabel) {
		super(type, fieldExpression, fieldLabel, Date.class);
	}

	/**
	 * Class constructor
	 *
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportDateFieldModel(String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(fieldLabel, Date.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportDateFieldModel(String fieldExpression, String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(fieldExpression, fieldLabel, Date.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportDateFieldModel(String fieldExpression, String fieldLabel) {
		super(fieldExpression, fieldLabel, Date.class);
	}
}