/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.vessels.transformers;

import java.io.Serializable;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.common.data.exchange.core.transformers.IdentityDataModelTransformer;
import org.fao.fi.vrmf.common.models.generated.Vessels;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Nov 2011
 */
public class SpaceRemoverTransformer<VESSEL extends Vessels> extends IdentityDataModelTransformer<VESSEL> {
	/**
	 * Class constructor
	 */
	public SpaceRemoverTransformer() {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.models.export.DataModelTransformer#transform(java.lang.Object, java.io.Serializable)
	 */
	@Override
	public Serializable transform(Serializable value, VESSEL data) {
		return value == null ? null : StringsHelper.removeAllSpaces(value.toString());
	}
}