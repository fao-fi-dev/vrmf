/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Jan 2012
 */
public class ValidationWarningException extends ValidationException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1063461859911988424L;

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public ValidationWarningException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public ValidationWarningException(String message) {
		super(message);
	}
}
