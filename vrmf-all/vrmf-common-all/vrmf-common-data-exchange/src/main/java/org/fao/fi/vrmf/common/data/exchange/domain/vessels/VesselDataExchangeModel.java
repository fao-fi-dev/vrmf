/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.vessels;

import java.util.Date;
import java.util.Map;

import org.fao.fi.sh.utility.common.helpers.singletons.lang.BeansHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.extensions.maps.impl.SortedKeysHashMap;
import org.fao.fi.vrmf.common.data.exchange.core.annotations.DataExchangeFieldModelMetadata;
import org.fao.fi.vrmf.common.data.exchange.core.annotations.DataExchangeMappedFieldKeyToLabelMapping;
import org.fao.fi.vrmf.common.data.exchange.core.annotations.DataExchangeMappedFieldModelMetadata;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExchangeModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModelType;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModelTransformer;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModelValidator;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportDateFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportFloatFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportIntegerFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportStringFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.validators.PositiveValueValidator;
import org.fao.fi.vrmf.common.data.exchange.domain.countries.validators.ISO2CountryCodeValidator;
import org.fao.fi.vrmf.common.data.exchange.domain.vessels.transformers.CurrentVesselAuthorizationRangeToAuthorizationStatusTransformer;
import org.fao.fi.vrmf.common.data.exchange.domain.vessels.transformers.SpaceRemoverTransformer;
import org.fao.fi.vrmf.common.data.exchange.domain.vessels.transformers.VesselNameToURLTransformer;
import org.fao.fi.vrmf.common.data.exchange.domain.vessels.validators.typed.AuthorizationTerminationCodeListValidator;
import org.fao.fi.vrmf.common.data.exchange.domain.vessels.validators.typed.GearTypeCodeListValidator;
import org.fao.fi.vrmf.common.data.exchange.domain.vessels.validators.typed.PowerTypeCodeListValidator;
import org.fao.fi.vrmf.common.data.exchange.domain.vessels.validators.typed.VesselTypeCodeListValidator;
import org.fao.fi.vrmf.common.models.generated.Vessels;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Nov 2011
 */
abstract public class VesselDataExchangeModel<VESSEL extends Vessels> extends DataExchangeModel<VESSEL> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4468727718653071123L;
	
	@DataExchangeFieldModelMetadata(
		label="ID",
		fieldModel=DataExportIntegerFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_HIDDEN_MASK
	)
	
	//@Skip
	private Integer _id;

	@DataExchangeFieldModelMetadata(
		label="UID",
		fieldModel=DataExportIntegerFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_MASK
	)
	//@Skip
	private Integer _uid;

	@DataExchangeFieldModelMetadata(
		label="IMO number",
		fieldModel=DataExportStringFieldModel.class
	)
	private String _IMO;
	
	@DataExchangeFieldModelMetadata(
			label="EU CFR",
			fieldModel=DataExportStringFieldModel.class
		)
	private String _EU_CFR;
	
	@DataExchangeFieldModelMetadata(
		label="Reporting date",
		fieldModel=DataExportDateFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.MANDATORY_MASK
	)
	private Date _reportingDate;	
	
	@DataExchangeFieldModelMetadata(
		label="Vessel flag (ISO2)",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.MANDATORY_MASK,
		validator=ISO2CountryCodeValidator.class
	)
	private String _flag;
	
	@DataExchangeFieldModelMetadata(
		label="Flag reference date",
		fieldModel=DataExportDateFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_HIDDEN_MASK
	)
	//@Skip
	private Date _flagReferenceDate;
	
	@DataExchangeFieldModelMetadata(
		label="Previous vessel flag (ISO2)",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK,
		validator=ISO2CountryCodeValidator.class
	)
	private String _previousFlag;
	
	@DataExchangeFieldModelMetadata(
		label="Previous vessel flag reference date",
		fieldModel=DataExportDateFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK
	)
	private Date _previousFlagReferenceDate;
	
	@DataExchangeFieldModelMetadata(
		label="Name",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.MANDATORY_MASK,
		transformer=VesselNameToURLTransformer.class
	)
	private String _name;
	
	@DataExchangeFieldModelMetadata(
		label="Name reference date",
		fieldModel=DataExportDateFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_HIDDEN_MASK
	)
	//@Skip
	private Date _nameReferenceDate;
	
	@DataExchangeFieldModelMetadata(
		label="Previous vessel name",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK
	)
	private String _previousName;
	
	@DataExchangeFieldModelMetadata(
		label="Previous vessel name reference date",
		fieldModel=DataExportDateFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK
	)
	private Date _previousNameReferenceDate;
	
	@DataExchangeFieldModelMetadata(
		label="Registration number",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.MANDATORY_MASK
	)
	private String _registrationNumber;
	
	@DataExchangeFieldModelMetadata(
		label="Country of registry (ISO2)",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.MANDATORY_MASK,
		validator=ISO2CountryCodeValidator.class
	)
	private String _registrationCountry;
	
	@DataExchangeFieldModelMetadata(
		label="Port of registry (code)",
		fieldModel=DataExportIntegerFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_HIDDEN_MASK
	)
	private Integer _registrationPortCode;
	
	@DataExchangeFieldModelMetadata(
		label="Port of registry",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK		
	)
	private String _registrationPortName;
	
	@DataExchangeFieldModelMetadata(
		label="Callsign (IRCS)",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK,
		transformer=SpaceRemoverTransformer.class
	)
	private String _callsign;
	
	@DataExchangeFieldModelMetadata(
		label="Year of build",
		fieldModel=DataExportIntegerFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK,
		validator=PositiveValueValidator.class
	)
	private Integer _buildingYear;
	
	@DataExchangeFieldModelMetadata(
		label="Shipyard",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK	
	)
	private String _shipyard;

	@DataExchangeFieldModelMetadata(
		label="Country of build (ISO2)",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK,
		validator=ISO2CountryCodeValidator.class	
	)	
	private String _buildingCountry;
	
	@DataExchangeFieldModelMetadata(
		label="Port of build",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK	
	)	
	private String _buildingPort;
	
	@DataExchangeFieldModelMetadata(
		label="Vessel type (ISSCFV)",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.MANDATORY_MASK,
		validator=VesselTypeCodeListValidator.class
	)
	private String _vesselType;
	
	@DataExchangeFieldModelMetadata(
		label="Vessel type",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_OPTIONAL_MASK
	)
	//@Skip
	private String _vesselTypeName;
		
	@DataExchangeFieldModelMetadata(
		label="Fishing gear - primary (ISSCFG)",
		fieldModel=DataExportStringFieldModel.class
	)	
	private String _primaryGearType;
	
	@DataExchangeFieldModelMetadata(
		label="Fishing gear - primary",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_OPTIONAL_MASK
	)
	//@Skip
	private String _primaryGearTypeName;
	
	@DataExchangeFieldModelMetadata(
		label="Fishing gear - secondary (ISSCFG)",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_MASK,
		validator=GearTypeCodeListValidator.class	
	)	
	private String _secondaryGearType;

	@DataExchangeFieldModelMetadata(
		label="Fishing gear - secondary",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_OPTIONAL_MASK
	)
	//@Skip
	private String _secondaryGearTypeName;	
	
	@DataExchangeMappedFieldModelMetadata(
		keyType=String.class,
		mappedType=Float.class,
		label="Physical dimensions",
		keyToLabelMappings={ 
			@DataExchangeMappedFieldKeyToLabelMapping(key="LOA", label="Length overall (m)", typeMask=DataExportFieldModelType.REQUIRED_MASK),
			@DataExchangeMappedFieldKeyToLabelMapping(key="BP", label="Length between perpendiculars (m)"),
			@DataExchangeMappedFieldKeyToLabelMapping(key="REG", label="Registered length (m)"),
			@DataExchangeMappedFieldKeyToLabelMapping(key="MOD", label="Depth (m)"),
			@DataExchangeMappedFieldKeyToLabelMapping(key="DRA", label="Draught (m)"),
			@DataExchangeMappedFieldKeyToLabelMapping(key="BEA", label="Beam (m)"),
		},
		validator=PositiveValueValidator.class
	)
	private Map<String, Float> _lengths = new SortedKeysHashMap<String, Float>();
	
	@DataExchangeMappedFieldModelMetadata(
		keyType=String.class,
		mappedType=Float.class,
		label="Tonnages",
		keyToLabelMappings={ 
			@DataExchangeMappedFieldKeyToLabelMapping(key="GT", label="Gross tonnage (metric tons)"),
			@DataExchangeMappedFieldKeyToLabelMapping(key="GRT", label="Gross registered tonnage (metric tons)")
		},
		validator=PositiveValueValidator.class
	)
	private Map<String, Float> _tonnages = new SortedKeysHashMap<String, Float>();
		
	@DataExchangeFieldModelMetadata(
		label="Main engine power",
		fieldModel=DataExportFloatFieldModel.class,
		validator=PositiveValueValidator.class
	)	
	private Float _mainEnginePower;	
	
	@DataExchangeFieldModelMetadata(
		label="Power unit",
		fieldModel=DataExportStringFieldModel.class,
		validator=PowerTypeCodeListValidator.class
	)		
	private String _mainEngineUnit;

	@DataExchangeFieldModelMetadata(
		label="Auxiliary engine power",
		fieldModel=DataExportFloatFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_HIDDEN_MASK,
		validator=PositiveValueValidator.class
	)	
	private Float _auxiliaryEnginesPower;
	
	@DataExchangeFieldModelMetadata(
		label="Auxiliary engine unit",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_HIDDEN_MASK,
		validator=PowerTypeCodeListValidator.class
	)		
	private String _auxiliaryEnginesUnit;
	
	@DataExchangeFieldModelMetadata(
		label="Crew (number)",
		fieldModel=DataExportIntegerFieldModel.class,
		validator=PositiveValueValidator.class
	)	
	private Integer _crew;

	@DataExchangeFieldModelMetadata(
		label="Owner (name)",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK
	)	
	private String _ownerName;

	@DataExchangeFieldModelMetadata(
		label="Owner country (ISO2)",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK,
		validator=ISO2CountryCodeValidator.class
	)		
	private String _ownerCountry;
	
	@DataExchangeFieldModelMetadata(
		label="Owner city",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK
	)		
	private String _ownerCity;
	
	@DataExchangeFieldModelMetadata(
		label="Owner address",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK
	)
	private String _ownerAddress;
	
	@DataExchangeFieldModelMetadata(
		label="Owner ZIP code",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.REQUIRED_MASK
	)
	private String _ownerZipCode;
	
	@DataExchangeFieldModelMetadata(
		label="Owner phone number",
		fieldModel=DataExportStringFieldModel.class
	)
	private String _ownerPhoneNumber;

	@DataExchangeFieldModelMetadata(
		label="Owner mobile number",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_HIDDEN_MASK
	)	
	private String _ownerMobileNumber;
	
	@DataExchangeFieldModelMetadata(
		label="Owner fax number",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_HIDDEN_MASK
	)		
	private String _ownerFaxNumber;
	
	@DataExchangeFieldModelMetadata(
		label="Owner e-mail",
		fieldModel=DataExportStringFieldModel.class
	)
	private String _ownerEMail;
	
	@DataExchangeFieldModelMetadata(
		label="Owner website",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_HIDDEN_MASK
	)			
	private String _ownerWebsite;
	
	@DataExchangeFieldModelMetadata(
		label="Operator (name)",
		fieldModel=DataExportStringFieldModel.class
	)
	private String _operatorName;
	
	@DataExchangeFieldModelMetadata(
		label="Operator country (ISO2)",
		fieldModel=DataExportStringFieldModel.class,
		validator=ISO2CountryCodeValidator.class
	)	
	private String _operatorCountry;
	
	@DataExchangeFieldModelMetadata(
		label="Operator city",
		fieldModel=DataExportStringFieldModel.class
	)
	private String _operatorCity;
	
	@DataExchangeFieldModelMetadata(
		label="Operator address",
		fieldModel=DataExportStringFieldModel.class
	)
	private String _operatorAddress;
	
	@DataExchangeFieldModelMetadata(
		label="Operator ZIP code",
		fieldModel=DataExportStringFieldModel.class
	)
	private String _operatorZipCode;
	
	@DataExchangeFieldModelMetadata(
		label="Operator phone number",
		fieldModel=DataExportStringFieldModel.class
	)
	private String _operatorPhoneNumber;
	
	@DataExchangeFieldModelMetadata(
		label="Operator mobile number",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_HIDDEN_MASK
	)	
	private String _operatorMobileNumber;
	
	@DataExchangeFieldModelMetadata(
		label="Operator fax number",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_HIDDEN_MASK
	)		
	private String _operatorFaxNumber;
	
	@DataExchangeFieldModelMetadata(
		label="Operator e-mail",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_HIDDEN_MASK
	)		
	private String _operatorEMail;
	
	@DataExchangeFieldModelMetadata(
		label="Operator website",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_HIDDEN_MASK
	)			
	private String _operatorWebsite;
	
	@DataExchangeFieldModelMetadata(
		label="Reporting flag",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.MANDATORY_MASK,
		validator=ISO2CountryCodeValidator.class
	)
	private String _reportingFlag;
	
	@DataExchangeFieldModelMetadata(
		label="Authorization issuer ID",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_OPTIONAL_HIDDEN_MASK
	)		
	private String _authorizationIssuerId;
	
	@DataExchangeFieldModelMetadata(
		label="Authorization type ID",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_OPTIONAL_HIDDEN_MASK
	)		
	private String _authorizationTypeId;
	
	@DataExchangeFieldModelMetadata(
		label="Authorization ID",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_OPTIONAL_HIDDEN_MASK
	)
	//@Skip
	private String _authorizationId;
	
	@DataExchangeFieldModelMetadata(
		label="Authorization vessel ID",
		fieldModel=DataExportIntegerFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_OPTIONAL_HIDDEN_MASK
	)	
	//@Skip
	private Integer _authorizationVesselId;
	
	@DataExchangeFieldModelMetadata(
		label="Authorization status",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_OPTIONAL_MASK,
		transformer=CurrentVesselAuthorizationRangeToAuthorizationStatusTransformer.class
	)	
	private String _authorizationStatus;
	
	@DataExchangeFieldModelMetadata(
		label="Start of authorization",
		fieldModel=DataExportDateFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.MANDATORY_MASK
	)	
	private Date _authorizationStart;
	
	@DataExchangeFieldModelMetadata(
		label="End of authorization",
		fieldModel=DataExportDateFieldModel.class
	)	
	private Date _authorizationEnd;
	
	@DataExchangeFieldModelMetadata(
		label="Termination of authorization",
		fieldModel=DataExportDateFieldModel.class
	)		
	private Date _authorizationTermination;
	
	@DataExchangeFieldModelMetadata(
		label="Authorization termination code",
		fieldModel=DataExportIntegerFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_MASK,
		validator=AuthorizationTerminationCodeListValidator.class
	)	
	private Integer _authorizationTerminationCode;
	
	@DataExchangeFieldModelMetadata(
		label="Authorization termination reason",
		fieldModel=DataExportStringFieldModel.class,
		fieldModelTypeMask=DataExportFieldModelType.READ_ONLY_OPTIONAL_MASK
	)	
	//@Skip
	private String _authorizationTerminationReason;
	
	@DataExchangeFieldModelMetadata(
			label="Comment related to vessel authorization termination",
			fieldModel=DataExportStringFieldModel.class,
			fieldModelTypeMask=DataExportFieldModelType.OPTIONAL_MASK
	)	
	//@Skip
	private String _authorizationTerminationComment;
	
	@DataExchangeFieldModelMetadata(
		label="Comment",
		fieldModel=DataExportStringFieldModel.class
	)
	//@Skip
	private String _authorizationComment;
			
	/**
	 * Class constructor
	 *
	 * @param owner
	 */
	public VesselDataExchangeModel(Class<VESSEL> owner, 
								   Map<String, DataExportModelTransformer<VESSEL>> transformers,
								   Map<String, DataExportModelValidator> validators) {
		super(owner, transformers, validators);
	}
	
//	/**
//	 * Class constructor
//	 *
//	 * @param owner
//	 */
//	public VesselDataExchangeModel(Class<VESSEL> owner) {
//		super(owner, null);
//	}

	/**
	 * Class constructor
	 *
	 */
	public VesselDataExchangeModel(Class<VESSEL> owner, 
								   VesselDataExchangeModel<VESSEL> another, 
								   Map<String, DataExportModelTransformer<VESSEL>> transformers,
								   Map<String, DataExportModelValidator> validators) throws Throwable {
		this(owner, transformers, validators);
		
		BeansHelper.transferBean(another, this);
	}
	
//	/**
//	 * Class constructor
//	 *
//	 */
//	public VesselDataExchangeModel(Class<VESSEL> owner, VesselDataExchangeModel<VESSEL> another) throws Throwable {
//		super(owner, null);
//		
//		ObjectsHelper.transferBean(another, this);
//	}

	/**
	 * @return the 'id' value
	 */
	public Integer getId() {
		return this._id;
	}

	/**
	 * @param id the 'id' value to set
	 */
	public void setId(Integer id) {
		this._id = id;
	}

	/**
	 * @return the 'uid' value
	 */
	public Integer getUid() {
		return this._uid;
	}

	/**
	 * @param uid the 'uid' value to set
	 */
	public void setUid(Integer uid) {
		this._uid = uid;
	}

	/**
	 * @return the 'iMO' value
	 */
	public String getIMO() {
		return this._IMO;
	}

	/**
	 * @param iMO the 'iMO' value to set
	 */
	public void setIMO(String iMO) {
		this._IMO = iMO;
	}
	
	/**
	 * @return the 'eU_CFR' value
	 */
	public String getEU_CFR() {
		return this._EU_CFR;
	}

	/**
	 * @param eU_CFR the 'eU_CFR' value to set
	 */
	public void setEU_CFR(String eU_CFR) {
		this._EU_CFR = eU_CFR;
	}

	/**
	 * @return the 'flag' value
	 */
	public String getFlag() {
		return this._flag;
	}

	/**
	 * @param flag the 'flag' value to set
	 */
	public void setFlag(String flag) {
		this._flag = flag;
	}

	/**
	 * @return the 'flagReferenceDate' value
	 */
	public Date getFlagReferenceDate() {
		return this._flagReferenceDate;
	}

	/**
	 * @param flagReferenceDate the 'flagReferenceDate' value to set
	 */
	public void setFlagReferenceDate(Date flagReferenceDate) {
		this._flagReferenceDate = flagReferenceDate;
	}

	/**
	 * @return the 'previousFlag' value
	 */
	public String getPreviousFlag() {
		return this._previousFlag;
	}

	/**
	 * @param previousFlag the 'previousFlag' value to set
	 */
	public void setPreviousFlag(String previousFlag) {
		this._previousFlag = previousFlag;
	}

	/**
	 * @return the 'previousFlagReferenceDate' value
	 */
	public Date getPreviousFlagReferenceDate() {
		return this._previousFlagReferenceDate;
	}

	/**
	 * @param previousFlagReferenceDate the 'previousFlagReferenceDate' value to set
	 */
	public void setPreviousFlagReferenceDate(Date previousFlagReferenceDate) {
		this._previousFlagReferenceDate = previousFlagReferenceDate;
	}

	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return this._name;
	}

	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		this._name = name;
	}
	
	/**
	 * @return the 'nameReferenceDate' value
	 */
	public Date getNameReferenceDate() {
		return this._nameReferenceDate;
	}

	/**
	 * @param nameReferenceDate the 'nameReferenceDate' value to set
	 */
	public void setNameReferenceDate(Date nameReferenceDate) {
		this._nameReferenceDate = nameReferenceDate;
	}

	/**
	 * @return the 'previousName' value
	 */
	public String getPreviousName() {
		return this._previousName;
	}

	/**
	 * @param previousName the 'previousName' value to set
	 */
	public void setPreviousName(String previousName) {
		this._previousName = previousName;
	}

	/**
	 * @return the 'previousNameReferenceDate' value
	 */
	public Date getPreviousNameReferenceDate() {
		return this._previousNameReferenceDate;
	}

	/**
	 * @param previousNameReferenceDate the 'previousNameReferenceDate' value to set
	 */
	public void setPreviousNameReferenceDate(Date previousNameReferenceDate) {
		this._previousNameReferenceDate = previousNameReferenceDate;
	}

	/**
	 * @return the 'registrationNumber' value
	 */
	public String getRegistrationNumber() {
		return this._registrationNumber;
	}

	/**
	 * @param registrationNumber the 'registrationNumber' value to set
	 */
	public void setRegistrationNumber(String registrationNumber) {
		this._registrationNumber = registrationNumber;
	}

	/**
	 * @return the 'registrationCountry' value
	 */
	public String getRegistrationCountry() {
		return this._registrationCountry;
	}

	/**
	 * @param registrationCountry the 'registrationCountry' value to set
	 */
	public void setRegistrationCountry(String registrationCountry) {
		this._registrationCountry = registrationCountry;
	}

	/**
	 * @return the 'registrationPortCode' value
	 */
	public Integer getRegistrationPortCode() {
		return this._registrationPortCode;
	}

	/**
	 * @param registrationPortCode the 'registrationPortCode' value to set
	 */
	public void setRegistrationPortCode(Integer registrationPortCode) {
		this._registrationPortCode = registrationPortCode;
	}
	
	/**
	 * @return the 'registrationPortName' value
	 */
	public String getRegistrationPortName() {
		return this._registrationPortName;
	}

	/**
	 * @param registrationPortName the 'registrationPortName' value to set
	 */
	public void setRegistrationPortName(String registrationPortName) {
		this._registrationPortName = registrationPortName;
	}

	/**
	 * @return the 'callsign' value
	 */
	public String getCallsign() {
		return this._callsign;
	}

	/**
	 * @param callsign the 'callsign' value to set
	 */
	public void setCallsign(String callsign) {
//		this._callsign = callsign;
		this._callsign = StringsHelper.removeAllSpaces(callsign);
	}

	/**
	 * @return the 'buildingYear' value
	 */
	public Integer getBuildingYear() {
		return this._buildingYear;
	}

	/**
	 * @param buildingYear the 'buildingYear' value to set
	 */
	public void setBuildingYear(Integer buildingYear) {
		this._buildingYear = buildingYear;
	}

	/**
	 * @return the 'shipyard' value
	 */
	public String getShipyard() {
		return this._shipyard;
	}

	/**
	 * @param shipyard the 'shipyard' value to set
	 */
	public void setShipyard(String shipyard) {
		this._shipyard = shipyard;
	}

	/**
	 * @return the 'buildingCountry' value
	 */
	public String getBuildingCountry() {
		return this._buildingCountry;
	}

	/**
	 * @param buildingCountry the 'buildingCountry' value to set
	 */
	public void setBuildingCountry(String buildingCountry) {
		this._buildingCountry = buildingCountry;
	}

	/**
	 * @return the 'buildingPort' value
	 */
	public String getBuildingPort() {
		return this._buildingPort;
	}

	/**
	 * @param buildingPort the 'buildingPort' value to set
	 */
	public void setBuildingPort(String buildingPort) {
		this._buildingPort = buildingPort;
	}

	/**
	 * @return the 'vesselType' value
	 */
	public String getVesselType() {
		return this._vesselType;
	}

	/**
	 * @param vesselType the 'vesselType' value to set
	 */
	public void setVesselType(String vesselType) {
		this._vesselType = vesselType;
	}
	
	/**
	 * @return the 'vesselTypeName' value
	 */
	public String getVesselTypeName() {
		return this._vesselTypeName;
	}

	/**
	 * @param vesselTypeName the 'vesselTypeName' value to set
	 */
	public void setVesselTypeName(String vesselTypeName) {
		this._vesselTypeName = vesselTypeName;
	}

	/**
	 * @return the 'primaryGearType' value
	 */
	public String getPrimaryGearType() {
		return this._primaryGearType;
	}

	/**
	 * @param primaryGearType the 'primaryGearType' value to set
	 */
	public void setPrimaryGearType(String primaryGearType) {
		this._primaryGearType = primaryGearType;
	}
	
	/**
	 * @return the 'primaryGearTypeName' value
	 */
	public String getPrimaryGearTypeName() {
		return this._primaryGearTypeName;
	}

	/**
	 * @param primaryGearTypeName the 'primaryGearTypeName' value to set
	 */
	public void setPrimaryGearTypeName(String primaryGearTypeName) {
		this._primaryGearTypeName = primaryGearTypeName;
	}

	/**
	 * @return the 'secondaryGearType' value
	 */
	public String getSecondaryGearType() {
		return this._secondaryGearType;
	}

	/**
	 * @param secondaryGearType the 'secondaryGearType' value to set
	 */
	public void setSecondaryGearType(String secondaryGearType) {
		this._secondaryGearType = secondaryGearType;
	}
	
	/**
	 * @return the 'secondaryGearTypeName' value
	 */
	public String getSecondaryGearTypeName() {
		return this._secondaryGearTypeName;
	}

	/**
	 * @param secondaryGearTypeName the 'secondaryGearTypeName' value to set
	 */
	public void setSecondaryGearTypeName(String secondaryGearTypeName) {
		this._secondaryGearTypeName = secondaryGearTypeName;
	}

	/**
	 * @return the 'lengths' value
	 */
	public Map<String, Float> getLengths() {
		return this._lengths;
	}

	/**
	 * @param lengths the 'lengths' value to set
	 */
	public void setLengths(Map<String, Float> lengths) {
		this._lengths = lengths;
	}

	/**
	 * @return the 'tonnages' value
	 */
	public Map<String, Float> getTonnages() {
		return this._tonnages;
	}

	/**
	 * @param tonnages the 'tonnages' value to set
	 */
	public void setTonnages(Map<String, Float> tonnages) {
		this._tonnages = tonnages;
	}
	
	/**
	 * @return the 'mainEngineUnit' value
	 */
	public String getMainEngineUnit() {
		return this._mainEngineUnit;
	}

	/**
	 * @param mainEngineUnit the 'mainEngineUnit' value to set
	 */
	public void setMainEngineUnit(String mainEngineUnit) {
		this._mainEngineUnit = mainEngineUnit;
	}

	/**
	 * @return the 'mainEnginePower' value
	 */
	public Float getMainEnginePower() {
		return this._mainEnginePower;
	}

	/**
	 * @param mainEnginePower the 'mainEnginePower' value to set
	 */
	public void setMainEnginePower(Float mainEnginePower) {
		this._mainEnginePower = mainEnginePower;
	}

	/**
	 * @return the 'auxiliaryEnginesUnit' value
	 */
	public String getAuxiliaryEnginesUnit() {
		return this._auxiliaryEnginesUnit;
	}

	/**
	 * @param auxiliaryEnginesUnit the 'auxiliaryEnginesUnit' value to set
	 */
	public void setAuxiliaryEnginesUnit(String auxiliaryEnginesUnit) {
		this._auxiliaryEnginesUnit = auxiliaryEnginesUnit;
	}

	/**
	 * @return the 'auxiliaryEnginesPower' value
	 */
	public Float getAuxiliaryEnginesPower() {
		return this._auxiliaryEnginesPower;
	}

	/**
	 * @param auxiliaryEnginesPower the 'auxiliaryEnginesPower' value to set
	 */
	public void setAuxiliaryEnginesPower(Float auxiliaryEnginesPower) {
		this._auxiliaryEnginesPower = auxiliaryEnginesPower;
	}

	/**
	 * @return the 'crew' value
	 */
	public Integer getCrew() {
		return this._crew;
	}

	/**
	 * @param crew the 'crew' value to set
	 */
	public void setCrew(Integer crew) {
		this._crew = crew;
	}

	/**
	 * @return the 'ownerName' value
	 */
	public String getOwnerName() {
		return this._ownerName;
	}

	/**
	 * @param ownerName the 'ownerName' value to set
	 */
	public void setOwnerName(String ownerName) {
		this._ownerName = ownerName;
	}

	/**
	 * @return the 'ownerCountry' value
	 */
	public String getOwnerCountry() {
		return this._ownerCountry;
	}

	/**
	 * @param ownerCountry the 'ownerCountry' value to set
	 */
	public void setOwnerCountry(String ownerCountry) {
		this._ownerCountry = ownerCountry;
	}

	/**
	 * @return the 'ownerCity' value
	 */
	public String getOwnerCity() {
		return this._ownerCity;
	}

	/**
	 * @param ownerCity the 'ownerCity' value to set
	 */
	public void setOwnerCity(String ownerCity) {
		this._ownerCity = ownerCity;
	}

	/**
	 * @return the 'ownerAddress' value
	 */
	public String getOwnerAddress() {
		return this._ownerAddress;
	}

	/**
	 * @param ownerAddress the 'ownerAddress' value to set
	 */
	public void setOwnerAddress(String ownerAddress) {
		this._ownerAddress = ownerAddress;
	}

	/**
	 * @return the 'ownerZipCode' value
	 */
	public String getOwnerZipCode() {
		return this._ownerZipCode;
	}

	/**
	 * @param ownerZipCode the 'ownerZipCode' value to set
	 */
	public void setOwnerZipCode(String ownerZipCode) {
		this._ownerZipCode = ownerZipCode;
	}

	/**
	 * @return the 'ownerPhone' value
	 */
	public String getOwnerPhoneNumber() {
		return this._ownerPhoneNumber;
	}

	/**
	 * @param ownerPhone the 'ownerPhone' value to set
	 */
	public void setOwnerPhoneNumber(String ownerPhone) {
		this._ownerPhoneNumber = ownerPhone;
	}

	/**
	 * @return the 'ownerMobilePhone' value
	 */
	public String getOwnerMobileNumber() {
		return this._ownerMobileNumber;
	}

	/**
	 * @param ownerMobilePhone the 'ownerMobilePhone' value to set
	 */
	public void setOwnerMobileNumber(String ownerMobilePhone) {
		this._ownerMobileNumber = ownerMobilePhone;
	}

	/**
	 * @return the 'ownerFax' value
	 */
	public String getOwnerFaxNumber() {
		return this._ownerFaxNumber;
	}

	/**
	 * @param ownerFax the 'ownerFax' value to set
	 */
	public void setOwnerFaxNumber(String ownerFax) {
		this._ownerFaxNumber = ownerFax;
	}

	/**
	 * @return the 'ownerEMail' value
	 */
	public String getOwnerEMail() {
		return this._ownerEMail;
	}

	/**
	 * @param ownerEMail the 'ownerEMail' value to set
	 */
	public void setOwnerEMail(String ownerEMail) {
		this._ownerEMail = ownerEMail;
	}

	/**
	 * @return the 'ownerWebsite' value
	 */
	public String getOwnerWebsite() {
		return this._ownerWebsite;
	}

	/**
	 * @param ownerWebsite the 'ownerWebsite' value to set
	 */
	public void setOwnerWebsite(String ownerWebsite) {
		this._ownerWebsite = ownerWebsite;
	}

	/**
	 * @return the 'operatorName' value
	 */
	public String getOperatorName() {
		return this._operatorName;
	}

	/**
	 * @param operatorName the 'operatorName' value to set
	 */
	public void setOperatorName(String operatorName) {
		this._operatorName = operatorName;
	}

	/**
	 * @return the 'operatorCountry' value
	 */
	public String getOperatorCountry() {
		return this._operatorCountry;
	}

	/**
	 * @param operatorCountry the 'operatorCountry' value to set
	 */
	public void setOperatorCountry(String operatorCountry) {
		this._operatorCountry = operatorCountry;
	}

	/**
	 * @return the 'operatorCity' value
	 */
	public String getOperatorCity() {
		return this._operatorCity;
	}

	/**
	 * @param operatorCity the 'operatorCity' value to set
	 */
	public void setOperatorCity(String operatorCity) {
		this._operatorCity = operatorCity;
	}

	/**
	 * @return the 'operatorAddress' value
	 */
	public String getOperatorAddress() {
		return this._operatorAddress;
	}

	/**
	 * @param operatorAddress the 'operatorAddress' value to set
	 */
	public void setOperatorAddress(String operatorAddress) {
		this._operatorAddress = operatorAddress;
	}

	/**
	 * @return the 'operatorZipCode' value
	 */
	public String getOperatorZipCode() {
		return this._operatorZipCode;
	}

	/**
	 * @param operatorZipCode the 'operatorZipCode' value to set
	 */
	public void setOperatorZipCode(String operatorZipCode) {
		this._operatorZipCode = operatorZipCode;
	}

	/**
	 * @return the 'operatorPhone' value
	 */
	public String getOperatorPhoneNumber() {
		return this._operatorPhoneNumber;
	}

	/**
	 * @param operatorPhone the 'operatorPhone' value to set
	 */
	public void setOperatorPhoneNumber(String operatorPhone) {
		this._operatorPhoneNumber = operatorPhone;
	}

	/**
	 * @return the 'operatorMobilePhone' value
	 */
	public String getOperatorMobileNumber() {
		return this._operatorMobileNumber;
	}

	/**
	 * @param operatorMobilePhone the 'operatorMobilePhone' value to set
	 */
	public void setOperatorMobileNumber(String operatorMobilePhone) {
		this._operatorMobileNumber = operatorMobilePhone;
	}

	/**
	 * @return the 'operatorFax' value
	 */
	public String getOperatorFaxNumber() {
		return this._operatorFaxNumber;
	}

	/**
	 * @param operatorFax the 'operatorFax' value to set
	 */
	public void setOperatorFaxNumber(String operatorFax) {
		this._operatorFaxNumber = operatorFax;
	}

	/**
	 * @return the 'operatorEMail' value
	 */
	public String getOperatorEMail() {
		return this._operatorEMail;
	}

	/**
	 * @param operatorEMail the 'operatorEMail' value to set
	 */
	public void setOperatorEMail(String operatorEMail) {
		this._operatorEMail = operatorEMail;
	}

	/**
	 * @return the 'operatorWebsite' value
	 */
	public String getOperatorWebsite() {
		return this._operatorWebsite;
	}

	/**
	 * @param operatorWebsite the 'operatorWebsite' value to set
	 */
	public void setOperatorWebsite(String operatorWebsite) {
		this._operatorWebsite = operatorWebsite;
	}

	/**
	 * @return the 'reportingFlag' value
	 */
	public String getReportingFlag() {
		return this._reportingFlag;
	}

	/**
	 * @param reportingFlag the 'reportingFlag' value to set
	 */
	public void setReportingFlag(String reportingFlag) {
		this._reportingFlag = reportingFlag;
	}

	/**
	 * @return the 'authorizationIssuerId' value
	 */
	public String getAuthorizationIssuerId() {
		return this._authorizationIssuerId;
	}

	/**
	 * @param authorizationIssuerId the 'authorizationIssuerId' value to set
	 */
	public void setAuthorizationIssuerId(String authorizationIssuerId) {
		this._authorizationIssuerId = authorizationIssuerId;
	}

	/**
	 * @return the 'authorizationTypeId' value
	 */
	public String getAuthorizationTypeId() {
		return this._authorizationTypeId;
	}

	/**
	 * @param authorizationTypeId the 'authorizationTypeId' value to set
	 */
	public void setAuthorizationTypeId(String authorizationTypeId) {
		this._authorizationTypeId = authorizationTypeId;
	}
	
	/**
	 * @return the 'authorizationId' value
	 */
	public String getAuthorizationId() {
		return this._authorizationId;
	}

	/**
	 * @param authorizationId the 'authorizationId' value to set
	 */
	public void setAuthorizationId(String authorizationId) {
		this._authorizationId = authorizationId;
	}

	/**
	 * @return the 'authorizationVesselId' value
	 */
	public Integer getAuthorizationVesselId() {
		return this._authorizationVesselId;
	}

	/**
	 * @param authorizationVesselId the 'authorizationVesselId' value to set
	 */
	public void setAuthorizationVesselId(Integer authorizationVesselId) {
		this._authorizationVesselId = authorizationVesselId;
	}

	/**
	 * @return the 'authorizationStatus' value
	 */
	public String getAuthorizationStatus() {
		return this._authorizationStatus;
	}

	/**
	 * @param authorizationStatus the 'authorizationStatus' value to set
	 */
	public void setAuthorizationStatus(String authorizationStatus) {
		this._authorizationStatus = authorizationStatus;
	}

	/**
	 * @return the 'authorizationStart' value
	 */
	public Date getAuthorizationStart() {
		return this._authorizationStart;
	}

	/**
	 * @param authorizationStart the 'authorizationStart' value to set
	 */
	public void setAuthorizationStart(Date authorizationStart) {
		this._authorizationStart = authorizationStart;
	}

	/**
	 * @return the 'authorizationEnd' value
	 */
	public Date getAuthorizationEnd() {
		return this._authorizationEnd;
	}

	/**
	 * @param authorizationEnd the 'authorizationEnd' value to set
	 */
	public void setAuthorizationEnd(Date authorizationEnd) {
		this._authorizationEnd = authorizationEnd;
	}

	/**
	 * @return the 'authorizationTermination' value
	 */
	public Date getAuthorizationTermination() {
		return this._authorizationTermination;
	}

	/**
	 * @param authorizationTermination the 'authorizationTermination' value to set
	 */
	public void setAuthorizationTermination(Date authorizationTermination) {
		this._authorizationTermination = authorizationTermination;
	}

	/**
	 * @return the 'authorizationTerminationCode' value
	 */
	public Integer getAuthorizationTerminationCode() {
		return this._authorizationTerminationCode;
	}

	/**
	 * @param authorizationTerminationCode the 'authorizationTerminationCode' value to set
	 */
	public void setAuthorizationTerminationCode(Integer authorizationTerminationCode) {
		this._authorizationTerminationCode = authorizationTerminationCode;
	}

	/**
	 * @return the 'authorizationTerminationReason' value
	 */
	public String getAuthorizationTerminationReason() {
		return this._authorizationTerminationReason;
	}

	/**
	 * @param authorizationTerminationReason the 'authorizationTerminationReason' value to set
	 */
	public void setAuthorizationTerminationReason(String authorizationTerminationReason) {
		this._authorizationTerminationReason = authorizationTerminationReason;
	}
	
	/**
	 * @return the 'authorizationTerminationComment' value
	 */
	public String getAuthorizationTerminationComment() {
		return this._authorizationTerminationComment;
	}

	/**
	 * @param authorizationTerminationComment the 'authorizationTerminationComment' value to set
	 */
	public void setAuthorizationTerminationComment(String authorizationTerminationComment) {
		this._authorizationTerminationComment = authorizationTerminationComment;
	}

	/**
	 * @return the 'authorizationComment' value
	 */
	public String getAuthorizationComment() {
		return this._authorizationComment;
	}

	/**
	 * @param authorizationComment the 'authorizationComment' value to set
	 */
	public void setAuthorizationComment(String authorizationComment) {
		this._authorizationComment = authorizationComment;
	}

	/**
	 * @return the 'reportingDate' value
	 */
	public Date getReportingDate() {
		return this._reportingDate;
	}

	/**
	 * @param reportingDate the 'reportingDate' value to set
	 */
	public void setReportingDate(Date reportingDate) {
		this._reportingDate = reportingDate;
	}		
	
	abstract protected Map<String, Float> filterLengths(Map<String, Float> lengthsMap);
	abstract protected Map<String, Float> filterTonnages(Map<String, Float> tonnagesMap);
}