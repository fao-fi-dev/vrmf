/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.models;

import java.io.Serializable;

import org.fao.fi.sh.model.core.spi.Exportable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Nov 2011
 */
public interface DataExportModelTransformer<DATA extends Exportable> {
	Serializable transform(Serializable value, DATA data);
	Serializable reverseTransform(Serializable value, DataExchangeModel<DATA> model);
}
