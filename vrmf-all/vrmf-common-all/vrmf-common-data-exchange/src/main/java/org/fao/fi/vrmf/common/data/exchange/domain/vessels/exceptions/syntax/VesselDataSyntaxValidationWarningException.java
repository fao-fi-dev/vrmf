/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.vessels.exceptions.syntax;

import org.fao.fi.vrmf.common.data.exchange.domain.vessels.VesselDataExchangeModel;
import org.fao.fi.vrmf.common.data.exchange.domain.vessels.exceptions.VesselDataValidationWarningException;
import org.fao.fi.vrmf.common.models.extended.GenericCurrentVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Nov 2011
 */
public class VesselDataSyntaxValidationWarningException extends VesselDataValidationWarningException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2142536934945426948L;

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param existingVessel
	 * @param uploadedVessel
	 */
	public VesselDataSyntaxValidationWarningException(String message, GenericCurrentVessel<?> existingVessel, VesselDataExchangeModel<?> uploadedVessel) {
		super(message, existingVessel, uploadedVessel);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 * @param existingVessel
	 * @param uploadedVessel
	 */
	public VesselDataSyntaxValidationWarningException(String message, Throwable cause, GenericCurrentVessel<?> existingVessel, VesselDataExchangeModel<?> uploadedVessel) {
		super(message, cause, existingVessel, uploadedVessel);
	}
}