/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.emitters;

import java.io.Serializable;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.Properties;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExchangeModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- ----------------------- Date Author Comment ------------- --------------- ----------------------- 11 Jan 2012 Fiorellato Creation.
 *
 * @version 1.0
 * @since 11 Jan 2012
 */
abstract public class AbstractDatasetEmitter<DATA extends Exportable> extends AbstractLoggingAwareClient implements DatasetEmitter<DATA> {
	final protected JexlEngine JEXL_ENGINE = new JexlEngine();

	final protected String JEXL_CONTEXT_PREFIX = "row";

	/**
	 * Class constructor
	 *
	 */
	public AbstractDatasetEmitter() {
		super();

		this.JEXL_ENGINE.setSilent(true);
	}

	final public Object getProperty(Properties pool, String name) {
		return pool.get(name);
	}

	/**
	 * @param value
	 * @return
	 */
	final protected boolean isURL(Object value) {
		if(value == null) return false;

		if(URL.class.isAssignableFrom(value.getClass())) return true;

		if(String.class.isAssignableFrom(value.getClass())) {
			String asString = value.toString().toLowerCase();

			return asString.startsWith("http://") || asString.startsWith("https://") || asString.startsWith("ftp://") || asString.startsWith("ftps://");
		}

		return false;
	}

	/**
	 * @param value
	 * @return
	 */
	final protected boolean isPercentage(Object value) {
		if(value == null) return false;

		if(String.class.isAssignableFrom(value.getClass())) {
			String asString = StringsHelper.trim(value.toString());

			if(asString != null && asString.endsWith("%")) {
				asString = asString.replaceAll("\\%", "");

				try {
					Double.parseDouble(asString);

					return true;
				} catch(NumberFormatException NFe) {
					return false;
				}
			}
		}

		return false;
	}

	/**
	 * @param value
	 * @return
	 */
	final protected boolean isDecimal(Object value) {
		if(value == null) return false;

		return Double.class.isAssignableFrom(value.getClass()) || Float.class.isAssignableFrom(value.getClass());
	}

	/**
	 * @param url
	 * @return
	 */
	final protected NameValuePair getURLDetails(String url) {
		String encodedHash = "#";

		try {
			encodedHash = URLEncoder.encode("#", "UTF-8");
		} catch(Throwable t) {
			//
		}

		boolean hasAnchor = url.lastIndexOf(encodedHash) >= 0;

		String link = hasAnchor ? url.substring(0, url.lastIndexOf(encodedHash)) : url;
		String label = StringsHelper.trim(hasAnchor ? url.substring(url.lastIndexOf(encodedHash) + encodedHash.length()) : link);

		if(label != null) {
			try {
				label = URLDecoder.decode(label, "UTF-8");
			} catch(Throwable t) {
				this._log.warn("Unable to decode URL displayed value for '" + label + "'. Reverting to original representation...", t);
			}
		}

		return new NameValuePair(label, link);
	}

	/**
	 * @param url
	 * @return
	 */
	final protected NameValuePair getURLDetails(URL url) {
		return this.getURLDetails(url.toString());
	}

	final protected String getValue(String value) {
		if(value == null) return DatasetEmitter.DEFAULT_NULL_LABEL;

		return value;
	}

	final protected String getValue(Integer value) {
		if(value == null) return DatasetEmitter.DEFAULT_NULL_LABEL;

		return this.getValue(String.valueOf(value));
	}

	final protected String getValue(Number value) {
		if(value == null) return DatasetEmitter.DEFAULT_NULL_LABEL;

		return this.getValue(String.valueOf(value));
	}

	@SuppressWarnings("unchecked")
	final protected Object getFieldValue(DataExportModel<? super DATA> model, DataExportFieldModel<? super DATA> fieldModel, DATA source) throws Throwable {
		Object value = null;
		final String expression = fieldModel.getFieldExpression();

		if(expression != null) {
			try {
				Expression x = JEXL_ENGINE.createExpression(JEXL_CONTEXT_PREFIX + "." + expression);

				JexlContext c = new MapContext();

				if(DataExchangeModel.class.isAssignableFrom(model.getClass())) {
					c.set(JEXL_CONTEXT_PREFIX, ( (DataExchangeModel<DATA>) model ).populateWithData(source));
				} else {
					c.set(JEXL_CONTEXT_PREFIX, source);
				}

				value = x.evaluate(c);
			} catch(Throwable t) {
				this._log.warn("Unable to evaluate expression '{}' on {}", expression, source, t);
			}
		}

		if(fieldModel.hasTransformer()) return fieldModel.getTransformer().transform((Serializable) value, source);
		// return model.getTransformer().transform(model.getFieldName() == null ? null : (Serializable)ObjectsHelper.getFieldValue(source, model.getFieldName(), true), source);
		// else
		// return ObjectsHelper.getFieldValue(source, model.getFieldName(), true);

		return value;
	}

	final protected String formatPercentage(double fraction, int digits, boolean addPercentageSign) {
		String pattern = "{0,number,##0.";

		for(int d = 0; d < digits; d++)
			pattern += "0";

		pattern += "%}";

		String result = MessageFormat.format(pattern, fraction);

		if(!addPercentageSign) return result.replaceAll("\\%", "");

		return result;
	}

	final protected String formatPercentage(Integer value, Integer maximum, int digits, boolean addPercentageSign) {
		return value == null || maximum == null ? null : this.formatPercentage(this.percentage(value, maximum, digits) * 0.01D, digits, addPercentageSign);
	}

	final protected String formatPercentage(double fraction, int digits) {
		return this.formatPercentage(fraction, digits, DatasetEmitter.ADD_PERCENTAGE_SIGN);
	}

	final protected String formatPercentage(Integer value, Integer maximum, int digits) {
		return this.formatPercentage(value, maximum, digits, DatasetEmitter.ADD_PERCENTAGE_SIGN);
	}

	final protected Double percentage(Integer value, Integer maximum, int digits) {
		return value == null || maximum == null ? null : Math.round(100D * value * Math.pow(10, digits) / maximum) * Math.pow(10, -digits);
	}
}