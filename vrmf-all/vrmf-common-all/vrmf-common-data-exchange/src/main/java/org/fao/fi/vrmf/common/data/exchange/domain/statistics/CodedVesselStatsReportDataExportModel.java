/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.domain.statistics;

import java.util.ArrayList;
import java.util.List;

import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportIntegerFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportStringFieldModel;
import org.fao.fi.vrmf.common.models.stats.CodedVesselStatsReport;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Nov 2011
 */
public class CodedVesselStatsReportDataExportModel extends DataExportModel<CodedVesselStatsReport> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3177541835998211122L;

	/**
	 * Class constructor
	 *
	 * @param fieldsModel
	 */
	public CodedVesselStatsReportDataExportModel() {
		super();
		
		List<DataExportFieldModel<CodedVesselStatsReport>> model = new ArrayList<DataExportFieldModel<CodedVesselStatsReport>>();
		model.add(new DataExportStringFieldModel<CodedVesselStatsReport>("code", "Code"));
		model.add(new DataExportStringFieldModel<CodedVesselStatsReport>("abbreviation", "Abbreviation"));
		model.add(new DataExportStringFieldModel<CodedVesselStatsReport>("name", "Name"));
		model.add(new DataExportIntegerFieldModel<CodedVesselStatsReport>("vessels", "Vessels"));
		
		this.setFieldsModel(model);
	}	
}
