/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.models.typed;

import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModelType;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModelTransformer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Nov 2011
 */
public class DataExportBooleanFieldModel<DATA extends Exportable> extends DataExportFieldModel<DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3059684874250587009L;

	/**
	 * Class constructor
	 */
	public DataExportBooleanFieldModel() {
		super(Boolean.class);
	}
	
	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param type
	 * @param fieldExpression
	 * @param fieldClass
	 */
	public DataExportBooleanFieldModel(Class<DATA> owner, DataExportFieldModelType type, String fieldExpression) {
		super(owner, type, fieldExpression, Boolean.class);
	}

	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param type
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportBooleanFieldModel(Class<DATA> owner, DataExportFieldModelType type, String fieldExpression, String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(owner, type, fieldExpression, fieldLabel, Boolean.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param owner
	 * @param fieldExpression
	 * @param fieldClass
	 */
	public DataExportBooleanFieldModel(Class<DATA> owner, String fieldExpression) {
		super(owner, fieldExpression, Boolean.class);
	}

	/**
	 * Class constructor
	 *
	 * @param type
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportBooleanFieldModel(DataExportFieldModelType type, String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(type, fieldLabel, Boolean.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param type
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportBooleanFieldModel(DataExportFieldModelType type, String fieldExpression, String fieldLabel) {
		super(type, fieldExpression, fieldLabel, Boolean.class);
	}

	/**
	 * Class constructor
	 *
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportBooleanFieldModel(String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(fieldLabel, Boolean.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 * @param transformer
	 */
	public DataExportBooleanFieldModel(String fieldExpression, String fieldLabel, DataExportModelTransformer<DATA> transformer) {
		super(fieldExpression, fieldLabel, Boolean.class, transformer);
	}

	/**
	 * Class constructor
	 *
	 * @param fieldExpression
	 * @param fieldLabel
	 * @param fieldClass
	 */
	public DataExportBooleanFieldModel(String fieldExpression, String fieldLabel) {
		super(fieldExpression, fieldLabel, Boolean.class);
	}
}