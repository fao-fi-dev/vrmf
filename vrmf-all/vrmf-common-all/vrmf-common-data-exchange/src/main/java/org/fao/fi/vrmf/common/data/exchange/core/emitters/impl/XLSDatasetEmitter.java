/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-data-exchange)
 */
package org.fao.fi.vrmf.common.data.exchange.core.emitters.impl;

import java.io.OutputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.model.core.spi.services.ProcessTracker;
import org.fao.fi.sh.utility.core.helpers.singletons.text.DateFormatHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.AbstractDatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.DatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModelType;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Jan 2012
 */
@Deprecated
public class XLSDatasetEmitter<DATA extends Exportable> extends AbstractDatasetEmitter<DATA> {
	static final protected String XLS_HEADER_CELL_STYLE_ID 	   		= "header";
	static final protected String XLS_HEADER_OPTIONAL_CELL_STYLE_ID = "headerOptional";
	static final protected String XLS_READ_ONLY_CELL_STYLE_ID 		= "readOnly";
	static final protected String XLS_HYPERLINK_CELL_STYLE_ID  		= "hyperlink";
	static final protected String XLS_NULL_HYPERLINK_CELL_STYLE_ID 	= "nullHyperlink";
	static final protected String XLS_DECIMAL_CELL_STYLE_ID 		= "decimal";
	static final protected String XLS_PERCENTAGE_CELL_STYLE_ID 		= "percentage";
	static final protected String XLS_RIGHT_ALIGN_CELL_STYLE_ID		= "rightAlign";
	static final protected String XLS_BOLD_CELL_STYLE_ID			= "bold";
	static final protected String XLS_MISSING_MANDATORY_STYLE_ID	= "missingMandatory";
	static final protected String XLS_NULL_STYLE_ID 		   		= "null";
	static final protected String XLS_DEFAULT_STYLE_ID 		   		= "default";

	static final protected boolean XLS_FORCE_NUMERIC_CELL = true;
	static final protected boolean XLS_GUESS_NUMERIC_CELL = false;

	static final protected boolean XLS_SKIP_ROW 	   = true;
	static final protected boolean XLS_CONTIGUOUS_ROWS = false;
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.models.export.emitters.DataEmitter#emit(java.io.OutputStream, org.fao.vrmf.utilities.common.models.behaviours.ProcessTracker, org.fao.vrmf.utilities.common.models.export.DataExportModel, java.util.Collection, java.util.Properties)
	 */
	@Override
	public void emit(OutputStream out, ProcessTracker tracker, DataExportModel<? super DATA> dataModel, Collection<DATA> data, Properties parameters) throws Throwable {
		final boolean track = tracker != null;
		
		final String title = (String)this.getProperty(parameters, DatasetEmitter.TITLE_PROPERTY);

		final Long elapsed = (Long)this.getProperty(parameters, DatasetEmitter.ELAPSED_PROPERTY);

		final Integer datasetBaseSize = (Integer)this.getProperty(parameters, DatasetEmitter.DATASET_BASE_SIZE_PROPERTY);
		final Integer datasetSize = data == null ? 0 : data.size();
		
		final Boolean emitFooter = (Boolean)this.getProperty(parameters, DatasetEmitter.EMIT_FOOTER_PROPERTY);
		
		if(track) tracker.start("Building XLS file...");
		
		Workbook workbook = new HSSFWorkbook();
		Sheet sheet = workbook.createSheet(title);
		
		sheet.createFreezePane( 0, 1, 0, 1 );
		
		Map<String, CellStyle> cellStyles = this.createFontMap(workbook);
		
//		EXPORT_MODEL dataModel = this.getExportDataModel(servletRequest, serviceRequest, XLS_EXPORT);
		
		this.addXLSHeader(sheet, cellStyles, dataModel);
		
		int row = 1;
		int counter = 1;
		
		for(DATA current : data) {
			this._log.debug("Processing results row #" + row);
						
			this.addXLSRowDetails(sheet, cellStyles, dataModel, current, datasetBaseSize, row++);
			
			if(track) tracker.setCompletionStatus(this.percentage(counter++, datasetSize, 2));
		}

		if(Boolean.TRUE.equals(emitFooter)) {
			if(track) tracker.setStatus("Building XLS footer...");
			
			this.addXLSFooter(sheet, cellStyles.get(XLS_BOLD_CELL_STYLE_ID), XLS_SKIP_ROW, title);
			this.addXLSFooter(sheet, cellStyles.get(XLS_BOLD_CELL_STYLE_ID), XLS_CONTIGUOUS_ROWS, "Creation time : ", DateFormatHelper.formatDateAndTime());
			this.addXLSFooter(sheet, cellStyles.get(XLS_BOLD_CELL_STYLE_ID), XLS_CONTIGUOUS_ROWS, "Elapsed (mSec): ", elapsed);
			this.addXLSFooter(sheet, cellStyles.get(XLS_BOLD_CELL_STYLE_ID), XLS_CONTIGUOUS_ROWS, "Current data size: ", datasetSize);
			this.addXLSFooter(sheet, cellStyles.get(XLS_BOLD_CELL_STYLE_ID), XLS_CONTIGUOUS_ROWS, "Total data size: ", datasetBaseSize);
			
			if(track) tracker.setStatus("XLS footer built");
		}
		
		int from = sheet.getRow(0).getFirstCellNum();
		int to = sheet.getRow(0).getLastCellNum();
		
		int dataModelColumns = dataModel.getFieldsModel().size();
		
		for(int col=from; col<to; col++) {
			if(col >= dataModelColumns || !dataModel.getFieldsModel().get(col).isHidden())
				sheet.autoSizeColumn(col);
			else
				sheet.setColumnHidden(col, true);
		}
		
		if(track) tracker.finish("Finished building XLS file: " + datasetSize + " rows processed");
		
		workbook.write(out);
	}
		
	private void addXLSHeader(Sheet sheet, Map<String, CellStyle> cellStyles, DataExportModel<? super DATA> dataModel) throws Throwable {
		Row header = sheet.createRow(0); 
		int column = 0;
				
		for(DataExportFieldModel<? super DATA> field : dataModel.getFieldsModel()) {
			this.createXLSCells(header, column++, cellStyles.get(field.isMandatory() ? XLS_HEADER_CELL_STYLE_ID : XLS_HEADER_OPTIONAL_CELL_STYLE_ID), field.getFieldLabel());
		}
		
//		this.addXLSExtraHeaders(header, column++, cellStyles, getXLSExtraHeaders());
	}
	
//	abstract protected String[] getXLSExtraHeaders();
	
	protected void addXLSExtraHeaders(Row header, int startingColumn, Map<String, CellStyle> cellStyles, String[] headers) {
		if(headers != null)
			for(String column : headers)
				this.createXLSCells(header, startingColumn++, cellStyles.get(XLS_HEADER_CELL_STYLE_ID), XLS_GUESS_NUMERIC_CELL, column);
	}
	
	private Cell addXLSFooter(Sheet sheet, CellStyle footerCellStyle, boolean skipRow, Object... values) throws Throwable {
		int lastRow = sheet.getLastRowNum(); 
		
		if(skipRow)
			lastRow += 2;
		else
			lastRow++;
		
		Row footer = sheet.createRow(lastRow);
		
		return this.createXLSCells(footer, 0, footerCellStyle, values);		
	}
	
	private Cell createXLSCells(Row row, int column, CellStyle style, Object... values) {
		return this.doCreateXLSCells(row, column, style, XLS_GUESS_NUMERIC_CELL, values);
	}
	
	private Cell doCreateXLSCells(Row row, int column, CellStyle style, boolean forceNumeric, Object... values) {
		int currentColumn = column;
		
		Cell firstCell = null;
		Cell cell = null;
		
		
		for(Object value : values) {
			cell = row.createCell(currentColumn++);
			
			if(firstCell == null)
				firstCell = cell;
						
			this.setXLSCellContent(cell, style, value, forceNumeric);			
		}
		
		return firstCell;
	}
	
	private Cell createXLSCells(Row row, int column, DataExportFieldModelType fieldModelType, Map<String, CellStyle> styles, Object... values) {
		return this.createXLSCells(row, column, fieldModelType, styles, XLS_GUESS_NUMERIC_CELL, values);
	}
	
	private Cell createXLSCells(Row row, int column, DataExportFieldModelType fieldModelType, Map<String, CellStyle> styles, boolean forceNumeric, Object... values) {
		int currentColumn = column;
		
		Cell firstCell = null;
		Cell cell = null;
		
		CellStyle style = null;
		
		for(Object value : values) {
			cell = row.createCell(currentColumn);
			
			if(firstCell == null)
				firstCell = cell;

			if(fieldModelType.is(DataExportFieldModelType.READ_ONLY)) {
				style = styles.get(XLS_READ_ONLY_CELL_STYLE_ID);
			} else {
				if(value == null)
					style = styles.get(fieldModelType.is(DataExportFieldModelType.MANDATORY) ? XLS_MISSING_MANDATORY_STYLE_ID : XLS_NULL_STYLE_ID);
				else
					style = this.guessXLSCellStyle(styles, value);
			}
						
			this.setXLSCellContent(cell, style, value);
			
//			if(value == null)
			
			currentColumn++;
		}
		
		return firstCell;
	}
	
	private void addXLSRowDetails(Sheet sheet, Map<String, CellStyle> styles, DataExportModel<? super DATA> dataModel, DATA report, Integer vesselBaseSize, int rowNumber) throws Throwable {
		Row row = sheet.createRow(rowNumber); 
						
		int column = 0;
		
		for(DataExportFieldModel<? super DATA> field : dataModel.getFieldsModel()) {
			this.createXLSCells(row, column++, field.getType(), styles, this.getFieldValue(dataModel, field, report));
		}

//		this.addXLSExtraDetails(row, column++, styles, report, vesselBaseSize);
	}
	
//	abstract protected Object[] getXLSExtraDetails(DATA report, Integer vesselBaseSize);
	
//	protected void addXLSExtraDetails(Row row, int startingColumn, Map<String, CellStyle> styles, DATA report, Integer vesselBaseSize) {
//		for(Object data : this.getXLSExtraDetails(report, vesselBaseSize)) {
//			this.createXLSCells(row, startingColumn++, this.guessXLSCellStyle(styles, data), XLS_GUESS_NUMERIC_CELL, data);
//		}
//	}
	
	private CellStyle guessXLSCellStyle(Map<String, CellStyle> styles, Object content) {
		if(content == null)
			return styles.get(XLS_NULL_STYLE_ID);
		
		if(this.isURL(content))
			return styles.get(XLS_HYPERLINK_CELL_STYLE_ID);
		
		if(this.isPercentage(content))
			return styles.get(XLS_PERCENTAGE_CELL_STYLE_ID);

		if(this.isDecimal(content))
			return styles.get(XLS_DECIMAL_CELL_STYLE_ID);
		
		return styles.get(XLS_DEFAULT_STYLE_ID);
	}
	
	private void setXLSCellContent(Cell cell, CellStyle style, Object value, boolean forceNumeric) {
		if(value != null && Number.class.isAssignableFrom(value.getClass())) {
			cell.setCellType(Cell.CELL_TYPE_NUMERIC);
			
			if(Float.class.isAssignableFrom(value.getClass()))
				cell.setCellValue((Float)value);
			else 
				cell.setCellValue(((Number)value).doubleValue());
		} else if(value != null && Date.class.isAssignableFrom(value.getClass())) {
			cell.setCellValue(this.getValue(DateFormatHelper.formatDate((Date)value)));
		} else if(value != null && URL.class.isAssignableFrom(value.getClass())) {
			NameValuePair URLData = this.getURLDetails((URL)value);
			
			org.apache.poi.ss.usermodel.Hyperlink hyperLink = new HSSFHyperlink(org.apache.poi.ss.usermodel.Hyperlink.LINK_URL);
			hyperLink.setLabel(URLData.getName());	
			hyperLink.setAddress((String)URLData.getValue());
			
			cell.setHyperlink(hyperLink);
			cell.setCellValue(this.getValue(URLData.getName()));
		} else {
			cell.setCellValue(this.getValue(value == null ? null : value.toString()));			
		}
		
		if(style != null)
			cell.setCellStyle(style);
		
		//Temporarily removed ( works bad with percentages... :( )
		//if(forceNumeric)
		//	cell.setCellType(Cell.CELL_TYPE_NUMERIC);
	}
	
	private void setXLSCellContent(Cell cell, CellStyle style, Object value) {
		this.setXLSCellContent(cell, style, value, XLS_GUESS_NUMERIC_CELL);
	}	
	
	protected Map<String, CellStyle> createFontMap(Workbook workbook) {
		Font headerFont = workbook.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		
		Font optionalHeaderFont = workbook.createFont();
		optionalHeaderFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		optionalHeaderFont.setColor(HSSFColor.GREY_50_PERCENT.index);
		
		Font hyperlinkFont = workbook.createFont();
		hyperlinkFont.setUnderline(Font.U_SINGLE);
		hyperlinkFont.setColor(HSSFColor.BLUE.index);
		
		Font nullHyperlinkFont = workbook.createFont();
		nullHyperlinkFont.setUnderline(Font.U_SINGLE);
		nullHyperlinkFont.setColor(HSSFColor.RED.index);

		Font boldFont = workbook.createFont();
		boldFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		
		Font missingMandatoryValueFont = workbook.createFont();
		missingMandatoryValueFont.setColor(HSSFColor.RED.index);
		
		Font readOnlyCellFont = workbook.createFont();
		readOnlyCellFont.setItalic(true);
		readOnlyCellFont.setColor(HSSFColor.GREY_40_PERCENT.index);
		
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setWrapText(false);
		headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	
		CellStyle optionalHeaderCellStyle = workbook.createCellStyle();
		optionalHeaderCellStyle.setFont(optionalHeaderFont);
		optionalHeaderCellStyle.setWrapText(false);
		optionalHeaderCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		
		CellStyle hyperlinkCellStyle = workbook.createCellStyle();
		hyperlinkCellStyle.setFont(hyperlinkFont);
		
		CellStyle nullHyperlinkCellStyle = workbook.createCellStyle();
		nullHyperlinkCellStyle.setFont(nullHyperlinkFont);
		
		CellStyle percentageCellStyle = workbook.createCellStyle();
		percentageCellStyle.setDataFormat(workbook.createDataFormat().getFormat("#00.00%"));
		percentageCellStyle.setAlignment(CellStyle.ALIGN_RIGHT);
		
		CellStyle rightAlignCellStyle = workbook.createCellStyle();
		rightAlignCellStyle.setAlignment(CellStyle.ALIGN_RIGHT);
		
		CellStyle boldCellStyle = workbook.createCellStyle();
		boldCellStyle.setAlignment(CellStyle.ALIGN_RIGHT);
		boldCellStyle.setFont(boldFont);
		
		CellStyle missingMandatoryValueStyle = workbook.createCellStyle();
		missingMandatoryValueStyle.setAlignment(CellStyle.ALIGN_RIGHT);
		missingMandatoryValueStyle.setFont(missingMandatoryValueFont);
		
		CellStyle decimalCellStyle = workbook.createCellStyle();
		decimalCellStyle.setAlignment(CellStyle.ALIGN_RIGHT);
		decimalCellStyle.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
		
		CellStyle readOnlyCellStyle = workbook.createCellStyle();
		readOnlyCellStyle.setAlignment(CellStyle.ALIGN_RIGHT);
		readOnlyCellStyle.setFont(readOnlyCellFont);
		
		Map<String, CellStyle> cellStyles = new HashMap<String, CellStyle>();
		cellStyles.put(XLS_HEADER_CELL_STYLE_ID, headerCellStyle);
		cellStyles.put(XLS_HEADER_OPTIONAL_CELL_STYLE_ID, optionalHeaderCellStyle);
		cellStyles.put(XLS_READ_ONLY_CELL_STYLE_ID, readOnlyCellStyle);
		cellStyles.put(XLS_NULL_HYPERLINK_CELL_STYLE_ID, nullHyperlinkCellStyle);
		cellStyles.put(XLS_HYPERLINK_CELL_STYLE_ID, hyperlinkCellStyle);
		cellStyles.put(XLS_DECIMAL_CELL_STYLE_ID, decimalCellStyle);
		cellStyles.put(XLS_PERCENTAGE_CELL_STYLE_ID, percentageCellStyle);
		cellStyles.put(XLS_RIGHT_ALIGN_CELL_STYLE_ID, rightAlignCellStyle);
		cellStyles.put(XLS_BOLD_CELL_STYLE_ID, boldCellStyle);
		cellStyles.put(XLS_MISSING_MANDATORY_STYLE_ID, missingMandatoryValueStyle);
		cellStyles.put(XLS_NULL_STYLE_ID, rightAlignCellStyle);
		cellStyles.put(XLS_DEFAULT_STYLE_ID, rightAlignCellStyle);
		
		return cellStyles;
	}
}