/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.utilities.configuration;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.io.Resource;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Mar 2013
 */
public class JNDIPropertiesConfigurationResolver extends PropertyPlaceholderConfigurer implements InitializingBean, BeanPostProcessor, PriorityOrdered  {
	private Logger _log = LoggerFactory.getLogger(this.getClass());
	
	private String _base;
	private String _filter;
	
	private Pattern _filterPattern;
	
	/**
	 * @return the 'base' value
	 */
	public String getBase() {
		return this._base;
	}

	/**
	 * @param base the 'base' value to set
	 */
	public void setBase(String base) {
		this._base = base;
	}

	/**
	 * @return the 'filter' value
	 */
	public String getFilter() {
		return this._filter;
	}

	/**
	 * @param filter the 'filter' value to set
	 */
	public void setFilter(String filter) {
		this._filter = filter;
	}

	/**
	 * @return the 'filterPattern' value
	 */
	public Pattern getFilterPattern() {
		return this._filterPattern;
	}

	/**
	 * @param filterPattern the 'filterPattern' value to set
	 */
	public void setFilterPattern(Pattern filterPattern) {
		this._filterPattern = filterPattern;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		if(this._base != null)
			if(!this._base.endsWith("/"))
				this._base += "/";
		
		this._filter = StringsHelper.trim(this._filter);
		
		if(this._filter == null)
			this._filter = "*";
		
		this._filterPattern = Pattern.compile(this._base + this._filter.replaceAll("\\*", ".*"));
	}

	/* (non-Javadoc)
	 * @see org.springframework.core.io.support.PropertiesLoaderSupport#setLocation(org.springframework.core.io.Resource)
	 */
	@Override
	public void setLocation(Resource location) {
		throw new UnsupportedOperationException("Cannot set location on this property placeholder configurer");
	}

	/* (non-Javadoc)
	 * @see org.springframework.core.io.support.PropertiesLoaderSupport#setLocations(org.springframework.core.io.Resource[])
	 */
	@Override
	public void setLocations(Resource... locations) {
		throw new UnsupportedOperationException("Cannot set location on this property placeholder configurer");
	}
	
	private Collection<String> listResources(Collection<String> all, String prefix, Context context) throws NamingException {
		Collection<String> current = new ArrayList<String>();
		
		NamingEnumeration<NameClassPair> list = context.list("");
		
		Object value;
		NameClassPair entry;
		String entryName;
		
		while(list.hasMore()) {
			entry = list.next();
			entryName = entry.getName();

			value = context.lookup(entryName);
			
			if(value instanceof Context) {
				current = this.listResources(current, prefix == null ? entryName : ( prefix + "/" + entryName ), (Context)context.lookup(entryName));
			} else {
				current.add(prefix == null ? entryName : ( prefix + "/" + entryName ));
			}
		}
		
		all.addAll(current);
		
		return all;
	}

	/* (non-Javadoc)
	 * @see org.springframework.core.io.support.PropertiesLoaderSupport#loadProperties(java.util.Properties)
	 */
	@Override
	protected void loadProperties(Properties props) throws IOException {
		try {
			InitialContext initialContext = new InitialContext();
			
			StringBuilder properties = new StringBuilder();

			boolean isPrimitive;

			Object value;
			
			for(String resource : this.listResources(new ArrayList<String>(), null, initialContext)) {
				if(this._filterPattern.matcher(resource).find()) {
					value = initialContext.lookup(resource);

					if(value != null) {
						isPrimitive = ObjectsHelper.isPrimitive(null, value); 
						
						if(!(value instanceof Context)) {
							properties.append("#JNDI resource: ").append(resource).append("\n");
							properties.append("#Bound type: ").append(value.getClass().getName()).append(" [ ").append(isPrimitive ? "primitive" : "non primitive").append(" ]").append("\n");
							
							properties.append(resource.replace(this._base, "").replaceAll("\\/", ".")).append("=").append(value.toString()).append("\n");
						}
					}
				}
			}
			
			this._log.debug("Assembled properties file:");
			this._log.debug(properties.toString());
			
			InputStream stream = new ByteArrayInputStream( properties.toString().getBytes("UTF-8"));
			
			props.load(stream);
		} catch(Throwable t) {
			throw new RuntimeException(t);
		}
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessBeforeInitialization(java.lang.Object, java.lang.String)
	 */
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.config.BeanPostProcessor#postProcessAfterInitialization(java.lang.Object, java.lang.String)
	 */
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.config.PropertyResourceConfigurer#getOrder()
	 */
	public int getOrder() {
		return Ordered.HIGHEST_PRECEDENCE;
	}
}