/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.filters.security.sessions;

import java.io.IOException;
import java.util.Date;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.fao.fi.vrmf.common.j2ee.filters.AbstractOncePerRequestFilter;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Jul 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Jul 2012
 */
@Named
@Singleton
public class SpoofingBlockerFilter extends AbstractOncePerRequestFilter implements SpoofingBlockerFilterConstants {
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		super.init(filterConfig);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void internalDoFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		this._log.info("Executing " + this.getFilterName() + " doFilter...");
		
		long end, start = System.currentTimeMillis();
		
		try {
			long now = System.currentTimeMillis();
			
			HttpServletRequest hRequest = (HttpServletRequest)request;
			HttpServletResponse hResponse = (HttpServletResponse)response;
			HttpSession session = hRequest.getSession(false);
			
			if(session != null) {
				Boolean alreadyBlocked = (Boolean)session.getAttribute(ALREADY_BLOCKED_SESSION_ATTRIBUTE);
				String sessionStoredIP = (String)session.getAttribute(INITIAL_IP_SESSION_ATTRIBUTE);
				String requestIP = ServletsHelper.getRequestInfo(hRequest).getRemoteAddress();
				
				if(sessionStoredIP != null) {
					if(!sessionStoredIP.equals(requestIP)) {
						if(Boolean.TRUE.equals(alreadyBlocked)) {
							this._log.info("Skipping session spoofing check as it was already performed in one of the previous requests");
						} else {
							String message = "An attempt to spoof session " + session.getId() + " has been detected. " +
											 "Session initial IP '" + sessionStoredIP + "' (stored on " + ( new Date(now) ) + ") conflicts with current request IP '" +  requestIP + "'";
			
							this._log.warn(message);
							
							session.setAttribute(ALREADY_BLOCKED_SESSION_ATTRIBUTE, Boolean.TRUE);
							
							hResponse.sendError(HttpServletResponse.SC_FORBIDDEN, message);
							
							return;
						}
					}
				} else {
					session.setAttribute(INITIAL_IP_TIMESTAMP_SESSION_ATTRIBUTE, now);
					session.setAttribute(INITIAL_IP_SESSION_ATTRIBUTE, requestIP);
					session.setAttribute(ALREADY_BLOCKED_SESSION_ATTRIBUTE, Boolean.FALSE);
				}
			} else {
				this._log.debug("Cannot set request IP into HTTP session as the latter is NULL");
			}
			
			chain.doFilter(hRequest, hResponse);
			
			this._log.debug("Request has been filtered via " + this.getClass().getSimpleName());
		} finally {
			end = System.currentTimeMillis();
			this._log.info(this.getClass().getSimpleName() + " took " + ( end - start ) + " mSec. to execute");
		}
	}
}