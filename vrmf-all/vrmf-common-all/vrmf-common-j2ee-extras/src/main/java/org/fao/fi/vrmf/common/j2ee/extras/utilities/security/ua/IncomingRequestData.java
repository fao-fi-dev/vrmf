/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua;

import java.util.Date;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 Jan 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 Jan 2014
 */
public class IncomingRequestData extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7784030003357904185L;
	
	private Integer id;
	private Date timestamp;
	private String targetApp;
	private String requestSource;
	private String requestSourceCountry;
	private String requestUrl;
	private String requestMethod;
	private String requestTarget;
	private String userAgent;
	private String userAgentType;
	private Boolean isBot;
	private String referrer;
	private String loggedUser;
	private String sessionId;
	private String comment;
	
	/**
	 * @return the 'id' value
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @param id the 'id' value to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the 'timestamp' value
	 */
	public Date getTimestamp() {
		return this.timestamp;
	}

	/**
	 * @param timestamp the 'timestamp' value to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the 'targetApp' value
	 */
	public String getTargetApp() {
		return this.targetApp;
	}

	/**
	 * @param targetApp the 'targetApp' value to set
	 */
	public void setTargetApp(String targetApp) {
		this.targetApp = targetApp;
	}

	/**
	 * @return the 'requestSource' value
	 */
	public String getRequestSource() {
		return this.requestSource;
	}

	/**
	 * @param requestSource the 'requestSource' value to set
	 */
	public void setRequestSource(String requestSource) {
		this.requestSource = requestSource;
	}

	/**
	 * @return the 'requestSourceCountry' value
	 */
	public String getRequestSourceCountry() {
		return this.requestSourceCountry;
	}

	/**
	 * @param requestSourceCountry the 'requestSourceCountry' value to set
	 */
	public void setRequestSourceCountry(String requestSourceCountry) {
		this.requestSourceCountry = requestSourceCountry;
	}

	/**
	 * @return the 'requestUrl' value
	 */
	public String getRequestUrl() {
		return this.requestUrl;
	}

	/**
	 * @param requestUrl the 'requestUrl' value to set
	 */
	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	/**
	 * @return the 'requestMethod' value
	 */
	public String getRequestMethod() {
		return this.requestMethod;
	}

	/**
	 * @param requestMethod the 'requestMethod' value to set
	 */
	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}
	
	/**
	 * @return the 'requestTarget' value
	 */
	public String getRequestTarget() {
		return this.requestTarget;
	}

	/**
	 * @param requestTarget the 'requestTarget' value to set
	 */
	public void setRequestTarget(String requestTarget) {
		this.requestTarget = requestTarget;
	}

	/**
	 * @return the 'userAgent' value
	 */
	public String getUserAgent() {
		return this.userAgent;
	}

	/**
	 * @param userAgent the 'userAgent' value to set
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	/**
	 * @return the 'userAgentType' value
	 */
	public String getUserAgentType() {
		return this.userAgentType;
	}

	/**
	 * @param userAgentType the 'userAgentType' value to set
	 */
	public void setUserAgentType(String userAgentType) {
		this.userAgentType = userAgentType;
	}

	/**
	 * @return the 'isBot' value
	 */
	public Boolean getIsBot() {
		return this.isBot;
	}

	/**
	 * @param isBot the 'isBot' value to set
	 */
	public void setIsBot(Boolean isBot) {
		this.isBot = isBot;
	}

	/**
	 * @return the 'referrer' value
	 */
	public String getReferrer() {
		return this.referrer;
	}

	/**
	 * @param referrer the 'referrer' value to set
	 */
	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	/**
	 * @return the 'loggedUser' value
	 */
	public String getLoggedUser() {
		return this.loggedUser;
	}

	/**
	 * @param loggedUser the 'loggedUser' value to set
	 */
	public void setLoggedUser(String loggedUser) {
		this.loggedUser = loggedUser;
	}

	/**
	 * @return the 'sessionId' value
	 */
	public String getSessionId() {
		return this.sessionId;
	}

	/**
	 * @param sessionId the 'sessionId' value to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the 'comment' value
	 */
	public String getComment() {
		return this.comment;
	}

	/**
	 * @param comment the 'comment' value to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}	
}