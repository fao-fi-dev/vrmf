/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.utilities.security;

import java.util.Collection;

import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.ProcessedRequestData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 Jan 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 Jan 2014
 */
public interface BandwidthManager {
	Collection<BandwidthLimits> getCurrentLimits() throws Exception;
	
	int log(ProcessedRequestData request) throws Exception;
	
	ProcessedRequestData getLogById(int id) throws Exception;
	
	Collection<ProcessedRequestData> filter(ProcessedRequestsFilter filter) throws Exception;
	
	int count(ProcessedRequestsFilter filter) throws Exception;
}
