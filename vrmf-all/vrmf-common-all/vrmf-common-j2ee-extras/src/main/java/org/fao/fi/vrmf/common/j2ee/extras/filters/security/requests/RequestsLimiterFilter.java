/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.filters.security.requests;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.gis.services.geocoding.countries.CountryLookupByIPService;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.j2ee.NetworkConstants;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.BandwidthLimits;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.BandwidthManager;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ProcessedRequestsFilter;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.ProcessedRequestData;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentManager;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentModel;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.providers.SimpleUserAgentChecker;
import org.fao.fi.vrmf.common.j2ee.filters.AbstractOncePerRequestFilter;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper.RequestInfo;
import org.fao.fi.vrmf.common.models.security.BasicUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Jul 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Jul 2012
 */
@Named
@Singleton
public class RequestsLimiterFilter extends AbstractOncePerRequestFilter implements RequestsLimiterFilterConstants {
	private Boolean _logInternalRequests = Boolean.FALSE;
	private String _applicationId = null;
	private String _internalIPsPattern = null;
	private Pattern _internalIPsChecker;

	final private Long SECOND = 1000L;
	final private Long MINUTE = SECOND * 60;
	final private Long HOUR   = MINUTE * 60;
	final private Long DAY    = HOUR * 24;

	private Logger _crawlersLog = LoggerFactory.getLogger(this.getClass().getName() + ".crawlers");

	@Inject private BandwidthManager _bandwidthManager;
	@Inject private CountryLookupByIPService _countryLookupService;	
	@Inject private UserAgentManager _userAgentManager;

	private Map<String, Collection<BandwidthLimits>> _limitsMap = new HashMap<String, Collection<BandwidthLimits>>();
	private Map<String, Pattern> _patternsMap = new HashMap<String, Pattern>();
	
	@PostConstruct public void init() throws Exception {
		String appId;
		String filter;
		Collection<BandwidthLimits> limits;
		
		for(BandwidthLimits limit : this._bandwidthManager.getCurrentLimits()) {
			appId = limit.getApplicationId();
			limits = this._limitsMap.get(appId);
			
			if(limits == null) {
				limits = new ListSet<BandwidthLimits>();
			
				this._limitsMap.put(appId, limits);
			}

			filter = StringsHelper.trim(limit.getIpFilter());
					
			if(filter != null) {
				if(!this._patternsMap.containsKey(filter)) {
					try {
						this._patternsMap.put(filter, Pattern.compile(filter));
						
						this._log.info("Request limit data {} specifies {} as an IP filter", limit, filter);
					} catch (Throwable t) {
						this._log.error("IP Filter '{}' specified by request limit data {} cannot be compiled as a proper regexp: {} [ {} ]",
										filter,
										limit,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			} else {
				this._log.warn("Request limit data {} does not specify an IP filter", limit);
			}
			
			limits.add(limit);
		}
	}

	/**
	 * @return the 'applicationID' value
	 */
	public String getApplicationId() {
		return this._applicationId;
	}

	/**
	 * @param applicationId the 'applicationID' value to set
	 */
	public void setApplicationId(String applicationId) {
		this._applicationId = applicationId;
	}

	/**
	 * @return the 'internalIPsPattern' value
	 */
	public String getInternalIPsPattern() {
		return this._internalIPsPattern;
	}

	/**
	 * @param internalIPsPattern the 'internalIPsPattern' value to set
	 */
	public void setInternalIPsPattern(String internalIPsPattern) {
		this._internalIPsPattern = internalIPsPattern;

		if(internalIPsPattern != null) {
			this._log.info("Attempting to set " + internalIPsPattern + " as 'internal' IPs pattern...");

			try {
				this._internalIPsChecker = Pattern.compile(this._internalIPsPattern);
			} catch (Throwable t) {
				this._log.error(internalIPsPattern + " is not a valid regular expression. Internal IPs checking will be DISABLED", t);
			}
		}
	}

	/**
	 * @return the 'countryLookupService' value
	 */
	public CountryLookupByIPService getCountryLookupService() {
		return this._countryLookupService;
	}

	/**
	 * @param countryLookupService the 'countryLookupService' value to set
	 */
	public void setCountryLookupService(CountryLookupByIPService countryLookupService) {
		this._countryLookupService = countryLookupService;
	}

	/**
	 * @return the 'userAgentManager' value
	 */
	public UserAgentManager getUserAgentManager() {
		return this._userAgentManager;
	}

	/**
	 * @param userAgentManager the 'userAgentManager' value to set
	 */
	public void setUserAgentManager(UserAgentManager userAgentManager) {
		this._userAgentManager = userAgentManager;
	}

	public UserAgentChecker getUserAgentChecker() {
//		UserAgentChecker checker = this.getUserAgentManager().getFirstValidUserAgentCheckerOfType(UASUserAgentChecker.class);

		UserAgentChecker checker = this.getUserAgentManager().getDefaultChecker();
		
		if(checker == null) {
			this._log.warn("The default User Agent checker registered by the User Agent Manager is currently NULL. Returning its simplest implementation ({})", SimpleUserAgentChecker.class.getSimpleName());
			
			return new SimpleUserAgentChecker();
		}
		
		return checker;
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		super.init(filterConfig);

		try {
			Context initCtx = new InitialContext(); 
			Context envCtx = (Context) initCtx.lookup("java:comp/env"); 
			String logInternalRequestsJNDIPropertyValue = "false";

			try {
				logInternalRequestsJNDIPropertyValue = (String)envCtx.lookup(LOG_INTERNAL_REQUESTS_JNDI_RESOURCE);
			} catch (Throwable t) {
				this._log.info("Internal requests log JNDI property (" + LOG_INTERNAL_REQUESTS_JNDI_RESOURCE + ") is set to " + logInternalRequestsJNDIPropertyValue); 
			}

			this._logInternalRequests = Boolean.parseBoolean(logInternalRequestsJNDIPropertyValue);
		} catch (Throwable t) {
			this._log.warn("Unable to lookup or parse internal requests log JNDI property (" + LOG_INTERNAL_REQUESTS_JNDI_RESOURCE + ")", t);
		}

		this._log.info("Logging of internal requests is " + ( this._logInternalRequests ? "ENABLED" : "DISABLED" ));

		this._applicationId = this.getConfigurationParameter(APPLICATION_ID_CONFIGURATION_PARAMETER);

		if(this._applicationId != null)
			this._log.debug("Filter will track requests for application '" + this._applicationId + "'");
		else
			this._log.warn("No application ID specified");

		this._internalIPsPattern = this.getConfigurationParameter(INTERNAL_IPS_PATTERN_CONFIGURATION_PARAMETER);

		if(this._internalIPsPattern == null) {
			this._log.warn("No internal IP pattern specified for filter either as its init parameter or at context level: using default FAO LAN internal IPs pattern (" + NetworkConstants.INTERNAL_IPS_REGEXP + ")");
			this._internalIPsPattern = NetworkConstants.INTERNAL_IPS_REGEXP;
		}

		this.setInternalIPsPattern(this._internalIPsPattern);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void internalDoFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		this._log.info("Executing " + this.getFilterName() + " internalDoFilter...");

		HttpServletRequest hRequest = (HttpServletRequest)request;
		HttpServletResponse hResponse = (HttpServletResponse)response;

		long end, start = System.currentTimeMillis();

		this._log.debug("BEFORE doFilter : [ URI: " + hRequest.getRequestURI() + " - LRMID: " + hRequest.getAttribute(LAST_RECORD_ID_REQUEST_ATTRIBUTE) + " ]");

//		super.doFilter(request, response, chain);

		this._log.debug("AFTER doFilter : [ URI: " + hRequest.getRequestURI() + " - LRMID: " + hRequest.getAttribute(LAST_RECORD_ID_REQUEST_ATTRIBUTE) + " ]");

		BasicUser loggedUser = ServletsHelper.getLoggedUser(hRequest);

		try {
			RequestInfo requestInfo = ServletsHelper.getRequestInfo(hRequest);

			boolean allow = this.allow(this._applicationId, requestInfo, loggedUser);

			if(!allow || this.shouldLog(requestInfo)) {
				this.log(hRequest, this._applicationId, requestInfo, loggedUser, !allow);
			}

			if(allow) {
				chain.doFilter(request, response);
			} else {
				this._log.warn("Bandwidth limit exceeded for request " + requestInfo);

				hResponse.sendError(429, "Bandwidth limit exceeded!"); //Bandwidth Limit Exceeded - see: http://en.wikipedia.org/wiki/List_of_HTTP_status_codes#429 (previously it was 509)
			}
		} catch (Throwable t) {
			String message = "Unexpected error (" + t.getClass().getSimpleName() + ": " + t.getMessage() + ")";
			this._log.error(message);

			hResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message);
		} finally {
			end = System.currentTimeMillis();
			this._log.info(this.getClass().getSimpleName() + " took " + ( end - start ) + " mSec. to execute");
		}

		this._log.debug("END doFilter : [ URI: " + hRequest.getRequestURI() + " - LRMID: " + hRequest.getAttribute(LAST_RECORD_ID_REQUEST_ATTRIBUTE) + " ]");

		this._log.debug("Request has been filtered via " + this.getClass().getSimpleName());
	}

	private boolean shouldLog(RequestInfo requestInfo) {
		UserAgentChecker checker = this.getUserAgentChecker();
		UserAgentModel userAgent = checker.identify(requestInfo.getUserAgent());
			
		boolean isInternal = this.isInternalRequest(requestInfo);
		boolean isCrawler = userAgent != null && checker.isCrawler(userAgent);
		boolean identifyIP = isCrawler && checker.canIdentifyIPs();
		boolean validIP = identifyIP && checker.checkIP(userAgent, requestInfo.getRemoteAddress());
		
		//Log if a user agent is set, request is not internal, is not coming from a crawler OR the crawler is on a valid IP... 
		return userAgent!= null && !isInternal && (!isCrawler || ( identifyIP && validIP ));
	}

	private boolean isInternalRequest(RequestInfo requestInfo) {
		return this._logInternalRequests ? Boolean.FALSE : this._internalIPsChecker.matcher(requestInfo.getRemoteAddress()).matches();
	}

	private void log(HttpServletRequest request, String applicationID, RequestInfo requestInfo, BasicUser loggedUser, boolean blocked) {
		this._log.debug("Logging remote request to '" + applicationID + "'");

		try {
			UserAgentModel userAgent = this.getUserAgentChecker().identify(requestInfo.getUserAgent());

			ProcessedRequestData meta = new ProcessedRequestData();
			meta.setTargetApp(this._applicationId);
			meta.setLoggedUser(loggedUser == null ? null : loggedUser.getId());
			meta.setRequestMethod(request.getMethod());
			meta.setRequestSource(requestInfo.getRemoteAddress());
			meta.setRequestSourceCountry(this._countryLookupService.getCountryCodeByIP(requestInfo.getRemoteAddress()));
			meta.setRequestTarget(requestInfo.getLocalAddress());
			meta.setRequestUrl(request.getRequestURI());
			meta.setReferrer(requestInfo.getReferer());
			meta.setUserAgent(requestInfo.getUserAgent());
			meta.setUserAgentType(userAgent == null ? null : userAgent.getAgentType());
			meta.setIsBot(requestInfo.getIsBot());
			meta.setSessionId(request.getSession(false) == null ? null : request.getSession(false).getId());
			meta.setBlocked(blocked);

			if(blocked)
				meta.setStatusCode(509); //Bandwidth limit exceeded (Non standard HTTP Status code)

			meta.setId(this._bandwidthManager.log(meta));

			request.setAttribute(LAST_RECORD_ID_REQUEST_ATTRIBUTE, meta.getId());
		} catch (Throwable t) {
			this._log.warn("Unable to handle remote request metadata. Unexpected " + t.getClass().getSimpleName() + " caught (" + t.getMessage() + ")");
		} 
	}
	
	private BandwidthLimits computeLimits(String applicationID, RequestInfo requestInfo) {
		Collection<BandwidthLimits> limits = this._limitsMap.get(applicationID);

		BandwidthLimits narrowed = new BandwidthLimits();
		narrowed.setApplicationId(applicationID);
		narrowed.setMaxPerDay(1000);
		narrowed.setMaxPerHour(300);
		narrowed.setMaxPerMinute(30);
		narrowed.setMaxPerSecond(5);
		
		final String IP = requestInfo.getRemoteAddress();
		
		if(limits != null && !limits.isEmpty()) {
			String IPFilter;
			for(BandwidthLimits limit : limits) {
				IPFilter = StringsHelper.trim(limit.getIpFilter());
				
				if(IPFilter != null && this._patternsMap.get(IPFilter).matcher(IP).matches()) {
					narrowed.setMaxPerDay(Math.min(narrowed.getMaxPerDay(), limit.getMaxPerDay()));
					narrowed.setMaxPerHour(Math.min(narrowed.getMaxPerHour(), limit.getMaxPerHour()));
					narrowed.setMaxPerMinute(Math.min(narrowed.getMaxPerMinute(), limit.getMaxPerMinute()));
					narrowed.setMaxPerSecond(Math.min(narrowed.getMaxPerSecond(), limit.getMaxPerSecond()));
				}
			}
		}
		
		return narrowed;
	}

	private boolean allow(String applicationID, RequestInfo requestInfo, BasicUser loggedUser) throws Throwable {
		UserAgentChecker checker = this.getUserAgentChecker();
		
		if(this.shouldLog(requestInfo)) {
			if(loggedUser != null && loggedUser.is("ADMINISTRATOR"))
				return true;

			boolean filterByIP = loggedUser == null || !loggedUser.is("REAL_USER");

			boolean filterByUserID = !filterByIP;

			boolean hasLimitPerSecond = loggedUser != null && !loggedUser.getCapabilities("LIMIT_PER_SECOND").isEmpty();
			boolean hasLimitPerMinute = loggedUser != null && !loggedUser.getCapabilities("LIMIT_PER_MINUTE").isEmpty();
			boolean hasLimitPerHour   = loggedUser != null && !loggedUser.getCapabilities("LIMIT_PER_HOUR").isEmpty();
			boolean hasLimitPerDay 	  = loggedUser != null && !loggedUser.getCapabilities("LIMIT_PER_DAY").isEmpty();

			Integer limitPerSecond = hasLimitPerSecond ? Integer.valueOf(loggedUser.getCapability("LIMIT_PER_SECOND").getCapabilityParameters()) : 5;
			Integer limitPerMinute = hasLimitPerMinute ? Integer.valueOf(loggedUser.getCapability("LIMIT_PER_MINUTE").getCapabilityParameters()) : 30;
			Integer limitPerHour = hasLimitPerHour ? Integer.valueOf(loggedUser.getCapability("LIMIT_PER_HOUR").getCapabilityParameters()) : 300;
			Integer limitPerDay = hasLimitPerDay ? Integer.valueOf(loggedUser.getCapability("LIMIT_PER_DAY").getCapabilityParameters()) : 1000;

			long now = System.currentTimeMillis();

			BandwidthLimits limit = this.computeLimits(applicationID, requestInfo);

			if(filterByIP) {
				limitPerSecond = limit.getMaxPerSecond() < limitPerSecond ? limit.getMaxPerSecond() : limitPerSecond;
				limitPerMinute = limit.getMaxPerMinute() < limitPerMinute ? limit.getMaxPerMinute() : limitPerMinute;
				limitPerHour = limit.getMaxPerHour() < limitPerHour ? limit.getMaxPerHour() : limitPerHour;
				limitPerDay = limit.getMaxPerDay() < limitPerDay ? limit.getMaxPerDay() : limitPerDay;
			}

			if(limitPerSecond == 0 || limitPerMinute == 0 || limitPerHour == 0 || limitPerDay == 0)
				return false;

			ProcessedRequestsFilter filter = new ProcessedRequestsFilter();
			filter.setApplicationID(applicationID);

			if(filterByIP)
				filter.setRequestSource(requestInfo.getRemoteAddress());
			else if(filterByUserID) 
				filter.setLoggedUser(loggedUser.getId());
			
			filter.setTimestampFrom(now - SECOND);
			filter.setTimestampTo(now);

			int currentPerSecond = this._bandwidthManager.count(filter); //this._requestMetadataDAO.countByExample(metadataFilter);

			this._log.debug("{} last second requests to {} by {}",
							 currentPerSecond, 
							 applicationID, 
							 filterByIP ? "IP " + requestInfo.getRemoteAddress() : "User " + loggedUser.getId());

			if(currentPerSecond > limitPerSecond)
				return false;

			filter.setTimestampFrom(now - MINUTE);

			int currentPerMinute = this._bandwidthManager.count(filter); //this._requestMetadataDAO.countByExample(metadataFilter);

			this._log.debug("{} last minute requests to {} by {} {}",
							currentPerMinute,
							applicationID,
							filterByIP ? "IP " + requestInfo.getRemoteAddress() : "User " + loggedUser.getId());

			if(currentPerMinute > limitPerMinute)
				return false;

			filter.setTimestampFrom(now - HOUR);

			int currentPerHour = this._bandwidthManager.count(filter);

			this._log.debug("{} last hour requests to {} by {}",
							currentPerMinute, 
							applicationID,
							filterByIP ? "IP " + requestInfo.getRemoteAddress() : "User " + loggedUser.getId());

			if(currentPerHour > limitPerHour)
				return false;

			filter.setTimestampFrom(now - DAY);
			int currentPerDay = this._bandwidthManager.count(filter);

			this._log.debug("{} last day requests to {} by {} {}",
							requestInfo.getRequestURI(),
							applicationID,
							filterByIP ? "IP " + requestInfo.getRemoteAddress() : "User " + loggedUser.getId());

			if(currentPerDay > limitPerDay)
				return false;

			return true;
		} else if(checker.isCrawler(requestInfo.getUserAgent())) {
			String crawlerVersion = this.getUserAgentChecker().getUserAgentAndVersion(requestInfo.getUserAgent());

			this._crawlersLog.info("{} crawling {} [ IP: {} - UA: {} ]",
					crawlerVersion, 
					requestInfo.getRequestURI(),
					requestInfo.getRemoteAddress(),
					requestInfo.getUserAgent());

			return true;
		} else {
			this._crawlersLog.debug("Letting requests for {} [ IP: {} - UA: {} ] pass by unlogged (internal IP?)",
									requestInfo.getRequestURI(),
									requestInfo.getRemoteAddress(),
									requestInfo.getUserAgent());
			
			return true;
		}
	}
}