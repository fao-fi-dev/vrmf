/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.http.conn.ConnectTimeoutException;
import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentModel;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentType;
import org.springframework.cache.annotation.Cacheable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Nov 2012
 */
@Named
@Singleton
abstract public class UserAgentChecker extends AbstractLoggingAwareClient {
	static final protected long PRIORITY_LOW 	= 0;
	static final protected long PRIORITY_MEDIUM	= 10;
	static final protected long PRIORITY_HIGH	= 100;
	static final protected long PRIORITY_HIGHEST= 1000;

	static final private String DEFAULT_METHODS_INVOCATION_CACHE_ID = "vrmf.common.j2ee.extras.security.user.agent";
	static final private String EXAMPLE_UA_STRING = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31";

	static final protected String[] JSON_CONTENT_TYPES = new String[] {
		"application/json",
		"application/x-javascript",
		"text/javascript",
		"text/x-javascript",
		"text/x-json"
	};

	static final protected String[] XML_CONTENT_TYPES = new String[] {
		"application/xml",
		"text/xml"
	};

	static final protected String[] HTML_CONTENT_TYPES = new String[] {
		"text/html"
	};

	@Inject	protected HTTPHelper _helper;

	abstract public long getPriority();

	/**
	 * Class constructor
	 *
	 */
	public UserAgentChecker() {
		super();
	}

	/**
	 * @return the 'helper' value
	 */
	public HTTPHelper getHelper() {
		return this._helper;
	}

	/**
	 * @param helper the 'helper' value to set
	 */
	public void setHelper(HTTPHelper helper) {
		this._helper = helper;
	}

	abstract public boolean canIdentifyIPs();

	abstract protected String[] getExpectedResponseContentType();

	abstract protected HTTPRequestMetadata doInvokeIdentificationServiceAPI(String userAgentString, int timeout) throws ConnectTimeoutException, IOException;

	abstract protected UserAgentModel doProcessIdentificationResults(UserAgentModel agent, HTTPRequestMetadata response) throws Exception;

	abstract protected HTTPRequestMetadata doInvokeIPCheckServiceAPI(String userAgentString, int timeout)  throws ConnectTimeoutException, IOException;

	abstract protected Set<String> doProcessIPCheckResults(UserAgentModel userAgent);

	final protected Set<String> doProcessIPCheckResults(String userAgentString) {
		return this.doProcessIPCheckResults(this.identify(userAgentString));
	}

	final protected boolean checkResponseType(HTTPRequestMetadata response) {
		if(response == null)
			return false;

		if(response.getContentLength() == 0) {
			this._log.warn("Response to user agent identification has a content length of 0");

			return false;
		}

		String actualContentType = StringsHelper.trim(response.getContentType());

		if(actualContentType != null) {
			actualContentType = StringsHelper.trim(actualContentType.replaceAll("\\;.+", ""));
		}

		if(actualContentType == null) {
			this._log.warn("Response to user agent identification has a NULL content type ('{}')", response.getContentType());

			return false;
		}

		String[] expected = this.getExpectedResponseContentType();

		if(expected != null && expected.length > 0) {
			Set<String> expectedSet = new TreeSet<String>(Arrays.asList(expected));

			if(expectedSet.contains(actualContentType)) {
				this._log.debug("Content type {} is valid according to User Agent Manager {} specifications", actualContentType, this.getClass().getSimpleName());

				return true;
			}

			this._log.warn("Content type {} is not valid according to User Agent Manager {} specifications. Valid content type(s) are: {}", actualContentType, this.getClass().getSimpleName(), CollectionsHelper.join(expected, ", "));

			return false;
		} else {
			this._log.warn("User Agent Manager {} doesn't specify any expected response content type: assuming that current {} is valid, then", this.getClass().getSimpleName(), response.getContentType());
		}

		return true;
	}

	final public boolean isAvailable() {
		return this.isAvailable(1000);
	}

	final public boolean isAvailable(int timeout) {
		try {
			this.invokeIdentificationService(EXAMPLE_UA_STRING, timeout);

			return true;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]. Assuming service is unavailable",
							this.getClass().getSimpleName(),
							t.getClass().getSimpleName(),
							t.getMessage());
			
			return false;
		}
	}

	final protected HTTPRequestMetadata invokeIdentificationService(String userAgentString) {
		return this.invokeIdentificationService(userAgentString, 1000);
	}

	protected HTTPRequestMetadata invokeIdentificationService(String userAgentString, int timeout) {
		try {
			HTTPRequestMetadata response = this.doInvokeIdentificationServiceAPI(userAgentString, timeout);

			if(response != null) {
				if(response.getResponseCode() >= 400) {
					this._log.warn("{} API is down [ got a {} error while attempting to access {} ]", this.getClass().getSimpleName(), response.getResponseCode(), response.getURL());

					throw new UnsupportedOperationException("ERROR_CODE_" + response.getResponseCode());
				}
			} else {
				this._log.warn("NULL response returned by connecting to {} API", this.getClass().getSimpleName());

				throw new UnsupportedOperationException("NULL_RESPONSE");
			}

			if(this.checkResponseType(response))
				return response;

			this._log.warn("Wrong response type returned by connecting to {} API at {} [ Expecting: {} - Returned content type: {} - Returned content length: {} ]", this.getClass().getSimpleName(), response.getURL(), this.getExpectedResponseContentType(), response.getContentType(), response.getContentLength());

			throw new UnsupportedOperationException("WRONG_TYPE_" + response.getContentType() + "_" + response.getContentLength());
		} catch (ConnectTimeoutException Ce) {
			this._log.warn("Connection to {} API timed out (the service is probaly down... [ {} ])", this.getClass().getSimpleName(), Ce.getMessage());

			throw new UnsupportedOperationException(Ce);
		} catch (IOException IOe) {
			this._log.warn("IO exception caught while connecting to {} API [ {} ])", this.getClass().getSimpleName(), IOe.getMessage());

			throw new UnsupportedOperationException(IOe);
		}
	}

	final protected UserAgentModel processResults(String userAgentString, HTTPRequestMetadata response) {
		UserAgentModel agent = new UserAgentModel(userAgentString);

		try {
			return this.doProcessIdentificationResults(agent, response);
		} catch(Throwable t) {
			this._log.error("Unable to process results retrieved by User Agent Manager {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());
			this._log.error("Returned service response is: {}", new String(response.getContent()));
			this._log.error("As a consequence, no User Agent can be properly identified...");

			return agent;
		}
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #userAgentString")
	public UserAgentModel identify(String userAgentString) {
		this._log.debug("Attempting to identify user agent {}", userAgentString);

		String trimmed = StringsHelper.trim(userAgentString);

		if(trimmed == null)
			return null;

		try {
			UserAgentModel identified = this.doProcessIdentificationResults(new UserAgentModel(userAgentString), this.invokeIdentificationService(userAgentString));

			this._log.info("User Agent {} was identified as {} by {}", userAgentString, identified, this.getClass().getSimpleName());

			return identified;
		} catch (Throwable t) {
			this._log.error("Unexpected error caught while attempting to search User Agent details for {} [ {}: {} ]", trimmed, t.getClass().getSimpleName(), t.getMessage());

			return new UserAgentModel(userAgentString);
		}
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #userAgentString")
	public boolean isCrawler(String userAgentString) {
		this._log.debug("Attempting to check whether user agent {} is a crawler", userAgentString);

		try {
			return this.isCrawler(this.identify(userAgentString));
		} catch (Throwable t) {
			this._log.error("Unable to determine whether {} is a crawler: {} [ {} ]. Returning 'FALSE'", userAgentString, t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #userAgent.agentType")
	public boolean isCrawler(UserAgentModel userAgent) {
		this._log.debug("Attempting to check whether user agent {} is a crawler", userAgent);

		if(userAgent == null)
			return false;

		try {
			return UserAgentType.CRAWLER.getType().equalsIgnoreCase(userAgent.getAgentType());
		} catch (Throwable t) {
			this._log.error("Unable to determine whether {} is a crawler: {} [ {} ]. Returning 'FALSE'", userAgent, t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #userAgentString")
	public String getUserAgentAndVersion(String userAgentString) {
		this._log.debug("Attempting to get user agent details for {}", userAgentString);

		if(userAgentString == null)
			return null;

		try {
			return this.getUserAgentAndVersion(this.identify(userAgentString));
		} catch (Throwable t) {
			this._log.error("Unable to get user agent details for {}: {} [ {} ]", userAgentString, t.getClass().getSimpleName(), t.getMessage());

			return null;
		}
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #userAgent.userAgentString")
	public String getUserAgentAndVersion(UserAgentModel userAgent) {
		this._log.debug("Attempting to get user agent details for " + userAgent);

		if(userAgent == null)
			return null;

		try {
			return userAgent.getAgentName() +
				  (userAgent.getAgentVersion() == null ? "" : "/" + userAgent.getAgentVersion()) +
				  (userAgent.getAgentType() == null ? "" : " [ " + userAgent.getAgentType() + " ]");
		} catch (Throwable t) {
			this._log.error("Unable to get user agent details for " + userAgent, t);

			return null;
		}
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #userAgent.userAgentString")
	public Set<String> getIPAddressesFor(UserAgentModel userAgent) {
		if(this.canIdentifyIPs()) {
			Set<String> IPAddresses = new HashSet<String>();

			if(userAgent == null || userAgent.getUserAgentString() == null)
				return IPAddresses;

			try {
				IPAddresses = this.doProcessIPCheckResults(userAgent);

				if(IPAddresses != null)
					this._log.info("{} valid IPs found for user agent {}", IPAddresses.size(), userAgent.getUserAgentString());
				else
					this._log.info("No valid IPs found for user agent {}", userAgent.getUserAgentString());
			} catch(Throwable t) {
				this._log.error("Unable to fetch valid IP addresses for {}: {} [ {} ] ", userAgent.getUserAgentString(), t.getClass().getSimpleName(), t.getMessage());
			}


			this._log.info("{} valid source IPs found for {}", IPAddresses.size(), userAgent.getUserAgentString());

			return IPAddresses;
		}

		throw new UnsupportedOperationException("This User Agent Manager (" + this.getClass().getSimpleName() + ") has no capabilities to check IP addresses for a given user agent");
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #userAgentString")
	public Set<String> getIPAddressesFor(String userAgentString) throws Throwable {
		if(this.canIdentifyIPs()) {
			return this.getIPAddressesFor(this.identify(userAgentString));
		}

		throw new UnsupportedOperationException("This User Agent Manager (" + this.getClass().getSimpleName() + ") has no capabilities to check IP addresses for a given user agent");
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #userAgent.userAgentString + #IPAddress")
	public boolean checkIP(UserAgentModel userAgent, String IPAddress) {
		if(this.canIdentifyIPs()) {
			Set<String> IPAddresses = this.getIPAddressesFor(userAgent);

			if(IPAddresses == null || IPAddresses.isEmpty()) {
				this._log.warn("Either User Agent Manager {} does not list a range of valid IP addresses for {} or IP {} is invalid accordingly", this.getClass().getSimpleName(), userAgent.getUserAgentString(), IPAddress);

				return false;
			}

			return IPAddresses.contains(IPAddress);
		}

		throw new UnsupportedOperationException("This User Agent Manager (" + this.getClass().getSimpleName() + ") has no capabilities to check IP addresses for a given user agent");
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [ priority " + this.getPriority() + " ]";
 	}
}