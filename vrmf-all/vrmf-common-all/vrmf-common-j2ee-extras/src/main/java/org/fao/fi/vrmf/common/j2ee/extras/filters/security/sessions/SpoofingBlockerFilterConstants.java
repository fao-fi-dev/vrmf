/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.filters.security.sessions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Nov 2012
 */
public interface SpoofingBlockerFilterConstants {
	String INITIAL_IP_SESSION_ATTRIBUTE 		  = "initialIP";
	String INITIAL_IP_TIMESTAMP_SESSION_ATTRIBUTE = "initialIPTimestamp";
	
	String ALREADY_BLOCKED_SESSION_ATTRIBUTE	  = "alreadyBlocked";
}
