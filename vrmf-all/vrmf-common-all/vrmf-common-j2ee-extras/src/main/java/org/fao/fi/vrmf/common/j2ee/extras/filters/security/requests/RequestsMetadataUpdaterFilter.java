/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.filters.security.requests;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.BandwidthManager;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.ProcessedRequestData;
import org.fao.fi.vrmf.common.j2ee.filters.AbstractProcessingStatusWrapperFilter;
import org.fao.fi.vrmf.common.j2ee.filters.security.requests.RequestStatus;
import org.fao.fi.vrmf.common.j2ee.servlets.response.StatusExposingServletResponse;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2012
 */
@Named
@Singleton
public class RequestsMetadataUpdaterFilter extends AbstractProcessingStatusWrapperFilter<RequestStatus> {
	@Inject private BandwidthManager _bandwidthManager;
//	@Inject private PersistanceMetadataDAOImpl<Integer> _persistanceMetadataDAO;
//	
//	@Inject private RemoteRequestMetadataDAOImpl _remoteRequestMetadataDAO;
//	
//	/**
//	 * @return the 'persistanceMetadataDAO' value
//	 */
//	public PersistanceMetadataDAOImpl<Integer> getPersistanceMetadataDAO() {
//		return this._persistanceMetadataDAO;
//	}
//
//	/**
//	 * @param persistanceMetadataDAO the 'persistanceMetadataDAO' value to set
//	 */
//	public void setPersistanceMetadataDAO(PersistanceMetadataDAOImpl<Integer> persistanceMetadataDAO) {
//		this._persistanceMetadataDAO = persistanceMetadataDAO;
//	}
//
//	/**
//	 * @return the 'remoteRequestMetadataDAO' value
//	 */
//	public RemoteRequestMetadataDAOImpl getRemoteRequestMetadataDAO() {
//		return this._remoteRequestMetadataDAO;
//	}
//
//	/**
//	 * @param remoteRequestMetadataDAO the 'remoteRequestMetadataDAO' value to set
//	 */
//	public void setRemoteRequestMetadataDAO(RemoteRequestMetadataDAOImpl remoteRequestMetadataDAO) {
//		this._remoteRequestMetadataDAO = remoteRequestMetadataDAO;
//	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.web.common.filters.AbstractProcessingStatusWrapperFilter#beforeFiltering(javax.servlet.http.HttpServletRequest, org.fao.vrmf.utilities.web.common.servlets.response.StatusExposingServletResponse)
	 */
	@Override
	protected RequestStatus beforeFiltering(HttpServletRequest request, StatusExposingServletResponse response, Long filteringStart) {
		Integer lastRequestMetadataID = (Integer)request.getAttribute(RequestsLimiterFilterConstants.LAST_RECORD_ID_REQUEST_ATTRIBUTE);

		RequestStatus status = new RequestStatus(lastRequestMetadataID, filteringStart, null, null, null);

		this._log.debug("Before [ URI: " + request.getRequestURI() + " - LRMID: " + status.getRequestMetadataID() + " ] - Status: [ " + status + " ]");
		
		return status;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.web.common.filters.AbstractProcessingStatusWrapperFilter#afterFiltering(java.io.Serializable, javax.servlet.http.HttpServletRequest, org.fao.vrmf.utilities.web.common.servlets.response.StatusExposingServletResponse)
	 */
	@Override
	protected RequestStatus afterFiltering(RequestStatus status, HttpServletRequest request, StatusExposingServletResponse response) {
		Integer lastRequestMetadataID = (Integer)request.getAttribute(RequestsLimiterFilterConstants.LAST_RECORD_ID_REQUEST_ATTRIBUTE);
		
		if(lastRequestMetadataID != null)
			status.setRequestMetadataID(lastRequestMetadataID);
		
		this._log.debug("After [ URI: " + request.getRequestURI() + " - LRMID: " + status.getRequestMetadataID() + " ] - Status: [ " + status + " ]");

		return status;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.web.common.filters.AbstractProcessingStatusWrapperFilter#onFailure(java.io.Serializable, javax.servlet.http.HttpServletRequest, org.fao.vrmf.utilities.web.common.servlets.response.StatusExposingServletResponse, java.lang.Throwable)
	 */
	@Override
	protected RequestStatus onFailure(RequestStatus status, HttpServletRequest request, StatusExposingServletResponse response, Long filteringStart, Throwable t) {
		this._log.info("On Failure [ URI: " + request.getRequestURI() + " - LRMID: " + status.getRequestMetadataID() + " ] - Status: [ " + status + " ]");
		
		return status;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.web.common.filters.AbstractProcessingStatusWrapperFilter#completeFiltering(java.io.Serializable, javax.servlet.http.HttpServletRequest, org.fao.vrmf.utilities.web.common.servlets.response.StatusExposingServletResponse)
	 */
	@Override
	protected RequestStatus completeFiltering(RequestStatus status, HttpServletRequest request, StatusExposingServletResponse response) {
		if(status == null)
			return null;

		Integer lastRequestMetadataID = (Integer)request.getAttribute(RequestsLimiterFilterConstants.LAST_RECORD_ID_REQUEST_ATTRIBUTE);
		
		if(lastRequestMetadataID != null)
			status.setRequestMetadataID(lastRequestMetadataID);
		
		status.setRequestEndTimestamp(System.currentTimeMillis());
		status.setResponseCode(response.getStatus());
		status.setResponseMessage(response.getStatusMessage());
		
		ProcessedRequestData requestMetadata;
		
		if(status.getRequestMetadataID() != null) {
			try {
				requestMetadata = this._bandwidthManager.getLogById(status.getRequestMetadataID().intValue());
				
				Long elapsed = status.getRequestEndTimestamp() - status.getRequestStartTimestamp();
				
				requestMetadata.setElapsed(elapsed.intValue());
				
				if(response.getStatus() != 0)
					requestMetadata.setStatusCode(response.getStatus());
				
				if(response.getStatusMessage() != null)
					requestMetadata.setComment(response.getStatusMessage());
				
				if(requestMetadata.getSessionId() == null) {
					HttpSession session = request.getSession(false);
					
					if(session != null)
						requestMetadata.setSessionId(session.getId());
				}

				this._bandwidthManager.log(requestMetadata);
			} catch (Throwable t) {
				this._log.error("Unable to update last remote request metadata record #" + status.getRequestMetadataID(), t);
			} 
		} else {
			this._log.debug("Unable to retrieve last remote request metadata record: last record ID request attribute (" + RequestsLimiterFilterConstants.LAST_RECORD_ID_REQUEST_ATTRIBUTE + ") is NULL");
		}

		this._log.debug("Complete [ URI: " + request.getRequestURI() + " - LRMID: " + status.getRequestMetadataID() + " ] - Status: [ " + status + " ]");
		
		return status;
	}
}
