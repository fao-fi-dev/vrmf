/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.providers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentModel;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentType;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker;

import flexjson.JSONDeserializer;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Nov 2012
 */
@Named("vrmf.core.manager.ua.uas")
@Singleton
public class UASUserAgentChecker extends UserAgentChecker {
	static final private String IDENTIFICATION_SERVICE_URL = "http://www.useragentstring.com/?uas={query}&getJSON=all";
	static final private String IP_IDENTIFICATION_SERVICE_URL = "http://www.useragentstring.com/index.php?uas={query}";

	static final private Pattern IP_IDENTIFICATION_PATTERN = Pattern.compile("([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})");

	/**
	 * Class constructor.
	 */
	public UASUserAgentChecker() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#getPriority()
	 */
	@Override
	public long getPriority() {
		return PRIORITY_HIGH;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#canIdentifyIPs()
	 */
	@Override
	public boolean canIdentifyIPs() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#getExpectedResponseContentType()
	 */
	@Override
	public String[] getExpectedResponseContentType() {
		return UserAgentChecker.JSON_CONTENT_TYPES;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#doInvokeIdentificationServiceAPI(java.lang.String, int)
	 */
	@Override
	protected HTTPRequestMetadata doInvokeIdentificationServiceAPI(String userAgentString, int timeout) throws ConnectTimeoutException, IOException {
		return this._helper.fetchURLAsBytes(IDENTIFICATION_SERVICE_URL.replace("{query}", userAgentString), timeout);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#doInvokeIPCheckServiceAPI(java.lang.String, int)
	 */
	@Override
	protected HTTPRequestMetadata doInvokeIPCheckServiceAPI(String userAgentString, int timeout) throws ConnectTimeoutException, IOException {
		return this._helper.fetchURLAsBytes(IP_IDENTIFICATION_SERVICE_URL.replace("{query}", userAgentString), timeout);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#doProcessIdentificationResults(org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentModel, org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata)
	 */
	@Override
	protected UserAgentModel doProcessIdentificationResults(UserAgentModel agent, HTTPRequestMetadata response) throws UnsupportedEncodingException {
		String json = new String(response.getContent(), "UTF-8");

		Map<String, String> map = new JSONDeserializer<Map<String, String>>().deserialize(json, Map.class);

		agent.setAgentName(map.get("agent_name"));
		agent.setAgentVersion(map.get("agent_version"));

		String agentType = map.get("agent_type");

		if("Crawler".equalsIgnoreCase(agentType))
			agent.setAgentType(UserAgentType.CRAWLER.getType());
		else if("Browser".equalsIgnoreCase(agentType))
			agent.setAgentType(UserAgentType.BROWSER.getType());
		else if("Librarie".equalsIgnoreCase(agentType))
			agent.setAgentType(UserAgentType.LIBRARY.getType());
		else if("Other".equalsIgnoreCase(agentType))
			agent.setAgentType(UserAgentType.OTHER.getType());
		else
			agent.setAgentType(UserAgentType.UNKNOWN.getType());
		return agent;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#doProcessIPCheckResults(org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentModel)
	 */
	@Override
	public Set<String> doProcessIPCheckResults(UserAgentModel userAgent) {
		Set<String> IPAddresses = new HashSet<String>();

		if(userAgent == null || userAgent.getUserAgentString() == null)
			return IPAddresses;

		String userAgentString = userAgent.getUserAgentString();

		try {
			String content = this._helper.fetchHTMLPageAsUTF8String(new HttpPost(), IP_IDENTIFICATION_SERVICE_URL.replace("{query}", userAgentString));

			Matcher matcher = IP_IDENTIFICATION_PATTERN.matcher(content);

			while(matcher.find()) {
				IPAddresses.add(matcher.group(1));
			}

			this._log.info("{} valid IPs found for User Agent {} according to {}", IPAddresses.size(), userAgentString, this.getClass().getSimpleName());
		} catch(Throwable t) {
			this._log.error("Unable to fetch valid IP addresses for User Agent {}: {} [ {} ]", userAgentString, t.getClass().getSimpleName(), t.getMessage());
		}

		return IPAddresses;
	}
}