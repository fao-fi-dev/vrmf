/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.filters.content;

import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.webutilities.filters.YUIMinFilter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Nov 2012
 */
public class ConditionalMinifierFilter extends YUIMinFilter {
	private Logger _log = LoggerFactory.getLogger(this.getClass());
	
	private Boolean _applyFilter = Boolean.FALSE;
	
	/* (non-Javadoc)
	 * @see com.googlecode.webutilities.filters.YUIMinFilter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if(this._applyFilter)
			super.doFilter(request, response, chain);
		else
			chain.doFilter(request, response);
	}

	/* (non-Javadoc)
	 * @see com.googlecode.webutilities.filters.YUIMinFilter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {
		super.init(config);

		try {
			Context initCtx = new InitialContext(); 
			Context envCtx = (Context) initCtx.lookup("java:comp/env"); 
			
			this._applyFilter = Boolean.parseBoolean((String)envCtx.lookup("yui/minimizer/enabled"));
			
			this._log.info("YUI css + js minifier is " + ( this._applyFilter ? "enabled" : "disabled").toUpperCase());
		} catch (Throwable t) {
			this._log.error("Unable to check whether the YUI css + js minimizer is enabled", t);
		}
	}
}
