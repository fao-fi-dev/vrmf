/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 May 2013
 */
@Named
@Singleton
final public class UserAgentManager extends AbstractLoggingAwareClient {
	class UserAgentCheckerPrioritizer implements Comparator<UserAgentChecker> {
		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(UserAgentChecker firstChecker, UserAgentChecker secondChecker) {
			return new Long(secondChecker.getPriority()).compareTo(new Long(firstChecker.getPriority()));
		}
	}

	@Inject private List<UserAgentChecker> _userAgentCheckers;

	private UserAgentChecker _defaultChecker;

	private int _connectionTimeout = 1000; // 1 second
	private double _executionSchedulerDelayMultiplier = 300; //Total delay for checker thread would be: # of registered checker * connection timeout * multiplier (mS), that is roughly 900 seconds (having 3 checkers)

	private ScheduledFuture<?> _poller;
	
	/**
	 * @return the 'connectionTimeout' value
	 */
	public int getConnectionTimeout() {
		return this._connectionTimeout;
	}

	/**
	 * @param connectionTimeout the 'connectionTimeout' value to set
	 */
	public void setConnectionTimeout(int connectionTimeout) {
		this._connectionTimeout = connectionTimeout;
	}

	/**
	 * @return the 'executionSchedulerDelayMultiplier' value
	 */
	public double getExecutionSchedulerDelayMultiplier() {
		return this._executionSchedulerDelayMultiplier;
	}

	/**
	 * @param executionSchedulerDelayMultiplier the 'executionSchedulerDelayMultiplier' value to set
	 */
	public void setExecutionSchedulerDelayMultiplier(double executionSchedulerDelayMultiplier) {
		this._executionSchedulerDelayMultiplier = executionSchedulerDelayMultiplier;
	}

	/**
	 * @return the 'poller' value
	 */
	protected ScheduledFuture<?> getPoller() {
		return this._poller;
	}

	/**
	 * @param poller the 'poller' value to set
	 */
	protected void setPoller(ScheduledFuture<?> poller) {
		this._poller = poller;
	}

	/**
	 * @return the 'userAgentCheckers' value
	 */
	public List<UserAgentChecker> getUserAgentCheckers() {
		return this._userAgentCheckers;
	}

	/**
	 * @param userAgentCheckers the 'userAgentCheckers' value to set
	 */
	public void setUserAgentCheckers(List<UserAgentChecker> userAgentCheckers) {
		this._userAgentCheckers = userAgentCheckers;
	}

	/**
	 * @return the 'defaultChecker' value
	 */
	public UserAgentChecker getDefaultChecker() {
		return this._defaultChecker;
	}

	public UserAgentChecker getFirstValidUserAgentChecker() {
		return this.getFirstValidUserAgentCheckerOfType(UserAgentChecker.class);
	}

	public UserAgentChecker getFirstValidUserAgentCheckerOfType(Class<? extends UserAgentChecker> type) {
		if(this._userAgentCheckers != null && !this._userAgentCheckers.isEmpty()) {
			this._log.info("{} User Agent checkers registered: scanning for valid ones of type {}...", this._userAgentCheckers.size(), type.getSimpleName());

			int counter = 1;

			for(UserAgentChecker checker : this._userAgentCheckers) {
				this._log.info(" #{}: {}", counter++, checker);
			}

			for(UserAgentChecker checker : this._userAgentCheckers) {
				if(checker != null) {
					if(type.isAssignableFrom(checker.getClass())) {
						this._log.info("Verifying whether checker of type {} is currently available or not...", checker.getClass().getSimpleName());

						if(checker.isAvailable(this._connectionTimeout)) {
							this._log.info("User Agent checker {} is available: setting it as default checker until further notice", checker);

							return checker;
						} else {
							this._log.warn("User Agent checker {} is not available", checker);
						}
					} else {
						this._log.info("Skipping User Agent checker {} as it is not of type {}", checker, type.getSimpleName());
					}

				} else
					this._log.warn("A NULL User Agent checker was detected among the list of available User Agent checkers...");
			}
		} else {
			this._log.warn("No User Agent checkers are currently registered...");
		}

		return null;
	}

	public void refresh() throws Exception {
		if(this._userAgentCheckers != null)
			Collections.sort(this._userAgentCheckers, new UserAgentCheckerPrioritizer());

		UserAgentChecker checker = this.getFirstValidUserAgentChecker();

		if(checker.equals(this._defaultChecker)) {
			this._log.debug("Default checker is unchanged ({})", this._defaultChecker.getClass().getSimpleName());
		} else {
			this._log.info("Default checker has changed from {} to {}", 
							this._defaultChecker == null ? "NOT SET" : this._defaultChecker.getClass().getSimpleName(), 
							checker.getClass().getSimpleName());
		}

		this._defaultChecker = checker;
	}

	private ScheduledFuture<?> startPoller() {
		final long $delay = Math.round(this._userAgentCheckers.size() * this._connectionTimeout * this._executionSchedulerDelayMultiplier);

		final UserAgentManager $this = this;
		final Logger $log = LoggerFactory.getLogger("org.fao.vrmf.executors");

		return Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					long end, start = System.currentTimeMillis();
					$log.warn("Starting User Agent checker polling with thread {} [ delay: {} ]...", Thread.currentThread().getName(), $delay);
					$this.refresh();
					end = System.currentTimeMillis();
					$log.warn("Completed User Agent checker polling with thread {} in {} mSec. Identified default User Agent checker is {}",
							  Thread.currentThread().getName(),
							  end - start,
							  $this._defaultChecker == null ? "NULL" : $this._defaultChecker.getClass().getSimpleName());
				} catch (Throwable t) {
					$log.error("An error occurred while executing scheduled User Agent checker polling: {} [ {} ]", t.getClass().getSimpleName(), t.getMessage());
				}

			}
		}, $delay, $delay, TimeUnit.MILLISECONDS);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.beans.lifecycle.Initializable#init()
	 */
	@PostConstruct public void init() throws Exception {
		this.refresh();

		this._poller = this.startPoller();
	}
}