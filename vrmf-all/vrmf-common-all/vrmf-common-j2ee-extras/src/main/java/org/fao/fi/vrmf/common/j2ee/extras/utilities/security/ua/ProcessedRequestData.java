/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 Jan 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 Jan 2014
 */
public class ProcessedRequestData extends IncomingRequestData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8618167292621796491L;
	
	private Boolean blocked;
	private Integer elapsed;
	private Integer statusCode;

	/**
	 * @return the 'blocked' value
	 */
	public Boolean getBlocked() {
		return this.blocked;
	}

	/**
	 * @param blocked the 'blocked' value to set
	 */
	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}

	/**
	 * @return the 'elapsed' value
	 */
	public Integer getElapsed() {
		return this.elapsed;
	}

	/**
	 * @param elapsed the 'elapsed' value to set
	 */
	public void setElapsed(Integer elapsed) {
		this.elapsed = elapsed;
	}

	/**
	 * @return the 'statusCode' value
	 */
	public Integer getStatusCode() {
		return this.statusCode;
	}

	/**
	 * @param statusCode the 'statusCode' value to set
	 */
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
}