/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 May 2013
 */
public class UserAgentModel extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -9043030744629115719L;

	private String _userAgentString;

	private String _agentType;
	private String _agentName;
	private String _agentVersion;
	private String _osType;
	private String _osName;
	private String _osVersionName;
	private String _osVersionNumber;
	private String _osProducer;
	private String _osProducerURL;
	private String _linuxDistibution;
	private String _agentLanguage;
	private String _agentLanguageTag;

	/**
	 * Class constructor
	 *
	 */
	public UserAgentModel() {
		this._agentType = UserAgentType.UNKNOWN.getType();
	}

	/**
	 * Class constructor
	 *
	 */
	public UserAgentModel(String userAgentString) {
		this();

		this._userAgentString = userAgentString;
	}

	/**
	 * @return the '_userAgentString' value
	 */
	public String getUserAgentString() {
		return this._userAgentString;
	}
	/**
	 * @param _userAgentString the '_userAgentString' value to set
	 */
	public void setUserAgentString(String user_agent_string) {
		this._userAgentString = user_agent_string;
	}
	/**
	 * @return the '_agentType' value
	 */
	public String getAgentType() {
		return this._agentType;
	}
	/**
	 * @param _agentType the '_agentType' value to set
	 */
	public void setAgentType(String agent_type) {
		this._agentType = agent_type;
	}
	/**
	 * @return the '_agentName' value
	 */
	public String getAgentName() {
		return this._agentName;
	}
	/**
	 * @param _agentName the '_agentName' value to set
	 */
	public void setAgentName(String agent_name) {
		this._agentName = agent_name;
	}
	/**
	 * @return the '_agentVersion' value
	 */
	public String getAgentVersion() {
		return this._agentVersion;
	}
	/**
	 * @param _agentVersion the '_agentVersion' value to set
	 */
	public void setAgentVersion(String agent_version) {
		this._agentVersion = agent_version;
	}
	/**
	 * @return the '_osType' value
	 */
	public String getOsType() {
		return this._osType;
	}
	/**
	 * @param _osType the '_osType' value to set
	 */
	public void setOsType(String os_type) {
		this._osType = os_type;
	}
	/**
	 * @return the '_osName' value
	 */
	public String getOsName() {
		return this._osName;
	}
	/**
	 * @param _osName the '_osName' value to set
	 */
	public void setOsName(String os_name) {
		this._osName = os_name;
	}
	/**
	 * @return the '_osVersionName' value
	 */
	public String getOsVersionName() {
		return this._osVersionName;
	}
	/**
	 * @param _osVersionName the '_osVersionName' value to set
	 */
	public void setOsVersionName(String os_versionName) {
		this._osVersionName = os_versionName;
	}
	/**
	 * @return the '_osVersionNumber' value
	 */
	public String getOsVersionNumber() {
		return this._osVersionNumber;
	}
	/**
	 * @param _osVersionNumber the '_osVersionNumber' value to set
	 */
	public void setOsVersionNumber(String os_versionNumber) {
		this._osVersionNumber = os_versionNumber;
	}
	/**
	 * @return the '_osProducer' value
	 */
	public String getOsProducer() {
		return this._osProducer;
	}
	/**
	 * @param _osProducer the '_osProducer' value to set
	 */
	public void setOsProducer(String os_producer) {
		this._osProducer = os_producer;
	}
	/**
	 * @return the '_osProducerURL' value
	 */
	public String getOsProducerURL() {
		return this._osProducerURL;
	}
	/**
	 * @param _osProducerURL the '_osProducerURL' value to set
	 */
	public void setOsProducerURL(String os_producerURL) {
		this._osProducerURL = os_producerURL;
	}
	/**
	 * @return the '_linuxDistibution' value
	 */
	public String getLinuxDistibution() {
		return this._linuxDistibution;
	}
	/**
	 * @param _linuxDistibution the '_linuxDistibution' value to set
	 */
	public void setLinuxDistibution(String linux_distibution) {
		this._linuxDistibution = linux_distibution;
	}
	/**
	 * @return the '_agentLanguage' value
	 */
	public String getAgentLanguage() {
		return this._agentLanguage;
	}
	/**
	 * @param _agentLanguage the '_agentLanguage' value to set
	 */
	public void setAgentLanguage(String agent_language) {
		this._agentLanguage = agent_language;
	}
	/**
	 * @return the '_agentLanguageTag' value
	 */
	public String getAgentLanguageTag() {
		return this._agentLanguageTag;
	}
	/**
	 * @param _agentLanguageTag the '_agentLanguageTag' value to set
	 */
	public void setAgentLanguageTag(String agent_languageTag) {
		this._agentLanguageTag = agent_languageTag;
	}

	@Override
	public String toString() {
		return this._userAgentString + " [ " + this._agentName + ", " + this._agentVersion + ", " + this._agentType + " ]";
	}
}
