/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.filters.security.requests;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.j2ee.filters.security.requests.AbstractLoggedUserFilter;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.OutputEncodingConstants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Mar 2011
 */
@Named
@Singleton
public class LoggedUserJSONFilter extends AbstractLoggedUserFilter {
	private OutputStreamWriter getOutputStreamWriter(HttpServletRequest request, HttpServletResponse response) throws IOException {
		return new OutputStreamWriter(response.getOutputStream(), OutputEncodingConstants.DEFAULT_CHARSET);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.web.common.filters.AbstractLoggedUserFilter#manageMissingLoggedUser(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	protected void manageMissingLoggedUser(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException {
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "This service requires an authenticated user session to be accessed");
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.web.common.filters.AbstractLoggedUserFilter#manageMissingRoles(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	protected void manageMissingRoles(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException {
		OutputStreamWriter osw = this.getOutputStreamWriter(request, response);
		
		try {
			JSONResponse<Serializable> JSONResponse = new JSONResponse<Serializable>();
			JSONResponse.setError(true);
			JSONResponse.setErrorMessage("The currently logged user doesn't belong to the necessary roles set to access this service");
			
			osw.write(JSONResponse.JSONify());
		} finally {
			if(osw != null) {
				osw.flush();
				osw.close();
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.web.common.filters.AbstractLoggedUserFilter#manageMissingCapabilities(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	protected void manageMissingCapabilities(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException {
		OutputStreamWriter osw = this.getOutputStreamWriter(request, response);
		
		try {
			JSONResponse<Serializable> JSONResponse = new JSONResponse<Serializable>();
			JSONResponse.setError(true);
			JSONResponse.setErrorMessage("The currently logged user doesn't have the necessary capabilities to access this service");
			
			osw.write(JSONResponse.JSONify());
		} finally {
			if(osw != null) {
				osw.flush();
				osw.close();
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.web.common.filters.AbstractLoggedUserFilter#currentDoFilter(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	protected void currentDoFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		filterChain.doFilter(request, response);
	}
}