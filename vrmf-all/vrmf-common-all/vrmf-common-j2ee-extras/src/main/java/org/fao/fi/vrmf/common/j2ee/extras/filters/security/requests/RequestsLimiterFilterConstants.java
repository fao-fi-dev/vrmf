/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.filters.security.requests;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Mar 2011
 */
public interface RequestsLimiterFilterConstants {
	String LOG_INTERNAL_REQUESTS_JNDI_RESOURCE   = "security/requests/internal/log"; 
	
	String APPLICATION_ID_CONFIGURATION_PARAMETER		= "applicationId";
	String INTERNAL_IPS_PATTERN_CONFIGURATION_PARAMETER	= "internalIPsPattern";
	
	String LAST_RECORD_ID_REQUEST_ATTRIBUTE  = "lastRecordId";
}
