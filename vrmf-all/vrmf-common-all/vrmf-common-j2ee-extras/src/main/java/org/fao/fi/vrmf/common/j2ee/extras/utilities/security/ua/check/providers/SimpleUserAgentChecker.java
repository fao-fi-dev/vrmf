/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.providers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.http.conn.ConnectTimeoutException;
import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentModel;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentType;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Nov 2012
 */
@Named("vrmf.core.manager.ua.simple")
@Singleton
public class SimpleUserAgentChecker extends UserAgentChecker {
	final private Set<String> NO_IP_ADDRESSES = new TreeSet<String>();
	
	/**
	 * Class constructor.
	 */
	public SimpleUserAgentChecker() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#getPriority()
	 */
	@Override
	public long getPriority() {
		return PRIORITY_LOW;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#canIdentifyIPs()
	 */
	@Override
	public boolean canIdentifyIPs() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#getExpectedResponseContentType()
	 */
	@Override
	public String[] getExpectedResponseContentType() {
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#doProcessIPCheckResults(org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentModel)
	 */
	@Override
	protected Set<String> doProcessIPCheckResults(UserAgentModel userAgent) {
		return NO_IP_ADDRESSES;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#invokeIdentificationService(java.lang.String, int)
	 */
	public HTTPRequestMetadata invokeIdentificationService(String userAgentString, int timeout) {
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#doInvokeIdentificationServiceAPI(java.lang.String, int)
	 */
	@Override
	protected HTTPRequestMetadata doInvokeIdentificationServiceAPI(String userAgentString, int timeout) throws ConnectTimeoutException, IOException {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#doInvokeIPCheckServiceAPI(java.lang.String, int)
	 */
	@Override
	protected HTTPRequestMetadata doInvokeIPCheckServiceAPI(String userAgentString, int timeout) throws ConnectTimeoutException, IOException {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker#doProcessIdentificationResults(org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentModel, org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata)
	 */
	@Override
	protected UserAgentModel doProcessIdentificationResults(UserAgentModel agent, HTTPRequestMetadata response) throws UnsupportedEncodingException {
		if(agent != null && agent.getUserAgentString() != null) {
			String userAgentString = agent.getUserAgentString();
			
			boolean isCrawler = userAgentString.indexOf("Googlebot/") >= 0 ||
								userAgentString.indexOf("/msnbot.htm") >= 0 ||
								userAgentString.indexOf("bingbot/") >= 0;
								
			boolean isLibrary = userAgentString.indexOf("facebookexternalhit/") >= 0 ||
								userAgentString.indexOf("Java/") >= 0;
								
			if(isCrawler)
				agent.setAgentType(UserAgentType.CRAWLER.getType());
			else if(isLibrary)
				agent.setAgentType(UserAgentType.LIBRARY.getType());
			else
				agent.setAgentType(UserAgentType.BROWSER.getType());
		}
		
		return agent;
	}
}