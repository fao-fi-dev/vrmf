/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 May 2013
 */
public enum UserAgentType {
	UNKNOWN("unknown"),
	CRAWLER("crawler"),
	LIBRARY("library"),
	MAIL_CLIENT("mail.client"),
	ANONIMIZER("anonymizer"),
	BROWSER("browser"),
	OTHER("other");
	
	String _type;
	
	private UserAgentType() {
		this._type = "unknown";
	}
	
	private UserAgentType(String type) {
		this._type = type;
	}
	
	public String getType() {
		return this._type;
	}
}