/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.utilities.security;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 Jan 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 Jan 2014
 */
public class ProcessedRequestsFilter extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3276429447776240204L;
	
	private String _applicationID;
	private String _requestSource;
	private String _loggedUser;
	private long _timestampFrom;
	private long _timestampTo;

	/**
	 * @return the 'applicationID' value
	 */
	public String getApplicationID() {
		return this._applicationID;
	}

	/**
	 * @param applicationID the 'applicationID' value to set
	 */
	public void setApplicationID(String applicationID) {
		this._applicationID = applicationID;
	}

	/**
	 * @return the 'requestSource' value
	 */
	public String getRequestSource() {
		return this._requestSource;
	}

	/**
	 * @param requestSource the 'requestSource' value to set
	 */
	public void setRequestSource(String requestSource) {
		this._requestSource = requestSource;
	}

	/**
	 * @return the 'loggedUser' value
	 */
	public String getLoggedUser() {
		return this._loggedUser;
	}

	/**
	 * @param loggedUser the 'loggedUser' value to set
	 */
	public void setLoggedUser(String loggedUser) {
		this._loggedUser = loggedUser;
	}

	/**
	 * @return the 'timestampFrom' value
	 */
	public long getTimestampFrom() {
		return this._timestampFrom;
	}

	/**
	 * @param timestampFrom the 'timestampFrom' value to set
	 */
	public void setTimestampFrom(long timestampFrom) {
		this._timestampFrom = timestampFrom;
	}

	/**
	 * @return the 'timestampTo' value
	 */
	public long getTimestampTo() {
		return this._timestampTo;
	}

	/**
	 * @param timestampTo the 'timestampTo' value to set
	 */
	public void setTimestampTo(long timestampTo) {
		this._timestampTo = timestampTo;
	}
}
