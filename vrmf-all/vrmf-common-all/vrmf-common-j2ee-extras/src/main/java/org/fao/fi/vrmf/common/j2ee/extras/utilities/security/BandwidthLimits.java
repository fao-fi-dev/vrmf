/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.utilities.security;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 Jan 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 Jan 2014
 */
public class BandwidthLimits {
	private String ipFilter;

	private String applicationId;

	private Integer maxPerSecond;
	
	private Integer maxPerMinute;

	private Integer maxPerHour;

	private Integer maxPerDay;
	
	/**
	 * Class constructor
	 *
	 */
	public BandwidthLimits() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param ipFilter
	 * @param applicationId
	 * @param maxPerSecond
	 * @param maxPerMinute
	 * @param maxPerHour
	 * @param maxPerDay
	 */
	public BandwidthLimits(String ipFilter, String applicationId, Integer maxPerSecond, Integer maxPerMinute, Integer maxPerHour, Integer maxPerDay) {
		super();
		this.ipFilter = ipFilter;
		this.applicationId = applicationId;
		this.maxPerSecond = maxPerSecond;
		this.maxPerMinute = maxPerMinute;
		this.maxPerHour = maxPerHour;
		this.maxPerDay = maxPerDay;
	}

	/**
	 * @return the 'ipFilter' value
	 */
	public String getIpFilter() {
		return this.ipFilter;
	}

	/**
	 * @param ipFilter the 'ipFilter' value to set
	 */
	public void setIpFilter(String ipFilter) {
		this.ipFilter = ipFilter;
	}

	/**
	 * @return the 'applicationId' value
	 */
	public String getApplicationId() {
		return this.applicationId;
	}

	/**
	 * @param applicationId the 'applicationId' value to set
	 */
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * @return the 'maxPerSecond' value
	 */
	public Integer getMaxPerSecond() {
		return this.maxPerSecond;
	}

	/**
	 * @param maxPerSecond the 'maxPerSecond' value to set
	 */
	public void setMaxPerSecond(Integer maxPerSecond) {
		this.maxPerSecond = maxPerSecond;
	}

	/**
	 * @return the 'maxPerMinute' value
	 */
	public Integer getMaxPerMinute() {
		return this.maxPerMinute;
	}

	/**
	 * @param maxPerMinute the 'maxPerMinute' value to set
	 */
	public void setMaxPerMinute(Integer maxPerMinute) {
		this.maxPerMinute = maxPerMinute;
	}

	/**
	 * @return the 'maxPerHour' value
	 */
	public Integer getMaxPerHour() {
		return this.maxPerHour;
	}

	/**
	 * @param maxPerHour the 'maxPerHour' value to set
	 */
	public void setMaxPerHour(Integer maxPerHour) {
		this.maxPerHour = maxPerHour;
	}

	/**
	 * @return the 'maxPerDay' value
	 */
	public Integer getMaxPerDay() {
		return this.maxPerDay;
	}

	/**
	 * @param maxPerDay the 'maxPerDay' value to set
	 */
	public void setMaxPerDay(Integer maxPerDay) {
		this.maxPerDay = maxPerDay;
	}
}
