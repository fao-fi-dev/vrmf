/**
 * (c) 2013 FAO / UN (project: vrmf-core-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.test.utilities.configuration.support;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Mar 2013
 */
public class Bean {
	private String _value;

	/**
	 * @return the 'value' value
	 */
	public String getValue() {
		return this._value;
	}

	/**
	 * @param value the 'value' value to set
	 */
	public void setValue(String value) {
		this._value = value;
	}
}
