/**
 * (c) 2013 FAO / UN (project: vrmf-core-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.test.utilities;

import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentModel;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentType;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16/mar/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 16/mar/2013
 */
public class UASUserAgentCheckerTest extends UserAgentCheckerTest {
	private boolean isAvailable() {
		return this.isAvailable(10000);
	}

	private boolean isAvailable(int timeout) {
		return UAC_UAS.isAvailable(timeout);
	}

	@Test
	public void testAvailability() throws Throwable {
		Assert.assertTrue(this.isAvailable());
	}

	@Test
	public void testIPCheck() throws Throwable {
		Assert.assertTrue(UAC_UAS.canIdentifyIPs());
	}

	@Test
	public void testBrowserDetection() throws Throwable {
		if(this.isAvailable())
			Assert.assertEquals(UserAgentType.BROWSER.getType(), UAC_UAS.identify(BROWSER_UA_STRING).getAgentType());
	}

	@Test
	public void testCrawlerDetection() throws Throwable {
		if(this.isAvailable()) {
			Assert.assertEquals(UserAgentType.CRAWLER.getType(), UAC_UAS.identify(CRAWLER_UA_STRING).getAgentType());
			Assert.assertEquals("bingbot", UAC_UAS.identify(CRAWLER_UA_STRING).getAgentName().toLowerCase());
		}
	}

	@Test
	public void testCrawlerValidIPDetection() throws Throwable {
		if(this.isAvailable()) {
			UserAgentModel crawler = UAC_UAS.identify(CRAWLER_UA_STRING);

			Assert.assertEquals("bingbot", crawler == null || crawler.getAgentName() == null ? null : crawler.getAgentName().toLowerCase());
			Assert.assertTrue(UAC_UAS.checkIP(crawler, VALID_CRAWLER_IP));
		}
	}

	@Test
	public void testCrawlerInvalidIPDetection() throws Throwable {
		if(this.isAvailable()) {
			UserAgentModel crawler = UAC_UAS.identify(CRAWLER_UA_STRING);

			Assert.assertEquals("bingbot", crawler == null || crawler.getAgentName() == null ? null : crawler.getAgentName().toLowerCase());
			Assert.assertFalse(UAC_UAS.checkIP(crawler, INVALID_CRAWLER_IP));
		}
	}

	@Test
	public void testFakeUADetection() throws Throwable {
		if(this.isAvailable())
			Assert.assertEquals(UserAgentType.UNKNOWN.getType(), UAC_UAS.identify(FAKE_UA_STRING).getAgentType());
	}
}
