/**
 * (c) 2013 FAO / UN (project: vrmf-core-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.test.utilities;

import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.UserAgentChecker;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.providers.SimpleUserAgentChecker;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.check.providers.UASUserAgentChecker;
import org.junit.BeforeClass;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16/mar/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 16/mar/2013
 */
public class UserAgentCheckerTest {
	protected String FAKE_UA_STRING = "FooBaz/6.66";
	protected String BROWSER_UA_STRING = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; GTB7.4; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET4.0C; .NET4.0E; OfficeLiveConnector.1.5; OfficeLivePatch.1.3; InfoPath.3; MS-RTC LM 8)";
	protected String CRAWLER_UA_STRING = "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)";
	protected String VALID_CRAWLER_IP  = "131.253.46.114";
	protected String INVALID_CRAWLER_IP= "231.153.146.14";
	
	static protected UserAgentChecker UAC_SIMPLE = new SimpleUserAgentChecker();
	static protected UserAgentChecker UAC_UAS = new UASUserAgentChecker();
	
	@BeforeClass
	static public void initialize() {
		HTTPHelper helper = new HTTPHelper(5000);
		
		UAC_SIMPLE.setHelper(helper);
		UAC_UAS.setHelper(helper);
	}
}
