/**
 * (c) 2013 FAO / UN (project: vrmf-core-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.test.utilities;

import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.UserAgentType;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16/mar/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 16/mar/2013
 */
public class SimpleUserAgentCheckerTest extends UserAgentCheckerTest {
	@Test
	public void testAvailability() throws Throwable {
		Assert.assertTrue(UAC_SIMPLE.isAvailable());
	}
	
	@Test
	public void testIPCheck() throws Throwable {
		Assert.assertFalse(UAC_SIMPLE.canIdentifyIPs());
	}
	
	@Test
	public void testBrowserDetection() throws Throwable {
		Assert.assertEquals(UserAgentType.BROWSER.getType(), UAC_SIMPLE.identify(BROWSER_UA_STRING).getAgentType());
	}
	
	@Test
	public void testCrawlerDetection() throws Throwable {
		Assert.assertEquals(UserAgentType.CRAWLER.getType(), UAC_SIMPLE.identify(CRAWLER_UA_STRING).getAgentType());
	}
	
	/** The Simple UA Checker can only discern between a few crawlers and a few libraries: any other UA string maps to 'Browser' */
	@Test	
	public void testFakeUADetection() throws Throwable {
		Assert.assertEquals(UserAgentType.BROWSER.getType(), UAC_SIMPLE.identify(FAKE_UA_STRING).getAgentType());
	}
}