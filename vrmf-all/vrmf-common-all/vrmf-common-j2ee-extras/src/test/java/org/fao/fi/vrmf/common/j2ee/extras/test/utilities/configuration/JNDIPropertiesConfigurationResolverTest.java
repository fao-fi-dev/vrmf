/**
 * (c) 2013 FAO / UN (project: vrmf-core-j2ee-extras)
 */
package org.fao.fi.vrmf.common.j2ee.extras.test.utilities.configuration;

import javax.inject.Inject;
import javax.inject.Named;

import org.fao.fi.vrmf.common.j2ee.extras.test.utilities.configuration.support.Bean;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Mar 2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/config/spring/test/vrmf-common-j2ee-extras-test-spring-context.xml" })
public class JNDIPropertiesConfigurationResolverTest {
	@Inject @Named("beanA") private Bean A;
	@Inject @Named("beanB") private Bean B;
	
	@Test public void testResolver() {
		Assert.assertEquals("steak/house", A.getValue());
		Assert.assertEquals("steak/hats", B.getValue());
	}
}
