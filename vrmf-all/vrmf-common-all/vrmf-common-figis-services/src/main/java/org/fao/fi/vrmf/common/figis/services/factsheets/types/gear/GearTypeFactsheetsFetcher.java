/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-figis-services)
 */
package org.fao.fi.vrmf.common.figis.services.factsheets.types.gear;

import java.io.ByteArrayInputStream;
import java.util.Collection;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.common.figis.models.factsheets.types.gear.GearTypeFactsheetData;
import org.fao.fi.vrmf.common.figis.services.factsheets.AbstractFilterableFactsheetFetcher;
import org.springframework.cache.annotation.Cacheable;
import org.w3c.dom.Document;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Sep 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Sep 2012
 */
@Named @Singleton
public class GearTypeFactsheetsFetcher extends AbstractFilterableFactsheetFetcher<GearTypeFactsheetData> {
	static final protected String DEFAULT_METHODS_INVOCATION_CACHE_ID = COMMON_METHODS_INVOCATION_CACHE_ID + ".type.gear";

	static final private String IDENT_ELEM = "GeartypeIdent";
	
	static final private String TITLE_XPATH = "//fi:" + IDENT_ELEM + "/dc:Title[@xml:lang='en']";
	static final private String ALPHA_3_XPATH = "//fi:" + IDENT_ELEM + "/fi:ISSCFGCode[@Type='3-alpha']";
	static final private String ISSCFG_CODE_XPATH = "//fi:" + IDENT_ELEM + "/fi:ISSCFGCode[@Type='Num']";
	
	private final XPathExpression TITLE_XPATH_EXPR;
	private final XPathExpression ALPHA_3_XPATH_EXPR;
	private final XPathExpression ISSCFG_CODE_XPATH_EXPR;
	
	public GearTypeFactsheetsFetcher() throws Throwable {
		super(IDENT_ELEM);
		
		this.TITLE_XPATH_EXPR = this.getXPath().compile(TITLE_XPATH);
		this.ALPHA_3_XPATH_EXPR = this.getXPath().compile(ALPHA_3_XPATH);
		this.ISSCFG_CODE_XPATH_EXPR = this.getXPath().compile(ISSCFG_CODE_XPATH);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#getDomain()
	 */
	@Override
	public String getDomain() throws Exception {
		return this.getDomainForFactsheetType(GearTypeFactsheetData.class);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#getAllData()
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=GET_ALL_DATA_CACHE_KEY)
	public Collection<GearTypeFactsheetData> getAllData() throws Exception {
		return this.getAllData(this.getDomain());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#getData(java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=GET_DATA_CACHE_KEY)
	public GearTypeFactsheetData getData(String factsheetID) throws Exception {
		return this.getData(this.getDomain(), factsheetID);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#resetAndExtractData()
	 */
	@Override
	public Collection<GearTypeFactsheetData> resetAndExtractData() throws Exception {
		return this.resetAndExtractData(this.getDomain());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#fetchFactsheetIDs()
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=FETCH_FACTSHEET_IDS_CACHE_KEY)
	public Collection<String> fetchFactsheetIDs() throws Exception {
		return this.fetchFactsheetIDs(this.getDomain());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#fetchFactsheet(java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=FETCH_FACTSHEET_CACHE_KEY)
	public String fetchFactsheet(String factsheetID) throws Exception {
		return this.fetchFactsheet(this.getDomain(), factsheetID);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#doExtractData(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=EXTRACT_DATA_CACHE_KEY)
	public GearTypeFactsheetData extractData(String domain, String factsheetID, String factsheetContent) throws Exception {
		if(factsheetContent == null)
			return null;

		Document doc = this._documentBuilder.parse(new ByteArrayInputStream(factsheetContent.getBytes("UTF-8")));

		GearTypeFactsheetData data = new GearTypeFactsheetData();
		
		data.setFactsheetID(factsheetID == null ? null : String.valueOf(factsheetID));
		data.setFactsheetURL(this.buildFactsheetURL(data.getFactsheetDomain(), factsheetID, DEFAULT_LANG));
		
		data.setFactsheetTitle(StringsHelper.trim(this.sanitizeText((String)this.TITLE_XPATH_EXPR.evaluate(doc, XPathConstants.STRING))));
		data.setStandardAbbreviation(StringsHelper.trim(this.sanitizeText((String)this.ALPHA_3_XPATH_EXPR.evaluate(doc, XPathConstants.STRING))));
		data.setISSCFGCode(StringsHelper.trim(this.sanitizeText((String)this.ISSCFG_CODE_XPATH_EXPR.evaluate(doc, XPathConstants.STRING))));
		
		return data;
	}
}