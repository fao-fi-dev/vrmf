/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-figis-services)
 */
package org.fao.fi.vrmf.common.figis.services.factsheets.psm;

import java.io.ByteArrayInputStream;
import java.util.Collection;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.common.figis.models.factsheets.psm.PSMFactsheetData;
import org.fao.fi.vrmf.common.figis.services.factsheets.AbstractFilterableFactsheetFetcher;
import org.springframework.cache.annotation.Cacheable;
import org.w3c.dom.Document;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Sep 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Sep 2012
 */
@Named
@Singleton
public class PSMFactsheetsFetcher extends AbstractFilterableFactsheetFetcher<PSMFactsheetData> {
	static final protected String DEFAULT_METHODS_INVOCATION_CACHE_ID = COMMON_METHODS_INVOCATION_CACHE_ID + ".psm";

	static final private String TITLE_XPATH = "//fi:PortStateMeasureIdent/dc:Title";
	private final XPathExpression TITLE_XPATH_EXPR;
	
	public PSMFactsheetsFetcher() throws Throwable {
		super("PortStateMeasureIdent");
		
		this.TITLE_XPATH_EXPR = this.getXPath().compile(TITLE_XPATH);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#getDomain()
	 */
	@Override
	public String getDomain() throws Exception {
		return this.getDomainForFactsheetType(PSMFactsheetData.class);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#getAllData()
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=GET_ALL_DATA_CACHE_KEY)
	public Collection<PSMFactsheetData> getAllData() throws Exception {
		return this.getAllData(this.getDomain());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#getData(java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=GET_DATA_CACHE_KEY)
	public PSMFactsheetData getData(String factsheetID) throws Exception {
		return this.getData(this.getDomain(), factsheetID);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#resetAndExtractData()
	 */
	@Override
	public Collection<PSMFactsheetData> resetAndExtractData() throws Exception {
		return this.resetAndExtractData(this.getDomain());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#fetchFactsheetIDs()
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=FETCH_FACTSHEET_IDS_CACHE_KEY)
	public Collection<String> fetchFactsheetIDs() throws Exception {
		return this.fetchFactsheetIDs(this.getDomain());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#fetchFactsheet(java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=FETCH_FACTSHEET_CACHE_KEY)
	public String fetchFactsheet(String factsheetID) throws Exception {
		return this.fetchFactsheet(this.getDomain(), factsheetID);
	}
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#doExtractData(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=EXTRACT_DATA_CACHE_KEY)
	public PSMFactsheetData extractData(String domain, String factsheetID, String factsheetContent) throws Exception {
		if(factsheetContent == null)
			return null;

		Document doc = this._documentBuilder.parse(new ByteArrayInputStream(factsheetContent.getBytes("UTF-8")));

		PSMFactsheetData data = new PSMFactsheetData();
		
		data.setFactsheetID(factsheetID == null ? null : String.valueOf(factsheetID));
		data.setFactsheetURL(this.buildFactsheetURL(data.getFactsheetDomain(), factsheetID, DEFAULT_LANG));
		
		data.setFactsheetTitle(StringsHelper.trim(this.sanitizeText((String)this.TITLE_XPATH_EXPR.evaluate(doc, XPathConstants.STRING))));
		
		return data;
	}
}