/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-figis-services)
 */
package org.fao.fi.vrmf.common.figis.services.factsheets.species;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.common.figis.models.factsheets.species.SpeciesFactsheetData;
import org.fao.fi.vrmf.common.figis.services.factsheets.AbstractFilterableFactsheetFetcher;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Sep 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Sep 2012
 */
@Named @Singleton
public class SpeciesFactsheetsFetcher extends AbstractFilterableFactsheetFetcher<SpeciesFactsheetData> {
	static final protected String DEFAULT_METHODS_INVOCATION_CACHE_ID = COMMON_METHODS_INVOCATION_CACHE_ID + ".species";

	static final private String THREE_ALPHA_CODE_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesIdent/fi:FAO3AlphaCode";
	static final private String NAME_EN_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesIdent/fi:FAOName/fi:En";
	static final private String NAME_FR_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesIdent/fi:FAOName/fi:Fr";
	static final private String NAME_ES_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesIdent/fi:FAOName/fi:Sp";
	static final private String IMAGES_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesIdent/fi:Image";
	static final private String SCIENTIFIC_NAME_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesIdent/fint:ScientificName";
	static final private String FAMILY_NAME_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesIdent/fi:SciName/fi:Family";
	static final private String CHANGED_GENUS_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesIdent/fi:SciName/@GenusChanged";
	static final private String PERSONAL_AUTHOR_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesIdent/fi:SciName/ags:PersonalAuthor";
	static final private String YEAR_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesIdent/fi:SciName/fi:Year";
	static final private String DIAGNOSTIC_FEAT_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesProfile/fi:DiagnosticFeat";
	static final private String AQ_SPECIES_TEXT_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesFeature/fi:GeoDist/fi:AqSpeciesText";
	static final private String AREA_TEXT_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesFeature/fi:GeoDist/fi:AreaText";
	static final private String HABITAT_BIO_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesFeature/fi:HabitatBio";
//	static final private String GENERAL_BIOLOGY_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesFeature/fi:HabitatBio/fi:GeneralBiology";
//	static final private String REPRODUCTION_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesFeature/fi:HabitatBio/fi:Reproduction";
//	static final private String FEEDING_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesFeature/fi:HabitatBio/fi:Feeding";
	static final private String FISHERIES_TEXT_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesFeature/fi:InterestFisheries/fi:FisheriesText";
	static final private String LOCAL_NAME_ENTRY_XPATH = "/fi:FIGISDoc/fi:AqSpecies/fi:AqSpeciesFeature/fi:LocalName/fi:LocalNameEntry";
	static final private String ORIG_TEXT_XPATH = "fi:LandPoliticalRef/fi:OrigText/text()";
	static final private String NAME_XPATH = "fi:Name/text()";


	static final private Set<String> NO_NODES_TO_SKIP = null;
	static final private Set<String> HABITAT_BIO_NODES_TO_SKIP = new TreeSet<String>(Arrays.asList(new String[] {
		"fi:Bathymetry",
		"fi:Reproduction",
		"fi:Feeding"
	}));

//	static final private Set<String> GEO_DIST_NODES_TO_SKIP = new TreeSet<String>(HABITAT_BIO_NODES_TO_SKIP);

	private final XPathExpression THREE_ALPHA_CODE_XPATH_EXPR;
	private final XPathExpression NAME_EN_XPATH_EXPR;
	private final XPathExpression NAME_FR_XPATH_EXPR;
	private final XPathExpression NAME_ES_XPATH_EXPR;
	private final XPathExpression IMAGES_XPATH_EXPR;
	private final XPathExpression SCIENTIFIC_NAME_XPATH_EXPR;
	private final XPathExpression FAMILY_NAME_XPATH_EXPR;
	private final XPathExpression CHANGED_GENUS_XPATH_EXPR;
	private final XPathExpression PERSONAL_AUTHOR_XPATH_EXPR;
	private final XPathExpression YEAR_XPATH_EXPR;
	private final XPathExpression DIAGNOSTIC_FEAT_XPATH_EXPR;
	private final XPathExpression AQ_SPECIES_TEXT_XPATH_EXPR;
	private final XPathExpression AREA_TEXT_XPATH_EXPR;
	private final XPathExpression HABITAT_BIO_XPATH_EXPR;
//	private XPathExpression GENERAL_BIOLOGY_XPATH_EXPR;
//	private XPathExpression REPRODUCTION_XPATH_EXPR;
//	private XPathExpression FEEDING_XPATH_EXPR;
	private final XPathExpression FISHERIES_TEXT_XPATH_EXPR;
	private final XPathExpression LOCAL_NAME_ENTRY_XPATH_EXPR;
	private final XPathExpression ORIG_TEXT_XPATH_EXPR;
	private final XPathExpression NAME_XPATH_EXPR;

	public SpeciesFactsheetsFetcher() throws Throwable {
		super();
		
		this.THREE_ALPHA_CODE_XPATH_EXPR = this.getXPath().compile(THREE_ALPHA_CODE_XPATH);
		this.NAME_EN_XPATH_EXPR = this.getXPath().compile(NAME_EN_XPATH);
		this.NAME_FR_XPATH_EXPR = this.getXPath().compile(NAME_FR_XPATH);
		this.NAME_ES_XPATH_EXPR = this.getXPath().compile(NAME_ES_XPATH);
		this.IMAGES_XPATH_EXPR = this.getXPath().compile(IMAGES_XPATH);
		this.SCIENTIFIC_NAME_XPATH_EXPR = this.getXPath().compile(SCIENTIFIC_NAME_XPATH);
		this.FAMILY_NAME_XPATH_EXPR = this.getXPath().compile(FAMILY_NAME_XPATH);
		this.CHANGED_GENUS_XPATH_EXPR = this.getXPath().compile(CHANGED_GENUS_XPATH);
		this.PERSONAL_AUTHOR_XPATH_EXPR = this.getXPath().compile(PERSONAL_AUTHOR_XPATH);
		this.YEAR_XPATH_EXPR = this.getXPath().compile(YEAR_XPATH);
		this.DIAGNOSTIC_FEAT_XPATH_EXPR = this.getXPath().compile(DIAGNOSTIC_FEAT_XPATH);
		this.AQ_SPECIES_TEXT_XPATH_EXPR = this.getXPath().compile(AQ_SPECIES_TEXT_XPATH);
		this.AREA_TEXT_XPATH_EXPR = this.getXPath().compile(AREA_TEXT_XPATH);
		this.HABITAT_BIO_XPATH_EXPR = this.getXPath().compile(HABITAT_BIO_XPATH);
//		this.GENERAL_BIOLOGY_XPATH_EXPR = this.getXPath().compile(GENERAL_BIOLOGY_XPATH);
//		this.REPRODUCTION_XPATH_EXPR = this.getXPath().compile(REPRODUCTION_XPATH);
//		this.FEEDING_XPATH_EXPR = this.getXPath().compile(FEEDING_XPATH);
		this.FISHERIES_TEXT_XPATH_EXPR = this.getXPath().compile(FISHERIES_TEXT_XPATH);
		this.LOCAL_NAME_ENTRY_XPATH_EXPR = this.getXPath().compile(LOCAL_NAME_ENTRY_XPATH);
		this.ORIG_TEXT_XPATH_EXPR = this.getXPath().compile(ORIG_TEXT_XPATH);
		this.NAME_XPATH_EXPR = this.getXPath().compile(NAME_XPATH);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#getDomain()
	 */
	@Override
	public String getDomain() throws Exception {
		return this.getDomainForFactsheetType(SpeciesFactsheetData.class);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#getAllData()
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=GET_ALL_DATA_CACHE_KEY)
	public Collection<SpeciesFactsheetData> getAllData() throws Exception {
		return this.getAllData(this.getDomain());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#getData(java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=GET_DATA_CACHE_KEY)
	public SpeciesFactsheetData getData(String factsheetID) throws Exception {
		return this.getData(this.getDomain(), factsheetID);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#resetAndExtractData()
	 */
	@CacheEvict(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, allEntries=true)
	public Collection<SpeciesFactsheetData> resetAndExtractData() throws Exception {
		return this.resetAndExtractData(this.getDomain());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#fetchFactsheetIDs()
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=FETCH_FACTSHEET_IDS_CACHE_KEY)
	public Collection<String> fetchFactsheetIDs() throws Exception {
		return this.fetchFactsheetIDs(this.getDomain());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#fetchFactsheet(java.lang.String)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=FETCH_FACTSHEET_CACHE_KEY)
	public String fetchFactsheet(String factsheetID) throws Exception {
		return this.fetchFactsheet(this.getDomain(), factsheetID);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.factsheets.AbstractFactsheetFetcher#extractData(java.lang.Class, java.lang.String, java.lang.String)
	 */
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key=EXTRACT_DATA_CACHE_KEY)
	public SpeciesFactsheetData extractData(String domain, String factsheetID, String factsheetContent) throws Exception {
		if(factsheetContent == null)
			return null;

		Document doc = this._documentBuilder.parse(new ByteArrayInputStream(factsheetContent.getBytes("UTF-8")));

		SpeciesFactsheetData data = new SpeciesFactsheetData();
		
		data.setFactsheetID(factsheetID == null ? null : String.valueOf(factsheetID));
		data.setFactsheetURL(this.buildFactsheetURL(data.getFactsheetDomain(), factsheetID, DEFAULT_LANG));
		
		data.setAlpha3Code(StringsHelper.trim(this.sanitizeText((String)this.THREE_ALPHA_CODE_XPATH_EXPR.evaluate(doc, XPathConstants.STRING))));
		data.setNameEn(StringsHelper.trim(this.sanitizeText((String)this.NAME_EN_XPATH_EXPR.evaluate(doc, XPathConstants.STRING))));
		data.setNameFr(StringsHelper.trim(this.sanitizeText((String)this.NAME_FR_XPATH_EXPR.evaluate(doc, XPathConstants.STRING))));
		data.setNameEs(StringsHelper.trim(this.sanitizeText((String)this.NAME_ES_XPATH_EXPR.evaluate(doc, XPathConstants.STRING))));

		NodeList images = (NodeList)this.IMAGES_XPATH_EXPR.evaluate(doc, XPathConstants.NODESET);

		if(images != null && images.getLength() > 0) {
			StringBuilder sources = new StringBuilder();

			Node width;
			for(int n=0; n<images.getLength(); n++) {
				if(images.item(n).hasAttributes()) {
					width = images.item(n).getAttributes().getNamedItem("Width");

					if(width != null && "300".equals(width.getNodeValue()))
						sources.append(images.item(n).getAttributes().getNamedItem("Src").getNodeValue()).append(";");
				}
			}

			if(sources.length() > 0)
				data.setImages(StringsHelper.trim(this.sanitizeText(sources.toString().replaceAll("\\;$", ""))));
		}

		data.setScientificName(StringsHelper.trim(this.sanitizeText((String)this.SCIENTIFIC_NAME_XPATH_EXPR.evaluate(doc, XPathConstants.STRING))));
		data.setFamily(StringsHelper.trim(this.sanitizeText((String)this.FAMILY_NAME_XPATH_EXPR.evaluate(doc, XPathConstants.STRING))));

		String year = null;

		try {
			year = (String)this.YEAR_XPATH_EXPR.evaluate(doc, XPathConstants.STRING);
			data.setYear(Integer.valueOf(year));
		} catch (Throwable t) {
			this._log.warn("Unable to parse year value '" + year + "'");
		}

		String changedGenusAttr = (String)this.CHANGED_GENUS_XPATH_EXPR.evaluate(doc, XPathConstants.STRING);

		boolean changedGenus = changedGenusAttr != null &&
							   "y".equalsIgnoreCase(changedGenusAttr);

		String personalAuthor = StringsHelper.trim(this.sanitizeText((String)this.PERSONAL_AUTHOR_XPATH_EXPR.evaluate(doc, XPathConstants.STRING)));

		if(changedGenus)
			data.setPersonalAuthor("(" + ( personalAuthor == null ? "" : personalAuthor + ", " ) + year + ")");
		else
			data.setPersonalAuthor(personalAuthor);

		data.setDiagnosticFeatures(StringsHelper.trim(this.sanitizeText(this.extractText((NodeList)this.DIAGNOSTIC_FEAT_XPATH_EXPR.evaluate(doc, XPathConstants.NODESET), NO_NODES_TO_SKIP))));

		String part;
		StringBuilder speciesAndAreaText = new StringBuilder();

		part = StringsHelper.trim(this.sanitizeText(this.extractText((NodeList)this.AQ_SPECIES_TEXT_XPATH_EXPR.evaluate(doc, XPathConstants.NODESET), NO_NODES_TO_SKIP)));

		if(part != null)
			speciesAndAreaText.append(part).append(";");

		part = StringsHelper.trim(this.sanitizeText(this.extractText((NodeList)this.AREA_TEXT_XPATH_EXPR.evaluate(doc, XPathConstants.NODESET), NO_NODES_TO_SKIP)));

		if(part != null)
			speciesAndAreaText.append(part).append(";");

		data.setAreaText(this.sanitizeText(StringsHelper.trim(speciesAndAreaText.toString())));
		data.setHabitatBio(StringsHelper.trim(this.sanitizeText(this.extractText((NodeList)this.HABITAT_BIO_XPATH_EXPR.evaluate(doc, XPathConstants.NODESET), HABITAT_BIO_NODES_TO_SKIP))));
//		data.setGeneralBiology(StringsHelper.trim(this.sanitizeText(this.extractText((NodeList)this.GENERAL_BIOLOGY_XPATH_EXPR.evaluate(doc, XPathConstants.NODESET)))));
//		data.setReproduction(StringsHelper.trim(this.sanitizeText((String)this.REPRODUCTION_XPATH_EXPR.evaluate(doc, XPathConstants.STRING))));
//		data.setFeeding(StringsHelper.trim(this.sanitizeText((String)this.FEEDING_XPATH_EXPR.evaluate(doc, XPathConstants.STRING))));
		data.setInterestFisheries(StringsHelper.trim(this.sanitizeText(this.extractText((NodeList)this.FISHERIES_TEXT_XPATH_EXPR.evaluate(doc, XPathConstants.NODESET), NO_NODES_TO_SKIP))));

		NodeList localNames = (NodeList)this.LOCAL_NAME_ENTRY_XPATH_EXPR.evaluate(doc, XPathConstants.NODESET);

		StringBuilder localNamesText = new StringBuilder();

		Node localNameNode;

		String localName;
		String country;
		for(int l=0; l<localNames.getLength(); l++) {
			localNameNode = localNames.item(l);

			localName = StringsHelper.trim(this.sanitizeText(StringsHelper.trim((String)this.NAME_XPATH_EXPR.evaluate(localNameNode, XPathConstants.STRING))));
			country = StringsHelper.trim(this.sanitizeText(StringsHelper.trim((String)this.ORIG_TEXT_XPATH_EXPR.evaluate(localNameNode, XPathConstants.STRING))));

			if(localName != null)
				localNamesText.append(localName);

			if(country != null) {
				localNamesText.append(" (");

				if(country.length() > 3) {
					localNamesText.append(country.substring(0, 1).toUpperCase()).append(country.toLowerCase().substring(1));
				} else
					localNamesText.append(country.toUpperCase());

				localNamesText.append(")");
			}

			if(localName != null || country != null)
				localNamesText.append(",");
		}

		data.setLocalNames(this.sanitizeText(StringsHelper.trim(localNamesText.toString())));
		
		return data;
	}
}