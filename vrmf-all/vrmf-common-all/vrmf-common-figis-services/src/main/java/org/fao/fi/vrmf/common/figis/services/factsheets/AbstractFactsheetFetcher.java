/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-figis-services)
 */
package org.fao.fi.vrmf.common.figis.services.factsheets;

import org.fao.fi.vrmf.common.figis.models.factsheets.FactsheetData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2013
 */
abstract public class AbstractFactsheetFetcher<F extends FactsheetData> extends AbstractFilterableFactsheetFetcher<F> {
	/**
	 * Class constructor
	 *
	 * @throws Throwable
	 */
	public AbstractFactsheetFetcher() throws Throwable {
		super();
	}
}