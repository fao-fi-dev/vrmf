/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-figis-services)
 */
package org.fao.fi.vrmf.common.figis.services.factsheets;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.common.figis.models.factsheets.FactsheetData;
import org.fao.fi.vrmf.common.figis.models.factsheets.support.FINamespaceContext;
import org.fao.fi.vrmf.common.figis.models.factsheets.support.annotations.Domain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2013
 */
abstract public class AbstractFilterableFactsheetFetcher<F extends FactsheetData> {
	protected final Logger _log = LoggerFactory.getLogger(this.getClass());
	
	static final private String DOMAIN_PLACEHOLDER = "{domain}";
	static final private String FID_PLACEHOLDER	   = "{fid}";
	static final private String LANG_PLACEHOLDER   = "{lang}";
	static final private String XPATH_PLACEHOLDER  = "{xpath}";

	static final protected String NO_FS_ID		  = null;
	static final protected String DEFAULT_LANG	  = null;
	static final protected String NO_XPATH_FILTER = null;

	static final protected String FS_URL					   = "http://www.fao.org/fishery/" + DOMAIN_PLACEHOLDER + "/" + FID_PLACEHOLDER + "/" + LANG_PLACEHOLDER;
	static final protected String WS_FACTSHEET_LIST_URL		   = "http://www.fao.org/figis/ws/factsheets/domain/" + DOMAIN_PLACEHOLDER;
	static final protected String WS_FACTSHEET_RETRIEVE_URL    = WS_FACTSHEET_LIST_URL + "/factsheet/" + FID_PLACEHOLDER + "/language/" + LANG_PLACEHOLDER;
	static final protected String WS_FACTSHEET_FILTER_URL      = WS_FACTSHEET_RETRIEVE_URL + "/xpathdescendant/" + XPATH_PLACEHOLDER;

	static final protected String COMMON_METHODS_INVOCATION_CACHE_ID = "vrmf.common.figis.cache.factsheets";
	static final protected String METHOD_NAME_CACHE_KEY = "#root.method.name";
	
	static final protected String FETCH_FACTSHEET_IDS_CACHE_KEY = METHOD_NAME_CACHE_KEY;
	
	static final protected String GET_ALL_DATA_CACHE_KEY = METHOD_NAME_CACHE_KEY;
	static final protected String GET_ALL_DATA_WITH_SELECTOR_CACHE_KEY = GET_ALL_DATA_CACHE_KEY + " + #selector";
	
	static final protected String GET_DATA_CACHE_KEY = METHOD_NAME_CACHE_KEY + " + #factsheetID";
	static final protected String GET_DATA_WITH_SELECTOR_CACHE_KEY = GET_DATA_CACHE_KEY + " + #selector";
	
	static final protected String FETCH_FACTSHEET_CACHE_KEY = METHOD_NAME_CACHE_KEY + " + #factsheetID";
	static final protected String FETCH_FACTSHEET_WITH_SELECTOR_CACHE_KEY = FETCH_FACTSHEET_CACHE_KEY + " + #selector";
	
	static final protected String EXTRACT_DATA_CACHE_KEY = METHOD_NAME_CACHE_KEY + " + #factsheetID";
		
	static final protected FINamespaceContext FI_CUSTOM_NS_CONTEXT = new FINamespaceContext();

	@Inject protected HTTPHelper _http;

	protected final DocumentBuilder _documentBuilder;
	protected final XPathFactory _xPathFactory;

	private String _xPathFilter;

	protected long _updateTimestamp = 0L;

	public AbstractFilterableFactsheetFetcher() throws Throwable {
		this(NO_XPATH_FILTER);
	}
	
	/**
	 * Class constructor
	 */
	public AbstractFilterableFactsheetFetcher(String xPathFilter) throws Throwable {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);

		this._documentBuilder = factory.newDocumentBuilder();

		this._xPathFactory = XPathFactory.newInstance();
		
		this._xPathFilter = xPathFilter;
	}
	
	/**
	 * @return the 'HTTPHelper' value
	 */
	public HTTPHelper getHTTPHelper() {
		return this._http;
	}

	/**
	 * @param HTTPHelper the 'HTTPHelper' value to set
	 */
	public void setHTTPHelper(HTTPHelper HTTPHelper) {
		this._http = HTTPHelper;
	}

	/**
	 * @return the 'updateTimestamp' value
	 */
	public long getUpdateTimestamp() {
		return this._updateTimestamp;
	}
	
	abstract public String getDomain() throws Exception;
		
	final protected String getDomainForFactsheetType(Class<F> factsheetType) {
		if(factsheetType == null)
			return null;
		
		Domain annotated = factsheetType.getAnnotation(Domain.class);
		
		return annotated == null ? null : annotated.value(); 
	}
	
	abstract public Collection<F> getAllData() throws Exception;
	
	protected Collection<F> getAllData(String domain) throws Exception {
		Collection<F> result = new ArrayList<F>();

		Collection<String> IDs = this.fetchFactsheetIDs(domain);

		F current;
		for(String ID : IDs) {
			current = this.extractData(domain, ID, this.fetchFactsheet(domain, ID));

			if(current != null)
				result.add(current);
		}
		
		this._updateTimestamp = System.currentTimeMillis();

		return result;
	}

	abstract public F getData(String factsheetID) throws Exception;
	
	protected F getData(String domain, String factsheetID) throws Exception {
		return this.extractData(domain, factsheetID, this.fetchFactsheet(domain, factsheetID));
	}
	
	abstract public Collection<F> resetAndExtractData() throws Exception;

	protected Collection<F> resetAndExtractData(String domain) throws Exception {
		return this.getAllData(domain);
	}
	
	abstract public Collection<String> fetchFactsheetIDs() throws Exception;

	protected Collection<String> fetchFactsheetIDs(String domain) throws Exception {
		this._log.info("Fetching factsheets IDs for domain '{}'...", domain);

		Collection<String> IDs = new TreeSet<String>();

		Document doc = this._documentBuilder.parse(new URL(this.buildFactsheetListURL(domain, DEFAULT_LANG)).openStream());

		this.getXPath().setNamespaceContext(FI_CUSTOM_NS_CONTEXT);

		XPathExpression xe = this.getXPath().compile("//fiws:FactsheetDiscriminator");

		NodeList nodes = (NodeList)xe.evaluate(doc, XPathConstants.NODESET);

		for(int n=0; n<nodes.getLength(); n++)
			IDs.add(nodes.item(n).getAttributes().getNamedItem("factsheet").getNodeValue());

		this._log.info("#{} factsheets IDs for domain '{}' have been retrieved", IDs.size(), domain);

		return IDs;
	}

	abstract public String fetchFactsheet(String factsheetID) throws Exception;
	
	protected String fetchFactsheet(String domain, String factsheetID) throws Exception {
		String URL = this.buildFactsheetRetrieveURL(domain, factsheetID, DEFAULT_LANG);

		this._log.info("Fetching factsheet fid:{} of domain '{}' from URL {}", factsheetID, domain, URL);

		HTTPRequestMetadata meta = this._http.fetchURLAsBytes(URL);

		String result = null;

		if(200 == meta.getResponseCode()) {
			result = new String(meta.getContent(), "UTF-8");

			if(result.indexOf("Server returned HTTP response code: 500 for URL") >= 0) {
				this._log.error("Unexpected server error 500 received. Actual content is: {}", result);

				result = null;
			}
		} else {
			this._log.warn("Unexpected HTTP response code {} received", meta.getResponseCode());
		}

		this._log.info("Factsheet fid:{} of domain '{}' has been fetched in {} mSec.", factsheetID, domain, meta.getDownloadTime());

		return result;
	}
	
	abstract public F extractData(String domain, String factsheetID, String factsheetContent) throws Exception;
	
	protected XPath getXPath() {
		XPath xp = this._xPathFactory.newXPath();
		xp.setNamespaceContext(FI_CUSTOM_NS_CONTEXT);

		return xp;
	}
	
	protected String buildEndpointURL(String URLPrototype, String factsheetDomain, String factsheetID, String language) {
		if(factsheetDomain == null || URLPrototype == null)
			return null;
		
		String actual = URLPrototype.replace(DOMAIN_PLACEHOLDER, factsheetDomain).
									 replace(LANG_PLACEHOLDER, language == null ? "en" : language);
		
		if(factsheetID != null)
			actual = actual.replace(FID_PLACEHOLDER, factsheetID);

		if(this._xPathFilter != null)
			actual = actual.replace(XPATH_PLACEHOLDER, this._xPathFilter);
		
		return actual;
	}
	
	protected String buildFactsheetListURL(String factsheetDomain, String language) {
		return this.buildEndpointURL(WS_FACTSHEET_LIST_URL, factsheetDomain, NO_FS_ID, language);
	}
	
	protected String buildFactsheetRetrieveURL(String factsheetDomain, String factsheetID, String language) {
		return this.buildEndpointURL(this._xPathFilter == null ? WS_FACTSHEET_RETRIEVE_URL : WS_FACTSHEET_FILTER_URL, factsheetDomain, factsheetID, language);
	}
		
	protected String buildFactsheetURL(String factsheetDomain, String factsheetID, String language) {
		return this.buildEndpointURL(FS_URL, factsheetDomain, factsheetID, language);
	}
		
	protected String sanitizeText(StringBuilder toSanitize) {
		if(toSanitize == null)
			return "";

		return this.sanitizeText(toSanitize.toString());
	}

	protected String sanitizeText(String toSanitize) {
		String sanitized = StringsHelper.trim(toSanitize);

		if(sanitized != null) {
			sanitized = sanitized.replaceAll("\\t+", " ").
								  replaceAll("\\n", " ").
								  replaceAll("\\r", " ").
								  replaceAll("\\s+", " ").
								  replaceAll("\\s\\,", ",").
								  replaceAll("\\;$", "").
								  replaceAll("\\,$", "");
		}

		return sanitized == null ? "" : sanitized;
	}

	protected String extractText(NodeList nodes, Collection<String> excludedElements) throws Exception {
		StringBuilder text = new StringBuilder();

		String value = null;
		if(nodes != null && nodes.getLength() > 0) {
			Node node;
			boolean exclude;
			for(int n=0; n<nodes.getLength(); n++) {
				node = nodes.item(n);

				exclude = Node.ELEMENT_NODE == node.getNodeType() &&
						  excludedElements != null &&
						  !excludedElements.isEmpty() &&
						  excludedElements.contains(node.getNodeName());

				if(!exclude) {
					value = StringsHelper.trim(this.extractText(nodes.item(n), excludedElements));

					if(value != null)
						text.append(" ").append(value);
				}
			}
		}

		return this.sanitizeText(text);
	}

	protected String extractText(Node node, Collection<String> excludedElements) throws Exception {
		StringBuilder text = new StringBuilder();
		String value = null;
		if(Node.TEXT_NODE == node.getNodeType()) {
			value = StringsHelper.trim(node.getNodeValue());

			if(value != null)
				text.append(value);
		}

		text.append(this.extractText(node.getChildNodes(), excludedElements));

		return this.sanitizeText(text);
	}
}