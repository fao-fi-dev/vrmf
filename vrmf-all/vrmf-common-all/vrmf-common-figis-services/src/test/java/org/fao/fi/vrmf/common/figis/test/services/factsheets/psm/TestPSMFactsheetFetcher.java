/**
 * (c) 2013 FAO / UN (project: vrmf-common)
 */
package org.fao.fi.vrmf.common.figis.test.services.factsheets.psm;

import java.util.Collection;

import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.vrmf.common.figis.models.factsheets.psm.PSMFactsheetData;
import org.fao.fi.vrmf.common.figis.services.factsheets.psm.PSMFactsheetsFetcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2013
 */
public class TestPSMFactsheetFetcher {
	private PSMFactsheetsFetcher _fetcher;
	
	@Before
	public void setup() throws Throwable {
		this._fetcher = new PSMFactsheetsFetcher();
		this._fetcher.setHTTPHelper(new HTTPHelper());
	}
	
	@Test
	public void testFactsheetIDsRetrievement() throws Throwable {
		try {
			Collection<String> IDs = this._fetcher.fetchFactsheetIDs();
			
			System.out.println(IDs);
		} catch (Throwable t) {
			Assert.fail(t.getMessage());
		}
	}
	
	@Test
	public void testFactsheetRetrievement() throws Throwable {
		String ID = "EUR_63";
		String domain = new PSMFactsheetData().getFactsheetDomain();

		Collection<String> availableIDs = this._fetcher.fetchFactsheetIDs();
		
		if(availableIDs.contains(ID)) {
			try {
				PSMFactsheetData data = this._fetcher.extractData(this._fetcher.getDomain(), 
										  						  ID, 
										  						  this._fetcher.fetchFactsheet(ID));
				
				Assert.assertEquals("European Union - PSM63 - Landing/transhipment authorization - All vessels", data.getFactsheetTitle());
				Assert.assertEquals(ID, data.getFactsheetID());
				Assert.assertEquals(domain, data.getFactsheetDomain());
				Assert.assertEquals("http://www.fao.org/fishery/" + domain + "/" + ID + "/en", data.getFactsheetURL());
				
			} catch(Throwable t) {
				Assert.fail(t.getMessage());
			}
		} else {
			Assert.fail("Missing ID " + ID + " from currently available list of factsheet IDs for '" + domain + "' domain");
		}
	}
	
	@Test
	public void testAllFactsheetsRetrievement() throws Throwable {
		try {
			Collection<String> IDs = this._fetcher.fetchFactsheetIDs();
			Collection<PSMFactsheetData> data = this._fetcher.getAllData();
			
			Assert.assertEquals(IDs.size(), data.size());
		} catch (Throwable t) {
			Assert.fail(t.getMessage());
		}
	}
}
