/**
 * (c) 2013 FAO / UN (project: vrmf-common)
 */
package org.fao.fi.vrmf.common.figis.test.services.factsheets.types.vessel;

import java.util.Collection;

import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.vrmf.common.figis.models.factsheets.types.vessel.VesselTypeFactsheetData;
import org.fao.fi.vrmf.common.figis.services.factsheets.types.vessel.VesselTypeFactsheetsFetcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2013
 */
public class TestVesselTypeFactsheetFetcher {
	private VesselTypeFactsheetsFetcher _fetcher;
	
	@Before
	public void setup() throws Throwable {
		this._fetcher = new VesselTypeFactsheetsFetcher();
		this._fetcher.setHTTPHelper(new HTTPHelper());
	}
	
	@Test
	public void testFactsheetIDsRetrievement() throws Throwable {
		try {
			Collection<String> IDs = this._fetcher.fetchFactsheetIDs();
			
			System.out.println(IDs);
		} catch (Throwable t) {
			Assert.fail(t.getMessage());
		}
	}
	
	@Test
	public void testFactsheetRetrievement() throws Throwable {
		String ID = "732";
		String domain = new VesselTypeFactsheetData().getFactsheetDomain();

		Collection<String> availableIDs = this._fetcher.fetchFactsheetIDs();
		
		if(availableIDs.contains(ID)) {
			try {
				VesselTypeFactsheetData data = this._fetcher.extractData(this._fetcher.getDomain(), 
																  ID, 
																  this._fetcher.fetchFactsheet(ID));
				
				System.out.println(data);
				
				Assert.assertEquals("American type pole and line vessels", data.getFactsheetTitle());
				Assert.assertEquals(ID, data.getFactsheetID());
				Assert.assertEquals(domain, data.getFactsheetDomain());
				Assert.assertEquals("07.3.2", data.getISSCFVCode());
				Assert.assertEquals("LPA", data.getStandardAbbreviation());
				Assert.assertEquals("http://www.fao.org/fishery/" + domain + "/" + ID + "/en", data.getFactsheetURL());
			} catch(Throwable t) {
				Assert.fail(t.getMessage());
			}
		} else {
			Assert.fail("Missing ID " + ID + " from currently available list of factsheet IDs for '" + domain + "' domain");
		}
	}
	
	@Test
	public void testAllFactsheetsRetrievement() throws Throwable {
		try {
			Collection<String> IDs = this._fetcher.fetchFactsheetIDs();
			Collection<VesselTypeFactsheetData> data = this._fetcher.getAllData();
			
			Assert.assertEquals(IDs.size(), data.size());
		} catch (Throwable t) {
			Assert.fail(t.getMessage());
		}
	}
}
