/**
 * (c) 2013 FAO / UN (project: vrmf-common)
 */
package org.fao.fi.vrmf.common.figis.test.services.factsheets.species;

import java.util.Collection;

import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.vrmf.common.figis.models.factsheets.species.SpeciesFactsheetData;
import org.fao.fi.vrmf.common.figis.services.factsheets.species.SpeciesFactsheetsFetcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2013
 */
public class TestSpeciesFactsheetFetcher {
	//http://www.fao.org/figis/ws/factsheets/domain/species/factsheet/2096/language/en
	private SpeciesFactsheetsFetcher _fetcher;
	
	@Before
	public void setup() throws Throwable {
		this._fetcher = new SpeciesFactsheetsFetcher();
		this._fetcher.setHTTPHelper(new HTTPHelper());
	}
	
	@Test
	public void testFactsheetIDsRetrievement() throws Throwable {
		try {
			Collection<String> IDs = this._fetcher.fetchFactsheetIDs();
			
			System.out.println(IDs);
		} catch (Throwable t) {
			Assert.fail(t.getMessage());
		}
	}
	
	@Test
	public void testFactsheetsRetrievement() throws Throwable {
		try {
			Collection<String> IDs = this._fetcher.fetchFactsheetIDs();
			Collection<SpeciesFactsheetData> data = this._fetcher.getAllData();
			
			Assert.assertEquals(IDs.size(), data.size());
		} catch (Throwable t) {
			Assert.fail(t.getMessage());
		}
	}
	
	@Test
	public void testFactsheetRetrievement() throws Throwable {
		String ID = "2096";
		String domain = new SpeciesFactsheetData().getFactsheetDomain();
		
		Collection<String> availableIDs = this._fetcher.fetchFactsheetIDs();

		if(availableIDs.contains(ID)) {
			try {
				
				SpeciesFactsheetData data = this._fetcher.extractData(this._fetcher.getDomain(), 
										  							  ID, 
										  							  this._fetcher.fetchFactsheet(ID));
				
				Assert.assertEquals("WRR", data.getAlpha3Code());
				Assert.assertEquals("Whitehead's round herring", data.getNameEn());
				Assert.assertEquals("Shadine de Angola", data.getNameFr());
				Assert.assertEquals("Sardina angoleña", data.getNameEs());
				Assert.assertEquals("figis/species/images/Etrumeus/clu_etr_whi_web_2096_0.gif", data.getImages());
				Assert.assertEquals("Etrumeus whiteheadi", data.getScientificName());
				Assert.assertEquals("Clupeidae", data.getFamily());
				Assert.assertEquals("Wongratana", data.getPersonalAuthor());
				Assert.assertEquals(new Integer(1983), data.getYear());
				Assert.assertEquals("Separate statistics for South Africa (63 009 t in 1983) must refer largely to this species, although given as E. teres . Much smaller catches, also presumed to be this species, are reported by Namibia, and in earlier years by Poland and the former German Democratic Republic working off this coast (6 653 t in 1983). The total catch reported for this species to FAO for 1999 was 59 032 t. The countries with the largest catches were South Africa (58 856 t) and Namibia (176 t).", data.getInterestFisheries());
				Assert.assertEquals(ID, data.getFactsheetID());
				Assert.assertEquals(domain, data.getFactsheetDomain());
				Assert.assertEquals("http://www.fao.org/fishery/" + domain + "/" + ID + "/en", data.getFactsheetURL());
	
				System.out.println(data);
			} catch(Throwable t) {
				Assert.fail(t.getMessage());
			}
		} else {
			Assert.fail("Missing ID " + ID + " from currently available list of factsheet IDs for '" + domain + "' domain");
		}
	}	
}
