/**
 * (c) 2013 FAO / UN (project: vrmf-common-core)
 */
package org.fao.vrmf.test.common.vessel.attributes;

import java.util.Date;

import org.fao.fi.vrmf.common.core.behavior.vessels.attributes.DateReferencedVesselAttribute;
import org.fao.fi.vrmf.common.core.behavior.vessels.attributes.VesselAttribute;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToRegistrations;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Mar 2013
 */
public class TestVesselAttributeBehaviour {
	protected <A extends VesselAttribute> A fillWithMetaData(A toFill) {
		toFill.setVesselId(42);
		toFill.setVesselUid(42);
		toFill.setSourceSystem("TRAAL");
		toFill.setUpdateDate(new Date());
		toFill.setUpdaterId("DNADAMS");
		toFill.setComment("Mostly harmless");
		
		return toFill;
	}
	
	protected <A extends DateReferencedVesselAttribute> A fillWithMetaData(A toFill) {
		this.fillWithMetaData((VesselAttribute)toFill);
		
		toFill.setReferenceDate(new Date());
		
		return toFill;
	} 
	
	@Test
	public void testEmptyVesselAttribute() {
		Assert.assertTrue(this.fillWithMetaData(new VesselsToIdentifiers()).isEmpty());
	}
	
	@Test
	public void testNonEmptyVesselAttribute() {
		VesselsToIdentifiers attribute = this.fillWithMetaData(new VesselsToIdentifiers());
		attribute.setAlternateIdentifier("24");
		Assert.assertFalse(attribute.isEmpty());
	}
	
	@Test
	public void testEmptyDateReferencedVesselAttribute() {
		Assert.assertTrue(this.fillWithMetaData(new VesselsToRegistrations()).isEmpty());
	}
}
