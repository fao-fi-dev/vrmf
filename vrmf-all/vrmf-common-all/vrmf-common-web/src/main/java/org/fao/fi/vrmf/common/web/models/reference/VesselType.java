/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.models.reference;

import org.fao.fi.vrmf.common.models.generated.SVesselTypes;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Jan 2012
 */
public class VesselType extends CommonMetaType {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6751016982802442406L;
	
	private String _ISSCFVCode;
	private String _countryOfReference;
	
	/**
	 * Class constructor
	 *
	 * @param id
	 * @param uid
	 * @param originalId
	 * @param isscfvCode
	 * @param metaCode
	 * @param standardAbbreviation
	 * @param name
	 * @param description
	 * @param sourceSystem
	 */
	public VesselType(Integer id, Integer uid, String originalId, String countryOfReference, String isscfvCode, Integer metaCode, String standardAbbreviation, String name, String description, String sourceSystem) {
		super(id, uid, originalId, metaCode, standardAbbreviation, name, description, sourceSystem);
		
		this._ISSCFVCode = isscfvCode;
		this._countryOfReference = countryOfReference;
	}
	
	public VesselType(SVesselTypes dto, String countryOfReference) {
		this(dto.getId(), dto.getUid(), dto.getOriginalVesselTypeId(), countryOfReference, dto.getIsscfvCode(), dto.getMetaCode(), dto.getStandardAbbreviation(), dto.getName(), dto.getDescription(), dto.getSourceSystem());
	}

	/**
	 * @return the 'iSSCFVCode' value
	 */
	public String getISSCFVCode() {
		return this._ISSCFVCode;
	}

	/**
	 * @param iSSCFVCode the 'iSSCFVCode' value to set
	 */
	public void setISSCFVCode(String iSSCFVCode) {
		this._ISSCFVCode = iSSCFVCode;
	}

	/**
	 * @return the 'countryOfReference' value
	 */
	public String getCountryOfReference() {
		return this._countryOfReference;
	}

	/**
	 * @param countryOfReference the 'countryOfReference' value to set
	 */
	public void setCountryOfReference(String countryOfReference) {
		this._countryOfReference = countryOfReference;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._ISSCFVCode == null) ? 0 : this._ISSCFVCode.hashCode());
		result = prime * result + ((this._countryOfReference == null) ? 0 : this._countryOfReference.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof VesselType))
			return false;
		VesselType other = (VesselType) obj;
		if (this._ISSCFVCode == null) {
			if (other._ISSCFVCode != null)
				return false;
		} else if (!this._ISSCFVCode.equals(other._ISSCFVCode))
			return false;
		if (this._countryOfReference == null) {
			if (other._countryOfReference != null)
				return false;
		} else if (!this._countryOfReference.equals(other._countryOfReference))
			return false;
		return true;
	}
}