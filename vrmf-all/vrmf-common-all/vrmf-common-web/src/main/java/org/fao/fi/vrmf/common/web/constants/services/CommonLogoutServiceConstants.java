/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.constants.services;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Sep 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Sep 2011
 */
public interface CommonLogoutServiceConstants {
	String TO_REQUEST_PARAM = "to";
}
