/**
 * (c) 2016 FAO / UN (project: vrmf-web-finder-base)
 */
package org.fao.fi.vrmf.common.web;

import org.fao.fi.vrmf.common.search.dsl.impl.Column;

import javax.servlet.http.HttpServletRequest;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 21, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 21, 2016
 */
public interface DataCustomizer {
	String DEFAULT_VESSELS_TABLE = "VESSELS";
	
	String getTargetTable(HttpServletRequest request);
	Column[] getDataColumns(HttpServletRequest request);
}
