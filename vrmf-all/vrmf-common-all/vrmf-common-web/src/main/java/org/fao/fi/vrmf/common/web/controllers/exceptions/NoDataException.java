/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.exceptions;

import org.springframework.http.HttpStatus;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2012
 */
public class NoDataException extends ExecutionException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7531876031890630103L;

	static final private String DEFAULT_MESSAGE = "Requested operation was not capable to identify any data";

	/**
	 * Class constructor
	 *
	 */
	public NoDataException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public NoDataException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public NoDataException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public NoDataException(Throwable cause) {
		super(DEFAULT_MESSAGE, cause);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.controllers.exceptions.ControllerException#getCorrespondingHttpStatusCode()
	 */
	@Override
	public HttpStatus getCorrespondingHttpStatusCode() {
		return HttpStatus.NO_CONTENT;
	}	
}