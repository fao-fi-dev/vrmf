/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.metadata;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.common.helpers.singletons.lang.BeansHelper;
import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.sh.utility.model.extensions.collections.SerializableCollection;
import org.fao.fi.sh.utility.model.extensions.collections.SerializableList;
import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableArrayList;
import org.fao.fi.sh.utility.model.extensions.maps.SerializableMap;
import org.fao.fi.sh.utility.model.extensions.maps.impl.SerializableHashMap;
import org.fao.fi.vrmf.business.dao.CommonMetadataDAO;
import org.fao.fi.vrmf.business.dao.ExtendedPortsDAO;
import org.fao.fi.vrmf.business.dao.generated.SAgeCategoriesDAO;
import org.fao.fi.vrmf.business.dao.generated.SAuthorityRoleDAO;
import org.fao.fi.vrmf.business.dao.generated.SAuthorizationHoldersDAO;
import org.fao.fi.vrmf.business.dao.generated.SAuthorizationTerminationReasonsDAO;
import org.fao.fi.vrmf.business.dao.generated.SAuthorizationTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SAuthorizationTypesI18nDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesGroupDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesI18nDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesSubdivisionDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesToGroupDAO;
import org.fao.fi.vrmf.business.dao.generated.SGearTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SGearTypesI18nDAO;
import org.fao.fi.vrmf.business.dao.generated.SHullMaterialDAO;
import org.fao.fi.vrmf.business.dao.generated.SIdentifierTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SInspectionInfringementTypeDAO;
import org.fao.fi.vrmf.business.dao.generated.SInspectionOutcomeTypeDAO;
import org.fao.fi.vrmf.business.dao.generated.SInspectionReportTypeDAO;
import org.fao.fi.vrmf.business.dao.generated.SLengthCategoriesDAO;
import org.fao.fi.vrmf.business.dao.generated.SLengthTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SLengthTypesI18nDAO;
import org.fao.fi.vrmf.business.dao.generated.SMeasureUnitsDAO;
import org.fao.fi.vrmf.business.dao.generated.SPortEntryDenialReasonDAO;
import org.fao.fi.vrmf.business.dao.generated.SPowerCategoriesDAO;
import org.fao.fi.vrmf.business.dao.generated.SPowerTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SRfmoIuuListsDAO;
import org.fao.fi.vrmf.business.dao.generated.SSystemsDAO;
import org.fao.fi.vrmf.business.dao.generated.SSystemsGroupsDAO;
import org.fao.fi.vrmf.business.dao.generated.SSystemsI18nDAO;
import org.fao.fi.vrmf.business.dao.generated.SSystemsToGroupsDAO;
import org.fao.fi.vrmf.business.dao.generated.STonnageCategoriesDAO;
import org.fao.fi.vrmf.business.dao.generated.STonnageTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.STonnageTypesI18nDAO;
import org.fao.fi.vrmf.business.dao.generated.SVesselStatusDAO;
import org.fao.fi.vrmf.business.dao.generated.SVesselTypesDAO;
import org.fao.fi.vrmf.business.dao.generated.SVesselTypesI18nDAO;
import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.figis.models.factsheets.FactsheetData;
import org.fao.fi.vrmf.common.figis.models.factsheets.FactsheetMetadata;
import org.fao.fi.vrmf.common.figis.models.factsheets.psm.PSMFactsheetData;
import org.fao.fi.vrmf.common.figis.models.factsheets.species.SpeciesFactsheetData;
import org.fao.fi.vrmf.common.figis.models.factsheets.support.annotations.Domain;
import org.fao.fi.vrmf.common.figis.models.factsheets.types.gear.GearTypeFactsheetData;
import org.fao.fi.vrmf.common.figis.models.factsheets.types.vessel.VesselTypeFactsheetData;
import org.fao.fi.vrmf.common.figis.services.factsheets.AbstractFilterableFactsheetFetcher;
import org.fao.fi.vrmf.common.figis.services.factsheets.psm.PSMFactsheetsFetcher;
import org.fao.fi.vrmf.common.figis.services.factsheets.species.SpeciesFactsheetsFetcher;
import org.fao.fi.vrmf.common.figis.services.factsheets.types.gear.GearTypeFactsheetsFetcher;
import org.fao.fi.vrmf.common.figis.services.factsheets.types.vessel.VesselTypeFactsheetsFetcher;
import org.fao.fi.vrmf.common.models.extended.ExtendedCountriesGroup;
import org.fao.fi.vrmf.common.models.extended.ExtendedPort;
import org.fao.fi.vrmf.common.models.extended.ExtendedSystemsGroups;
import org.fao.fi.vrmf.common.models.generated.SAgeCategories;
import org.fao.fi.vrmf.common.models.generated.SAgeCategoriesExample;
import org.fao.fi.vrmf.common.models.generated.SAuthorityRole;
import org.fao.fi.vrmf.common.models.generated.SAuthorityRoleExample;
import org.fao.fi.vrmf.common.models.generated.SAuthorizationHolders;
import org.fao.fi.vrmf.common.models.generated.SAuthorizationHoldersExample;
import org.fao.fi.vrmf.common.models.generated.SAuthorizationTerminationReasons;
import org.fao.fi.vrmf.common.models.generated.SAuthorizationTerminationReasonsExample;
import org.fao.fi.vrmf.common.models.generated.SAuthorizationTypes;
import org.fao.fi.vrmf.common.models.generated.SAuthorizationTypesExample;
import org.fao.fi.vrmf.common.models.generated.SAuthorizationTypesI18n;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SCountriesExample;
import org.fao.fi.vrmf.common.models.generated.SCountriesGroup;
import org.fao.fi.vrmf.common.models.generated.SCountriesGroupExample;
import org.fao.fi.vrmf.common.models.generated.SCountriesI18n;
import org.fao.fi.vrmf.common.models.generated.SCountriesSubdivision;
import org.fao.fi.vrmf.common.models.generated.SCountriesSubdivisionExample;
import org.fao.fi.vrmf.common.models.generated.SCountriesToGroup;
import org.fao.fi.vrmf.common.models.generated.SCountriesToGroupExample;
import org.fao.fi.vrmf.common.models.generated.SGearTypes;
import org.fao.fi.vrmf.common.models.generated.SGearTypesExample;
import org.fao.fi.vrmf.common.models.generated.SGearTypesI18n;
import org.fao.fi.vrmf.common.models.generated.SHullMaterial;
import org.fao.fi.vrmf.common.models.generated.SHullMaterialExample;
import org.fao.fi.vrmf.common.models.generated.SIdentifierTypes;
import org.fao.fi.vrmf.common.models.generated.SIdentifierTypesExample;
import org.fao.fi.vrmf.common.models.generated.SInspectionInfringementType;
import org.fao.fi.vrmf.common.models.generated.SInspectionInfringementTypeExample;
import org.fao.fi.vrmf.common.models.generated.SInspectionOutcomeType;
import org.fao.fi.vrmf.common.models.generated.SInspectionOutcomeTypeExample;
import org.fao.fi.vrmf.common.models.generated.SInspectionReportType;
import org.fao.fi.vrmf.common.models.generated.SInspectionReportTypeExample;
import org.fao.fi.vrmf.common.models.generated.SLengthCategories;
import org.fao.fi.vrmf.common.models.generated.SLengthCategoriesExample;
import org.fao.fi.vrmf.common.models.generated.SLengthTypes;
import org.fao.fi.vrmf.common.models.generated.SLengthTypesExample;
import org.fao.fi.vrmf.common.models.generated.SLengthTypesI18n;
import org.fao.fi.vrmf.common.models.generated.SMeasureUnits;
import org.fao.fi.vrmf.common.models.generated.SMeasureUnitsExample;
import org.fao.fi.vrmf.common.models.generated.SPortEntryDenialReason;
import org.fao.fi.vrmf.common.models.generated.SPortEntryDenialReasonExample;
import org.fao.fi.vrmf.common.models.generated.SPowerCategories;
import org.fao.fi.vrmf.common.models.generated.SPowerCategoriesExample;
import org.fao.fi.vrmf.common.models.generated.SPowerTypes;
import org.fao.fi.vrmf.common.models.generated.SPowerTypesExample;
import org.fao.fi.vrmf.common.models.generated.SRfmoIuuLists;
import org.fao.fi.vrmf.common.models.generated.SRfmoIuuListsExample;
import org.fao.fi.vrmf.common.models.generated.SSystems;
import org.fao.fi.vrmf.common.models.generated.SSystemsExample;
import org.fao.fi.vrmf.common.models.generated.SSystemsGroups;
import org.fao.fi.vrmf.common.models.generated.SSystemsGroupsExample;
import org.fao.fi.vrmf.common.models.generated.SSystemsI18n;
import org.fao.fi.vrmf.common.models.generated.SSystemsToGroups;
import org.fao.fi.vrmf.common.models.generated.SSystemsToGroupsExample;
import org.fao.fi.vrmf.common.models.generated.STonnageCategories;
import org.fao.fi.vrmf.common.models.generated.STonnageCategoriesExample;
import org.fao.fi.vrmf.common.models.generated.STonnageTypes;
import org.fao.fi.vrmf.common.models.generated.STonnageTypesExample;
import org.fao.fi.vrmf.common.models.generated.STonnageTypesI18n;
import org.fao.fi.vrmf.common.models.generated.SVesselStatus;
import org.fao.fi.vrmf.common.models.generated.SVesselStatusExample;
import org.fao.fi.vrmf.common.models.generated.SVesselTypes;
import org.fao.fi.vrmf.common.models.generated.SVesselTypesExample;
import org.fao.fi.vrmf.common.models.generated.SVesselTypesI18n;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.cache.annotation.Cacheable;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 May 2012
 */
@Named @Singleton
public class ReferenceMetadataDataRetriever extends AbstractLoggingAwareClient implements InitializingBean {
	static final private String DEFAULT_METHODS_INVOCATION_CACHE_ID = "vrmf.web.core.metadata.reference";
	
	static Set<String> CATEGORIES = new TreeSet<String>();
	static Map<String, String[]> CATEGORY_GROUPS = new HashMap<String, String[]>();
	static Map<String, String[]> INCLUSION_PATTERNS_BY_CATEGORY = new HashMap<String, String[]>();
	
	static {
		CATEGORIES.add("otherSystems");
		CATEGORIES.add("systems");
		CATEGORIES.add("systemsGroups");
		CATEGORIES.add("identifiers");
		CATEGORIES.add("countries");
		CATEGORIES.add("countriesGroups");
		CATEGORIES.add("subdivisions");
		CATEGORIES.add("hullMaterials");
		CATEGORIES.add("vesselStatus");
		CATEGORIES.add("vesselTypes");
		CATEGORIES.add("vesselTypesCategories");
		CATEGORIES.add("gearTypes");
		CATEGORIES.add("gearTypesCategories");
		CATEGORIES.add("measureUnits");
		CATEGORIES.add("lengthTypes");
		CATEGORIES.add("lengthRanges");
		CATEGORIES.add("tonnageTypes");
		CATEGORIES.add("tonnageRanges");
		CATEGORIES.add("powerTypes");
		CATEGORIES.add("powerRanges");
		CATEGORIES.add("ageRanges");
		CATEGORIES.add("authorizationSystems");
		CATEGORIES.add("authorizationSystemsGroups");
		CATEGORIES.add("authorizationTypes");		
		CATEGORIES.add("authorizationTerminationReasons");
		CATEGORIES.add("vesselTypeFactsheets");
		CATEGORIES.add("gearTypeFactsheets");
		CATEGORIES.add("portStateMeasureFactsheets");
		CATEGORIES.add("speciesFactsheets");
		
		CATEGORIES.add("authorityRoles");
		CATEGORIES.add("authorizationHolders");
		CATEGORIES.add("inspectionInfringementTypes");
		CATEGORIES.add("inspectionOutcomeTypes");
		CATEGORIES.add("inspectionReportTypes");
		CATEGORIES.add("portEntryDenialReasons");
		CATEGORIES.add("rfmoIuuLists");
		
		CATEGORY_GROUPS.put("systems", new String[] { "otherSystems", "systems", "systemsGroups" });
		CATEGORY_GROUPS.put("countries", new String[] { "countries", "countriesGroups" });
		CATEGORY_GROUPS.put("authorizationSystems", new String[] { "authorizationSystems", "authorizationSystemsGroups" });
		CATEGORY_GROUPS.put("codes", new String[] { 
			"hullMaterials", "vesselTypes", "vesselTypesCategories", "gearTypes", "gearTypesCategories", "measureUnits", "lengthTypes", "tonnageTypes", "powerTypes", "authorizationTypes", "authorizationTerminationReasons",
			"authorityRoles", "authorizationHolders", "inspectionInfringementTypes", "inspectionOutcomeTypes", "inspectionReportTypes", "portEntryDenialReasons", "rfmoIuuLists"
		});
		
		CATEGORY_GROUPS.put("ranges", new String[] { "lengthRanges", "tonnageRanges", "powerRanges", "ageRanges" });
		
		CATEGORY_GROUPS.put("factsheets", new String[] { "vesselTypeFactsheets", "gearTypeFactsheets", "portStateMeasureFactsheets", "speciesFactsheets" });
				
		INCLUSION_PATTERNS_BY_CATEGORY.put("systems", new String[] { "*.id", "*.name", "*.isPublic", "*.website", "*.vesselSource", "*.authSource", "*.crossSystemsData" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("otherSystems", INCLUSION_PATTERNS_BY_CATEGORY.get("systems"));
		INCLUSION_PATTERNS_BY_CATEGORY.put("systemsGroups", new String[] {  "*.id", "*.description", "*.systems.*.id", "*.systems.*.name", "*.systems.*.isPublic", "*.systems.*.website", "*.systems.*.vesselSource", "*.systems.*.authSource", "*.systems.*.crossSystemsData" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("identifiers", new String[] { "*.id", "*.isPublic", "*.isUnique", "*.priority", "*.description", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("countries", new String[] { "*.id", "*.iso2Code", "*.iso3Code", "*.name", "*.officialName" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("countriesGroups", new String[] { "*.id", "*.description", "*.countries.*.id", "*.countries.*.iso2Code", "*.countries.*.iso3Code", "*.countries.*.name", "*.countries.*.officialName" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("subdivisions", new String[] { "*.id", "*.countryId", "*.name", "*.alternateName", "*.typeId" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("hullMaterials", new String[] { "*.id", "*.description", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("vesselStatus", new String[] { "*.id", "*.description", "*.inService", "*.temporarilyNotInService", "*.notInService", "*.permanentlyNotInService", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("vesselTypes", new String[] { "*.id", "*.originalVesselTypeId", "*.countryId", "*.isscfvCode", "*.standardAbbreviation", "*.name", "*.description", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("vesselTypesCategories", new String[] { "*.id", "*.originalVesselTypeId", "*.isscfvCode", "*.standardAbbreviation", "*.name", "*.description", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("gearTypes", new String[] { "*.id", "*.originalGearTypeCode", "*.isscfgCode", "*.standardAbbreviation", "*.name", "*.description", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("gearTypesCategories", new String[] { "*.id", "*.originalGearTypeCode", "*.isscfgCode", "*.standardAbbreviation", "*.name", "*.description", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("measureUnits", new String[] { "*.id", "*.description", "*.symbol", "*.comment" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("lengthTypes", new String[] { "*.id", "*.description", "*.defaultUnit", "*.sourceSystem", "*.comment" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("lengthRanges", new String[] { "*.lengthTypeId", "*.rangeFrom", "*.rangeTo", "*.description", "*.sourceSystem"  });
		INCLUSION_PATTERNS_BY_CATEGORY.put("tonnageTypes", INCLUSION_PATTERNS_BY_CATEGORY.get("lengthTypes"));
		INCLUSION_PATTERNS_BY_CATEGORY.put("tonnageRanges", new String[] { "*.tonnageTypeId", "*.rangeFrom", "*.rangeTo", "*.description", "*.sourceSystem"  });
		INCLUSION_PATTERNS_BY_CATEGORY.put("powerTypes", INCLUSION_PATTERNS_BY_CATEGORY.get("lengthTypes"));
		INCLUSION_PATTERNS_BY_CATEGORY.put("powerRanges", new String[] { "*.powerTypeId", "*.rangeFrom", "*.rangeTo", "*.description", "*.sourceSystem"  });
		INCLUSION_PATTERNS_BY_CATEGORY.put("ageRanges", new String[] { "*.rangeFrom", "*.rangeTo", "*.description", "*.sourceSystem"  });
		INCLUSION_PATTERNS_BY_CATEGORY.put("authorizationSystems", INCLUSION_PATTERNS_BY_CATEGORY.get("systems"));
		INCLUSION_PATTERNS_BY_CATEGORY.put("authorizationSystemsGroups", INCLUSION_PATTERNS_BY_CATEGORY.get("systemsGroups"));
		INCLUSION_PATTERNS_BY_CATEGORY.put("authorizationTypes", new String[] { "*.id", "*.description", "*.countryDependant", "*.sourceSystem", "*.comment" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("authorizationTerminationReasons", new String[] { "*.id", "*.description", "*.sourceSystem", "*.comment" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("vesselTypeFactsheets", new String[] { "*.availableIDs", "*.factsheetDomain", "*.factsheetData" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("gearTypeFactsheets", INCLUSION_PATTERNS_BY_CATEGORY.get("vesselTypeFactsheets"));
		INCLUSION_PATTERNS_BY_CATEGORY.put("portStateMeasureFactsheets", INCLUSION_PATTERNS_BY_CATEGORY.get("vesselTypeFactsheets"));
		INCLUSION_PATTERNS_BY_CATEGORY.put("speciesFactsheets", new String[] { "*.availableIDs", "*.factsheetDomain" });
		
		INCLUSION_PATTERNS_BY_CATEGORY.put("authorityRoles", new String[] { "*.id", "*.code", "*.description", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("authorizationHolders", new String[] { "*.id", "*.description", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("inspectionInfringementTypes", new String[] { "*.id", "*.code", "*.description", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("inspectionOutcomeTypes", new String[] { "*.id", "*.code", "*.description", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("inspectionReportTypes", new String[] { "*.id", "*.code", "*.description", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("portEntryDenialReasons", new String[] { "*.id", "*.code", "*.description", "*.sourceSystem" });
		INCLUSION_PATTERNS_BY_CATEGORY.put("rfmoIuuLists", new String[] { "*.id", "*.rfmoId", "*.code", "*.url", "*.description", "*.sourceSystem" });	
	}
	
	@Inject private @Getter @Setter SpeciesFactsheetsFetcher speciesFSFetcher;
	@Inject private @Getter @Setter PSMFactsheetsFetcher psmFSFetcher;
	@Inject private @Getter @Setter VesselTypeFactsheetsFetcher vesselTypeFSFetcher;
	@Inject private @Getter @Setter GearTypeFactsheetsFetcher gearTypeFSFetcher;
	
	private Map<String, AbstractFilterableFactsheetFetcher<? extends FactsheetData>> factsheetFetchers;
	
	@Inject private @Getter @Setter CommonMetadataDAO metadataDAO;
	
	@Inject private @Getter @Setter SSystemsDAO systemsDAO;
	@Inject private @Getter @Setter SSystemsI18nDAO systemsI18nDAO;
	@Inject private @Getter @Setter SSystemsGroupsDAO systemsGroupsDAO;
	@Inject private @Getter @Setter SSystemsToGroupsDAO systemsToGroupsDAO;
	@Inject private @Getter @Setter SCountriesDAO countriesDAO;
	@Inject private @Getter @Setter SCountriesI18nDAO countriesI18nDAO;
	@Inject private @Getter @Setter SCountriesGroupDAO countriesGroupDAO;
	@Inject private @Getter @Setter SCountriesToGroupDAO countriesToGroupDAO;
	@Inject private @Getter @Setter SCountriesSubdivisionDAO countriesSubdivisionDAO;
	
	@Inject private @Getter @Setter ExtendedPortsDAO portsDAO;
	
	@Inject private @Getter @Setter SIdentifierTypesDAO identifierTypesDAO;
	@Inject private @Getter @Setter SHullMaterialDAO hullMaterialsDAO;
	@Inject private @Getter @Setter SVesselStatusDAO vesselStatusDAO;
	@Inject private @Getter @Setter SVesselTypesDAO vesselTypesDAO;
	@Inject private @Getter @Setter SVesselTypesI18nDAO vesselTypesI18nDAO;
	@Inject private @Getter @Setter SGearTypesDAO gearTypesDAO;
	@Inject private @Getter @Setter SGearTypesI18nDAO gearTypesI18nDAO;
	@Inject private @Getter @Setter SMeasureUnitsDAO measureUnitsDAO;
	@Inject private @Getter @Setter SLengthTypesDAO lengthTypesDAO;
	@Inject private @Getter @Setter SLengthTypesI18nDAO lengthTypesI18nDAO;
	@Inject private @Getter @Setter STonnageTypesDAO tonnageTypesDAO;
	@Inject private @Getter @Setter STonnageTypesI18nDAO tonnageTypesI18nDAO;
	@Inject private @Getter @Setter SPowerTypesDAO powerTypesDAO;
	@Inject private @Getter @Setter SLengthCategoriesDAO lengthCategoriesDAO;
	@Inject private @Getter @Setter STonnageCategoriesDAO tonnageCategoriesDAO;
	@Inject private @Getter @Setter SPowerCategoriesDAO powerCategoriesDAO;
	@Inject private @Getter @Setter SAgeCategoriesDAO ageCategoriesDAO;
	@Inject private @Getter @Setter SAuthorizationTypesDAO authorizationTypesDAO;
	@Inject private @Getter @Setter SAuthorizationTypesI18nDAO authorizationTypesI18nDAO;
	@Inject private @Getter @Setter SAuthorizationTerminationReasonsDAO authorizationTerminationReasonsDAO;
	
	@Inject private @Getter @Setter SAuthorizationHoldersDAO authorizationHoldersDAO;
	@Inject private @Getter @Setter SAuthorityRoleDAO authorityRolesDAO;
	@Inject private @Getter @Setter SInspectionInfringementTypeDAO inspectionInfringementTypesDAO;
	@Inject private @Getter @Setter SInspectionOutcomeTypeDAO inspectionOutcomeTypesDAO;
	@Inject private @Getter @Setter SInspectionReportTypeDAO inspectionReportTypesDAO;
	@Inject private @Getter @Setter SPortEntryDenialReasonDAO portEntryDenialsDAO;
	@Inject private @Getter @Setter SRfmoIuuListsDAO rfmoIuuListsDAO;
	
	/**
	 * Class constructor
	 */
	public ReferenceMetadataDataRetriever() throws Throwable {
		super();
		
		factsheetFetchers = new HashMap<String, AbstractFilterableFactsheetFetcher<? extends FactsheetData>>();
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		factsheetFetchers.put(this.getFactsheetDomain(SpeciesFactsheetData.class), speciesFSFetcher);
		factsheetFetchers.put(this.getFactsheetDomain(PSMFactsheetData.class), psmFSFetcher);
		factsheetFetchers.put(this.getFactsheetDomain(GearTypeFactsheetData.class), gearTypeFSFetcher);
		factsheetFetchers.put(this.getFactsheetDomain(VesselTypeFactsheetData.class), vesselTypeFSFetcher);
	}

	public String doGetPSMFactsheetDataByID(String[] availableSourceSystems, String fid, String format) throws Throwable {
		this._log.debug("Retrieving PSM factsheet by ID #" + fid);
		
		String content = null;

		PSMFactsheetData toReturn = psmFSFetcher.getData(fid);
		
		if(toReturn != null) {
			if("json".equals(format)) {
				content = new JSONResponse<PSMFactsheetData>(toReturn).JSONify();
			} else if("xml".equals(format)) {
				content = JAXBHelper.toXML(toReturn);
			}
		}
				
		return content;
	}
	
	public String doGetPSMFactsheetDataByIDPrefixes(String[] availableSourceSystems, final String[] idPrefixes, String format) throws Throwable {
		this._log.debug("Retrieving PSM factsheet by ID prefixes '{}'", idPrefixes == null || idPrefixes.length == 0 ? "[ NOT SET]" : CollectionsHelper.join(idPrefixes, ","));
		
		final Set<String> prefixes = new TreeSet<String>();
		
		if(idPrefixes != null && idPrefixes.length > 0)
			prefixes.addAll(Arrays.asList(idPrefixes));
		
		String content = null;

		List<String> PSMFactsheetIDs = 
			psmFSFetcher.fetchFactsheetIDs().stream().
				filter(I -> I != null && prefixes.contains(I.replaceAll("\\_.*", ""))).collect(Collectors.toList());
		
		SerializableCollection<PSMFactsheetData> results = new SerializableArrayList<PSMFactsheetData>();
		
		for(String ID : PSMFactsheetIDs) {
			results.add(psmFSFetcher.getData(ID));
		}
		
		if(results != null) {
			if("json".equals(format)) {
				content = new JSONResponse<SerializableCollection<PSMFactsheetData>>(results).JSONify();
			} else if("xml".equals(format)) {
				content = JAXBHelper.toXML(results);
			}
		}
				
		return content;
	}
	
	public String doGetSpeciesFactsheetDataByID(String[] availableSourceSystems, String fid, String format) throws Throwable {
		this._log.debug("Retrieving species factsheet by ID #" + fid);
		
		String content = null;

		SpeciesFactsheetData toReturn = speciesFSFetcher.getData(fid);
		
		if(toReturn != null) {
			if("json".equals(format)) {
				content = new JSONResponse<SpeciesFactsheetData>(toReturn).JSONify();
			} else if("xml".equals(format)) {
				content = JAXBHelper.toXML(toReturn);
			}
		}
				
		return content;
	}

	public String doGetPortByIDContent(String[] availableSourceSystems, Integer portID, String format) throws Throwable {
		this._log.debug("Retrieving port by ID #" + portID + " and sources " + CollectionsHelper.serializeArray(availableSourceSystems));
		
		Set<String> availableSources = new TreeSet<String>();
		
		if(availableSourceSystems != null)
			for(String source : availableSourceSystems)
				availableSources.add(source);
		
		ExtendedPort toReturn = null;
		
		try {
			toReturn = portsDAO.selectExtendedByPrimaryKey(portID);
		} catch (Throwable t) {
			this._log.error("Unable to retrieve port by ID #" + portID, t);
		} finally {
			if(toReturn == null)
				this._log.warn("No port can be retrieved by ID #" + portID);
			else
				this._log.debug("Retrieved port by ID #" + portID + ": " + toReturn);
		}

		String content = null;

		if(toReturn != null && availableSources.contains(toReturn.getSourceSystem())) {
			if("json".equals(format)) {
				content = new JSONResponse<ExtendedPort>(toReturn).JSONify();
			} else if("xml".equals(format)) {
				content = JAXBHelper.toXML(toReturn);
			}
		}
				
		return content;
	}
	
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, 
			   key="#root.method.name + " +
			   	   "#language + " + 
			   	   "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#availableSourceSystems) + " +
			   	   "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#categories) + " +
			   	   "#format")
	public String doGetReferenceDataContent(String language, String[] availableSourceSystems, String[] categories, String format) throws Throwable {
		SerializableMap<String, Serializable> resultMap = new SerializableHashMap<String, Serializable>();
		
		Set<String> finalCategories = new TreeSet<String>();
		List<String> systems = new ListSet<String>();
		systems.addAll(Arrays.asList(availableSourceSystems));
		
		Collections.sort(systems);
		
		if(categories != null) {
			String updatedCategory = null;
			String[] currentCategories = null;
			
			Method retriever = null;
			Serializable data = null;
			
			boolean isCategoryGroup = false;
			
			for(String category : categories) {				
				if(category == null || category.length() == 0)
					throw new RuntimeException("Cannot retrieve data for a NULL or empty category");

				isCategoryGroup = Pattern.matches("\\{.+\\}", category);
				
				if(isCategoryGroup) {					
					currentCategories = ReferenceMetadataDataRetriever.CATEGORY_GROUPS.get(category.replaceAll("\\{|\\}", ""));
					
					if(currentCategories == null || currentCategories.length == 0)
						throw new RuntimeException("Cannot retrieve data for a NULL or empty category group ('" + category.replaceAll("\\{|\\}", "") + "')");
				} else {
					currentCategories = new String[] { category };
				}
				
				for(String currentCategory : currentCategories) {
					finalCategories.add(currentCategory);
					
					updatedCategory = currentCategory.substring(0, 1).toUpperCase() + ( currentCategory.length() > 1 ? currentCategory.substring(1) : "");
	
					try {
						retriever = this.getClass().getDeclaredMethod("doGet" + updatedCategory, String.class, List.class);
						
						data = (Serializable)retriever.invoke(this, language, systems);
					} catch (NoSuchMethodException NSMe) {
						try {
							retriever = this.getClass().getDeclaredMethod("doGet" + updatedCategory, List.class);

							data = (Serializable)retriever.invoke(this, systems);
						} catch (NoSuchMethodException NSM2e) {
							throw new RuntimeException("Unknown category '" + currentCategory + "'");
						}
					}
					
					if(data != null) {
						if(Collection.class.isAssignableFrom(data.getClass()))
							this._log.debug("Retrieved " + ((Collection<?>)data).size() + " elements for metadata '" + currentCategory + "'");
						else
							this._log.debug("Metadata '" + currentCategory + "' retrieved");

						resultMap.put(currentCategory, data);
					}
				}
			}
		}
		
		String content = null;
		
		if("json".equals(format)) {
			List<String> inclusionPatterns = new ArrayList<String>();
			
			String[] patterns = null;
			for(String key : resultMap.keySet()) {
				patterns = INCLUSION_PATTERNS_BY_CATEGORY.get(key);
				
				if(patterns != null && patterns.length > 0) {
					for(String currentPattern : patterns)
						inclusionPatterns.add("data." + key + "." + currentPattern);
				}
			}
			
			content = new JSONResponse<Serializable>(resultMap).JSONifyOnly(inclusionPatterns.toArray(new String[inclusionPatterns.size()]));
		} else if("xml".equals(format)) {
			content = JAXBHelper.toXML(resultMap);
		}
		
		return content;
	}

	private SerializableList<SSystems> doGetSystems(String lang, List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing systems data...");
		
		try {
			SSystemsExample filter = new SSystemsExample();
			SSystemsExample.Criteria criteria = filter.createCriteria().andVesselSourceEqualTo(Boolean.TRUE);
			
			if(managedSystems != null && !managedSystems.isEmpty())
				criteria.andIdIn(managedSystems);
			else
				criteria.andIsPublicEqualTo(Boolean.TRUE);
			
			filter.setOrderByClause("name asc");
						
			this._log.debug("Systems data have been initialized!");
			
			List<SSystems> systems = new ArrayList<SSystems>();
			
			SSystemsI18n byLang;
			
			for(SSystems in : systemsDAO.selectByExample(filter)) {
				byLang = systemsI18nDAO.selectByPrimaryKey(in.getId(), lang);
				
				if(byLang != null)
					BeansHelper.transferBean(byLang, in);
				
				systems.add(in);
			};
			
			return this.toSerializableList(systems);
		} catch (Throwable t) {
			this._log.error("Unable to initialize systems data", t);
		}			
		
		return null;
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SSystems> doGetOtherSystems(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing other systems data...");
		
		try {
			SSystemsExample filter = new SSystemsExample();
			SSystemsExample.Criteria criteria = filter.createCriteria().andVesselSourceEqualTo(Boolean.FALSE).andAuthSourceEqualTo(Boolean.FALSE);
			
			if(managedSystems != null && !managedSystems.isEmpty())
				criteria.andIdIn(managedSystems);
			else
				criteria.andIsPublicEqualTo(Boolean.TRUE);
			
			filter.setOrderByClause("name asc");
						
			this._log.debug("Other systems data have been initialized!");
			
			return this.toSerializableList(systemsDAO.selectByExample(filter));
		} catch (Throwable t) {
			this._log.error("Unable to initialize other systems data", t);
		}			
		
		return null;
	}
	
	@SuppressWarnings("unused")
	private SerializableList<ExtendedSystemsGroups> doGetSystemsGroups(String lang, List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing systems groups data...");
		
		Map<String, SSystems> systemsMap = this.buildSystemsMap(this.doGetSystems(lang, managedSystems));
		
		List<ExtendedSystemsGroups> systemGroups = new ArrayList<ExtendedSystemsGroups>();
		
		try {
			SSystemsGroupsExample filter = new SSystemsGroupsExample();
			filter.createCriteria().andIdNotEqualTo(5);
			filter.setOrderByClause("description asc");

			ExtendedSystemsGroups extendedGroup;
			SSystemsToGroupsExample groupFilter;
			SSystems currentSystem;
			for(SSystemsGroups group : systemsGroupsDAO.selectByExample(filter)) {
				extendedGroup = new ExtendedSystemsGroups(group);
				
				groupFilter = new SSystemsToGroupsExample();
				groupFilter.createCriteria().andGroupIdEqualTo(group.getId());
				
				for(SSystemsToGroups linkedSystems : systemsToGroupsDAO.selectByExample(groupFilter)) {
					if((currentSystem = systemsMap.get(linkedSystems.getSystemId())) != null) {
						extendedGroup.getSystems().add(currentSystem);
					}
				}
				
				if(extendedGroup.getSystems().size() > 0)
					systemGroups.add(extendedGroup);
			}
						
			this._log.debug("Systems groups data have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize systems groups data", t);
		}	
		
		return this.toSerializableList(systemGroups);
	}
			
	private SerializableList<SSystems> doGetAuthorizationSystems(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing authorization issuer systems data...");
		
		try {
			return this.toSerializableList(metadataDAO.getAuthorizationsIssuerSystems(managedSystems));
		} catch (Throwable t) {
			this._log.error("Unable to initialize authorization issuer systems data", t);
		}
		
		return null;
	}
		
	@SuppressWarnings("unused")
	private SerializableList<ExtendedSystemsGroups> doGetAuthorizationSystemsGroups(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing authorization issuers systems groups data...");
		
		Map<String, SSystems> systemsMap = this.buildSystemsMap(this.doGetAuthorizationSystems(managedSystems));
		
		List<ExtendedSystemsGroups> systemGroups = new ArrayList<ExtendedSystemsGroups>();
		
		try {
			ExtendedSystemsGroups extendedGroup;
			SSystemsToGroupsExample groupFilter;
			for(SSystemsGroups group : metadataDAO.getAuthorizationsIssuerSystemsGroups(new ArrayList<String>(systemsMap.keySet()))) {
				extendedGroup = new ExtendedSystemsGroups(group);
				
				groupFilter = new SSystemsToGroupsExample();
				
				if(group.getId() != null)
					groupFilter.createCriteria().andGroupIdEqualTo(group.getId());
				
				for(SSystemsToGroups linkedSystems : systemsToGroupsDAO.selectByExample(groupFilter)) {
					if(systemsMap.containsKey(linkedSystems.getSystemId()))
						extendedGroup.getSystems().add(systemsMap.get(linkedSystems.getSystemId()));
				}
				
				if(extendedGroup.getSystems().size() > 0)
					systemGroups.add(extendedGroup);
			}
			
			this._log.debug("Authorization issuers systems groups data have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize suthorization issuers systems groups data", t);
		}		
		
		return this.toSerializableList(systemGroups);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SIdentifierTypes> doGetIdentifiers(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing vessel identifier types data...");
		
		List<SIdentifierTypes> identifierTypes = new ListSet<SIdentifierTypes>();
		
		try {
			SIdentifierTypesExample filter = new SIdentifierTypesExample();
			filter.createCriteria().andIsPublicEqualTo(Boolean.TRUE);
			filter.or(filter.createCriteria().andIsPublicEqualTo(Boolean.FALSE).andSourceSystemIn(managedSystems));
			filter.setOrderByClause("PRIORITY ASC");
			
			identifierTypes.addAll(identifierTypesDAO.selectByExample(filter));
			
			this._log.debug("Vessel identifier types data have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize vessel identifier types data", t);
		}
		
		return this.toSerializableList(identifierTypes);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SCountries> doGetCountries(String lang, List<String> managedSystems) throws Throwable {
		this._log.debug("Retrieving countries data...");
		
		List<SCountries> countries = new ArrayList<SCountries>();
			
		try {
			SCountriesExample filter = new SCountriesExample();
			
			filter.setOrderByClause("name asc");
			
			SCountriesI18n byLang;
			for(SCountries country : countriesDAO.selectByExample(filter)) {
				byLang = countriesI18nDAO.selectByPrimaryKey(country.getId(), lang);
				
				if(byLang != null)
					BeansHelper.transferBean(byLang, country);
				
				countries.add(country);
			}
			
			this._log.debug("Countries data have been retrieved!");
		} catch (Throwable t) {
			this._log.error("Unable to retrieve countries data", t);
		}		
		
		return this.toSerializableList(countries);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<ExtendedCountriesGroup> doGetCountriesGroups(String lang, List<String> managedSystems) throws Throwable {
		this._log.debug("Retrieving countries groups data...");
		
		List<ExtendedCountriesGroup> countriesGroups = new ArrayList<ExtendedCountriesGroup>();
			
		try {
			SCountriesGroupExample countriesGroupFilter = new SCountriesGroupExample();
			//filter.createCriteria().andIso2CodeIsNotNull();
			countriesGroupFilter.setOrderByClause("id asc");
			
			ExtendedCountriesGroup extendedCountriesGroup;
			
			SCountriesToGroupExample countriesToGroupFilter;
			SCountriesExample countriesFilter;
			
			List<SCountriesToGroup> groups;
			
			List<Integer> countryIDs;
			
			for(SCountriesGroup countriesGroup : countriesGroupDAO.selectByExample(countriesGroupFilter)) {
				countryIDs = new ListSet<Integer>();
				
				countriesToGroupFilter = new SCountriesToGroupExample();
				countriesToGroupFilter.createCriteria().andGroupIdEqualTo(countriesGroup.getId());
			
				groups = countriesToGroupDAO.selectByExample(countriesToGroupFilter);
				
				if(groups != null && !groups.isEmpty()) {
					for(SCountriesToGroup groupData : groups)
						countryIDs.add(groupData.getCountryId());
				}
				
				if(!countryIDs.isEmpty()) {
					countriesFilter = new SCountriesExample();
					countriesFilter.createCriteria().andIdIn(countryIDs);
					
					extendedCountriesGroup = new ExtendedCountriesGroup(countriesGroup, countriesDAO.selectByExample(countriesFilter));
					
					countriesGroups.add(extendedCountriesGroup);
				}
			}
			
			this._log.debug("Countries groups data have been retrieved!");
		} catch (Throwable t) {
			this._log.error("Unable to retrieve countries groups data", t);
		}		
		
		return this.toSerializableList(countriesGroups);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SCountriesSubdivision> doGetSubdivisions(List<String> managedSystems) throws Throwable {
		this._log.debug("Retrieving subdivisions data...");
		
		List<SCountriesSubdivision> subdivisions = new ArrayList<SCountriesSubdivision>();
			
		try {
			SCountriesSubdivisionExample filter = new SCountriesSubdivisionExample();
			//filter.createCriteria().andIso2CodeIsNotNull();
			filter.setOrderByClause("name asc");
			
			for(SCountriesSubdivision subdivision : countriesSubdivisionDAO.selectByExample(filter)) {
				subdivisions.add(subdivision);
			}
			
			this._log.debug("Subdivisions data have been retrieved!");
		} catch (Throwable t) {
			this._log.error("Unable to retrieve subdivisions data", t);
		}		
		
		return this.toSerializableList(subdivisions);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SVesselStatus> doGetVesselStatus(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing vessel status data...");
		
		List<SVesselStatus> vesselStatus = new ArrayList<SVesselStatus>();
		
		try {			
			SVesselStatusExample filter = new SVesselStatusExample();
			filter.createCriteria().andSourceSystemIn(managedSystems);
			filter.setOrderByClause("SOURCE_SYSTEM DESC, DESCRIPTION ASC");
			
			vesselStatus = vesselStatusDAO.selectByExample(filter);
			
			this._log.debug("Vessel status data have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize vessel status data", t);
		}
		
		return this.toSerializableList(vesselStatus);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SHullMaterial> doGetHullMaterials(List<String> managedSystems) {
		this._log.debug("Initializing hull material types...");
		
		List<SHullMaterial> hullMaterials = new ArrayList<SHullMaterial>();
		
		try {	
			SHullMaterialExample filter = new SHullMaterialExample();
			SHullMaterialExample.Criteria criteria = filter.createCriteria();
			
			criteria.andSourceSystemIn(managedSystems);
			
			filter.setOrderByClause("DESCRIPTION ASC");
			hullMaterials = hullMaterialsDAO.selectByExample(filter);
			
			this._log.debug("The hull materials have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the hull materials", t);
		}
		
		return this.toSerializableList(hullMaterials);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SVesselTypes> doGetVesselTypes(String lang, List<String> managedSystems) throws Throwable {
		return this.doGetVesselTypes(lang, managedSystems, false);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SVesselTypes> doGetVesselTypesCategories(String lang, List<String> managedSystems) throws Throwable {
		return this.doGetVesselTypes(lang, managedSystems, true);
	}
	
	private SerializableList<SVesselTypes> doGetVesselTypes(String lang, List<String> managedSystems, boolean mainCategoriesOnly) {
		this._log.debug("Initializing vessel types" + ( mainCategoriesOnly ? " categories" : "" ) + "...");
		
		List<SVesselTypes> vesselTypes = new ArrayList<SVesselTypes>();
		
		try {	
			SVesselTypesExample filter = new SVesselTypesExample();
			SVesselTypesExample.Criteria criteria = filter.createCriteria();
			
			criteria.andSourceSystemIn(managedSystems);
			
			if(mainCategoriesOnly)
				criteria.andIsscfvCodeIsNotNull().andIsscfvCodeLike("%.0.0");
			
			filter.setOrderByClause("ISSCFV_CODE ASC, STANDARD_ABBREVIATION ASC, SOURCE_SYSTEM ASC, NAME ASC");
			
			SVesselTypesI18n byLang;
			for(SVesselTypes in : vesselTypesDAO.selectByExample(filter)) {
				byLang = vesselTypesI18nDAO.selectByPrimaryKey(in.getId(), lang);
				
				if(byLang != null)
					BeansHelper.transferBean(byLang, in);
				
				vesselTypes.add(in);
			};
			
			this._log.debug("The vessel types" + ( mainCategoriesOnly ? " categories" : "" ) + " have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the vessel types" + ( mainCategoriesOnly ? " categories" : "" ), t);
		}
		
		return this.toSerializableList(vesselTypes);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SGearTypes> doGetGearTypes(String lang, List<String> managedSystems) throws Throwable {
		return this.doGetGearTypes(lang, managedSystems, false);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SGearTypes> doGetGearTypesCategories(String lang, List<String> managedSystems) throws Throwable {
		return this.doGetGearTypes(lang, managedSystems, true);
	}
	
	private SerializableList<SGearTypes> doGetGearTypes(String lang, List<String> managedSystems, boolean mainCategoriesOnly) {
		this._log.debug("Initializing gear types" + ( mainCategoriesOnly ? " categories" : "" ) + "...");
		
		List<SGearTypes> gearTypes = new ArrayList<SGearTypes>();
		
		try {	
			SGearTypesExample filter = new SGearTypesExample();
			SGearTypesExample.Criteria criteria = filter.createCriteria();
			
			criteria.andSourceSystemIn(managedSystems);
			
			if(mainCategoriesOnly)
				criteria.andIsscfgCodeIsNotNull().andIsscfgCodeLike("%.0.0");
			
			filter.setOrderByClause("ISSCFG_CODE ASC, STANDARD_ABBREVIATION ASC, SOURCE_SYSTEM ASC, NAME ASC");
			
			SGearTypesI18n byLang;
			for(SGearTypes in : gearTypesDAO.selectByExample(filter)) {
				byLang = gearTypesI18nDAO.selectByPrimaryKey(in.getId(), lang);
				
				if(byLang != null)
					BeansHelper.transferBean(byLang, in);
				
				gearTypes.add(in);
			}
			
			this._log.debug("The gear types" + ( mainCategoriesOnly ? " categories" : "" ) + " have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the gear types" + ( mainCategoriesOnly ? " categories" : "" ), t);
		}
		
		return this.toSerializableList(gearTypes);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SMeasureUnits> doGetMeasureUnits(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing measure units...");
		
		List<SMeasureUnits> measureUnits = new ArrayList<SMeasureUnits>();
		
		try {	
			SMeasureUnitsExample filter = new SMeasureUnitsExample();
			
			measureUnits = measureUnitsDAO.selectByExample(filter);
			
			this._log.debug("The measure units have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the measure units", t);
		}
		
		return this.toSerializableList(measureUnits);
	}

	@SuppressWarnings("unused")
	private SerializableList<SLengthTypes> doGetLengthTypes(String lang, List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing length types...");
		
		List<SLengthTypes> lengthTypes = new ArrayList<SLengthTypes>();
		
		try {	
			SLengthTypesExample filter = new SLengthTypesExample();
			filter.createCriteria().andSourceSystemIn(managedSystems);
			
			SLengthTypesI18n byLang;
			for(SLengthTypes in : lengthTypesDAO.selectByExample(filter)) {
				byLang = lengthTypesI18nDAO.selectByPrimaryKey(in.getId(), lang);
				
				if(byLang != null)
					BeansHelper.transferBean(byLang, in);
				
				lengthTypes.add(in);
			};
			
			this._log.debug("The length types have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the length types", t);
		}
		
		return this.toSerializableList(lengthTypes);
	}

	@SuppressWarnings("unused")
	private SerializableList<SLengthCategories> doGetLengthRanges(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing length ranges...");
		
		List<SLengthCategories> lengthCategories = new ArrayList<SLengthCategories>();
		
		try {
			lengthCategories = lengthCategoriesDAO.selectByExample(new SLengthCategoriesExample());
			
			this._log.debug("The length ranges have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the length ranges", t);
		}
		
		return this.toSerializableList(lengthCategories);
	}

	@SuppressWarnings("unused")
	private SerializableList<STonnageTypes> doGetTonnageTypes(String lang, List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing tonnage types...");
		
		List<STonnageTypes> tonnageTypes = new ArrayList<STonnageTypes>();
		
		try {	
			STonnageTypesExample filter = new STonnageTypesExample();
			filter.createCriteria().andSourceSystemIn(managedSystems);
			
			STonnageTypesI18n byLang;
			for(STonnageTypes in : tonnageTypesDAO.selectByExample(filter)) {
				byLang = tonnageTypesI18nDAO.selectByPrimaryKey(in.getId(), lang);
				
				if(byLang != null)
					BeansHelper.transferBean(byLang, in);
				
				tonnageTypes.add(in);
			};
			
			this._log.debug("The tonnage types have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the tonnage types", t);
		}
		
		return this.toSerializableList(tonnageTypes);
	}

	@SuppressWarnings("unused")
	private SerializableList<STonnageCategories> doGetTonnageRanges(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing tonnage ranges...");
		
		List<STonnageCategories> tonnageCategories = new ArrayList<STonnageCategories>();
		
		try {
			tonnageCategories = tonnageCategoriesDAO.selectByExample(new STonnageCategoriesExample());
			
			this._log.debug("The tonnage ranges have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the tonnage ranges", t);
		}
		
		return this.toSerializableList(tonnageCategories);
	}

	@SuppressWarnings("unused")
	private SerializableList<SPowerTypes> doGetPowerTypes(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing power types...");
		
		List<SPowerTypes> powerTypes = new ArrayList<SPowerTypes>();
		
		try {
			SPowerTypesExample filter = new SPowerTypesExample();
			filter.createCriteria().andSourceSystemIn(managedSystems);
				
			powerTypes = powerTypesDAO.selectByExample(filter);
			
			this._log.debug("The power types have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the power types", t);
		}
		
		return this.toSerializableList(powerTypes);	
	}

	@SuppressWarnings("unused")
	private SerializableList<SPowerCategories> doGetPowerRanges(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing power ranges...");
		
		List<SPowerCategories> powerCategories = new ArrayList<SPowerCategories>();
		
		try {
			powerCategories = powerCategoriesDAO.selectByExample(new SPowerCategoriesExample());
			
			this._log.debug("The power ranges have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the power ranges", t);
		}
		
		return this.toSerializableList(powerCategories);
	}

	@SuppressWarnings("unused")
	private SerializableList<SAgeCategories> doGetAgeRanges(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing age ranges...");
		
		List<SAgeCategories> ageCategories = new ArrayList<SAgeCategories>();
		
		try {
			ageCategories = ageCategoriesDAO.selectByExample(new SAgeCategoriesExample());
			
			this._log.debug("The age ranges have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the age ranges", t);
		}
		
		return this.toSerializableList(ageCategories);
	}

	@SuppressWarnings("unused")
	private SerializableList<SAuthorizationTypes> doGetAuthorizationTypes(String lang, List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing authorization types data...");
		
		List<SAuthorizationTypes> authorizationTypes = new ListSet<SAuthorizationTypes>();
		
		try {
			SAuthorizationTypesExample filter = new SAuthorizationTypesExample();
			filter.createCriteria().andSourceSystemIn(managedSystems);
		
			SAuthorizationTypesI18n byLang;
			
			for(SAuthorizationTypes in : authorizationTypesDAO.selectByExample(filter)) {
				byLang = authorizationTypesI18nDAO.selectByPrimaryKey(in.getId(), lang, in.getSourceSystem());
				
				if(byLang != null)
					BeansHelper.transferBean(byLang, in);
				
				authorizationTypes.add(in);
			}
			
			this._log.debug("The authorization types data has been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the authorization types data", t);
		}
		
		return this.toSerializableList(authorizationTypes);
	}

	@SuppressWarnings("unused")
	private SerializableList<SAuthorizationTerminationReasons> doGetAuthorizationTerminationReasons(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing authorization terminations reasons...");
		
		List<SAuthorizationTerminationReasons> authorizationTerminationReasons = new ArrayList<SAuthorizationTerminationReasons>();	
		
		try {
			authorizationTerminationReasons = authorizationTerminationReasonsDAO.selectByExample(new SAuthorizationTerminationReasonsExample());
			
			this._log.debug("The authorization termination reasons have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the authorization termination reasons", t);
		}
		
		return this.toSerializableList(authorizationTerminationReasons);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SAuthorityRole> doGetAuthorityRoles(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing authority roles...");
		
		List<SAuthorityRole> authorityRoles = new ArrayList<SAuthorityRole>();
		
		try {	
			SAuthorityRoleExample filter = new SAuthorityRoleExample();
			
			authorityRoles = authorityRolesDAO.selectByExample(filter);
			
			this._log.debug("The authority roles have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the authority roles", t);
		}
		
		return this.toSerializableList(authorityRoles);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SAuthorizationHolders> doGetAuthorizationHolders(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing authorization holders...");
		
		List<SAuthorizationHolders> authorizationHolders = new ArrayList<SAuthorizationHolders>();
		
		try {	
			SAuthorizationHoldersExample filter = new SAuthorizationHoldersExample();
			
			authorizationHolders = authorizationHoldersDAO.selectByExample(filter);
			
			this._log.debug("The authorization holders have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the authorization holders", t);
		}
		
		return this.toSerializableList(authorizationHolders);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SInspectionReportType> doGetInspectionReportTypes(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing inspection report types...");
		
		List<SInspectionReportType> inspectionReportTypes = new ArrayList<SInspectionReportType>();
		
		try {	
			SInspectionReportTypeExample filter = new SInspectionReportTypeExample();
			
			inspectionReportTypes = inspectionReportTypesDAO.selectByExample(filter);
			
			this._log.debug("The inspection report types have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the inspection report types", t);
		}
		
		return this.toSerializableList(inspectionReportTypes);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SInspectionOutcomeType> doGetInspectionOutcomeTypes(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing inspection outcome types...");
		
		List<SInspectionOutcomeType> inspectionOutcomeTypes = new ArrayList<SInspectionOutcomeType>();
		
		try {	
			SInspectionOutcomeTypeExample filter = new SInspectionOutcomeTypeExample();
			
			inspectionOutcomeTypes = inspectionOutcomeTypesDAO.selectByExample(filter);
			
			this._log.debug("The inspection outcome types have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the inspection outcome types", t);
		}
		
		return this.toSerializableList(inspectionOutcomeTypes);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SInspectionInfringementType> doGetInspectionInfringementTypes(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing inspection infringement types...");
		
		List<SInspectionInfringementType> inspectionInfringementTypes = new ArrayList<SInspectionInfringementType>();
		
		try {	
			SInspectionInfringementTypeExample filter = new SInspectionInfringementTypeExample();
			
			inspectionInfringementTypes = inspectionInfringementTypesDAO.selectByExample(filter);
			
			this._log.debug("The inspection infringement types have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the inspection infringement types", t);
		}
		
		return this.toSerializableList(inspectionInfringementTypes);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SRfmoIuuLists> doGetRfmoIuuLists(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing RFMO IUU lists...");
		
		List<SRfmoIuuLists> rfmoIuuListsExample = new ArrayList<SRfmoIuuLists>();
		
		try {	
			SRfmoIuuListsExample filter = new SRfmoIuuListsExample();
			
			rfmoIuuListsExample = rfmoIuuListsDAO.selectByExample(filter);
			
			this._log.debug("The RFMO IUU lists have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the RFMO IUU lists", t);
		}
		
		return this.toSerializableList(rfmoIuuListsExample);
	}
	
	@SuppressWarnings("unused")
	private SerializableList<SPortEntryDenialReason> doGetPortEntryDenialReasons(List<String> managedSystems) throws Throwable {
		this._log.debug("Initializing Port Entry Denial Reasons...");
		
		List<SPortEntryDenialReason> rfmoIuuListsExample = new ArrayList<SPortEntryDenialReason>();
		
		try {	
			SPortEntryDenialReasonExample filter = new SPortEntryDenialReasonExample();
			
			rfmoIuuListsExample = portEntryDenialsDAO.selectByExample(filter);
			
			this._log.debug("The Port Entry Denial Reasons have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize the Port Entry Denial Reasons", t);
		}
		
		return this.toSerializableList(rfmoIuuListsExample);
	}

	private String getFactsheetDomain(Class<? extends FactsheetData> factsheetType) {
		if(factsheetType == null)
			return null;
		
		Domain domain = factsheetType.getAnnotation(Domain.class);
		
		if(domain != null)
			return domain.value();
		
		return null;
	}
	
	@SuppressWarnings("unused")
	private FactsheetMetadata<VesselTypeFactsheetData> doGetVesselTypeFactsheets(List<String> managedSystems) {
		String domain = this.getFactsheetDomain(VesselTypeFactsheetData.class);
		
		return new FactsheetMetadata<VesselTypeFactsheetData>(domain, 
															  this.getAvailableFactsheetIDs(domain),
															  null /*this.getAvailableFactsheets(VesselTypeFactsheetData.class, domain) */);
	}

	@SuppressWarnings("unused")
	private FactsheetMetadata<GearTypeFactsheetData> doGetGearTypeFactsheets(List<String> managedSystems) {
		String domain = this.getFactsheetDomain(GearTypeFactsheetData.class);
		
		return new FactsheetMetadata<GearTypeFactsheetData>(domain, 
														    this.getAvailableFactsheetIDs(domain),
														    null /* this.getAvailableFactsheets(GearTypeFactsheetData.class, domain) */);
	}

	@SuppressWarnings("unused")
	private FactsheetMetadata<PSMFactsheetData> doGetPortStateMeasureFactsheets(List<String> managedSystems) {
		String domain = this.getFactsheetDomain(PSMFactsheetData.class);
		
		return new FactsheetMetadata<PSMFactsheetData>(domain, 
													   this.getAvailableFactsheetIDs(domain),
													   null /* this.getAvailableFactsheets(PSMFactsheetData.class, domain) */);
	}
	
	@SuppressWarnings("unused")
	private FactsheetMetadata<SpeciesFactsheetData> doGetSpeciesFactsheets(List<String> managedSystems) {
		String domain = this.getFactsheetDomain(SpeciesFactsheetData.class);
		
		return new FactsheetMetadata<SpeciesFactsheetData>(domain, 
														   this.getAvailableFactsheetIDs(domain),
														   null /* this.getAvailableFactsheets(SpeciesFactsheetData.class, domain) */);
	}
	
	protected Collection<String> getAvailableFactsheetIDs(String domain) {
		Collection<String> result = null;
		
		try {
			AbstractFilterableFactsheetFetcher<? extends FactsheetData> fetcher = factsheetFetchers.get(domain);
			
			if(fetcher == null) {
				this._log.error("Unable to retrieve proper factsheets fetcher for domain '{}'", domain);
			} else {
				result = fetcher.fetchFactsheetIDs();
				
				this._log.info("{} available factsheets for domain '{}'", result.size(), domain);
			}
		} catch(Throwable t) {
			this._log.error("Unable to retrieve available factsheets for domain '{}' [ Reason: {} / {} ]", new Object[] { domain, t.getClass().getSimpleName(), t.getMessage() });
		}

		return result;
	}
	
	@SuppressWarnings("unchecked")
	protected <F extends FactsheetData> Collection<F> getAvailableFactsheets(Class<F> type, String domain) {
		Collection<F> result = null;
		
		try {
			AbstractFilterableFactsheetFetcher<?> fetcher = factsheetFetchers.get(domain);
			
			if(fetcher == null) {
				this._log.error("Unable to retrieve proper factsheets fetcher for domain '{}'", domain);
			} else {
				result = new ArrayList<F>();
				
				for(F data : (Collection<F>)fetcher.getAllData())
					result.add(data);
				
				this._log.info("{} available factsheets for domain '{}'", result.size(), domain);
			}
		} catch(Throwable t) {
			this._log.error("Unable to retrieve available factsheets for domain '{}' [ Reason: {} / {} ]", new Object[] { domain, t.getClass().getSimpleName(), t.getMessage() });
		}

		return result;
	}
	
	protected Map<String, SSystems> buildSystemsMap(List<SSystems> managedSystems) {
		this._log.debug("Initializing systems map data...");
				
		Map<String, SSystems> systemsMap = new HashMap<String, SSystems>();
		
		try {			
			if(managedSystems != null)
				for(SSystems system : managedSystems)
					systemsMap.put(system.getId(), system);	
			
			this._log.debug("Systems map data have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize systems map data", t);
		}			
		
		return systemsMap;
	}
	
	protected <S extends Serializable> SerializableList<S> toSerializableList(List<S> toSerialize) {
		if(toSerialize == null)
			return null;
		
		return new SerializableArrayList<S>(toSerialize);
	}
}
