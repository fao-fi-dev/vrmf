/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.retrieve;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.model.core.auth.UserModel;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.vrmf.business.dao.CommonVesselRecordSearchDAO;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.fao.fi.vrmf.common.web.controllers.services.pub.CommonPublicServicesConstants;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 May 2012
 */
@Singleton @Controller
@RequestMapping(CommonPublicServicesConstants.VESSELS_SERVICES_PREFIX + "get/*")
public class GetVesselDataController<S extends CommonVesselRecordSearchDAO<VesselRecord, VesselRecordSearchFilter>> extends CommonRetrieveVesselDataController<VesselRecord, VesselRecordSearchFilter> {
	static final protected String[] DEFAULT_EXCLUSION_PATTERNS = {
		"*.checksum",
		"*.currentFlag", 
		"*.currentFlagId",
		"*.currentName",
		"*.currentLength",
		"*.currentTonnage",
		"*.mappingComment",
		"*.mappingDate",
		"*.mappingUser",
		"*.mappingWeight",
		"*.mapsTo",
		"*.empty",
		"*.allIdentifiers",
		"*.identifiers",
		"*.flags",
		"*.shipbuildersData",
		"*.statusData",
		"*.typeData",
		"*.gearData",
		"*.callsigns",
		"*.registrationData",
		"*.fishingLicenses",
		"*.lengths",
		"*.tonnages",
		"*.powers",
		"*.ownersData",
		"*.operatorsData",
		"*.authorizationsData",
		"*.comment",
		"*.keywords"
	};

	@RequestMapping(value="uid/{uid:[0-9]+$}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getByUID(HttpServletRequest request, @PathVariable Integer uid) {
		try {
			UserModel loggedUser = ServletsHelper.getLoggedUser(request);

			return this.doRetrieveByUID(request, 
										INCLUDE_OTHER_SOURCES_BY_DEFAULT,
										null, //no specific sources
										uid, 
										"json", 
										null, //no jsonp callback 
										CollectionsHelper.append(DEFAULT_EXCLUSION_PATTERNS, loggedUser == null || !loggedUser.can("READ_I_DATA") ? new String[] { "*.internalIdentifiers" } : new String[0]), 
										NO_REQUIRED_ROLES, 
										NO_REQUIRED_CAPABILITIES);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}

	@RequestMapping(value="id/{id:[0-9]+$}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getByID(HttpServletRequest request, @PathVariable Integer id) {
		try {
			UserModel loggedUser = ServletsHelper.getLoggedUser(request);

			return this.doRetrieveByID(request, 
									   INCLUDE_OTHER_SOURCES_BY_DEFAULT,
									   null, //no specific sources
									   id, 
									   "json", 
									   null, //no jsonp callback 
									   CollectionsHelper.append(DEFAULT_EXCLUSION_PATTERNS, loggedUser == null || !loggedUser.can("READ_I_DATA") ? new String[] { "*.internalIdentifiers" } : new String[0]), 
									   NO_REQUIRED_ROLES, 
									   NO_REQUIRED_CAPABILITIES);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.controllers.services.pub.vessels.retrieve.AbstractRetrieveVesselDataController#getEmptySearchFilter()
	 */
	@Override
	protected VesselRecordSearchFilter getEmptySearchFilter() {
		return new VesselRecordSearchFilter();
	}
}