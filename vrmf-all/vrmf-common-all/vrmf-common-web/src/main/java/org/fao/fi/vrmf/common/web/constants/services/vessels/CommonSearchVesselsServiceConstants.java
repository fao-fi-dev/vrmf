/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.constants.services.vessels;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Dec 2010
 */
public interface CommonSearchVesselsServiceConstants {
	String VESSELS_TABLE_PARAMETER	  = "vt";
	
	String IDENTIFIER_TYPE_PARAMETER  = "it";
	String IDENTIFIER_VALUE_PARAMETER = "id";
	
	String IDS_VALUE_PARAMETER  = "ids";
	String UIDS_VALUE_PARAMETER = "uids";
	
	String NUM_SOURCES_FROM = "sf";
	String NUM_SOURCES_TO   = "st";

	String UPDATED_FROM = "uf";
	String UPDATED_TO   = "ut";

	String VESSEL_BUILDING_YEAR_MIN_PARAMETER = "bym";
	String VESSEL_BUILDING_YEAR_MAX_PARAMETER = "byM";
	
	String NO_TYPE						   = "not";
	String VESSEL_TYPE_PARAMETER		   = "t";
	
	String NO_PARENT_TYPE				   = "nopt";
	String PARENT_VESSEL_TYPE_PARAMETER	   = "pt";

	
	String VESSEL_PRIMARY_GEAR_TYPE_PARAMETER   = "pg";
	String VESSEL_SECONDARY_GEAR_TYPE_PARAMETER = "sg";

	String VESSEL_STATUS_PARAMETER		   = "u";
	
	String NO_FLAG						   = "nof";
	String VESSEL_FLAG_PARAMETER 		   = "f";
	
	String VESSEL_NAME_PARAMETER		   = "n";
	String VESSEL_EXT_MARKING_PARAMETER	   = "e";

	String VESSEL_IRCS_PARAMETER		   = "i";
	String VESSEL_MMSI_PARAMETER		   = "m";
	
	String VESSEL_REG_COUNTRY_PARAMETER    = "pc";
	String VESSEL_REG_PORT_PARAMETER	   = "p";
	String VESSEL_REG_PORT_NAME_PARAMETER  = "pn";
	String VESSEL_REG_NO_PARAMETER	   	   = "r";
	
	String VESSEL_F_LIC_COUNTRY_PARAMETER  = "flc";
	String VESSEL_F_LIC_NUMBER_PARAMETER   = "fln";
	
	String NO_LENGTH					   = "nol";
	String VESSEL_LENGTH_TYPES 			   = "lt";
	String VESSEL_LENGTH_MIN 			   = "lm";
	String VESSEL_LENGTH_MAX 			   = "lM";
	String VESSEL_LENGTHS				   = "ls";

	String VESSEL_TONNAGE_TYPES 		   = "tt";
	String VESSEL_TONNAGE_MIN 			   = "tm";
	String VESSEL_TONNAGE_MAX 			   = "tM";
	
	String VESSEL_MAIN_ENGINE_TYPES		   = "met";
	String VESSEL_MAIN_ENGINE_MIN		   = "mem";
	String VESSEL_MAIN_ENGINE_MAX		   = "meM";

	String VESSEL_AUXILIARY_ENGINE_TYPES   = "aet";
	String VESSEL_AUXILIARY_ENGINE_MIN	   = "aem";
	String VESSEL_AUXILIARY_ENGINE_MAX	   = "aeM";

	String VESSEL_AUTH_ISSUING_AUTHORITY   = "aia";
	String VESSEL_AUTH_ISSUING_COUNTRY 	   = "aic";
	String VESSEL_AUTH_TYPE 			   = "at";
	String VESSEL_AUTH_STATUS 			   = "as";
	String VESSEL_AUTH_STATUS_AT_DATE	   = "asad";
	String VESSEL_AUTH_START_FROM 		   = "asf";
	String VESSEL_AUTH_START_TO 		   = "ast";
	String VESSEL_AUTH_END_FROM		   	   = "aef";
	String VESSEL_AUTH_END_TO 			   = "aet";
	String VESSEL_AUTH_TERM_FROM		   = "atf";
	String VESSEL_AUTH_TERM_TO 			   = "att";	
}
