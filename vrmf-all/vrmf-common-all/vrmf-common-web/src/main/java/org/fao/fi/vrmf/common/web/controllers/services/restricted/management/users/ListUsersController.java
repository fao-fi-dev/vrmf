/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.restricted.management.users;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.utility.model.extensions.collections.SerializableList;
import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableArrayList;
import org.fao.fi.vrmf.business.dao.UserManagementDAO;
import org.fao.fi.vrmf.common.behavior.security.CountryManagerUser;
import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.web.controllers.AbstractExplicitController;
import org.fao.fi.vrmf.common.web.controllers.services.restricted.CommonRestrictedServicesConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jan 2012
 */
@Singleton @Controller
@RequestMapping(CommonRestrictedServicesConstants.MANAGEMENT_USERS_SERVICES_PREFIX + "*")
public class ListUsersController extends AbstractExplicitController {
	@Inject
	private UserManagementDAO _userManagementDAO;
	
	/**
	 * Class constructor
	 *
	 * @throws Throwable
	 */
	public ListUsersController() throws Throwable {
		super();
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.controllers.AbstractController#initializeCustomMaps()
	 */
	@Override
	protected void initializeCustomMaps() throws Throwable {
	}

	@RequestMapping(value="list/real/{application}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<byte[]> listRealUsersForApplication(HttpServletRequest request, @PathVariable String application) throws Throwable {
		return this.doListUsersForApplication(request, application, true, false);
	}
	
	@RequestMapping(value="list/real/countryManagers/{application}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<byte[]> listRealCountryManagerUsersForApplication(HttpServletRequest request, @PathVariable String application) throws Throwable {
		return this.doListUsersForApplication(request, application, true, true);
	}
	
	@RequestMapping(value="list/countryManagers/{application}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<byte[]> listCountryManagerUsersForApplication(HttpServletRequest request, @PathVariable String application) throws Throwable {
		return this.doListUsersForApplication(request, application, false, true);
	}
	
	public @ResponseBody ResponseEntity<byte[]> doListUsersForApplication(HttpServletRequest request, @PathVariable String application, boolean realUsersOnly, boolean countryManagersOnly) throws Throwable {
		try {
			this.requireLoggedUser(request);
			
			if(application == null)
				return this.badRequest();
				
			SerializableList<CountryManagerUser> results = new SerializableArrayList<CountryManagerUser>();
			
			long end, start = System.currentTimeMillis();
			
			for(CountryManagerUser retrieved : this._userManagementDAO.listApplicativeUserDetails(application, realUsersOnly, countryManagersOnly)) {
				//retrieved.getRoles().clear();
				//retrieved.getCapabilities().clear();
				
				results.add(retrieved);
			}
			
			end = System.currentTimeMillis();
			
			JSONResponse<SerializableList<CountryManagerUser>> response = new JSONResponse<SerializableList<CountryManagerUser>>(results);
			response.setElapsed(end - start);
			
			byte[] content = response.JSONify().getBytes("UTF-8");
			
			HttpHeaders headers = this.getContentTypeDispositionAndLengthHeaders(null, "json", content == null ? 0 : content.length);
						
			return new ResponseEntity<byte[]>(content, this.mergeHeaders(headers, this.getCachingHeaders(3600)), HttpStatus.OK);
		} catch (Throwable t) {
			return this.manageError(t, "Unable to retrieve details for users of '" + application + "'");
		}
	}
}
