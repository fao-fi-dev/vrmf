/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.search;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Nov 2012
 */
public interface CommonSearchControllersConstants {
	String VRMF_SEARCH_REQUEST_TOKEN_HEADER = "vrmf.search.request.token";
}