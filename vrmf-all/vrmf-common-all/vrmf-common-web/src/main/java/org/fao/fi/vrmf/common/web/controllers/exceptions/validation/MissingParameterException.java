/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.exceptions.validation;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2012
 */
public class MissingParameterException extends ValidationException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 261525243372150541L;

	static final private String DEFAULT_MESSAGE = "One or more required parameters is missing";

	/**
	 * Class constructor
	 *
	 */
	public MissingParameterException() {
		this(DEFAULT_MESSAGE);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public MissingParameterException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public MissingParameterException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public MissingParameterException(Throwable cause) {
		super(DEFAULT_MESSAGE, cause);
	}	
}