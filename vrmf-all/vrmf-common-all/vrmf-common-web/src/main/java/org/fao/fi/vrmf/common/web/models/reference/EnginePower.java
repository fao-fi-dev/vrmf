/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.models.reference;

import org.fao.fi.vrmf.common.models.generated.SPowerTypes;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Jan 2012
 */
public class EnginePower extends SPowerTypes {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2644209737792686954L;

	public EnginePower(String id, String description, String sourceSystem) {
		super();
		
		this.setId(id);
		this.setDescription(description);
		this.setSourceSystem(sourceSystem);
	}
	
	public EnginePower(SPowerTypes dto) {
		this(dto.getId(), dto.getDescription(), dto.getSourceSystem());
	}
}
