/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 May 2012
 */
public interface CommonServicesConstants {
	String MAPPING_PREFIX = "/";
}
