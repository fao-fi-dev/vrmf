/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.models.reference;

import org.fao.fi.vrmf.common.core.model.GenericData;
import org.fao.fi.vrmf.common.models.generated.SCountries;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Jan 2012
 */
public class Country extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3476886628202019983L;
	
	private Integer _id;
	private Integer _uid;
	private String _iso2Code;
	private String _iso3Code;
	private Integer _faostatCode;
	private Integer _unCode;
	private String _name;
	private String _officialName;
	private String _sourceSystem;
	
	/**
	 * Class constructor
	 *
	 * @param id
	 * @param uid
	 * @param iso2Code
	 * @param iso3Code
	 * @param faostatCode
	 * @param unCode
	 * @param name
	 * @param officialName
	 * @param sourceSystem
	 */
	public Country(Integer id, Integer uid, String iso2Code, String iso3Code, Integer faostatCode, Integer unCode, String name, String officialName, String sourceSystem) {
		super();
		this._id = id;
		this._uid = uid;
		this._iso2Code = iso2Code;
		this._iso3Code = iso3Code;
		this._faostatCode = faostatCode;
		this._unCode = unCode;
		this._name = name;
		this._officialName = officialName;
		this._sourceSystem = sourceSystem;
	}
	
	public Country(SCountries dto) {
		this(dto.getId(), dto.getUid(), dto.getIso2Code(), dto.getIso3Code(), dto.getFaostatCode(), dto.getUnCode(), dto.getName(), dto.getOfficialName(), dto.getSourceSystem());
	}

	/**
	 * @return the 'id' value
	 */
	public Integer getId() {
		return this._id;
	}
	
	/**
	 * @param id the 'id' value to set
	 */
	public void setId(Integer id) {
		this._id = id;
	}
	
	/**
	 * @return the 'uid' value
	 */
	public Integer getUid() {
		return this._uid;
	}
	
	/**
	 * @param uid the 'uid' value to set
	 */
	public void setUid(Integer uid) {
		this._uid = uid;
	}
	
	/**
	 * @return the 'iso2Code' value
	 */
	public String getIso2Code() {
		return this._iso2Code;
	}
	
	/**
	 * @param iso2Code the 'iso2Code' value to set
	 */
	public void setIso2Code(String iso2Code) {
		this._iso2Code = iso2Code;
	}
	
	/**
	 * @return the 'iso3Code' value
	 */
	public String getIso3Code() {
		return this._iso3Code;
	}
	
	/**
	 * @param iso3Code the 'iso3Code' value to set
	 */
	public void setIso3Code(String iso3Code) {
		this._iso3Code = iso3Code;
	}
	
	/**
	 * @return the 'faostatCode' value
	 */
	public Integer getFaostatCode() {
		return this._faostatCode;
	}
	
	/**
	 * @param faostatCode the 'faostatCode' value to set
	 */
	public void setFaostatCode(Integer faostatCode) {
		this._faostatCode = faostatCode;
	}
	
	/**
	 * @return the 'unCode' value
	 */
	public Integer getUnCode() {
		return this._unCode;
	}
	
	/**
	 * @param unCode the 'unCode' value to set
	 */
	public void setUnCode(Integer unCode) {
		this._unCode = unCode;
	}
	
	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return this._name;
	}
	
	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		this._name = name;
	}
	
	/**
	 * @return the 'officialName' value
	 */
	public String getOfficialName() {
		return this._officialName;
	}
	
	/**
	 * @param officialName the 'officialName' value to set
	 */
	public void setOfficialName(String officialName) {
		this._officialName = officialName;
	}
	
	/**
	 * @return the 'sourceSystem' value
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}
	
	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}
}
