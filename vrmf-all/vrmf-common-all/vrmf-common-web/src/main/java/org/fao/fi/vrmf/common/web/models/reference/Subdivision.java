/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.models.reference;

import org.fao.fi.vrmf.common.core.model.GenericData;
import org.fao.fi.vrmf.common.models.generated.SCountriesSubdivision;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 May 2012
 */
public class Subdivision extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6122104343272973510L;
	
	private String _countryIso2Code;
	private String _id;
	private String _name;
	private String _alternateName;
	private Integer _typeId;
	private String _type;
	private String _sourceSystem;
	
	/**
	 * Class constructor
	 *
	 * @param countryIso2Code
	 * @param id
	 * @param name
	 * @param alternateName
	 * @param typeId
	 * @param sourceSystem
	 */
	public Subdivision(String countryIso2Code, String id, String name, String alternateName, Integer typeId, String type, String sourceSystem) {
		super();
		this._countryIso2Code = countryIso2Code;
		this._id = id;
		this._name = name;
		this._alternateName = alternateName;
		this._typeId = typeId;
		this._type = type;
		this._sourceSystem = sourceSystem;
	}
	
	public Subdivision(SCountriesSubdivision dto, String countryIso2Code, String type) {
		this(countryIso2Code, dto.getId(), dto.getName(), dto.getAlternateName(), dto.getTypeId(), type, dto.getSourceSystem());
	}

	/**
	 * @return the 'countryIso2Code' value
	 */
	public String getCountryIso2Code() {
		return this._countryIso2Code;
	}
	
	/**
	 * @param countryIso2Code the 'countryIso2Code' value to set
	 */
	public void setCountryIso2Code(String countryIso2Code) {
		this._countryIso2Code = countryIso2Code;
	}
	
	/**
	 * @return the 'id' value
	 */
	public String getId() {
		return this._id;
	}
	
	/**
	 * @param id the 'id' value to set
	 */
	public void setId(String id) {
		this._id = id;
	}
	
	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return this._name;
	}
	
	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		this._name = name;
	}
	
	/**
	 * @return the 'alternateName' value
	 */
	public String getAlternateName() {
		return this._alternateName;
	}
	
	/**
	 * @param alternateName the 'alternateName' value to set
	 */
	public void setAlternateName(String alternateName) {
		this._alternateName = alternateName;
	}
	
	/**
	 * @return the 'typeId' value
	 */
	public Integer getTypeId() {
		return this._typeId;
	}
	
	/**
	 * @param typeId the 'typeId' value to set
	 */
	public void setTypeId(Integer typeId) {
		this._typeId = typeId;
	}
		
	/**
	 * @return the 'type' value
	 */
	public String getType() {
		return this._type;
	}

	/**
	 * @param type the 'type' value to set
	 */
	public void setType(String type) {
		this._type = type;
	}

	/**
	 * @return the 'sourceSystem' value
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}
	
	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}
	
	
}
