/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.models.reference;

import org.fao.fi.vrmf.common.models.generated.SAuthorizationTerminationReasons;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Jan 2012
 */
public class AuthorizationTerminationReason extends SAuthorizationTerminationReasons {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2944471874921073155L;
	
	public AuthorizationTerminationReason(SAuthorizationTerminationReasons source) {
		this.setId(source.getId());
		this.setSourceSystem(source.getSourceSystem());
		this.setDescription(source.getDescription());
	}
}