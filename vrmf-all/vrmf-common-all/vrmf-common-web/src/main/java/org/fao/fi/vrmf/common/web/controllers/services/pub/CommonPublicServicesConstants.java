/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub;

import org.fao.fi.vrmf.common.web.controllers.services.CommonServicesConstants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 May 2012
 */
public interface CommonPublicServicesConstants {
	String MAPPING_PREFIX = CommonServicesConstants.MAPPING_PREFIX + "public/";
	
	String SYSTEM_SERVICES_PREFIX = MAPPING_PREFIX + "system/";
	
	String USER_SERVICES_PREFIX = MAPPING_PREFIX + "user/";
	
	String META_SERVICES_PREFIX = MAPPING_PREFIX + "meta/";
	
	String METADATA_SERVICES_PREFIX = MAPPING_PREFIX + "metadata/";
	
	String REFERENCE_SERVICES_PREFIX 	   = MAPPING_PREFIX + "reference/";
	String REFERENCE_CODES_SERVICES_PREFIX = REFERENCE_SERVICES_PREFIX + "codes/";
	
	String INDEXING_SERVICES_PREFIX = MAPPING_PREFIX + "indexing/";
	
	String VESSELS_SERVICES_PREFIX 	   		= MAPPING_PREFIX + "vessels/";
	String VESSELS_SEARCH_PREFIX 	   		= VESSELS_SERVICES_PREFIX + "search/";	
	String VESSELS_AUTOCOMPLETE_PREFIX 	   	= VESSELS_SERVICES_PREFIX + "autocomplete/";
	String VESSELS_RETRIEVE_SERVICES_PREFIX = VESSELS_SERVICES_PREFIX + "retrieve/";
}
