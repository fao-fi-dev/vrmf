/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.search.infringements;

import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.Collection;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.sh.utility.model.extensions.collections.SerializableCollection;
import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableArrayList;
import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper.RequestInfo;
import org.fao.fi.vrmf.common.models.extended.FullVessel;
import org.fao.fi.vrmf.common.services.vessels.metadata.infringements.VesselInfringementsManager;
import org.fao.fi.vrmf.common.services.vessels.metadata.infringements.support.VesselInfringementsIdentificationRequest;
import org.fao.fi.vrmf.common.services.vessels.metadata.infringements.support.VesselInfringementsIdentificationResponse;
import org.fao.fi.vrmf.common.services.vessels.metadata.infringements.support.VesselInfringementsIdentificationResponseData;
import org.fao.fi.vrmf.common.web.controllers.services.pub.CommonPublicServicesConstants;
import org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.search.ExternalSearchControllerSkeleton;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 May 2012
 */
@Singleton @Controller
@RequestMapping(CommonPublicServicesConstants.VESSELS_SEARCH_PREFIX + "infringements/*")
public class SearchVesselInfringementsController extends ExternalSearchControllerSkeleton<VesselInfringementsManager> {
	static final private String XML_NAMESPACE = "http://www.fao.org/figis/vrmf/schemas/domain/services/search/infringements";
	

	/**
	 * Class constructor
	 */
	public SearchVesselInfringementsController() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.web.controllers.services.pub.vessels.search.SearchControllerSkeleton#getXMLNamespace()
	 */
	@Override
	protected String getXMLNamespace() {
		return XML_NAMESPACE;
	}

	@RequestMapping(value="{by:ID|UID}/{id:[0-9]{9}}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchInfringements(HttpServletRequest request, @PathVariable String by, @PathVariable Integer id, @PathVariable String format) {
		try {
			FullVessel vesselData = this.getVesselDetails(dataCustomizer.getTargetTable(request), by, id, this.getAvailableSources(request));
			
			if(vesselData == null)
				return this.notFound();
			
			String[] IMOs = this.extractIMOs(vesselData);
			String[] names = this.extractNames(vesselData);
			String[] IRCSs = this.extractIRCSs(vesselData);
	
			boolean valid = ( IMOs != null && IMOs.length > 0 ) ||
							( names != null && names.length > 0 ) ||
							( IRCSs != null && IRCSs.length > 0 );
			
			if(!valid)
				return this.badRequest("Cannot perform vessel infringement search: no identifying data available for vessel with " + by + " " + id);
			
			long end, start = System.currentTimeMillis();
	
			RequestInfo requestInfo = ServletsHelper.getRequestInfo(request);
			
			SerializableCollection<VesselInfringementsIdentificationResponseData> results = new SerializableArrayList<VesselInfringementsIdentificationResponseData>(); 
			
			if(Boolean.TRUE.equals(requestInfo.getIsBot())) {
				this._log.debug("Skipping data retrievement as the request comes from a search bot");
			} else {
				if(names != null)
					for(int n=0; n<names.length; n++)
						names[n] = URLDecoder.decode(names[n], "UTF-8");
	
				VesselInfringementsIdentificationRequest criteria = new VesselInfringementsIdentificationRequest(
					IMOs,
					null, //EU CFRs,
					names,
					IRCSs,
					null, //MMSIs,
					null //unique IDs
				);
				
				Collection<VesselInfringementsIdentificationResponse> serviceResults = this.manager.invokeAsynchronously(criteria, 30 * 1000);
				
				for(VesselInfringementsIdentificationResponse singleResult : serviceResults)
					if(singleResult != null && singleResult.getResults() != null)
						results.addAll(singleResult.getResults());
			}
			
			end = System.currentTimeMillis();
			
			String content = null;
			byte[] data = new byte[0];
			
			if("json".equals(format)) {
				content = new JSONResponse<SerializableCollection<VesselInfringementsIdentificationResponseData>>(results, end - start).JSONify();
			} else if("xml".equals(format)) {
				content = JAXBHelper.toXML(results);
			} 
				
			data = content == null ? new byte[0] : content.getBytes(Charset.forName("UTF-8"));
			
			HttpHeaders contentHeaders = this.getContentTypeDispositionAndLengthHeaders("Vessels_pictures", format, data.length);
			HttpHeaders cacheHeaders = this.getCachingHeaders(60);
			
			return new ResponseEntity<byte[]>(data, this.mergeHeaders(contentHeaders, cacheHeaders), HttpStatus.OK);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
}