/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.constants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Nov 2010
 */
public interface CommonResourcesConstants {
	String VRMF_WEB_EMBEDDED_RESOURCES_PATH				  = "org/fao/vrmf/web/resources";
	String VRMF_WEB_EMBEDDED_IBATIS_RESOURCES_PATH		  = VRMF_WEB_EMBEDDED_RESOURCES_PATH + "/configuration/ibatis";
	
	String VRMF_WEB_IBATIS_SQL_MAP_CONFIGURATION_RESOURCE = CommonResourcesConstants.VRMF_WEB_EMBEDDED_IBATIS_RESOURCES_PATH + "/sqlmap-config.xml";
}
