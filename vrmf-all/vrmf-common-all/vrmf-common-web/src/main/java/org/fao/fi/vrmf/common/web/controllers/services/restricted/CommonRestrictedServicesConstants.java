/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.restricted;

import org.fao.fi.vrmf.common.web.controllers.services.CommonServicesConstants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 May 2012
 */
public interface CommonRestrictedServicesConstants {
	String MAPPING_PREFIX = CommonServicesConstants.MAPPING_PREFIX + "restricted/";
	
	String MANAGEMENT_SERVICES_PREFIX = MAPPING_PREFIX + "management/";
	
	String MANAGEMENT_USERS_SERVICES_PREFIX = MANAGEMENT_SERVICES_PREFIX + "users/";
		
	String MATCHING_SERVICES_PREFIX	= MAPPING_PREFIX + "matching/";
}
