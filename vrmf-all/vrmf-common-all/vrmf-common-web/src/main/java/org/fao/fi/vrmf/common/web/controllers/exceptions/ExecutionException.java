/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2012
 */
public class ExecutionException extends ControllerException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5678808684843009578L;

	/**
	 * Class constructor
	 *
	 */
	public ExecutionException() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public ExecutionException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public ExecutionException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public ExecutionException(Throwable cause) {
		super(cause);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.controllers.exceptions.ControllerException#getCorrespondingHttpStatusCode()
	 */
	@Override
	public HttpStatus getCorrespondingHttpStatusCode() {
		return HttpStatus.INTERNAL_SERVER_ERROR;
	}	
}