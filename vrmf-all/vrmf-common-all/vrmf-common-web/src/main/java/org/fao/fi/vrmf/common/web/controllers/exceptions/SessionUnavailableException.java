/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.exceptions;

import org.fao.fi.vrmf.common.web.controllers.exceptions.PreconditionException;
import org.springframework.http.HttpStatus;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2012
 */
public class SessionUnavailableException extends PreconditionException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1605776674513439014L;

	static final private String DEFAULT_MESSAGE = "No HTTP session available";

	/**
	 * Class constructor
	 *
	 */
	public SessionUnavailableException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public SessionUnavailableException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public SessionUnavailableException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public SessionUnavailableException(Throwable cause) {
		super(DEFAULT_MESSAGE, cause);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.controllers.exceptions.ControllerException#getCorrespondingHttpStatusCode()
	 */
	@Override
	public HttpStatus getCorrespondingHttpStatusCode() {
		return HttpStatus.BAD_REQUEST;
	}
}