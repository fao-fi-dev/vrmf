/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.meta;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.fao.fi.sh.model.core.spi.Pair;
import org.fao.fi.sh.utility.model.BasicPair;
import org.fao.fi.vrmf.common.j2ee.utilities.process.trackers.HttpSessionTracker;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.common.web.controllers.services.pub.CommonPublicServicesConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 May 2012
 */
@Singleton @Controller
@RequestMapping(CommonPublicServicesConstants.META_SERVICES_PREFIX + "*")
public class ProcessStatusTrackerController extends AbstractController {
	@RequestMapping(value="track/process/{process:[a-zA-Z|\\.|\\_]+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> trackProcess(HttpServletRequest request, @PathVariable String process) {
		try {
			HttpSession session = request.getSession(false);
			
			String action = process == null ? "No process selected" : "Idle"; 
			Double progress = 0D;
				
			Pair<String, Double> status = new BasicPair<String, Double>(action, progress);
			
			if(session != null) {
				@SuppressWarnings("unchecked")
				Map<String, Pair<String, Double>> serviceStatusMap = (Map<String, Pair<String, Double>>)session.getAttribute(HttpSessionTracker.CURRENT_ACTION_MAP_SESSION_ATTRIBUTE);
				
				if(serviceStatusMap == null) {
					serviceStatusMap = Collections.synchronizedMap(new HashMap<String, Pair<String, Double>>());
					session.setAttribute(HttpSessionTracker.CURRENT_ACTION_MAP_SESSION_ATTRIBUTE, serviceStatusMap);
				}
				
				if(serviceStatusMap.containsKey(process))
					status = serviceStatusMap.get(process);
			}
						
			StringBuilder result = new StringBuilder();
			
			result.append("{").
				   append("\"action\": \"").append((status.getLeftPart() == null ? "" : status.getLeftPart()).replaceAll("\n", "\\\\n").replaceAll("\"", "\\\\\"")).
				   append("\",").
				   append("\"progress\": ").append(status.getRightPart()).append("}");
			
			byte[] data = result.toString().getBytes("UTF-8");
						
			HttpHeaders contentHeaders = this.getContentTypeDispositionAndLengthHeaders("processStatus", "json", data.length);
			
			return new ResponseEntity<byte[]>(data, this.mergeHeaders(contentHeaders), HttpStatus.OK);
		} catch (Throwable t) {
			this._log.error("Unable to track current service status", t);
			
			return this.manageError(t);
		} 		
	}
}