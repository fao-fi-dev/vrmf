/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.users;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.fao.fi.sh.model.core.auth.UserModel;
import org.fao.fi.vrmf.business.dao.generated.UsersDAO;
import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.generated.Users;
import org.fao.fi.vrmf.common.models.security.BasicUser;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.common.web.controllers.services.pub.CommonPublicServicesConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Feb 2012
 */
@Controller @Singleton
@RequestMapping(CommonPublicServicesConstants.USER_SERVICES_PREFIX + "*")
public class UserDetailsController extends AbstractController {
	@Inject private @Getter @Setter UsersDAO usersDAO;
	
	private Map<String, byte[]> _cache;
	
	/**
	 * Class constructor
	 *
	 * @throws Throwable
	 */
	public UserDetailsController() throws Throwable {
		super();
		
		this._cache = new HashMap<String, byte[]>();
	}

	@RequestMapping(value="details", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<byte[]> retrieveUserDetails(HttpSession session, HttpServletRequest servletRequest) throws Throwable {
		return this.doRetrieveUserDetails(session, servletRequest, null);
	}
	
	@RequestMapping(value="details/{system}", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<byte[]> retrieveUserDetails(HttpSession session, HttpServletRequest servletRequest, @PathVariable String system) throws Throwable {
		return this.doRetrieveUserDetails(session, servletRequest, system);
	}

	private @ResponseBody ResponseEntity<byte[]> doRetrieveUserDetails(HttpSession session, HttpServletRequest servletRequest, String system) throws Throwable {
		try {
			this._log.info("Requesting currently logged user details" + ( system == null ? "" : " for system '" + system + "'" ) + "...");
			
			long end, start = System.currentTimeMillis();
			
			BasicUser loggedUser = ServletsHelper.getLoggedUser(session);
			
			if(loggedUser == null) {
				String message = "No user is currently logged in"; 
				
				this._log.info(message);
				
				return new ResponseEntity<byte[]>(HttpStatus.NO_CONTENT);
			}

			String key = loggedUser.getId() + ( system == null ? "" : "_" + system);
			
			byte[] content = this._cache.get(key);
			
			this._log.info("Returning logged user details for user " + loggedUser.getId());

			if(content == null) {
				Users systemUser = this.usersDAO.selectByPrimaryKey(loggedUser.getId());
				
				//This should never happen...
				if(systemUser == null) {
					return new ResponseEntity<byte[]>(("Unable to retrieve user details for user '" + loggedUser.getId() + "'").getBytes("UTF-8"), HttpStatus.NOT_FOUND);
				}
		
				end = System.currentTimeMillis();
				
				JSONResponse<UserModel> response = new JSONResponse<UserModel>(loggedUser);
				response.setElapsed(end - start);
				
				String[] exclusionPatterns = new String[] { "*" };
				String[] inclusionPatterns = new String[] { "data.id", 
															"data.managedCountries.*", 
															"data.managedSources.*",
															"data.capabilities.capabilityName",
															"data.capabilities.capabilityParameters",
															"data.roles.role",
															"data.roles.capabilities.capabilityName",
															"data.roles.capabilities.capabilityParameters",
															"data.userEntity.name",
															"data.granterEntity.name"};
				
				String serialized = response.JSONify(inclusionPatterns, exclusionPatterns);
				content = serialized.getBytes("UTF-8");
				
				this._cache.put(key, content);
			}
			
			HttpHeaders headers = this.getContentTypeDispositionAndLengthHeaders(null, "json", content.length);
			
			headers = this.mergeHeaders(headers, this.getCachingHeaders(60));
			
			return new ResponseEntity<byte[]>(content, headers, HttpStatus.OK);
//			return this.gzipIfPossible(servletRequest, new ResponseEntity<byte[]>(asBytes, headers, HttpStatus.OK));
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
}