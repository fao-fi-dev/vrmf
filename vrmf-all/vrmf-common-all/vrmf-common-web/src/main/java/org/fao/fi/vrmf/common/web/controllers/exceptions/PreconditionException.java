/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2012
 */
abstract public class PreconditionException extends ControllerException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1597331824544969038L;

	/**
	 * Class constructor
	 *
	 */
	public PreconditionException() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public PreconditionException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public PreconditionException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public PreconditionException(Throwable cause) {
		super(cause);
	}	
}