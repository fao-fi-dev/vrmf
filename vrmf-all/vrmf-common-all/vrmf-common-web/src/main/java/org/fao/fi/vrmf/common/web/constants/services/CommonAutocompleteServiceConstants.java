/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.constants.services;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 Jan 2011
 */
public interface CommonAutocompleteServiceConstants {
	String GROUP_MAPPED_ITEMS_PARAMETER   = "g";
	String LIMIT_TO_MAX_ITEMS_PARAMETER   = "l";
	String AUTHORIZED_ONLY_PARAMETER	  = "a";
	String AUTHORIZATION_SOURCE_PARAMETER = "as";
}