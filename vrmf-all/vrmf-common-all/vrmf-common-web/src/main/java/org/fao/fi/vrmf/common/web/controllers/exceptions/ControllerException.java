/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2012
 */
abstract public class ControllerException extends Exception {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7530025143761497398L;

	/**
	 * Class constructor
	 *
	 */
	public ControllerException() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public ControllerException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public ControllerException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public ControllerException(Throwable cause) {
		super(cause);
	}
	
	abstract public HttpStatus getCorrespondingHttpStatusCode();
}