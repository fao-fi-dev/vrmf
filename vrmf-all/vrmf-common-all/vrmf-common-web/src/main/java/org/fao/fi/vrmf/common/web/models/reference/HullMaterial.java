/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.models.reference;

import org.fao.fi.vrmf.common.models.generated.SHullMaterial;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 May 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 May 2014
 */
public class HullMaterial extends SHullMaterial {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4716966752555351736L;

	public HullMaterial(String id, String description, String sourceSystem) {
		super();
		
		this.setId(id);
		this.setDescription(description);
		this.setSourceSystem(sourceSystem);
	}
	
	public HullMaterial(SHullMaterial dto) {
		this(dto.getId(), dto.getDescription(), dto.getSourceSystem());
	}
}
