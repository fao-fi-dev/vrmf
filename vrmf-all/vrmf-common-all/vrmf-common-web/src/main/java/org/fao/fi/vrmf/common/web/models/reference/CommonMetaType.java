/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.models.reference;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Jan 2012
 */
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class CommonMetaType extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1628720893958605954L;
	
	@XmlAttribute(name="vrmfId")
	private Integer _id;
	
	@XmlAttribute(name="vrmfUid")
	private Integer _uid;
	
	@XmlAttribute(name="originalId")
	private String _originalId;
	
	@XmlElement(name="metaCode")
	private Integer _metaCode;
	
	@XmlElement(name="standardAbbreviation")
	private String _standardAbbreviation;
	
	@XmlElement(name="name")
	private String _name;
	
	@XmlElement(name="description")
	private String _description;
	
	@XmlAttribute(name="sourceSystem")
	private String _sourceSystem;
	
	/**
	 * Class constructor
	 *
	 * @param id
	 * @param uid
	 * @param originalId
	 * @param metaCode
	 * @param standardAbbreviation
	 * @param name
	 * @param description
	 * @param sourceSystem
	 */
	public CommonMetaType(Integer id, Integer uid, String originalId, Integer metaCode, String standardAbbreviation, String name, String description, String sourceSystem) {
		super();
		this._id = id;
		this._uid = uid;
		this._originalId = originalId;
		this._metaCode = metaCode;
		this._standardAbbreviation = standardAbbreviation;
		this._name = name;
		this._description = description;
		this._sourceSystem = sourceSystem;
	}

	/**
	 * @return the 'id' value
	 */
	public Integer getId() {
		return this._id;
	}

	/**
	 * @param id the 'id' value to set
	 */
	public void setId(Integer id) {
		this._id = id;
	}

	/**
	 * @return the 'uid' value
	 */
	public Integer getUid() {
		return this._uid;
	}

	/**
	 * @param uid the 'uid' value to set
	 */
	public void setUid(Integer uid) {
		this._uid = uid;
	}

	/**
	 * @return the 'originalId' value
	 */
	public String getOriginalId() {
		return this._originalId;
	}

	/**
	 * @param originalId the 'originalId' value to set
	 */
	public void setOriginalId(String originalId) {
		this._originalId = originalId;
	}

	/**
	 * @return the 'metaCode' value
	 */
	public Integer getMetaCode() {
		return this._metaCode;
	}

	/**
	 * @param metaCode the 'metaCode' value to set
	 */
	public void setMetaCode(Integer metaCode) {
		this._metaCode = metaCode;
	}

	/**
	 * @return the 'standardAbbreviation' value
	 */
	public String getStandardAbbreviation() {
		return this._standardAbbreviation;
	}

	/**
	 * @param standardAbbreviation the 'standardAbbreviation' value to set
	 */
	public void setStandardAbbreviation(String standardAbbreviation) {
		this._standardAbbreviation = standardAbbreviation;
	}

	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return this._name;
	}

	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		this._name = name;
	}

	/**
	 * @return the 'description' value
	 */
	public String getDescription() {
		return this._description;
	}

	/**
	 * @param description the 'description' value to set
	 */
	public void setDescription(String description) {
		this._description = description;
	}

	/**
	 * @return the 'sourceSystem' value
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}

	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._description == null) ? 0 : this._description.hashCode());
		result = prime * result + ((this._id == null) ? 0 : this._id.hashCode());
		result = prime * result + ((this._metaCode == null) ? 0 : this._metaCode.hashCode());
		result = prime * result + ((this._name == null) ? 0 : this._name.hashCode());
		result = prime * result + ((this._originalId == null) ? 0 : this._originalId.hashCode());
		result = prime * result + ((this._sourceSystem == null) ? 0 : this._sourceSystem.hashCode());
		result = prime * result + ((this._standardAbbreviation == null) ? 0 : this._standardAbbreviation.hashCode());
		result = prime * result + ((this._uid == null) ? 0 : this._uid.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CommonMetaType))
			return false;
		CommonMetaType other = (CommonMetaType) obj;
		if (this._description == null) {
			if (other._description != null)
				return false;
		} else if (!this._description.equals(other._description))
			return false;
		if (this._id == null) {
			if (other._id != null)
				return false;
		} else if (!this._id.equals(other._id))
			return false;
		if (this._metaCode == null) {
			if (other._metaCode != null)
				return false;
		} else if (!this._metaCode.equals(other._metaCode))
			return false;
		if (this._name == null) {
			if (other._name != null)
				return false;
		} else if (!this._name.equals(other._name))
			return false;
		if (this._originalId == null) {
			if (other._originalId != null)
				return false;
		} else if (!this._originalId.equals(other._originalId))
			return false;
		if (this._sourceSystem == null) {
			if (other._sourceSystem != null)
				return false;
		} else if (!this._sourceSystem.equals(other._sourceSystem))
			return false;
		if (this._standardAbbreviation == null) {
			if (other._standardAbbreviation != null)
				return false;
		} else if (!this._standardAbbreviation.equals(other._standardAbbreviation))
			return false;
		if (this._uid == null) {
			if (other._uid != null)
				return false;
		} else if (!this._uid.equals(other._uid))
			return false;
		return true;
	}
}