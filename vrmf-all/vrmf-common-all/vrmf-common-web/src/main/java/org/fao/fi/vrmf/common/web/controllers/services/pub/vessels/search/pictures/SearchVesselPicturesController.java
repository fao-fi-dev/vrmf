/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.search.pictures;

import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.SerializableCollection;
import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableArrayList;
import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper.RequestInfo;
import org.fao.fi.vrmf.common.models.extended.FullVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesManager;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationRequest;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponse;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.fao.fi.vrmf.common.web.controllers.services.pub.CommonPublicServicesConstants;
import org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.search.ExternalSearchControllerSkeleton;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 May 2012
 */
@Singleton @Controller
@RequestMapping(CommonPublicServicesConstants.VESSELS_SEARCH_PREFIX + "pictures/*")
public class SearchVesselPicturesController extends ExternalSearchControllerSkeleton<VesselPicturesManager> {
	final static private String XML_NAMESPACE = "http://www.fao.org/figis/vrmf/schemas/domain/services/search/pictures";
	
	/**
	 * Class constructor
	 */
	public SearchVesselPicturesController() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.web.controllers.services.pub.vessels.search.SearchControllerSkeleton#getXMLNamespace()
	 */
	@Override
	final protected String getXMLNamespace() {
		return XML_NAMESPACE;
	}
	
	@RequestMapping(value="{by:ID|UID}/{id:[0-9]{9}}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchPictures(HttpServletRequest request, @PathVariable String by, @PathVariable Integer id, @PathVariable String format) {
		try {
			FullVessel vesselData = this.getVesselDetails(dataCustomizer.getTargetTable(request), by, id, this.getAvailableSources(request));
			
			if(vesselData == null)
				return this.notFound();
			
			String[] IMOs = this.extractIMOs(vesselData);
			String[] EUCFRs = this.extractEUCFRs(vesselData);
			String[] names = this.extractNames(vesselData);
			String[] IRCSs = this.extractIRCSs(vesselData);
			String[] MMSIs = this.extractMMSIs(vesselData);
			
			VesselsToIdentifiers CCAMLR_ID = this.extractIdentifier(vesselData, "CCAMLR_ID");
			VesselsToIdentifiers IATTC_ID = this.extractIdentifier(vesselData, "IATTC_ID");
			VesselsToIdentifiers WCPFC_ID = this.extractIdentifier(vesselData, "WCPFC_ID");

			boolean valid = ( IMOs != null && IMOs.length > 0 ) ||
							( EUCFRs != null && EUCFRs.length > 0 ) ||
							( names != null && names.length > 0 ) ||
							( IRCSs != null && IRCSs.length > 0 ) ||
							( MMSIs != null && MMSIs.length > 0 ) ||
							CCAMLR_ID != null ||
							IATTC_ID != null ||
							WCPFC_ID != null;
			
			if(!valid)
				return this.badRequest();
			
			long end, start = System.currentTimeMillis();

			RequestInfo requestInfo = ServletsHelper.getRequestInfo(request);
			
			SerializableCollection<VesselPicturesIdentificationResponseData> results = new SerializableArrayList<VesselPicturesIdentificationResponseData>();; 
			
			if(Boolean.TRUE.equals(requestInfo.getIsBot())) {
				this._log.debug("Skipping data retrievement as the request comes from a search bot");
			} else {
				Set<NameValuePair> uniqueIDs = WCPFC_ID == null && IATTC_ID == null && CCAMLR_ID == null ? null : new HashSet<NameValuePair>(); 

				if(WCPFC_ID != null)
					uniqueIDs.add(new NameValuePair("WCPFC", WCPFC_ID.getIdentifier()));
				
				if(IATTC_ID != null)
					uniqueIDs.add(new NameValuePair("IATTC", IATTC_ID.getIdentifier()));

				if(CCAMLR_ID != null)
					uniqueIDs.add(new NameValuePair("CCAMLR", CCAMLR_ID.getIdentifier()));

				VesselPicturesIdentificationRequest criteria = new VesselPicturesIdentificationRequest(
					IMOs,
					EUCFRs, //EU CFRs,
					names,
					IRCSs,
					MMSIs,
					uniqueIDs == null || uniqueIDs.isEmpty() ? null : uniqueIDs.toArray(new NameValuePair[uniqueIDs.size()])
				);
				
				Collection<VesselPicturesIdentificationResponse> serviceResults = this.manager.invokeAsynchronously(criteria, 30 * 1000);
				
				for(VesselPicturesIdentificationResponse singleResult : serviceResults)
					if(singleResult != null && singleResult.getResults() != null)
						results.addAll(singleResult.getResults());
			}
			
			end = System.currentTimeMillis();
			
			String content = null;
			byte[] data = new byte[0];
			
			if("json".equals(format)) {
				content = new JSONResponse<SerializableCollection<VesselPicturesIdentificationResponseData>>(results, end - start).JSONify();
			} else if("xml".equals(format)) {
				content = JAXBHelper.toXML(results);
			} 
				
			data = content == null ? new byte[0] : content.getBytes(Charset.forName("UTF-8"));
			
			HttpHeaders contentHeaders = this.getContentTypeDispositionAndLengthHeaders("Vessels_pictures", format, data.length);
			HttpHeaders cacheHeaders = this.getCachingHeaders(60);
			
			return new ResponseEntity<byte[]>(data, this.mergeHeaders(contentHeaders, cacheHeaders), HttpStatus.OK);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
}