/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.helpers.beans.text.i18n;

import java.util.Locale;

import org.fao.fi.vrmf.common.legacy.helpers.BundleManager;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 03/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 03/mag/2013
 */
public class CustomBundleManager extends BundleManager {
	public CustomBundleManager(String basePath, Locale locale, String... bundleNames) {
		super(locale, rebase(basePath, bundleNames));
	}

	public CustomBundleManager(Locale locale, String bundleName) {
		super(locale, rebase(bundleName));
	}

	public CustomBundleManager(String basePath, String bundleName) {
		super(rebase(basePath, bundleName));
	}

	static private String rebase(String basePath, String bundleName) {
		if(bundleName == null)
			return bundleName;

		return ( basePath + ( bundleName.startsWith("/") ? bundleName : "/" + bundleName) ).replace("//", "/");
	}

	static private String[] rebase(String basePath, String... bundleNames) {
		if(bundleNames == null || bundleNames.length == 0)
			return bundleNames;

		String[] rebased = new String[bundleNames.length];

		int b=0;
		for(String bundleName : bundleNames) {
			rebased[b++] = rebase(basePath, bundleName);
		}

		return rebased;
	}
}
