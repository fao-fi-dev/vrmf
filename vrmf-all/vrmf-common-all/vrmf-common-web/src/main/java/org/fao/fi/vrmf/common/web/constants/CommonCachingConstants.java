/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.constants;

import org.fao.fi.sh.utility.caching.CachingConstants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 28, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 28, 2015
 */
public interface CommonCachingConstants extends CachingConstants {
	String COUNTRY_ID_TO_ISO2_CACHE_ID		= "CountryIDToIso2" + CACHE_ID_SUFFIX;
	String COUNTRY_ISO2_TO_ID_CACHE_ID		= "CountryIso2ToId" + CACHE_ID_SUFFIX;
	String COUNTRY_ID_TO_COUNTRY_CACHE_ID	= "CountryIdToCountry" + CACHE_ID_SUFFIX;
	String COUNTRY_ISO2_TO_COUNTRY_CACHE_ID = "CountryIso2ToCountry" + CACHE_ID_SUFFIX;
	
	String SUBDIVISION_ID_TO_SUBDIVISION_CACHE_ID = "SubdivisionIdToSubdivision" + CACHE_ID_SUFFIX;
	String SUBDIVISION_TYPE_ID_TO_SUBDIVISION_TYPE_CACHE_ID = "SubdivisionTypeIdToSubdivisionType" + CACHE_ID_SUFFIX;
	
	String ENTITY_ID_TO_ENTITY_CACHE_ID  = "EntityIdToEntity" + CACHE_ID_SUFFIX;
	String ENTITY_KEY_TO_ENTITY_CACHE_ID = "EntityKeyToEntity" + CACHE_ID_SUFFIX;
	String ENTITY_KEY_TO_ENTITY_ID_CACHE_ID = "EntityKeyToEntityId" + CACHE_ID_SUFFIX;
}
