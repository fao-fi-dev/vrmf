/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jws.WebMethod;

import org.codehaus.plexus.util.FileUtils;
import org.fao.fi.sh.model.core.spi.services.ProcessTracker;
import org.fao.fi.sh.utility.caching.spi.CacheFacade;
import org.fao.fi.sh.utility.ehcache.factory.impl.EhCacheFactory;
import org.fao.fi.sh.utility.services.tracking.NullProcessTracker;
import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractIBATISDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesSubdivisionDAO;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SCountriesSubdivision;
import org.fao.fi.vrmf.common.web.constants.CommonCachingConstants;

import com.ibatis.sqlmap.client.SqlMapClient;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Dec 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 Dec 2011
 */
abstract public class AbstractExplicitController extends AbstractController {
	static final protected String[] DEFAULT_JSON_EXCLUSION_PATTERNS = new String[] { "class", "*.class", "*.checksum", "*.updaterId", "*.updateDate", "*.comment" };
	
	static final protected int JSON = 0;
	static final protected int JSONP= 1;
	static final protected int XML  = 2;
	static final protected int HTML = 3;
	static final protected int TXT  = 4;
	static final protected int CSV  = 5;
	static final protected int XLS  = 6;
	static final protected int XLSX = 7;
	static final protected int PDF  = 8;
	static final protected int CLASS= 9;
	
	static final protected Boolean MERGE_HEADERS 	  = Boolean.TRUE;
	static final protected Boolean DONT_MERGE_HEADERS = Boolean.FALSE;
		
	final protected Long CONTENT_MAX_AGE_IN_SECONDS = 60 * 60 * 2L; 
	
	@Inject @Named(AbstractIBATISDAO.VRMF_DEFAULT_SQL_MAP_CLIENT_BEAN_ID)
	@Getter @Setter protected SqlMapClient vrmfClient;

	@Inject @Getter @Setter protected SCountriesDAO countriesDAO;
	
	@Inject	@Getter @Setter protected SCountriesSubdivisionDAO countriesSubdivisionDAO;

	protected CacheFacade<Integer, String> COUNTRY_ID_TO_ISO2_CACHE = new EhCacheFactory<Integer, String>().getCache(CommonCachingConstants.COUNTRY_ID_TO_ISO2_CACHE_ID);
	protected CacheFacade<String, Integer> COUNTRY_ISO2_TO_ID_CACHE = new EhCacheFactory<String, Integer>().getCache(CommonCachingConstants.COUNTRY_ISO2_TO_ID_CACHE_ID);
	
	protected CacheFacade<Integer, SCountries> COUNTRY_ID_TO_COUNTRY_CACHE = new EhCacheFactory<Integer, SCountries>().getCache(CommonCachingConstants.COUNTRY_ID_TO_COUNTRY_CACHE_ID);	
	protected CacheFacade<String, SCountries> COUNTRY_ISO2_TO_COUNTRY_CACHE = new EhCacheFactory<String, SCountries>().getCache(CommonCachingConstants.COUNTRY_ISO2_TO_COUNTRY_CACHE_ID);;
	
	protected CacheFacade<String, SCountriesSubdivision> SUBDIVISION_ID_TO_SUBDIVISION_CACHE = new EhCacheFactory<String, SCountriesSubdivision>().getCache(CommonCachingConstants.SUBDIVISION_ID_TO_SUBDIVISION_CACHE_ID);

	final protected ProcessTracker _processTracker = new NullProcessTracker();
	
	@Inject protected FileUtils _fileHelper;
	
	public AbstractExplicitController() throws Exception {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.beans.lifecycle.Initializable#init()
	 */
	@WebMethod(exclude = true)
	@PostConstruct public void init() throws Throwable {
		this.initializeMaps();
		this.initializeCustomMaps();
	}
	
	final protected void initializeMaps() throws Throwable { }
	
	abstract protected void initializeCustomMaps() throws Throwable;
}