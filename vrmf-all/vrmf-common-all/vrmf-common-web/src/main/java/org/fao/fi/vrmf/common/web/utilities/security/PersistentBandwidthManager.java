/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.utilities.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.business.core.dao.PersistanceMetadataDAO;
import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractIBATISDAO;
import org.fao.fi.vrmf.business.dao.generated.RemoteRequestLimitDAO;
import org.fao.fi.vrmf.business.dao.generated.RemoteRequestMetadataDAO;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.BandwidthLimits;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.BandwidthManager;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ProcessedRequestsFilter;
import org.fao.fi.vrmf.common.j2ee.extras.utilities.security.ua.ProcessedRequestData;
import org.fao.fi.vrmf.common.models.generated.RemoteRequestLimit;
import org.fao.fi.vrmf.common.models.generated.RemoteRequestLimitExample;
import org.fao.fi.vrmf.common.models.generated.RemoteRequestMetadata;
import org.fao.fi.vrmf.common.models.generated.RemoteRequestMetadataExample;

import com.ibatis.sqlmap.client.SqlMapClient;

@Singleton
@Named
public class PersistentBandwidthManager implements BandwidthManager {
	@Inject @Named(AbstractIBATISDAO.VRMF_DEFAULT_SQL_MAP_CLIENT_BEAN_ID) SqlMapClient _sqlMapClient;
	@Inject private PersistanceMetadataDAO<Long> _persistanceMetadataDAO;
	@Inject private RemoteRequestLimitDAO _requestLimitDAO;
	@Inject private RemoteRequestMetadataDAO _requestMetadataDAO;

	/**
	 * @return the 'persistanceMetadataDAO' value
	 */
	public PersistanceMetadataDAO<Long> getPersistanceMetadataDAO() {
		return this._persistanceMetadataDAO;
	}

	/**
	 * @param persistanceMetadataDAO the 'persistanceMetadataDAO' value to set
	 */
	public void setPersistanceMetadataDAO(PersistanceMetadataDAO<Long> persistanceMetadataDAO) {
		this._persistanceMetadataDAO = persistanceMetadataDAO;
	}

	/**
	 * @return the 'requestLimitDAO' value
	 */
	public RemoteRequestLimitDAO getRequestLimitDAO() {
		return this._requestLimitDAO;
	}

	/**
	 * @param requestLimitDAO the 'requestLimitDAO' value to set
	 */
	public void setRequestLimitDAO(RemoteRequestLimitDAO requestLimitDAO) {
		this._requestLimitDAO = requestLimitDAO;
	}

	/**
	 * @return the 'requestMetadataDAO' value
	 */
	public RemoteRequestMetadataDAO getRequestMetadataDAO() {
		return this._requestMetadataDAO;
	}

	/**
	 * @param requestMetadataDAO the 'requestMetadataDAO' value to set
	 */
	public void setRequestMetadataDAO(RemoteRequestMetadataDAO requestMetadataDAO) {
		this._requestMetadataDAO = requestMetadataDAO;
	}
	
	private BandwidthLimits convert(RemoteRequestLimit dto) {
		if(dto == null)
			return null;
		
		BandwidthLimits current = new BandwidthLimits();
		current.setApplicationId(dto.getApplicationId());
		current.setIpFilter(dto.getIpFilter());
		current.setMaxPerDay(dto.getMaxPerDay());
		current.setMaxPerHour(dto.getMaxPerHour());
		current.setMaxPerMinute(dto.getMaxPerMinute());
		current.setMaxPerSecond(dto.getMaxPerSecond());
		
		return current;
	}
	
	private ProcessedRequestData convert(RemoteRequestMetadata dto) {
		if(dto == null)
			return null;
		
		ProcessedRequestData current = new ProcessedRequestData();
		current.setBlocked(dto.getBlocked());
		current.setComment(dto.getComment());
		current.setElapsed(dto.getElapsed());
		current.setId(dto.getId());
		current.setIsBot(dto.getIsBot());
		current.setLoggedUser(dto.getLoggedUser());
		current.setReferrer(dto.getReferrer());
		current.setRequestMethod(dto.getRequestMethod());
		current.setRequestSource(dto.getRequestSource());
		current.setRequestSourceCountry(dto.getRequestSourceCountry());
		current.setRequestTarget(dto.getRequestTarget());
		current.setRequestUrl(dto.getRequestUrl());
		current.setSessionId(dto.getSessionId());
		current.setStatusCode(dto.getStatusCode());
		current.setTargetApp(dto.getTargetApp());
		current.setTimestamp(dto.getTimestamp());
		current.setUserAgent(dto.getUserAgent());
		current.setUserAgentType(dto.getUserAgentType());
		
		return current;
	}
	
	private RemoteRequestMetadata convert(ProcessedRequestData dto) {
		if(dto == null)
			return null;
		
		RemoteRequestMetadata current = new RemoteRequestMetadata();
		current.setBlocked(dto.getBlocked());
		current.setComment(dto.getComment());
		current.setElapsed(dto.getElapsed());
		current.setId(dto.getId());
		current.setIsBot(dto.getIsBot());
		current.setLoggedUser(dto.getLoggedUser());
		current.setReferrer(dto.getReferrer());
		current.setRequestMethod(dto.getRequestMethod());
		current.setRequestSource(dto.getRequestSource());
		current.setRequestSourceCountry(dto.getRequestSourceCountry());
		current.setRequestTarget(dto.getRequestTarget());
		current.setRequestUrl(dto.getRequestUrl());
		current.setSessionId(dto.getSessionId());
		current.setStatusCode(dto.getStatusCode());
		current.setTargetApp(dto.getTargetApp());
		current.setTimestamp(dto.getTimestamp());
		current.setUserAgent(dto.getUserAgent());
		current.setUserAgentType(dto.getUserAgentType());
		
		return current;
	}
	
	private RemoteRequestMetadataExample convert(ProcessedRequestsFilter filter) {
		if(filter == null)
			return null;
		
		RemoteRequestMetadataExample example = new RemoteRequestMetadataExample();
		RemoteRequestMetadataExample.Criteria criteria = example.createCriteria();
		
		if(filter.getLoggedUser() != null)
			criteria.andLoggedUserEqualTo(filter.getLoggedUser());
		
		if(filter.getRequestSource() != null)
			criteria.andRequestSourceEqualTo(filter.getRequestSource());
		
		if(filter.getApplicationID() != null)
			criteria.andTargetAppEqualTo(filter.getApplicationID());

		if(filter.getTimestampFrom() > 0)
			criteria.andTimestampGreaterThanOrEqualTo(new Date(filter.getTimestampFrom()));
		
		if(filter.getTimestampTo() > 0)
			criteria.andTimestampLessThanOrEqualTo(new Date(filter.getTimestampFrom()));

		return example;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.j2ee.extras.utilities.security.BandwidthManager#getCurrentLimits()
	 */
	@Override
	public Collection<BandwidthLimits> getCurrentLimits() throws Exception {
		Collection<BandwidthLimits> limits = new ArrayList<BandwidthLimits>();
		
		for(RemoteRequestLimit limit : this._requestLimitDAO.selectByExample(new RemoteRequestLimitExample())) {
			limits.add(this.convert(limit));
		}
		
		return limits;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.j2ee.extras.utilities.security.BandwidthManager#log(org.fao.vrmf.core.j2ee.extras.utilities.security.ua.ProcessedRequestData)
	 */
	@Override
	public int log(ProcessedRequestData request) throws Exception {
		int newId;
		try {
			this._sqlMapClient.startTransaction();

			RemoteRequestMetadata meta = this.convert(request);
			
			if(request.getId() == null)
				this._requestMetadataDAO.insert(meta);
			else
				this._requestMetadataDAO.updateByPrimaryKey(meta);
			
			newId = this._persistanceMetadataDAO.getLastInsertedID().intValue();
			
			this._sqlMapClient.commitTransaction();
			
			return newId;
		} finally {
			this._sqlMapClient.endTransaction();
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.j2ee.extras.utilities.security.BandwidthManager#getLogById(int)
	 */
	@Override
	public ProcessedRequestData getLogById(int id) throws Exception {
		return this.convert(this._requestMetadataDAO.selectByPrimaryKey(id));
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.j2ee.extras.utilities.security.BandwidthManager#filter(org.fao.vrmf.core.j2ee.extras.utilities.security.ProcessedRequestsFilter)
	 */
	@Override
	public Collection<ProcessedRequestData> filter(ProcessedRequestsFilter filter) throws Exception {
		Collection<ProcessedRequestData> entries = new ArrayList<ProcessedRequestData>();
		
		for(RemoteRequestMetadata metadata : this._requestMetadataDAO.selectByExample(this.convert(filter))) {
			entries.add(this.convert(metadata));
		}
		
		return entries;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.j2ee.extras.utilities.security.BandwidthManager#count(org.fao.vrmf.core.j2ee.extras.utilities.security.ProcessedRequestsFilter)
	 */
	@Override
	public int count(ProcessedRequestsFilter filter) throws Exception {
		return this._requestMetadataDAO.countByExample(this.convert(filter));
	}
}
