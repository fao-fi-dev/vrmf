/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.retrieve;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.TimestampHelper;
import org.fao.fi.vrmf.business.dao.CommonVesselRecordSearchDAO;
import org.fao.fi.vrmf.business.dao.FullVesselsDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesDAO;
import org.fao.fi.vrmf.business.dao.generated.SSystemsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsHistoryDAO;
import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.extended.FullVessel;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.security.BasicUser;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.fao.fi.vrmf.common.search.dsl.support.services.response.vessels.VesselRecordSearchResponse;
import org.fao.fi.vrmf.common.web.DataCustomizer;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.common.web.controllers.exceptions.authentication.UnauthorizedUserException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import flexjson.JSONSerializer;
import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 May 2012
 */
abstract public class AbstractRetrieveVesselDataController<R extends VesselRecord, F extends VesselRecordSearchFilter, S extends CommonVesselRecordSearchDAO<R, F>> extends AbstractController {
	static final protected boolean INCLUDE_OTHER_SOURCES_BY_DEFAULT 	 = true;
	static final protected boolean DONT_INCLUDE_OTHER_SOURCES_BY_DEFAULT = !INCLUDE_OTHER_SOURCES_BY_DEFAULT;

	static final protected String[] NO_REQUIRED_ROLES 		 = null;
	static final protected String[] NO_REQUIRED_CAPABILITIES = null;

	static final protected boolean BY_UID = true;
	static final protected boolean BY_ID  = false;
	
	@Inject protected @Getter @Setter DataCustomizer dataCustomizer;

	@Inject protected @Getter @Setter SSystemsDAO sourcesDAO;
	@Inject protected @Getter @Setter SCountriesDAO countriesDAO;
	
	@Inject protected @Named("dao.vessels") @Getter @Setter VesselsDAO vesselsDAO;
	@Inject protected @Getter @Setter VesselsHistoryDAO vesselsHistoryDAO;

	@Inject protected FullVesselsDAO fullVesselsDAO;

	protected Map<Integer, SCountries> _countriesMap;

	protected FullVessel postProcess(FullVessel vessel, String sources, String identifierType, String identifier) {
		return SerializationHelper.clone(vessel);
	}

	protected ResponseEntity<byte[]> doRetrieveByUID(HttpServletRequest request, boolean includePublicSources, String sources, Integer uid, String format, String callback, String[] JSONExclusionPatterns, String[] requiredRoles, String[] requiredCapabilities) {
		try {
			BasicUser loggedUser = ServletsHelper.getLoggedUser(request);

			if(requiredRoles != null && requiredRoles.length > 0)
				if(loggedUser == null || !loggedUser.is(requiredRoles))
					throw new UnauthorizedUserException("To access this service you need to belong to the following roles: " + CollectionsHelper.serializeArray(requiredRoles));

			if(requiredCapabilities != null && requiredCapabilities.length > 0)
				if(loggedUser == null || !loggedUser.can(requiredCapabilities))
					throw new UnauthorizedUserException();//"To access this service you need to have the following capabilities: " + CollectionsHelper.serializeArray(requiredCapabilities));

			String[] effectiveSources = this.getSources(loggedUser, request, sources, includePublicSources);

			long end, start = System.currentTimeMillis();

			FullVessel vessel = this.doRetrieveVesselByID(dataCustomizer.getTargetTable(request), BY_UID, uid, effectiveSources);

			end = System.currentTimeMillis();

			if(vessel == null)
				return this.notFound("No data can be retrieved for vessel with UID #" + uid + ( sources == null ? "" : " and sources " + sources ));

//			vessel = this.postProcess(vessel);

			String filename = "VesselData_UID_" + uid + "_" + CollectionsHelper.join(effectiveSources, "_") + "_" + TimestampHelper.buildTimestamp() + "." + format;

			String data = null;

			if("xml".equals(format)) {
				data = JAXBHelper.toXML(vessel);
			} else if("json".equals(format))
				data = new JSONResponse<FullVessel>(vessel, end - start).JSONifyExcluding(JSONExclusionPatterns == null ? BASIC_JSON_EXCLUSION_PATTERNS : JSONExclusionPatterns);
			else if("jsonp".equals(format)) {
				data = ( callback == null ? "_vrmfVesselDataCallback" + System.currentTimeMillis() : callback ) + "(" +
					new JSONSerializer().exclude(BASIC_JSON_EXCLUSION_PATTERNS).deepSerialize(vessel) +
				");\n";
			}

			byte[] content = data == null ? new byte[0] : data.getBytes(Charset.forName("UTF-8"));

			HttpHeaders contentTypeHeaders = this.getContentTypeDispositionAndLengthHeaders(filename, format, data == null ? 0 : content.length);
			HttpHeaders cachingHeaders = this.getCachingHeaders(60);

			this._log.debug("Vessel by UID #" + uid + " for sources " + CollectionsHelper.serializeArray(effectiveSources) + " has been retrieved");

			return new ResponseEntity<byte[]>(content, this.mergeHeaders(contentTypeHeaders, cachingHeaders), HttpStatus.OK );
		} catch (Throwable t) {
			this._log.error("Unable to retrieve vessel data for UID #" + uid + ( sources == null ? "" : ", sources " + sources ) + " and format " + format, t);

			return this.manageError(t);
		}
	}

	protected ResponseEntity<byte[]> doRetrieveByID(HttpServletRequest request, boolean includePublicSources, String sources, Integer id, String format, String callback, String[] JSONExclusionPatterns, String[] requiredRoles, String[] requiredCapabilities) {
		try {
			BasicUser loggedUser = ServletsHelper.getLoggedUser(request);

			if(requiredRoles != null && requiredRoles.length > 0)
				if(loggedUser == null || !loggedUser.is(requiredRoles))
					throw new UnauthorizedUserException("To access this service you need to belong to the following roles: " + CollectionsHelper.serializeArray(requiredRoles));

			if(requiredCapabilities != null && requiredCapabilities.length > 0)
				if(loggedUser == null || !loggedUser.can(requiredCapabilities))
					throw new UnauthorizedUserException();//"To access this service you need to have the following capabilities: " + CollectionsHelper.serializeArray(requiredCapabilities));

			String[] effectiveSources = this.getSources(loggedUser, request, sources, includePublicSources);

			long end, start = System.currentTimeMillis();

			FullVessel vessel = this.doRetrieveVesselByID(dataCustomizer.getTargetTable(request), BY_ID, id, effectiveSources);

			end = System.currentTimeMillis();

			if(vessel == null)
				return this.notFound("No data can be retrieved for vessel with ID #" + id + ( sources == null ? "" : " and sources " + sources ));

//			vessel = this.postProcess(vessel);

			String filename = "VesselData_ID_" + id + "_" + CollectionsHelper.join(effectiveSources, "_") + "_" + TimestampHelper.buildTimestamp() + "." + format;

			String data = null;

			if("xml".equals(format)) {
				data = JAXBHelper.toXML(vessel);
			} else if("json".equals(format))
				data = new JSONResponse<FullVessel>(vessel, end - start).JSONifyExcluding(JSONExclusionPatterns == null ? BASIC_JSON_EXCLUSION_PATTERNS : JSONExclusionPatterns);
			else if("jsonp".equals(format)) {
				data = "var " + ( callback == null ? "_vrmfVesselDataCallback" + System.currentTimeMillis() : callback ) + "(" +
					new JSONSerializer().exclude(BASIC_JSON_EXCLUSION_PATTERNS).deepSerialize(vessel) +
				");\n";
			}

			byte[] content = data == null ? new byte[0] : data.getBytes(Charset.forName("UTF-8"));

			HttpHeaders contentTypeHeaders = this.getContentTypeDispositionAndLengthHeaders(filename, format, data == null ? 0 : content.length);
			HttpHeaders cachingHeaders = this.getCachingHeaders(60);

			this._log.debug("Vessel by ID #{} for sources {} has been retrieved", id, CollectionsHelper.serializeArray(effectiveSources));

			return new ResponseEntity<byte[]>(content, this.mergeHeaders(contentTypeHeaders, cachingHeaders), HttpStatus.OK );
		} catch (Throwable t) {
			this._log.error("Unable to retrieve vessel data for ID #{}{} and format {}", id, sources == null ? "" : ", sources " + sources, format, t);

			return this.manageError(t);
		}
	}

	protected FullVessel doRetrieveVesselByID(String vesselsTable, Boolean byUID, Integer id, String[] sources) throws Throwable {
		String groupBy = Boolean.TRUE.equals(byUID) ? "UID" : "ID";
		this._log.debug("Retrieving vessel by " + groupBy + " #" + id + " for sources " + CollectionsHelper.serializeArray(sources));

		FullVessel vessel = null;

		if(byUID)
			vessel = this.fullVesselsDAO.selectFullByUIDAndDataSources(id, sources, vesselsTable);
		else
			vessel = this.fullVesselsDAO.selectFullByPrimaryKeyAndDataSources(id, sources);

		if(vessel == null)
			return null;

		String idAsString = String.valueOf(id);
		while(idAsString.length() < 9)
			idAsString = "0" + idAsString;

		return this.postProcess(vessel, sources == null || sources.length == 0 ? null : CollectionsHelper.join(sources, ","), byUID ? "UID" : "ID", idAsString);
	}

	protected abstract F getEmptySearchFilter();

	protected List<R> doRetrieveVesselsByIdentifier(S searchDAO, String vesselsTable, Boolean byUID, String identifierType, String identifier, String[] sources) throws Throwable {
		String groupBy = Boolean.TRUE.equals(byUID) ? "UID" : "ID";
		this._log.debug("Retrieving vessel(s) with {} {} grouped by {} for sources {}", identifierType, identifier, groupBy, CollectionsHelper.serializeArray(sources));

		F filter = this.getEmptySearchFilter();

		filter.SELECT(filter.columns().include(VesselRecordDataColumns.NAME, VesselRecordDataColumns.FLAG_ID));
		filter.FROM(vesselsTable);

		filter.setGroupBy(groupBy);
		filter.setLinkMetadataToSearchCriteria(Boolean.FALSE);

		filter.setSourceSystems(sources);
		filter.setAvailableSourceSystems(sources);

		filter.WHERE(filter.clauses().
				and(VesselRecordClauses.IDENTIFIER_TYPE, identifierType).
				and(VesselRecordClauses.IDENTIFIERS, identifier == null ? null : new String[] { identifier }));

		VesselRecordSearchResponse<R> response = searchDAO.searchVessels(filter);

		if(response != null)
			return response.getDataSet();

		return null;
	}
}