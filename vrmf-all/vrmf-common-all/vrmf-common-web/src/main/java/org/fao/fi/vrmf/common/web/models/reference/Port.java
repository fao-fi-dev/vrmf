/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.models.reference;

import org.fao.fi.vrmf.common.core.model.GenericData;
import org.fao.fi.vrmf.common.models.generated.SPorts;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Jan 2012
 */
public class Port extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6929944017604102792L;
	
	private Integer _id;
	private Integer _uid;
	private String _originalId;
	private String _country;
	private String _subdivisionCode;
	private String _subdivision;
	private String _name;
	private String _sourceSystem;
	
	/**
	 * Class constructor
	 *
	 * @param id
	 * @param uid
	 * @param originalId
	 * @param country
	 * @param subdivisionCode
	 * @param name
	 * @param sourceSystem
	 */
	public Port(Integer id, Integer uid, String originalId, String country, String subdivisionCode, String subdivision, String name, String sourceSystem) {
		super();
		this._id = id;
		this._uid = uid;
		this._originalId = originalId;
		this._country = country;
		this._subdivisionCode = subdivisionCode;
		this._subdivision = subdivision;
		this._name = name;
		this._sourceSystem = sourceSystem;
	}
	
	public Port(SPorts dto, String country, String subdivision) {
		this(dto.getId(), dto.getUid(), dto.getOriginalPortId(), country, dto.getSubdivisionId(), subdivision, dto.getName(), dto.getSourceSystem());
	}

	/**
	 * @return the 'id' value
	 */
	public Integer getId() {
		return this._id;
	}

	/**
	 * @param id the 'id' value to set
	 */
	public void setId(Integer id) {
		this._id = id;
	}

	/**
	 * @return the 'uid' value
	 */
	public Integer getUid() {
		return this._uid;
	}

	/**
	 * @param uid the 'uid' value to set
	 */
	public void setUid(Integer uid) {
		this._uid = uid;
	}

	/**
	 * @return the 'originalId' value
	 */
	public String getOriginalId() {
		return this._originalId;
	}

	/**
	 * @param originalId the 'originalId' value to set
	 */
	public void setOriginalId(String originalId) {
		this._originalId = originalId;
	}

	/**
	 * @return the 'country' value
	 */
	public String getCountry() {
		return this._country;
	}

	/**
	 * @param country the 'country' value to set
	 */
	public void setCountry(String country) {
		this._country = country;
	}

	/**
	 * @return the 'subdivisionCode' value
	 */
	public String getSubdivisionCode() {
		return this._subdivisionCode;
	}

	/**
	 * @param subdivisionCode the 'subdivisionCode' value to set
	 */
	public void setSubdivisionCode(String subdivisionCode) {
		this._subdivisionCode = subdivisionCode;
	}
	
	/**
	 * @return the 'subdivision' value
	 */
	public String getSubdivision() {
		return this._subdivision;
	}

	/**
	 * @param subdivision the 'subdivision' value to set
	 */
	public void setSubdivision(String subdivision) {
		this._subdivision = subdivision;
	}

	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return this._name;
	}

	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		this._name = name;
	}

	/**
	 * @return the 'sourceSystem' value
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}

	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._country == null) ? 0 : this._country.hashCode());
		result = prime * result + ((this._id == null) ? 0 : this._id.hashCode());
		result = prime * result + ((this._name == null) ? 0 : this._name.hashCode());
		result = prime * result + ((this._originalId == null) ? 0 : this._originalId.hashCode());
		result = prime * result + ((this._sourceSystem == null) ? 0 : this._sourceSystem.hashCode());
		result = prime * result + ((this._subdivisionCode == null) ? 0 : this._subdivisionCode.hashCode());
		result = prime * result + ((this._uid == null) ? 0 : this._uid.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Port))
			return false;
		Port other = (Port) obj;
		if (this._country == null) {
			if (other._country != null)
				return false;
		} else if (!this._country.equals(other._country))
			return false;
		if (this._id == null) {
			if (other._id != null)
				return false;
		} else if (!this._id.equals(other._id))
			return false;
		if (this._name == null) {
			if (other._name != null)
				return false;
		} else if (!this._name.equals(other._name))
			return false;
		if (this._originalId == null) {
			if (other._originalId != null)
				return false;
		} else if (!this._originalId.equals(other._originalId))
			return false;
		if (this._sourceSystem == null) {
			if (other._sourceSystem != null)
				return false;
		} else if (!this._sourceSystem.equals(other._sourceSystem))
			return false;
		if (this._subdivisionCode == null) {
			if (other._subdivisionCode != null)
				return false;
		} else if (!this._subdivisionCode.equals(other._subdivisionCode))
			return false;
		if (this._uid == null) {
			if (other._uid != null)
				return false;
		} else if (!this._uid.equals(other._uid))
			return false;
		return true;
	}	
}
