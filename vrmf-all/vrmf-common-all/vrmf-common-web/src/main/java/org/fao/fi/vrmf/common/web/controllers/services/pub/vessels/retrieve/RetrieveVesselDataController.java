/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.retrieve;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.fao.fi.vrmf.common.web.controllers.services.pub.CommonPublicServicesConstants;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 May 2012
 */
@Singleton @Controller
@RequestMapping(CommonPublicServicesConstants.VESSELS_RETRIEVE_SERVICES_PREFIX + "data/*")
public class RetrieveVesselDataController extends CommonRetrieveVesselDataController<VesselRecord, VesselRecordSearchFilter> {
	@RequestMapping(value="UID/{UID:[0-9]{9}}.{format:xml|json|jsonp}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> retrieveByUID(HttpServletRequest request, @PathVariable Integer UID, @PathVariable String format, @RequestParam(value="jsonpcallback", required=false) String callback) {
		return this.doRetrieveByUID(request, INCLUDE_OTHER_SOURCES_BY_DEFAULT, null, UID, format, callback, null, REQUIRED_ROLES, REQUIRED_CAPABILITIES);
	}
	
	@RequestMapping(value="{sources}/UID/{UID:[0-9]{9}}.{format:xml|json|jsonp}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> retrieveByUID(HttpServletRequest request, @PathVariable String sources, @PathVariable Integer UID, @PathVariable String format, @RequestParam(value="jsonpcallback", required=false) String callback) {
		return this.doRetrieveByUID(request, INCLUDE_OTHER_SOURCES_BY_DEFAULT, sources, UID, format, callback, null, REQUIRED_ROLES, REQUIRED_CAPABILITIES);
	}

	@RequestMapping(value="ID/{ID:[0-9]{1,9}}.{format:xml|json|jsonp}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> retrieveByID(HttpServletRequest request, @PathVariable Integer ID, @PathVariable String format, @RequestParam(value="jsonpcallback", required=false) String callback) {
		return this.doRetrieveByID(request, INCLUDE_OTHER_SOURCES_BY_DEFAULT, null, ID, format, callback, null, REQUIRED_ROLES, REQUIRED_CAPABILITIES);
	}
	
	@RequestMapping(value="{sources}/ID/{ID:[0-9]{1,9}}.{format:xml|json|jsonp}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> retrieveByID(HttpServletRequest request, @PathVariable String sources, @PathVariable Integer ID, @PathVariable String format, @RequestParam(value="jsonpcallback", required=false) String callback) {
		return this.doRetrieveByID(request, INCLUDE_OTHER_SOURCES_BY_DEFAULT, sources, ID, format, callback, null, REQUIRED_ROLES, REQUIRED_CAPABILITIES);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.web.controllers.services.pub.vessels.retrieve.AbstractRetrieveVesselDataController#getEmptySearchFilter()
	 */
	@Override
	protected VesselRecordSearchFilter getEmptySearchFilter() {
		return new VesselRecordSearchFilter();
	}
}