/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.metadata;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.model.core.auth.UserModel;
import org.fao.fi.sh.utility.core.helpers.singletons.io.MD5Helper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.j2ee.RequestAttributesConstants;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.RequestHeadersConstants;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.ResponseHeadersConstants;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.generated.SAuthorizationTypes;
import org.fao.fi.vrmf.common.models.generated.SSystems;
import org.fao.fi.vrmf.common.web.constants.services.CommonSearchServiceConstants;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.common.web.controllers.services.pub.CommonPublicServicesConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Singleton @Controller
@RequestMapping(CommonPublicServicesConstants.METADATA_SERVICES_PREFIX + "reference/*")
public class ReferenceMetadataDataController extends AbstractController implements CommonSearchServiceConstants {
	@Inject private @Getter @Setter ReferenceMetadataDataRetriever retriever;

	@RequestMapping(value="data/port/get/id/{portId:[0-9]+}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getPortByID(HttpServletRequest request, @PathVariable Integer portId, @PathVariable String format) {
		try {
			List<String> sourceSystems = new ArrayList<String>();
			sourceSystems.addAll(Arrays.asList(this.getAvailableSources(request)));

			byte[] data = null;
			String content = this.retriever.doGetPortByIDContent(sourceSystems.toArray(new String[sourceSystems.size()]), portId, format);

			if(content == null)
				return this.notFound();

			data = content.getBytes(Charset.forName("UTF-8"));

			HttpHeaders contentHeaders = this.getContentTypeDispositionAndLengthHeaders("Port_" + portId, format, data.length);

			return new ResponseEntity<byte[]>(data, contentHeaders, HttpStatus.OK);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="factsheet/psm/get/id/prefixes/{prefixes}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getPSMFactsheetDataByIDPrefixes(HttpServletRequest request, @PathVariable String prefixes, @PathVariable String format) {
		try {
			List<String> sourceSystems = new ArrayList<String>();
			sourceSystems.addAll(Arrays.asList(this.getAvailableSources(request)));

			byte[] data = null;
			String content = this.retriever.doGetPSMFactsheetDataByIDPrefixes(sourceSystems.toArray(new String[sourceSystems.size()]), prefixes == null ? null : prefixes.split("\\,"), format);

			if(content == null)
				return this.notFound();

			data = content.getBytes(Charset.forName("UTF-8"));

			HttpHeaders contentHeaders = this.getContentTypeDispositionAndLengthHeaders("FS_PSM_" + prefixes, format, data.length);

			return new ResponseEntity<byte[]>(data, contentHeaders, HttpStatus.OK);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="factsheet/psm/get/id/{fid}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getPSMFactsheetDataByID(HttpServletRequest request, @PathVariable String fid, @PathVariable String format) {
		try {
			List<String> sourceSystems = new ArrayList<String>();
			sourceSystems.addAll(Arrays.asList(this.getAvailableSources(request)));

			byte[] data = null;
			String content = this.retriever.doGetPSMFactsheetDataByID(sourceSystems.toArray(new String[sourceSystems.size()]), fid, format);

			if(content == null)
				return this.notFound();

			data = content.getBytes(Charset.forName("UTF-8"));

			HttpHeaders contentHeaders = this.getContentTypeDispositionAndLengthHeaders("FS_PSM_" + fid, format, data.length);

			return new ResponseEntity<byte[]>(data, contentHeaders, HttpStatus.OK);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="factsheet/species/get/id/{fid}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getSpeciesFactsheetDataByID(HttpServletRequest request, @PathVariable String fid, @PathVariable String format) {
		try {
			List<String> sourceSystems = new ArrayList<String>();
			sourceSystems.addAll(Arrays.asList(this.getAvailableSources(request)));

			byte[] data = null;
			String content = this.retriever.doGetSpeciesFactsheetDataByID(sourceSystems.toArray(new String[sourceSystems.size()]), fid, format);

			if(content == null)
				return this.notFound();

			data = content.getBytes(Charset.forName("UTF-8"));

			HttpHeaders contentHeaders = this.getContentTypeDispositionAndLengthHeaders("FS_SPECIES_" + fid, format, data.length);

			return new ResponseEntity<byte[]>(data, contentHeaders, HttpStatus.OK);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}

	@RequestMapping(value="data/{categories}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getReferenceDataByCategories(HttpServletRequest request, @PathVariable String categories, @PathVariable String format) {
		return this.doGetReferenceData(request, categories.split("\\,"), format);
	}

	@RequestMapping(value="data/all.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getAllReferenceData(HttpServletRequest request, @PathVariable String format) {
		return this.doGetReferenceData(request, ReferenceMetadataDataRetriever.CATEGORIES.toArray(new String[ReferenceMetadataDataRetriever.CATEGORIES.size()]), format);
	}

	private String eTag(UserModel loggedUser, String[] sources, String[] categories, String format)  throws Throwable {
		StringBuilder key = new StringBuilder(loggedUser == null ? "" : loggedUser.getId());
		key.append("_").append(sources == null || sources.length == 0 ? "" : CollectionsHelper.join(CollectionsHelper.sortArray(sources), "_"));
		key.append("_").append(categories == null || categories.length == 0 ? "" : CollectionsHelper.join(CollectionsHelper.sortArray(categories), "_"));
		key.append("_").append(format);

		StringBuilder eTag = new StringBuilder("\"").append(MD5Helper.digest(key.toString())).append("\"");

		this._log.debug("Returning eTag {} for {}", eTag.toString(), key.toString());

		return eTag.toString();
	}

	private ResponseEntity<byte[]> cacheIfPossible(HttpServletRequest request, String[] categories, String format) throws Throwable {
		String ifNoneMatchHeader = request.getHeader(RequestHeadersConstants.REQUEST_HEADER_IF_NONE_MATCH);

		String eTag = this.eTag(ServletsHelper.getLoggedUser(request),
								ServletsHelper.getAvailableSources(request, ServletsHelper.INCLUDE_OTHER_SOURCES),
								categories,
								format);

		if(eTag.equals(ifNoneMatchHeader)) {
			this._log.debug("Resource " + request.getRequestURI() + " was not modified. Sending a 304...");

			return new ResponseEntity<byte[]>(HttpStatus.NOT_MODIFIED);/* 304 - Not Modified */
		}

		return null;
	}

	private ResponseEntity<byte[]> doGetReferenceData(HttpServletRequest request, String[] categories, String format) {
		try {
			this._log.debug("Building reference data for {} ({})", CollectionsHelper.serializeArray(categories), format);

			String lang = (String)request.getAttribute(RequestAttributesConstants.CURRENT_LANGUAGE_REQUEST_ATTRIBUTE);
			
			String[] availableSourceSystems = this.getAvailableSources(request);

			ResponseEntity<byte[]> response = this.cacheIfPossible(request, categories, format);

			if(response == null) {
				byte[] data = null;
				String content = this.retriever.doGetReferenceDataContent(lang, availableSourceSystems, categories, format);

				data = content == null ? new byte[0] : content.getBytes(Charset.forName("UTF-8"));

				HttpHeaders contentHeaders = this.getContentTypeDispositionAndLengthHeaders("Metadata_" + CollectionsHelper.join(categories, "_"), format, data.length);
				HttpHeaders eTagHeaders = new HttpHeaders();
				eTagHeaders.add(ResponseHeadersConstants.RESPONSE_HEADER_ETAG,
								this.eTag(ServletsHelper.getLoggedUser(request),
										  ServletsHelper.getAvailableSources(request, ServletsHelper.INCLUDE_OTHER_SOURCES),
										  categories,
										  format));

				this._log.debug("Returning reference data for  {} ({})", CollectionsHelper.serializeArray(categories), format);

				response = new ResponseEntity<byte[]>(data, this.mergeHeaders(contentHeaders, eTagHeaders), HttpStatus.OK);
			}

			return response;
		} catch (Throwable t) {
			return this.manageError(t);
		} finally {
			this._log.debug("Reference data for {} ({}) have been returned", CollectionsHelper.serializeArray(categories), format);
		}
	}

	protected Map<String, SSystems> getAuthorizationIssuerSystemsMap(List<SSystems> managedSystems) {
		this._log.debug("Initializing authorization issuers systems map data...");

		Map<String, SSystems> systemsMap = new HashMap<String, SSystems>();

		try {
			if(managedSystems != null)
				for(SSystems system : managedSystems)
					systemsMap.put(system.getId(), system);

			this._log.debug("Authorization issuers systems map data have been initialized!");
		} catch (Throwable t) {
			this._log.error("Unable to initialize authorization issuers systems map data", t);
		}

		return systemsMap;
	}

	protected Map<String, List<SAuthorizationTypes>> getAuthorizationTypesMap(Collection<SAuthorizationTypes> authorizationTypes) {
		Map<String, List<SAuthorizationTypes>> authorizationTypesMap = new HashMap<String, List<SAuthorizationTypes>>();
		List<SAuthorizationTypes> currentTypes;

		try {
			for(SAuthorizationTypes type : authorizationTypes) {
				if((currentTypes = authorizationTypesMap.get(type.getSourceSystem())) == null) {
					currentTypes = new ListSet<SAuthorizationTypes>();

					authorizationTypesMap.put(type.getSourceSystem(), currentTypes);
				}

				currentTypes.add(type);
			}
		} catch (Throwable t) {
			this._log.error("Unable to initialize the authorization types map", t);
		}

		return authorizationTypesMap;
	}
}