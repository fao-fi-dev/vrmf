/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.models.reference;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.models.generated.SGearTypes;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Jan 2012
 */
@XmlRootElement(name="gearType")
public class GearType extends CommonMetaType {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6751016982802442406L;
	
	@XmlElement(name="ISSCFGCode")
	private String _ISSCFGCode;

	/**
	 * Class constructor
	 *
	 * @param id
	 * @param uid
	 * @param isscfgCode
	 * @param metaCode
	 * @param standardAbbreviation
	 * @param name
	 * @param description
	 * @param sourceSystem
	 */
	public GearType(Integer id, Integer uid, String originalId, String isscfgCode, Integer metaCode, String standardAbbreviation, String name, String description, String sourceSystem) {
		super(id, uid, originalId, metaCode, standardAbbreviation, name, description, sourceSystem);
		
		this._ISSCFGCode = isscfgCode;
	}
	
	public GearType(SGearTypes dto) {
		this(dto.getId(), dto.getUid(), dto.getOriginalGearTypeCode(), dto.getIsscfgCode(), dto.getMetaCode(), dto.getStandardAbbreviation(), dto.getName(), dto.getDescription(), dto.getSourceSystem());
	}

	/**
	 * @return the 'iSSCFGCode' value
	 */
	public String getISSCFGCode() {
		return this._ISSCFGCode;
	}

	/**
	 * @param iSSCFGCode the 'iSSCFGCode' value to set
	 */
	public void setISSCFGCode(String iSSCFGCode) {
		this._ISSCFGCode = iSSCFGCode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._ISSCFGCode == null) ? 0 : this._ISSCFGCode.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof GearType))
			return false;
		GearType other = (GearType) obj;
		if (this._ISSCFGCode == null) {
			if (other._ISSCFGCode != null)
				return false;
		} else if (!this._ISSCFGCode.equals(other._ISSCFGCode))
			return false;
		return true;
	}
}