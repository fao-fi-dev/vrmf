/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.redirectors;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 Jan 2012
 */
@Singleton @Controller @RequestMapping("*")
public class DisplayCombinedVesselDataRedirector {
	@RequestMapping(value="{application}/display/combined/file/{fileId}/row/{rowId}/uid/{uid}", method=RequestMethod.GET)
	public void checkExistingVessel(HttpServletRequest servletRequest, HttpServletResponse servletResponse, @PathVariable String application, @PathVariable Integer fileId, @PathVariable Integer rowId, @PathVariable Integer uid) throws Throwable {
		servletRequest.getRequestDispatcher("/" + application + "/display.jsp?fileId=" + fileId + "&rowId=" + rowId + "&uid=" + uid).forward(servletRequest, servletResponse);
	}
}
