/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.constants.services;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Sep 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Sep 2011
 */
public interface CommonAddUserServiceConstants {
	String GRANTER_FIELDS_PREFIX        = "granter_";
	
	String ENTITY_TYPE_REQUEST_PARAMETER= "entityType";
	String FULL_NAME_REQUEST_PARAMETER  = "fullName";
	String SALUTATION_REQUEST_PARAMETER = "salutation";
	String BRANCH_REQUEST_PARAMETER 	= "branch";
	String ROLE_REQUEST_PARAMETER 		= "role";
	String COUNTRY_REQUEST_PARAMETER    = "country";
	String CITY_REQUEST_PARAMETER    	= "city";
	String ADDRESS_REQUEST_PARAMETER    = "address";
	String ZIP_CODE_REQUEST_PARAMETER   = "zipCode";	
	String EMAIL_REQUEST_PARAMETER    	= "email";
	String PHONE_REQUEST_PARAMETER    	= "phone";
	String MOBILE_REQUEST_PARAMETER    	= "mobile";
	String FAX_REQUEST_PARAMETER    	= "fax";
	String WEBSITE_REQUEST_PARAMETER    = "website";
	String COMMENT_REQUEST_PARAMETER    = "comment";
	String TYPE_REQUEST_PARAMETER		= "type";

	String MANAGED_COUNTRIES_REQUEST_PARAMETER = "managedCountries";
	
	String ENTITY_ID_REQUEST_PARAMETER  = "entityId";
	
	String ADDITIONAL_ROLES_REQUEST_PARAMETER = "additionalRoles";
	String AVAILABLE_ROLES_REQUEST_PARAMETER = "availableRoles";
	
	String SOURCE_SYSTEM_REQUEST_PARAMETER 	  = "system";
	String CURRENT_SYSTEM_REQUEST_PARAMETER	  = "currentSystem";
	
	String UPDATER_REQUEST_PARAMETER	= "updater";	
}
