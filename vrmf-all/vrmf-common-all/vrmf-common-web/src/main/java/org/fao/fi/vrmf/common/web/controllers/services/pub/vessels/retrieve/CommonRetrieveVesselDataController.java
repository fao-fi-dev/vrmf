/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.retrieve;

import java.util.ArrayList;
import java.util.List;

import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.business.dao.CommonVesselRecordSearchDAO;
import org.fao.fi.vrmf.common.models.extended.ExtendedAuthorizations;
import org.fao.fi.vrmf.common.models.extended.FullVessel;
import org.fao.fi.vrmf.common.models.generated.SSystems;
import org.fao.fi.vrmf.common.models.generated.SSystemsExample;
import org.fao.fi.vrmf.common.models.generated.Vessels;
import org.fao.fi.vrmf.common.models.generated.VesselsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 5 Jul 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 5 Jul 2012
 */
abstract public class CommonRetrieveVesselDataController<RECORD extends VesselRecord, FILTER extends VesselRecordSearchFilter> extends AbstractRetrieveVesselDataController<RECORD, FILTER, CommonVesselRecordSearchDAO<RECORD, FILTER>> {
	static final protected String[] REQUIRED_ROLES 		  = null;
	static final protected String[] REQUIRED_CAPABILITIES = { "EXPORT_V_DATA" };

	private boolean isAuthorizedByCLAV(FullVessel vessel, Integer vesselID) {
		List<ExtendedAuthorizations> auths = vessel.getAuthorizations();
		
		if(auths != null) {
			for(ExtendedAuthorizations auth : auths)
				if(vesselID == null || auth.getVesselId().equals(vesselID))
					if(auth.getSourceSystem().equals("CLAV"))
						return true;
		}
		
		return false;
	}
	
	/** Returns public vessel sources only */
	final protected List<SSystems> getVesselSources() throws Throwable {
		return this.getVesselSources(false);
	}
	
	final protected List<SSystems> getVesselSources(boolean includePrivate) throws Throwable {
		SSystemsExample filter = new SSystemsExample();
		SSystemsExample.Criteria criteria = filter.createCriteria();
		criteria.andVesselSourceEqualTo(Boolean.TRUE);
		
		if(!includePrivate)
			criteria.andIsPublicEqualTo(true);
		
		return this.sourcesDAO.selectByExample(filter);
	}
	
	final protected List<String> getVesselSourcesID() throws Throwable {
		return this.getVesselSourcesID(false);
	}
	
	final protected List<String> getVesselSourcesID(boolean includePrivate) throws Throwable {
		List<String> sourceIDs = new ListSet<String>();
		
		for(SSystems source : this.getVesselSources(includePrivate))
			sourceIDs.add(source.getId());

		return sourceIDs;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.controllers.services.retrieve.vessel.RetrieveVesselDataController#postProcess(org.fao.vrmf.utilities.common.models.extended.FullVessel)
	 */
	@Override
	final protected FullVessel postProcess(FullVessel vessel, String sources, String identifierType, String identifier) {
		vessel = super.postProcess(vessel, sources, identifierType, identifier);
		
		if(vessel == null)
			return null;
		
		List<VesselsToIdentifiers> identifiers = vessel.getIdentifiers() == null ? new ArrayList<VesselsToIdentifiers>() : new ArrayList<VesselsToIdentifiers>(vessel.getIdentifiers());
				
		final Integer UID = vessel.getUid();
				
		String UIDAsString = String.valueOf(UID);
		String TUVIAsString = UIDAsString;
		
		while(TUVIAsString.length() < 7)
			TUVIAsString = "0" + TUVIAsString;
		
		while(UIDAsString.length() < 9)
			UIDAsString = "0" + UIDAsString;

		for(Integer id : vessel.getIds()) {
			VesselsToIdentifiers VRMF_UID = new VesselsToIdentifiers();
			VRMF_UID.setVesselId(id);
			VRMF_UID.setVesselUid(UID);
			VRMF_UID.setTypeId("VRMF_UID");
			VRMF_UID.setIdentifier(UIDAsString);

			VRMF_UID.setSourceSystem("VRMF");

			VRMF_UID.setUpdateDate(vessel.getUpdateDate());
			VRMF_UID.setUpdaterId("VRMF_ADMIN");

			VRMF_UID.setComment("The VRMF UID for vessel with VRMF ID #" + id);
			
			if(this.isAuthorizedByCLAV(vessel, id)) {
				VesselsToIdentifiers TUVI = new VesselsToIdentifiers();
				TUVI.setVesselId(id);
				TUVI.setVesselUid(UID);
				TUVI.setTypeId("TUVI");
				TUVI.setIdentifier("TUVI" + TUVIAsString);
	
				TUVI.setSourceSystem("CLAV");
	
				TUVI.setUpdateDate(vessel.getUpdateDate());
				TUVI.setUpdaterId("CLAV_ADMIN");
	
				TUVI.setComment("The CLAV TUVI number for vessel with VRMF ID #" + id);
				
				identifiers.add(0, TUVI);
			}
			
			identifiers.add(0, VRMF_UID);
		}
		
		vessel.setIdentifiers(identifiers);
		
		//Sets the list of sources this vessel's duplicates appear in (by UID)
		try {
			List<String> allPublicVesselSources = this.getVesselSourcesID();
			
			if(allPublicVesselSources != null && !allPublicVesselSources.isEmpty()) {
				VesselsExample filter = new VesselsExample();
				filter.createCriteria().andUidEqualTo(UID).andSourceSystemIn(allPublicVesselSources);
				
				List<Vessels> joined = this.vesselsDAO.selectByExample(filter);
				
				List<String> allAvailableSourceSystems = new ListSet<String>();
				
				for(Vessels component : joined) {
					allAvailableSourceSystems.add(component.getSourceSystem());
				}
				
				vessel.setAllAvailableSourceSystemsForVessel(allAvailableSourceSystems);
			}
		} catch (Throwable t) {
			this._log.warn("Unable to retrieve other sources for vessel with UID #" + UIDAsString, t);
		}
		
//		try {
//			String[] sourcesArray = StringHelper.rawTrim(sources) == null ? null : sources.split(",");
//			String pattern = "%" + ( sourcesArray == null || sourcesArray.length > 1 ? "" : sourcesArray[0] + "/%");
//			pattern += "/display/vessel/" + identifierType + "/" + identifier;
//			
//			this._log.warn("Finding vessel views for " + sources + " / " + identifierType + " / " + identifier + " by pattern: " + pattern + " ");
//			
//			RemoteRequestMetadataExample filter = new RemoteRequestMetadataExample();
//			filter.createCriteria().andRequestUrlLike(pattern).andUserAgentTypeEqualTo("Browser");
//			
//			vessel.setViews(this._requestMetadataDAO.countByExample(filter));
//			
//			this._log.warn(vessel.getViews() + " views for " + sources + " / " + identifierType + " / " + identifier);
//		} catch (Throwable t) {
//			
//		}
		
		return vessel;
	}	
}