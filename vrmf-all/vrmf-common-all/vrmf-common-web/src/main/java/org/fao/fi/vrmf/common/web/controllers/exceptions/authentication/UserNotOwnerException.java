/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.exceptions.authentication;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2012
 */
public class UserNotOwnerException extends UnauthorizedUserException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1605776674513439014L;

	static final private String DEFAULT_MESSAGE = "Currently logged user is not the data owner";

	/**
	 * Class constructor
	 *
	 */
	public UserNotOwnerException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public UserNotOwnerException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public UserNotOwnerException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public UserNotOwnerException(Throwable cause) {
		super(DEFAULT_MESSAGE, cause);
	}	
}
