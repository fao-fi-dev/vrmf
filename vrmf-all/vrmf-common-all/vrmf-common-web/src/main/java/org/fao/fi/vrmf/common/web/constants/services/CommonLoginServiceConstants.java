/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.constants.services;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 Aug 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 Aug 2011
 */
public interface CommonLoginServiceConstants {
	String USERNAME_REQUEST_PARAM 	  	 = "username";
	String USERNAME_SEARCH_REQUEST_PARAM = "username_search";
	String PASSWORD_REQUEST_PARAM 	  	 = "password";
	String NEW_PASSWORD_REQUEST_PARAM 	 = "newPassword";
	
	String REQUIRED_ROLES_INIT_PARAM 		= "REQUIRED_ROLES";
	String REQUIRED_CAPABILITIES_INIT_PARAM = "REQUIRED_CAPABILITIES";
}
