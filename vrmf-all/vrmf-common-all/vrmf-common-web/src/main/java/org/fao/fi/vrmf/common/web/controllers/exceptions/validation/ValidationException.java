/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.exceptions.validation;

import org.fao.fi.vrmf.common.web.controllers.exceptions.PreconditionException;
import org.springframework.http.HttpStatus;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2012
 */
public class ValidationException extends PreconditionException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 633511870405972314L;

	/**
	 * Class constructor
	 *
	 */
	public ValidationException() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public ValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public ValidationException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public ValidationException(Throwable cause) {
		super(cause);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.controllers.exceptions.ControllerException#getCorrespondingHttpStatusCode()
	 */
	@Override
	public HttpStatus getCorrespondingHttpStatusCode() {
		return HttpStatus.BAD_REQUEST;
	}
}