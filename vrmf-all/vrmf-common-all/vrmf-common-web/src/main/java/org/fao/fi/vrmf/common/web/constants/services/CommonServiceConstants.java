/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.constants.services;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Sep 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Sep 2011
 */
public interface CommonServiceConstants {
	String SERVICE_TARGET_APPLICATION_INIT_PARAMETER = "TARGET_APPLICATION";
	String SERVICE_IDENTIFIER_INIT_PARAMETER 		 = "SERVICE_IDENTIFIER";
	
	String VRMF_APPLICATION_ID  = "VRMF";
	String HSVAR_APPLICATION_ID = "HSVAR";
	String CLAV_APPLICATION_ID  = "CLAV";
	
	String DEFAULT_APPLICATION_ID = VRMF_APPLICATION_ID;
}
