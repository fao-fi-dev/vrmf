/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.exceptions.authentication;

import org.fao.fi.vrmf.common.web.controllers.exceptions.PreconditionException;
import org.springframework.http.HttpStatus;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2012
 */
public class UnauthorizedUserException extends PreconditionException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1605776674513439014L;

	static final private String DEFAULT_MESSAGE = "Currently logged user is unauthorized to perform the requested operation";
	
	/**
	 * Class constructor
	 *
	 */
	public UnauthorizedUserException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public UnauthorizedUserException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public UnauthorizedUserException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public UnauthorizedUserException(Throwable cause) {
		super(DEFAULT_MESSAGE, cause);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.web.controllers.exceptions.ControllerException#getCorrespondingHttpStatusCode()
	 */
	@Override
	public HttpStatus getCorrespondingHttpStatusCode() {
		return HttpStatus.UNAUTHORIZED;
	}	
}