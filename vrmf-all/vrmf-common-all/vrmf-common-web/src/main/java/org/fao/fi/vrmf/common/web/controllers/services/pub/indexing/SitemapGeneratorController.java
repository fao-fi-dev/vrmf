/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.indexing;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.core.model.i18n.LocalizationInfo;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.business.dao.VesselIndexingDAO;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.indexing.SourceUpdateMetadata;
import org.fao.fi.vrmf.common.models.indexing.VesselUpdateMetadata;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.common.web.controllers.services.pub.CommonPublicServicesConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Oct 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Oct 2012
 */
@NoArgsConstructor
@Singleton @Controller 
@RequestMapping(CommonPublicServicesConstants.INDEXING_SERVICES_PREFIX)
public class SitemapGeneratorController extends AbstractController {
	static final private String LASTMOD_DATE_PATTERN = "yyyy-MM-dd";
	
	@Inject private @Getter @Setter VesselIndexingDAO _vesselsIndexingDAO;

	static final int    DEFAULT_MAX_ENTRIES = 5000;
	static final String SITEMAP_INDEX_MAPPING   = "sitemapIndex.xml";
	static final String DYNAMIC_SITEMAP_MAPPING = "dynamicSitemap.xml";
	static final String SOURCES_SITEMAP_MAPPING = "sourcesSitemap.xml";
	
	static final String IMO_SITEMAP_INDEX_MAPPING   = "IMO_" + SITEMAP_INDEX_MAPPING;
	static final String IMO_DYNAMIC_SITEMAP_MAPPING = "IMO_" + DYNAMIC_SITEMAP_MAPPING;
	static final String IMO_SOURCES_SITEMAP_MAPPING = "IMO_" + SOURCES_SITEMAP_MAPPING;
	
	private LocalizationInfo _localizationInfo;
	
	
	/**
	 * @return the 'localizationInfo' value
	 */
	public LocalizationInfo getLocalizationInfo() {
		return this._localizationInfo;
	}

	/**
	 * @param localizationInfo the 'localizationInfo' value to set
	 */
	@Inject @Named("vrmf.localizationInfo") 
	public void setLocalizationInfo(LocalizationInfo localizationInfo) {
		this._log.info("Setting localization info {} into {}", localizationInfo, this);
		
		if(localizationInfo != null) {
			this._log.info("Default language: {}", localizationInfo.getDefaultLanguage());
			this._log.info("Other languages: {}", CollectionsHelper.serializeArray(localizationInfo.getOtherLanguages()));
		}
		
		this._localizationInfo = localizationInfo;
	}

	private String[] getAvailableLanguages() {
		if(this._localizationInfo == null)
			return new String[] { "en" };
		
		return this._localizationInfo.getAllAvailableLanguages();
	}
	
	private String padVesselUID(Integer id) {
		return this.padID(id, 9);
	}
	
	private String padIMO(Integer imo) {
		return imo == null ? null : String.valueOf(imo);
	}
	
	private String padID(Integer id, int length) {
		String padded = id.toString();
		
		while(padded.length() < length)
			padded = "0" + padded;
		
		return padded;
	}
	
	@RequestMapping(value=SITEMAP_INDEX_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getSitemapIndex(HttpServletRequest request) throws Throwable {
		return this.doGetSitemapIndex(request, null, null);
	}
	
	@RequestMapping(value="lastMod/{lastMod:[0-9]{4}-[0-9]{2}-[0-9]{2}}/" + SITEMAP_INDEX_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getSitemapIndex(HttpServletRequest request, @PathVariable String lastMod) throws Throwable {
		return this.doGetSitemapIndex(request, null, lastMod);
	}
	
	@RequestMapping(value="max/{max:[1-9][0-9]*}/" + SITEMAP_INDEX_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getSitemapIndex(HttpServletRequest request, @PathVariable(value="max") Integer maxEntries) throws Throwable {
		return this.doGetSitemapIndex(request, maxEntries, null);
	}	
	
	@RequestMapping(value="lastMod/{lastMod:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{max:[1-9][0-9]*}/" + SITEMAP_INDEX_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getSitemapIndex(HttpServletRequest request, @PathVariable String lastMod, @PathVariable(value="max") Integer maxEntries) throws Throwable {
		return this.doGetSitemapIndex(request, maxEntries, lastMod);
	}
	
	private ResponseEntity<byte[]> doGetSitemapIndex(HttpServletRequest request, Integer maxEntries, String lastMod) throws Throwable {
		try {
			int limit = maxEntries == null || maxEntries <= 0 ? DEFAULT_MAX_ENTRIES : maxEntries;
			
			String[] sources = this.filterSources(this.getAvailableSources(request, ServletsHelper.DONT_INCLUDE_OTHER_SOURCES));
			
			String baseURL = this.doGetBaseURL(request) + "/services" + CommonPublicServicesConstants.INDEXING_SERVICES_PREFIX;
			
			int numVesselsBySources = this._vesselsIndexingDAO.countSourcesData(sources, "UID");
			int numIterations = numVesselsBySources / limit;
			
			if(numVesselsBySources % limit > 0)
				numIterations++;
			
			StringBuilder result = new StringBuilder();
			result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			result.append("<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");

			if(sources != null && sources.length > 1) {
				for(String language : this.getAvailableLanguages()) {
					result.append("\t<sitemap>\n");
					result.append("\t\t<loc>").append(baseURL).
											   append(lastMod == null ? "" : "lastMod/" + lastMod).
											   append(lastMod == null ? "" : "/").
											   append(language).append("_").append(SOURCES_SITEMAP_MAPPING).append("</loc>\n");
					
					if(lastMod != null) {
						result.append("\t\t<lastmod>").append(lastMod).append("</lastmod>\n");
					}
					
					result.append("\t</sitemap>\n");
				}
			}
			
			String lastModPath = lastMod == null ? "" : "lastMod/" + lastMod + "/";
			
			for(int v=0; v<numIterations; v++) {
				for(String language : this.getAvailableLanguages()) {
					result.append("\t<sitemap>\n");
					result.append("\t\t<loc>").append(baseURL).append(lastModPath).append("offset/").append(v * limit).append("/max/").append(limit).append("/").append(language).append("_").append(DYNAMIC_SITEMAP_MAPPING).append("</loc>\n");
					
					if(lastMod != null) {
						result.append("\t\t<lastmod>").append(lastMod).append("</lastmod>\n");
					}
					
					result.append("\t</sitemap>\n");
				}
			}
			
			result.append("</sitemapindex>\n");
			
			byte[] content = result.toString().getBytes("UTF-8");
			
			HttpHeaders headers = this.getContentTypeDispositionAndLengthHeaders(SOURCES_SITEMAP_MAPPING, "xml", content.length);
			
			return new ResponseEntity<byte[]>(content, headers, HttpStatus.OK);
			
		} catch (Throwable t) {
			return this.errorCode(HttpStatus.INTERNAL_SERVER_ERROR, t.getMessage());
		}
	}
	
	@RequestMapping(value=IMO_SITEMAP_INDEX_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getIMOSitemapIndex(HttpServletRequest request) throws Throwable {
		return this.doGetIMOSitemapIndex(request, null, null);
	}
	
	@RequestMapping(value="lastMod/{lastMod:[0-9]{4}-[0-9]{2}-[0-9]{2}}/" + IMO_SITEMAP_INDEX_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getIMOSitemapIndex(HttpServletRequest request, @PathVariable String lastMod) throws Throwable {
		return this.doGetIMOSitemapIndex(request, null, lastMod);
	}
	
	@RequestMapping(value="max/{max:[1-9][0-9]*}/" + IMO_SITEMAP_INDEX_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getIMOSitemapIndex(HttpServletRequest request, @PathVariable(value="max") Integer maxEntries) throws Throwable {
		return this.doGetIMOSitemapIndex(request, maxEntries, null);
	}
	
	@RequestMapping(value="lastMod/{lastMod:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{max:[1-9][0-9]*}/" + IMO_SITEMAP_INDEX_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getIMOSitemapIndex(HttpServletRequest request, @PathVariable String lastMod, @PathVariable(value="max") Integer maxEntries) throws Throwable {
		return this.doGetIMOSitemapIndex(request, maxEntries, lastMod);
	}
	
	private ResponseEntity<byte[]> doGetIMOSitemapIndex(HttpServletRequest request, Integer maxEntries, String lastMod) throws Throwable {
		try {
			int limit = maxEntries == null || maxEntries <= 0 ? DEFAULT_MAX_ENTRIES : maxEntries;
			
			String baseURL = this.doGetBaseURL(request) + "/services" + CommonPublicServicesConstants.INDEXING_SERVICES_PREFIX;
			String sourceBaseURL;
			
			StringBuilder result = new StringBuilder();
			result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
			result.append("<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");

			String[] sources = this.filterSources(this.getAvailableSources(request, ServletsHelper.DONT_INCLUDE_OTHER_SOURCES));

			int numVesselsBySources, numIterations;
			
			String lastModPath = lastMod == null ? "" : "lastMod/" + lastMod + "/";
			

			if(sources != null && sources.length > 1) {
				for(String source : sources) {
					numVesselsBySources = this._vesselsIndexingDAO.countIMOSourcesData(new String[] { source }, "UID");
					
					if(numVesselsBySources > 0) {
						numIterations = numVesselsBySources / limit;
						
						if(numVesselsBySources % limit > 0)
							numIterations++;

						for(int v=0; v<numIterations; v++) {
							for(String lang : this.getAvailableLanguages()) {
								sourceBaseURL = this.doGetBaseURL(request) + "/" + source + "/services" + CommonPublicServicesConstants.INDEXING_SERVICES_PREFIX;
								
								result.append("\t<sitemap>\n");
								result.append("\t\t<loc>").append(sourceBaseURL).append(lastModPath).append("offset/").append(v * limit).append("/max/").append(limit).append("/").append(lang).append("_").append(IMO_DYNAMIC_SITEMAP_MAPPING).append("</loc>\n");
								
								if(lastMod != null) {
									result.append("\t\t<lastmod>").append(lastMod).append("</lastmod>\n");
								}
									
								result.append("\t</sitemap>\n");
							}
						}
					}
				}
				
				numVesselsBySources = this._vesselsIndexingDAO.countIMOSourcesData(sources, "UID");
				
				if(numVesselsBySources > 0) {
					numIterations = numVesselsBySources / limit;
	
					if(numVesselsBySources % limit > 0)
						numIterations++;
					
					for(int v=0; v<numIterations; v++) {
						for(String lang : this.getAvailableLanguages()) {
							result.append("\t<sitemap>\n");
							result.append("\t\t<loc>").append(baseURL).append(lastModPath).append("offset/").append(v * limit).append("/max/").append(limit).append("/").append(lang).append("_").append(IMO_DYNAMIC_SITEMAP_MAPPING).append("</loc>\n");
								
							if(lastMod != null) {
								result.append("\t\t<lastmod>").append(lastMod).append("</lastmod>\n");
							}
							result.append("\t</sitemap>\n");
						}
					}
				}
			}

			result.append("</sitemapindex>\n");
			
			byte[] content = result.toString().getBytes("UTF-8");
			
			HttpHeaders headers = this.getContentTypeDispositionAndLengthHeaders(IMO_SOURCES_SITEMAP_MAPPING, "xml", content.length);
			
			return new ResponseEntity<byte[]>(content, headers, HttpStatus.OK);
			
		} catch (Throwable t) {
			return this.errorCode(HttpStatus.INTERNAL_SERVER_ERROR, t.getMessage());
		}
	}
	
	@RequestMapping(value="{lang:[a-z]{2}}_" + DYNAMIC_SITEMAP_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getDynamicSitemap(HttpServletRequest request, @PathVariable String lang) throws Throwable {
		return this.doGetDynamicSitemap(request, null, DEFAULT_MAX_ENTRIES, null, lang);
	}
	
	@RequestMapping(value="lastMod/{lastMod:[0-9]{4}-[0-9]{2}-[0-9]{2}}/{lang:[a-z]{2}}_" + DYNAMIC_SITEMAP_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getDynamicSitemap(HttpServletRequest request, @PathVariable String lastMod, @PathVariable String lang) throws Throwable {
		return this.doGetDynamicSitemap(request, null, DEFAULT_MAX_ENTRIES, lastMod, lang);
	}
	
	@RequestMapping(value="offset/{offset:[0-9]+}/max/{max:[1-9][0-9]*}/{lang:[a-z]{2}}_" + DYNAMIC_SITEMAP_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getDynamicSitemap(HttpServletRequest request, @PathVariable(value="offset") int offset, @PathVariable(value="max") int maxEntries, @PathVariable String lang) throws Throwable {
		return this.doGetDynamicSitemap(request, offset, maxEntries, null, lang);
	}
	
	@RequestMapping(value="lastMod/{lastMod:[0-9]{4}-[0-9]{2}-[0-9]{2}}/offset/{offset:[0-9]+}/max/{max:[1-9][0-9]*}/{lang:[a-z]{2}}_" + DYNAMIC_SITEMAP_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getDynamicSitemap(HttpServletRequest request, @PathVariable String lastMod, @PathVariable(value="offset") int offset, @PathVariable(value="max") int maxEntries, @PathVariable String lang) throws Throwable {
		return this.doGetDynamicSitemap(request, offset, maxEntries, lastMod, lang);
	}
		
	private ResponseEntity<byte[]> doGetDynamicSitemap(HttpServletRequest request, Integer offset, Integer maxEntries, String lastMod, String lang) throws Throwable {
		try {
			String baseURL = this.doGetBaseURL(request) + "/!/display/vessel/UID/";
			
			String[] sources = this.filterSources(this.getAvailableSources(request, ServletsHelper.DONT_INCLUDE_OTHER_SOURCES));
			
			if(sources.length > 0) {
				List<VesselUpdateMetadata> vesselsMeta = this._vesselsIndexingDAO.getVesselIdentifiers(sources, "UID", maxEntries, offset);
				
				if(!vesselsMeta.isEmpty()) {
					StringBuilder result = new StringBuilder();
					result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
					result.append("\t<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
					
					SimpleDateFormat dateFormatter = new SimpleDateFormat(LASTMOD_DATE_PATTERN);
					
					for(VesselUpdateMetadata meta : vesselsMeta) {
						result.append("\t\t<url>\n");
						result.append("\t\t\t<loc>").append(baseURL).append(this.padVesselUID(meta.getIdentifier())).append("/").append(lang).append("</loc>\n");
						result.append("\t\t\t<lastmod>").append(lastMod == null ? dateFormatter.format(meta.getMaximumDate()) : lastMod).append("</lastmod>\n");
						result.append("\t\t</url>\n");
					}
					
					result.append("\t</urlset>\n");
					
					byte[] content = result.toString().getBytes("UTF-8");
					
					HttpHeaders headers = this.getContentTypeDispositionAndLengthHeaders(SOURCES_SITEMAP_MAPPING, "xml", content.length);
					
					return new ResponseEntity<byte[]>(content, headers, HttpStatus.OK);
				}
			} 
				
			return this.errorCode(HttpStatus.NO_CONTENT, "Unable to retrieve sample list of vessel URLs for current user");
			
		} catch (Throwable t) {
			return this.errorCode(HttpStatus.INTERNAL_SERVER_ERROR, t.getMessage());
		}
	}
	
	@RequestMapping(value="{lang:[a-z]{2}}_" + IMO_DYNAMIC_SITEMAP_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getIMODynamicSitemap(HttpServletRequest request, @PathVariable String lang) throws Throwable {
		return this.doGetIMODynamicSitemap(request, null, DEFAULT_MAX_ENTRIES, null, lang);
	}
	
	@RequestMapping(value="lastMod/{lastMod:[0-9]{4}-[0-9]{2}-[0-9]{2}}/{lang:[a-z]{2}}_" + IMO_DYNAMIC_SITEMAP_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getIMODynamicSitemap(HttpServletRequest request, @PathVariable String lastMod, @PathVariable String lang) throws Throwable {
		return this.doGetIMODynamicSitemap(request, null, DEFAULT_MAX_ENTRIES, lastMod, lang);
	}
	
	@RequestMapping(value="offset/{offset:[0-9]+}/max/{max:[1-9][0-9]*}/{lang:[a-z]{2}}_" + IMO_DYNAMIC_SITEMAP_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getIMODynamicSitemap(HttpServletRequest request, @PathVariable(value="offset") int offset, @PathVariable(value="max") int maxEntries, @PathVariable String lang) throws Throwable {
		return this.doGetIMODynamicSitemap(request, offset, maxEntries, null, lang);
	}
	
	@RequestMapping(value="lastMod/{lastMod:[0-9]{4}-[0-9]{2}-[0-9]{2}}/offset/{offset:[0-9]+}/max/{max:[1-9][0-9]*}/{lang:[a-z]{2}}_" + IMO_DYNAMIC_SITEMAP_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getIMODynamicSitemap(HttpServletRequest request, @PathVariable String lastMod, @PathVariable(value="offset") int offset, @PathVariable(value="max") int maxEntries, @PathVariable String lang) throws Throwable {
		return this.doGetIMODynamicSitemap(request, offset, maxEntries, lastMod, lang);
	}
		
	private ResponseEntity<byte[]> doGetIMODynamicSitemap(HttpServletRequest request, Integer offset, Integer maxEntries, String lastMod, String lang) throws Throwable {
		try {
			String baseURL = this.doGetBaseURL(request) + "/!/display/vessel/IMO/";
			
			String[] sources = this.filterSources(this.getAvailableSources(request, ServletsHelper.DONT_INCLUDE_OTHER_SOURCES));
			
			if(sources.length > 0) {
				List<VesselUpdateMetadata> vesselsMeta = this._vesselsIndexingDAO.getIMOVesselIdentifiers(sources, "UID", maxEntries, offset);
				
				if(!vesselsMeta.isEmpty()) {
					StringBuilder result = new StringBuilder();
					result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
					result.append("\t<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
					
					SimpleDateFormat dateFormatter = new SimpleDateFormat(LASTMOD_DATE_PATTERN);
					
					for(VesselUpdateMetadata meta : vesselsMeta) {
						result.append("\t\t<url>\n");
						result.append("\t\t\t<loc>").append(baseURL).append(this.padIMO(meta.getIdentifier())).append("/").append(lang).append("</loc>\n");
						result.append("\t\t\t<lastmod>").append(lastMod == null ? dateFormatter.format(meta.getMaximumDate()): lastMod).append("</lastmod>\n");
						result.append("\t\t</url>\n");
					}
					
					result.append("\t</urlset>\n");
					
					byte[] content = result.toString().getBytes("UTF-8");
					
					HttpHeaders headers = this.getContentTypeDispositionAndLengthHeaders(IMO_SOURCES_SITEMAP_MAPPING, "xml", content.length);
					
					return new ResponseEntity<byte[]>(content, headers, HttpStatus.OK);
				}
			} 
				
			return this.errorCode(HttpStatus.NO_CONTENT, "Unable to retrieve sample list of vessel URLs for current user");
			
		} catch (Throwable t) {
			return this.errorCode(HttpStatus.INTERNAL_SERVER_ERROR, t.getMessage());
		}
	}
	
	@RequestMapping(value="{lang:[a-z]{2}}_" + SOURCES_SITEMAP_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getSourcesSitemap(HttpServletRequest request, @PathVariable String lang) throws Throwable {
		return this.doGetSourcesSitemap(request, null, lang);
	}
	
	@RequestMapping(value="lastMod/{lastMod:[0-9]{4}-[0-9]{2}-[0-9]{2}}/{lang:[a-z]{2}}_" + SOURCES_SITEMAP_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getSourcesSitemap(HttpServletRequest request, @PathVariable String lastMod, @PathVariable String lang) throws Throwable {
		return this.doGetSourcesSitemap(request, lastMod, lang);
	}
	
	private ResponseEntity<byte[]> doGetSourcesSitemap(HttpServletRequest request, String lastMod, String lang) throws Throwable {
		try {
			String baseURL = this.doGetBaseURL(request);
			
			String[] sources = this.filterSources(this.getAvailableSources(request, ServletsHelper.DONT_INCLUDE_OTHER_SOURCES));
			
			if(sources.length > 0) {
				StringBuilder result = new StringBuilder();
				result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
				result.append("\t<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
				
				SimpleDateFormat dateFormatter = new SimpleDateFormat(LASTMOD_DATE_PATTERN);
				
				for(SourceUpdateMetadata sourceMeta : this._vesselsIndexingDAO.getSourceUpdateMetadata(sources, "UID")) {
					result.append("\t\t<url>\n");
					result.append("\t\t\t<loc>").append(baseURL).append("/").
																 append(sources.length == 1 ? "" : sourceMeta.getSourceSystem()).
																 append(sources.length == 1 ? "" : "/").append("search/").append(lang).append("/#advanced").
																 append("</loc>\n");
					result.append("\t\t\t<lastmod>").append(lastMod == null ? dateFormatter.format(sourceMeta.getMaximumDate()) : lastMod).append("</lastmod>\n");
					result.append("\t\t</url>\n");
				}
				
				result.append("\t</urlset>\n");
				
				byte[] content = result.toString().getBytes("UTF-8");
				
				HttpHeaders headers = this.getContentTypeDispositionAndLengthHeaders(SOURCES_SITEMAP_MAPPING, "xml", content.length);
				
				return new ResponseEntity<byte[]>(content, headers, HttpStatus.OK);
			} else
				return this.errorCode(HttpStatus.NO_CONTENT, "Unable to retrieve valid list of sources for current user");
		} catch (Throwable t) {
			return this.errorCode(HttpStatus.INTERNAL_SERVER_ERROR, t.getMessage());
		}
	}
	
	@RequestMapping(value="{lang:[a-z]{2}}_" + IMO_SOURCES_SITEMAP_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getIMOSourcesSitemap(HttpServletRequest request, @PathVariable String lang) throws Throwable {
		return this.doGetIMOSourcesSitemap(request, null, lang);
	}
	
	@RequestMapping(value="lastMod/{lastMod:[0-9]{4}-[0-9]{2}-[0-9]{2}}/{lang:[a-z]{2}}_" + IMO_SOURCES_SITEMAP_MAPPING, method=RequestMethod.GET)
	public ResponseEntity<byte[]> getIMOSourcesSitemap(HttpServletRequest request, @PathVariable String lastMod, @PathVariable String lang) throws Throwable {
		return this.doGetIMOSourcesSitemap(request, lastMod, lang);
	}
	
	private ResponseEntity<byte[]> doGetIMOSourcesSitemap(HttpServletRequest request, String lastMod, String lang) throws Throwable {
		try {
			String baseURL = this.doGetBaseURL(request);
			
			String[] sources = this.filterSources(this.getAvailableSources(request, ServletsHelper.DONT_INCLUDE_OTHER_SOURCES));
			
			if(sources.length > 0) {
				StringBuilder result = new StringBuilder();
				result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
				result.append("\t<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");
				
				SimpleDateFormat dateFormatter = new SimpleDateFormat(LASTMOD_DATE_PATTERN);
				
				for(SourceUpdateMetadata sourceMeta : this._vesselsIndexingDAO.getIMOSourceUpdateMetadata(sources, "UID")) {
					result.append("\t\t<url>\n");
					result.append("\t\t\t<loc>").append(baseURL).append("/").append(sourceMeta.getSourceSystem()).append("/search/").append(lang).append("/").append("</loc>\n");
					result.append("\t\t\t<lastmod>").append(lastMod == null ? dateFormatter.format(sourceMeta.getMaximumDate()) : lastMod).append("</lastmod>\n");
					result.append("\t\t</url>\n");
				}
				
				result.append("\t</urlset>\n");
				
				byte[] content = result.toString().getBytes("UTF-8");
				
				HttpHeaders headers = this.getContentTypeDispositionAndLengthHeaders(IMO_SOURCES_SITEMAP_MAPPING, "xml", content.length);
				
				return new ResponseEntity<byte[]>(content, headers, HttpStatus.OK);
			} else
				return this.errorCode(HttpStatus.NO_CONTENT, "Unable to retrieve valid list of sources for current user");
			
		} catch (Throwable t) {
			return this.errorCode(HttpStatus.INTERNAL_SERVER_ERROR, t.getMessage());
		}
	}
	
	private String[] filterSources(String[] vesselSources) {
		List<String> filtered = new ListSet<String>();
		
		for(String source : vesselSources)
			if(!"CLAV".equalsIgnoreCase(source))
				filtered.add(source);
		
		return filtered.toArray(new String[filtered.size()]);
	}
}