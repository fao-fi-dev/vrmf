/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.users;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.business.dao.generated.UsersDAO;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.security.BasicUser;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.common.web.controllers.services.pub.CommonPublicServicesConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Feb 2012
 */
@NoArgsConstructor
@Singleton @Controller
@RequestMapping(CommonPublicServicesConstants.USER_SERVICES_PREFIX + "*")
public class LogoutController extends AbstractController {
	@Inject private @Getter @Setter UsersDAO usersDAO;
		
	@RequestMapping(value="logout", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<byte[]> logout(HttpSession session, HttpServletRequest servletRequest, @RequestParam(required=false) String to) throws Throwable {
		try {
			this._log.info("Requesting logout" + ( to == null ? "" : " to " + to ) + "...");

			BasicUser loggedUser = ServletsHelper.getLoggedUser(session);
			
			if(session == null) {
				String message = "No session is currently active";
				
				this._log.warn(message);
				
				return new ResponseEntity<byte[]>(message.getBytes("UTF-8"), HttpStatus.BAD_REQUEST);
			}
			
			if(loggedUser == null) {
				String message = "No user is currently logged in"; 
				
				this._log.warn(message);
				
				return new ResponseEntity<byte[]>(message.getBytes("UTF-8"), HttpStatus.NOT_FOUND);
			}
						
			session.invalidate();
	
			if(to != null) {
				this._log.info("Redirecting to: " + to);
				
				HttpHeaders headers = new HttpHeaders();
				headers.add("Location", to);
				
				return new ResponseEntity<byte[]>(headers, HttpStatus.TEMPORARY_REDIRECT);
			} else {
				String referer = StringsHelper.trim(servletRequest.getHeader("Referer"));
				
				if(referer != null) {
					this._log.info("'Referer' header set to " + referer + " for current logout request. Redirecting...");
					
					HttpHeaders headers = new HttpHeaders();
					headers.add("Location", referer);
					
					return new ResponseEntity<byte[]>(headers, HttpStatus.TEMPORARY_REDIRECT);
				} else {
					this._log.info("No 'Referer' header set: generating a custom response");
										
					String message = "You've been logged out from the system (you were logged in as '" + loggedUser.getId() + "')";
					
					return new ResponseEntity<byte[]>(message.getBytes("UTF-8"), HttpStatus.OK);
				}
			}
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
}