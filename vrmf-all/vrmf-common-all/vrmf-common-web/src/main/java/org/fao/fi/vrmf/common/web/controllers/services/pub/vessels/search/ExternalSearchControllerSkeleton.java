/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.search;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Set;

import javax.inject.Inject;

import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.sh.utility.services.impl.AbstractAsynchronousService;
import org.fao.fi.sh.utility.services.impl.invokers.AbstractAsynchronousServiceInvokerAndManager;
import org.fao.fi.sh.utility.services.spi.AsynchronousService;
import org.fao.fi.sh.utility.services.spi.AsynchronousServiceInfo;
import org.fao.fi.vrmf.business.dao.FullVesselsDAO;
import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.models.extended.FullVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToMmsi;
import org.fao.fi.vrmf.common.models.generated.VesselsToName;
import org.fao.fi.vrmf.common.web.DataCustomizer;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Sep 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Sep 2013
 */
@NoArgsConstructor
abstract public class ExternalSearchControllerSkeleton<M extends AbstractAsynchronousServiceInvokerAndManager<?, ?, ?>> extends AbstractController {
	protected M manager;
	
	@Inject protected @Getter @Setter DataCustomizer dataCustomizer;
	@Inject protected @Getter @Setter FullVesselsDAO fullVesselsDAO;

	abstract protected String getXMLNamespace();
	
	/**
	 * @return the 'manager' value
	 */
	protected M getManager() {
		return this.manager;
	}

	/**
	 * @param manager the 'manager' value to set
	 */
	//This is required, otherwise dependency injection for the specific manager won't happen!
	@Inject protected void setManager(M manager) {
		this.manager = manager;
	}
	
	final protected FullVessel getVesselDetails(String vesselsTable, String by, Integer vesselID, String[] dataSources) throws Throwable {
		boolean byID = "id".equalsIgnoreCase(by);
		
		return byID 
				? this.fullVesselsDAO.selectFullByPrimaryKeyAndDataSources(vesselID, dataSources) 
				: this.fullVesselsDAO.selectFullByUIDAndDataSources(vesselID, dataSources, vesselsTable);
	}
	
	final protected VesselsToIdentifiers extractIdentifier(FullVessel vessel, String identifierTypeID) {
		if(vessel == null || vessel.getIdentifiers() == null || vessel.getIdentifiers().isEmpty() || identifierTypeID == null)
			return null;
		
		for(VesselsToIdentifiers identifier : vessel.getIdentifiers()) {
			if(identifierTypeID.equals(identifier.getTypeId()))
				return identifier;
		}
		
		return null;
	}
	
	final protected String[] extractIdentifiers(FullVessel vessel, String identifierTypeID) {
		if(vessel == null || vessel.getIdentifiers() == null || vessel.getIdentifiers().isEmpty() || identifierTypeID == null)
			return new String[0];
		
		Set<String> identifiers = new ListSet<String>();
		
		for(VesselsToIdentifiers identifier : vessel.getIdentifiers()) {
			if(identifierTypeID.equals(identifier.getTypeId()))
				this.safeAdd(identifiers, identifier.getIdentifier());
		}
		
		return identifiers.toArray(new String[identifiers.size()]);
	}
	
	final protected String[] extractIMOs(FullVessel vessel) {
		return this.extractIdentifiers(vessel, "IMO");
	}
	
	final protected String[] extractEUCFRs(FullVessel vessel) {
		return this.extractIdentifiers(vessel, "EU_CFR");
	}
	
	final protected String[] extractNames(FullVessel vessel) {
		if(vessel == null || vessel.getNameData() == null || vessel.getNameData().isEmpty())
			return new String[0];
		
		Set<String> names = new ListSet<String>();
		
		for(VesselsToName name : vessel.getNameData()) {
			this.safeAdd(names, name.getName().toUpperCase());
			this.safeAdd(names, name.getSimplifiedName().toUpperCase());
		}
		
		return names.toArray(new String[names.size()]);
	}
	
	final protected String[] extractIRCSs(FullVessel vessel) {
		if(vessel == null || vessel.getCallsignData() == null || vessel.getCallsignData().isEmpty())
			return new String[0];
		
		Set<String> IRCSs = new ListSet<String>();
		
		for(VesselsToCallsigns IRCS : vessel.getCallsignData()) {
			this.safeAdd(IRCSs, IRCS.getCallsignId().replaceAll("[^a-zA-Z0-9]", ""));
		}
		
		return IRCSs.toArray(new String[IRCSs.size()]);
	}
	
	final protected String[] extractMMSIs(FullVessel vessel) {
		if(vessel == null || vessel.getMMSIData() == null || vessel.getMMSIData().isEmpty())
			return new String[0];
		
		Set<String> MMSIs = new ListSet<String>();
		
		for(VesselsToMmsi MMSI : vessel.getMMSIData()) {
			this.safeAdd(MMSIs, MMSI.getMmsi().replaceAll("[^0-9]", ""));
		}
		
		return MMSIs.toArray(new String[MMSIs.size()]);
	}
	
	final protected <S> void safeAdd(Collection<S> target, S data) {
		if(target != null && data != null)
			target.add(data);
	}
	
	@RequestMapping(value="providers.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getProvidersInfo(@PathVariable String format) {
		try {
			AsynchronousServiceInfo status = new AsynchronousServiceInfo();
			status.setManagerId(this.manager.getClass().getSimpleName());
			
			for(AsynchronousService<?, ?> provider : this.manager.getServiceProviders()) {
				if(provider.isAvailable())
					status.notifyServiceIsEnabled((AbstractAsynchronousService<?, ?>)provider);
				else
					status.notifyServiceIsDisabled((AbstractAsynchronousService<?, ?>)provider);
			}
			
			String content = null;
			
			if("json".equals(format)) {
				content = new JSONResponse<Serializable>(status, 0).JSONifyOnly(
					"*.managerId",
				    "*.enabledServices.serviceId",
				    "*.enabledServices.serviceTimeout",
				    "*.enabledServices.lastCheck",
				    "*.enabledServices.available",
				    "*.disabledServices.serviceId",
				    "*.disabledServices.serviceTimeout",
				    "*.disabledServices.lastCheck",
				    "*.disabledServices.available"
				);
			} else if("xml".equals(format)) {
				content = JAXBHelper.toXML(new Class[] {
					AsynchronousServiceInfo.class
				}, status);
			} 
			
			byte[] data = content == null ? new byte[0] : content.getBytes(Charset.forName("UTF-8"));
			
			HttpHeaders contentHeaders = this.getContentTypeDispositionAndLengthHeaders(status.getManagerId() + "_providers", format, data.length);
			HttpHeaders cacheHeaders = this.getCachingHeaders(60);
			
			return new ResponseEntity<byte[]>(data, this.mergeHeaders(contentHeaders, cacheHeaders), HttpStatus.OK);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="providers/refresh.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> refreshProviders(@PathVariable String format) {
		try {
			this.manager.updateAsynchronousServiceProvidersAvailability();
			
			return this.getProvidersInfo(format);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
}