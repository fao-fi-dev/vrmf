/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.exceptions.validation;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2012
 */
public class WrongParametersException extends ValidationException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 261525243372150541L;

	static final private String DEFAULT_MESSAGE = "One or more of the provided parameters is invalid";
	
	/**
	 * Class constructor
	 *
	 */
	public WrongParametersException() {
		super(DEFAULT_MESSAGE);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public WrongParametersException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public WrongParametersException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public WrongParametersException(Throwable cause) {
		super(DEFAULT_MESSAGE, cause);
	}	
}