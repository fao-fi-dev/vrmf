/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.zip.Deflater;
import java.util.zip.GZIPOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.fao.fi.sh.model.core.auth.UserModel;
import org.fao.fi.sh.model.core.spi.Exportable;
import org.fao.fi.sh.model.core.spi.services.ProcessTracker;
import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.TimestampHelper;
import org.fao.fi.sh.utility.model.extensions.collections.SerializableList;
import org.fao.fi.sh.utility.model.extensions.dates.SmartDate;
import org.fao.fi.sh.utility.services.tracking.NullProcessTracker;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.impl.CSVDatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.impl.HTMLDatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.impl.JSONDatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.impl.XLSDatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.impl.XMLDatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.security.BasicUser;
import org.fao.fi.vrmf.common.web.controllers.exceptions.ControllerException;
import org.fao.fi.vrmf.common.web.controllers.exceptions.SessionUnavailableException;
import org.fao.fi.vrmf.common.web.controllers.exceptions.authentication.NoLoggedUserException;
import org.fao.fi.vrmf.common.web.controllers.exceptions.authentication.UnauthorizedUserException;
import org.fao.fi.vrmf.common.web.controllers.exceptions.authentication.UserNotOwnerException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Apr 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Apr 2012
 */
@NoArgsConstructor
public class AbstractController extends AbstractLoggingAwareClient {
	static final protected String[] BASIC_JSON_EXCLUSION_PATTERNS = new String[] { "class", "*.class", "empty", "*.empty", "*.checksum", "converter", "*.converter" };
	static final protected String[] DEFAULT_JSON_EXCLUSION_PATTERNS = new String[] { "class", "*.class", "empty", "*.empty", "*.checksum", "converter", "*.converter", "*.updaterId", "*.updateDate", "*.comment" };

	static final protected int JSON = 0;
	static final protected int JSONP= 1;
	static final protected int XML  = 2;
	static final protected int HTML = 3;
	static final protected int TXT  = 4;
	static final protected int CSV  = 5;
	static final protected int XLS  = 6;
	static final protected int XLSX = 7;
	static final protected int PDF  = 8;
	static final protected int CLASS= 9;

	static final protected Boolean MERGE_HEADERS 	  = Boolean.TRUE;
	static final protected Boolean DONT_MERGE_HEADERS = Boolean.FALSE;

	static final protected Boolean INCLUDE_SERVER_AND_PROTOCOL_IN_BASE_URL = Boolean.TRUE;
	static final protected Boolean DONT_INCLUDE_SERVER_AND_PROTOCOL_IN_BASE_URL = Boolean.FALSE;

	static final protected Boolean INCLUDE_CONTEXT_IN_BASE_URL = Boolean.TRUE;
	static final protected Boolean DONT_INCLUDE_CONTEXT_IN_BASE_URL = Boolean.FALSE;

	static final protected String NO_REQUESTED_SOURCES = null;

	final protected Long CONTENT_MAX_AGE_IN_SECONDS = 60 * 60 * 2L;

	@Getter @Setter protected ProcessTracker processTracker = new NullProcessTracker();

	static final public Set<String> intersectSourceSystems(Set<String> firstSet, Set<String> secondSet) {
		if(firstSet == null || secondSet == null)
			return null;

		Set<String> intersection = new TreeSet<String>(firstSet);

		intersection.retainAll(secondSet);

		return intersection;
	}

	static final public String[] intersectSourceSystems(String[] firstSet, String[] secondSet) {
		if(firstSet == null || secondSet == null)
			return null;

		Set<String> intersection = AbstractController.intersectSourceSystems(new TreeSet<String>(Arrays.asList(firstSet)), new TreeSet<String>(Arrays.asList(secondSet)));

		if(intersection != null)
			return intersection.toArray(new String[intersection.size()]);

		return null;
	}

	final protected String[] augmentArray(String[] toAugment, String... additions) {
		if(toAugment == null)
			return additions;

		if(additions == null)
			return toAugment;

		String[] toReturn = Arrays.copyOf(toAugment, toAugment.length + additions.length);

		int startIndex = toAugment.length;

		for(String toAdd : additions) {
			toReturn[startIndex++] = toAdd;
		}

		return toReturn;
	}

	final protected String doGetBaseURL(HttpServletRequest request) throws Throwable {
		return this.doGetBaseURL(request, INCLUDE_SERVER_AND_PROTOCOL_IN_BASE_URL, INCLUDE_CONTEXT_IN_BASE_URL);
	}

	final protected String doGetBaseURL(HttpServletRequest request, boolean includeServerAndProtocol, boolean includeContext) throws Throwable {
		return ServletsHelper.getBaseURL(request, includeServerAndProtocol, includeContext);
	}

	final protected int getFormatType(String format) {
		if("json".equals(format))
			return JSON;
		else if("jsonp".equals(format))
			return JSONP;
		else if("xml".equals(format))
			return XML;
		else if("html".equals(format))
			return HTML;
		else if("txt".equals(format))
			return TXT;
		else if("csv".equals(format))
			return CSV;
		else if("xls".equals(format))
			return XLS;
		else if("xlsx".equals(format))
			return XLSX;
		else if("pdf".equals(format))
			return PDF;
		else if("class".equals(format))
			return CLASS;

		throw new UnsupportedOperationException("Unknown or unmanaged format '" + format + "'");
	}

	final protected boolean inline(String format) {
		switch(this.getFormatType(format)) {
			case XML:
			case CSV:
			case XLS:
			case XLSX:
			case PDF:
			case CLASS:
				return true;
			default:
				return false;
		}
	}

	final protected HttpHeaders getContentTypeDispositionAndLengthHeaders(String filename, String format, int length) {
		HttpHeaders headers = new HttpHeaders();

		MediaType type = null;

		switch(this.getFormatType(format)) {
			case JSON:
				type = new MediaType("application", "json", Charset.forName("UTF-8"));
				break;
			case JSONP:
				type = new MediaType("text", "javascript", Charset.forName("UTF-8"));
				break;
			case XML:
				type = new MediaType("text", "xml", Charset.forName("UTF-8"));
				break;
			case HTML:
				type = new MediaType("text", "html", Charset.forName("UTF-8"));
				break;
			case TXT:
				type = new MediaType("text", "plain", Charset.forName("UTF-8"));
				break;
			case CSV:
				type = new MediaType("text", "csv", Charset.forName("UTF-8"));
				break;
			case XLS:
				type = new MediaType("application", "xls");
				break;
			case XLSX:
				type = new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				break;
			case PDF:
				type = new MediaType("application", "pdf");
				break;
			case CLASS:
				type = new MediaType("application", "java-byte-code");
				break;
			default:
				throw new UnsupportedOperationException("Unknown or unmanaged format '" + format + "'");
		}

		headers.setContentType(type);

		if(this.inline(format) && filename != null) {
			headers.add("Content-Disposition", "inline; fileName=" + filename + "_" + TimestampHelper.buildTimestamp() + "." + format);
		}

		headers.setContentLength(length);

		return headers;
	}

	final protected HttpHeaders getCachingHeaders(long cachingPeriodInSeconds) {
		HttpHeaders headers = new HttpHeaders();
		headers.setCacheControl("public");
		headers.setExpires(System.currentTimeMillis() + cachingPeriodInSeconds * 1000);
		return headers;
	}

	final protected HttpHeaders mergeHeaders(HttpHeaders... headers) {
		return this.mergeHeaders(AbstractExplicitController.MERGE_HEADERS, headers);
	}

	final protected HttpHeaders mergeHeaders(boolean overwrite, HttpHeaders... headers) {
		HttpHeaders mergedHeaders = new HttpHeaders();

		if(headers != null)
			for(HttpHeaders current : headers)
				if(current != null)
					for(String key : current.keySet()) {
						if(overwrite || !mergedHeaders.containsKey(key))
							mergedHeaders.put(key, current.get(key));
					}

		return mergedHeaders;
	}

	final protected <DATA extends Exportable> byte[] emitContentForFormat(String format, SerializableList<DATA> content, Properties parameters) throws Throwable {
		switch(this.getFormatType(format)) {
			case JSON:
				return this.asJSON(content, parameters);
			case JSONP:
				return this.asJSONP(content, parameters);
			case CLASS:
				return this.asClass(content);
			default:
				throw new UnsupportedOperationException("No default content provider set for format '" + format + "'");
		}
	}

	final protected <DATA extends Exportable> byte[] emitContentForFormat(String format, SerializableList<DATA> content, DataExportModel<DATA> model, Properties parameters) throws Throwable {
		switch(this.getFormatType(format)) {
			case JSON:
				return this.asJSON(content, parameters);
			case JSONP:
				return this.asJSONP(content, parameters);
			case XML:
				return this.asXML(content, model, parameters);
			case XLS:
				return this.asXLS(content, model, parameters);
			case CSV:
				return this.asCSV(content, model, parameters);
			case HTML:
				return this.asHTML(content, model, parameters);
			case CLASS:
				return this.asClass(content);
			default:
				throw new UnsupportedOperationException("No default content provider set for format '" + format + "'");
		}
	}

	final protected <DATA extends Exportable> byte[] asJSONP(SerializableList<DATA> content, Properties parameters) throws Throwable {
		Properties updatedParameters = new Properties(parameters);

		if(!updatedParameters.containsKey("jsonpCallback")) {
			updatedParameters.put("jsonpCallback", "vrmf_jsonpCallback");
		}

		return this.asJSON(content, updatedParameters);
	}

	final protected <DATA extends Exportable> byte[] asJSON(SerializableList<DATA> content, Properties parameters) throws Throwable {
		if(parameters == null)
			parameters = new Properties();

		String[] exclusionPatterns = (String[])parameters.get(JSONDatasetEmitter.EXCLUSION_PATTERNS_PROPERTY);
		String[] inclusionPatterns = (String[])parameters.get(JSONDatasetEmitter.INCLUSION_PATTERNS_PROPERTY);

		String[] exclude = exclusionPatterns == null ? DEFAULT_JSON_EXCLUSION_PATTERNS : exclusionPatterns;
		exclude = this.augmentArray(exclude, "*.mapsTo",
											 "*.mappingWeight",
											 "*.mappingUser",
											 "*.mappingDate",
											 "*.mappingComment");

		String[] include = inclusionPatterns == null ? new String[0] : inclusionPatterns;

		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			parameters.put(JSONDatasetEmitter.EXCLUSION_PATTERNS_PROPERTY, exclude);
			parameters.put(JSONDatasetEmitter.INCLUSION_PATTERNS_PROPERTY, include);

			new JSONDatasetEmitter<DATA>().emit(out, this.processTracker, null, content, parameters);

			out.flush();

			return out.toByteArray();
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch (Throwable t) {
					this._log.warn("Unable to close output stream: " + t.getMessage());
				}
			}
		}
	}

	final protected <DATA extends Exportable> byte[] asXML(SerializableList<DATA> content, DataExportModel<DATA> model, Properties parameters) throws Throwable {
//		XStream serializer = new XStream();
//		return serializer.toXML(content).getBytes("UTF-8");
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			new XMLDatasetEmitter<DATA>().emit(out, this.processTracker, model, content, parameters);

			out.flush();

			return out.toByteArray();
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch (Throwable t) {
					this._log.warn("Unable to close output stream: " + t.getMessage());
				}
			}
		}
	}

	final protected <DATA extends Exportable> byte[] asXLS(SerializableList<DATA> content, DataExportModel<DATA> model, Properties parameters) throws Throwable {
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			new XLSDatasetEmitter<DATA>().emit(out, this.processTracker, model, content, parameters);

			out.flush();

			return out.toByteArray();
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch (Throwable t) {
					this._log.warn("Unable to close output stream: " + t.getMessage());
				}
			}
		}
	}

	final protected <DATA extends Exportable> byte[] asCSV(SerializableList<DATA> content, DataExportModel<DATA> model, Properties parameters) throws Throwable {
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			new CSVDatasetEmitter<DATA>().emit(out, this.processTracker, model, content, parameters);

			out.flush();

			return out.toByteArray();
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch (Throwable t) {
					this._log.warn("Unable to close output stream: " + t.getMessage());
				}
			}
		}
	}

	final protected <DATA extends Exportable> byte[] asHTML(SerializableList<DATA> content, DataExportModel<DATA> model, Properties parameters) throws Throwable {
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			new HTMLDatasetEmitter<DATA>().emit(out, this.processTracker, model, content, parameters);

			out.flush();

			return out.toByteArray();
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch (Throwable t) {
					this._log.warn("Unable to close output stream: " + t.getMessage());
				}
			}
		}
	}

	final protected <DATA extends Exportable> byte[] asClass(SerializableList<DATA> content) throws Throwable {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);

		oos.writeObject(content);

		byte[] bytes = baos.toByteArray();

		try {
			oos.flush();
			oos.close();
			baos.flush();
			baos.close();
		} catch (Throwable t) {
			;
		}

		return bytes;
	}

	final protected ResponseEntity<byte[]> manageError(Throwable cause, HttpStatus status, String additionalMessage) {
		this._log.error("Unexpected error", cause);

		HttpStatus effectiveStatus = HttpStatus.INTERNAL_SERVER_ERROR;

		if(ControllerException.class.isAssignableFrom(cause.getClass())) {
			effectiveStatus = ((ControllerException)cause).getCorrespondingHttpStatusCode();
		}

		return new ResponseEntity<byte[]>((cause.getMessage() == null ? ( additionalMessage == null ? cause.getClass().getSimpleName() : additionalMessage ) : cause.getMessage()).getBytes(), effectiveStatus);
	}

	final protected ResponseEntity<byte[]> manageError(Throwable cause, HttpStatus status) {
		return this.manageError(cause, status, (String)null);
	}

	final protected ResponseEntity<byte[]> manageError(Throwable cause, String additionalMessage) {
		return this.manageError(cause, HttpStatus.INTERNAL_SERVER_ERROR, additionalMessage);
	}

	final protected ResponseEntity<byte[]> manageError(ControllerException cause, String additionalMessage) {
		return this.manageError(cause, cause.getCorrespondingHttpStatusCode(), additionalMessage);
	}

	final protected ResponseEntity<byte[]> manageError(Throwable cause) {
		return this.manageError(cause, null, (String)null);
	}

	final protected void manageError(Throwable cause, HttpStatus status, String additionalMessage, HttpServletResponse response) {
		this._log.error("Unexpected error", cause);

		HttpStatus effectiveStatus = HttpStatus.INTERNAL_SERVER_ERROR;

		if(ControllerException.class.isAssignableFrom(cause.getClass())) {
			effectiveStatus = ((ControllerException)cause).getCorrespondingHttpStatusCode();
		}

		String message = cause.getMessage() == null ? ( additionalMessage == null ? cause.getClass().getSimpleName() : additionalMessage ) : cause.getMessage();

		try {
			response.sendError(effectiveStatus.value(), message);
		} catch (IOException IOe) {
			throw new RuntimeException("Unable to manage error by directly sending an error in the response", IOe);
		}
	}

	final protected void  manageError(Throwable cause, HttpStatus status, HttpServletResponse response) {
		this.manageError(cause, status, null, response);
	}

	final protected void  manageError(Throwable cause, String additionalMessage, HttpServletResponse response) {
		this.manageError(cause, HttpStatus.INTERNAL_SERVER_ERROR, additionalMessage, response);
	}

	final protected void  manageError(ControllerException cause, String additionalMessage, HttpServletResponse response) {
		this.manageError(cause, cause.getCorrespondingHttpStatusCode(), additionalMessage, response);
	}

	final protected void  manageError(Throwable cause, HttpServletResponse response) {
		this.manageError(cause, null, null, response);
	}

	/**
	 * @param request
	 * @return
	 */
	final protected String[] getAvailableSources(HttpServletRequest request) throws UserNotOwnerException {
		return this.doGetAvailableSources(ServletsHelper.getLoggedUser(request), request, ServletsHelper.INCLUDE_OTHER_SOURCES);
	}

	final protected String[] getAvailableSources(HttpServletRequest request, boolean includeOtherSources) throws UserNotOwnerException {
		return this.doGetAvailableSources(ServletsHelper.getLoggedUser(request), request, includeOtherSources);
	}

	final protected String[] getAvailableSources(BasicUser loggedUser, HttpServletRequest request) throws UserNotOwnerException {
		return this.doGetAvailableSources(loggedUser, request, ServletsHelper.INCLUDE_OTHER_SOURCES);
	}

	final protected String[] getAvailableSources(BasicUser loggedUser, HttpServletRequest request, boolean includeOtherSources) throws UserNotOwnerException {
		return this.doGetAvailableSources(loggedUser, request, includeOtherSources);
	}


	/**
	 * @param request
	 * @return
	 */
	final protected String[] doGetAvailableSources(BasicUser loggedUser, HttpServletRequest request, boolean includeOtherSources) throws UserNotOwnerException {
		return ServletsHelper.getAvailableSources(loggedUser, request, includeOtherSources);
	}

	/**
	 * @param request
	 * @return
	 */
	final protected String[] getAvailableOtherSources(HttpServletRequest request) throws UserNotOwnerException {
		return ServletsHelper.getAvailableOtherSources(ServletsHelper.getLoggedUser(request), request);
	}

	/**
	 * @param request
	 * @return
	 */
	final protected String[] getAvailableOtherSources(BasicUser loggedUser, HttpServletRequest request) throws UserNotOwnerException {
		return ServletsHelper.getAvailableOtherSources(loggedUser, request);
	}


	/**
	 * @param servletRequest
	 * @param toReturn
	 * @return
	 * @throws Throwable
	 *
	 * @Deprecated Either use a compression filter or - better - enable Tomcat compression
	 */
	final protected ResponseEntity<byte[]> gzipIfPossible(HttpServletRequest servletRequest, ResponseEntity<byte[]> toReturn) throws Throwable {
		String acceptEncoding = servletRequest.getHeader("Accept-Encoding");

		if(acceptEncoding == null ||
		   !new TreeSet<String>(Arrays.asList(acceptEncoding.split("\\,"))).contains("gzip"))
			return toReturn;

		byte[] original = toReturn.getBody();
		byte[] gzipped = original == null ? null : this.gzip(original);

		HttpHeaders additionalHeaders = new HttpHeaders();
		additionalHeaders.set("Content-Encoding", "gzip");
		additionalHeaders.setContentLength(gzipped == null ? 0 : gzipped.length);

		return new ResponseEntity<byte[]>(gzipped,
										  this.mergeHeaders(AbstractExplicitController.MERGE_HEADERS, toReturn.getHeaders(), additionalHeaders),
										  toReturn.getStatusCode());
	}

	/**
	 * @see http://www.dscripts.net/2010/06/04/compress-and-uncompress-a-java-byte-array-using-deflater-and-enflater/
	 * @param input
	 * @return
	 */
	@SuppressWarnings("unused")
	final private byte[] deflate(byte[] input) {
		Deflater df = new Deflater();
        df.setInput(input);
        df.finish();

        ByteArrayOutputStream baos = new ByteArrayOutputStream(input.length);

        byte[] buff = new byte[1024];

        while(!df.finished()) {
            baos.write(buff, 0, df.deflate(buff));     //write 4m 0 to count
        }

        try {
        	baos.close();
        } catch (Throwable t) {
        	t.printStackTrace();
        }

        return baos.toByteArray();
	}

	final private byte[] gzip(byte[] input) throws IOException {
		ByteArrayOutputStream baos = null;
        GZIPOutputStream zos = null;

        try {
        	baos = new ByteArrayOutputStream();
        	zos = new GZIPOutputStream(baos);

            zos.write(input);

            zos.finish();
            zos.close();

            return baos.toByteArray();
        } finally {
        	if(baos != null) {
        		try {
        			baos.flush();
        			baos.close();
        		} catch(Throwable t) {
        			t.printStackTrace();
        		}
        	}
        }
	}

	final protected UserModel requireLoggedUser(HttpServletRequest servletRequest) throws SessionUnavailableException, NoLoggedUserException {
		return this.requireLoggedUser(servletRequest.getSession(false));
	}

	final protected UserModel requireLoggedUser(HttpSession session) throws SessionUnavailableException, NoLoggedUserException {
		if(session == null)
			throw new SessionUnavailableException();

		UserModel user = ServletsHelper.getLoggedUser(session);

		if(user == null || user.getId() == null)
			throw new NoLoggedUserException();

		return user;
	}

	final protected void checkLoggedUser(UserModel user, String[] roles, String[] capabilities) throws NoLoggedUserException, UnauthorizedUserException {
		boolean checked = true;

		if(roles != null && roles.length > 0)
			checked &= user.is(roles);

		if(capabilities != null && capabilities.length > 0)
			checked &= user.can(capabilities);

		if(!checked) {
			throw new UnauthorizedUserException("Logged user is not authorized to perform the requested operation. " +
												"Required roles: " + ( roles == null ? null : CollectionsHelper.join(roles, ",") ) + " - " +
												"Required capabilities: " + ( capabilities == null ? null : CollectionsHelper.join(capabilities, ",") ) );
		}
	}

	final protected void checkLoggedUserRoles(UserModel user, String[] roles) throws NoLoggedUserException, UnauthorizedUserException {
		this.checkLoggedUser(user, roles, null);
	}

	final protected void checkLoggedUserCapabilities(UserModel user, String[] capabilities) throws NoLoggedUserException, UnauthorizedUserException {
		this.checkLoggedUser(user, null, capabilities);
	}

	final protected ResponseEntity<byte[]> notFound() {
		return this.notFound(null);
	}

	final protected ResponseEntity<byte[]> notFound(String message) {
		return this.errorCode(HttpStatus.NOT_FOUND, message);
	}

	final protected ResponseEntity<byte[]> badRequest() {
		return this.badRequest(null);
	}

	final protected ResponseEntity<byte[]> badRequest(String message) {
		return this.errorCode(HttpStatus.BAD_REQUEST, message);
	}

	final protected ResponseEntity<byte[]> errorCode(HttpStatus code, String message) {
		this._log.error("Sending an HTTP error " + code.value() + " - " + code.getReasonPhrase() + ( message == null ? "" : ". Reason: " + message ));

		byte[] asBytes = null;

		if(message != null) {
			try {
				asBytes = message.getBytes("UTF-8");
			} catch(UnsupportedEncodingException UEe) {
				;
			}
		}

		return asBytes == null ? new ResponseEntity<byte[]>(code) : new ResponseEntity<byte[]>(asBytes, code);
	}

	protected void sendResponseError(HttpServletResponse response, HttpStatus code, String message) {
		this.sendResponseError(response, code.value(), message);
	}

	protected void sendResponseError(HttpServletResponse response, int code, String message) {
		try {
			this._log.error("Sending error code " + code + ( message == null ? "" : ". Message: " + message ));

			if(message != null) {
				response.sendError(code, message);
			} else
				response.sendError(code);
		} catch (IOException IOe) {
			this._log.error("Unable to send error code " + code + " (message: " + message + ")", IOe);
		}
	}

	protected void sendResponseError(HttpServletResponse response, HttpStatus code) {
		this.sendResponseError(response, code.value());
	}

	protected void sendResponseError(HttpServletResponse response, int code) {
		this.sendResponseError(response, code, (String)null);
	}

	protected void sendResponseError(HttpServletResponse response, HttpStatus code, Throwable t) {
		this.sendResponseError(response, code.value(), t);
	}

	protected void sendResponseError(HttpServletResponse response, int code, Throwable t) {
		if(t != null)
			this._log.error("Attempting to send error code " + code + " as a consequence of a " + t.getClass().getSimpleName() + " [ message: " + t.getMessage() + " ]");

		this.sendResponseError(response, code, t == null ? null : t.getClass().getSimpleName() + ": " + t.getMessage());
	}

	protected String[] getSources(BasicUser loggedUser, HttpServletRequest request, String sources, boolean includePublicSources) throws UserNotOwnerException {
		String[] availableSources = this.doGetAvailableSources(loggedUser, request, includePublicSources);
		String[] requestedSources = sources == null ? availableSources : sources.split("\\,");

		return AbstractController.intersectSourceSystems(availableSources, requestedSources);
	}

	protected Date truncate(Date toTruncate) {
		if(toTruncate == null)
			return null;

		return new SmartDate(toTruncate).roundToDay().asDate();
	}
}