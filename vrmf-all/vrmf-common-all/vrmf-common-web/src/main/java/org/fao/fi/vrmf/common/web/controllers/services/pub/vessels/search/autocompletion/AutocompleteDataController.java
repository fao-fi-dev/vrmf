/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.search.autocompletion;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.sh.utility.model.extensions.collections.SerializableList;
import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableArrayList;
import org.fao.fi.vrmf.business.dao.GearTypeFuzzyAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.RegistrationPortAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselExternalMarkingsAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselFishingLicenseAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselIMOAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselIRCSAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselIdentifierAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselMMSIAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselNameFuzzyAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselOperatorFuzzyAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselOwnerFuzzyAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselRegNoAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselTUVIAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselTypeFuzzyAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselUIDAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesDAO;
import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SCountriesExample;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.common.web.controllers.services.pub.CommonPublicServicesConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 May 2012
 */
@Singleton @Controller
@RequestMapping(CommonPublicServicesConstants.VESSELS_AUTOCOMPLETE_PREFIX + "*")
public class AutocompleteDataController extends AbstractController {
	protected String DEFAULT_PROCEDURE  = null;
	protected String NO_SPECIFIC_SOURCE = null;
	protected Date NO_DATE_SET = null;
	protected Integer DEFAULT_MAX_ENTRIES = null;
	
	protected String NO_REGISTRATION_COUNTRY = null;
	protected String NO_REGISTRATION_PORT = null;
	
	protected String GROUP_BY_UID = "UID";
	
	protected boolean GROUP_MAPPED_TYPES 	 = true;
	protected boolean DONT_GROUP_MAPPED_TYPES = false;
	
	@Inject private @Getter @Setter SCountriesDAO countriesDAO;
	@Inject private @Getter @Setter VesselIdentifierAutocompletionDAO identifierAutocompletionDAO;
	@Inject private @Getter @Setter VesselUIDAutocompletionDAO uidAutocompletionDAO;
	@Inject private @Getter @Setter VesselTUVIAutocompletionDAO tuviAutocompletionDAO;
	@Inject private @Getter @Setter VesselIMOAutocompletionDAO imoAutocompletionDAO;
	@Inject private @Getter @Setter VesselTypeFuzzyAutocompletionDAO typeAutocompletionDAO;
	@Inject private @Getter @Setter GearTypeFuzzyAutocompletionDAO gearAutocompletionDAO;
	@Inject private @Getter @Setter VesselNameFuzzyAutocompletionDAO nameAutocompletionDAO;
	@Inject private @Getter @Setter VesselExternalMarkingsAutocompletionDAO externalMarkingsAutocompletionDAO;
	@Inject private @Getter @Setter VesselIRCSAutocompletionDAO IRCSAutocompletionDAO;
	@Inject private @Getter @Setter VesselMMSIAutocompletionDAO MMSIAutocompletionDAO;
	@Inject private @Getter @Setter VesselRegNoAutocompletionDAO regNoAutocompletionDAO;
	@Inject private @Getter @Setter VesselFishingLicenseAutocompletionDAO fishingLicenseAutocompletionDAO;
	@Inject private @Getter @Setter VesselOwnerFuzzyAutocompletionDAO ownerAutocompletionDAO;
	@Inject private @Getter @Setter VesselOperatorFuzzyAutocompletionDAO operatorAutocompletionDAO;
	
	@Inject private @Getter @Setter RegistrationPortAutocompletionDAO regPortAutocompletionDAO;
	
	
	
	/**
	 * Class constructor
	 *
	 */
	public AutocompleteDataController() {
		super();
	}
	
	protected Date atDate(String date) {
		if(date == null)
			return NO_DATE_SET;
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				
		try {
			return formatter.parse(date);
		} catch (ParseException Pe) {
			this._log.warn("Unparseable date provided for 'at' autocompletion parameter: " + date);
			
			return NO_DATE_SET;
		}
	}
	
	protected Date getDate(String historical) {
		if(historical == null)
			return NO_DATE_SET;

		return "historical".equalsIgnoreCase(historical) ? NO_DATE_SET : new Date();
	}

	protected ResponseEntity<byte[]> doAutocompleteVesselType(HttpServletRequest request, String procedureName, String sources, Date atDate, String groupBy, Integer maxEntries, String vesselTypeName, boolean groupMappedTypes, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, NO_DATE_SET, maxEntries, "UID");
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.typeAutocompletionDAO.searchVesselTypes(procedureName, vesselTypeName, filter.getAtDate(), filter.getLimitTo(), filter.getAvailableSources(), filter.getSources(), this.getAvailableOtherSources(request), filter.getGroupByUID(), groupMappedTypes), format, end - start), "VesselType_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	protected ResponseEntity<byte[]> doAutocompleteGearType(HttpServletRequest request, String procedureName, String sources, Date atDate, String groupBy, Integer maxEntries, String gearTypeName, boolean groupMappedTypes, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, NO_DATE_SET, maxEntries, "UID");
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.gearAutocompletionDAO.searchGearTypes(procedureName, gearTypeName, filter.getAtDate(), filter.getLimitTo(), filter.getAvailableSources(), filter.getSources(), this.getAvailableOtherSources(request), filter.getGroupByUID(), groupMappedTypes), format, end - start), "GearType_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}

	
	@RequestMapping(value="sources/{sources}/{historical}/by/{groupBy:id|uid}/max/{maxEntries}/ircs/{ircs}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIRCSHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String ircs, @PathVariable String format) {
		return this.doAutocompleteIRCS(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), groupBy, maxEntries, ircs, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/ircs/{ircs}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIRCSAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String ircs, @PathVariable String format) {
		return this.doAutocompleteIRCS(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), groupBy, maxEntries, ircs, format);
	}
	
	@RequestMapping(value="{historical}/by/{groupBy:id|uid}/ircs/{ircs}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIRCSHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String groupBy, @PathVariable String ircs, @PathVariable String format) {
		return this.doAutocompleteIRCS(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), groupBy, DEFAULT_MAX_ENTRIES, ircs, format);
	}
	
	@RequestMapping(value="at/{atDate}/by/{groupBy:id|uid}/ircs/{ircs}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIRCSAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable String ircs, @PathVariable String format) {
		return this.doAutocompleteIRCS(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), groupBy, DEFAULT_MAX_ENTRIES, ircs, format);
	}
	
	protected ResponseEntity<byte[]> doAutocompleteIRCS(HttpServletRequest request, String procedureName, String sources, Date atDate, String groupBy, Integer maxEntries, String IRCS, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, atDate, maxEntries, groupBy);
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.IRCSAutocompletionDAO.searchVesselIRCS(procedureName, IRCS, filter.getAtDate(), filter.getLimitTo(), filter.getSources() == null ? filter.getAvailableSources() : filter.getSources(), filter.getGroupByUID()), format, end - start), "VesselIRCS_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/by/{groupBy:id|uid}/max/{maxEntries}/mmsi/{mmsi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteMMSIHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String mmsi, @PathVariable String format) {
		return this.doAutocompleteMMSI(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), groupBy, maxEntries, mmsi, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/mmsi/{mmsi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteMMSIAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String mmsi, @PathVariable String format) {
		return this.doAutocompleteMMSI(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), groupBy, maxEntries, mmsi, format);
	}
	
	@RequestMapping(value="{historical}/by/{groupBy:id|uid}/mmsi/{mmsi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteMMSIHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String groupBy, @PathVariable String mmsi, @PathVariable String format) {
		return this.doAutocompleteMMSI(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), groupBy, DEFAULT_MAX_ENTRIES, mmsi, format);
	}
	
	@RequestMapping(value="at/{atDate}/by/{groupBy:id|uid}/mmsi/{mmsi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteMMSIAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable String mmsi, @PathVariable String format) {
		return this.doAutocompleteMMSI(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), groupBy, DEFAULT_MAX_ENTRIES, mmsi, format);
	}

	
	protected ResponseEntity<byte[]> doAutocompleteMMSI(HttpServletRequest request, String procedureName, String sources, Date atDate, String groupBy, Integer maxEntries, String MMSI, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, atDate, maxEntries, groupBy);
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.MMSIAutocompletionDAO.searchVesselMMSI(procedureName, MMSI, filter.getAtDate(), filter.getLimitTo(), filter.getSources() == null ? filter.getAvailableSources() : filter.getSources(), filter.getGroupByUID()), format, end - start), "VesselIRCS_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/by/{groupBy:id|uid}/max/{maxEntries}/name/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteNameHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteName(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), groupBy, maxEntries, name, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/name/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteNameAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteName(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), groupBy, maxEntries, name, format);
	}
	
	@RequestMapping(value="{historical}/by/{groupBy:id|uid}/name/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteNameHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String groupBy, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteName(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), groupBy, DEFAULT_MAX_ENTRIES, name, format);
	}
	
	@RequestMapping(value="at/{atDate}/by/{groupBy:id|uid}/name/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteNameAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteName(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), groupBy, DEFAULT_MAX_ENTRIES, name, format);
	}
	
	protected ResponseEntity<byte[]> doAutocompleteName(HttpServletRequest request, String procedureName, String sources, Date atDate, String groupBy, Integer maxEntries, String name, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, atDate, maxEntries, groupBy);
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.nameAutocompletionDAO.searchVesselNames(procedureName, name, filter.getAtDate(), filter.getLimitTo(), filter.getSources() == null ? filter.getAvailableSources() : filter.getSources(), filter.getGroupByUID()), format, end - start), "VesselNames_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/by/{groupBy:id|uid}/max/{maxEntries}/externalMarkings/{externalMarkings}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteExternalMarkingsHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String externalMarkings, @PathVariable String format) {
		return this.doAutocompleteExternalMarkings(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), groupBy, maxEntries, externalMarkings, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/externalMarkings/{externalMarkings}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteExternalMarkingsAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String externalMarkings, @PathVariable String format) {
		return this.doAutocompleteExternalMarkings(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), groupBy, maxEntries, externalMarkings, format);
	}
	
	@RequestMapping(value="{historical}/by/{groupBy:id|uid}/externalMarkings/{externalMarkings}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteExternalMarkingsHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String groupBy, @PathVariable String externalMarkings, @PathVariable String format) {
		return this.doAutocompleteExternalMarkings(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), groupBy, DEFAULT_MAX_ENTRIES, externalMarkings, format);
	}
	
	@RequestMapping(value="at/{atDate}/by/{groupBy:id|uid}/externalMarkings/{externalMarkings}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteExternalMarkingsAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable String externalMarkings, @PathVariable String format) {
		return this.doAutocompleteExternalMarkings(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), groupBy, DEFAULT_MAX_ENTRIES, externalMarkings, format);
	}
	
	protected ResponseEntity<byte[]> doAutocompleteExternalMarkings(HttpServletRequest request, String procedureName, String sources, Date atDate, String groupBy, Integer maxEntries, String externalMarkings, String format) {
		try {
			long end, start = System.currentTimeMillis();
						
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, atDate, maxEntries, groupBy);
			
			end = System.currentTimeMillis();
						
			return this.getResponse(this.getRawData(this.externalMarkingsAutocompletionDAO.searchVesselExternalMarkings(procedureName, externalMarkings, filter.getAtDate(), filter.getLimitTo(), filter.getSources() == null ? filter.getAvailableSources() : filter.getSources(), filter.getGroupByUID()), format, end - start), "VesselNames_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/by/{groupBy:id|uid}/max/{maxEntries}/registration/countries/{regCountries}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), groupBy, maxEntries, regCountries, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/registration/countries/{regCountries}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), groupBy, maxEntries, regCountries, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="{historical}/by/{groupBy:id|uid}/max/{maxEntries}/registration/countries/{regCountries}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), groupBy, maxEntries, regCountries, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/registration/countries/{regCountries}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), groupBy, maxEntries, regCountries, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/by/{groupBy:id|uid}/max/{maxEntries}/registration/port/{regPort}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoByPortHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regPort, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), groupBy, maxEntries, NO_REGISTRATION_COUNTRY, regPort, regNo, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/registration/port/{regPort}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoByPortAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regPort, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), groupBy, maxEntries, NO_REGISTRATION_COUNTRY, regPort, regNo, format);
	}
	
	@RequestMapping(value="{historical}/by/{groupBy:id|uid}/max/{maxEntries}/registration/port/{regPort}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoByPortHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regPort, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), groupBy, maxEntries, NO_REGISTRATION_COUNTRY, regPort, regNo, format);
	}
	
	@RequestMapping(value="at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/registration/port/{regPort}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoByPortAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regPort, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), groupBy, maxEntries, NO_REGISTRATION_COUNTRY, regPort, regNo, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/by/{groupBy:id|uid}/max/{maxEntries}/registration/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), groupBy, maxEntries, NO_REGISTRATION_COUNTRY, null /* No Reg. Port */, regNo, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/registration/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), groupBy, maxEntries, NO_REGISTRATION_COUNTRY, null /* No Reg. Port */, regNo, format);
	}
	
	@RequestMapping(value="{historical}/by/{groupBy:id|uid}/registration/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String groupBy, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), groupBy, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, null /* No Reg. Port */, regNo, format);
	}
	
	@RequestMapping(value="at/{atDate}/by/{groupBy:id|uid}/registration/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), groupBy, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, null /* No Reg. Port */, regNo, format);
	}
	
	protected ResponseEntity<byte[]> doAutocompleteRegistrationNumber(HttpServletRequest request, String procedureName, String sources, Date atDate, String groupBy, Integer maxEntries, String regCountries, String regPort, String regNo, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			List<Integer> registrationCountryIDs = new ListSet<Integer>();
			List<Integer> registrationPortIDs = new ListSet<Integer>();
			
			if(regCountries != null) {
				String[] parts = regCountries.split("\\,");
				
				SCountriesExample filter = new SCountriesExample();
				filter.createCriteria().andIso2CodeIn(Arrays.asList(parts));
				
				for(SCountries country : this.countriesDAO.selectByExample(filter)) {
					registrationCountryIDs.add(country.getId());
				}
			}
			
			if(regPort != null) {
				for(String part : regPort.split("\\,"))
					registrationPortIDs.add(Integer.valueOf(part));				
			}
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, atDate, maxEntries, groupBy);
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.regNoAutocompletionDAO.searchVesselRegNo(procedureName, 
																								   registrationCountryIDs == null || registrationCountryIDs.isEmpty() ? null : registrationCountryIDs.toArray(new Integer[registrationCountryIDs.size()]), 
																								   registrationPortIDs == null || registrationPortIDs.isEmpty() ? null : registrationPortIDs.toArray(new Integer[registrationPortIDs.size()]), 
																								   regNo, 
																								   filter.getAtDate(), 
																								   filter.getLimitTo(), 
																								   filter.getSources() == null ? filter.getAvailableSources() : filter.getSources(), filter.getGroupByUID()), 
																								   format, 
																								   end - start), "VesselRegNos_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/by/{groupBy:id|uid}/max/{maxEntries}/registration/countries/{regCountries}/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), groupBy, maxEntries, regCountries, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/registration/countries/{regCountries}/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), groupBy, maxEntries, regCountries, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="{historical}/by/{groupBy:id|uid}/max/{maxEntries}/registration/countries/{regCountries}/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), groupBy, maxEntries, regCountries, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/registration/countries/{regCountries}/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), groupBy, maxEntries, regCountries, port, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/{historical}/by/{groupBy:id|uid}/max/{maxEntries}/registration/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), groupBy, maxEntries, NO_REGISTRATION_COUNTRY, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/registration/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), groupBy, maxEntries, NO_REGISTRATION_COUNTRY, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="{historical}/by/{groupBy:id|uid}/registration/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String groupBy, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), groupBy, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="at/{atDate}/by/{groupBy:id|uid}/registration/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), groupBy, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	protected ResponseEntity<byte[]> doAutocompleteRegistrationPort(HttpServletRequest request, String procedureName, String sources, Date atDate, String groupBy, Integer maxEntries, String countryIDs, String portName, boolean groupMappedTypes, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			List<Integer> registrationCountryIDs = new ListSet<Integer>();
			
			if(countryIDs != null) {
				String[] parts = countryIDs.split("\\,");
				
				SCountriesExample filter = new SCountriesExample();
				filter.createCriteria().andIso2CodeIn(Arrays.asList(parts));
				
				for(SCountries country : this.countriesDAO.selectByExample(filter)) {
					registrationCountryIDs.add(country.getId());
				}
			}
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, atDate, maxEntries, groupBy); 
			
			end = System.currentTimeMillis();

			return this.getResponse(this.getRawData(this.regPortAutocompletionDAO.searchRegistrationPorts(procedureName, registrationCountryIDs == null || registrationCountryIDs.isEmpty() ? null : registrationCountryIDs.toArray(new Integer[registrationCountryIDs.size()]), portName, filter.getAtDate(), filter.getLimitTo(), filter.getAvailableSources(), filter.getSources(), this.getAvailableOtherSources(request), filter.getGroupByUID(), groupMappedTypes), format, end - start), "VesselRegPort_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	protected ResponseEntity<byte[]> doAutocompleteFishingLicense(HttpServletRequest request, String procedureName, String sources, Date atDate, String groupBy, Integer maxEntries, String issuingCountries, String licenseNo, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			List<Integer> issuingCountryIDs = new ListSet<Integer>();
			
			if(issuingCountries != null) {
				String[] parts = issuingCountries.split("\\,");
				
				SCountriesExample filter = new SCountriesExample();
				filter.createCriteria().andIso2CodeIn(Arrays.asList(parts));
				
				for(SCountries country : this.countriesDAO.selectByExample(filter)) {
					issuingCountryIDs.add(country.getId());
				}
			}
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, atDate, maxEntries, groupBy);
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.fishingLicenseAutocompletionDAO.searchVesselFishingLicense(procedureName, 
																													issuingCountryIDs == null || issuingCountryIDs.isEmpty() ? null : issuingCountryIDs.toArray(new Integer[issuingCountryIDs.size()]), 
																													licenseNo, 
																													filter.getAtDate(), 
																													filter.getLimitTo(), 
																													filter.getSources() == null ? filter.getAvailableSources() : filter.getSources(), filter.getGroupByUID()), 
																													format, 
																													end - start), "VesselFishingLicense_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="sources/{sources}/by/{groupBy:id|uid}/max/{maxEntries}/imo/{imo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIMO(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String imo, @PathVariable String format) {
		return this.doAutocompleteIMO(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), groupBy, maxEntries, imo, format);
	}
	
	@RequestMapping(value="by/{groupBy:id|uid}/imo/{imo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIMO(HttpServletRequest request, @PathVariable String historical, @PathVariable String groupBy, @PathVariable String imo, @PathVariable String format) {
		return this.doAutocompleteIMO(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), groupBy, DEFAULT_MAX_ENTRIES, imo, format);
	}
	
	protected ResponseEntity<byte[]> doAutocompleteIMO(HttpServletRequest request, String procedureName, String sources, Date atDate, String groupBy, Integer maxEntries, String IMO, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, atDate, maxEntries, groupBy);
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.imoAutocompletionDAO.searchVesselIMO(procedureName, IMO, filter.getLimitTo(), filter.getSources() == null ? filter.getAvailableSources() : filter.getSources(), filter.getGroupByUID()), format, end - start), "VesselIMO_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="sources/{sources}/by/{groupBy:id|uid}/max/{maxEntries}/uid/{uid}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteUID(HttpServletRequest request, @PathVariable String sources, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String uid, @PathVariable String format) {
		return this.doAutocompleteUID(request, DEFAULT_PROCEDURE, sources, groupBy, maxEntries, uid, format);
	}
	
	@RequestMapping(value="by/{groupBy:id|uid}/max/{maxEntries}/uid/{uid}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteUID(HttpServletRequest request, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String uid, @PathVariable String format) {
		return this.doAutocompleteUID(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, groupBy, maxEntries, uid, format);
	}
	
	@RequestMapping(value="uid/{uid}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteUID(HttpServletRequest request, @PathVariable String uid, @PathVariable String format) {
		return this.doAutocompleteUID(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, "UID", DEFAULT_MAX_ENTRIES, uid, format);
	}
	
	protected ResponseEntity<byte[]> doAutocompleteUID(HttpServletRequest request, String procedureName, String sources, String groupBy, Integer maxEntries, String UID, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, NO_DATE_SET, maxEntries, groupBy);
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.uidAutocompletionDAO.searchVesselUID(procedureName, UID, filter.getSources() == null ? filter.getAvailableSources() : filter.getSources(), filter.getLimitTo()), format, end - start), "VesselUID_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="sources/{sources}/by/{groupBy:id|uid}/max/{maxEntries}/tuvi/{tuvi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteTUVI(HttpServletRequest request, @PathVariable String sources, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String tuvi, @PathVariable String format) {
		return this.doAutocompleteTUVI(request, DEFAULT_PROCEDURE, sources, groupBy, maxEntries, tuvi, format);
	}
	
	@RequestMapping(value="by/{groupBy:id|uid}/max/{maxEntries}/tuvi/{tuvi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteTUVI(HttpServletRequest request, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String tuvi, @PathVariable String format) {
		return this.doAutocompleteTUVI(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, groupBy, maxEntries, tuvi, format);
	}
	
	@RequestMapping(value="tuvi/{tuvi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteTUVI(HttpServletRequest request, @PathVariable String tuvi, @PathVariable String format) {
		return this.doAutocompleteTUVI(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, "UID", DEFAULT_MAX_ENTRIES, tuvi, format);
	}
	
	protected ResponseEntity<byte[]> doAutocompleteTUVI(HttpServletRequest request, String procedureName, String sources, String groupBy, Integer maxEntries, String TUVI, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, NO_DATE_SET, maxEntries, groupBy);
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.tuviAutocompletionDAO.searchVesselTUVI(procedureName, TUVI, filter.getSources() == null ? filter.getAvailableSources() : filter.getSources(), filter.getLimitTo()), format, end - start), "VesselTUVI_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="sources/{sources}/identifier/{identifierType}/by/{groupBy:id|uid}/max/{maxEntries}/{identifier}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIdentifier(HttpServletRequest request, @PathVariable String sources, @PathVariable String identifierType, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String identifier, @PathVariable String format) {
		return this.doAutocompleteIdentifier(request, DEFAULT_PROCEDURE, sources, identifierType, groupBy, maxEntries, identifier, format);
	}
	
	@RequestMapping(value="identifier/{identifierType}/by/{groupBy:id|uid}/{identifier}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIdentifier(HttpServletRequest request, @PathVariable String identifierType, @PathVariable String groupBy, @PathVariable String identifier, @PathVariable String format) {
		return this.doAutocompleteIdentifier(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, identifierType, groupBy, DEFAULT_MAX_ENTRIES, identifier, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/by/{groupBy:id|uid}/max/{maxEntries}/owner/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteOwnerHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteOwner(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), groupBy, maxEntries, name, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/ow/operator/me}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteOwnerAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteOwner(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), groupBy, maxEntries, name, format);
	}
	
	protected ResponseEntity<byte[]> doAutocompleteIdentifier(HttpServletRequest request, String procedureName, String sources, String identifierType, String groupBy, Integer maxEntries, String identifier, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, NO_DATE_SET, maxEntries, groupBy);
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.identifierAutocompletionDAO.searchVesselIdentifier(procedureName, identifierType, identifier, filter.getLimitTo(), filter.getSources() == null ? filter.getAvailableSources() : filter.getSources(), filter.getGroupByUID()), format, end - start), "VesselUID_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="{historical}/by/{groupBy:id|uid}/owner/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteOwnerHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String groupBy, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteOwner(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), groupBy, DEFAULT_MAX_ENTRIES, name, format);
	}
	
	@RequestMapping(value="at/{atDate}/by/{groupBy:id|uid}/owner/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteOwnerAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteOwner(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), groupBy, DEFAULT_MAX_ENTRIES, name, format);
	}
	
	protected ResponseEntity<byte[]> doAutocompleteOwner(HttpServletRequest request, String procedureName, String sources, Date atDate, String groupBy, Integer maxEntries, String ownerName, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, atDate, maxEntries, groupBy);
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.ownerAutocompletionDAO.searchOwnerNames(procedureName, ownerName, filter.getAtDate(), filter.getLimitTo(), filter.getSources() == null ? filter.getAvailableSources() : filter.getSources(), filter.getGroupByUID()), format, end - start), "VesselOwners_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/by/{groupBy:id|uid}/max/{maxEntries}/operator/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteOperatorHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteOperator(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), groupBy, maxEntries, name, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate}/by/{groupBy:id|uid}/max/{maxEntries}/operator/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteOperatorAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable Integer maxEntries, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteOperator(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), groupBy, maxEntries, name, format);
	}
	
	@RequestMapping(value="{historical}/by/{groupBy:id|uid}/operator/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteOperatorHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String groupBy, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteOperator(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), groupBy, DEFAULT_MAX_ENTRIES, name, format);
	}
	
	@RequestMapping(value="at/{atDate}/by/{groupBy:id|uid}/Operator/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteOperatorAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String groupBy, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteOperator(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), groupBy, DEFAULT_MAX_ENTRIES, name, format);
	}
	
	protected ResponseEntity<byte[]> doAutocompleteOperator(HttpServletRequest request, String procedureName, String sources, Date atDate, String groupBy, Integer maxEntries, String operatorName, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			CommonAutocompletionFilter filter = this.getCommonAutocompletionFilter(request, sources, atDate, maxEntries, groupBy);
			
			end = System.currentTimeMillis();
			
			return this.getResponse(this.getRawData(this.operatorAutocompletionDAO.searchOperatorNames(procedureName, operatorName, filter.getAtDate(), filter.getLimitTo(), filter.getSources() == null ? filter.getAvailableSources() : filter.getSources(), filter.getGroupByUID()), format, end - start), "VesselOperators_autocompletion_results", format, request);
		} catch(Throwable t) {
			return this.manageError(t);
		}
	}
	
	private byte[] getRawData(Collection<? extends Serializable> results, String format, long elapsed) {
		String content = null;
		
		SerializableList<? extends Serializable> toSerialize = results == null ? null : new SerializableArrayList<Serializable>(results);
		
		if("json".equalsIgnoreCase(format)) {
			content = results == null ? null : new JSONResponse<SerializableList<?>>(toSerialize, elapsed).JSONify();
		} else if("xml".equalsIgnoreCase(format)) {
			try {
				content = results == null ? null : JAXBHelper.toXML(toSerialize);
			} catch(Throwable t) {
				throw new RuntimeException("Unable to serialize results as XML", t);
			}
		} else			
			throw new RuntimeException("Unsupported output format '" + format + "'");
		
		return content == null ? new byte[0] : content.getBytes(Charset.forName("UTF-8"));
	}
	
	private ResponseEntity<byte[]> getResponse(byte[] data, String filename, String format, HttpServletRequest request) {
		HttpHeaders contentTypeHeaders = this.getContentTypeDispositionAndLengthHeaders(filename, format, data.length);
		
		return new ResponseEntity<byte[]>(data, this.mergeHeaders(contentTypeHeaders), HttpStatus.OK);
	}
	
	protected CommonAutocompletionFilter getCommonAutocompletionFilter(HttpServletRequest request, String requestedSources, Date atDate, Integer limitTo, String groupBy) throws Throwable {
		String[] availableSources = this.getAvailableSources(request, false);
		String[] sources = requestedSources == null ? null : requestedSources.split("\\,");
		
		return new CommonAutocompletionFilter(atDate, "uid".equalsIgnoreCase(groupBy), availableSources, sources, limitTo == null ? DEFAULT_MAX_ENTRIES : limitTo);
	}
	
	@EqualsAndHashCode
	protected class CommonAutocompletionFilter implements Serializable {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 5130814352456082192L;
		
		@Getter @Setter private Date atDate = null;
		@Getter @Setter private Boolean groupByUID = Boolean.TRUE;
		@Getter @Setter private String[] availableSources = null;
		@Getter @Setter private String[] sources = null;
		@Getter @Setter private Integer limitTo = 30;
		
		/**
		 * Class constructor
		 *
		 * @param atDate
		 * @param groupByUID
		 * @param sources
		 * @param limitTo
		 * @param authorizedOnly
		 * @param authorizationSource
		 */
		public CommonAutocompletionFilter(Date atDate, Boolean groupByUID, String[] availableSources, String[] sources, Integer limitTo) {
			super();
			this.atDate = atDate;
			this.groupByUID = groupByUID;
			this.availableSources = availableSources;
			this.sources = sources == null || sources.length == 0 ? null : AbstractController.intersectSourceSystems(sources, availableSources);
			this.limitTo = limitTo;
		}
	}
}