/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-web)
 */
package org.fao.fi.vrmf.common.web.constants.services.vessels;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Dec 2010
 */
public interface CommonCompareVesselsServiceConstants {
	String SOURCE_VESSEL_PARAMETER = "s";
	String TARGET_VESSELS_PARAMETER = "t";
}
