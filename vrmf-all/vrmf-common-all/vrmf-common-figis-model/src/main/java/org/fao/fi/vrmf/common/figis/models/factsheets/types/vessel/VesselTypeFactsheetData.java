/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-figis-model)
 */
package org.fao.fi.vrmf.common.figis.models.factsheets.types.vessel;

import org.fao.fi.vrmf.common.figis.models.factsheets.TitledFactsheetData;
import org.fao.fi.vrmf.common.figis.models.factsheets.support.annotations.Domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2013
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
@Domain("vesselType")
public class VesselTypeFactsheetData extends TitledFactsheetData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1015117782294029728L;

	private @Getter @Setter String standardAbbreviation;
	private @Getter @Setter String ISSCFVCode;
}
