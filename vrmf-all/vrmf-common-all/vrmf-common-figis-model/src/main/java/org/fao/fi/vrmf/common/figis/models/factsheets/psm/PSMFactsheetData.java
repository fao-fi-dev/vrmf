/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-figis-model)
 */
package org.fao.fi.vrmf.common.figis.models.factsheets.psm;

import org.fao.fi.vrmf.common.figis.models.factsheets.TitledFactsheetData;
import org.fao.fi.vrmf.common.figis.models.factsheets.support.annotations.Domain;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2013
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
@Domain("psm")
public class PSMFactsheetData extends TitledFactsheetData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 51776375658421150L;
}
