/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-figis-model)
 */
package org.fao.fi.vrmf.common.figis.models.factsheets.support;

import java.util.Iterator;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Oct 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Oct 2012
 */
public class FINamespaceContext implements NamespaceContext {
	public String getNamespaceURI(String prefix) {
		if(prefix == null)
			throw new NullPointerException("Null prefix");
		else if("ags".equals(prefix))
			return "http://www.purl.org/agmes/1.1/";
		else if("aida".equals(prefix))
			return "http://www.idmlinitiative.org/resources/dtds/AIDA22.xsd";
		else if("dc".equals(prefix))
			return "http://purl.org/dc/elements/1.1/"; 
		else if("dcterms".equals(prefix))
			return "http://purl.org/dc/terms/";
		else if("fi".equals(prefix))
			return "http://www.fao.org/fi/figis/devcon/";
		else if("fint".equals(prefix))
			return "http://www.fao.org/fi/figis/internal/";
		else if("fiws".equals(prefix))
			return "http://www.fao.org/fi/figis/devcon/webservice";
		else if("xml".equals(prefix))
			return XMLConstants.XML_NS_URI;
		
		return XMLConstants.DEFAULT_NS_PREFIX;
	}

	// This method isn't necessary for XPath processing.
	public String getPrefix(String uri) {
		throw new UnsupportedOperationException();
	}

	// This method isn't necessary for XPath processing either.
	@SuppressWarnings("rawtypes")
	public Iterator getPrefixes(String uri) {
		throw new UnsupportedOperationException();
	}
}