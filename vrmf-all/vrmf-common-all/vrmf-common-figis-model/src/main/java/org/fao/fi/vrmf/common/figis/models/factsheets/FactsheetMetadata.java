/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-figis-model)
 */
package org.fao.fi.vrmf.common.figis.models.factsheets;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.fao.fi.vrmf.common.core.model.GenericData;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2013
 */
@EqualsAndHashCode(callSuper=true)
public class FactsheetMetadata<F extends FactsheetData> extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4605859581879157517L;
	
	private @Getter @Setter String factsheetDomain;
	private @Getter @Setter Collection<String> availableIDs;
	private @Getter @Setter Map<String, F> factsheetData;
	
	/**
	 * Class constructor
	 *
	 * @param factsheetPrototype
	 * @param availableIDs
	 */
	public FactsheetMetadata(String factsheetDomain, 
							 Collection<String> availableIDs, 
							 Collection<F> factsheetData) {
		super();
		this.factsheetDomain = factsheetDomain;
		this.availableIDs = availableIDs;
		this.factsheetData = new HashMap<String, F>();
		
		if(factsheetData != null && !factsheetData.isEmpty()) {
			for(F data : factsheetData)
				this.factsheetData.put(data.getFactsheetID(), data);
		}
	}
}
