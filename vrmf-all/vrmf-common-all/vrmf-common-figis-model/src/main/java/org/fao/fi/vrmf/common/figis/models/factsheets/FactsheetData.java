/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-figis-model)
 */
package org.fao.fi.vrmf.common.figis.models.factsheets;

import org.fao.fi.vrmf.common.core.model.GenericData;
import org.fao.fi.vrmf.common.figis.models.factsheets.support.annotations.Domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2013
 */
@EqualsAndHashCode(callSuper=true)
public class FactsheetData extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -574823996173889331L;
	
	private @Getter @Setter String factsheetDomain;
	private @Getter @Setter String factsheetID;
	private @Getter @Setter String factsheetURL;
	
	/**
	 * Class constructor
	 *
	 */
	public FactsheetData() {
		super();
		this.setFactsheetDomain(this.getClass().getAnnotation(Domain.class).value());
	}
}