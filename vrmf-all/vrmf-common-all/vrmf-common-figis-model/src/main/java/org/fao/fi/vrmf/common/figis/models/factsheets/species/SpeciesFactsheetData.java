/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-figis-model)
 */
package org.fao.fi.vrmf.common.figis.models.factsheets.species;

import org.fao.fi.vrmf.common.figis.models.factsheets.FactsheetData;
import org.fao.fi.vrmf.common.figis.models.factsheets.support.annotations.Domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Sep 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Sep 2012
 */
@NoArgsConstructor @EqualsAndHashCode(callSuper=true)
@Domain("species")
public class SpeciesFactsheetData extends FactsheetData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4743528659766519221L;
	
	private @Getter @Setter String alpha3Code;
	private @Getter @Setter String nameEn;
	private @Getter @Setter String nameFr;
	private @Getter @Setter String nameEs;
	private @Getter @Setter String images;
	private @Getter @Setter String scientificName;
	private @Getter @Setter String family;
	private @Getter @Setter String personalAuthor;
	private @Getter @Setter Integer year;
	private @Getter @Setter String diagnosticFeatures;
	private @Getter @Setter String areaText;
	private @Getter @Setter String habitatBio;
	private @Getter @Setter String interestFisheries;
	private @Getter @Setter String localNames;
}