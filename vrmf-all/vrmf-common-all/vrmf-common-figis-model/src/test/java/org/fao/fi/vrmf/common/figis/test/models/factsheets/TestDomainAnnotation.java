/**
 * (c) 2013 FAO / UN (project: vrmf-common)
 */
package org.fao.fi.vrmf.common.figis.test.models.factsheets;

import org.fao.fi.vrmf.common.figis.models.factsheets.psm.PSMFactsheetData;
import org.fao.fi.vrmf.common.figis.models.factsheets.species.SpeciesFactsheetData;
import org.fao.fi.vrmf.common.figis.models.factsheets.support.annotations.Domain;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2013
 */
public class TestDomainAnnotation {
	@Test
	public void testSpeciesFactsheetAnnotation() {
		SpeciesFactsheetData fs = new SpeciesFactsheetData();
		Assert.assertEquals(fs.getFactsheetDomain(), SpeciesFactsheetData.class.getAnnotation(Domain.class).value());
	}
	
	@Test
	public void testPSMFactsheetAnnotation() {
		PSMFactsheetData fs = new PSMFactsheetData();
		Assert.assertEquals(fs.getFactsheetDomain(), PSMFactsheetData.class.getAnnotation(Domain.class).value());
	}
}