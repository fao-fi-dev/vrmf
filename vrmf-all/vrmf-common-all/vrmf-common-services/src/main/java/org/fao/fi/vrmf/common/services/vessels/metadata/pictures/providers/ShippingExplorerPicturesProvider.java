/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.http.client.methods.HttpGet;
import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.support.ShippingExplorerPicturesSearchResult;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 May 2013
 */
@Singleton
@Named("vrmf.common.vessels.metadata.pictures.provider.shippingexplorer")
public class ShippingExplorerPicturesProvider extends VesselPicturesProvider {
	final static private String SHIPPING_EXPLORER_PING_URL = "http://www.shippingexplorer.net/en";
	final static private String SHIPPING_EXPLORER_SEARCH_URL = "http://www.shippingexplorer.net/en/vessels/search?s={query}&Area={criteria}&VesselType=15&OnlyPhotos=true";
	
	final static private int SEARCH_NAME = 1;
	final static private int SEARCH_MMSI = 2;
	final static private int SEARCH_IMO  = 3;
	final static private int SEARCH_IRCS = 4;
	
	/**
	 * Class constructor
	 *
	 * @param timeout
	 * @throws Throwable
	 */
	public ShippingExplorerPicturesProvider() throws Throwable {
		super(3000);
	}
	
	/**
	 * Class constructor
	 *
	 * @param timeout
	 * @throws Throwable
	 */
	public ShippingExplorerPicturesProvider(int timeout) throws Throwable {
		super(timeout);
	}

	private Collection<String> doSearchBy(int criteria, String value) throws Throwable {
		String URL = SHIPPING_EXPLORER_SEARCH_URL.replace("{criteria}", String.valueOf(criteria)).replace("{query}", value);
		
		HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(new HttpGet(), URL, null, HTTPHelper.FOLLOW_REDIRECTS, this.getServiceTimeout());
		
		String html = new String(response.getContent(), "UTF-8");
		
		Collection<String> relatedPages = new ListSet<String>();
		
		String pattern = "/en/vessels/view/([0-9]+)";
		
		Matcher matcher = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(html);
		
		int index = 0;

		while(matcher.find(index)) {
			relatedPages.add("http://www.shippingexplorer.net/en/vessels/view/" + matcher.group(1));

			index = matcher.start(1);
		}
		
		return relatedPages;
	}
	
	private Collection<String> getPictureIDsInPage(String URL) throws Throwable {
		HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(new HttpGet(), URL, null, HTTPHelper.DONT_FOLLOW_REDIRECTS, this.getServiceTimeout());
		
		String html = new String(response.getContent(), "UTF-8");
		
		Collection<String> pictureIDs = new ListSet<String>();
		
		String pattern = "src=\"/vesselphoto.svc\\?size=([a-z]+)&photoid=([0-9]+)";
		
		Matcher matcher = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(html);
		
		int index = 0;

		while(matcher.find(index)) {
			pictureIDs.add(matcher.group(2));

			index = matcher.start(2);
		}
		
		return pictureIDs;
	}
	
	private Collection<String> getPictureIDs(int criteria, String value) throws Throwable {
		Collection<String> pictureIDs = new ListSet<String>();		
		
		try {
			Collection<String> pages = this.doSearchBy(criteria, value);
			
			if(pages != null && !pages.isEmpty()) {
				Collection<String> currentIDs;
				
				for(String page : pages) {
					try {
						currentIDs = this.getPictureIDsInPage(page);
						
						if(currentIDs != null && !currentIDs.isEmpty())
							pictureIDs.addAll(currentIDs);
					} catch (Throwable t) {
						this._log.warn("Unable to extract shippingexplorer.net pictures from page {}: {} [ {} ]", page, t.getClass(), t.getMessage());
					}
				}
			}
		} catch(Throwable t) {
			this._log.warn("Unable to retrieve shippingexplorer.net pictures by criteria {}: {} [ {} ]", value, t.getClass(), t.getMessage());
		}
		
		return pictureIDs;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByIMO(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IMO)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIMO(String IMO) {
		Collection<VesselPicturesIdentificationResponseData> results = null;
		
		if(IMO != null) {
			try {
				String parentURL = SHIPPING_EXPLORER_SEARCH_URL.replace("{criteria}", String.valueOf(SEARCH_IMO)).replace("{query}", IMO);
				
				Collection<String> IDs = this.getPictureIDs(SEARCH_IMO, IMO);
				
				if(IDs != null && !IDs.isEmpty()) {
					results = new ArrayList<VesselPicturesIdentificationResponseData>();
					
					results.add(new ShippingExplorerPicturesSearchResult("IMO = " + IMO, parentURL, IDs));
				}
			} catch(Throwable t) {
				this._log.warn("Unable to retrieve shippingexplorer.net pictures by IMO {}: {} [ {} ]", IMO, t.getClass(), t.getMessage());
			}
		}
		
		return results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByEUCFR(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#EUCFR)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByEUCFR(String EUCFR) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByName(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#name)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByName(String name) {
		Collection<VesselPicturesIdentificationResponseData> results = null;
		
		if(name != null) {
			try {
				String parentURL = SHIPPING_EXPLORER_SEARCH_URL.replace("{criteria}", String.valueOf(SEARCH_NAME)).replace("{query}", name);
				
				Collection<String> IDs = this.getPictureIDs(SEARCH_NAME, name);
				
				if(IDs != null && !IDs.isEmpty()) {
					results = new ArrayList<VesselPicturesIdentificationResponseData>();
					
					results.add(new ShippingExplorerPicturesSearchResult("Name = " + name, parentURL, IDs));
				}
			} catch(Throwable t) {
				this._log.warn("Unable to retrieve shippingexplorer.net pictures by name {}: {} [ {} ]", name, t.getClass(), t.getMessage());
			}
		}
		
		return results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByIRCS(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IRCS)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIRCS(String IRCS) {
		Collection<VesselPicturesIdentificationResponseData> results = null;
		
		if(IRCS != null) {
			try {
				String parentURL = SHIPPING_EXPLORER_SEARCH_URL.replace("{criteria}", String.valueOf(SEARCH_IRCS)).replace("{query}", IRCS);
				
				Collection<String> IDs = this.getPictureIDs(SEARCH_IRCS, IRCS);
				
				if(IDs != null && !IDs.isEmpty()) {
					results = new ArrayList<VesselPicturesIdentificationResponseData>();
					
					results.add(new ShippingExplorerPicturesSearchResult("IRCS = " + IRCS, parentURL, IDs));
				}
			} catch(Throwable t) {
				this._log.warn("Unable to retrieve shippingexplorer.net pictures by IRCS {}: {} [ {} ]", IRCS, t.getClass(), t.getMessage());
			}
		}
		
		return results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByMMSI(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#MMSI)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByMMSI(String MMSI) {
		Collection<VesselPicturesIdentificationResponseData> results = null;
		
		if(MMSI != null) {
			try {
				String parentURL = SHIPPING_EXPLORER_SEARCH_URL.replace("{criteria}", String.valueOf(SEARCH_MMSI)).replace("{query}", MMSI);
				
				Collection<String> IDs = this.getPictureIDs(SEARCH_MMSI, MMSI);
				
				if(IDs != null && !IDs.isEmpty()) {
					results = new ArrayList<VesselPicturesIdentificationResponseData>();
					
					results.add(new ShippingExplorerPicturesSearchResult("MMSI = " + MMSI, parentURL, IDs));
				}
			} catch(Throwable t) {
				this._log.warn("Unable to retrieve shippingexplorer.net pictures by MMSI {}: {} [ {} ]", MMSI, t.getClass(), t.getMessage());
			}
		}
		
		return results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByIdentifier(org.fao.vrmf.core.impl.design.patterns.pair.NameValuePair)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#identifier.name + #identifier.value)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByEUCFR()
	 */
	@Override
	protected boolean canIdentifyByEUCFR() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByIMO()
	 */
	@Override
	protected boolean canIdentifyByIMO() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByIRCS()
	 */
	@Override
	protected boolean canIdentifyByIRCS() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByMMSI()
	 */
	@Override
	protected boolean canIdentifyByMMSI() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByName()
	 */
	@Override
	protected boolean canIdentifyByName() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyBySourceID()
	 */
	@Override
	protected boolean canIdentifyBySourceID() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doCheckAvailability(int)
	 */
	@Override
	protected boolean doCheckAvailability(int timeout) {
		try {
			HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(SHIPPING_EXPLORER_PING_URL, timeout);

			return response != null &&
				   response.getResponseCode() <= 299 &&
				   response.getContentType() != null &&
				   response.getContentType().startsWith("text/html") &&
				   response.getContentLength() > 0;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#getSpecificSources()
	 */
	@Override
	protected String[] getSpecificSources() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#isSourceSpecific()
	 */
	@Override
	protected boolean isSourceSpecific() {
		return false;
	}
		
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doGetCustomRequestHeaders(java.lang.Object)
	 */
	protected Collection<NameValuePair> doGetCustomRequestHeaders(Object params) {
		Collection<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new NameValuePair("Cookie", "SESSID=" + StringsHelper.randomString(24, StringsHelper.WITH_NUMBERS)));
		headers.add(new NameValuePair("Host", "www.shippingexplorer.net"));
		headers.add(new NameValuePair("Referer", "http://www.shippingexplorer.net/en/vessels/advancedsearch"));

		return headers;
}
}