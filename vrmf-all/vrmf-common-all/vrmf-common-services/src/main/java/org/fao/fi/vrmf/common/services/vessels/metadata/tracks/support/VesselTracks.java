/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.tracks.support;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 May 2013
 */
@XmlType(name="vesselTrack")
public class VesselTracks extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -325854535706689935L;
	
	@XmlAttribute(name="latitude")
	private String _latitude;
	
	@XmlAttribute(name="longitude")
	private String _longitude;
	
	@XmlAttribute(name="timestamp")
	private long _timestamp;
	
	@XmlAttribute(name="speed")
	private Double _speed;
	
	@XmlAttribute(name="course")
	private Double _course;
	
	/**
	 * Class constructor
	 *
	 * @param latitude
	 * @param longitude
	 * @param timestamp
	 * @param speed
	 * @param course
	 */
	public VesselTracks(String latitude, String longitude, long timestamp, Double speed, Double course) {
		super();
		this._latitude = latitude;
		this._longitude = longitude;
		this._timestamp = timestamp;
		this._speed = speed;
		this._course = course;
	}

	/**
	 * @return the 'latitude' value
	 */
	public String getLatitude() {
		return this._latitude;
	}

	/**
	 * @param latitude the 'latitude' value to set
	 */
	public void setLatitude(String latitude) {
		this._latitude = latitude;
	}

	/**
	 * @return the 'longitude' value
	 */
	public String getLongitude() {
		return this._longitude;
	}

	/**
	 * @param longitude the 'longitude' value to set
	 */
	public void setLongitude(String longitude) {
		this._longitude = longitude;
	}

	/**
	 * @return the 'timestamp' value
	 */
	public long getTimestamp() {
		return this._timestamp;
	}

	/**
	 * @param timestamp the 'timestamp' value to set
	 */
	public void setTimestamp(long timestamp) {
		this._timestamp = timestamp;
	}

	/**
	 * @return the 'speed' value
	 */
	public Double getSpeed() {
		return this._speed;
	}

	/**
	 * @param speed the 'speed' value to set
	 */
	public void setSpeed(Double speed) {
		this._speed = speed;
	}

	/**
	 * @return the 'course' value
	 */
	public Double getCourse() {
		return this._course;
	}

	/**
	 * @param course the 'course' value to set
	 */
	public void setCourse(Double course) {
		this._course = course;
	}
}
