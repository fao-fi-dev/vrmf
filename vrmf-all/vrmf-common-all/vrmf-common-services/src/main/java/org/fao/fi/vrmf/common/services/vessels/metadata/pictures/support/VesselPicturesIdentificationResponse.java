/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support;

import java.util.Collection;

import org.fao.fi.vrmf.common.services.vessels.metadata.VesselMetadataIdentificationResponse;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 May 2013
 */
public class VesselPicturesIdentificationResponse extends VesselMetadataIdentificationResponse<VesselPicturesIdentificationResponseData> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 34143162659137996L;

	public VesselPicturesIdentificationResponse() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param data
	 */
	public VesselPicturesIdentificationResponse(Collection<VesselPicturesIdentificationResponseData> data) {
		super(data);
	}
}