/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures;

import java.util.Collection;
import java.util.List;

import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.AbstractVesselMetadataProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.VesselMetadataIdentificationCriteria;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationRequest;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponse;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2013
 */
abstract public class VesselPicturesProvider extends AbstractVesselMetadataProvider<VesselPicturesIdentificationRequest, VesselPicturesIdentificationResponseData, VesselPicturesIdentificationResponse> {
	static final private String DEFAULT_METHODS_INVOCATION_CACHE_ID = "vrmf.common.services.vessels.pictures";

	/**
	 * Class constructor
	 *
	 * @throws Throwable
	 */
	public VesselPicturesProvider() throws Throwable {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param timeout
	 * @throws Throwable
	 */
	public VesselPicturesProvider(int timeout) throws Throwable {
		super(timeout);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.Service#invoke(org.fao.fi.vrmf.common.core.services.request.ServiceRequest)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.serviceId + #request")
	public VesselPicturesIdentificationResponse invoke(VesselPicturesIdentificationRequest request) throws Exception {
		return this.searchPictures(request);
	}

	final protected Collection<VesselPicturesIdentificationResponseData> searchByIMOs(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselPicturesIdentificationResponseData> results = new ListSet<VesselPicturesIdentificationResponseData>();

		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getIMOs())) {
			this._log.info("{}: searching pictures by IMOs {}", this.getClass().getSimpleName(), criteria.getIMOs());

			for(String data : criteria.getIMOs()) {
				data = StringsHelper.trim(data);

				if(data != null) {
					try {
						this._log.info("{}: searching pictures by IMO {}", this.getClass().getSimpleName(), data);

						results = this.safeAdd(results, this.doSearchByIMO(data));
					} catch (Throwable t) {
						this._log.info("Unable to search pictures by IMO {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}

			int pics = 0;

			for(VesselPicturesIdentificationResponseData result : results)
				pics += result.getIDs().size();

			this._log.info("{}: {} pictures identified by IMOs {}", this.getClass().getSimpleName(), pics, criteria.getIMOs());
		}

		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IMO)")
	abstract public Collection<VesselPicturesIdentificationResponseData> doSearchByIMO(String IMO);

	final protected Collection<VesselPicturesIdentificationResponseData> searchByEUCFRs(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselPicturesIdentificationResponseData> results = new ListSet<VesselPicturesIdentificationResponseData>();

		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getEUCFRs())) {
			this._log.info("{}: searching pictures by EU CFRs {}", this.getClass().getSimpleName(), criteria.getEUCFRs());

			for(String data : criteria.getEUCFRs()) {
				data = StringsHelper.trim(data);

				if(data != null) {
					try {
						this._log.info("{}: searching pictures by EU CFR {}", this.getClass().getSimpleName(), data);

						results = this.safeAdd(results, this.doSearchByEUCFR(data));
					} catch (Throwable t) {
						this._log.info("Unable to search pictures by EU CFR {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}

			int pics = 0;

			for(VesselPicturesIdentificationResponseData result : results)
				pics += result.getIDs().size();

			this._log.info("{}: {} pictures identified by EU CFRs {}", this.getClass().getSimpleName(), pics, criteria.getEUCFRs());
		}

		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#EUCFR)")
	abstract public Collection<VesselPicturesIdentificationResponseData> doSearchByEUCFR(String EUCFR);

	final protected Collection<VesselPicturesIdentificationResponseData> searchByNames(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselPicturesIdentificationResponseData> results = new ListSet<VesselPicturesIdentificationResponseData>();

		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getNames())) {
			this._log.info("{}: searching pictures by names {}", this.getClass().getSimpleName(), criteria.getNames());

			for(String data : criteria.getNames()) {
				data = StringsHelper.trim(data);

				if(data != null) {
					try {
						this._log.info("{}: searching pictures by name {}", this.getClass().getSimpleName(), data);

						results = this.safeAdd(results, this.doSearchByName(data));
					} catch (Throwable t) {
						this._log.info("Unable to search pictures by name {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}

			int pics = 0;

			for(VesselPicturesIdentificationResponseData result : results)
				pics += result.getIDs().size();

			this._log.info("{}: {} pictures identified by names {}", this.getClass().getSimpleName(), pics, criteria.getNames());
		}

		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#name)")
	abstract public Collection<VesselPicturesIdentificationResponseData> doSearchByName(String name);

	final protected Collection<VesselPicturesIdentificationResponseData> searchByIRCSs(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselPicturesIdentificationResponseData> results = new ListSet<VesselPicturesIdentificationResponseData>();

		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getIRCSs())) {
			this._log.info("{}: searching pictures by IRCSs {}", this.getClass().getSimpleName(), criteria.getIRCSs());

			for(String data : criteria.getIRCSs()) {
				data = StringsHelper.trim(data);

				if(data != null) {
					try {
						this._log.info("{}: searching pictures by IRCS {}", this.getClass().getSimpleName(), data);

						results = this.safeAdd(results, this.doSearchByIRCS(data));
					} catch (Throwable t) {
						this._log.info("Unable to search pictures by IRCS {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}

			int pics = 0;

			for(VesselPicturesIdentificationResponseData result : results)
				pics += result.getIDs().size();

			this._log.info("{}: {} pictures identified by IRCSs {}", this.getClass().getSimpleName(), pics, criteria.getIRCSs());
		}

		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IRCS)")
	abstract public Collection<VesselPicturesIdentificationResponseData> doSearchByIRCS(String IRCS);

	final protected Collection<VesselPicturesIdentificationResponseData> searchByMMSIs(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselPicturesIdentificationResponseData> results = new ListSet<VesselPicturesIdentificationResponseData>();

		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getMMSIs())) {
			this._log.info("{}: searching pictures by MMSIs {}", this.getClass().getSimpleName(), criteria.getMMSIs());

			for(String data : criteria.getMMSIs()) {
				data = StringsHelper.trim(data);

				if(data != null) {
					try {
						this._log.info("{}: searching pictures by MMSI {}", this.getClass().getSimpleName(), data);

						results = this.safeAdd(results, this.doSearchByMMSI(data));
					} catch (Throwable t) {
						this._log.info("Unable to search pictures by MMSI {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}

			int pics = 0;

			for(VesselPicturesIdentificationResponseData result : results)
				pics += result.getIDs().size();

			this._log.info("{}: {} pictures identified by MMSIs {}", this.getClass().getSimpleName(), pics, criteria.getMMSIs());
		}

		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#MMSI)")
	abstract public Collection<VesselPicturesIdentificationResponseData> doSearchByMMSI(String MMSI);

	final protected Collection<VesselPicturesIdentificationResponseData> searchByIdentifiers(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselPicturesIdentificationResponseData> results = new ListSet<VesselPicturesIdentificationResponseData>();

		if(criteria != null && !criteria.isEmpty() &&
		   !this.isEmpty(criteria.getSourceIdentifiers()) &&
		   !this.isEmpty(this.getSourceIDs(criteria))) {
			String identifierType, identifier;
			for(NameValuePair data : this.getSourceIDs(criteria)) {
				identifierType = StringsHelper.trim(data.getName());
				identifier = StringsHelper.trim((String)data.getValue());

				if(identifierType != null && identifier != null) {
					try {
						this._log.info("{}: searching pictures by identifier {}", this.getClass().getSimpleName(), data);

						results = this.safeAdd(results, this.doSearchByIdentifier(data));
					} catch (Throwable t) {
						this._log.info("Unable to search pictures by identifier {}: {} [ {} ]",
										data.getName() + " #" + data.getValue(),
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}

			int pics = 0;

			for(VesselPicturesIdentificationResponseData result : results)
				pics += result.getIDs().size();

			this._log.info("{}: {} pictures identified by identifiers {}", this.getClass().getSimpleName(), pics, this.getSourceIDs(criteria));
		}

		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#identifier.name + #identifier.value)")
	abstract public Collection<VesselPicturesIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier);

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.serviceId #criteria")
	public VesselPicturesIdentificationResponse searchPictures(VesselMetadataIdentificationCriteria criteria) {
		List<VesselPicturesIdentificationResponseData> data = new ListSet<VesselPicturesIdentificationResponseData>();

		if(this.canIdentifyByIMO()) {
			data.addAll(this.searchByIMOs(criteria));
		}

		if(this.canIdentifyByEUCFR()) {
			data.addAll(this.searchByEUCFRs(criteria));
		}

		if(this.canIdentifyByName()) {
			VesselMetadataIdentificationCriteria updatedCriteria = SerializationHelper.clone(criteria);
			
			if(criteria.getNames() != null) {
				List<String> names = new ListSet<String>();
				
				String simplified;
				
				for(String name : criteria.getNames()) {
					if(name != null) {
						names.add(name);
						
						simplified = this._vesselNamesSimplifier.process(name);
						
						if(simplified != null && !simplified.equalsIgnoreCase(name))
							names.add(simplified);
					}
					
					updatedCriteria.setNames(names.toArray(new String[names.size()]));
				}
			}
			
			data.addAll(this.searchByNames(updatedCriteria));
		}

		if(this.canIdentifyByIRCS()) {
			data.addAll(this.searchByIRCSs(criteria));
		}

		if(this.canIdentifyByMMSI()) {
			data.addAll(this.searchByMMSIs(criteria));
		}

		if(this.canIdentifyBySourceID()) {
			data.addAll(this.searchByIdentifiers(criteria));
		}

		return new VesselPicturesIdentificationResponse(this.normalize(this.filter(data)));
	}
}