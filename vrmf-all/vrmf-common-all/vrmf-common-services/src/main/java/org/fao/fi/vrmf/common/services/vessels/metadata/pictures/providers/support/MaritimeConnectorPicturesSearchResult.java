/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.support;

import java.util.Collection;

import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2011
 */
final public class MaritimeConnectorPicturesSearchResult extends VesselPicturesIdentificationResponseData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -9114774768279925562L;

	/**
	 * Class constructor
	 *
	 * @param source
	 * @param thumbsURL
	 * @param fullURL
	 * @param criteria
	 * @param iDs
	 */
	public MaritimeConnectorPicturesSearchResult(String source, String thumbsURL, String fullURL, String criteria, String parentURL, Collection<String> IDs) {
		super(PRIVATE, "Maritime Connector", source, thumbsURL, fullURL, criteria, parentURL, IDs);
	}
	
	public MaritimeConnectorPicturesSearchResult(String criteria, String parentURL, Collection<String> IDs) {
		this("http://www.maritime-connector.com",
			 "http://maritime-connector.com/ships_uploads/",
			 "http://maritime-connector.com/ships_uploads/",
			 criteria,
			 parentURL,
			 IDs);
	}
}