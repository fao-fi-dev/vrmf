/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.support.CCAMLRPicturesSearchResult;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

@Singleton
@Named("vrmf.common.vessels.metadata.pictures.provider.ccamlr")
public class CCAMLRPicturesProvider extends VesselPicturesProvider {
	static final private String CCAMLR_PICTURES_URL = "http://www.ccamlr.org/en/node/{query}/images";
	static final private String CCAMLR_PING_URL = "http://www.ccamlr.org";

	public CCAMLRPicturesProvider() throws Throwable {
		super(10000);
	}

	@Override
	protected boolean doCheckAvailability(int timeout) {
		try {
			HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(CCAMLR_PING_URL, timeout);

			return response != null &&
				   response.getResponseCode() <= 299 &&
				   response.getContentType() != null &&
				   response.getContentType().startsWith("text/html") &&
				   response.getContentLength() > 0;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}

	@Override
	protected boolean canIdentifyByIMO() {
		return false;
	}

	@Override
	protected boolean canIdentifyByEUCFR() {
		return false;
	}

	@Override
	protected boolean canIdentifyByName() {
		return false;
	}

	@Override
	protected boolean canIdentifyByIRCS() {
		return false;
	}

	@Override
	protected boolean canIdentifyByMMSI() {
		return false;
	}

	@Override
	protected boolean canIdentifyBySourceID() {
		return true;
	}

	@Override
	protected boolean isSourceSpecific() {
		return false;
	}

	@Override
	protected String[] getSpecificSources() {
		return new String[] { "CCAMLR" };
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#IMO)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIMO(String IMO) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#EUCFR)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByEUCFR(String EUCFR) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#name)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByName(String name) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#IRCS)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIRCS(String IRCS) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#MMSI)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByMMSI(String MMSI) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#identifier.name + #identifier.value)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier) {
		Collection<VesselPicturesIdentificationResponseData> results = null;

		if("CCAMLR".equals(identifier.getName())) {
			String CCAMLR_ID = (String)identifier.getValue();
			String URL = CCAMLR_PICTURES_URL.replace("{query}", CCAMLR_ID);

			try {
				String result = this._httpHelper.fetchHTMLPageAsUTF8String(URL, this.getServiceTimeout());

				String pattern = "src=\"[^\"]+/styles/medium/public/([^\"]+\\.jpg).*\"[^>]*>";

				Collection<String> pictureIDs = new TreeSet<String>();

				Matcher matcher = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(result);

				int index = 0;

				while(matcher.find(index)) {
					pictureIDs.add(matcher.group(1));

					index = matcher.start(1);
				}

				results = new ArrayList<VesselPicturesIdentificationResponseData>();

				results.add(new CCAMLRPicturesSearchResult("CCAMLR ID = " + CCAMLR_ID, URL, pictureIDs));
			} catch(Throwable t) {
				this._log.warn("Unable to retrieve CCAMLR pictures by CCAMLR ID {}: {} [ {} ]", CCAMLR_ID, t.getClass(), t.getMessage());
			}
		}

		return results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doGetCustomRequestHeaders(java.lang.Object)
	 */
	@Override
	protected Collection<NameValuePair> doGetCustomRequestHeaders(Object params) {
		return NO_CUSTOM_HEADERS;
	}
}