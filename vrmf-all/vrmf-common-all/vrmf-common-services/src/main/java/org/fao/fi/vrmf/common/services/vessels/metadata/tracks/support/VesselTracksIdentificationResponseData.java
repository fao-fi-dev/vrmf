/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.tracks.support;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.vrmf.common.services.vessels.metadata.VesselMetadataIdentificationResult;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 Oct 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 Oct 2012
 */
@XmlType(name="vesselPositionsData")
public class VesselTracksIdentificationResponseData extends VesselMetadataIdentificationResult {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6634962412843712144L;

	@XmlAttribute(name="isPublic")
	private Boolean _isPublic;

	@XmlElement(name="source")
	private String _source;

	@XmlElement(name="sourceURL")
	private String _sourceURL;

	@XmlElement(name="criteria")
	private String _criteria;

	@XmlElement(name="timestamp")
	private long _timestamp;
	
	@XmlElementWrapper(name="vesselPositions")
	@XmlElement(name="vesselPosition")
	private Collection<VesselTracks> _positions;

	/**
	 * Class constructor
	 *
	 */
	public VesselTracksIdentificationResponseData() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param isPublic
	 * @param source
	 * @param sourceURL
	 * @param criteria
	 * @param timestamp
	 * @param positions
	 */
	public VesselTracksIdentificationResponseData(Boolean isPublic, String source, String sourceURL, String criteria, long timestamp, Collection<VesselTracks> positions) {
		super();
		this._isPublic = isPublic;
		this._source = source;
		this._sourceURL = sourceURL;
		this._criteria = criteria;
		this._timestamp = timestamp;
		this._positions = positions;
	}

	/**
	 * Class constructor
	 *
	 * @param isPublic
	 * @param source
	 * @param sourceURL
	 * @param criteria
	 * @param position
	 */
	public VesselTracksIdentificationResponseData(Boolean isPublic, String source, String sourceURL, String criteria, long timestamp, VesselTracks position) {
		this(isPublic, source, sourceURL, criteria, timestamp, new ArrayList<VesselTracks>(Arrays.asList(position == null ? new VesselTracks[] {} : new VesselTracks[] { position } )));
	}

	/**
	 * @return the 'isPublic' value
	 */
	public Boolean getIsPublic() {
		return this._isPublic;
	}

	/**
	 * @param isPublic the 'isPublic' value to set
	 */
	public void setIsPublic(Boolean isPublic) {
		this._isPublic = isPublic;
	}

	/**
	 * @return the 'source' value
	 */
	public String getSource() {
		return this._source;
	}

	/**
	 * @param source the 'source' value to set
	 */
	public void setSource(String source) {
		this._source = source;
	}

	/**
	 * @return the 'sourceURL' value
	 */
	public String getSourceURL() {
		return this._sourceURL;
	}

	/**
	 * @param sourceURL the 'sourceURL' value to set
	 */
	public void setSourceURL(String sourceURL) {
		this._sourceURL = sourceURL;
	}

	/**
	 * @return the 'criteria' value
	 */
	public String getCriteria() {
		return this._criteria;
	}

	/**
	 * @param criteria the 'criteria' value to set
	 */
	public void setCriteria(String criteria) {
		this._criteria = criteria;
	}

	/**
	 * @return the 'positions' value
	 */
	public Collection<VesselTracks> getPositions() {
		return this._positions;
	}

	/**
	 * @param positions the 'positions' value to set
	 */
	public void setPositions(Collection<VesselTracks> positions) {
		this._positions = positions;
	}

	/**
	 * @return the 'timestamp' value
	 */
	public long getTimestamp() {
		return this._timestamp;
	}

	/**
	 * @param timestamp the 'timestamp' value to set
	 */
	public void setTimestamp(long timestamp) {
		this._timestamp = timestamp;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.VesselMetadataIdentificationResponseData#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return this._positions == null || this._positions.isEmpty();
	}
}
