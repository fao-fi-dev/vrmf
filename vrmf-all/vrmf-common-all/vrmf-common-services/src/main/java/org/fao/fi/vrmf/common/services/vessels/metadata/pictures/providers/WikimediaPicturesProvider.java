/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.support.WikimediaPicturesSearchResult;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2013
 */
@Singleton
@Named("vrmf.common.vessels.metadata.pictures.provider.wikimedia")
public class WikimediaPicturesProvider extends VesselPicturesProvider {
	static final private String WIKIMEDIA_API_QUERY_CATEGORIES_URL = "http://commons.wikimedia.org/w/api.php?format=xml&action=query&list=categorymembers&cmtitle=";
	static final private String WIKIMEDIA_API_IMAGE_INFO_URL = "http://commons.wikimedia.org/w/api.php?format=xml&action=query&prop=imageinfo&iiprop=url&titles=";

	static final private String WIKIMEDIA_PAGE_URL = "http://commons.wikimedia.org/wiki/";
	static final private String WIKIMEDIA_SEARCH_PAGE_URL = WIKIMEDIA_PAGE_URL + "Category:IMO_";

	static final private String WIKIMEDIA_PING_URL = WIKIMEDIA_API_QUERY_CATEGORIES_URL + "Category:IMO_0100052";

	static final private String WIKIMEDIA_CATEGORIES_XPATH_EXTRACTOR = "/api/query/categorymembers/cm";
	static final private String WIKIMEDIA_IMAGEINFO_XPATH_EXTRACTOR = "/api/query/pages/page/imageinfo/ii";

	static private XPathExpression WIKIMEDIA_CATEGORIES_XPATH_EXPR;
	static private XPathExpression WIKIMEDIA_IMAGEINFO_XPATH_EXPR;

	/**
	 * Class constructor
	 *
	 * @throws Throwable
	 */
	public WikimediaPicturesProvider() throws Throwable {
		super(2000);

		WIKIMEDIA_CATEGORIES_XPATH_EXPR = this._xPathFactory.newXPath().compile(WIKIMEDIA_CATEGORIES_XPATH_EXTRACTOR);
		WIKIMEDIA_IMAGEINFO_XPATH_EXPR  = this._xPathFactory.newXPath().compile(WIKIMEDIA_IMAGEINFO_XPATH_EXTRACTOR);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#checkAvailability(int)
	 */
	@Override
	protected boolean doCheckAvailability(int timeout) {
		try {
			HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(WIKIMEDIA_PING_URL,
																			this.forgeRequestHeaders(),
																			timeout);

			return response != null &&
				   response.getResponseCode() <= 299 &&
				   response.getContentType() != null &&
				   response.getContentType().startsWith("text/xml") &&
				   response.getContentLength() > 0;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#canIdentifyByIMO()
	 */
	@Override
	protected boolean canIdentifyByIMO() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#canIdentifyByEUCFR()
	 */
	@Override
	protected boolean canIdentifyByEUCFR() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#canIdentifyByName()
	 */
	@Override
	protected boolean canIdentifyByName() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#canIdentifyByIRCS()
	 */
	@Override
	protected boolean canIdentifyByIRCS() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#canIdentifyByMMSI()
	 */
	@Override
	protected boolean canIdentifyByMMSI() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#canIdentifyBySourceID()
	 */
	@Override
	protected boolean canIdentifyBySourceID() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#isSourceSpecific()
	 */
	@Override
	protected boolean isSourceSpecific() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#getSpecificSources()
	 */
	@Override
	protected String[] getSpecificSources() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#doSearchByIMO(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#IMO)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIMO(String IMO) {
		Collection<String> pictureIDs = new ListSet<String>();
		Collection<VesselPicturesIdentificationResponseData> results = new ArrayList<VesselPicturesIdentificationResponseData>();

		Collection<NameValuePair> headers = this.forgeRequestHeaders();
		
		try {
			results.add(new WikimediaPicturesSearchResult("IMO = " + IMO, WIKIMEDIA_SEARCH_PAGE_URL + IMO, pictureIDs));

			String URL = WIKIMEDIA_API_QUERY_CATEGORIES_URL + "Category:IMO_" + IMO;

			String content = this._httpHelper.fetchHTMLPageAsUTF8String(URL, headers, this.getServiceTimeout());

			Document vesselCategories = this._documentBuilder.parse(new ByteArrayInputStream(content.getBytes("UTF-8")));

			NodeList categoryMembers = (NodeList)WIKIMEDIA_CATEGORIES_XPATH_EXPR.evaluate(vesselCategories, XPathConstants.NODESET);

			if(categoryMembers.getLength() == 0)
				return results;

			Node category;
			String title;
			for(int c=0; c<categoryMembers.getLength(); c++) {
				category = categoryMembers.item(c);

				title = StringsHelper.trim(category.getAttributes().getNamedItem("title").getNodeValue());

				if(title != null) {
					title = title.replaceAll("\\s", "_");//VesselPicturesRetriever.encodeTerm(title.replaceAll("\\s", "_"));

					URL = WIKIMEDIA_API_QUERY_CATEGORIES_URL + title;

					content = this._httpHelper.fetchHTMLPageAsUTF8String(URL, headers, this.getServiceTimeout());

					Document images = this._documentBuilder.parse(new ByteArrayInputStream(content.getBytes("UTF-8")));

					NodeList imagesCategoryMembers = (NodeList)WIKIMEDIA_CATEGORIES_XPATH_EXPR.evaluate(images, XPathConstants.NODESET);

					if(imagesCategoryMembers.getLength() == 0)
						continue;

					Node image;
					String imageCategory;
					for(int f=0; f<imagesCategoryMembers.getLength(); f++) {
						image = imagesCategoryMembers.item(f);

						imageCategory = StringsHelper.trim(image.getAttributes().getNamedItem("title").getNodeValue());//VesselPicturesRetriever.encodeTerm(StringHelper.rawTrim(image.getAttributes().getNamedItem("title").getNodeValue()));

						if(imageCategory != null) {
							URL = WIKIMEDIA_API_IMAGE_INFO_URL + imageCategory;

							content = this._httpHelper.fetchHTMLPageAsUTF8String(URL, headers, this.getServiceTimeout());

							Document imageInfos = this._documentBuilder.parse(new ByteArrayInputStream(content.getBytes("UTF-8")));

							NodeList imageInfo = (NodeList)WIKIMEDIA_IMAGEINFO_XPATH_EXPR.evaluate(imageInfos, XPathConstants.NODESET);

							if(imageInfo.getLength() == 0)
								continue;

							for(int i=0; i<imageInfo.getLength(); i++)
								//photoIDs.add(VesselPicturesRetriever.encodeTerm(imageInfo.item(i).getAttributes().getNamedItem("url").getNodeValue().substring("http://upload.wikimedia.org/wikipedia/commons/".length())));
								pictureIDs.add(this.encodeTerm(imageInfo.item(i).getAttributes().getNamedItem("url").getNodeValue().substring("http://upload.wikimedia.org/wikipedia/commons/".length())));
						}
					}
				}
			}
		} catch (SAXException SAXe) {
			this._log.warn("Unable to parse Wikimedia content: {} [ {} ]", SAXe.getClass(), SAXe.getMessage());
		} catch (XPathExpressionException XPEe) {
			this._log.warn("Unable to evaluate Wikimedia content: {} [ {} ]", XPEe.getClass(), XPEe.getMessage());
		} catch (Throwable t) {
			this._log.warn("Unable to retrieve Wikimedia pictures by IMO {}: {} [ {} ]", IMO, t.getClass(), t.getMessage());
		}

		return results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#doSearchByEUCFR(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#EUCFR)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByEUCFR(String EUCFR) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#doSearchByName(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#name)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByName(String name) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#doSearchByIRCS(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#IRCS)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIRCS(String IRCS) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#doSearchByMMSI(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#MMSI)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByMMSI(String MMSI) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.pictures.VesselPicturesProvider#doSearchByIdentifier(org.fao.vrmf.core.impl.design.patterns.pair.NameValuePair)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#identifier.name + #identifier.value)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier) {
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doGetCustomRequestHeaders(java.lang.Object)
	 */
	@Override
	protected Collection<NameValuePair> doGetCustomRequestHeaders(Object params) {
		Collection<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new NameValuePair("User-Agent", "FAO_VRMF_Vessel_Images_Finder/1.2 (http://www.fao.org/fishery/statistics/software/fvrmf/en; VRMF-Administrator@fao.org)"));

		return headers;
	}
}
