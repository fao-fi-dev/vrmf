/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 May 2013
 */
public class VesselMetadataIdentificationCriteria extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6936025018299486471L;
	
	protected String[] _IMOs;
	protected String[] _EUCFRs;
	protected String[] _names;
	protected String[] _IRCSs;
	protected String[] _MMSIs;
	
	protected NameValuePair[] _sourceIdentifiers;
	
	/**
	 * Class constructor
	 *
	 * @param iMOs
	 * @param eUCFRs
	 * @param names
	 * @param iRCSs
	 * @param mMSIs
	 * @param sourceIdentifiers
	 */
	public VesselMetadataIdentificationCriteria(String[] iMOs, String[] eUCFRs, String[] names, String[] iRCSs, String[] mMSIs, NameValuePair[] sourceIdentifiers) {
		super();
		this._IMOs = iMOs;
		this._EUCFRs = eUCFRs;
		this._names = names;
		this._IRCSs = iRCSs;
		this._MMSIs = mMSIs;
		this._sourceIdentifiers = sourceIdentifiers;
	}
	
	/**
	 * @return the 'iMOs' value
	 */
	public String[] getIMOs() {
		return this._IMOs;
	}

	/**
	 * @param iMOs the 'iMOs' value to set
	 */
	public void setIMOs(String[] iMOs) {
		this._IMOs = iMOs;
	}

	/**
	 * @return the 'eUCFRs' value
	 */
	public String[] getEUCFRs() {
		return this._EUCFRs;
	}

	/**
	 * @param eUCFRs the 'eUCFRs' value to set
	 */
	public void setEUCFRs(String[] eUCFRs) {
		this._EUCFRs = eUCFRs;
	}

	/**
	 * @return the 'names' value
	 */
	public String[] getNames() {
		return this._names;
	}

	/**
	 * @param names the 'names' value to set
	 */
	public void setNames(String[] names) {
		this._names = names;
	}

	/**
	 * @return the 'iRCSs' value
	 */
	public String[] getIRCSs() {
		return this._IRCSs;
	}

	/**
	 * @param iRCSs the 'iRCSs' value to set
	 */
	public void setIRCSs(String[] iRCSs) {
		this._IRCSs = iRCSs;
	}

	/**
	 * @return the 'mMSIs' value
	 */
	public String[] getMMSIs() {
		return this._MMSIs;
	}

	/**
	 * @param mMSIs the 'mMSIs' value to set
	 */
	public void setMMSIs(String[] mMSIs) {
		this._MMSIs = mMSIs;
	}

	/**
	 * @return the 'sourceIdentifiers' value
	 */
	public NameValuePair[] getSourceIdentifiers() {
		return this._sourceIdentifiers;
	}

	/**
	 * @param sourceIdentifiers the 'sourceIdentifiers' value to set
	 */
	public void setSourceIdentifiers(NameValuePair[] sourceIdentifiers) {
		this._sourceIdentifiers = sourceIdentifiers;
	}
	
	final public NameValuePair[] findSourceIdentifiers(String[] sources) {
		if(sources == null || sources.length == 0 || this._sourceIdentifiers == null || this._sourceIdentifiers.length == 0)
			return null;
		
		Set<String> uniqueSources = new TreeSet<String>(Arrays.asList(sources));
		Collection<NameValuePair> identifiers = new ListSet<NameValuePair>();
		
		for(NameValuePair id : this._sourceIdentifiers) {
			if(uniqueSources.contains(id.getName()))
				identifiers.add(id);
		}
		
		if(identifiers.isEmpty())
			return null;
		
		return identifiers.toArray(new NameValuePair[identifiers.size()]);
	}
	
	/**
	 * @return
	 */
	final public boolean isEmpty() {
		return ( this._IMOs == null || this._IMOs.length == 0 ) &&
			   ( this._EUCFRs == null || this._EUCFRs.length == 0 ) &&
			   ( this._names == null || this._names.length == 0 ) &&
			   ( this._IRCSs == null || this._IRCSs.length == 0 ) &&
			   ( this._MMSIs == null || this._MMSIs.length == 0 ) &&
			   ( this._sourceIdentifiers == null || this._sourceIdentifiers.length == 0 );
	}
	
	final public boolean hasData() {
		return !this.isEmpty();
	}
	
	public String toString() {
		StringBuilder string = new StringBuilder();
		
		string.append(this._IMOs == null ? "NULL" : CollectionsHelper.join(this._IMOs, ","));
		string.append("_");
		string.append(this._EUCFRs == null ? "NULL" : CollectionsHelper.join(this._EUCFRs, ","));
		string.append("_");
		string.append(this._names == null ? "NULL" : CollectionsHelper.join(this._names, ","));
		string.append("_");
		string.append(this._IRCSs == null ? "NULL" : CollectionsHelper.join(this._IRCSs, ","));
		string.append("_");
		string.append(this._MMSIs == null ? "NULL" : CollectionsHelper.join(this._MMSIs, ","));
		string.append("_");
		string.append(this._sourceIdentifiers == null ? "NULL" : CollectionsHelper.join(this._sourceIdentifiers, ","));
		
		return string.toString();
	}
}
