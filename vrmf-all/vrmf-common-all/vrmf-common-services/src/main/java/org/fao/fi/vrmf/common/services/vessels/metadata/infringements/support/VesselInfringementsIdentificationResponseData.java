/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.infringements.support;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.common.services.vessels.metadata.VesselMetadataIdentificationResult;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Feb 2013
 */
public class VesselInfringementsIdentificationResponseData extends VesselMetadataIdentificationResult {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2705575174582864890L;
	
	private String _dataId;
	private String _criteria;
	private String _details;
	private String _source;
	private String _sourceURL;
	
	/**
	 * @return the 'dataId' value
	 */
	public String getDataId() {
		return this._dataId;
	}
	
	/**
	 * @param dataId the 'dataId' value to set
	 */
	public void setDataId(String dataId) {
		this._dataId = dataId;
	}
	
	/**
	 * @return the 'criteria' value
	 */
	public String getCriteria() {
		return this._criteria;
	}

	/**
	 * @param criteria the 'criteria' value to set
	 */
	public void setCriteria(String criteria) {
		this._criteria = criteria;
	}

	/**
	 * @return the 'details' value
	 */
	public String getDetails() {
		return this._details;
	}
	
	/**
	 * @param details the 'details' value to set
	 */
	public void setDetails(String details) {
		this._details = details;
	}

	/**
	 * @return the 'source' value
	 */
	public String getSource() {
		return this._source;
	}

	/**
	 * @param source the 'source' value to set
	 */
	public void setSource(String source) {
		this._source = source;
	}

	/**
	 * @return the 'sourceURL' value
	 */
	public String getSourceURL() {
		return this._sourceURL;
	}

	/**
	 * @param sourceURL the 'sourceURL' value to set
	 */
	public void setSourceURL(String sourceURL) {
		this._sourceURL = sourceURL;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.VesselMetadataIdentificationResult#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return StringsHelper.trim(this._details) == null;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _criteria == null ) ? 0 : _criteria.hashCode() );
		result = prime * result + ( ( _dataId == null ) ? 0 : _dataId.hashCode() );
		result = prime * result + ( ( _details == null ) ? 0 : _details.hashCode() );
		result = prime * result + ( ( _source == null ) ? 0 : _source.hashCode() );
		result = prime * result + ( ( _sourceURL == null ) ? 0 : _sourceURL.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		VesselInfringementsIdentificationResponseData other = (VesselInfringementsIdentificationResponseData) obj;
		if(_criteria == null) {
			if(other._criteria != null) return false;
		} else if(!_criteria.equals(other._criteria)) return false;
		if(_dataId == null) {
			if(other._dataId != null) return false;
		} else if(!_dataId.equals(other._dataId)) return false;
		if(_details == null) {
			if(other._details != null) return false;
		} else if(!_details.equals(other._details)) return false;
		if(_source == null) {
			if(other._source != null) return false;
		} else if(!_source.equals(other._source)) return false;
		if(_sourceURL == null) {
			if(other._sourceURL != null) return false;
		} else if(!_sourceURL.equals(other._sourceURL)) return false;
		return true;
	}
}