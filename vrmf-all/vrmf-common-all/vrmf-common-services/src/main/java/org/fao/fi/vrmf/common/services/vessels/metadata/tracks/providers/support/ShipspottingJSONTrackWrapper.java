/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.tracks.providers.support;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 May 2013
 */
public class ShipspottingJSONTrackWrapper extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6605531067159565778L;

	private ShipspottingJSONTrackData[] Results;

	/**
	 * @return the 'results' value
	 */
	public ShipspottingJSONTrackData[] getResults() {
		return this.Results;
	}

	/**
	 * @param results the 'results' value to set
	 */
	public void setResults(ShipspottingJSONTrackData[] results) {
		this.Results = results;
	}

	/**
	 * Class constructor
	 *
	 * @param results
	 */
	public ShipspottingJSONTrackWrapper(ShipspottingJSONTrackData[] results) {
		super();
		this.Results = results;
	}

	/**
	 * Class constructor
	 *
	 */
	public ShipspottingJSONTrackWrapper() {
		super();
	}
}
