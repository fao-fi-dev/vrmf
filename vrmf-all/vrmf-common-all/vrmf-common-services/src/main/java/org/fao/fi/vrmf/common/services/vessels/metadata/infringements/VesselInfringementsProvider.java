/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.infringements;

import java.util.Collection;
import java.util.List;

import javax.inject.Named;

import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.AbstractVesselMetadataProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.VesselMetadataIdentificationCriteria;
import org.fao.fi.vrmf.common.services.vessels.metadata.infringements.support.VesselInfringementsIdentificationRequest;
import org.fao.fi.vrmf.common.services.vessels.metadata.infringements.support.VesselInfringementsIdentificationResponse;
import org.fao.fi.vrmf.common.services.vessels.metadata.infringements.support.VesselInfringementsIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Feb 2013
 */
@Named
abstract public class VesselInfringementsProvider extends AbstractVesselMetadataProvider<VesselInfringementsIdentificationRequest, VesselInfringementsIdentificationResponseData, VesselInfringementsIdentificationResponse> {
	static final protected String DEFAULT_METHODS_INVOCATION_CACHE_ID = "vrmf.common.services.vessels.infringements";
		
	/**
	 * Class constructor
	 *
	 * @throws Throwable
	 */
	public VesselInfringementsProvider() throws Throwable {
		super(3000);
	}

	/**
	 * Class constructor
	 *
	 * @param timeout
	 * @throws Throwable
	 */
	public VesselInfringementsProvider(int timeout) throws Throwable {
		super(timeout);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.Service#invoke(org.fao.fi.vrmf.common.core.services.request.ServiceRequest)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.serviceId + #request")
	public VesselInfringementsIdentificationResponse invoke(VesselInfringementsIdentificationRequest request) throws Exception {
		return this.searchInfringements(request);
	}
	
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IMO)")
	abstract public Collection<VesselInfringementsIdentificationResponseData> doSearchByIMO(String IMO);

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IRCS)")
	abstract public Collection<VesselInfringementsIdentificationResponseData> doSearchByIRCS(String IRCS);

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#name)")
	abstract public Collection<VesselInfringementsIdentificationResponseData> doSearchByName(String name);

	final protected Collection<VesselInfringementsIdentificationResponseData> searchByIMOs(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselInfringementsIdentificationResponseData> results = new ListSet<VesselInfringementsIdentificationResponseData>();
	
		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getIMOs())) {
			this._log.info("{}: searching positions by IMOs {}", this.getClass().getSimpleName(), criteria.getIMOs());
	
			for(String data : criteria.getIMOs()) {
				data = StringsHelper.trim(data);
	
				if(data != null) {
					try {
						this._log.info("{}: searching infringements by IMO {}", this.getClass().getSimpleName(), data);
	
						results = this.safeAdd(results, this.doSearchByIMO(data));
					} catch (Throwable t) {
						this._log.info("Unable to search infringements by IMO {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}
	
			int infringements = 0;
	
			for(VesselInfringementsIdentificationResponseData result : results)
				if(!result.isEmpty())
					infringements++;
	
			this._log.info("{}: {} infringements identified by IMOs {}", this.getClass().getSimpleName(), infringements, criteria.getIMOs());
		}
	
		return results;
	}

	final protected Collection<VesselInfringementsIdentificationResponseData> searchByIRCSs(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselInfringementsIdentificationResponseData> results = new ListSet<VesselInfringementsIdentificationResponseData>();
	
		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getIRCSs())) {
			this._log.info("{}: searching infringements by IRCSs {}", this.getClass().getSimpleName(), criteria.getIRCSs());
	
			for(String data : criteria.getIRCSs()) {
				data = StringsHelper.trim(data);
	
				if(data != null) {
					try {
						this._log.info("{}: searching infringements by IRCS {}", this.getClass().getSimpleName(), data);
	
						results = this.safeAdd(results, this.doSearchByIRCS(data));
					} catch (Throwable t) {
						this._log.info("Unable to search infringements by IRCS {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}
	
			int infringements = 0;
	
			for(VesselInfringementsIdentificationResponseData result : results)
				if(!result.isEmpty())
					infringements++;
	
			this._log.info("{}: {} infringements identified by IRCSs {}", this.getClass().getSimpleName(), infringements, criteria.getIRCSs());
		}
	
		return results;
	}

	final protected Collection<VesselInfringementsIdentificationResponseData> searchByNames(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselInfringementsIdentificationResponseData> results = new ListSet<VesselInfringementsIdentificationResponseData>();
	
		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getNames())) {
			this._log.info("{}: searching infringements by names {}", this.getClass().getSimpleName(), criteria.getNames());
	
			for(String data : criteria.getNames()) {
				data = StringsHelper.trim(data);
	
				if(data != null) {
					try {
						this._log.info("{}: searching infringements by name {}", this.getClass().getSimpleName(), data);
	
						results = this.safeAdd(results, this.doSearchByName(data));
					} catch (Throwable t) {
						this._log.info("Unable to search infringements by name {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}
	
			int infringements = 0;
	
			for(VesselInfringementsIdentificationResponseData result : results)
				if(!result.isEmpty())
					infringements++;
	
			this._log.info("{}: {} infringements identified by names {}", this.getClass().getSimpleName(), infringements, criteria.getNames());
		}
	
		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.serviceId #criteria")
	public VesselInfringementsIdentificationResponse searchInfringements(VesselMetadataIdentificationCriteria criteria) {
		List<VesselInfringementsIdentificationResponseData> data = new ListSet<VesselInfringementsIdentificationResponseData>();
	
		if(this.canIdentifyByIMO()) {
			data.addAll(this.searchByIMOs(criteria));
		}
	
		if(this.canIdentifyByEUCFR()) {
			data.addAll(this.searchByEUCFRs(criteria));
		}
	
		if(this.canIdentifyByName()) {
			VesselMetadataIdentificationCriteria updatedCriteria = SerializationHelper.clone(criteria);
			
			if(criteria.getNames() != null) {
				List<String> names = new ListSet<String>();
				
				String simplified;
				
				for(String name : criteria.getNames()) {
					if(name != null) {
						names.add(name);
						
						simplified = this._vesselNamesSimplifier.process(name);
						
						if(simplified != null && !simplified.equalsIgnoreCase(name))
							names.add(simplified);
					}
					
					updatedCriteria.setNames(names.toArray(new String[names.size()]));
				}
			}
			
			data.addAll(this.searchByNames(updatedCriteria));
		}
	
		if(this.canIdentifyByIRCS()) {
			data.addAll(this.searchByIRCSs(criteria));
		}
	
		if(this.canIdentifyByMMSI()) {
			data.addAll(this.searchByMMSIs(criteria));
		}
	
		if(this.canIdentifyBySourceID()) {
			data.addAll(this.searchByIdentifiers(criteria));
		}
	
		return new VesselInfringementsIdentificationResponse(this.normalize(this.filter(data)));
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#EUCFR)")
	abstract public Collection<VesselInfringementsIdentificationResponseData> doSearchByEUCFR(String EUCFR);

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#identifier.name + #identifier.value)")
	abstract public Collection<VesselInfringementsIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier);

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#MMSI)")
	abstract public Collection<VesselInfringementsIdentificationResponseData> doSearchByMMSI(String MMSI);

	final protected Collection<VesselInfringementsIdentificationResponseData> searchByEUCFRs(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselInfringementsIdentificationResponseData> results = new ListSet<VesselInfringementsIdentificationResponseData>();
	
		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getEUCFRs())) {
			this._log.info("{}: searching infringements by EU CFRs {}", this.getClass().getSimpleName(), criteria.getEUCFRs());
	
			for(String data : criteria.getEUCFRs()) {
				data = StringsHelper.trim(data);
	
				if(data != null) {
					try {
						this._log.info("{}: searching infringements by EU CFR {}", this.getClass().getSimpleName(), data);
	
						results = this.safeAdd(results, this.doSearchByEUCFR(data));
					} catch (Throwable t) {
						this._log.info("Unable to search infringements by EU CFR {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}
	
			int infringements = 0;
	
			for(VesselInfringementsIdentificationResponseData result : results)
				if(!result.isEmpty())
					infringements++;
	
			this._log.info("{}: {} infringements identified by EU CFRs {}", this.getClass().getSimpleName(), infringements, criteria.getEUCFRs());
		}
	
		return results;
	}

	final protected Collection<VesselInfringementsIdentificationResponseData> searchByIdentifiers(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselInfringementsIdentificationResponseData> results = new ListSet<VesselInfringementsIdentificationResponseData>();
	
		if(criteria != null && !criteria.isEmpty() &&
		   !this.isEmpty(criteria.getSourceIdentifiers()) &&
		   !this.isEmpty(this.getSourceIDs(criteria))) {
			String identifierType, identifier;
			for(NameValuePair data : this.getSourceIDs(criteria)) {
				identifierType = StringsHelper.trim(data.getName());
				identifier = StringsHelper.trim((String)data.getValue());
	
				if(identifierType != null && identifier != null) {
					try {
						this._log.info("{}: searching infringements by identifier {}", this.getClass().getSimpleName(), data);
	
						results = this.safeAdd(results, this.doSearchByIdentifier(data));
					} catch (Throwable t) {
						this._log.info("Unable to search infringements by identifier {}: {} [ {} ]",
										data.getName() + " #" + data.getValue(),
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}
	
			int infringements = 0;
	
			for(VesselInfringementsIdentificationResponseData result : results)
				if(!result.isEmpty())
					infringements++;
	
			this._log.info("{}: {} infringements identified by identifiers {}", this.getClass().getSimpleName(), infringements, this.getSourceIDs(criteria));
		}
	
		return results;
	}

	final protected Collection<VesselInfringementsIdentificationResponseData> searchByMMSIs(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselInfringementsIdentificationResponseData> results = new ListSet<VesselInfringementsIdentificationResponseData>();
	
		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getMMSIs())) {
			this._log.info("{}: searching infringements by MMSIs {}", this.getClass().getSimpleName(), criteria.getMMSIs());
	
			for(String data : criteria.getMMSIs()) {
				data = StringsHelper.trim(data);
	
				if(data != null) {
					try {
						this._log.info("{}: searching infringements by MMSI {}", this.getClass().getSimpleName(), data);
	
						results = this.safeAdd(results, this.doSearchByMMSI(data));
					} catch (Throwable t) {
						this._log.info("Unable to search infringements by MMSI {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}
	
			int infringements = 0;
	
			for(VesselInfringementsIdentificationResponseData result : results)
				if(!result.isEmpty())
					infringements++;
	
			this._log.info("{}: {} infringements identified by MMSIs {}", this.getClass().getSimpleName(), infringements, criteria.getMMSIs());
		}
	
		return results;
	}
}
