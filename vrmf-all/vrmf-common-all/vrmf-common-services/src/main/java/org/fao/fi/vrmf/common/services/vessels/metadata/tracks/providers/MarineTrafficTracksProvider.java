/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.tracks.providers;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.VesselTracksProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.support.VesselTracks;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.support.VesselTracksIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 May 2013
 * @deprecated Changes in remote API make this provider basically useless
 */
@Deprecated
//@Singleton
//@Named("vrmf.common.vessels.metadata.tracks.provider.marinetraffic")
public class MarineTrafficTracksProvider extends VesselTracksProvider {
	static final private String MARINE_TRAFFIC_AIS_API_URL		   = "http://mob0.marinetraffic.com/ais/gettrackxml/mmsi:{query}";
	static final private String MARINE_TRAFFIC_AIS_HISTORY_API_URL = "http://www.marinetraffic.com/map/get{service}xml/mmsi:{query}/date:{atDate}/id:null";
	static final private String MARINE_TRAFFIC_SEARCH_BY_TEXT_URL  = "http://www.marinetraffic.com/en/ais/index/ships/all/shipname:{query}/ship_type:2";
	static final private String MARINE_TRAFFIC_VESSEL_DETAILS_URL  = "http://www.marinetraffic.com/en/ais/details/ships/{id}";
	
	static final private String MARINE_TRAFFIC_POS_XPATH = "/TRACK/POS";
	static final private String MARINE_TRAFFIC_LON_XPATH = "@LON";
	static final private String MARINE_TRAFFIC_LAT_XPATH = "@LAT";
	static final private String MARINE_TRAFFIC_SPEED_XPATH = "@SPEED";
	static final private String MARINE_TRAFFIC_COURSE_XPATH = "@COURSE";
	static final private String MARINE_TRAFFIC_TIMESTAMP_XPATH = "@TIMESTAMP";

	private final XPathExpression MARINE_TRAFFIC_POS_XPATH_EXPR;
	private final XPathExpression MARINE_TRAFFIC_LON_XPATH_EXPR;
	private final XPathExpression MARINE_TRAFFIC_LAT_XPATH_EXPR;
	private final XPathExpression MARINE_TRAFFIC_SPEED_XPATH_EXPR;
	private final XPathExpression MARINE_TRAFFIC_COURSE_XPATH_EXPR;
	private final XPathExpression MARINE_TRAFFIC_TIMESTAMP_XPATH_EXPR;

	public MarineTrafficTracksProvider() throws Throwable {
		this(10000);
	}

	public MarineTrafficTracksProvider(int timeout) throws Throwable {
		super(timeout);

		this.MARINE_TRAFFIC_POS_XPATH_EXPR = this._xPathFactory.newXPath().compile(MARINE_TRAFFIC_POS_XPATH);
		this.MARINE_TRAFFIC_LON_XPATH_EXPR = this._xPathFactory.newXPath().compile(MARINE_TRAFFIC_LON_XPATH);
		this.MARINE_TRAFFIC_LAT_XPATH_EXPR = this._xPathFactory.newXPath().compile(MARINE_TRAFFIC_LAT_XPATH);
		this.MARINE_TRAFFIC_SPEED_XPATH_EXPR = this._xPathFactory.newXPath().compile(MARINE_TRAFFIC_SPEED_XPATH);
		this.MARINE_TRAFFIC_COURSE_XPATH_EXPR = this._xPathFactory.newXPath().compile(MARINE_TRAFFIC_COURSE_XPATH);
		this.MARINE_TRAFFIC_TIMESTAMP_XPATH_EXPR = this._xPathFactory.newXPath().compile(MARINE_TRAFFIC_TIMESTAMP_XPATH);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#doCheckAvailability(int)
	 */
	@Override
	protected boolean doCheckAvailability(int timeout) {
		try {
			HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(MARINE_TRAFFIC_AIS_API_URL.replace("{query}", "123456789"), this.forgeRequestHeaders(), timeout);

			return response != null &&
				   response.getResponseCode() <= 399 &&
				   response.getContentType() != null &&
				   response.getContentType().startsWith("text/xml") &&
				   response.getContentLength() > 0;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}
	
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#field + #text)")
	private Collection<VesselTracksIdentificationResponseData> searchByText(String field, String text) {
		try {
			Collection<VesselTracksIdentificationResponseData> identified = new ListSet<VesselTracksIdentificationResponseData>();
			
			text = StringsHelper.trim(text);
			
			if(text != null) {
				Collection<NameValuePair> idAndName = this.doSearchByText(field, text);

				if(idAndName != null) {
					Set<NameValuePair> details;
					Collection<VesselTracksIdentificationResponseData> trackData;
					
					for(NameValuePair data : idAndName) {
						details = this.extractDataFromMarineTrafficPage(data, field, text);
						
						if(details != null && !details.isEmpty()) {
							trackData = this.internalDoSearchByDetails(field, text, details);
							
							if(trackData != null)
								identified.addAll(trackData);
						}	
					}
				}
			}
			
			return identified.isEmpty() ? null : identified;
		} catch (Throwable t) {
			this._log.warn("Unable to retrieve MarineTraffic track data by {} {}: {} [ {} ]", field, text, t.getClass(), t.getMessage());
		}

		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.tracks.VesselPositionsProvider#doSearchByIMO(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.positions", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IMO)")
	public Collection<VesselTracksIdentificationResponseData> doSearchByIMO(String IMO) {
		try {
			return this.searchByText("IMO",  IMO);
		} catch (Throwable t) {
			this._log.warn("Unable to retrieve MarineTraffic track data by {} {}: {} [ {} ]", "IMO", IMO, t.getClass(), t.getMessage());
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.tracks.VesselPositionsProvider#doSearchByEUCFR(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.positions", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#EUCFR)")
	public Collection<VesselTracksIdentificationResponseData> doSearchByEUCFR(String EUCFR) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.tracks.VesselPositionsProvider#doSearchByName(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.positions", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#name)")
	public Collection<VesselTracksIdentificationResponseData> doSearchByName(String name) {
		try {
			return this.searchByText("NAME",  name);
		} catch (Throwable t) {
			this._log.warn("Unable to retrieve MarineTraffic track data by {} {}: {} [ {} ]", "NAME", name, t.getClass(), t.getMessage());
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.tracks.VesselPositionsProvider#doSearchByIRCS(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.positions", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IRCS)")
	public Collection<VesselTracksIdentificationResponseData> doSearchByIRCS(String IRCS) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.tracks.VesselPositionsProvider#doSearchByMMSI(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.positions", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#MMSI)")
	public Collection<VesselTracksIdentificationResponseData> doSearchByMMSI(String MMSI) {
		try {
			return this.searchByText("MMSI",  MMSI);
		} catch (Throwable t) {
			this._log.warn("Unable to retrieve MarineTraffic track data by {} {}: {} [ {} ]", "MMSI", MMSI, t.getClass(), t.getMessage());
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.tracks.VesselPositionsProvider#doSearchByIdentifier(org.fao.vrmf.core.impl.design.patterns.pair.NameValuePair)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.positions", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#identifier.name + #identifier.value)")
	public Collection<VesselTracksIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByEUCFR()
	 */
	@Override
	protected boolean canIdentifyByEUCFR() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByIMO()
	 */
	@Override
	protected boolean canIdentifyByIMO() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByIRCS()
	 */
	@Override
	protected boolean canIdentifyByIRCS() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByMMSI()
	 */
	@Override
	protected boolean canIdentifyByMMSI() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByName()
	 */
	@Override
	protected boolean canIdentifyByName() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyBySourceID()
	 */
	@Override
	protected boolean canIdentifyBySourceID() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#getSpecificSources()
	 */
	@Override
	protected String[] getSpecificSources() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#isSourceSpecific()
	 */
	@Override
	protected boolean isSourceSpecific() {
		return false;
	}
	
	private Collection<NameValuePair> doSearchByText(String field, String text) {
		String URL = MARINE_TRAFFIC_SEARCH_BY_TEXT_URL.replace("{query}", text);

		Set<NameValuePair> IDAndNames = new HashSet<NameValuePair>();
		
		try {
			String result = this._httpHelper.fetchHTMLPageAsUTF8String(URL, this.getServiceTimeout());

			String idAndNamePattern = "/en/ais/details/ships/(\\d+)/vessel:[^>]+>([^<]+)";

			Matcher idAndNameMatcher = Pattern.compile(idAndNamePattern).matcher(result);

			int index = 0;

			String id;
			String name;
			while(idAndNameMatcher.find(index)) {
				id = idAndNameMatcher.group(1);
				name = idAndNameMatcher.group(2);

				if(id != null)
					IDAndNames.add(new NameValuePair(name, id));

				index = idAndNameMatcher.start(1);
			}

		} catch (Throwable t) {
			this._log.warn("Unable to retrieve MarineTraffic fishing vessels ID and MMSI by " + field + " {}: {} [ {} ]", text, t.getClass(), t.getMessage());
		}

		return IDAndNames;
	}
	
	private Set<NameValuePair> extractDataFromMarineTrafficPage(NameValuePair idAndName, String field, String text) throws Throwable {
		Set<NameValuePair> data = new HashSet<NameValuePair>();
		data.add(new NameValuePair("NAME", idAndName.getName()));
		
		if(!"NAME".equals(field))
			data.add(new NameValuePair(field, text));

		String URL = MARINE_TRAFFIC_VESSEL_DETAILS_URL.replace("{id}", (String)idAndName.getValue());
		
		String details = this._httpHelper.fetchHTMLPageAsUTF8String(URL, this.doGetCustomRequestHeaders(""), this.getServiceTimeout());
		
		details = details.replaceAll("\\n|\\r", "");
		
		String IMOPattern  = "IMO: <span class=\"details_data\">([^\\<]+)<";
		String MMSIPattern = "MMSI: <span class=\"details_data\">([^\\<]+)<";
		String IRCSPattern = "Call Sign: <span class=\"details_data\">([^\\<]+)<";
		
		Matcher IMOMatcher = Pattern.compile(IMOPattern).matcher(details);
		Matcher MMSIMatcher = Pattern.compile(MMSIPattern).matcher(details);
		Matcher IRCSMatcher = Pattern.compile(IRCSPattern).matcher(details);
		
		String IMO, MMSI, IRCS;
		
		IMO = MMSI = IRCS = null;
		
		if(IMOMatcher.find(0)) {
			IMO = IMOMatcher.group(1);
		}

		if(MMSIMatcher.find(0)) {
			MMSI = MMSIMatcher.group(1);
		}

		if(IRCSMatcher.find(0)) {
			IRCS = IRCSMatcher.group(1);
		}

		if(IMO != null && !"-".equals(IMO))
			data.add(new NameValuePair("IMO", IMO));
		
		if(MMSI != null && !MMSI.startsWith("-"))
			data.add(new NameValuePair("MMSI", MMSI));
		
		if(IRCS != null && !"-".equals(IRCS))
			data.add(new NameValuePair("IRCS", IRCS));
		
		return data;
	}

	private Collection<VesselTracksIdentificationResponseData> internalDoSearchByDetails(String field, String text, Set<NameValuePair> details) throws Throwable {
		String MMSI = null;
		
		List<String> additionalCriteria = new ArrayList<String>();
		
		for(NameValuePair detail : details) {
			if(detail.getName().equals("MMSI")) {
				MMSI = (String)detail.getValue();
			}
			
			additionalCriteria.add(detail.getName() + " = " + detail.getValue());
		}
		
		if(MMSI == null)
			return new ArrayList<VesselTracksIdentificationResponseData>();
		
		return this.getHistoricalMarineTrafficPosition(field + " = " + text, MMSI, CollectionsHelper.join(additionalCriteria, ", "));
	}

	@SuppressWarnings("unused")
	private Collection<VesselTracksIdentificationResponseData> getCurrentMarineTrafficPosition(String criteria, String MMSI, String additionalCriteria) throws Throwable {
		Collection<VesselTracksIdentificationResponseData> results = new ArrayList<VesselTracksIdentificationResponseData>();

		String response = this._httpHelper.fetchHTMLPageAsUTF8String(MARINE_TRAFFIC_AIS_API_URL.replaceAll("\\{query\\}", MMSI), this.forgeRequestHeaders(MMSI)).replaceAll("\\r|\\n|\\s{2,}", "");
		
		if("<TRACK></TRACK>".equals(response))
			return results;

		VesselTracksIdentificationResponseData data;

		Document xml = null;
		try {
			xml = this._documentBuilder.parse(new ByteArrayInputStream(response.getBytes("UTF-8")));

			data = new VesselTracksIdentificationResponseData(Boolean.TRUE,
												   "www.marinetraffic.com",
												   "http://www.marinetraffic.com/ais/default.aspx?mmsi=" + MMSI + "&centerx=0&centery=0&zoom=10&type_color=8",
												   criteria + ( additionalCriteria == null ? "" : " [ " + additionalCriteria + " ]"),
												   System.currentTimeMillis(),
												   new ArrayList<VesselTracks>());

			NodeList positions = (NodeList)MARINE_TRAFFIC_POS_XPATH_EXPR.evaluate(xml, XPathConstants.NODESET);

			Node node;
			VesselTracks position;
			
			long maxTimestamp = -1;
			
			long curTimestamp = -1;
			for(int p=0; p<positions.getLength(); p++) {
				node = positions.item(p);
				curTimestamp = this.getZuluTimestamp((String)MARINE_TRAFFIC_TIMESTAMP_XPATH_EXPR.evaluate(node, XPathConstants.STRING));
				
				position = new VesselTracks((String)MARINE_TRAFFIC_LAT_XPATH_EXPR.evaluate(node, XPathConstants.STRING),
											(String)MARINE_TRAFFIC_LON_XPATH_EXPR.evaluate(node, XPathConstants.STRING),
											curTimestamp,
											Double.parseDouble((String)MARINE_TRAFFIC_SPEED_XPATH_EXPR.evaluate(node, XPathConstants.STRING)),
											Double.parseDouble((String)MARINE_TRAFFIC_COURSE_XPATH_EXPR.evaluate(node, XPathConstants.STRING)));

				if(curTimestamp > maxTimestamp)
					maxTimestamp = curTimestamp;
				
				data.getPositions().add(position);
			}
			
			data.setTimestamp(maxTimestamp);

			results.add(data);
		} catch (Throwable t) {
			this._log.warn("Unable to parse XML response '" + response + "'", t);
		}

		return results;
	}

	private Collection<VesselTracksIdentificationResponseData> getHistoricalMarineTrafficPosition(String criteria, String MMSI, String additionalCriteria) throws Throwable {
		Collection<VesselTracksIdentificationResponseData> results = new ArrayList<VesselTracksIdentificationResponseData>();

		String URL = MARINE_TRAFFIC_AIS_HISTORY_API_URL.replaceAll("\\{service\\}", "track").replaceAll("\\{atDate\\}", "").replaceAll("\\{query\\}", MMSI);
		String response = this._httpHelper.fetchHTMLPageAsUTF8String(URL).replaceAll("\\r|\\n|\\s{2,}", "");
		
		if("<TRACK></TRACK>".equals(response))
			return results;
		
		VesselTracksIdentificationResponseData data;

		Document xml = null;
		try {
			xml = this._documentBuilder.parse(new ByteArrayInputStream(response.getBytes("UTF-8")));

			data = new VesselTracksIdentificationResponseData(Boolean.TRUE,
															  "www.marinetraffic.com",
															  "http://www.marinetraffic.com/ais/default.aspx?oldmmsi=" + MMSI + "&zoom=10&olddate=lastknown#",
															  criteria + ( additionalCriteria == null ? "" : " [ " + additionalCriteria + " ]" ),
															  System.currentTimeMillis(),
//															  , " [ LAST KNOWN TRACK @ " + this._yyyyMMddConverter.format(new Date()) + " ] ",
															  new ArrayList<VesselTracks>());

			NodeList positions = (NodeList)MARINE_TRAFFIC_POS_XPATH_EXPR.evaluate(xml, XPathConstants.NODESET);

			Node node;
			VesselTracks position;
			for(int p=0; p<positions.getLength(); p++) {
				node = positions.item(p);
				position = new VesselTracks((String)MARINE_TRAFFIC_LAT_XPATH_EXPR.evaluate(node, XPathConstants.STRING),
											(String)MARINE_TRAFFIC_LON_XPATH_EXPR.evaluate(node, XPathConstants.STRING),
											this.getZuluTimestamp((String)MARINE_TRAFFIC_TIMESTAMP_XPATH_EXPR.evaluate(node, XPathConstants.STRING)),
											Double.parseDouble((String)MARINE_TRAFFIC_SPEED_XPATH_EXPR.evaluate(node, XPathConstants.STRING)),
											Double.parseDouble((String)MARINE_TRAFFIC_COURSE_XPATH_EXPR.evaluate(node, XPathConstants.STRING)));

				data.getPositions().add(position);
			}

			results.add(data);
		} catch (Throwable t) {
			this._log.warn("Unable to parse XML response '{}'. Reason: {} '{}'", response, t.getClass().getSimpleName(), t.getMessage());
		}

		return results;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doGetCustomRequestHeaders(org.fao.fi.vrmf.common.core.services.vessels.metadata.VesselMetadataIdentificationCriteria)
	 */
	@Override
	protected Collection<NameValuePair> doGetCustomRequestHeaders(Object MMSI) {
		Collection<NameValuePair> headers = this.getCommonRequestHeaders();
		headers.add(new NameValuePair("Host", "www.marinetraffic.com"));
		headers.add(new NameValuePair("Referer", "http://www.marinetraffic.com/ais/default.aspx?mmsi=" + ( MMSI == null ? "" : MMSI ) + "&centerx=-" + Math.random() * 180.0 + "&centery=" + Math.random() * 90.0 + "&zoom=10&type_color=7"));

		return headers;
	}
}
