/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathFactory;

import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.io.MD5Helper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.sh.utility.services.impl.AbstractAsynchronousService;
import org.fao.fi.sh.utility.services.spi.AsynchronousService;
import org.fao.fi.vrmf.common.tools.lexical.processors.queue.impl.VesselNameSimplifier;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 May 2013
 */
abstract public class AbstractVesselMetadataProvider<REQ extends VesselMetadataIdentificationRequest, RESULT extends VesselMetadataIdentificationResult, RES extends VesselMetadataIdentificationResponse<RESULT>> extends AbstractAsynchronousService<REQ, RES> {
	@Inject protected HTTPHelper _httpHelper;
	@Inject protected VesselNameSimplifier _vesselNamesSimplifier;

	final protected DocumentBuilderFactory _builderFactory;
	final protected DocumentBuilder _documentBuilder;

	final protected XPathFactory _xPathFactory;
	
	final protected Collection<RESULT> EMPTY_RESULTS = new ListSet<RESULT>();

	final protected Collection<NameValuePair> NO_CUSTOM_HEADERS = null;

	protected SimpleDateFormat _zuluTimezoneConverter;
	protected SimpleDateFormat _yyyyMMddConverter;

	public AbstractVesselMetadataProvider() throws Throwable {
		this(AsynchronousService.NO_TIMEOUT);
	}

	/**
	 * Class constructor
	 *
	 * @param timeout
	 * @throws Throwable
	 */
	public AbstractVesselMetadataProvider(int timeout) throws Throwable {
		super(timeout);

		this._builderFactory = DocumentBuilderFactory.newInstance();
		this._builderFactory.setNamespaceAware(false);

		this._documentBuilder =  this._builderFactory.newDocumentBuilder();

		this._xPathFactory = XPathFactory.newInstance();
		
		this._zuluTimezoneConverter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		this._zuluTimezoneConverter.setTimeZone(TimeZone.getTimeZone("UTC"));

		this._yyyyMMddConverter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	}

	/**
	 * @return the 'httpHelper' value
	 */
	final public HTTPHelper getHttpHelper() {
		return this._httpHelper;
	}

	/**
	 * @param httpHelper the 'httpHelper' value to set
	 */
	final public void setHttpHelper(HTTPHelper httpHelper) {
		this._httpHelper = httpHelper;
	}

	/**
	 * @return the 'vesselNamesSimplifier' value
	 */
	final public VesselNameSimplifier getVesselNamesSimplifier() {
		return this._vesselNamesSimplifier;
	}

	/**
	 * @param vesselNamesSimplifier the 'vesselNamesSimplifier' value to set
	 */
	final public void setVesselNamesSimplifier(VesselNameSimplifier vesselNamesSimplifier) {
		this._vesselNamesSimplifier = vesselNamesSimplifier;
	}

	abstract protected boolean canIdentifyByEUCFR();
	abstract protected boolean canIdentifyByIMO();
	abstract protected boolean canIdentifyByIRCS();
	abstract protected boolean canIdentifyByMMSI();
	abstract protected boolean canIdentifyByName();
	abstract protected boolean canIdentifyBySourceID();

	abstract protected boolean doCheckAvailability(int timeout);

	abstract protected String[] getSpecificSources();

	final protected String buildCacheKey(VesselMetadataIdentificationCriteria criteria) {
		String dataHash = null;

		try {
			dataHash = criteria == null ? "NULL" : MD5Helper.digest(criteria.toString());
		} catch(Exception e) {
			dataHash = String.valueOf(criteria == null ? 0 : criteria.hashCode());

			this._log.error("Unable to build cache key for {}: {} [ {} ]. Using object hash code ({}) instead...",
							criteria,
							e.getClass().getSimpleName(),
							e.getMessage(),
							dataHash);		}

		String key = "searchPictures_" + this.getClass().getSimpleName() + "_" + dataHash;

		this._log.debug("Returning {} as {} cache key for {}", key, this.getClass().getSimpleName(), criteria);

		return key;
	}

	final protected String buildSpecificCriteriaCacheKey(String criteriaValue) {
		String key = this.getClass().getSimpleName() + "_" + criteriaValue;

		this._log.info("Returning {} as {} cache key for {}", key, this.getClass().getSimpleName(), criteriaValue);

		return key;
	}

	@Override
	final public boolean checkAvailability() {
		return this.checkAvailability(this.getServiceTimeout());
	}

	@Override
	final public boolean checkAvailability(int timeout) {
		try {
			return this.doCheckAvailability(timeout);
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]. Assuming service is unavailable...",
							this.getClass().getSimpleName(),
							t.getClass().getSimpleName(),
							t.getMessage());

			return false;
		}
	}

	final protected String decodeTerm(String term) {
		try {
			return term == null ? "" : URLDecoder.decode(term, "UTF-8");
		} catch (UnsupportedEncodingException UEe) {
			return term;
		}
	}

	final protected String encodeTerm(String term) {
		try {
			return term == null ? "" : URLEncoder.encode(term, "UTF-8");
		} catch (UnsupportedEncodingException UEe) {
			return term;
		}
	}

	final protected Collection<RESULT> filter(Collection<RESULT> toFilter) {
		if(toFilter == null || toFilter.isEmpty())
			return EMPTY_RESULTS;

		return toFilter.stream().filter(R -> R != null && !R.isEmpty()).collect(Collectors.toList());
	}

	final protected NameValuePair[] getSourceIDs(VesselMetadataIdentificationCriteria criteria) {
		String[] sources = this.getSpecificSources();

		if(!this.canIdentifyBySourceID())
			throw new UnsupportedOperationException("This metadata provider cannot identify metadata by specific source ID");

		if(criteria == null || criteria.isEmpty())
			return null;

		NameValuePair[] filteredIDs = criteria.findSourceIdentifiers(sources);

		if(this.isEmpty(filteredIDs))
			return null;

		return filteredIDs;
	}

	final protected boolean isEmpty(Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	final protected boolean isEmpty(Object[] array) {
		return array == null || array.length == 0;
	}

	abstract protected boolean isSourceSpecific();

	final protected Collection<NameValuePair> forgeRequestHeaders() {
		return this.forgeRequestHeaders(null);
	}
	
	final protected Collection<NameValuePair> forgeRequestHeaders(Object params) {
		Collection<NameValuePair> allHeaders = this.getCommonRequestHeaders();
		Collection<NameValuePair> customHeaders = this.doGetCustomRequestHeaders(params);
		
		if(customHeaders != null)
			allHeaders.addAll(customHeaders);
		
		return allHeaders;
	}
	
	final protected Collection<NameValuePair> getCommonRequestHeaders() {
		Collection<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new NameValuePair("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"));
		headers.add(new NameValuePair("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3"));
		headers.add(new NameValuePair("Accept-Language", "en,en-GB;q=0.8,en;q=0.6,en-US;q=0.4"));
		headers.add(new NameValuePair("Connection", "keep-alive"));
		headers.add(new NameValuePair("User-Agent", "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4"));

		return headers;
	}

	abstract protected Collection<NameValuePair> doGetCustomRequestHeaders(Object params);

	final protected <S extends VesselMetadataIdentificationResult> Collection<S> normalize(Collection<S> toNormalize) {
		if(toNormalize == null || toNormalize.isEmpty())
			return null;

		boolean isEmpty = true;

		for(S data : toNormalize)
			isEmpty &= data == null || data.isEmpty();

		return isEmpty ? null : toNormalize;
	}

	final protected <S extends Serializable> Collection<S> safeAdd(Collection<S> target, S data) {
		if(target != null && data != null)
			target.add(data);

		return target;
	}

	final protected <S extends Serializable> Collection<S> safeAdd(Collection<S> target, Collection<S> data) {
		if(target != null && data != null)
			for(S entry : data)
				this.safeAdd(target, entry);

		return target;
	}
}