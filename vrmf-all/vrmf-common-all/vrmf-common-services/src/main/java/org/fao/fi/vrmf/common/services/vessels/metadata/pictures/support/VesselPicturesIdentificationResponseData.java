/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Collection;

import org.fao.fi.sh.utility.core.helpers.singletons.io.URLHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.VesselMetadataIdentificationResult;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2011
 */
public class VesselPicturesIdentificationResponseData extends VesselMetadataIdentificationResult {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4182970539781235518L;
	
	static final public boolean PUBLIC  = true;
	static final public boolean PRIVATE = false;
	
	private boolean _isPublic;
	private String _sourceID;
	private String _source;
	private String _thumbsURL;
	private String _fullURL;
	private String _criteria;
	private String _parentURL;
	
	private Collection<String> _IDs = new ListSet<String>();
	
	/**
	 * Class constructor
	 *
	 * @param source
	 * @param thumbsURL
	 * @param fullURL
	 * @param criteria
	 * @param iDs
	 */
	public VesselPicturesIdentificationResponseData(boolean isPublic, String sourceID, String source, String thumbsURL, String fullURL, String criteria, String parentURL, Collection<String> iDs) {
		super();
		this._isPublic = isPublic;
		this._sourceID = sourceID;
		this._source = source;
		this._thumbsURL = thumbsURL;
		this._fullURL = fullURL;
		this._criteria = StringsHelper.trim(criteria);
		this._parentURL = parentURL; 
		this._IDs = iDs;
		
		if(this._criteria != null)
			try {
				this._criteria = URLHelper.unescape(URLDecoder.decode(this._criteria, "UTF-8"));
			} catch (UnsupportedEncodingException UEe) {
				;
			}
	}
	
	/**
	 * @return the 'isPublic' value
	 */
	public Boolean getIsPublic() {
		return this._isPublic;
	}

	/**
	 * @param isPublic the 'isPublic' value to set
	 */
	public void setIsPublic(Boolean isPublic) {
		this._isPublic = isPublic;
	}
	
	/**
	 * @return the 'sourceID' value
	 */
	public String getSourceID() {
		return this._sourceID;
	}

	/**
	 * @param sourceID the 'sourceID' value to set
	 */
	public void setSourceID(String sourceID) {
		this._sourceID = sourceID;
	}

	/**
	 * @return the 'source' value
	 */
	public String getSource() {
		return this._source;
	}

	/**
	 * @param source the 'source' value to set
	 */
	public void setSource(String source) {
		this._source = source;
	}

	/**
	 * @return the 'thumbsURL' value
	 */
	public String getThumbsURL() {
		return this._thumbsURL;
	}

	/**
	 * @param thumbsURL the 'thumbsURL' value to set
	 */
	public void setThumbsURL(String thumbsURL) {
		this._thumbsURL = thumbsURL;
	}

	/**
	 * @return the 'fullURL' value
	 */
	public String getFullURL() {
		return this._fullURL;
	}

	/**
	 * @param fullURL the 'fullURL' value to set
	 */
	public void setFullURL(String fullURL) {
		this._fullURL = fullURL;
	}

	/**
	 * @return the 'criteria' value
	 */
	public String getCriteria() {
		return this._criteria;
	}

	/**
	 * @param criteria the 'criteria' value to set
	 */
	public void setCriteria(String criteria) {
		this._criteria = criteria;
	}
	
	/**
	 * @return the 'parentURL' value
	 */
	public String getParentURL() {
		return this._parentURL;
	}

	/**
	 * @param parentURL the 'parentURL' value to set
	 */
	public void setParentURL(String parentURL) {
		this._parentURL = parentURL;
	}

	/**
	 * @return the 'iDs' value
	 */
	public Collection<String> getIDs() {
		return this._IDs;
	}

	/**
	 * @param iDs the 'iDs' value to set
	 */
	public void setIDs(Collection<String> iDs) {
		this._IDs = iDs;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.VesselMetadataIdentificationResponse#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return this._IDs == null || this._IDs.isEmpty();
	}
}