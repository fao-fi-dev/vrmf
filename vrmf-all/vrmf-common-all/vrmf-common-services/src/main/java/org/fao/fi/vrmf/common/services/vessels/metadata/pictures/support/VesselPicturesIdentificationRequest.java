/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support;

import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.vrmf.common.services.vessels.metadata.VesselMetadataIdentificationRequest;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2013
 */
public class VesselPicturesIdentificationRequest extends VesselMetadataIdentificationRequest {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 505208724482911225L;

	/**
	 * Class constructor
	 *
	 * @param iMOs
	 * @param eUCFRs
	 * @param names
	 * @param iRCSs
	 * @param mMSIs
	 * @param sourceIdentifiers
	 */
	public VesselPicturesIdentificationRequest(String[] iMOs, String[] eUCFRs, String[] names, String[] iRCSs, String[] mMSIs, NameValuePair[] sourceIdentifiers) {
		super(iMOs, eUCFRs, names, iRCSs, mMSIs, sourceIdentifiers);
	}
}