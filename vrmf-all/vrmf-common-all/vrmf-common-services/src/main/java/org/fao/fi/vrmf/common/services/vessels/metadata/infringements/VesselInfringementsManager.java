/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.infringements;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.services.impl.invokers.AbstractAsynchronousServiceInvokerAndManager;
import org.fao.fi.vrmf.common.services.vessels.metadata.infringements.support.VesselInfringementsIdentificationRequest;
import org.fao.fi.vrmf.common.services.vessels.metadata.infringements.support.VesselInfringementsIdentificationResponse;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 May 2012
 */
@Named
@Singleton
public class VesselInfringementsManager extends AbstractAsynchronousServiceInvokerAndManager<VesselInfringementsIdentificationRequest, VesselInfringementsIdentificationResponse, VesselInfringementsProvider> {
	/**
	 * Class constructor
	 */
	public VesselInfringementsManager() throws Throwable {
		super();
	}
}