/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.support.MaritimeConnectorPicturesSearchResult;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

@Singleton
@Named("vrmf.common.vessels.metadata.pictures.provider.maritimeconnector")
public class MaritimeConnectorPicturesProvider extends VesselPicturesProvider {
	static final private String MARITIME_CONNECTOR_PING_URL = "http://maritime-connector.com";
	static final private String MARITIME_CONNECTOR_SEARCH_BY_IMO_URL = "http://maritime-connector.com/ship/{query}/";
	
	public MaritimeConnectorPicturesProvider() throws Throwable {
		super(5000);
	}

	@Override
	protected boolean doCheckAvailability(int timeout) {
		try {
			HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(MARITIME_CONNECTOR_PING_URL, timeout);

			return response != null &&
				   response.getResponseCode() <= 299 &&
				   response.getContentType() != null &&
				   response.getContentType().startsWith("text/html") &&
				   response.getContentLength() > 0;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}

	@Override
	protected boolean canIdentifyByIMO() {
		return true;
	}

	@Override
	protected boolean canIdentifyByEUCFR() {
		return false;
	}

	@Override
	protected boolean canIdentifyByName() {
		return false;
	}

	@Override
	protected boolean canIdentifyByIRCS() {
		return false;
	}

	@Override
	protected boolean canIdentifyByMMSI() {
		return false;
	}

	@Override
	protected boolean canIdentifyBySourceID() {
		return false;
	}

	@Override
	protected boolean isSourceSpecific() {
		return false;
	}

	@Override
	protected String[] getSpecificSources() {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#IMO)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIMO(String IMO) {
		Collection<String> pictureIDs = new ListSet<String>();
		String URL = MARITIME_CONNECTOR_SEARCH_BY_IMO_URL.replace("{query}", IMO);

		Collection<VesselPicturesIdentificationResponseData> results = null;

		try {
			final String result = this._httpHelper.fetchHTMLPageAsUTF8String(URL, this.getServiceTimeout());

			pictureIDs.addAll(this.getPictureIDsInPage(result));

			results = new ArrayList<VesselPicturesIdentificationResponseData>();

			results.add(new MaritimeConnectorPicturesSearchResult("IMO = " + IMO, URL, pictureIDs));
		} catch (Throwable t) {
			this._log.warn("Unable to retrieve MarineTraffic pictures by IMO {}: {} [ {} ]", IMO, t.getClass(), t.getMessage());
		}

		return results;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#EUCFR)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByEUCFR(String EUCFR) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#name)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByName(String name) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#IRCS)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIRCS(String IRCS) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#MMSI)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByMMSI(String MMSI) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#identifier.name + #identifier.value)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier) {
		return null;
	}

	protected Collection<String> getPictureIDsInPage(String HTML) {
		Collection<String> pictureIDs = new ListSet<String>();
		
		String pattern = "a id=\\\"avatar\\\" href=\\\"http:\\/\\/maritime-connector\\.com\\/ships_uploads\\/([^\\\"]+)";
		Matcher matcher = Pattern.compile(pattern).matcher(HTML);

		int index = 0;

		while(matcher.find(index)) {
			pictureIDs.add(matcher.group(1));

			index = matcher.start(1);
		}
		
		return pictureIDs;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doGetCustomRequestHeaders(java.lang.Object)
	 */
	@Override
	protected Collection<NameValuePair> doGetCustomRequestHeaders(Object params) {
		return NO_CUSTOM_HEADERS;
	}
}