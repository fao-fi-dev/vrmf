/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.support.ShipSpottingPicturesSearchResult;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

@Singleton
@Named("vrmf.common.vessels.metadata.pictures.provider.shipspotting")
public class ShipspottingPicturesProvider extends VesselPicturesProvider {
	static final private String SHIPSPOTTING_SEARCH_FISHING_VESSEL_OPTION = "14";

	static final private String SHIPSPOTTING_SEARCH_EXACT_NAME_OPTION 	 = "1";

	@SuppressWarnings("unused")
	static final private String SHIPSPOTTING_SEARCH_NAME_BEGINS_OPTION 	 = "2";

	@SuppressWarnings("unused")
	static final private String SHIPSPOTTING_SEARCH_NAME_CONTAINS_OPTION = "3";

	static final private String SHIPSPOTTING_SEARCH_PAGE_URL = "http://www.shipspotting.com/gallery/search.php";

	static final private String SHIPSPOTTING_PING_URL = SHIPSPOTTING_SEARCH_PAGE_URL;

	static final private String SHIPSPOTTING_SEARCH_PAGE_BY_CRITERIA_URL = SHIPSPOTTING_SEARCH_PAGE_URL + "?page_limit=192&{param}={query}";
	static final private String SHIPSPOTTING_SEARCH_PAGE_BY_CRITERIA_AND_TYPE_URL = SHIPSPOTTING_SEARCH_PAGE_BY_CRITERIA_URL + "&search_category_1=" + SHIPSPOTTING_SEARCH_FISHING_VESSEL_OPTION + "&search_cat1childs=on";

	public ShipspottingPicturesProvider() throws Throwable {
		super(6000);
	}

	@Override
	protected boolean doCheckAvailability(int timeout) {
		try {
			HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(SHIPSPOTTING_PING_URL, timeout);

			return response != null &&
				   response.getResponseCode() <= 299 &&
				   response.getContentType() != null &&
				   response.getContentType().startsWith("text/html") &&
				   response.getContentLength() > 0;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}

	@Override
	protected boolean canIdentifyByIMO() {
		return true;
	}

	@Override
	protected boolean canIdentifyByEUCFR() {
		return false;
	}

	@Override
	protected boolean canIdentifyByName() {
		return true;
	}

	@Override
	protected boolean canIdentifyByIRCS() {
		return true;
	}

	@Override
	protected boolean canIdentifyByMMSI() {
		return true;
	}

	@Override
	protected boolean canIdentifyBySourceID() {
		return false;
	}

	@Override
	protected boolean isSourceSpecific() {
		return false;
	}

	@Override
	protected String[] getSpecificSources() {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#IMO)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIMO(String IMO) {
		Collection<String> pictureIDs = new ListSet<String>();
		String URL = SHIPSPOTTING_SEARCH_PAGE_BY_CRITERIA_URL.replace("{param}", "search_imo").replace("{query}", IMO);

		Collection<VesselPicturesIdentificationResponseData> results = new ArrayList<VesselPicturesIdentificationResponseData>();

		results.add(new ShipSpottingPicturesSearchResult("IMO = " + IMO, URL, pictureIDs));

		try {
			final String result = this._httpHelper.fetchHTMLPageAsUTF8String(URL, this.getServiceTimeout());

			pictureIDs.addAll(this.getPictureIDsInPage(result));
		} catch (Throwable t) {
			this._log.warn("Unable to retrieve Shipspotting pictures by IMO {}: {} [ {} ]", IMO, t.getClass(), t.getMessage());
		}

		return results;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#EUCFR)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByEUCFR(String EUCFR) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#name)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByName(String name) {
		Collection<String> pictureIDs = new ListSet<String>();

		String URL = SHIPSPOTTING_SEARCH_PAGE_BY_CRITERIA_AND_TYPE_URL.replace("{param}", "search_title").replace("{query}", name) + "&search_title_option=" + SHIPSPOTTING_SEARCH_EXACT_NAME_OPTION;

		Collection<VesselPicturesIdentificationResponseData> results = new ArrayList<VesselPicturesIdentificationResponseData>();

		results.add(new ShipSpottingPicturesSearchResult("Name = " + name, URL, pictureIDs));

		try {
			final String result = this._httpHelper.fetchHTMLPageAsUTF8String(URL, this.getServiceTimeout());

			pictureIDs.addAll(this.getPictureIDsInPage(result));
		} catch (Throwable t) {
			this._log.warn("Unable to retrieve Shipspotting pictures by name {}: {} [ {} ]", name, t.getClass(), t.getMessage());
		}

		return results;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#IRCS)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIRCS(String IRCS) {
		Collection<String> pictureIDs = new ListSet<String>();
		String URL = SHIPSPOTTING_SEARCH_PAGE_BY_CRITERIA_URL.replace("{param}", "search_callsign").replace("{query}", IRCS);

		Collection<VesselPicturesIdentificationResponseData> results = new ArrayList<VesselPicturesIdentificationResponseData>();

		results.add(new ShipSpottingPicturesSearchResult("IRCS = " + IRCS, URL, pictureIDs));

		try {
			final String result = this._httpHelper.fetchHTMLPageAsUTF8String(URL, this.getServiceTimeout());

			pictureIDs.addAll(this.getPictureIDsInPage(result));
		} catch (Throwable t) {
			this._log.warn("Unable to retrieve Shipspotting pictures by IRCS {}: {} [ {} ]", IRCS, t.getClass(), t.getMessage());
		}

		return results;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#MMSI)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByMMSI(String MMSI) {
		Collection<String> pictureIDs = new ListSet<String>();
		String URL = SHIPSPOTTING_SEARCH_PAGE_BY_CRITERIA_URL.replace("{param}", "search_mmsi").replace("{query}", MMSI);

		Collection<VesselPicturesIdentificationResponseData> results = new ArrayList<VesselPicturesIdentificationResponseData>();

		results.add(new ShipSpottingPicturesSearchResult("MMSI = " + MMSI, URL, pictureIDs));

		try {
			final String result = this._httpHelper.fetchHTMLPageAsUTF8String(URL, this.getServiceTimeout());

			pictureIDs.addAll(this.getPictureIDsInPage(result));
		} catch (Throwable t) {
			this._log.warn("Unable to retrieve Shipspotting pictures by MMSI {}: {} [ {} ]", MMSI, t.getClass(), t.getMessage());
		}

		return results;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#identifier.name + #identifier.value)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier) {
		return null;
	}

	protected Collection<String> getPictureIDsInPage(String HTML) {
		Collection<String> pictureIDs = new ListSet<String>();

		String pattern = "small\\/(\\d)\\/(\\d)\\/(\\d)\\/(\\d+)\\.jpg";

		Matcher matcher = Pattern.compile(pattern).matcher(HTML);

		int index = 0;

		while(matcher.find(index)) {
			pictureIDs.add(matcher.group(1) + "/" + matcher.group(2) + "/" + matcher.group(3) + "/" + matcher.group(4) + ".jpg");

			index = matcher.start(1);
		}

		return pictureIDs;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doGetCustomRequestHeaders(java.lang.Object)
	 */
	@Override
	protected Collection<NameValuePair> doGetCustomRequestHeaders(Object params) {
		return NO_CUSTOM_HEADERS;
	}
}
