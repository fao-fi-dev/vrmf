/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.tracks;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;
import java.util.TimeZone;

import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.AbstractVesselMetadataProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.VesselMetadataIdentificationCriteria;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.support.VesselTracksIdentificationRequest;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.support.VesselTracksIdentificationResponse;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.support.VesselTracksIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2013
 */
abstract public class VesselTracksProvider extends AbstractVesselMetadataProvider<VesselTracksIdentificationRequest, VesselTracksIdentificationResponseData, VesselTracksIdentificationResponse> {
	static final protected String DEFAULT_METHODS_INVOCATION_CACHE_ID = "vrmf.common.services.vessels.tracks";

	/**
	 * Class constructor
	 *
	 * @throws Throwable
	 */
	public VesselTracksProvider() throws Throwable {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param timeout
	 * @throws Throwable
	 */
	public VesselTracksProvider(int timeout) throws Throwable {
		super(timeout);

		this._zuluTimezoneConverter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this._zuluTimezoneConverter.setTimeZone(TimeZone.getTimeZone("UTC"));

		this._yyyyMMddConverter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	}

	public long getZuluTimestamp(String date) throws Throwable {
		return this._zuluTimezoneConverter.parse(date).getTime();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.Service#invoke(org.fao.fi.vrmf.common.core.services.request.ServiceRequest)
	 */
	@Override
	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.serviceId + #request")
	public VesselTracksIdentificationResponse invoke(VesselTracksIdentificationRequest request) throws Exception {
		return this.searchPositions(request);
	}

	final protected Collection<VesselTracksIdentificationResponseData> searchByIMOs(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselTracksIdentificationResponseData> results = new ListSet<VesselTracksIdentificationResponseData>();

		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getIMOs())) {
			this._log.info("{}: searching positions by IMOs {}", this.getClass().getSimpleName(), criteria.getIMOs());

			for(String data : criteria.getIMOs()) {
				data = StringsHelper.trim(data);

				if(data != null) {
					try {
						this._log.info("{}: searching positions by IMO {}", this.getClass().getSimpleName(), data);

						results = this.safeAdd(results, this.doSearchByIMO(data));
					} catch (Throwable t) {
						this._log.info("Unable to search positions by IMO {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}

			int tracks = 0;

			for(VesselTracksIdentificationResponseData result : results)
				tracks += result.getPositions().size();

			this._log.info("{}: {} positions identified by IMOs {}", this.getClass().getSimpleName(), tracks, criteria.getIMOs());
		}

		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IMO)")
	abstract public Collection<VesselTracksIdentificationResponseData> doSearchByIMO(String IMO);

	final protected Collection<VesselTracksIdentificationResponseData> searchByEUCFRs(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselTracksIdentificationResponseData> results = new ListSet<VesselTracksIdentificationResponseData>();

		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getEUCFRs())) {
			this._log.info("{}: searching positions by EU CFRs {}", this.getClass().getSimpleName(), criteria.getEUCFRs());

			for(String data : criteria.getEUCFRs()) {
				data = StringsHelper.trim(data);

				if(data != null) {
					try {
						this._log.info("{}: searching positions by EU CFR {}", this.getClass().getSimpleName(), data);

						results = this.safeAdd(results, this.doSearchByEUCFR(data));
					} catch (Throwable t) {
						this._log.info("Unable to search positions by EU CFR {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}

			int pics = 0;

			for(VesselTracksIdentificationResponseData result : results)
				pics += result.getPositions().size();

			this._log.info("{}: {} positions identified by EU CFRs {}", this.getClass().getSimpleName(), pics, criteria.getEUCFRs());
		}

		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#EUCFR)")
	abstract public Collection<VesselTracksIdentificationResponseData> doSearchByEUCFR(String EUCFR);

	final protected Collection<VesselTracksIdentificationResponseData> searchByNames(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselTracksIdentificationResponseData> results = new ListSet<VesselTracksIdentificationResponseData>();

		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getNames())) {
			this._log.info("{}: searching positions by names {}", this.getClass().getSimpleName(), criteria.getNames());

			for(String data : criteria.getNames()) {
				data = StringsHelper.trim(data);

				if(data != null) {
					try {
						this._log.info("{}: searching positions by name {}", this.getClass().getSimpleName(), data);

						results = this.safeAdd(results, this.doSearchByName(data));
					} catch (Throwable t) {
						this._log.info("Unable to search positions by name {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}

			int pics = 0;

			for(VesselTracksIdentificationResponseData result : results)
				pics += result.getPositions().size();

			this._log.info("{}: {} positions identified by names {}", this.getClass().getSimpleName(), pics, criteria.getNames());
		}

		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#name)")
	abstract public Collection<VesselTracksIdentificationResponseData> doSearchByName(String name);

	final protected Collection<VesselTracksIdentificationResponseData> searchByIRCSs(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselTracksIdentificationResponseData> results = new ListSet<VesselTracksIdentificationResponseData>();

		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getIRCSs())) {
			this._log.info("{}: searching positions by IRCSs {}", this.getClass().getSimpleName(), criteria.getIRCSs());

			for(String data : criteria.getIRCSs()) {
				data = StringsHelper.trim(data);

				if(data != null) {
					try {
						this._log.info("{}: searching positions by IRCS {}", this.getClass().getSimpleName(), data);

						results = this.safeAdd(results, this.doSearchByIRCS(data));
					} catch (Throwable t) {
						this._log.info("Unable to search positions by IRCS {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}

			int pics = 0;

			for(VesselTracksIdentificationResponseData result : results)
				pics += result.getPositions().size();

			this._log.info("{}: {} positions identified by IRCSs {}", this.getClass().getSimpleName(), pics, criteria.getIRCSs());
		}

		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IRCS)")
	abstract public Collection<VesselTracksIdentificationResponseData> doSearchByIRCS(String IRCS);

	final protected Collection<VesselTracksIdentificationResponseData> searchByMMSIs(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselTracksIdentificationResponseData> results = new ListSet<VesselTracksIdentificationResponseData>();

		if(criteria != null && !criteria.isEmpty() && !this.isEmpty(criteria.getMMSIs())) {
			this._log.info("{}: searching positions by MMSIs {}", this.getClass().getSimpleName(), criteria.getMMSIs());

			for(String data : criteria.getMMSIs()) {
				data = StringsHelper.trim(data);

				if(data != null) {
					try {
						this._log.info("{}: searching positions by MMSI {}", this.getClass().getSimpleName(), data);

						results = this.safeAdd(results, this.doSearchByMMSI(data));
					} catch (Throwable t) {
						this._log.info("Unable to search positions by MMSI {}: {} [ {} ]",
										data,
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}

			int pics = 0;

			for(VesselTracksIdentificationResponseData result : results)
				pics += result.getPositions().size();

			this._log.info("{}: {} positions identified by MMSIs {}", this.getClass().getSimpleName(), pics, criteria.getMMSIs());
		}

		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#MMSI)")
	abstract public Collection<VesselTracksIdentificationResponseData> doSearchByMMSI(String MMSI);

	final protected Collection<VesselTracksIdentificationResponseData> searchByIdentifiers(VesselMetadataIdentificationCriteria criteria) {
		Collection<VesselTracksIdentificationResponseData> results = new ListSet<VesselTracksIdentificationResponseData>();

		if(criteria != null && !criteria.isEmpty() &&
		   !this.isEmpty(criteria.getSourceIdentifiers()) &&
		   !this.isEmpty(this.getSourceIDs(criteria))) {
			String identifierType, identifier;
			for(NameValuePair data : this.getSourceIDs(criteria)) {
				identifierType = StringsHelper.trim(data.getName());
				identifier = StringsHelper.trim((String)data.getValue());

				if(identifierType != null && identifier != null) {
					try {
						this._log.info("{}: searching positions by identifier {}", this.getClass().getSimpleName(), data);

						results = this.safeAdd(results, this.doSearchByIdentifier(data));
					} catch (Throwable t) {
						this._log.info("Unable to search positions by identifier {}: {} [ {} ]",
										data.getName() + " #" + data.getValue(),
										t.getClass().getSimpleName(),
										t.getMessage());
					}
				}
			}

			int pics = 0;

			for(VesselTracksIdentificationResponseData result : results)
				pics += result.getPositions().size();

			this._log.info("{}: {} positions identified by identifiers {}", this.getClass().getSimpleName(), pics, this.getSourceIDs(criteria));
		}

		return results;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#identifier.name + #identifier.value)")
	abstract public Collection<VesselTracksIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier);

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + #root.target.serviceId + #criteria")
	final public VesselTracksIdentificationResponse searchPositions(VesselMetadataIdentificationCriteria criteria) {
		List<VesselTracksIdentificationResponseData> data = new ListSet<VesselTracksIdentificationResponseData>();

		if(this.canIdentifyByIMO()) {
			data.addAll(this.searchByIMOs(criteria));
		}

		if(this.canIdentifyByEUCFR()) {
			data.addAll(this.searchByEUCFRs(criteria));
		}

		if(this.canIdentifyByName()) {
			VesselMetadataIdentificationCriteria updatedCriteria = SerializationHelper.clone(criteria);
			
			if(criteria.getNames() != null) {
				List<String> names = new ListSet<String>();
				
				String simplified;
				
				for(String name : criteria.getNames()) {
					if(name != null) {
						names.add(name);
						
						simplified = this._vesselNamesSimplifier.process(name);
						
						if(simplified != null && !simplified.equalsIgnoreCase(name))
							names.add(simplified);
					}
					
					updatedCriteria.setNames(names.toArray(new String[names.size()]));
				}
			}
			
			data.addAll(this.searchByNames(updatedCriteria));
		}

		if(this.canIdentifyByIRCS()) {
			data.addAll(this.searchByIRCSs(criteria));
		}

		if(this.canIdentifyByMMSI()) {
			data.addAll(this.searchByMMSIs(criteria));
		}

		if(this.canIdentifyBySourceID()) {
			data.addAll(this.searchByIdentifiers(criteria));
		}

		return new VesselTracksIdentificationResponse(this.normalize(this.filter(data)));
	}
}