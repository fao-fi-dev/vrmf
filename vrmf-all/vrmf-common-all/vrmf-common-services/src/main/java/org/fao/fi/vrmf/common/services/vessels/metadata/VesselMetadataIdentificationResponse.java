/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata;

import java.util.ArrayList;
import java.util.Collection;

import org.fao.fi.sh.utility.services.spi.response.ServiceResponse;
import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2013
 */
abstract public class VesselMetadataIdentificationResponse<RESULT extends VesselMetadataIdentificationResult> extends GenericData implements ServiceResponse {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6936025018299486471L;

	private Collection<RESULT> _results;

	/**
	 * Class constructor
	 *
	 * @param identificationResults
	 */
	public VesselMetadataIdentificationResponse(Collection<RESULT> identificationResults) {
		super();

		this._results = identificationResults;
	}

	/**
	 * Class constructor
	 */
	public VesselMetadataIdentificationResponse() {
		this(new ArrayList<RESULT>());
	}

	/**
	 * @return the 'results' value
	 */
	final public Collection<RESULT> getResults() {
		return this._results;
	}

	final public boolean isEmpty() {
		if(this._results == null || this._results.isEmpty())
			return true;

		for(RESULT data : this._results)
			if(data != null && !data.isEmpty())
				return false;

		return true;
	}
}