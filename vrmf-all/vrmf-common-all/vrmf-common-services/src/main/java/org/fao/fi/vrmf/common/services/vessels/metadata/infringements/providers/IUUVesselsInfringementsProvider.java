/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.infringements.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.infringements.VesselInfringementsProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.infringements.support.VesselInfringementsIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Sep 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Sep 2013
 */
@Singleton
@Named("vrmf.common.vessels.metadata.infringements.provider.iuuvessels")
public class IUUVesselsInfringementsProvider extends VesselInfringementsProvider {
	static final private String IUU_VESSELS_ORG_PING_URL = "http://iuu-vessels.org/iuu";
	static final private String IUU_VESSELS_ORG_SEARCH_API_URL = "http://iuu-vessels.org/iuu/php/showvessellist.php?searchTerm={query}&ccamlr=true&seafo=true&nafo=true&iccat=true&neafc=true&iattc=true&iotc=true&wcpfc=true&live=true&delist=true&column=vessel_name&dir=asc&group=3";
	static final private String IUU_VESSELS_ORG_DETAILS_API_URL = "http://iuu-vessels.org/iuu/php/showvesseldetails.php?uid={query}";
	
	/**
	 * Class constructor
	 *
	 * @throws Throwable
	 */
	public IUUVesselsInfringementsProvider() throws Throwable {
		this(3000);
	}

	/**
	 * Class constructor
	 *
	 * @param timeout
	 * @throws Throwable
	 */
	public IUUVesselsInfringementsProvider(int timeout) throws Throwable {
		super(timeout);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.infringements.VesselInfringementsProvider#doSearchByIMO(java.lang.String)
	 */
	@Override
	@Cacheable(value = DEFAULT_METHODS_INVOCATION_CACHE_ID, key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IMO)")
	public Collection<VesselInfringementsIdentificationResponseData> doSearchByIMO(String IMO) {
		return this.getDetails("IMO = " + IMO, this.identify(IMO));
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.infringements.VesselInfringementsProvider#doSearchByIRCS(java.lang.String)
	 */
	@Override
	@Cacheable(value = DEFAULT_METHODS_INVOCATION_CACHE_ID, key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IRCS)")
	public Collection<VesselInfringementsIdentificationResponseData> doSearchByIRCS(String IRCS) {
		return this.getDetails("IRCS = " + IRCS, this.identify(IRCS));
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.infringements.VesselInfringementsProvider#doSearchByName(java.lang.String)
	 */
	@Override
	@Cacheable(value = DEFAULT_METHODS_INVOCATION_CACHE_ID, key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#name)")
	public Collection<VesselInfringementsIdentificationResponseData> doSearchByName(String name) {
		return this.getDetails("NAME = " + name, this.identify(name));
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.infringements.VesselInfringementsProvider#doSearchByEUCFR(java.lang.String)
	 */
	@Override
	@Cacheable(value = DEFAULT_METHODS_INVOCATION_CACHE_ID, key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#EUCFR)")
	public Collection<VesselInfringementsIdentificationResponseData> doSearchByEUCFR(String EUCFR) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.infringements.VesselInfringementsProvider#doSearchByIdentifier(org.fao.vrmf.core.impl.design.patterns.pair.NameValuePair)
	 */
	@Override
	@Cacheable(value = DEFAULT_METHODS_INVOCATION_CACHE_ID, key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#identifier.name + #identifier.value)")
	public Collection<VesselInfringementsIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.infringements.VesselInfringementsProvider#doSearchByMMSI(java.lang.String)
	 */
	@Override
	@Cacheable(value = DEFAULT_METHODS_INVOCATION_CACHE_ID, key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#MMSI)")
	public Collection<VesselInfringementsIdentificationResponseData> doSearchByMMSI(String MMSI) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByEUCFR()
	 */
	@Override
	protected boolean canIdentifyByEUCFR() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByIMO()
	 */
	@Override
	protected boolean canIdentifyByIMO() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByIRCS()
	 */
	@Override
	protected boolean canIdentifyByIRCS() {
		return false; //Temporarily removed as it may lead to many false positives (see some spanish callsigns whose value can appear in vessel names: e.g. EDUS, ECKE etc.)
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByMMSI()
	 */
	@Override
	protected boolean canIdentifyByMMSI() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByName()
	 */
	@Override
	protected boolean canIdentifyByName() {
		return false; //Temporarily (?) removed as it may lead to many false positives (e.g. RED)
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyBySourceID()
	 */
	@Override
	protected boolean canIdentifyBySourceID() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doCheckAvailability(int)
	 */
	@Override
	protected boolean doCheckAvailability(int timeout) {
		try {
			HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(IUU_VESSELS_ORG_PING_URL, timeout);

			return response != null &&
				   response.getResponseCode() <= 299 &&
				   response.getContentType() != null &&
				   response.getContentType().startsWith("text/html") &&
				   response.getContentLength() > 0;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#getSpecificSources()
	 */
	@Override
	protected String[] getSpecificSources() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#isSourceSpecific()
	 */
	@Override
	protected boolean isSourceSpecific() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doGetCustomRequestHeaders(java.lang.Object)
	 */
	@Override
	protected Collection<NameValuePair> doGetCustomRequestHeaders(Object params) {
		Collection<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new NameValuePair("Accept", "*/*"));
		headers.add(new NameValuePair("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3"));
//		headers.add(new NameValuePair("Accept-Encoding", "gzip,deflate,sdch"));
		headers.add(new NameValuePair("Accept-Language", "sq,en-GB;q=0.8,en;q=0.6,en-US;q=0.4"));
		headers.add(new NameValuePair("Connection", "keep-alive"));
		headers.add(new NameValuePair("Content-Type", "application/x-www-form-urlencoded"));
		headers.add(new NameValuePair("Host", "iuu-vessels.org"));
		headers.add(new NameValuePair("Referer", "http://iuu-vessels.org/iuu/iuu/search"));
		headers.add(new NameValuePair("User-Agent", "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4"));
		headers.add(new NameValuePair("X-Requested-With", "XMLHttpRequest"));
		
		return headers;
	}

	private Set<Integer> identify(String query) {
		Set<Integer> results = new ListSet<Integer>();
		
		try {
			String URL = IUU_VESSELS_ORG_SEARCH_API_URL.replaceAll("\\{query\\}", query);
			String content = this._httpHelper.fetchHTMLPageAsUTF8String(URL, this.doGetCustomRequestHeaders(null));
			
			Matcher finder = Pattern.compile("href\\=\\'vessel\\?uid\\=(\\d+)").matcher(content);
			
			int index = 0;
			
			String ID;
			while(finder.find(index)) {
				ID = finder.group(1);
				
				try {
					results.add(Integer.parseInt(ID));
				} catch (Throwable t) {
					this._log.warn("Unable to parse infringement ID '" + ID + "': " + t.getMessage(), t);
				}
				
				index = finder.start(1);
			}
			
		} catch(Throwable t) {
			this._log.warn("Unable to identify infringement ID by term " + query + " on iuu-vessels.org: " + t.getMessage());
		}
		
		return results;
	};
	
	private Set<VesselInfringementsIdentificationResponseData> getDetails(String criteria, Set<Integer> identifiedIDs) {
		Set<VesselInfringementsIdentificationResponseData> results = new ListSet<VesselInfringementsIdentificationResponseData>();
		
		VesselInfringementsIdentificationResponseData result;
		for(Integer ID : identifiedIDs) {
			result = this.getIdentifiedVesselsInfringementData(criteria, ID);
			
			if(result != null)
				results.add(result);
		}
		
		return results;
	}
	
	private VesselInfringementsIdentificationResponseData getIdentifiedVesselsInfringementData(String criteria, Integer ID) {
		VesselInfringementsIdentificationResponseData result = null;

		if(ID != null) {
			try {
				String URL = IUU_VESSELS_ORG_DETAILS_API_URL.replaceAll("\\{query\\}", ID.toString());
				
				String content = this._httpHelper.fetchHTMLPageAsUTF8String(URL, this.doGetCustomRequestHeaders(null));
				result = new VesselInfringementsIdentificationResponseData();
				result.setDataId(ID.toString());
				result.setSource("iuu-vessels.org");
				result.setSourceURL(IUU_VESSELS_ORG_DETAILS_API_URL.replaceAll("\\{query\\}", ID.toString()));
				result.setDetails(content);
				result.setCriteria(criteria);
			} catch(Throwable t) {
				this._log.warn("Unable to retrieve infringement data by ID " + ID + " on iuu-vessels.org: " + t.getMessage());
			}
		}
		
		return result;
	}
}
