/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.tracks.providers.support;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 May 2013
 */
public class ShipspottingJSONTrackData extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6112149766926898983L;

	private String imo;
	private String mmsi;
	private String callsign;
	private String name;

	private String latitude;
	private String longitude;

	/**
	 * Class constructor
	 *
	 * @param imo
	 * @param mmsi
	 * @param callsign
	 * @param name
	 * @param latitude
	 * @param longitude
	 */
	public ShipspottingJSONTrackData(String imo, String mmsi, String callsign, String name, String latitude, String longitude) {
		super();
		this.imo = imo;
		this.mmsi = mmsi;
		this.callsign = callsign;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	/**
	 * Class constructor
	 *
	 */
	public ShipspottingJSONTrackData() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the 'imo' value
	 */
	public String getImo() {
		return this.imo;
	}

	/**
	 * @param imo the 'imo' value to set
	 */
	public void setImo(String imo) {
		this.imo = imo;
	}

	/**
	 * @return the 'mmsi' value
	 */
	public String getMmsi() {
		return this.mmsi;
	}

	/**
	 * @param mmsi the 'mmsi' value to set
	 */
	public void setMmsi(String mmsi) {
		this.mmsi = mmsi;
	}

	/**
	 * @return the 'callsign' value
	 */
	public String getCallsign() {
		return this.callsign;
	}

	/**
	 * @param callsign the 'callsign' value to set
	 */
	public void setCallsign(String callsign) {
		this.callsign = callsign;
	}

	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the 'latitude' value
	 */
	public String getLatitude() {
		return this.latitude;
	}

	/**
	 * @param latitude the 'latitude' value to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the 'longitude' value
	 */
	public String getLongitude() {
		return this.longitude;
	}

	/**
	 * @param longitude the 'longitude' value to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
}
