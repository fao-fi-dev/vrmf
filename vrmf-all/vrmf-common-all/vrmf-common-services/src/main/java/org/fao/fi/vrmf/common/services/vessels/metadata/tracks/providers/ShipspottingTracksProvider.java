/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.tracks.providers;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.VesselTracksProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.providers.support.ShipspottingJSONTrackData;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.providers.support.ShipspottingJSONTrackWrapper;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.support.VesselTracks;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.support.VesselTracksIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

import flexjson.JSONDeserializer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 May 2013
 */
@Singleton
@Named("vrmf.common.vessels.metadata.tracks.provider.shipspotting")
public class ShipspottingTracksProvider extends VesselTracksProvider {
	static final private String SHIPSPOTTING_AIS_API_URL = "http://www.shipspotting.com/api/ais/searchship.php?action=searchship&searchstring={query}";

	/**
	 * Class constructor
	 *
	 * @throws Throwable
	 */
	public ShipspottingTracksProvider() throws Throwable {
		this(3000);
	}

	/**
	 * Class constructor
	 *
	 * @param timeout
	 * @throws Throwable
	 */
	public ShipspottingTracksProvider(int timeout) throws Throwable {
		super(timeout);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#doCheckAvailability(int)
	 */
	@Override
	protected boolean doCheckAvailability(int timeout) {
		try {
			HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(SHIPSPOTTING_AIS_API_URL.replace("{query}", "foobar"), this.forgeRequestHeaders(), timeout);

			return response != null &&
				   response.getResponseCode() <= 299 &&
				   response.getContentType() != null &&
				   response.getContentType().startsWith("text/html") &&
				   response.getContentLength() > 0;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.tracks.VesselPositionsProvider#doSearchByIMO(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.positions", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IMO)")
	public Collection<VesselTracksIdentificationResponseData> doSearchByIMO(String IMO) {
		return this.getShipspottingTrackByCriteria("IMO", IMO);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.tracks.VesselPositionsProvider#doSearchByEUCFR(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.positions", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#EUCFR)")
	public Collection<VesselTracksIdentificationResponseData> doSearchByEUCFR(String EUCFR) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.tracks.VesselPositionsProvider#doSearchByName(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.positions", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#name)")
	public Collection<VesselTracksIdentificationResponseData> doSearchByName(String name) {
		return this.getShipspottingTrackByCriteria("NAME", name);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.tracks.VesselPositionsProvider#doSearchByIRCS(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.positions", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IRCS)")
	public Collection<VesselTracksIdentificationResponseData> doSearchByIRCS(String IRCS) {
		return this.getShipspottingTrackByCriteria("IRCS", IRCS);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.tracks.VesselPositionsProvider#doSearchByMMSI(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.positions", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#MMSI)")
	public Collection<VesselTracksIdentificationResponseData> doSearchByMMSI(String MMSI) {
		return this.getShipspottingTrackByCriteria("MMSI", MMSI);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.tracks.VesselPositionsProvider#doSearchByIdentifier(org.fao.vrmf.core.impl.design.patterns.pair.NameValuePair)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.positions", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#identifier.name + #identifier.value)")
	public Collection<VesselTracksIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByEUCFR()
	 */
	@Override
	protected boolean canIdentifyByEUCFR() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByIMO()
	 */
	@Override
	protected boolean canIdentifyByIMO() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByIRCS()
	 */
	@Override
	protected boolean canIdentifyByIRCS() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByMMSI()
	 */
	@Override
	protected boolean canIdentifyByMMSI() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByName()
	 */
	@Override
	protected boolean canIdentifyByName() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyBySourceID()
	 */
	@Override
	protected boolean canIdentifyBySourceID() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#getSpecificSources()
	 */
	@Override
	protected String[] getSpecificSources() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.helpers.beans.vessels.metadata.AbstractVesselMetadataProvider#isSourceSpecific()
	 */
	@Override
	protected boolean isSourceSpecific() {
		return false;
	}

	private Collection<VesselTracksIdentificationResponseData> getShipspottingTrackByCriteria(String criteria, String value) {
		try {
			Collection<VesselTracksIdentificationResponseData> results = new ArrayList<VesselTracksIdentificationResponseData>();

			String response = this._httpHelper.fetchHTMLPageAsUTF8String(SHIPSPOTTING_AIS_API_URL.replaceAll("\\{query\\}", value), this.forgeRequestHeaders(), this.getServiceTimeout());

			JSONDeserializer<ShipspottingJSONTrackWrapper> deserializer = new JSONDeserializer<ShipspottingJSONTrackWrapper>();
			ShipspottingJSONTrackWrapper data = null;

			try {
				data = deserializer.deserialize(response, ShipspottingJSONTrackWrapper.class);
			} catch (Throwable t) {
				this._log.warn("Unable to parse JSON response '" + response + "'", t);
			}

			if(data != null) {
				VesselTracksIdentificationResponseData result;
				Collection<String> returnedCriteria;

				boolean keep;
				for(ShipspottingJSONTrackData position : data.getResults()) {
					keep = false;

					returnedCriteria = new ArrayList<String>();

					if(StringsHelper.trim(position.getImo()) != null)
						returnedCriteria.add("IMO = " + position.getImo());

					if(StringsHelper.trim(position.getMmsi()) != null)
						returnedCriteria.add("MMSI = " + position.getMmsi());

					if(StringsHelper.trim(position.getCallsign()) != null)
						returnedCriteria.add("IRCS = " + position.getCallsign());

					if(StringsHelper.trim(position.getName()) != null)
						returnedCriteria.add("NAME = " + position.getName());

					result = new VesselTracksIdentificationResponseData(Boolean.FALSE,
										 								"www.shipspotting.com",
										 								"http://www.shipspotting.com/ais/index.php",
										 								criteria + " = " + value + " [ " + CollectionsHelper.join(returnedCriteria, ", ") + " ]",
										 								System.currentTimeMillis(),
										 								new VesselTracks(position.getLatitude(),
										 												 position.getLongitude(),
										 												 System.currentTimeMillis(),
										 												 null, null));

					keep |= "IMO".equals(criteria) && position.getImo() != null && position.getImo().equalsIgnoreCase(value);
					keep |= "MMSI".equals(criteria) && position.getMmsi() != null && position.getMmsi().equalsIgnoreCase(value);
					keep |= "IRCS".equals(criteria) && position.getCallsign() != null && position.getCallsign().equalsIgnoreCase(value);
					keep |= "NAME".equals(criteria) && position.getName() != null && position.getName().equalsIgnoreCase(value);

					if(keep)
						results.add(result);
				}

				return results;
			}
		} catch(Throwable t) {
			this._log.warn("Unable to retrieve Shipspotting track data by {} {}: {} [ {} ]", criteria, value, t.getClass(), t.getMessage());
		}

		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doGetCustomRequestHeaders(java.lang.Object)
	 */
	@Override
	protected Collection<NameValuePair> doGetCustomRequestHeaders(Object params) {
		Collection<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new NameValuePair("Accept", "application/json,text/javascript,*/*; q=0.01"));
		headers.add(new NameValuePair("Host", "www.shipspotting.com"));
		headers.add(new NameValuePair("Referer", "http://www.shipspotting.com/ais/map.php?uid=0&screenmode=standard"));
		headers.add(new NameValuePair("X-Requested-With", "XMLHttpRequest"));

		return headers;
	}
}
