/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata;

import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.services.spi.request.ServiceRequest;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2013
 */
abstract public class VesselMetadataIdentificationRequest extends VesselMetadataIdentificationCriteria implements ServiceRequest {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -587837525238508084L;

	/**
	 * Class constructor
	 *
	 * @param iMOs
	 * @param eUCFRs
	 * @param names
	 * @param iRCSs
	 * @param mMSIs
	 * @param sourceIdentifiers
	 */
	public VesselMetadataIdentificationRequest(String[] iMOs, String[] eUCFRs, String[] names, String[] iRCSs, String[] mMSIs, NameValuePair[] sourceIdentifiers) {
		super(iMOs, eUCFRs, names, iRCSs, mMSIs, sourceIdentifiers);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.request.ServiceRequest#isValid()
	 */
	public boolean isValid() {
		return !this.isEmpty();
	}
}