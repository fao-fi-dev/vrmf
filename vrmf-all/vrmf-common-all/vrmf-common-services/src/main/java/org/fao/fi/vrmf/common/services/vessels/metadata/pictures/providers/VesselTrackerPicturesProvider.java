/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.http.Header;
import org.apache.http.client.methods.HttpGet;
import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.support.VesselTrackerPicturesSearchResult;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 May 2013
 */
@Singleton
@Named("vrmf.common.vessels.metadata.pictures.provider.vesseltracker")
public class VesselTrackerPicturesProvider extends VesselPicturesProvider {
	final static private String VESSEL_TRACKER_PING_URL = "http://www.vesseltracker.com/app";
//	final static private String VESSEL_TRACKER_SEARCH_URL = "http://www.vesseltracker.com/en/VesselArchive/All/Fishing_boats.html?search={query}";
	final static private String VESSEL_TRACKER_SEARCH_URL = "http://www.vesseltracker.com/en/VesselArchive/All/All.html?search={query}";

	final static private Pattern SHIP_DETAIL_ID_PATTERN = Pattern.compile("en/Ships/(.+)\\.html", Pattern.CASE_INSENSITIVE);
	final static private Pattern SHIP_IMAGE_PATTERN = Pattern.compile("//images.vesseltracker.com/images/vessels/midres/[^\"]+\\-([0-9]+)\\.jpg", Pattern.CASE_INSENSITIVE);
		
	/**
	 * Class constructor
	 *
	 * @param timeout
	 * @throws Throwable
	 */
	public VesselTrackerPicturesProvider() throws Throwable {
		super(3000);
	}
	
	/**
	 * Class constructor
	 *
	 * @param timeout
	 * @throws Throwable
	 */
	public VesselTrackerPicturesProvider(int timeout) throws Throwable {
		super(timeout);
	}

	private String getRedirectionPage(HTTPRequestMetadata meta) {
		if(meta == null || meta.getHeaders() == null || meta.getHeaders().length == 0)
			return null;
		
		for(Header header : meta.getHeaders())
			if("Location".equalsIgnoreCase(header.getName()))
				return header.getValue();
		
		return null;
	}

	private Collection<String> doSearch(String value) throws Throwable {
		HTTPRequestMetadata meta = this._httpHelper.fetchURLAsBytes(new HttpGet(), VESSEL_TRACKER_SEARCH_URL.replace("{query}", value), this.forgeRequestHeaders(), HTTPHelper.DONT_FOLLOW_REDIRECTS, this.getServiceTimeout());

		String redirectionPage = this.getRedirectionPage(meta);
		
		Collection<String> relatedPages = new ListSet<String>();
		
		if(redirectionPage != null) {
			relatedPages.add(redirectionPage);
		} else {
			String html = new String(meta.getContent(), "UTF-8");
			
			Matcher matcher = SHIP_DETAIL_ID_PATTERN.matcher(html);
			
			int index = 0;
	
			while(matcher.find(index)) {
				relatedPages.add("http://www.vesseltracker.com/en/Ships/" + matcher.group(1) + ".html");
	
				index = matcher.start(1);
			}
		}
		
		return relatedPages;
	}
	
	private boolean isAFishingVessel(String html) {
		return html.indexOf("Fishing Vessel\n") >= 0;
	}
	
	private Collection<String> getPictureIDsInPage(String URL) throws Throwable {
		HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(new HttpGet(), URL, null, HTTPHelper.DONT_FOLLOW_REDIRECTS, this.getServiceTimeout());
		
		String html = new String(response.getContent(), "UTF-8");
		
		Collection<String> pictureIDs = new ListSet<String>();
		
		if(isAFishingVessel(html)) {
			Matcher matcher = SHIP_IMAGE_PATTERN.matcher(html);
			
			int index = 0;
	
			while(matcher.find(index)) {
				pictureIDs.add(matcher.group(1) + ".jpg");
	
				index = matcher.start(1);
			}
		} else {
			this._log.warn("Ship details at {} do not identify a fishing vessel", URL);
		}
		
		return pictureIDs;
	}
	
	private Collection<String> getPictureIDs(String value) throws Throwable {
		Collection<String> pictureIDs = new ListSet<String>();		
		
		try {
			Collection<String> pages = this.doSearch(value);
			
			if(pages != null && !pages.isEmpty()) {
				Collection<String> currentIDs;
				
				for(String page : pages) {
					try {
						currentIDs = this.getPictureIDsInPage(page);
						
						if(currentIDs != null && !currentIDs.isEmpty())
							pictureIDs.addAll(currentIDs);
					} catch (Throwable t) {
						this._log.warn("Unable to extract vesseltracker.com pictures from page {}: {} [ {} ]", page, t.getClass(), t.getMessage());
					}
				}
			}
		} catch(Throwable t) {
			this._log.warn("Unable to retrieve vesseltracker.com pictures by criteria {}: {} [ {} ]", value, t.getClass(), t.getMessage());
		}
		
		return pictureIDs;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByIMO(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IMO)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIMO(String IMO) {
		Collection<VesselPicturesIdentificationResponseData> results = null;
		
		if(IMO != null) {
			try {
				String parentURL = VESSEL_TRACKER_SEARCH_URL.replace("{query}", IMO);
				
				Collection<String> IDs = this.getPictureIDs(IMO);
				
				if(IDs != null && !IDs.isEmpty()) {
					results = new ArrayList<VesselPicturesIdentificationResponseData>();
					
					results.add(new VesselTrackerPicturesSearchResult("IMO = " + IMO, parentURL, IDs));
				}
			} catch(Throwable t) {
				this._log.warn("Unable to retrieve vesseltracker.com pictures by IMO {}: {} [ {} ]", IMO, t.getClass(), t.getMessage());
			}
		}
		
		return results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByEUCFR(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#EUCFR)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByEUCFR(String EUCFR) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByName(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#name)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByName(String name) {
		Collection<VesselPicturesIdentificationResponseData> results = null;
		
		if(name != null) {
			try {
				String parentURL = VESSEL_TRACKER_SEARCH_URL.replace("{query}", name);
				
				Collection<String> IDs = this.getPictureIDs(name);
				
				if(IDs != null && !IDs.isEmpty()) {
					results = new ArrayList<VesselPicturesIdentificationResponseData>();
					
					results.add(new VesselTrackerPicturesSearchResult("Name = " + name, parentURL, IDs));
				}
			} catch(Throwable t) {
				this._log.warn("Unable to retrieve vesseltracker.com pictures by name {}: {} [ {} ]", name, t.getClass(), t.getMessage());
			}
		}
		
		return results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByIRCS(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IRCS)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIRCS(String IRCS) {
		Collection<VesselPicturesIdentificationResponseData> results = null;
		
		if(IRCS != null) {
			try {
				String parentURL = VESSEL_TRACKER_SEARCH_URL.replace("{query}", IRCS);
				
				Collection<String> IDs = this.getPictureIDs(IRCS);
				
				if(IDs != null && !IDs.isEmpty()) {
					results = new ArrayList<VesselPicturesIdentificationResponseData>();
					
					results.add(new VesselTrackerPicturesSearchResult("IRCS = " + IRCS, parentURL, IDs));
				}
			} catch(Throwable t) {
				this._log.warn("Unable to retrieve vesseltracker.com pictures by IRCS {}: {} [ {} ]", IRCS, t.getClass(), t.getMessage());
			}
		}
		
		return results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByMMSI(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#MMSI)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByMMSI(String MMSI) {
		Collection<VesselPicturesIdentificationResponseData> results = null;
		
		if(MMSI != null) {
			try {
				String parentURL = VESSEL_TRACKER_SEARCH_URL.replace("{query}", MMSI);
				
				Collection<String> IDs = this.getPictureIDs(MMSI);
				
				if(IDs != null && !IDs.isEmpty()) {
					results = new ArrayList<VesselPicturesIdentificationResponseData>();
					
					results.add(new VesselTrackerPicturesSearchResult("MMSI = " + MMSI, parentURL, IDs));
				}
			} catch(Throwable t) {
				this._log.warn("Unable to retrieve vesseltracker.com pictures by MMSI {}: {} [ {} ]", MMSI, t.getClass(), t.getMessage());
			}
		}
		
		return results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByIdentifier(org.fao.vrmf.core.impl.design.patterns.pair.NameValuePair)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#identifier.name + #identifier.value)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByEUCFR()
	 */
	@Override
	protected boolean canIdentifyByEUCFR() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByIMO()
	 */
	@Override
	protected boolean canIdentifyByIMO() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByIRCS()
	 */
	@Override
	protected boolean canIdentifyByIRCS() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByMMSI()
	 */
	@Override
	protected boolean canIdentifyByMMSI() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByName()
	 */
	@Override
	protected boolean canIdentifyByName() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyBySourceID()
	 */
	@Override
	protected boolean canIdentifyBySourceID() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doCheckAvailability(int)
	 */
	@Override
	protected boolean doCheckAvailability(int timeout) {
		try {
			HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(VESSEL_TRACKER_PING_URL, timeout);

			return response != null &&
				   response.getResponseCode() <= 299 &&
				   response.getContentType() != null &&
				   response.getContentType().startsWith("text/html") &&
				   response.getContentLength() > 0;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#getSpecificSources()
	 */
	@Override
	protected String[] getSpecificSources() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#isSourceSpecific()
	 */
	@Override
	protected boolean isSourceSpecific() {
		return false;
	}
		
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doGetCustomRequestHeaders(java.lang.Object)
	 */
	protected Collection<NameValuePair> doGetCustomRequestHeaders(Object params) {
		Collection<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new NameValuePair("Cache-Control", "no-cache"));
//		headers.add(new NameValuePair("Content-Type", "application/x-www-form-urlencoded"));
//		headers.add(new NameValuePair("Cookie", "__utmz=23519549.1368017338.1.1.utmccn=(referral)|utmcsr=google.com|utmcct=/|utmcmd=referral; __utma=23519549.1513739139.1368017338.1368017338.1369210284.2; __utmc=23519549; __utmb=23519549"));
		headers.add(new NameValuePair("Host", "www.vesseltracker.com"));
		headers.add(new NameValuePair("Pragma", "no-cache"));
//		headers.add(new NameValuePair("Origin", "http://www.vesseltracker.com"));
		headers.add(new NameValuePair("Referer", "http://www.vesseltracker.com/en/Gallery/ShipTypes/Fishing_boats.html?dir=0"));

		return headers;
	}
}