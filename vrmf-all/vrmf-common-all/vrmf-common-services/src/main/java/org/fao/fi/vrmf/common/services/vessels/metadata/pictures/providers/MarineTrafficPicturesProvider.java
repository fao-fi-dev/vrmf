/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.support.MarineTrafficPicturesSearchResult;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 2, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 2, 2015
 * 
 * @deprecated Changes in remote API make this provider basically useless
 */
@Deprecated
//@Singleton
//@Named("vrmf.common.vessels.metadata.pictures.provider.marinetraffic")
public class MarineTrafficPicturesProvider extends VesselPicturesProvider {
	static final private String MARINE_TRAFFIC_PING_URL = "http://www.marinetraffic.com/photos/of/ships/photo_keywords:211542050";
	static final private String MARINE_TRAFFIC_SEARCH_BY_TEXT_URL = "http://www.marinetraffic.com/en/ais/index/ships/all/shipname:{query}/ship_type:2";

	public MarineTrafficPicturesProvider() throws Throwable {
		super(5000);
	}

	@Override
	protected boolean doCheckAvailability(int timeout) {
		try {
			HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(MARINE_TRAFFIC_PING_URL, timeout);

			return response != null &&
				   response.getResponseCode() <= 299 &&
				   response.getContentType() != null &&
				   response.getContentType().startsWith("text/html") &&
				   response.getContentLength() > 0;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}

	@Override
	protected boolean canIdentifyByIMO() {
		return true;
	}

	@Override
	protected boolean canIdentifyByEUCFR() {
		return false;
	}

	@Override
	protected boolean canIdentifyByName() {
		return true;
	}

	@Override
	protected boolean canIdentifyByIRCS() {
		return false;
	}

	@Override
	protected boolean canIdentifyByMMSI() {
		return true;
	}

	@Override
	protected boolean canIdentifyBySourceID() {
		return false;
	}

	@Override
	protected boolean isSourceSpecific() {
		return false;
	}

	@Override
	protected String[] getSpecificSources() {
		return null;
	}
	
	private Collection<VesselPicturesIdentificationResponseData> doSearchByText(String field, String text) {
		Collection<String> pictureIDs = new ListSet<String>();
		String URL = MARINE_TRAFFIC_SEARCH_BY_TEXT_URL.replace("{query}", text);

		Collection<VesselPicturesIdentificationResponseData> results = new ArrayList<VesselPicturesIdentificationResponseData>();

		results.add(new MarineTrafficPicturesSearchResult(field + " = " + text, URL, pictureIDs));

		try {
			String result = this._httpHelper.fetchHTMLPageAsUTF8String(URL, this.getServiceTimeout());

			String idPattern = "/en/ais/details/ships/(\\d+)";
			Matcher idMatcher = Pattern.compile(idPattern).matcher(result);

			int index = 0;

			while(idMatcher.find(index)) {
				String picturesURL = "http://www.marinetraffic.com/en/ais/details/ships/" + idMatcher.group(1);

				final String picturesResult = this._httpHelper.fetchHTMLPageAsUTF8String(picturesURL);

				pictureIDs.addAll(this.getPictureIDsInPage(picturesResult));

				index = idMatcher.start(1);
			}
		} catch (Throwable t) {
			this._log.warn("Unable to retrieve MarineTraffic pictures by " + field + " {}: {} [ {} ]", text, t.getClass(), t.getMessage());
		}

		return results;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#IMO)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIMO(String IMO) {
		return this.doSearchByText("IMO", IMO);
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#EUCFR)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByEUCFR(String EUCFR) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#name)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByName(String name) {
		return this.doSearchByText("Name", name);
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#IRCS)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIRCS(String IRCS) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#MMSI)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByMMSI(String MMSI) {
		return this.doSearchByText("MMSI", MMSI);
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#identifier.name + #identifier.value)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier) {
		return null;
	}

	protected Collection<String> getPictureIDsInPage(String HTML) {
		Collection<String> pictureIDs = new ListSet<String>();

		String picturesPattern = "photoid=(\\d+)";

		Matcher picturesMatcher = Pattern.compile(picturesPattern).matcher(HTML);

		int picturesIndex = 0;

		while(picturesMatcher.find(picturesIndex)) {
			pictureIDs.add(picturesMatcher.group(1));

			picturesIndex = picturesMatcher.start(1);
		}

		return pictureIDs;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doGetCustomRequestHeaders(java.lang.Object)
	 */
	@Override
	protected Collection<NameValuePair> doGetCustomRequestHeaders(Object params) {
		return NO_CUSTOM_HEADERS;
	}
}