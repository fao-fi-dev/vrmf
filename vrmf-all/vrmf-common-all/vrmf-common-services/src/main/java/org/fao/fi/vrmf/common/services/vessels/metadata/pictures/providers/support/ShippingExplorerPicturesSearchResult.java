/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.support;

import java.util.Collection;

import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2011
 */
final public class ShippingExplorerPicturesSearchResult extends VesselPicturesIdentificationResponseData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -9114774768279925562L;

	/**
	 * Class constructor
	 *
	 * @param source
	 * @param thumbsURL
	 * @param fullURL
	 * @param criteria
	 * @param iDs
	 */
	public ShippingExplorerPicturesSearchResult(String source, String thumbsURL, String fullURL, String criteria, String parentURL, Collection<String> IDs) {
		super(PRIVATE, "Shipping Explorer", source, thumbsURL, fullURL, criteria, parentURL, IDs);
	}
	
	public ShippingExplorerPicturesSearchResult(String criteria, String parentURL, Collection<String> IDs) {
		this("http://www.shippingexplorer.net/",
			 "http://www.shippingexplorer.net/vesselphoto.svc?size=th&photoid=",
			 "http://www.shippingexplorer.net/vesselphoto.svc?size=view&photoid=",
			 criteria,
			 parentURL,
			 IDs);
	}
}