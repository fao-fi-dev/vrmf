/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.support.WCPFCPicturesSearchResult;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

@Singleton
@Named("vrmf.common.vessels.metadata.pictures.provider.wcpfc")
public class WCPFCPicturesProvider extends VesselPicturesProvider {
	static final private String WCPFC_PICTURES_URL = "http://intra.wcpfc.int/Lists/Vessels/DispForm.aspx?ID={query}";
//	static final private String WCPFC_PICTURES_SEARCH_URL = "http://intra.wcpfc.int/_layouts/searchresults.aspx?k={query}&u=http%3A%2F%2Fintra%2Ewcpfc%2Eint%2FLists%2FVessels";
	static final private String WCPFC_PICTURES_SEARCH_URL = "http://intra.wcpfc.int/_layouts/searchresults.aspx?k={query}&u=http://intra.wcpfc.int/Lists/Vessels";
	static final private String WCPFC_PING_URL = "http://intra.wcpfc.int/Lists/Vessels/DispForm.aspx";

	public WCPFCPicturesProvider() throws Throwable {
		super(3000);
	}

	@Override
	protected boolean doCheckAvailability(int timeout) {
		try {
			HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(WCPFC_PING_URL, timeout);

			return response != null &&
				   response.getResponseCode() <= 299 &&
				   response.getContentType() != null &&
				   response.getContentType().startsWith("text/html") &&
				   response.getContentLength() > 0;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}

	@Override
	protected boolean canIdentifyByIMO() {
		return false;
	}

	@Override
	protected boolean canIdentifyByEUCFR() {
		return false;
	}

	@Override
	protected boolean canIdentifyByName() {
		return false;
	}

	@Override
	protected boolean canIdentifyByIRCS() {
		return true;
	}

	@Override
	protected boolean canIdentifyByMMSI() {
		return false;
	}

	@Override
	protected boolean canIdentifyBySourceID() {
		return true;
	}

	@Override
	protected boolean isSourceSpecific() {
		return false;
	}

	@Override
	protected String[] getSpecificSources() {
		return new String[] { "WCPFC" };
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#IMO)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIMO(String IMO) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#EUCFR)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByEUCFR(String EUCFR) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#name)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByName(String name) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#IRCS)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIRCS(String IRCS) {
		Collection<String> pictureIDs = new ListSet<String>();

		Collection<VesselPicturesIdentificationResponseData> results = new ArrayList<VesselPicturesIdentificationResponseData>();

		try {
			String URL = WCPFC_PICTURES_SEARCH_URL.replace("{query}", IRCS);

			String result = this._httpHelper.fetchHTMLPageAsUTF8String(URL);

			String detailPagePattern = "DispForm\\.aspx\\?ID\\=(\\d+)";

			Matcher detailPageMatcher = Pattern.compile(detailPagePattern).matcher(result);

			int index = 0;

			String WCPFCID = null;
			Set<String> processedVessels = new HashSet<String>();

			Collection<String> currentPictureIDs;

			while(detailPageMatcher.find(index)) {
				WCPFCID = StringsHelper.trim(detailPageMatcher.group(1));

				currentPictureIDs = this.getPictureIDByWCPFCID(WCPFC_PICTURES_URL.replace("{query}", WCPFCID), WCPFCID);

				if(WCPFCID != null && !processedVessels.contains(WCPFCID) && currentPictureIDs != null) {
					for(String ID : currentPictureIDs)
						pictureIDs = this.safeAdd(pictureIDs, ID);

					processedVessels.add(WCPFCID);
				} else {
					if(WCPFCID != null)
						this._log.debug("Got an alread processed reference to WCPFC vessel #" + WCPFCID + ": skipping...");
				}

				index = detailPageMatcher.start(1);
			}

			results.add(new WCPFCPicturesSearchResult("IRCS = " + IRCS, WCPFC_PICTURES_SEARCH_URL.replace("{query}", IRCS), pictureIDs));
		} catch(Throwable t) {
			this._log.warn("Unable to retrieve WCPFC pictures by IRCS {}: {} [ {} ]", IRCS, t.getClass(), t.getMessage());
		}

		return results;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#MMSI)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByMMSI(String MMSI) {
		return null;
	}

	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCacheKey(#identifier.name + #identifier.value)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier) {
		WCPFCPicturesSearchResult searchResult = null;

		String WCPFC_ID = (String)identifier.getValue();
		String URL = WCPFC_PICTURES_URL.replace("{query}", WCPFC_ID);

		Collection<VesselPicturesIdentificationResponseData> results = new ArrayList<VesselPicturesIdentificationResponseData>();

		if("WCPFC".equals(identifier.getName())) {
			try {
				try {
					searchResult = new WCPFCPicturesSearchResult("WCPFC ID = " + WCPFC_ID, URL, this.getPictureIDByWCPFCID(URL, WCPFC_ID));

					results.add(searchResult);
				} catch(Throwable t) {
					this._log.warn("Unable to retrieve WCPFC pictures by WCPFC ID {}: {} [ {} ]", WCPFC_ID, t.getClass(), t.getMessage());
				}
			} catch(Throwable t) {
				this._log.warn("Unable to retrieve WCPFC pictures by WCPFC ID {}: {} [ {} ]", WCPFC_ID, t.getClass(), t.getMessage());
			}
		}

		return results;
	}

	private Collection<String> getPictureIDByWCPFCID(String URL, String WCPFC_ID) throws Throwable {
		String result = this._httpHelper.fetchHTMLPageAsUTF8String(URL, this.getServiceTimeout());

		Collection<String> pictureIDs = new TreeSet<String>();

		String pattern = "href=\"[^\"]+/Attachments/" + WCPFC_ID + "/[^\"]+\"[^>]*>([^<]+)<";

		Matcher matcher = Pattern.compile(pattern).matcher(result);

		int index = 0;

		while(matcher.find(index)) {
			pictureIDs.add(WCPFC_ID + "/" + matcher.group(1));

			index = matcher.start(1);
		}

		return pictureIDs;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doGetCustomRequestHeaders(java.lang.Object)
	 */
	@Override
	protected Collection<NameValuePair> doGetCustomRequestHeaders(Object params) {
		return NO_CUSTOM_HEADERS;
	}
}