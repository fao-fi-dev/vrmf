/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-services)
 */
package org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.support.MagramaPicturesSearchResult;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.springframework.cache.annotation.Cacheable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 May 2013
 */
@Singleton
@Named("vrmf.common.vessels.metadata.pictures.provider.magrama")
public class MagramaPicturesProvider extends VesselPicturesProvider {
	final static private String MAGRAMA_PING_URL = "http://www.magrama.gob.es/gl/pesca/temas/la-pesca-en-espana/censo-de-la-flota-pesquera/censo.asp";
	final static private String MAGRAMA_SEARCH_URL = "http://www.magrama.gob.es/gl/pesca/temas/la-pesca-en-espana/censo-de-la-flota-pesquera/consulta.asp?cod={query}&cd_matr=Consultar";
	
	/**
	 * Class constructor
	 *
	 * @param timeout
	 * @throws Throwable
	 */
	public MagramaPicturesProvider() throws Throwable {
		super(3000);
	}
	
	/**
	 * Class constructor
	 *
	 * @param timeout
	 * @throws Throwable
	 */
	public MagramaPicturesProvider(int timeout) throws Throwable {
		super(timeout);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByIMO(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IMO)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIMO(String IMO) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByEUCFR(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#EUCFR)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByEUCFR(String EUCFR) {
		Collection<VesselPicturesIdentificationResponseData> results = null;
		
		if(EUCFR != null && EUCFR.toUpperCase().startsWith("ESP") && EUCFR.length() == 12) {
			String ID = EUCFR.substring(7);
			
			String URL = MAGRAMA_SEARCH_URL.replace("{query}", ID);

			try {
				HTTPRequestMetadata data = this._httpHelper.fetchURLAsBytes(URL, this.forgeRequestHeaders(), this.getServiceTimeout());
				
				if(data != null && 
				   data.getResponseCode() <= 299 &&
				   data.getContentType() != null &&
				   data.getContentType().startsWith("text/html") &&
				   data.getContentLength() > 0) {
					String content = new String(data.getContent(), "UTF-8");
					
					String pattern = "src=\"/pesca/img/Flota/([0-9]+\\.jpg)\"[^>]*>";
	
					Collection<String> pictureIDs = new TreeSet<String>();
	
					Matcher matcher = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(content);
	
					int index = 0;
	
					while(matcher.find(index)) {
						pictureIDs.add(matcher.group(1));
	
						index = matcher.start(1);
					}
	
					results = new ArrayList<VesselPicturesIdentificationResponseData>();
	
					results.add(new MagramaPicturesSearchResult("EU CFR = " + EUCFR, URL, pictureIDs));
				} else {
					this._log.warn("Wrong data returned by searching magrama.gob.es pictures by EU CFR {} via {}", EUCFR, URL);
				}
			} catch(Throwable t) {
				this._log.warn("Unable to retrieve magrama.gob.es pictures by EU CFR {}: {} [ {} ]", EUCFR, t.getClass(), t.getMessage());
			}
		}
		
		return results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByName(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#name)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByName(String name) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByIRCS(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#IRCS)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIRCS(String IRCS) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByMMSI(java.lang.String)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#MMSI)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByMMSI(String MMSI) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.pictures.VesselPicturesProvider#doSearchByIdentifier(org.fao.vrmf.core.impl.design.patterns.pair.NameValuePair)
	 */
	@Override
	@Cacheable(value = "vrmf.common.cache.vessel.pictures", key = "#root.method.name + #root.target.buildSpecificCriteriaCacheKey(#identifier.name + #identifier.value)")
	public Collection<VesselPicturesIdentificationResponseData> doSearchByIdentifier(NameValuePair identifier) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByEUCFR()
	 */
	@Override
	protected boolean canIdentifyByEUCFR() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByIMO()
	 */
	@Override
	protected boolean canIdentifyByIMO() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByIRCS()
	 */
	@Override
	protected boolean canIdentifyByIRCS() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByMMSI()
	 */
	@Override
	protected boolean canIdentifyByMMSI() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyByName()
	 */
	@Override
	protected boolean canIdentifyByName() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#canIdentifyBySourceID()
	 */
	@Override
	protected boolean canIdentifyBySourceID() {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doCheckAvailability(int)
	 */
	@Override
	protected boolean doCheckAvailability(int timeout) {
		try {
			HTTPRequestMetadata response = this._httpHelper.fetchURLAsBytes(MAGRAMA_PING_URL, timeout);

			return response != null &&
				   response.getResponseCode() <= 299 &&
				   response.getContentType() != null &&
				   response.getContentType().startsWith("text/html") &&
				   response.getContentLength() > 0;
		} catch (Throwable t) {
			this._log.warn("Unable to check availability of {}: {} [ {} ]", this.getClass().getSimpleName(), t.getClass().getSimpleName(), t.getMessage());

			return false;
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#getSpecificSources()
	 */
	@Override
	protected String[] getSpecificSources() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#isSourceSpecific()
	 */
	@Override
	protected boolean isSourceSpecific() {
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.services.vessels.metadata.AbstractVesselMetadataProvider#doGetCustomRequestHeaders(java.lang.Object)
	 */
	@Override
	protected Collection<NameValuePair> doGetCustomRequestHeaders(Object params) {
		Collection<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new NameValuePair("Host", "www.magrama.gob.es"));
		headers.add(new NameValuePair("Origin", "http://www.magrama.gob.es"));
		headers.add(new NameValuePair("Referer", "http://www.magrama.gob.es/gl/pesca/temas/la-pesca-en-espana/censo-de-la-flota-pesquera/censo.asp"));

		return headers;
	}
}