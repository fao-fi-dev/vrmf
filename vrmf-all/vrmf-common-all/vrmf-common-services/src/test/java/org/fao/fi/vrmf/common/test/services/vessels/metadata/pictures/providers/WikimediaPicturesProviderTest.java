/**
 * (c) 2013 FAO / UN (project: vrmf-common)
 */
package org.fao.fi.vrmf.common.test.services.vessels.metadata.pictures.providers;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.WikimediaPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationRequest;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2013
 */
public class WikimediaPicturesProviderTest extends AbstractPicturesProviderTest<WikimediaPicturesProvider> {
	protected WikimediaPicturesProvider getProvider() throws Throwable {
		WikimediaPicturesProvider provider = new WikimediaPicturesProvider();
		provider.setHttpHelper(new HTTPHelper());

		return provider;
	}

	@Test
	public void  testRetrievement() throws Throwable {
		Assume.assumeTrue(this.isAvailable());
		
		Collection<VesselPicturesIdentificationResponseData> result = this.getProvider().searchPictures(new VesselPicturesIdentificationRequest(new String[] { "5119143" }, null, null, null, null, null)).getResults();

		Assume.assumeNotNull(result);
		
		Assert.assertFalse("Result must not be empty", result.isEmpty());
		Assert.assertEquals("Result must contain one entry", 1, result.size());

		List<VesselPicturesIdentificationResponseData> asList = new ArrayList<VesselPicturesIdentificationResponseData>(result);
		VesselPicturesIdentificationResponseData item = asList.get(0);

		Assert.assertNotNull(item);
		Assert.assertNotNull(item.getIDs());
		Assert.assertTrue(!item.getIDs().isEmpty());
		Assert.assertTrue("Returned pictures must be more than (or equal to) 12 items", item.getIDs().size() >= 12);

		for(String ID : item.getIDs()) {
			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
		}
	}
}