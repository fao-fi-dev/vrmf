/**
 * (c) 2013 FAO / UN (project: vrmf-common)
 */
package org.fao.fi.vrmf.common.test.services.vessels.metadata.pictures.providers;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.ShippingExplorerPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationRequest;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.fao.fi.vrmf.common.tools.lexical.processors.queue.impl.VesselNameSimplifier;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2013
 */
public class ShippingExplorerPicturesProviderTest extends AbstractPicturesProviderTest<ShippingExplorerPicturesProvider> {
	protected ShippingExplorerPicturesProvider getProvider() throws Throwable {
		ShippingExplorerPicturesProvider provider = new ShippingExplorerPicturesProvider();
		provider.setHttpHelper(new HTTPHelper());
		provider.setVesselNamesSimplifier(new VesselNameSimplifier());

		return provider;
	}

	@Test
	public void  testRetrievementByIMO() throws Throwable {
		Assume.assumeTrue(this.isAvailable());
		
		Collection<VesselPicturesIdentificationResponseData> result = this.getProvider().searchPictures(new VesselPicturesIdentificationRequest(new String[] { "9204556" }, null, null, null, null, null)).getResults();

		Assume.assumeNotNull(result);
		
		Assert.assertFalse("Result must not be empty", result.isEmpty());
		Assert.assertEquals("Result must contain one entry", 1, result.size());

		List<VesselPicturesIdentificationResponseData> asList = new ArrayList<VesselPicturesIdentificationResponseData>(result);
		VesselPicturesIdentificationResponseData item = asList.get(0);

		Assert.assertNotNull(item);
		Assert.assertNotNull(item.getIDs());
		Assert.assertTrue(!item.getIDs().isEmpty());
		Assert.assertTrue("Returned pictures must be more than (or equal to) 1 item", item.getIDs().size() >= 1);

		for(String ID : item.getIDs()) {
			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
		}
	}

	@Test
	public void  testRetrievementByMMSI() throws Throwable {
		Assume.assumeTrue(this.isAvailable());
		
		Collection<VesselPicturesIdentificationResponseData> result = this.getProvider().searchPictures(new VesselPicturesIdentificationRequest(null, null, null, null, new String[] { "244563000" }, null)).getResults();

		Assume.assumeNotNull(result);
		
		Assert.assertFalse("Result must not be empty", result.isEmpty());
		Assert.assertEquals("Result must contain one entry", 1, result.size());

		List<VesselPicturesIdentificationResponseData> asList = new ArrayList<VesselPicturesIdentificationResponseData>(result);
		VesselPicturesIdentificationResponseData item = asList.get(0);

		Assert.assertNotNull(item);
		Assert.assertNotNull(item.getIDs());
		Assert.assertTrue(!item.getIDs().isEmpty());
		Assert.assertTrue("Returned pictures must be more than (or equal to) 1 item", item.getIDs().size() >= 1);

		for(String ID : item.getIDs()) {
			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
		}
	}

	@Test
	public void  testRetrievementByName() throws Throwable {
		Assume.assumeTrue(this.isAvailable());
		
		Collection<VesselPicturesIdentificationResponseData> result = this.getProvider().searchPictures(new VesselPicturesIdentificationRequest(null, null, new String[] { "Margiris" }, null, null, null)).getResults();

		Assume.assumeNotNull(result);
		
		Assert.assertFalse("Result must not be empty", result.isEmpty());
		Assert.assertEquals("Result must contain one entry", 1, result.size());

		List<VesselPicturesIdentificationResponseData> asList = new ArrayList<VesselPicturesIdentificationResponseData>(result);
		VesselPicturesIdentificationResponseData item = asList.get(0);

		Assert.assertNotNull(item);
		Assert.assertNotNull(item.getIDs());
		Assert.assertTrue(!item.getIDs().isEmpty());
		Assert.assertTrue("Returned pictures must be more than (or equal to) 2 items", item.getIDs().size() >= 2);

		for(String ID : item.getIDs()) {
			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
		}

//		item = asList.get(1);
//
//		Assert.assertNotNull(item);
//		Assert.assertNotNull(item.getIDs());
//		Assert.assertTrue(!item.getIDs().isEmpty());
//		Assert.assertTrue("Returned pictures must be more than (or equal to) 2 items", item.getIDs().size() >= 2);
//
//		for(String ID : item.getIDs()) {
//			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
//		}
	}

	@Test
	public void  testRetrievementByMultipleCriteria() throws Throwable {
		Assume.assumeTrue(this.isAvailable());
		
		Collection<VesselPicturesIdentificationResponseData> result = this.getProvider().searchPictures(new VesselPicturesIdentificationRequest(
				new String[] { "9204556", "9018593" }, 
				null, 
				new String[] { "Ann", "Margiris" }, 
				new String[] { "PHKE", "EI5920" }, 
				new String[] { "244563000", "250240000" }, 
				null)
		).getResults();
		
		Assume.assumeNotNull(result);
		
		for(VesselPicturesIdentificationResponseData item : result) {
			for(String ID : item.getIDs()) {
				System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
			}
		}
		
		Assert.assertFalse("Result must not be empty", result.isEmpty());
		Assert.assertEquals("Result must contain 8 entries", 8, result.size());

		List<VesselPicturesIdentificationResponseData> asList = new ArrayList<VesselPicturesIdentificationResponseData>(result);
		VesselPicturesIdentificationResponseData item = asList.get(0);

		Assert.assertNotNull(item);
		Assert.assertNotNull(item.getIDs());
		Assert.assertTrue(!item.getIDs().isEmpty());
		Assert.assertTrue("Returned pictures must be more than (or equal to) 1 item", item.getIDs().size() >= 1);

		for(String ID : item.getIDs()) {
			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
		}

		item = asList.get(1);

		Assert.assertNotNull(item);
		Assert.assertNotNull(item.getIDs());
		Assert.assertTrue(!item.getIDs().isEmpty());
		Assert.assertTrue("Returned pictures must be more than (or equal to) 2 items", item.getIDs().size() >= 2);

		for(String ID : item.getIDs()) {
			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
		}
	}
}