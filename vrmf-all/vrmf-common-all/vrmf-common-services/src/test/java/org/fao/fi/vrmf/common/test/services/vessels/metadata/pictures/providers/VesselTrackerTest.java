/**
 * (c) 2013 FAO / UN (project: vrmf-common)
 */
package org.fao.fi.vrmf.common.test.services.vessels.metadata.pictures.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.sh.utility.common.helpers.beans.io.support.HTTPRequestMetadata;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 May 2013
 */
public class VesselTrackerTest {
	private HttpPost buildPost(String param) throws Throwable {
		HttpPost method = new HttpPost();

		List<BasicNameValuePair> nvps = new ArrayList<BasicNameValuePair>();
		
		nvps.add(new BasicNameValuePair("formids", "hiddenChoice,searchSelector,searchTerm,searchSubmitButton"));
//		nvps.add(new BasicNameValuePair("seedids", "ZH4sIAAAAAAAAAGWTzW6DMAzHK1V7E445NIECPW7VulXqpGndfQrglaiQIEL39Ux7tR2mvcCckNKSXmr/7cb+xTHfP5Orr8lk+jf9JRraN5FDMCMN3xmTq7pREmRnfdlxIaFFX4PWQknjNfhzo9rCxrclVBXaGjqOZv0aMEZuKw1BSq7l58vMBtEwKxlKY0Nnoz5NA2oP4f+ojccunzibmlYd70S+RKgeby2RvvMTx44MK6FJe7VA1UKNtxFydw+8sHcoPl372bG/ARmXM8TbvBWN6XRXqYxXGyH3wZx0JViPhuQJJI7DFqSxrTjcyHFQdgyELhCO6tkJ2Xzk8pG7OpbqIeeGrX8vd2I20tTTzNOhpyNPzz0dezrxdOrphc9zAegTUh+R+ozUh6SGcqN2Qi7PNrUygZVqa/TxWaxHCZp+7YKYrARUxYZnYJb1gIsjeW32/RS382y41u+42+guS8j3mfowAIesFqbPs2oeQB7GLxcPK/iI39DwNPpEHdDk/EB6eYCNS9LY04mnF2PN7KhL0WyBt3k5rKwDMbF+Ov80I0SU/AMAAA=="));
		nvps.add(new BasicNameValuePair("component", "$Border.$ShipSearch.searchForm.theForm"));
		nvps.add(new BasicNameValuePair("page", "PicturesShiptype"));
		nvps.add(new BasicNameValuePair("service", "direct"));
		nvps.add(new BasicNameValuePair("submitmode", ""));
		nvps.add(new BasicNameValuePair("submitname", ""));
		nvps.add(new BasicNameValuePair("page", "PicturesShiptype"));
		nvps.add(new BasicNameValuePair("categoryName", "fishing_boats"));
		nvps.add(new BasicNameValuePair("sortby", "timestamp"));
		nvps.add(new BasicNameValuePair("dir", "0"));
		nvps.add(new BasicNameValuePair("page", "PicturesShiptype"));
		nvps.add(new BasicNameValuePair("language", "en"));
		nvps.add(new BasicNameValuePair("hiddenChoice", ""));
		nvps.add(new BasicNameValuePair("searchSelector", "0"));
		nvps.add(new BasicNameValuePair("searchTerm", param));
		nvps.add(new BasicNameValuePair("searchSubmitButton.x", "20"));
		nvps.add(new BasicNameValuePair("searchSubmitButton.y", "13"));
				
		method.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
		
		return method;
	}
	
	private Collection<NameValuePair> buildHeaders(String param) {
		Collection<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new NameValuePair("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"));
		headers.add(new NameValuePair("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3"));
		headers.add(new NameValuePair("Accept-Encoding", "gzip,deflate,sdch"));
		headers.add(new NameValuePair("Accept-Language", "en;q=0.8,en-GB;q=0.6,en-US;q=0.4"));
		headers.add(new NameValuePair("Cache-Control", "max-age=0"));
		headers.add(new NameValuePair("Connection", "keep-alive"));
		headers.add(new NameValuePair("Content-Type", "application/x-www-form-urlencoded"));
//		headers.add(new NameValuePair("Cookie", "__utmz=23519549.1368017338.1.1.utmccn=(referral)|utmcsr=google.com|utmcct=/|utmcmd=referral; __utma=23519549.1513739139.1368017338.1368017338.1369210284.2; __utmc=23519549; __utmb=23519549"));
		headers.add(new NameValuePair("Host", "www.vesseltracker.com"));
		headers.add(new NameValuePair("Origin", "http://www.vesseltracker.com"));
		headers.add(new NameValuePair("Referer", "http://www.vesseltracker.com/en/Gallery/ShipTypes/Fishing_boats.html?dir=0"));
		headers.add(new NameValuePair("User-Agent", "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31"));

		return headers;
	}
	
	@Test
	public void test() throws Throwable {
//		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
//		System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
//		System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
//		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "debug");
		
		String MMSI = "211550000";
		
		HTTPHelper helper = new HTTPHelper();
		
		HTTPRequestMetadata meta = helper.fetchURLAsBytes(this.buildPost(MMSI), "http://www.vesseltracker.com/app", this.buildHeaders(MMSI), true, 10000);
		
		System.out.println(meta.getResponseCode());
		
		for(Header header : meta.getHeaders()) {
			System.out.println(header.getName() + ": " + header.getValue());
		}
		
		System.out.println(new String(meta.getContent(), "UTF-8"));
	}
}
