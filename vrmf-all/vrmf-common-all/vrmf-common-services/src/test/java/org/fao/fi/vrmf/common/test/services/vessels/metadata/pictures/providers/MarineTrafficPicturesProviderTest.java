/**
 * (c) 2013 FAO / UN (project: vrmf-common)
 */
package org.fao.fi.vrmf.common.test.services.vessels.metadata.pictures.providers;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.MarineTrafficPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationRequest;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationResponseData;
import org.fao.fi.vrmf.common.tools.lexical.processors.queue.impl.VesselNameSimplifier;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2013
 */
@Ignore
public class MarineTrafficPicturesProviderTest extends AbstractPicturesProviderTest<MarineTrafficPicturesProvider> {
	protected MarineTrafficPicturesProvider getProvider() throws Throwable {
		MarineTrafficPicturesProvider provider = new MarineTrafficPicturesProvider();
		provider.setVesselNamesSimplifier(new VesselNameSimplifier());
		provider.setHttpHelper(new HTTPHelper());

		return provider;
	}

	@Test
	public void  testRetrievementByIMO() throws Throwable {
		Assume.assumeTrue(this.isAvailable());
		
		Collection<VesselPicturesIdentificationResponseData> result = this.getProvider().searchPictures(new VesselPicturesIdentificationRequest(new String[] { "6825907" }, null, null, null, null, null)).getResults();

		Assume.assumeNotNull(result);
		Assert.assertFalse("Result must not be empty", result.isEmpty());
		Assert.assertEquals("Result must contain one entry", 1, result.size());

		List<VesselPicturesIdentificationResponseData> asList = new ArrayList<VesselPicturesIdentificationResponseData>(result);
		VesselPicturesIdentificationResponseData item = asList.get(0);

		Assert.assertNotNull(item);
		Assert.assertNotNull(item.getIDs());
		Assert.assertTrue(!item.getIDs().isEmpty());
		Assert.assertTrue("Returned pictures must be more than (or equal to) 2 items", item.getIDs().size() >= 2);

		for(String ID : item.getIDs()) {
			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
		}
	}

	@Test
	public void  testRetrievementByMMSI() throws Throwable {
		Assume.assumeTrue(this.isAvailable());
		
		Collection<VesselPicturesIdentificationResponseData> result = this.getProvider().searchPictures(new VesselPicturesIdentificationRequest(null, null, null, null, new String[] { "258559000" }, null)).getResults();

		Assume.assumeNotNull(result);
		Assert.assertFalse("Result must not be empty", result.isEmpty());
		Assert.assertEquals("Result must contain one entry", 1, result.size());

		List<VesselPicturesIdentificationResponseData> asList = new ArrayList<VesselPicturesIdentificationResponseData>(result);
		VesselPicturesIdentificationResponseData item = asList.get(0);

		Assert.assertNotNull(item);
		Assert.assertNotNull(item.getIDs());
		Assert.assertTrue(!item.getIDs().isEmpty());
		Assert.assertTrue("Returned pictures must be more than (or equal to) 7 items", item.getIDs().size() >= 7);

		for(String ID : item.getIDs()) {
			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
		}
	}

	@Test
	public void  testRetrievementByName() throws Throwable {
		Assume.assumeTrue(this.isAvailable());
		
		Collection<VesselPicturesIdentificationResponseData> result = this.getProvider().searchPictures(new VesselPicturesIdentificationRequest(null, null, new String[] { "GOTTSKAR" }, null, null, null)).getResults();

		Assume.assumeNotNull(result);
		Assert.assertFalse("Result must not be empty", result.isEmpty());
		Assert.assertEquals("Result must contain one entry", 1, result.size());

		List<VesselPicturesIdentificationResponseData> asList = new ArrayList<VesselPicturesIdentificationResponseData>(result);
		VesselPicturesIdentificationResponseData item = asList.get(0);

		Assert.assertNotNull(item);
		Assert.assertNotNull(item.getIDs());
		Assert.assertTrue(!item.getIDs().isEmpty());
		Assert.assertTrue("Returned pictures must be more than (or equal to) 2 items", item.getIDs().size() >= 2);

		for(String ID : item.getIDs()) {
			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
		}

//		item = asList.get(1);
//
//		Assert.assertNotNull(item);
//		Assert.assertNotNull(item.getIDs());
//		Assert.assertTrue(!item.getIDs().isEmpty());
//		Assert.assertTrue("Returned pictures must be more than (or equal to) 2 items", item.getIDs().size() >= 2);
//
//		for(String ID : item.getIDs()) {
//			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
//		}
	}

	@Test
	public void  testRetrievementByMultipleCriteria() throws Throwable {
		Assume.assumeTrue(this.isAvailable());
		
		Collection<VesselPicturesIdentificationResponseData> result = this.getProvider().searchPictures(new VesselPicturesIdentificationRequest(null, null, new String[] { "FRISIA" }, null, new String[] { "266077000" }, null)).getResults();

		Assume.assumeNotNull(result);
		Assert.assertFalse("Result must not be empty", result.isEmpty());
		Assert.assertEquals("Result must contain two entries", 2, result.size());

		List<VesselPicturesIdentificationResponseData> asList = new ArrayList<VesselPicturesIdentificationResponseData>(result);
		VesselPicturesIdentificationResponseData item = asList.get(0);

		Assert.assertNotNull(item);
		Assert.assertNotNull(item.getIDs());
		Assert.assertTrue(!item.getIDs().isEmpty());
		Assert.assertTrue("Returned pictures must be more than (or equal to) 2 items", item.getIDs().size() >= 2);

		for(String ID : item.getIDs()) {
			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
		}

		item = asList.get(1);

		Assert.assertNotNull(item);
		Assert.assertNotNull(item.getIDs());
		Assert.assertTrue(!item.getIDs().isEmpty());
		Assert.assertTrue("Returned pictures must be more than (or equal to) 2 items", item.getIDs().size() >= 2);

		for(String ID : item.getIDs()) {
			System.out.println(item.getFullURL() + URLDecoder.decode(ID, "UTF-8"));
		}
	}
}