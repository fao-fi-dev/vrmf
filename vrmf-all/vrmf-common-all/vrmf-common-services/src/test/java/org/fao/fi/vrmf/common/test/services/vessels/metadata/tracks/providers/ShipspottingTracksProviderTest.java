/**
 * (c) 2013 FAO / UN (project: vrmf-common)
 */
package org.fao.fi.vrmf.common.test.services.vessels.metadata.tracks.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.sh.utility.services.spi.AsynchronousService;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.providers.ShipspottingTracksProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.support.VesselTracksIdentificationRequest;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.support.VesselTracksIdentificationResponseData;
import org.fao.fi.vrmf.common.tools.lexical.processors.queue.impl.VesselNameSimplifier;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2013
 */
public class ShipspottingTracksProviderTest {
	private ShipspottingTracksProvider getProvider() throws Throwable {
		return this.getProvider(AsynchronousService.NO_TIMEOUT);
	}
	
	private ShipspottingTracksProvider getProvider(int timeout) throws Throwable {
		ShipspottingTracksProvider provider = new ShipspottingTracksProvider(timeout);
		provider.setHttpHelper(new HTTPHelper());
		provider.setVesselNamesSimplifier(new VesselNameSimplifier());

		return provider;
	}

	@Test
	public void  testAvailability() throws Throwable {
		Assert.assertTrue(this.getProvider().checkAvailability());
	}

	@Test
	public void  testRetrievement() throws Throwable {
		Collection<VesselTracksIdentificationResponseData> result = this.getProvider().searchPositions(new VesselTracksIdentificationRequest(new String[] { "9262833" }, null, new String[] { "Antarctic Chieftain", "Volna", "Isabel" }, null, null, null)).getResults();

		Assert.assertNotNull(result);
		Assert.assertFalse("Result must not be empty", result.isEmpty());
		Assert.assertEquals("Result must contain 1 entry", 1, result.size());
		
		List<VesselTracksIdentificationResponseData> asList = new ArrayList<VesselTracksIdentificationResponseData>(result);

		VesselTracksIdentificationResponseData item = null;
		
		for(VesselTracksIdentificationResponseData in : asList) {
			if("NAME = Isabel [ MMSI = 265652720, IRCS = SGLU, NAME = Isabel ]".equals(in.getCriteria())) {
				item = in;
				break;
			}
		}

		Assert.assertNotNull(item);
		Assert.assertNotNull(item.getPositions());
		Assert.assertTrue(!item.getPositions().isEmpty());
		Assert.assertTrue("Returned tracks must be exactly one", item.getPositions().size() == 1);
	}
	
	@Test
	public void testTimeout() throws Throwable {
		ShipspottingTracksProvider provider = this.getProvider(1); //Times out after 1 mSec.
		
		Collection<VesselTracksIdentificationResponseData> result = provider.searchPositions(new VesselTracksIdentificationRequest(new String[] { "9262833" }, null, new String[] { "Isabel" }, null, null, null)).getResults();
		
		Assert.assertNull(result);
	}
}