/**
 * (c) 2013 FAO / UN (project: vrmf-common)
 */
package org.fao.fi.vrmf.common.test.services.vessels.metadata.tracks;

import java.util.ArrayList;
import java.util.List;

import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.VesselTracksManager;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.VesselTracksProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.providers.MarineTrafficTracksProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.providers.ShipspottingTracksProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.tracks.support.VesselTracksIdentificationRequest;
import org.fao.fi.vrmf.common.tools.lexical.processors.queue.impl.VesselNameSimplifier;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2013
 */
public class VesselTracksManagerTest {
	private VesselTracksManager initialize() throws Throwable {
		HTTPHelper helper = new HTTPHelper();
		VesselNameSimplifier nameSimplifier = new VesselNameSimplifier();

		VesselTracksProvider p1 = new MarineTrafficTracksProvider();
		VesselTracksProvider p2 = new ShipspottingTracksProvider();

		List<VesselTracksProvider> providers = new ArrayList<VesselTracksProvider>();
		providers.add(p1);
		providers.add(p2);

		for(VesselTracksProvider provider : providers) {
			provider.setHttpHelper(helper);
			provider.setVesselNamesSimplifier(nameSimplifier);
		}

		VesselTracksManager manager = new VesselTracksManager();
		manager.setServiceProviders(providers);

		manager.init();

		return manager;
	}

	private VesselTracksIdentificationRequest getCriteria() {
		return new VesselTracksIdentificationRequest(
			new String[] { "5119143", "6808090", "9441867" },
			null, //new String[] { "", "", "", "", "", "" },
			new String[] { "ANABAS", "ANTONIO", "Angelo" },
			new String[] { "C6FR5", "V3HS3" },
			new String[] { "565970000", "353069000" },
			new NameValuePair[] { new NameValuePair("CCAMLR", "75711"),
								  new NameValuePair("IATTC", "8379"),
								  new NameValuePair("WCPFC", "9061") }

//			null, //new String[] { "5119143" },
//			null, //new String[] { "", "", "", "", "", "" },
//			new String[] { "Angelo" },
//			new String[] { "C6FR5" },
//			null,
//			new NameValuePair[] { new NameValuePair("CCAMLR", "75711") }
		);
	}

	@Test
	public void testAsynchronously() throws Throwable {
		System.out.println(this.initialize().invokeAsynchronously(this.getCriteria(), 10000));
	}

	@Test
	public void testSynchronously() throws Throwable {
		System.out.println(this.initialize().invoke(this.getCriteria()));
	}
}