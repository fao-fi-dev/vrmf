/**
 * (c) 2013 FAO / UN (project: vrmf-common)
 */
package org.fao.fi.vrmf.common.test.services.vessels.metadata.pictures;

import java.util.ArrayList;
import java.util.List;

import org.fao.fi.sh.utility.common.helpers.beans.io.HTTPHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesManager;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.CCAMLRPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.IATTCPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.MagramaPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.MarineTrafficPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.ShipspottingPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.WCPFCPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.providers.WikimediaPicturesProvider;
import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.support.VesselPicturesIdentificationRequest;
import org.fao.fi.vrmf.common.tools.lexical.processors.queue.impl.VesselNameSimplifier;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 May 2013
 */
public class VesselPicturesManagerTest {
	private VesselPicturesManager initialize() throws Throwable {
		HTTPHelper helper = new HTTPHelper();

		VesselPicturesProvider p1 = new CCAMLRPicturesProvider();
		VesselPicturesProvider p2 = new IATTCPicturesProvider();
		VesselPicturesProvider p3 = new WCPFCPicturesProvider();
		VesselPicturesProvider p4 = new WikimediaPicturesProvider();
		VesselPicturesProvider p5 = new MarineTrafficPicturesProvider();
		VesselPicturesProvider p6 = new ShipspottingPicturesProvider();
		VesselPicturesProvider p7 = new MagramaPicturesProvider();

		List<VesselPicturesProvider> providers = new ArrayList<VesselPicturesProvider>();
		providers.add(p1);
		providers.add(p2);
		providers.add(p3);
		providers.add(p4);
		providers.add(p5);
		providers.add(p6);
		providers.add(p7);

		VesselNameSimplifier simplifier = new VesselNameSimplifier();
		
		for(VesselPicturesProvider provider : providers) {
			provider.setHttpHelper(helper);
			provider.setVesselNamesSimplifier(simplifier);
		}

		VesselPicturesManager manager = new VesselPicturesManager();
		manager.setServiceProviders(providers);
		
		manager.init();

		return manager;
	}

	private VesselPicturesIdentificationRequest getCriteria() {
		return new VesselPicturesIdentificationRequest(
			new String[] { "5119143", "6808090", "9441867" },
			new String[] { "ESP000021633" },
			new String[] { "ANABAS", "ANTONIO", "Angelo" },
			new String[] { "C6FR5", "V3HS3" },
			new String[] { "565970000", "353069000" },
			new NameValuePair[] { new NameValuePair("CCAMLR", "75711"),
								  new NameValuePair("IATTC", "8379"),
								  new NameValuePair("WCPFC", "9061") }

//			null, //new String[] { "5119143" },
//			null, //new String[] { "", "", "", "", "", "" },
//			new String[] { "Angelo" },
//			new String[] { "C6FR5" },
//			null,
//			new NameValuePair[] { new NameValuePair("CCAMLR", "75711") }
		);
	}

	@Test
	public void testAsynchronously() throws Throwable {
		System.out.println(this.initialize().invokeAsynchronously(this.getCriteria(), 10000));
	}

	@Test
	public void testSynchronously() throws Throwable {
		System.out.println(this.initialize().invoke(this.getCriteria()));
	}

}