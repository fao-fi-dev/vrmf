/**
 * (c) 2014 FAO / UN (project: vrmf-common)
 */
package org.fao.fi.vrmf.common.test.services.vessels.metadata.pictures.providers;

import org.fao.fi.vrmf.common.services.vessels.metadata.pictures.VesselPicturesProvider;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 5 Feb 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 5 Feb 2014
 */
abstract public class AbstractPicturesProviderTest<P extends VesselPicturesProvider> {
	abstract protected P getProvider() throws Throwable;
	
	@Test
	public void testAvailability() throws Throwable {
		this.getProvider().checkAvailability();
	}
	
	protected boolean isAvailable() throws Throwable {
		return this.getProvider().isAvailable() || this.getProvider().checkAvailability();
	}
}
