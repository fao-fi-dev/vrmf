/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.servlets.resource.management;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.sh.utility.core.helpers.singletons.io.MD5Helper;
import org.fao.fi.vrmf.common.j2ee.servlets.annotations.ClientSideCachingConfiguration;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.OutputEncodingConstants;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.RequestHeadersConstants;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.ResponseHeadersConstants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Feb 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Feb 2011
 */
@ClientSideCachingConfiguration(expirationTime=86400)
public class ResourcesProviderServlet extends AbstractResourcesManagerServlet {	
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5535109145889264967L;
	
	/**
	 * Class constructor
	 */
	public ResourcesProviderServlet() throws Throwable {
		super();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String contextPath = request.getContextPath();
		String requestURI = request.getRequestURI();
		String URI = requestURI.substring(contextPath.length());
		String realPath = this.getServletContext().getRealPath(URI);		
		
		String ifNoneMatchHeader = request.getHeader(RequestHeadersConstants.REQUEST_HEADER_IF_NONE_MATCH);
		
		try {
			File resource = new File(realPath);
			
			if(resource.exists() && resource.isFile()) {	
				String eTag = "\"" + MD5Helper.digest(new StringBuilder(realPath).append(resource.lastModified()).toString()) + "\"";
				
				if(eTag.equals(ifNoneMatchHeader)) {
					this._log.debug("Resource " + URI + " was not modified. Sending a 304...");
					response.setStatus(HttpServletResponse.SC_NOT_MODIFIED /* 304 - Not Modified */);
				} else {
					response.addHeader(ResponseHeadersConstants.RESPONSE_HEADER_ETAG, eTag);
					
					FileInputStream fis = new FileInputStream(realPath);
					
					String MIMEType = this.MIME_TYPES.getContentType(resource);
					
					boolean isText = MIMEType != null && MIMEType.startsWith("text/");
					
					if(MIMEType != null)
						response.setContentType(MIMEType);
					
					response.setContentLength((int)resource.length());
	
					byte[] buffer = new byte[32768];
					int len = -1;
	
					if(isText) {
						OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream(), OutputEncodingConstants.DEFAULT_CHARSET);
						
						while((len = fis.read(buffer)) != -1) {
							osw.write(new String(buffer, 0, len, OutputEncodingConstants.DEFAULT_CHARSET));
						}
						
						fis.close();
						osw.flush();
						osw.close();	
					} else {
						OutputStream os = response.getOutputStream();
						
						while((len = fis.read(buffer)) != -1) {
							os.write(buffer, 0, len);
						}
						
						fis.close();
						os.flush();
						os.close();	
					}
				}
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND /* 404 - Not Found */);
			}
		} catch (Throwable t) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR /* 500 - Internal Server Error */, t.getMessage());
		}
	}
}