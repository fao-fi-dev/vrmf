/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.servlets.response;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * An HTTP Servlet Response wrapper that exposes methods to retrieve status code and message.
 * 
 * See: http://stackoverflow.com/questions/1302072/how-can-i-get-the-http-status-code-out-of-a-servletresponse-in-a-servletfilter
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 Nov 2012
 */
public class StatusExposingServletResponse extends HttpServletResponseWrapper {
	private int _HTTPStatus = SC_OK;
	private String _HTTPStatusMessage;

	public StatusExposingServletResponse(HttpServletResponse response) {
		super(response);
	}

	@Override
	public void sendError(int sc) throws IOException {
		this._HTTPStatus = sc;
		super.sendError(sc);
	}

	@Override
	public void sendError(int sc, String msg) throws IOException {
		super.sendError(sc, msg);
		
		this._HTTPStatus = sc;
		this._HTTPStatusMessage = msg;
	}

	@Override
	public void setStatus(int sc) {
		super.setStatus(sc);

		this._HTTPStatus = sc;
	}

	@Override
	public void sendRedirect(String location) throws IOException {
		super.sendRedirect(location);

		this._HTTPStatus = SC_MOVED_TEMPORARILY;
	}

	@Override
	public void reset() {
		super.reset();
		
		this._HTTPStatus = SC_OK;
		this._HTTPStatusMessage = null;
	}

	@Override
	public void setStatus(int sc, String msg) {
		super.setStatus(sc, msg);
		
		this._HTTPStatusMessage = msg;
		this._HTTPStatus = sc;
	}
	
	public int getStatus() {
		return this._HTTPStatus;
	}

	/**
	 * @return the 'HTTPStatusMessage' value
	 */
	public String getStatusMessage() {
		return this._HTTPStatusMessage;
	}
}
