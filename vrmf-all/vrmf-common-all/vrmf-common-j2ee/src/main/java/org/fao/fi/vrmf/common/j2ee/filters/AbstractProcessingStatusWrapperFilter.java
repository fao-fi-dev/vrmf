/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.filters;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.vrmf.common.j2ee.servlets.response.StatusExposingServletResponse;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2012
 */
abstract public class AbstractProcessingStatusWrapperFilter<PROCESSING_STATUS extends Serializable> extends AbstractOncePerRequestFilter {
	
	/**
	 * @param request
	 * @param response
	 * @param filteringStart
	 * @return
	 */
	abstract protected PROCESSING_STATUS beforeFiltering(HttpServletRequest request, StatusExposingServletResponse response, Long filteringStart);
	
	/**
	 * @param status
	 * @param request
	 * @param response
	 * @return
	 */
	abstract protected PROCESSING_STATUS afterFiltering(PROCESSING_STATUS status, HttpServletRequest request, StatusExposingServletResponse response);
	
	/**
	 * @param status
	 * @param request
	 * @param response
	 * @param filteringStart
	 * @param t
	 * @return
	 */
	abstract protected PROCESSING_STATUS onFailure(PROCESSING_STATUS status, HttpServletRequest request, StatusExposingServletResponse response, Long filteringStart, Throwable t);
	
	/**
	 * @param status
	 * @param request
	 * @param response
	 * @return
	 */
	abstract protected PROCESSING_STATUS completeFiltering(PROCESSING_STATUS status, HttpServletRequest request, StatusExposingServletResponse response);
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.web.common.filters.AbstractFilter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void internalDoFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		this._log.info("Executing " + this.getFilterName() + " internalDoFilter...");
		
		HttpServletRequest hRequest = (HttpServletRequest)request;
		HttpServletResponse hResponse = (HttpServletResponse)response;
		
		StatusExposingServletResponse wrappedResponse = new StatusExposingServletResponse(hResponse); 
		
		PROCESSING_STATUS status = null;
		
		long filteringStart = System.currentTimeMillis();
		
		long end, start = System.currentTimeMillis();
		
		try {
			status = this.beforeFiltering(hRequest, wrappedResponse, filteringStart);
			chain.doFilter(hRequest, wrappedResponse);
			status = this.afterFiltering(status, hRequest, wrappedResponse);
		} catch (Throwable t) {
			this._log.warn("Filtering request " + hRequest.getRequestURI() + " yield a " + t.getClass().getName() + " [ " + t.getMessage() + " ]", t);
			status = this.onFailure(status, hRequest, wrappedResponse, filteringStart, t);
		} finally {
			this.completeFiltering(status, hRequest, wrappedResponse);
			
			end = System.currentTimeMillis();
			this._log.info(this.getClass().getSimpleName() + " took " + ( end - start ) + " mSec. to execute");
		}
	}
}
