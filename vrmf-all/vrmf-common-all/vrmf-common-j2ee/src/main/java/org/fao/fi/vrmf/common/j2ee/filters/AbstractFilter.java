/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.vrmf.common.j2ee.RequestAttributesConstants;

/**
 * Base class for custom J2EE filters. Provides a common framework to possibly apply each extending filter just once per request (heavily inspired 
 * on Spring's OncePerRequestFilter (see: {@see http://srcrr.com/java/spring/3.1.0/reference/org/springframework/web/filter/OncePerRequestFilter.html}).
 * This behaviour configurable either at filter implementation level or at filter config level (the latter overrides the filter implementation behaviour).
 * 
 * Adds convenience methods to retrieve filter name and initialization parameters and log filter initialization and destroyment.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Nov 2012
 */
abstract public class AbstractFilter extends AbstractLoggingAwareClient implements Filter {
	final static private String ALREADY_FILTERED_REQUEST_ATTRIBUTE_SUFFIX = ".already.filtered";
	
	protected FilterConfig _filterConfig;
	
	protected boolean _isOncePerRequest;
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this._log.debug("Initializing filter " + this.getClass().getName());

		this._filterConfig = filterConfig;
		
		this._isOncePerRequest = this.doOncePerRequest();
		
		String oncePerRequestConfigurationParameter = this.getConfigurationParameter(FiltersConstants.VRMF_ABSTRACT_FILTER_ONCE_PER_REQUEST_CONFIGURATION_PARAMETER);
		
		if(oncePerRequestConfigurationParameter != null) {
			this._isOncePerRequest = Boolean.valueOf(oncePerRequestConfigurationParameter);
			
			this._log.info(this.getFilterName() + " is configured to be executed " + ( this._isOncePerRequest ? "once" : "multiple times" ) + " per request");
		} else {
			this._log.info(this.getFilterName() + " is inherently configured to be executed " + ( this._isOncePerRequest ? "once" : "multiple times" ) + " per request");
		}
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		this._log.debug("Destroying filter " + this.getClass().getName());
	}
	
	/**
	 * @param parameterName
	 * @param filterConfig
	 * @return
	 */
	protected String getConfigurationParameter(String parameterName) {
		String filterConfigValue = this._filterConfig.getInitParameter(parameterName);
		String servletContextValue = this._filterConfig.getServletContext().getInitParameter(parameterName);

		String value = filterConfigValue != null ? filterConfigValue : servletContextValue;
		
		if(value != null) {
			this._log.info("Filter " + this.getFilterName() + " requested init parameter '" + parameterName + "'");
			this._log.info("Returning '" + value + "' as value for init parameter '" + parameterName + "' retrieved from " + ( filterConfigValue != null ? "its" : "the servlet context" ) + " init parameters");
		}
		
		return value;
	}
	
	/**
	 * @return
	 */
	protected String getFilterName() {
		if(this._filterConfig == null || this._filterConfig.getFilterName() == null)
			return this.getClass().getSimpleName();
		
		return this._filterConfig.getFilterName();
	}
	
	/**
	 * @return
	 */
	private String getAlreadyFilteredAttributeName() {
		return FiltersConstants.VRMF_FILTERS_REQUEST_ATTRIBUTES_PREFIX + this.getFilterName() + ALREADY_FILTERED_REQUEST_ATTRIBUTE_SUFFIX;
	}
	
	/**
	 * @param request
	 * @return
	 */
	private boolean alreadyFiltered(HttpServletRequest request) {
		return Boolean.TRUE.equals(request.getAttribute(this.getAlreadyFilteredAttributeName()));
	}
	
	/**
	 * @param request
	 */
	private void setAlreadyFiltered(HttpServletRequest request) {
		request.setAttribute(this.getAlreadyFilteredAttributeName(), Boolean.TRUE);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	final public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest hRequest = (HttpServletRequest)request;
		
		this._log.info("Filtering request for URI " + hRequest.getRequestURI() + " via " + this.getFilterName());

		if(this._isOncePerRequest && this.alreadyFiltered(hRequest))
			chain.doFilter(request, response);
		else {
			try {
				if(this._isOncePerRequest)
					this.setAlreadyFiltered(hRequest);

				this.internalDoFilter(request, response, chain);
			} finally {
				if(this._isOncePerRequest)
					hRequest.removeAttribute(this.getAlreadyFilteredAttributeName());
			}
		}
	}

	/**
	 * @return true if this filter must execute just once per request (by default), otherwise false.
	 * The actual filter behaviour might be different according to its configuration parameters.
	 */
	protected boolean doOncePerRequest() {
		return FiltersConstants.VRMF_DO_FILTER_MULTIPLE_TIMES_PER_REQUEST;
	}
	
	protected String stripContext(String context, String URL) {
		return URL == null ? 
					null : 
						context == null || URL.indexOf(context) < 0 ? 
							URL : 
								URL.substring(URL.indexOf(context) + context.length()).trim();
	}
	
	protected String passThroughOriginalURL(HttpServletRequest request) {
		return this.passThroughOriginalURL(request, null, true);
	}
	
	protected String passThroughOriginalURL(HttpServletRequest request, String explicitURL) {
		return this.passThroughOriginalURL(request, explicitURL, true);
	}
	
	protected String passThroughOriginalURL(HttpServletRequest request, String explicitURL, boolean stripContext) {
		String context = request.getContextPath() + "/";
		String URL = explicitURL != null ? explicitURL : request.getRequestURL().toString();
		String strippedURL = stripContext ? this.stripContext(context, URL) : URL;

		this._log.debug("Passing original" + ( stripContext ? ", context-stripped" : "" ) + " URL '" + strippedURL + "' through request attribute " + RequestAttributesConstants.ORIGINAL_URL_REQUEST_ATTRIBUTE);

		request.setAttribute(RequestAttributesConstants.ORIGINAL_URL_REQUEST_ATTRIBUTE, strippedURL);
		
		return strippedURL;
	}
	
	protected String resetOriginalURL(HttpServletRequest request) {
		String current = this.retrieveOriginalURL(request);
		
		request.removeAttribute(RequestAttributesConstants.ORIGINAL_URL_REQUEST_ATTRIBUTE);
		
		return current;
	}

	protected String retrieveOriginalURL(HttpServletRequest request) {
		return (String)request.getAttribute(RequestAttributesConstants.ORIGINAL_URL_REQUEST_ATTRIBUTE);
	}
	
	/**
	 * To be implemented by extending filters.
	 * 
	 * @param request
	 * @param response
	 * @param chain
	 * @throws IOException
	 * @throws ServletException
	 */
	abstract protected void internalDoFilter(ServletRequest request, ServletResponse response, FilterChain chain)  throws IOException, ServletException;
}