/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.filters.content;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.core.model.i18n.LocalizationInfo;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.j2ee.RequestAttributesConstants;
import org.fao.fi.vrmf.common.j2ee.filters.AbstractOncePerRequestFilter;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.RequestHeadersConstants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Mar 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Mar 2012
 */
@Named
@Singleton
public class BrowserConfigAwareLocalizerFilter extends AbstractOncePerRequestFilter implements LocalizerFilterConstants {
	@Inject @Named("vrmf.localizationInfo")
	private LocalizationInfo _localizationInfo;

	private Pattern _interceptorPattern;
	private Pattern _resourceAndQueryStringPattern;

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.j2ee.filters.AbstractFilter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		super.init(filterConfig);

		if(this._localizationInfo != null && this._localizationInfo.getAllAvailableLanguages().length > 0) {
			final String pattern = this._localizationInfo.getLocalizationURLPattern();
			
			this._interceptorPattern = Pattern.compile(pattern);
			this._log.warn("Interceptor pattern = {}", pattern);
		}
	}

	protected Matcher getURLParser(String URL) {
		String sanitized = StringsHelper.trim(URL);

		return sanitized == null || this._interceptorPattern == null ? null : this._interceptorPattern.matcher(sanitized);
	}

	/**
	 * Currently unused. Retrieves the first accepted language (as specified by the {@link RequestHeadersConstants.REQUEST_HEADER_ACCEPT_LANGUAGE} request header)
	 * and returns it if it is contained in the list of available languages.
	 *
	 * @param request
	 * @return
	 */
	protected String autodetectLanguageFromRequestHeader(HttpServletRequest request) {
		String acceptLanguageHeader = StringsHelper.trim(request.getHeader(RequestHeadersConstants.REQUEST_HEADER_ACCEPT_LANGUAGE));

		List<String> availableLanguages = this._localizationInfo.getAllAvailableLanguages() == null ? new ArrayList<String>() : Arrays.asList(this._localizationInfo.getAllAvailableLanguages());

		if(acceptLanguageHeader == null)
			return null;

		acceptLanguageHeader = StringsHelper.removeAllSpaces(acceptLanguageHeader);

		ListSet<String> currentLanguages = new ListSet<String>();

		double q;

		for(String part : acceptLanguageHeader.split("\\,|\\;")) {
			if(part.startsWith("q=")) {
				q = Double.parseDouble(part.replace("q=", "").replace(";", ""));

				if(currentLanguages.retainAll(availableLanguages)) {
					this._log.info("Language '" + currentLanguages.get(0) + "' is accepted by the browser with q=" + q);

					return currentLanguages.get(0);
				}

				currentLanguages = new ListSet<String>();
			} else {
				currentLanguages.add(part.replaceAll("\\_[a-zA-Z]+",  ""));
			}
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void internalDoFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		this._log.info("Executing " + this.getFilterName() + " internalDoFilter...");

		long end, start = System.currentTimeMillis();

		HttpServletRequest hRequest = (HttpServletRequest)request;
		HttpServletResponse hResponse = (HttpServletResponse)response;

		String context = hRequest.getContextPath() + "/";
		String URL = hRequest.getRequestURL().toString();
		String updatedURL = URL;
		
		this._log.debug("Intercepted URI: " + hRequest.getRequestURI());
		this._log.debug("Intercepted URL: " + URL);
		this._log.debug("Context: " + hRequest.getContextPath());

		String currentLanguage = (String)request.getAttribute(RequestAttributesConstants.CURRENT_LANGUAGE_REQUEST_ATTRIBUTE);
		String defaultLanguage = this._localizationInfo.getDefaultLanguage();
		String selectedLanguage = defaultLanguage;

		request.setAttribute(RequestAttributesConstants.DEFAULT_LANGUAGE_REQUEST_ATTRIBUTE, defaultLanguage);
		request.removeAttribute(RequestAttributesConstants.CURRENT_LANGUAGE_REQUEST_ATTRIBUTE);

		Matcher parser = this.getURLParser(URL);

		boolean URLSetsLanguage = parser.matches();
		boolean alreadySetLanguage = currentLanguage != null && !"".equals(currentLanguage);

		selectedLanguage = alreadySetLanguage ? currentLanguage : selectedLanguage;

		request.setAttribute(RequestAttributesConstants.CURRENT_LANGUAGE_REQUEST_ATTRIBUTE, selectedLanguage);
		
		if(!URLSetsLanguage) {
			boolean isErrorPage = URL.indexOf("error.jsp") >= 0;
			
			if(!isErrorPage) {
				Matcher resourceParser = this._resourceAndQueryStringPattern.matcher(URL);
				
				if(resourceParser.matches()) {
					boolean addPreSlash = !resourceParser.group(1).endsWith("/");
					boolean addPostSlash = URL.endsWith("/");
					
					updatedURL = resourceParser.replaceAll("$1" + ( addPreSlash ? "/" : "" ) + defaultLanguage + "$4$5");
					
					if(addPostSlash)
						updatedURL += "/";

						this._log.warn("No language set: updating URL {} to include default language and redirecting to {}", URL, updatedURL);
					
						hResponse.sendRedirect(updatedURL);
					} 
			} else {
				chain.doFilter(hRequest, hResponse);
			}
		} else {
			selectedLanguage = parser.group(3);

			updatedURL = this.stripContext(context, parser.replaceAll("$1$4"));

			request.setAttribute(RequestAttributesConstants.CURRENT_LANGUAGE_REQUEST_ATTRIBUTE, selectedLanguage);

			this.passThroughOriginalURL(hRequest, updatedURL);

			this._log.debug("Forwarding request for " + URL + " to /" + updatedURL);
			
			hRequest.getRequestDispatcher("/" + updatedURL).forward(request, response);
			
			chain.doFilter(hRequest, hResponse);
		}

		this._log.debug("Request has been filtered via " + this.getClass().getSimpleName());

		end = System.currentTimeMillis();

		this._log.debug(this.getClass().getSimpleName() + " took " + ( end - start ) + " mSec. to execute");
	}
}