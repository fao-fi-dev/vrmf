/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.servlets;

import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.common.j2ee.servlets.annotations.ClientSideCachingConfiguration;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.RequestResponseHeadersConstants;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Dec 2010
 */
/* @ClientSideCachingConfiguration(expirationTime=300) */
abstract public class CommonServlet extends HttpServlet {
	private static final long serialVersionUID = -3406913494075484447L;
	
	protected Logger _log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * Class constructor
	 */
	public CommonServlet() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this._log.debug("Servlet " + this.getClass().getName() + " : serving request for " + request.getRequestURI() + " by " + request.getRemoteAddr());

		String clientRequestTimestamp = request.getHeader(RequestResponseHeadersConstants.HEADER_CLIENT_REQUEST_TIMESTAMP);
		
		if(this.getClass().isAnnotationPresent(ClientSideCachingConfiguration.class))
			ServletsHelper.setMaxAgeHeader(response, this.getClass().getAnnotation(ClientSideCachingConfiguration.class).expirationTime());
		
		if(StringsHelper.trim(clientRequestTimestamp) != null)
			response.setHeader(RequestResponseHeadersConstants.HEADER_CLIENT_REQUEST_TIMESTAMP, clientRequestTimestamp);
		
		super.service(request, response);
	}	
	
	/**
	 * @param outputStreamWriter
	 * @throws IOException
	 */
	final protected void complete(OutputStreamWriter outputStreamWriter) throws IOException {
		ServletsHelper.complete(this.getClass(), outputStreamWriter);
	}
}