/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.filters.meta.requests;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Dec 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 Dec 2012
 */
public interface HTTPHeadersCopierFilterConstants {
	String VRMF_SEARCH_REQUEST_TOKEN_HEADER = "vrmf.search.request.token";
	String[] DEFAULT_HEADERS_TO_COPY = { VRMF_SEARCH_REQUEST_TOKEN_HEADER };
	
	String HEADERS_TO_COPY_CONFIGURATION_PARAMETER = "headersToCopy";
}
