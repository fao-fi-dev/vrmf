/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.context.listeners;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.core.helpers.beans.text.properties.FilterableUTF8Properties;
import org.fao.fi.sh.utility.core.helpers.singletons.io.ResourcesHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.common.j2ee.context.constants.PropertiesInitializerListenerConstants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Mar 2013
 */
public class SystemPropertiesInitializerListener extends AbstractLoggingAwareClient implements ServletContextListener {
	private String _propertiesResources;
	
	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext context = sce.getServletContext();
		this._propertiesResources = context.getInitParameter(PropertiesInitializerListenerConstants.PROPERTIES_RESOURCES_INIT_PARAMETER);
		
		if(this._propertiesResources == null)
			this._log.warn("No properties resources specified via context init parameter '" + PropertiesInitializerListenerConstants.PROPERTIES_RESOURCES_INIT_PARAMETER + "'");
		else {
			this._log.info("Using " + this._propertiesResources + " as properties resources location");
			
			String[] resources = this._propertiesResources.split("\\,");
			
			Pattern prefixPattern = Pattern.compile("^([a-zA-Z]+\\:)(.+)");
			Matcher prefixMatcher;
			String prefix;
			for(String resource : resources) {
				resource = StringsHelper.trim(resource);
				
				if(resource != null) {
					prefixMatcher = prefixPattern.matcher(resource);
					
					if(prefixMatcher.matches()) {
						prefix = prefixMatcher.group(1);
						resource = prefixMatcher.group(2);
					} else
						prefix = "file:";
					
					this.initialize(context, prefix, resource);
				}
			}
		}
	}
	
	private void initialize(ServletContext context, String prefix, String resource) {
		try {
			boolean isClasspath = "classpath:".equals(prefix);
			boolean isFile = "file:".equals(prefix);
			boolean isURL = "url:".equals(prefix);
			boolean isJNDI = "jndi:".equals(prefix);
			
			InputStream stream;
			
			if(isClasspath) {
				stream = ResourcesHelper.getClasspathResourceAsStream(resource);
			} else if(isFile) {
				stream = new FileInputStream(resource);
			} else if(isURL) {
				stream = new URL(resource).openStream();
			} else if(isJNDI) {
				InitialContext initialContext = new InitialContext();
				
				NamingEnumeration<NameClassPair> list = initialContext.list(resource);
				
				StringBuilder properties = new StringBuilder();
				NameClassPair entry;
				String className;
				Object value;
				boolean isPrimitive;
				
				while(list.hasMore()) {
					entry = list.next();
					
					className = entry.getClassName();
					
					if(className != null) {
						value = initialContext.lookup(resource + entry.getName());

						if(value != null && !Context.class.isAssignableFrom(value.getClass())) {
							isPrimitive = ObjectsHelper.isPrimitive(null, value); 
							properties.append("#JNDI resource: ").append(prefix).append(entry.getName()).append("\n");
							properties.append("#Bound type: ").append(value.getClass().getName()).append(" [ ").append(isPrimitive ? "primitive" : "non primitive").append(" ]").append("\n"); 
							properties.append(entry.getName().replaceAll("\\/", ".")).append("=").append(value.toString()).append("\n");
						}
					}
					
				}
				
				this._log.debug("Assembled properties file:");
				this._log.debug(properties.toString());
				
				stream = new ByteArrayInputStream( properties.toString().getBytes("UTF-8"));
			} else {
				throw new RuntimeException("Invalid prefix " + prefix);
			}
			
			Properties props = new FilterableUTF8Properties();
			props.load(stream);
			
			this._log.info(props.size() + " properties have been successfully loaded from resource " + prefix + resource);
			
			Properties systemProps = System.getProperties();
			
			boolean overwrites;
			Object overwritten;
			String message;
			for(Object key : props.keySet()) {
				overwrites = systemProps.containsKey(key);
				overwritten = overwrites ? systemProps.get(key) : null;
				
				message = "Property: " + key + " := " + props.getProperty((String)key) + 
						  (
								overwrites ? 
									" (overwrites corresponding system property with current value set to " + overwritten + ")" 
								: 
									"" 
						  );
				
				this._log.info(message);
			}
			
			System.getProperties().putAll(props);
			
			try {
				if(stream != null) stream.close();
			} catch(IOException IOe) {
				this._log.warn("Unable to close stream", IOe);
			}
		} catch (Throwable t) {
			this._log.warn("Unable to read properties resource from " + resource + ". Reason: " + t.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}
}