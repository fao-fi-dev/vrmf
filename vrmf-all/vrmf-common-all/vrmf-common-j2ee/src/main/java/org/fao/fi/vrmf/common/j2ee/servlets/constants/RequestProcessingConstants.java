/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.servlets.constants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Feb 2013
 */
public interface RequestProcessingConstants {
	boolean CONVERT_PARAMETER_VALUE_TO_UPPERCASE 	  = true;
	boolean DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE = false;
	
	String DEFAULT_QUERY_STRING_PARAMETER 	  = "q";
	String FOLLOW_REDIRECT_REQUEST_PARAMETER  = "followRedirect";
}
