/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.servlets.constants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Feb 2013
 */
public interface RequestHeadersConstants {
	String REQUEST_HEADER_REFERER 		  = "Referer";
	String REQUEST_HEADER_USER_AGENT	  = "User-Agent";
	String REQUEST_HEADER_ACCEPT_ENCODING = "Accept-Encoding";
	String REQUEST_HEADER_ACCEPT_LANGUAGE = "Accept-Language";
	
	String REQUEST_HEADER_IF_NONE_MATCH	  = "If-None-Match";
	
	String REQUEST_HEADER_X_FORWARDED_FOR  	   = "x-forwarded-for";
	String REQUEST_HEADER_X_FORWARDED_HOST 	   = "x-forwarded-host";
	String REQUEST_HEADER_X_FORWARDED_BASE_URI = "x-forwarded-base-uri"; //NON-STANDARD HEADER
}
