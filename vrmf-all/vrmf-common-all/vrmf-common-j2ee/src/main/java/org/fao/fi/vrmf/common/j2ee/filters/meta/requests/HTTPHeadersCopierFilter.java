/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.filters.meta.requests;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.j2ee.filters.AbstractFilter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Jul 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Jul 2012
 */
@Named
@Singleton
public class HTTPHeadersCopierFilter extends AbstractFilter implements HTTPHeadersCopierFilterConstants {
	private String[] _headersToCopy;
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.web.common.filters.AbstractFilter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void internalDoFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		this._log.info("Executing " + this.getFilterName() + " internalDoFilter...");
		
		long end, start = System.currentTimeMillis();
		Map<String, String> requestHeaders = new HashMap<String, String>();
		
		HttpServletRequest hRequest = (HttpServletRequest)request;
		HttpServletResponse hResponse = (HttpServletResponse)response;
		
		try {
			String value = null;
			for(String header : this._headersToCopy) {
				value = hRequest.getHeader(header);
				
				if(value != null)
					requestHeaders.put(header, value);
			}
			
			if(!requestHeaders.isEmpty()) {
				boolean overwrite = false;
				
				for(String header : this._headersToCopy) {
					value = requestHeaders.get(header);
					overwrite = hResponse.containsHeader(header);
					
					if(value != null) {
						if(overwrite)
							this._log.warn("Overwriting response header " + header + " with value " + value);
						
						hResponse.setHeader(header, value);
					} else {
						if(overwrite && requestHeaders.containsKey(header)) {
							this._log.warn("Overwriting response header " + header + " with NULL");
							
							hResponse.setHeader(header, null);
						}
					}
				}
			}
			
			chain.doFilter(hRequest, hResponse);
		} finally {
			end = System.currentTimeMillis();
			
			this._log.info(this.getFilterName() + " took " + ( end - start ) + " mSec. to execute");
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.web.common.filters.AbstractFilter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		super.init(filterConfig);
		
		String originalHeadersToCopy = this.getConfigurationParameter(HEADERS_TO_COPY_CONFIGURATION_PARAMETER);
		String headersToCopy = StringsHelper.trim(originalHeadersToCopy); 

		if(originalHeadersToCopy == null) {
			this._headersToCopy = DEFAULT_HEADERS_TO_COPY;
			
			this._log.info(this.getFilterName() + " has been configured in order to pass following headers from the initial intercepted REQUEST to the final RESPONSE: " + CollectionsHelper.serializeArray(this._headersToCopy));
		} else if(headersToCopy == null) {
			this._headersToCopy = DEFAULT_HEADERS_TO_COPY;
			
			this._log.warn(this.getFilterName() + " has been configured in order to pass headers " + originalHeadersToCopy + " from the initial intercepted REQUEST to the final RESPONSE, but this parameter doesn't resolve on a non-empty set of header. Using " + CollectionsHelper.serializeArray(this._headersToCopy) + " as default") ;
		} else {
			Collection<String> updatedHeadersToCopy = new ListSet<String>();
			
			String updatedHeaderToCopy;
			for(String headerToCopy : headersToCopy.split("\\,")) {
				updatedHeaderToCopy = StringsHelper.trim(headerToCopy);
				
				if(updatedHeaderToCopy != null)
					updatedHeadersToCopy.add(updatedHeaderToCopy);
			}
			
			if(updatedHeadersToCopy.isEmpty()) {
				this._log.warn(this.getFilterName() + " was configured in order to pass headers " + headersToCopy + " from the initial intercepted REQUEST to the final RESPONSE, but the configured values don't yield any valid set of headers. Using " + CollectionsHelper.serializeArray(this._headersToCopy) + " as default");
			} else {
				this._headersToCopy = updatedHeadersToCopy.toArray(new String[updatedHeadersToCopy.size()]);
				
				this._log.info(this.getFilterName() + " has been configured in order to pass following headers from the initial intercepted REQUEST to the final RESPONSE: " + CollectionsHelper.serializeArray(this._headersToCopy));
			}
		}
	}
}