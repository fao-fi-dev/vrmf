/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.filters.security.requests;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2012
 */
public class RequestStatus extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 835379016548179118L;
	
	private Integer _requestMetadataID;
	private Long _requestStartTimestamp;
	private Long _requestEndTimestamp;
	private Integer _responseCode;
	private String _responseMessage;
	
	/**
	 * Class constructor
	 *
	 * @param requestMetadataID
	 * @param requestStartTimestamp
	 * @param requestEndTimestamp
	 * @param responseCode
	 * @param responseMessage
	 */
	public RequestStatus(Integer requestMetadataID, Long requestStartTimestamp, Long requestEndTimestamp, Integer responseCode, String responseMessage) {
		super();
		this._requestMetadataID = requestMetadataID;
		this._requestStartTimestamp = requestStartTimestamp;
		this._requestEndTimestamp = requestEndTimestamp;
		this._responseCode = responseCode;
		this._responseMessage = responseMessage;
	}

	/**
	 * @return the 'requestMetadataID' value
	 */
	public Integer getRequestMetadataID() {
		return this._requestMetadataID;
	}
	
	/**
	 * @param requestMetadataID the 'requestMetadataID' value to set
	 */
	public void setRequestMetadataID(Integer requestMetadataID) {
		this._requestMetadataID = requestMetadataID;
	}
	
	/**
	 * @return the 'requestStartTimestamp' value
	 */
	public Long getRequestStartTimestamp() {
		return this._requestStartTimestamp;
	}
	
	/**
	 * @param requestStartTimestamp the 'requestStartTimestamp' value to set
	 */
	public void setRequestStartTimestamp(Long requestStartTimestamp) {
		this._requestStartTimestamp = requestStartTimestamp;
	}
	
	/**
	 * @return the 'requestEndTimestamp' value
	 */
	public Long getRequestEndTimestamp() {
		return this._requestEndTimestamp;
	}
	
	/**
	 * @param requestEndTimestamp the 'requestEndTimestamp' value to set
	 */
	public void setRequestEndTimestamp(Long requestEndTimestamp) {
		this._requestEndTimestamp = requestEndTimestamp;
	}

	/**
	 * @return the 'responseCode' value
	 */
	public Integer getResponseCode() {
		return this._responseCode;
	}

	/**
	 * @param responseCode the 'responseCode' value to set
	 */
	public void setResponseCode(Integer responseCode) {
		this._responseCode = responseCode;
	}

	/**
	 * @return the 'responseMessage' value
	 */
	public String getResponseMessage() {
		return this._responseMessage;
	}

	/**
	 * @param responseMessage the 'responseMessage' value to set
	 */
	public void setResponseMessage(String responseMessage) {
		this._responseMessage = responseMessage;
	}
}
