/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.exceptions.authentication;

import javax.servlet.ServletException;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Mar 2011
 */
public class LoggedUserFilterException extends ServletException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5640778835086988638L;

	/**
	 * Class constructor
	 */
	public LoggedUserFilterException() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public LoggedUserFilterException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public LoggedUserFilterException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public LoggedUserFilterException(Throwable cause) {
		super(cause);
	}	
}