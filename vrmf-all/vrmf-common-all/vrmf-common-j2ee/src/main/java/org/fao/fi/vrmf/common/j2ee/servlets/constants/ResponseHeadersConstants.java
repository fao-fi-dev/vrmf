/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.servlets.constants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Feb 2013
 */
public interface ResponseHeadersConstants {
	String RESPONSE_HEADER_CONTENT_ENCODING = "Content-Encoding";
	String RESPONSE_HEADER_CACHE_CONTROL 	= "Cache-Control";
	String RESPONSE_HEADER_EXPIRES 			= "Expires";
	String RESPONSE_HEADER_ETAG				= "ETag";
	
	//CUSTOM HEADERS
	String RESPONSE_HEADER_REDIRECT_TO				= "RedirectTo";
	String RESPONSE_HEADER_FORWARD_TO				= "ForwardTo";
	String RESPONSE_HEADER_ORIGINAL_REFERER 		= "OriginalReferer";
}
