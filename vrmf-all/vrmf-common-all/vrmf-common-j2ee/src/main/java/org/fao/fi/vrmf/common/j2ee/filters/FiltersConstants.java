/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.filters;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Mar 2013
 */
public interface FiltersConstants {
	String VRMF_FILTERS_REQUEST_ATTRIBUTES_PREFIX = "vrmf.filters.";
	
	String VRMF_ABSTRACT_FILTER_ONCE_PER_REQUEST_CONFIGURATION_PARAMETER = "oncePerRequest";
	
	boolean VRMF_DO_FILTER_ONCE_PER_REQUEST 		  = true;
	boolean VRMF_DO_FILTER_MULTIPLE_TIMES_PER_REQUEST = !VRMF_DO_FILTER_ONCE_PER_REQUEST;
}
