/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Jun 2011   faoadmin     Creation.
 *
 * @version 1.0
 * @since 20 Jun 2011
 */
public interface RequestAttributesConstants {
	String VRMF_PREFIX 									   = "vrmf.";
	String SOURCE_SPECIFIC_REQUEST_ATTRIBUTE_PREFIX		   = VRMF_PREFIX + "source.specific.";
	String AVAILABLE_REQUEST_ATTRIBUTE_PREFIX			   = VRMF_PREFIX + "available.";
	String AVAILABLE_DATA_REQUEST_ATTRIBUTE_PREFIX		   = AVAILABLE_REQUEST_ATTRIBUTE_PREFIX + "data.";
	String AVAILABLE_OTHER_REQUEST_ATTRIBUTE_PREFIX		   = AVAILABLE_REQUEST_ATTRIBUTE_PREFIX + "other.";
	
	String ORIGINAL_URL_REQUEST_ATTRIBUTE				   = VRMF_PREFIX + "original.url";
	
	String SOURCE_SPECIFIC_SOURCE_REQUEST_ATTRIBUTE 	   = SOURCE_SPECIFIC_REQUEST_ATTRIBUTE_PREFIX + "source";
	String SOURCE_SPECIFIC_SOURCE_ID_REQUEST_ATTRIBUTE 	   = SOURCE_SPECIFIC_REQUEST_ATTRIBUTE_PREFIX + "source.id";
	String SOURCE_SPECIFIC_AUTO_LOGIN_REQUEST_ATTRIBUTE    = SOURCE_SPECIFIC_REQUEST_ATTRIBUTE_PREFIX + "auto.login";
	String SOURCE_SPECIFIC_ENTRANCE_PAGE_REQUEST_ATTRIBUTE = SOURCE_SPECIFIC_REQUEST_ATTRIBUTE_PREFIX + "entrance.page";
	String AVAILABLE_DATA_SOURCES_REQUEST_ATTRIBUTE 	   = AVAILABLE_DATA_REQUEST_ATTRIBUTE_PREFIX + "sources";
	String AVAILABLE_DATA_SOURCES_STATS_REQUEST_ATTRIBUTE  = AVAILABLE_DATA_REQUEST_ATTRIBUTE_PREFIX + "sources.stats";
	String AVAILABLE_OTHER_SOURCES_REQUEST_ATTRIBUTE 	   = AVAILABLE_OTHER_REQUEST_ATTRIBUTE_PREFIX + "sources";

	String DATA_SOURCES_REQUEST_ATTRIBUTE 				   = VRMF_PREFIX + "data.sources";
	String CURRENT_LANGUAGE_REQUEST_ATTRIBUTE 			   = VRMF_PREFIX + "current.language";
	String DEFAULT_LANGUAGE_REQUEST_ATTRIBUTE 			   = VRMF_PREFIX + "default.language";
}
