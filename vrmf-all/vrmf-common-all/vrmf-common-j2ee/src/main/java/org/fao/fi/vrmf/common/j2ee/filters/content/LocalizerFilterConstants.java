/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.filters.content;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Mar 2013
 */
public interface LocalizerFilterConstants {
	String AUTODETECT_LANGUAGE_CONFIGURATION_PARAMETER = "autodetectLanguage";
}