/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.utilities;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.fao.fi.sh.utility.core.helpers.singletons.io.URLHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.j2ee.NetworkConstants;
import org.fao.fi.vrmf.common.j2ee.RequestAttributesConstants;
import org.fao.fi.vrmf.common.j2ee.context.constants.InitParameterConstants;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.OutputEncodingConstants;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.RequestHeadersConstants;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.RequestProcessingConstants;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.ResponseHeadersConstants;
import org.fao.fi.vrmf.common.models.security.BasicUser;
import org.fao.fi.vrmf.common.models.security.UserCapability;
import org.fao.fi.vrmf.common.models.security.VRMFUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Mar 2011
 */
public class ServletsHelper {
	static final public Boolean INCLUDE_OTHER_SOURCES 	   = Boolean.TRUE;
	static final public Boolean DONT_INCLUDE_OTHER_SOURCES = Boolean.FALSE;
	static final public Pattern FAO_LAN_INTERNAL_IPS_PATTERN = Pattern.compile(NetworkConstants.INTERNAL_IPS_REGEXP);

	static public Pattern LANGUAGE_LOCALIZER_PATTERN = null;

	static private transient Logger _log = null;

	static private Logger getLog() {
		if(ServletsHelper._log == null)
			ServletsHelper._log = LoggerFactory.getLogger(ServletsHelper.class);

		return ServletsHelper._log;
	}

	static public String getParameter(String parameterName, HttpServletRequest request) {
		return ServletsHelper.getParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);
	}

	static public String getParameter(String parameterName, boolean toUppercase, HttpServletRequest request) {
		String parameterValue = request.getParameter(parameterName);
		String trimmed = StringsHelper.trim(parameterValue);

		if(trimmed != null)
			if(toUppercase)
				return trimmed.toUpperCase();

		return trimmed;
	}

	static public String[] getMultipleParameter(String parameterName, HttpServletRequest request) {
		return ServletsHelper.getMultipleParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);
	}

	static public String[] getMultipleParameter(String parameterName, boolean toUppercase, HttpServletRequest request) {
		String[] parameterValues = request.getParameterValues(parameterName + "[]");

		if(parameterValues == null || parameterValues.length == 0)
			return null;

		Collection<String> values = new ListSet<String>();

		String trimmed;

		for(String value : parameterValues) {
			trimmed = StringsHelper.trim(value);

			if(trimmed != null)
				values.add(trimmed);
		}

		if(values.size() == 0)
			return null;

		return values.toArray(new String[values.size()]);
	}

	static public String getEscapedParameterAsString(String parameterName, HttpServletRequest request) {
		byte[] asByte = ServletsHelper.getEscapedParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);

		if(asByte == null)
			return null;

		return new String(asByte);
	}

	static public byte[] getEscapedParameter(String parameterName, HttpServletRequest request) {
		return ServletsHelper.getEscapedParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);
	}

	static public byte[] getEscapedParameter(String parameterName, boolean toUppercase, HttpServletRequest request) {
		String parameterValue = request.getParameter(parameterName);
		String trimmed = StringsHelper.trim(parameterValue);

		if(trimmed == null)
			return null;

		String toReturn = null;

		try {
			String value = URLHelper.unescape(trimmed);

			if(toUppercase)
				toReturn = value.toUpperCase();
			else
				toReturn = value;
		} catch (Throwable t) {
			if(toUppercase)
				toReturn = trimmed.toUpperCase();
			else
				toReturn = trimmed;
		}

		return toReturn.getBytes(Charset.forName("UTF-8"));
	}

	static public byte[][] getMultipleEscapedParameter(String parameterName, HttpServletRequest request) {
		return ServletsHelper.getMultipleEscapedParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);
	}

	static public byte[][] getMultipleEscapedParameter(String parameterName, boolean toUppercase, HttpServletRequest request) {
		String[] parameterValues = request.getParameterValues(parameterName + "[]");

		if(parameterValues == null || parameterValues.length == 0)
			return null;

		Collection<byte[]> values = new ArrayList<byte[]>();

		String trimmed;
		String unescaped = null;

		for(String value : parameterValues) {
			trimmed = StringsHelper.trim(value);

			if(trimmed != null) {
				try {
					unescaped = URLHelper.unescape(trimmed);
				} catch (Throwable t) {
					unescaped = trimmed;
				}
			} else {
				unescaped = null;
			}

			if(unescaped != null)
				values.add((toUppercase ? unescaped.toUpperCase() : unescaped).getBytes(Charset.forName("UTF-8")));
		}

		if(values.size() == 0)
			return null;

		return values.toArray(new byte[values.size()][]);
	}

	static public String[] getMultipleEscapedStringParameter(String parameterName, HttpServletRequest request) {
		return ServletsHelper.getMultipleEscapedStringParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);
	}

	static public String[] getMultipleEscapedStringParameter(String parameterName, boolean toUppercase, HttpServletRequest request) {
		String[] parameterValues = request.getParameterValues(parameterName + "[]");

		if(parameterValues == null || parameterValues.length == 0)
			return null;

		Collection<String> values = new ArrayList<String>();

		String trimmed;
		String unescaped = null;

		for(String value : parameterValues) {
			trimmed = StringsHelper.trim(value);

			if(trimmed != null) {
				try {
					unescaped = URLHelper.unescape(trimmed);
				} catch (Throwable t) {
					unescaped = trimmed;
				}
			} else {
				unescaped = null;
			}

			if(unescaped != null)
				values.add((toUppercase ? unescaped.toUpperCase() : unescaped));//.getBytes(Charset.forName("UTF-8")));
		}

		if(values.size() == 0)
			return null;

//		return values.toArray(new byte[values.size()][]);
		return values.toArray(new String[values.size()]);
	}

	static public Integer getIntParameter(String parameterName, HttpServletRequest request) {
		String parameterValue = ServletsHelper.getParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);

		if(parameterValue != null) {
			try {
				return Integer.valueOf(parameterValue);
			} catch (NumberFormatException NFe) {
				ServletsHelper.getLog().warn("Unable to parse parameter value '" + parameterValue + "' for parameter '" + parameterName + "' as an integer value");
			}
		}

		return null;
	}

	static public Long getLongParameter(String parameterName, HttpServletRequest request) {
		String parameterValue = ServletsHelper.getParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);

		if(parameterValue != null) {
			try {
				return Long.valueOf(parameterValue);
			} catch (NumberFormatException NFe) {
				ServletsHelper.getLog().warn("Unable to parse parameter value '" + parameterValue + "' for parameter '" + parameterName + "' as a long value");
			}
		}

		return null;
	}

	static public Double getDoubleParameter(String parameterName, HttpServletRequest request) {
		String parameterValue = ServletsHelper.getParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);

		if(parameterValue != null) {
			try {
				return Double.valueOf(parameterValue);
			} catch (NumberFormatException NFe) {
				ServletsHelper.getLog().warn("Unable to parse parameter value '" + parameterValue + "' for parameter '" + parameterName + "' as an integer value");
			}
		}

		return null;
	}

	static public Float getFloatParameter(String parameterName, HttpServletRequest request) {
		String parameterValue = ServletsHelper.getParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);

		if(parameterValue != null) {
			try {
				return Float.valueOf(parameterValue);
			} catch (NumberFormatException NFe) {
				ServletsHelper.getLog().warn("Unable to parse parameter value '" + parameterValue + "' for parameter '" + parameterName + "' as an integer value");
			}
		}

		return null;
	}

	static public Integer[] getMultipleIntParameter(String parameterName, HttpServletRequest request) {
		String[] parameterValues = ServletsHelper.getMultipleParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);

		Collection<Integer> values = new ArrayList<Integer>();

		if(parameterValues != null && parameterValues.length > 0) {
			for(String parameterValue : parameterValues) {
				try {
					values.add(Integer.valueOf(parameterValue));
				} catch (NumberFormatException NFe) {
					ServletsHelper.getLog().warn("Unable to parse parameter value '{}' for parameter '{}' as integer value", parameterValues, parameterName);
				}
			}
		}

		if(values.size() == 0)
			return null;

		return values.toArray(new Integer[values.size()]);
	}

	static public Boolean getBooleanParameter(String parameterName, Boolean defaultValue, HttpServletRequest request) {
		String parameterValue = ServletsHelper.getParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);

		if(parameterValue != null) {
			return Boolean.valueOf(parameterValue);
		}

		ServletsHelper.getLog().debug("Returning a '" + defaultValue + "' boolean value for unset parameter '" + parameterName + "'");

		return defaultValue;
	}

	static public Boolean[] getMultipleBooleanParameter(String parameterName, HttpServletRequest request) {
		String[] parameterValues = ServletsHelper.getMultipleParameter(parameterName, RequestProcessingConstants.DONT_CONVERT_PARAMETER_VALUE_TO_UPPERCASE, request);

		Collection<Boolean> values = new ArrayList<Boolean>();

		if(parameterValues != null && parameterValues.length > 0) {
			for(String parameterValue : parameterValues) {
				values.add(Boolean.valueOf(parameterValue));
			}
		}

		if(values.size() == 0)
			return null;

		return values.toArray(new Boolean[values.size()]);
	}

	static public boolean isParameterSet(Object[] parameterValue) {
		return parameterValue != null && parameterValue.length > 0;
	}

	static public boolean isParameterSet(Object parameterValue) {
		if(parameterValue == null)
			return false;

		if(parameterValue instanceof String)
			return StringsHelper.trim((String)parameterValue) != null;

		return true;
	}

	/**
	 * @param request
	 * @return
	 * @throws IOException
	 */
	static public boolean clientAcceptsGZIPEncoding(HttpServletRequest request) throws IOException {
		String acceptEncodingHeader = request.getHeader(RequestHeadersConstants.REQUEST_HEADER_ACCEPT_ENCODING);

        return acceptEncodingHeader != null && acceptEncodingHeader.contains("gzip");
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	static public OutputStreamWriter getOutputStreamWriter(HttpServletRequest request, HttpServletResponse response) throws IOException {
		if(ServletsHelper.clientAcceptsGZIPEncoding(request)) {
			ServletsHelper.getLog().debug("Client accepts GZIP encoding: serving compressed response...");

			//return new OutputStreamWriter(ServletsHelper.getGZippedOutputStream(response), ServletsConstants.DEFAULT_CHARSET);
			return new OutputStreamWriter(response.getOutputStream(), OutputEncodingConstants.DEFAULT_CHARSET);
		} else {
			ServletsHelper.getLog().debug("Client doesn't accept GZIP encoding: serving plain, uncompressed response...");

			return new OutputStreamWriter(response.getOutputStream(), OutputEncodingConstants.DEFAULT_CHARSET);
		}
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	static public OutputStream getOutputStream(HttpServletRequest request, HttpServletResponse response) throws IOException {
		if(ServletsHelper.clientAcceptsGZIPEncoding(request)) {
			ServletsHelper.getLog().debug("Client accepts GZIP encoding: serving compressed response...");

			return ServletsHelper.getGZippedOutputStream(response);
		} else {
			ServletsHelper.getLog().debug("Client doesn't accept GZIP encoding: serving plain, uncompressed response...");

			return response.getOutputStream();
		}
	}

	/**
	 * @param response
	 * @return
	 * @throws IOException
	 */
	static public OutputStream getGZippedOutputStream(HttpServletResponse response) throws IOException {
		response.setHeader(ResponseHeadersConstants.RESPONSE_HEADER_CONTENT_ENCODING, OutputEncodingConstants.GZIP_CONTENT_ENCODING);

		return new GZIPOutputStream(response.getOutputStream());
	}

	/**
	 * @param response
	 * @return
	 * @throws IOException
	 */
	static public OutputStreamWriter getGZippedOutputStreamWriter(HttpServletResponse response) throws IOException {
		return new OutputStreamWriter(ServletsHelper.getGZippedOutputStream(response));
	}

	/**
	 * @param writer
	 * @throws IOException
	 */
	static public void complete(Class<? extends Servlet> sourceServletClass, OutputStreamWriter writer) throws IOException {
		if(writer != null) {
			writer.flush();
			writer.close();
		} else {
			ServletsHelper.getLog().error("The output stream writer is currently NULL for servlet " + sourceServletClass );
		}
	}

	/**
	 * @param response
	 * @param seconds
	 */
	static public void setMaxAgeHeader(HttpServletResponse response, Integer seconds) {
		if(response != null && seconds != null) {
			response.setHeader(ResponseHeadersConstants.RESPONSE_HEADER_CACHE_CONTROL, "PUBLIC, max-age=" + seconds + ", must-revalidate");
			response.setHeader(ResponseHeadersConstants.RESPONSE_HEADER_EXPIRES, ServletsHelper.getHTMLExpiresDateFormat().format(new Date(System.currentTimeMillis() + seconds * 1000)));
		}
	}

	static public String getCaseInsensitiveRequestHeader(HttpServletRequest request, String header) {
		String currentHeader;

		@SuppressWarnings("unchecked")
		Enumeration<String> headerNames = request.getHeaderNames();

		while(headerNames.hasMoreElements()) {
			currentHeader = headerNames.nextElement();

			if(currentHeader.equalsIgnoreCase(header))
				return request.getHeader(currentHeader);
		}

		return null;
	}

	static public DateFormat getHTMLExpiresDateFormat() {
		DateFormat httpDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
		httpDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

		return httpDateFormat;
	}

	static public RequestInfo getRequestInfo(HttpServletRequest request) {
		HttpSession session = request.getSession(false);

		String[] contextParts = request.getContextPath().split("/");
		String[] requestURIParts = request.getRequestURI().split("/");

		String X_FORWARDED_FOR_HEADER 	   = StringsHelper.trim(ServletsHelper.getCaseInsensitiveRequestHeader(request, RequestHeadersConstants.REQUEST_HEADER_X_FORWARDED_FOR));
		String X_FORWARDED_HOST_HEADER 	   = StringsHelper.trim(ServletsHelper.getCaseInsensitiveRequestHeader(request, RequestHeadersConstants.REQUEST_HEADER_X_FORWARDED_HOST));
		String X_FORWARDED_BASE_URI_HEADER = StringsHelper.trim(ServletsHelper.getCaseInsensitiveRequestHeader(request, RequestHeadersConstants.REQUEST_HEADER_X_FORWARDED_BASE_URI));

//		Boolean isForwarded = request.getLocalAddr() == null ||
//							  X_FORWARDED_FOR_HEADER != null ||
//							  X_FORWARDED_HOST_HEADER != null ||
//							  X_FORWARDED_BASE_URI_HEADER != null;
		
		Boolean isForwarded = X_FORWARDED_FOR_HEADER != null && X_FORWARDED_HOST_HEADER != null;

		String remoteAddress = null;
		String remoteHost = null;
		Integer remotePort = null;

		String forwarderHost = null;
		Integer forwarderPort = null;

		String localAddress = null;
		String localHost = null;
		Integer localPort = null;

		if(isForwarded) {
			remoteAddress = X_FORWARDED_FOR_HEADER.split("\\,")[0];

			remoteHost = remoteAddress;

			InetAddress local = null;

			try {
				local = InetAddress.getLocalHost();
			} catch (Throwable t) {
				ServletsHelper.getLog().error("Unable to query local host address", t);
			}

			localAddress = local == null ? null : local.getHostAddress();
			localHost = local == null ? null : local.getHostName();

			if("127.0.0.1".equals(localAddress)) {
				localAddress = local.getCanonicalHostName();
			}

			forwarderHost = X_FORWARDED_HOST_HEADER.split("\\,")[0];
			remotePort = request.getRemotePort();
		} else {
			remoteAddress = request.getRemoteAddr();
			remoteHost = request.getRemoteHost();
			remotePort = request.getRemotePort();

			localAddress = request.getLocalAddr();
			localHost = request.getLocalName();
			localPort = request.getLocalPort();
		}

		String forwarderPath = X_FORWARDED_BASE_URI_HEADER == null ? null : X_FORWARDED_BASE_URI_HEADER.substring(0, X_FORWARDED_BASE_URI_HEADER.lastIndexOf("/"));

		return new RequestInfo(request.getScheme(),			//SCHEME
							   localAddress,				//LOCAL ADDRESS
							   localHost,					//LOCAL HOST
							   localPort,					//LOCAL PORT
							   isForwarded,					//IS FORWARDED
							   forwarderHost,				//FORWARDER HOST
							   forwarderPort, 				//FORWARDER PORT
							   X_FORWARDED_BASE_URI_HEADER,	//FORWARDER BASE URI (custom header)
							   forwarderPath,				//FORWARDER PATH
							   request.getContextPath(), 	//CONTEXT PATH
							   "/" + ( requestURIParts.length > contextParts.length ? requestURIParts[contextParts.length] : "" ),
							   request.getServletPath(),
							   request.getRequestURI(),
							   request.getRequestURI().replace(request.getContextPath(), "").replace(request.getServletPath(), ""),
							   remoteAddress,
							   remoteHost,
							   remotePort,
							   request.getRemoteUser(),
							   request.getHeader(RequestHeadersConstants.REQUEST_HEADER_REFERER),
							   request.getQueryString(),
							   session == null ? null : session.getId(),
							   request.getHeader(RequestHeadersConstants.REQUEST_HEADER_USER_AGENT),
							   ServletsHelper.isBot(request));
	}

	static public String addOriginalURLResponseHeader(HttpServletRequest request, HttpServletResponse response) {
		RequestInfo requestInfo = ServletsHelper.getRequestInfo(request);

		return ServletsHelper.addOriginalURLResponseHeader(requestInfo.getRequestURI() + ( StringsHelper.trim(requestInfo.getQueryString()) != null ? "?" + requestInfo.getQueryString() : "" ), response);
	}

	static public String addOriginalURLResponseHeader(String URL, HttpServletResponse response) {
		if(StringsHelper.trim(URL) != null)
			response.setHeader(ResponseHeadersConstants.RESPONSE_HEADER_REDIRECT_TO, URL);

		return URL;
	}

	static public String addOriginalRefererResponseHeader(HttpServletRequest request, HttpServletResponse response) {
		return ServletsHelper.addOriginalRefererResponseHeader(ServletsHelper.getRequestInfo(request).getReferer(), response);
	}

	static public String addOriginalRefererResponseHeader(String referer, HttpServletResponse response) {
		if(StringsHelper.trim(referer) != null)
			response.setHeader(ResponseHeadersConstants.RESPONSE_HEADER_ORIGINAL_REFERER, referer);

		return referer;
	}

	static public VRMFUser getLoggedUser(HttpSession session) {
		final Logger log = ServletsHelper.getLog();

		if(session == null || session.getAttribute("user") == null) {
			log.debug(session == null ? "Cannot get logged user: no HTTP Session" : "Cannot get logged user: HTTP Session " + session.getId() + " stores no 'user' attribute");
			return null;
		}

		try {
			VRMFUser toReturn = (VRMFUser)session.getAttribute("user");

			log.debug("Returning logged user details (" + toReturn.getId() + ") for HTTP Session " + session.getId());

			return toReturn;
		} catch (Throwable t) {
			log.error("Unable to access the 'user' session attribute. Unexpected " + t.getClass().getName() + " caught. Reason: " + t.getMessage(), t);

			return null;
		}
	}

	static public VRMFUser getLoggedUser(HttpServletRequest request) {
		return ServletsHelper.getLoggedUser(request.getSession(false));
	}

	static public void setLoggedUser(HttpSession session, VRMFUser user) {
		final Logger log = ServletsHelper.getLog();

		if(session != null) {
			if(user != null)
				log.debug("Setting user " + user.getId() + " into session " + session.getId());
			else
				log.debug("Setting NULL user " + " into session " + session.getId());

			session.setAttribute("user", user);
		} else {
			if(user != null)
				log.warn("Cannot set user " + user.getId() + " into a NULL session");
			else
				log.warn("Cannot set NULL user into a NULL session");
		}
	}

	static public void setLoggedUser(HttpServletRequest request, VRMFUser user) {
		ServletsHelper.setLoggedUser(request.getSession(false), user);
	}

	static public String[] getAvailableSources(HttpServletRequest request, boolean includeOtherSources) {
		return ServletsHelper.getAvailableSources(ServletsHelper.getLoggedUser(request), request, includeOtherSources);
	}

	static public String[] getAvailableSources(BasicUser loggedUser, HttpServletRequest request, boolean includeOtherSources) {
		Collection<String> sourceSystems = new HashSet<String>();

		sourceSystems.addAll(Arrays.asList(ServletsHelper.getAvailableDataSources(loggedUser, request)));

		if(includeOtherSources)
			sourceSystems.addAll(Arrays.asList(ServletsHelper.getAvailableOtherSources(loggedUser, request)));

		return sourceSystems.toArray(new String[sourceSystems.size()]);
	}

	static public String[] getAvailableDataSources(BasicUser loggedUser, HttpServletRequest request) {
		String[] dataSources = (String[])request.getAttribute(RequestAttributesConstants.AVAILABLE_DATA_SOURCES_REQUEST_ATTRIBUTE);

		Collection<String> sourceSystems = new HashSet<String>();

		if(dataSources != null)
			sourceSystems.addAll(Arrays.asList(dataSources));
		else if(loggedUser != null)
			sourceSystems.addAll(Arrays.asList(ServletsHelper.getAvailableDataSources(loggedUser)));

		return sourceSystems.toArray(new String[sourceSystems.size()]);
	}

	static public String[] getAvailableOtherSources(BasicUser loggedUser, HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		String[] otherSources = (String[])request.getAttribute(RequestAttributesConstants.AVAILABLE_OTHER_SOURCES_REQUEST_ATTRIBUTE);
		String contextOtherSources = session.getServletContext().getInitParameter(InitParameterConstants.OTHER_SOURCES_CONTEXT_INIT_PARAM);

		Collection<String> sourceSystems = new HashSet<String>();

		if(otherSources != null)
			sourceSystems.addAll(Arrays.asList(otherSources));

		if(contextOtherSources != null) {

			for(String source : contextOtherSources.split("\\,")) {
				source = StringsHelper.trim(source);

				if(source != null) {
					ServletsHelper._log.info("Adding '" + source + "' as a context-configured other source");
					sourceSystems.add(source);
				}
			}
		}

		sourceSystems.addAll(Arrays.asList(ServletsHelper.getAvailableOtherSources(loggedUser)));

		return sourceSystems.toArray(new String[sourceSystems.size()]);
	}

	static public String[] getAvailableDataSources(BasicUser loggedUser) {
		Collection<String> sourceSystems = new HashSet<String>();

		if(loggedUser != null) {
			if(loggedUser.can("ACCESS_*_DATA_SOURCES")) {
				String capabilityProperties = null;

				for(UserCapability capability : loggedUser.getCapabilities("ACCESS_*_DATA_SOURCES")) {
					capabilityProperties = capability.getCapabilityParameters();

					if(StringsHelper.trim(capabilityProperties) != null)
						sourceSystems.addAll(Arrays.asList(capabilityProperties.split("\\,")));
				}
			}
		}

		return sourceSystems.toArray(new String[sourceSystems.size()]);
	}

	static public String[] getAvailableOtherSources(BasicUser loggedUser) {
		Collection<String> sourceSystems = new HashSet<String>();

		if(loggedUser != null) {
			if(loggedUser.can("ACCESS_*_OTHER_SOURCES")) {
				String capabilityProperties = null;

				for(UserCapability capability : loggedUser.getCapabilities("ACCESS_*_OTHER_SOURCES")) {
					capabilityProperties = capability.getCapabilityParameters();

					if(StringsHelper.trim(capabilityProperties) != null)
						sourceSystems.addAll(Arrays.asList(capabilityProperties.split("\\,")));
				}
			}
		}

		return sourceSystems.toArray(new String[sourceSystems.size()]);
	}

	static public String localizeURI(String URI, String language) {
		if(ServletsHelper.LANGUAGE_LOCALIZER_PATTERN == null) {
			String initialPattern = "(^[^.|\\?]+)";
			String resourcePattern = "(/[a-zA-Z0-9_]+\\.[a-zA-Z0-9_]+)";
			String paramPattern = "(/?\\?.+)";
			
			String localizerPattern = initialPattern +
									 "(/?|(" + resourcePattern + "?" + paramPattern + "?))$";
			
			ServletsHelper.LANGUAGE_LOCALIZER_PATTERN = Pattern.compile(localizerPattern);
		}
		
		if(URI == null || language == null || "".equals(language))
			return URI;
		
		String localized = URI;
		
		Matcher resourceParser = ServletsHelper.LANGUAGE_LOCALIZER_PATTERN.matcher(localized);
		
		if(resourceParser.matches()) {
			boolean addPreSlash = !resourceParser.group(1).endsWith("/");
			boolean addPostSlash = localized.endsWith("/");
			
			localized = resourceParser.replaceAll("$1" + ( addPreSlash ? "/" : "" ) + language + "$4$5");
			
			if(addPostSlash)
				localized += "/";
		}
		
//		if(!URI.endsWith("/"))
//			localized += "/";
//		
//		localized += language;
//		
//		if(URI.endsWith("/"))
//			localized += "/";
		
		return localized;
	}
	
	static boolean isBot(HttpServletRequest request) {
		String userAgent = request.getHeader(RequestHeadersConstants.REQUEST_HEADER_USER_AGENT);

		boolean isBot = userAgent == null;

		if(!isBot) {
			for(String agent : NetworkConstants.CRAWLERS) {
				isBot |= userAgent.indexOf(agent) >= 0;

				if(isBot)
					break;
			}
		}

		return isBot;
	}

	static final public String getBaseURL(HttpServletRequest request, boolean includeServerAndProtocol, boolean includeContext) throws Throwable {
//		SSystems specificSource = (SSystems)request.getAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_SOURCE_REQUEST_ATTRIBUTE);
//		String sourceID = specificSource == null ? null : specificSource.getId();
		String sourceID = (String)request.getAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_SOURCE_ID_REQUEST_ATTRIBUTE);

		RequestInfo requestInfo = ServletsHelper.getRequestInfo(request);

		String forwarder = requestInfo.getForwarderHost();

		if(requestInfo.getForwarderPort() != null)
			forwarder += ":" + requestInfo.getForwarderPort();

//		String forwarderBaseURI = requestInfo.getForwarderBaseURI();
		String forwarderPath = requestInfo.getForwarderPath();

		String context = requestInfo.getContextPath();

//		String requestURI = requestInfo.getRequestURI();

//		Set<String> forwarderBaseURIParts = new ListSet<String>();
//
//		if(forwarderBaseURI != null) {
//			for(String path : forwarderBaseURI.split("\\/")) {
//				if(!path.equals("")) {
//					forwarderBaseURIParts.add(path);
//					requestURI = requestURI.replace(path + "/", "");
//				}
//			}
//		}

//		for(String forwarderBaseURIPart : forwarderBaseURIParts) {
//			context = context.replaceAll("\\/" + forwarderBaseURIPart, "");
//		}

		boolean includePort = requestInfo.getLocalPort() != null &&
							  ( ( requestInfo.getScheme() == "https" && requestInfo.getLocalPort() != 443 ) ||
							    ( requestInfo.getScheme() == "http" && requestInfo.getLocalPort() != 80 ) );

		return ( includeServerAndProtocol
				?
					requestInfo.getScheme() + "://" + ( forwarder == null ? requestInfo.getLocalHost() + ( includePort ? ":" + requestInfo.getLocalPort() : "" ) : forwarder )
				:
					"" ) +
			   ( forwarderPath == null ? "" : forwarderPath ) +
//			   ( forwarderBaseURI == null ? "" : forwarderBaseURI ) +
			   ( includeContext ? context : "/" ) +
			   ( sourceID == null ? "" : "/" + sourceID);
	}

	static public class RequestInfo implements Serializable {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -646370193487879871L;

		private String _scheme;
		private String _localAddress;
		private String _localHost;
		private Integer _localPort;
		private Boolean _isForwarded;
		private String _forwarderHost;
		private Integer _forwarderPort;
		private String _forwarderBaseURI;
		private String _forwarderPath;
		private String _remoteAddress;
		private String _remoteHost;
		private Integer _remotePort;
		private String _remoteUser;
		private String _contextPath;
		private String _subContextPath;
		private String _servletPath;
		private String _servletParentPath;
		private String _requestURI;
		private String _additionalPath;
		private String _queryString;
		private String _sessionID;
		private String _referer;
		private String _userAgent;
		private Boolean _isBot;

		/**
		 * Class constructor
		 *
		 * @param scheme
		 * @param localAddress
		 * @param localHost
		 * @param localPort
		 * @param isForwarded
		 * @param forwarderHost
		 * @param forwarderPort
		 * @param forwarderBaseURI
		 * @param forwarderPath
		 * @param contextPath
		 * @param subContextPath
		 * @param servletPath
		 * @param requestURI
		 * @param additionalPath
		 * @param remoteAddress
		 * @param remoteHost
		 * @param remotePort
		 * @param remoteUser
		 * @param referer
		 * @param queryString
		 * @param sessionID
		 * @param userAgent
		 * @param isBot
		 */
		private RequestInfo(String scheme, String localAddress, String localHost, Integer localPort, Boolean isForwarded, String forwarderHost, Integer forwarderPort, String forwarderBaseURI, String forwarderPath, String contextPath, String subContextPath, String servletPath, String requestURI, String additionalPath, String remoteAddress, String remoteHost, Integer remotePort, String remoteUser, String referer, String queryString, String sessionID, String userAgent, Boolean isBot) {
			super();
			this._scheme = StringsHelper.trim(scheme);
			this._localAddress = StringsHelper.trim(localAddress);
			this._localHost = StringsHelper.trim(localHost);
			this._localPort = localPort;
			this._isForwarded = isForwarded;
			this._forwarderHost = forwarderHost;
			this._forwarderPort = forwarderPort;
			this._forwarderBaseURI = forwarderBaseURI;
			this._forwarderPath = forwarderPath;
			this._contextPath = StringsHelper.trim(contextPath);
			this._subContextPath = StringsHelper.trim(subContextPath);
			this._servletPath = StringsHelper.trim(servletPath);
			this._requestURI = StringsHelper.trim(requestURI);
			this._additionalPath = StringsHelper.trim(additionalPath);
			this._remoteAddress = StringsHelper.trim(remoteAddress);
			this._remoteHost = StringsHelper.trim(remoteHost);
			this._remotePort = remotePort;
			this._remoteUser = StringsHelper.trim(remoteUser);
			this._referer = StringsHelper.trim(referer);
			this._queryString = StringsHelper.trim(queryString);
			this._userAgent = StringsHelper.trim(userAgent);
			this._isBot = isBot;
			if(this._servletPath != null) {
				int lastIndex = this._servletPath.lastIndexOf("/");
				this._servletParentPath = this._servletPath.substring(0, Math.min(lastIndex, this._servletPath.length()));
			}

			this.setQueryString(StringsHelper.trim(queryString));
			this._sessionID = sessionID;
		}

		/**
		 * @return the 'scheme' value
		 */
		public String getScheme() {
			return this._scheme;
		}

		/**
		 * @param scheme the 'scheme' value to set
		 */
		public void setScheme(String scheme) {
			this._scheme = scheme;
		}

		/**
		 * @return the 'host' value
		 */
		public String getLocalAddress() {
			return this._localAddress;
		}

		/**
		 * @param host the 'host' value to set
		 */
		public void setLocalAddress(String host) {
			this._localAddress = host;
		}

		/**
		 * @return the 'port' value
		 */
		public Integer getLocalPort() {
			return this._localPort;
		}

		/**
		 * @param port the 'port' value to set
		 */
		public void setLocalPort(Integer port) {
			this._localPort = port;
		}

		/**
		 * @return the 'contextPath' value
		 */
		public String getContextPath() {
			return this._contextPath;
		}

		/**
		 * @param contextPath the 'contextPath' value to set
		 */
		public void setContextPath(String contextPath) {
			this._contextPath = contextPath;
		}

		/**
		 * @return the 'subContextPath' value
		 */
		public String getSubContextPath() {
			return this._subContextPath;
		}

		/**
		 * @param subContextPath the 'subContextPath' value to set
		 */
		public void setSubContextPath(String subContextPath) {
			this._subContextPath = subContextPath;
		}

		/**
		 * @return the 'servletPath' value
		 */
		public String getServletPath() {
			return this._servletPath;
		}

		/**
		 * @param servletPath the 'servletPath' value to set
		 */
		public void setServletPath(String servletPath) {
			this._servletPath = servletPath;
		}

		/**
		 * @return the 'requestURI' value
		 */
		public String getRequestURI() {
			return this._requestURI;
		}

		/**
		 * @param requestURI the 'requestURI' value to set
		 */
		public void setRequestURI(String requestURI) {
			this._requestURI = requestURI;
		}

		/**
		 * @return the 'additionalPath' value
		 */
		public String getAdditionalPath() {
			return this._additionalPath;
		}

		/**
		 * @param additionalPath the 'additionalPath' value to set
		 */
		public void setAdditionalPath(String additionalPath) {
			this._additionalPath = additionalPath;
		}

		/**
		 * @return the 'referer' value
		 */
		public String getReferer() {
			return this._referer;
		}

		/**
		 * @param referer the 'referer' value to set
		 */
		public void setReferer(String referer) {
			this._referer = referer;
		}

		/**
		 * @return the 'servletParentPath' value
		 */
		public String getServletParentPath() {
			return this._servletParentPath;
		}

		/**
		 * @param servletParentPath the 'servletParentPath' value to set
		 */
		public void setServletParentPath(String servletParentPath) {
			this._servletParentPath = servletParentPath;
		}

		/**
		 * @return the 'queryString' value
		 */
		public String getQueryString() {
			return this._queryString;
		}

		/**
		 * @param queryString the 'queryString' value to set
		 */
		public void setQueryString(String queryString) {
			if(queryString == null)
				this._queryString = null;
			else {
				this._queryString = queryString.replaceAll("\\&?\\_\\=\\d+", "");
			}
		}

		/**
		 * @return the 'remoteAddress' value
		 */
		public String getRemoteAddress() {
			return this._remoteAddress;
		}

		/**
		 * @param remoteAddress the 'remoteAddress' value to set
		 */
		public void setRemoteAddress(String remoteAddress) {
			this._remoteAddress = remoteAddress;
		}

		/**
		 * @return the 'remotePort' value
		 */
		public Integer getRemotePort() {
			return this._remotePort;
		}

		/**
		 * @param remotePort the 'remotePort' value to set
		 */
		public void setRemotePort(Integer remotePort) {
			this._remotePort = remotePort;
		}

		/**
		 * @return the 'localHost' value
		 */
		public String getLocalHost() {
			return this._localHost;
		}

		/**
		 * @param localHost the 'localHost' value to set
		 */
		public void setLocalHost(String localHost) {
			this._localHost = localHost;
		}

		/**
		 * @return the 'remoteHost' value
		 */
		public String getRemoteHost() {
			return this._remoteHost;
		}

		/**
		 * @param remoteHost the 'remoteHost' value to set
		 */
		public void setRemoteHost(String remoteHost) {
			this._remoteHost = remoteHost;
		}

		/**
		 * @return the 'remoteUser' value
		 */
		public String getRemoteUser() {
			return this._remoteUser;
		}

		/**
		 * @param remoteUser the 'remoteUser' value to set
		 */
		public void setRemoteUser(String remoteUser) {
			this._remoteUser = remoteUser;
		}

		/**
		 * @return the 'sessionID' value
		 */
		public String getSessionID() {
			return this._sessionID;
		}

		/**
		 * @param sessionID the 'sessionID' value to set
		 */
		public void setSessionID(String sessionID) {
			this._sessionID = sessionID;
		}

		public String getContextURL() {
			return this._scheme + "://" + this._localAddress + ":" + this._localPort + this._contextPath;
		}

		public String getSubContextURL() {
			return this.getContextURL() + this._subContextPath;
		}

		/**
		 * @return the 'isForwarded' value
		 */
		public Boolean getIsForwarded() {
			return this._isForwarded;
		}

		/**
		 * @param isForwarded the 'isForwarded' value to set
		 */
		public void setIsForwarded(Boolean isForwarded) {
			this._isForwarded = isForwarded;
		}

		/**
		 * @return the 'forwarderHost' value
		 */
		public String getForwarderHost() {
			return this._forwarderHost;
		}

		/**
		 * @param forwarderHost the 'forwarderHost' value to set
		 */
		public void setForwarderHost(String forwarderHost) {
			this._forwarderHost = forwarderHost;
		}

		/**
		 * @return the 'forwarderPort' value
		 */
		public Integer getForwarderPort() {
			return this._forwarderPort;
		}

		/**
		 * @param forwarderPort the 'forwarderPort' value to set
		 */
		public void setForwarderPort(Integer forwarderPort) {
			this._forwarderPort = forwarderPort;
		}

		/**
		 * @return the 'forwarderBaseURI' value
		 */
		public String getForwarderBaseURI() {
			return this._forwarderBaseURI;
		}

		/**
		 * @param forwarderBaseURI the 'forwarderBaseURI' value to set
		 */
		public void setForwarderBaseURI(String forwarderBaseURI) {
			this._forwarderBaseURI = forwarderBaseURI;
		}

		/**
		 * @return the 'forwarderPath' value
		 */
		public String getForwarderPath() {
			return this._forwarderPath;
		}

		/**
		 * @param forwarderPath the 'forwarderPath' value to set
		 */
		public void setForwarderPath(String forwarderPath) {
			this._forwarderPath = forwarderPath;
		}

		/**
		 * @return the 'userAgent' value
		 */
		public String getUserAgent() {
			return this._userAgent;
		}

		/**
		 * @param userAgent the 'userAgent' value to set
		 */
		public void setUserAgent(String userAgent) {
			this._userAgent = userAgent;
		}

		/**
		 * @return the 'isCrawler' value
		 */
		public Boolean getIsBot() {
			return this._isBot;
		}

		/**
		 * @param isCrawler the 'isCrawler' value to set
		 */
		public void setIsBot(Boolean isCrawler) {
			this._isBot = isCrawler;
		}

		public boolean isInternalRequest() {
			return StringsHelper.trim(this._remoteAddress) != null &&
				   FAO_LAN_INTERNAL_IPS_PATTERN.matcher(StringsHelper.trim(this._remoteAddress)).matches();
		}

		@Override
		public String toString() {
			return ( this._remoteAddress + ( this._remotePort == null ? "" : ":" + this._remotePort ) + " => " +
				     this._requestURI + " @ " + this._localAddress + " [ " +
				   ( this._referer == null ? "DIRECT" : this._referer ) + " ] " +
				     "UA: " + this._userAgent );
		}
	}

	static final public void setStatusCodeOnly(int statusCode, HttpServletResponse response) throws IOException {
		response.setStatus(statusCode);
		response.setContentLength(0);

		Writer writer = response.getWriter();

		writer.write("");

		writer.flush();
	}

	static final public String sanitizeHTML(String toSanitize) {
		return ServletsHelper.sanitizeHTML(toSanitize, false);
	}

	static final public String sanitizeHTML(String toSanitize, boolean spacesAsNBSP) {
		if(toSanitize == null)
			return "";

		return toSanitize.replaceAll("\\&", "&amp;").
						  replaceAll("\\<", "&lt;").
						  replaceAll("\\>", "&gt;").
						  replaceAll("\n", "<br/>").replaceAll("\\s", spacesAsNBSP ? "&nbsp;" : " ");
	}
}