/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.exceptions.authentication;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Mar 2011
 */
public class NoLoggedUserException extends LoggedUserFilterException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3200295571357187043L;

	/**
	 * Class constructor
	 */
	public NoLoggedUserException() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public NoLoggedUserException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public NoLoggedUserException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public NoLoggedUserException(Throwable cause) {
		super(cause);
	}	
}