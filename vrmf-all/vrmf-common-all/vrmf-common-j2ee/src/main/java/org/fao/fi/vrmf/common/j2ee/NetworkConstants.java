/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Oct 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Oct 2012
 */
public interface NetworkConstants {
	String INTERNAL_IPS_REGEXP = "168\\.202\\.\\d{1,3}\\.\\d{1,3}|127\\.0\\.0\\.1";
	
	String[] CRAWLERS = {
		"Google",
		"msnbot",
		"Rambler",
		"Yahoo",
		"AbachoBOT",
		"accoona",
		"AcoiRobot",
		"ASPSeek",
		"CrocCrawler",
		"Dumbot",
		"FAST-WebCrawler",
		"GeonaBot",
		"Gigabot",
		"Lycos",
		"MSRBOT",
		"Scooter",
		"AltaVista",
		"IDBot",
		"eStyle",
		"Scrubby" 
	};
}
