/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.utilities.process.trackers;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.fao.fi.sh.model.core.spi.Pair;
import org.fao.fi.sh.utility.model.BasicPair;
import org.fao.fi.sh.utility.services.tracking.AbstractProcessTracker;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jan 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jan 2012
 */
public class HttpSessionTracker extends AbstractProcessTracker {
	static final public String CURRENT_ACTION_MAP_SESSION_ATTRIBUTE = "actionMap";
	
	private HttpSession _session;
	private String _serviceIdentifier;
	
	/**
	 * Class constructor
	 *
	 * @param session
	 * @param serviceIdentifier
	 */
	public HttpSessionTracker(HttpSession session, String serviceIdentifier) {
		super();
		
		this._session = $nN(session, "The session cannot be null");
		this._serviceIdentifier = $nN(serviceIdentifier, "The service identifier cannot be null");
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.process.ProcessTracker#setStatus(java.lang.String)
	 */
	@Override
	public void setStatus(String status) {
		this.setProcessStatus(this._session, this._serviceIdentifier, status, null);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.process.ProcessTracker#setCompletionStatus(java.lang.Double)
	 */
	@Override
	public void setCompletionStatus(Double percentage) {
		this.setProcessStatus(this._session, this._serviceIdentifier, null, percentage);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.process.trackers.AbstractProcessTracker#doStart()
	 */
	@Override
	protected void doStart() {
		this.setProcessStatus(this._session, this._serviceIdentifier, "Started", null);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.process.trackers.AbstractProcessTracker#doStart(java.lang.String)
	 */
	@Override
	protected void doStart(String status) {
		this.setProcessStatus(this._session, this._serviceIdentifier, status, null);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.process.trackers.AbstractProcessTracker#doFinish()
	 */
	@Override
	protected void doFinish() {
		this.setProcessStatus(this._session, this._serviceIdentifier, "Completed", null);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.process.trackers.AbstractProcessTracker#doFinish(java.lang.String)
	 */
	@Override
	protected void doFinish(String status) {
		this.setProcessStatus(this._session, this._serviceIdentifier, status, null);
	}
	
	final protected void setProcessStatus(HttpSession session, String serviceIdentifier, String status, Double actionProgress) {
		if(session == null) {
			this._log.warn("Cannot set process status on an empty session");
		} else {
			this._log.debug(serviceIdentifier + ": " + (status != null ? "Status: " + status : "" ) + (actionProgress != null ? (" Progress: " + actionProgress + "%") : ""));
		
			@SuppressWarnings("unchecked")
			Map<String, Pair<String, Double>> serviceStatusMap = (Map<String, Pair<String, Double>>)session.getAttribute(CURRENT_ACTION_MAP_SESSION_ATTRIBUTE);
			
			if(serviceStatusMap == null) {
				this._log.debug("No service status map currently available for session");
			} else {		
				Pair<String, Double> currentStatus = serviceStatusMap.get(serviceIdentifier);
				
				if(currentStatus == null) {
					currentStatus = new BasicPair<String, Double>("Processing...", 0D);
					
					serviceStatusMap.put(serviceIdentifier, currentStatus);
				}
				
				if(status != null) {
					currentStatus.setLeftPart(status);
				} 
			
				if(actionProgress != null) {
					currentStatus.setRightPart(actionProgress);
				}				 
				
				serviceStatusMap.put(serviceIdentifier, currentStatus);
			}
		}
	}
}