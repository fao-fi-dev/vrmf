/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.filters.security.requests;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Mar 2011
 */
public interface LoggedUserFilterConstants {
	String ALL_ROLES_CONFIGURATION_PARAMETER	= "allRoles";
	String ANY_ROLE_CONFIGURATION_PARAMETER		= "anyRole";
	String CAPABILITIES_CONFIGURATION_PARAMETER	= "capabilities";
	
	String REDIRECT_ON_MISSING_USER_CONFIGURATION_PARAMETER		  = "redirectOnMissingUser";
	String REDIRECT_ON_MISSING_ROLE_CONFIGURATION_PARAMETER		  = "redirectOnMissingRole";
	String REDIRECT_ON_MISSING_CAPABILITY_CONFIGURATION_PARAMETER = "redirectOnMissingCapability";
}
