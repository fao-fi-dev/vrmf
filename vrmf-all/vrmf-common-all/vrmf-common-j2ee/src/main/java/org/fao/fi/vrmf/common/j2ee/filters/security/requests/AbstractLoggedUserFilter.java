/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.filters.security.requests;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.common.j2ee.exceptions.authentication.LoggedUserFilterException;
import org.fao.fi.vrmf.common.j2ee.exceptions.authentication.MissingUserCapabilityException;
import org.fao.fi.vrmf.common.j2ee.exceptions.authentication.MissingUserRoleException;
import org.fao.fi.vrmf.common.j2ee.exceptions.authentication.NoLoggedUserException;
import org.fao.fi.vrmf.common.j2ee.filters.AbstractFilter;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.ResponseHeadersConstants;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.security.BasicUser;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Mar 2011
 */
abstract public class AbstractLoggedUserFilter extends AbstractFilter implements LoggedUserFilterConstants {
	private FilterConfig _filterConfig = null;
	protected Map<String, String> _initParameters = new HashMap<String, String>();
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		super.init(filterConfig);
		
		this._filterConfig = filterConfig;
		
		String name;
		String value;
		
		Enumeration<?> parameterNames = this._filterConfig.getInitParameterNames();
		
		while(parameterNames.hasMoreElements()) {
			name = (String)parameterNames.nextElement();
			value = this._filterConfig.getInitParameter(name);
			
			this._initParameters.put(name, value);
			
			this._log.info("Filter has been initialized with parameter '" + name + "' set to '" + value + "'");
		}
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	final public void destroy() {
		this._log.debug("Destroying filter " + this.getFilterName());
	}
	
	final protected ServletContext getServletContext() {
		if(this._filterConfig == null) {
			return null;
		}
		
		return this._filterConfig.getServletContext();
	}
		
	/**
	 * @param request
	 * @param response
	 * @param filterChain
	 * @throws IOException
	 * @throws ServletException
	 */
	final public void internalDoFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		this._log.info("Executing " + this.getFilterName() + " internalDoFilter...");
				
		HttpServletRequest hRequest = (HttpServletRequest)request;
		HttpServletResponse hResponse = (HttpServletResponse)response;
		
		long end, start = System.currentTimeMillis();

		BasicUser loggedUser = ServletsHelper.getLoggedUser(hRequest);
		
		try {
			if(loggedUser == null) {
				this.manageMissingLoggedUser(hRequest, hResponse, filterChain);
				
				throw new NoLoggedUserException();
			} else if(!this.checkUserRoles(loggedUser)) {
				this.manageMissingRoles(hRequest, hResponse, filterChain);
				
				throw new MissingUserRoleException();
			} else if(!this.checkUserCapabilities(loggedUser)) {
				this.manageMissingCapabilities(hRequest, hResponse, filterChain);
				
				throw new MissingUserCapabilityException();
			} else {
				this.currentDoFilter(hRequest, hResponse, filterChain);
			}
		} catch(LoggedUserFilterException LUFe) {
			this._log.error("No User (or User missing roles / capabilities) found: halting the filter propagation...");
		} finally {
			end = System.currentTimeMillis();
			this._log.info(this.getFilterName() + " took " + ( end - start ) + " mSec. to execute");
		}
	}

	protected void manageMissingLoggedUser(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		String redirectTo = ServletsHelper.addOriginalURLResponseHeader(request, response);				
		String referer = ServletsHelper.addOriginalURLResponseHeader(request, response);
		
		request.setAttribute(ResponseHeadersConstants.RESPONSE_HEADER_ORIGINAL_REFERER, referer);
		request.setAttribute(ResponseHeadersConstants.RESPONSE_HEADER_REDIRECT_TO, redirectTo);
		
		this.manageTarget(request, response, REDIRECT_ON_MISSING_USER_CONFIGURATION_PARAMETER);
	}
	
	protected void manageMissingRoles(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		this.manageTarget(request, response, REDIRECT_ON_MISSING_ROLE_CONFIGURATION_PARAMETER);		
	}
	
	protected void manageMissingCapabilities(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		this.manageTarget(request, response, REDIRECT_ON_MISSING_CAPABILITY_CONFIGURATION_PARAMETER);
	}
	
	protected void manageTarget(HttpServletRequest request, HttpServletResponse response, String redirectionInitParameterName) throws IOException, ServletException {
		String target = StringsHelper.trim(this._initParameters.get(redirectionInitParameterName));
		
		String allRoles = StringsHelper.trim(this._initParameters.get(ALL_ROLES_CONFIGURATION_PARAMETER));
		String anyRole = StringsHelper.trim(this._initParameters.get(ANY_ROLE_CONFIGURATION_PARAMETER));
		String capabilities = StringsHelper.trim(this._initParameters.get(CAPABILITIES_CONFIGURATION_PARAMETER));
		
		String[] allRequestedRoles = allRoles == null ? null : allRoles.split("\\+");
		String[] anyRequestedRole = anyRole == null ? null : anyRole.split("\\|");
		String[] requestedCapabilities = capabilities == null ? null : capabilities.split("\\|");

		//Stores the 'all roles', 'any role' and 'requested capabilities' in the request object
		request.setAttribute(ALL_ROLES_CONFIGURATION_PARAMETER, allRequestedRoles);
		request.setAttribute(ANY_ROLE_CONFIGURATION_PARAMETER, anyRequestedRole);
		request.setAttribute(CAPABILITIES_CONFIGURATION_PARAMETER, requestedCapabilities);
		
		if(target != null)
			this.getServletContext().getRequestDispatcher(target).forward(request, response);
		else {
			this._log.error("No redirection parameter set for current filter on missing role / capability / logged User. Returning custom HTML content");
			
			response.setContentType("text/html;charset=UTF-8");
			ServletOutputStream os = response.getOutputStream();
					
			String roleSet = null;
			
			if(allRequestedRoles != null && allRequestedRoles.length > 0) {
				roleSet = "<ul>";
				
				for(String role : allRequestedRoles)
					roleSet += "<li><b>Role:</b>&nbsp" + role;
				
				roleSet += "</ul>";
			}
			
			String capabilitySet = null;
			
			if(requestedCapabilities != null && requestedCapabilities.length > 0) {
				capabilitySet = "<ul>";
				
				for(String capability : requestedCapabilities)
					capabilitySet += "<li><b>Capability:</b>&nbsp" + capability;
				
				capabilitySet += "</ul>";
			}
			
			os.println(
					"<html>" +
						"<head>" +
							"<title>Unauthorized</title>" +
						"</head>" +
						"<body>" +
						"<h1>The content you are trying to access is restricted to logged users with the proper roles / capabilities only!" +
						(roleSet == null ? "" : roleSet) +
						(capabilitySet == null ? "" : capabilitySet) + 
						"<hr/>" + 
						"<h3>" + this.getFilterName() + " @ " + new Date() + " on " + InetAddress.getLocalHost().getHostAddress() + "</h3>" + 
						"</body>" +
					"</html>"
			);
			
			os.flush();
			os.close();
		}
	}

	final protected boolean checkUserRoles(BasicUser loggedUser) {
		String allRoles = this._initParameters.get(ALL_ROLES_CONFIGURATION_PARAMETER);
		String anyRole = this._initParameters.get(ANY_ROLE_CONFIGURATION_PARAMETER);
		
		boolean fulfilled = true;
		
		if(StringsHelper.trim(allRoles) != null)
			fulfilled &= this.checkAllRoles(loggedUser, allRoles.split("\\+"));
		
		if(fulfilled && StringsHelper.trim(anyRole) != null) {
			fulfilled = this.checkAnyRole(loggedUser, anyRole.split("\\|"));
		}
		
		return fulfilled;
	}
	
	final private boolean checkAllRoles(BasicUser loggedUser, String... allRoles) {
		return loggedUser.is(allRoles);
	}
	
	final private boolean checkAnyRole(BasicUser loggedUser, String... anyRole) {
		boolean fulfilled = false;
		
		for(String role : anyRole)
			fulfilled |= loggedUser.is(role);
		
		return fulfilled;
	}
	
	final protected boolean checkUserCapabilities(BasicUser loggedUser) {
		String capabilities = this._initParameters.get(CAPABILITIES_CONFIGURATION_PARAMETER);
		
		if(StringsHelper.trim(capabilities) == null)
			return true;
		
		return loggedUser.can(capabilities.split("\\|"));
	}
	
	abstract protected void currentDoFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException;
}