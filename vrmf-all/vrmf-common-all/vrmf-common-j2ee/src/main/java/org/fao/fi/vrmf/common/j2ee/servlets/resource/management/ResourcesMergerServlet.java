/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.servlets.resource.management;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URLDecoder;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.sh.utility.core.helpers.singletons.io.MD5Helper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.OutputEncodingConstants;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.RequestHeadersConstants;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.ResponseHeadersConstants;
import org.fao.fi.vrmf.common.j2ee.servlets.constants.ServletsConstants;
import org.fao.fi.vrmf.common.j2ee.servlets.resource.management.constants.ResourcesManagerServletConstants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Mar 2011
 */
public class ResourcesMergerServlet extends AbstractResourcesManagerServlet {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 217131249849526071L;

	/**
	 * Class constructor
	 */
	public ResourcesMergerServlet() throws Throwable {
		super();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String contextPath = request.getContextPath();
		String requestURI = request.getRequestURI();
		
		String requestBase = requestURI.substring(0, requestURI.lastIndexOf("/") + 1);
		
		String[] resources = URLDecoder.decode(requestURI, OutputEncodingConstants.DEFAULT_CHARSET).split(ResourcesManagerServletConstants.RESOURCES_SPLITTER_REGEXP);
		
		String realPath = null;
				
		OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream(), OutputEncodingConstants.DEFAULT_CHARSET);
		
		try {			
			File resource = null;
			String MIMEType = null;
			boolean isText = false;
						
			String requestedResource = null;
			
			Set<String> mergedResources = new ListSet<String>();
			Set<String> missingResources = new ListSet<String>();
			Set<String> nonTextualResources = new ListSet<String>();
			
			Set<String> mimeTypes = new ListSet<String>();
			
			String ifNoneMatchHeader = request.getHeader(RequestHeadersConstants.REQUEST_HEADER_IF_NONE_MATCH);
			
			StringBuilder eTagContent = new StringBuilder();
			
			//CHECK REQUESTED RESOURCES EXISTENCE, TYPE AND ETAG...
			for(String currentResource : resources) {
				currentResource = StringsHelper.trim(currentResource);
				
				if(currentResource == null)
					continue;
				
				requestedResource = ( currentResource.startsWith(requestBase) ) ? currentResource : requestBase + currentResource;
	
				realPath = this.getServletContext().getRealPath(requestedResource.substring(contextPath.length())); 
							
				resource = new File(realPath);
				
				if(!resource.exists() || !resource.isFile()) {
					missingResources.add(currentResource);
					
					this._log.warn("A request to merge a non existing resource was caught for '" + resource.getPath() + "'");
				} else {
					MIMEType = this.MIME_TYPES.getContentType(resource);
					
					isText = MIMEType != null && MIMEType.startsWith("text/");
					
					if(MIMEType != null)
						mimeTypes.add(MIMEType);
		
					if(!isText) {
						nonTextualResources.add(currentResource);
						
						this._log.warn("A request to merge a non-textual resource was caught for '" + resource.getPath() + "'");
					} else {
						mergedResources.add(currentResource);
						
						eTagContent.append(realPath).append(resource.lastModified());
					}
				} 
			}	
			
			String eTag = "\"" + MD5Helper.digest(eTagContent.toString()) + "\"";
			
			int resourcesToMerge = mergedResources.size();
			int resourcesMissing = missingResources.size();
			int binaryResources  = nonTextualResources.size();
						
			if(eTag.equals(ifNoneMatchHeader)) {
				this._log.debug("Resource " + requestURI + " was not modified. Sending a 304...");
				response.setStatus(HttpServletResponse.SC_NOT_MODIFIED); /* 304 - Not Modified */
			} else if(resourcesToMerge == 0) {
				boolean notFound = resourcesMissing > 0;
				boolean error    = !notFound && binaryResources > 1;
				
				if(!error)
					response.setStatus(notFound ? HttpServletResponse.SC_NOT_FOUND /* 404 - Not Found */ : HttpServletResponse.SC_NO_CONTENT /* 204 - No Content */);
				else 
					response.sendError(HttpServletResponse.SC_BAD_REQUEST /* 400 - Bad Request */, "Only textual resources can be merged together");
			} else {
				//DELIVER CONTENT...
				String effectiveMimeType = mimeTypes.isEmpty() || mimeTypes.size() > 1 ? "text/plain" : mimeTypes.toArray(new String[0])[0];
				
				if(effectiveMimeType != null)
					response.setContentType(effectiveMimeType);
				
				response.addHeader(ResponseHeadersConstants.RESPONSE_HEADER_ETAG, eTag);
				
				FileInputStream fis = null;
				byte[] buffer = null;
				int len = -1;

				for(String currentResource : mergedResources) {
					requestedResource = ( currentResource.startsWith(requestBase) ) ? currentResource : requestBase + currentResource;
	
					realPath = this.getServletContext().getRealPath(requestedResource.substring(contextPath.length())); 
								
					resource = new File(realPath);
										
					if(resource.exists() && resource.isFile()) {								
						fis = new FileInputStream(realPath);
							
						buffer = new byte[32768];
						len = -1;

						osw.write("/** Merged content for '" + currentResource + "' */\n");
						
						while((len = fis.read(buffer)) != -1) {
							osw.write(new String(buffer, 0, len, ServletsConstants.DEFAULT_CHARSET));
						}		
							
						osw.write("\n\n");
						
						if(fis != null) {
							try {
								fis.close();
							} catch (Throwable t) {
								this._log.error("Unable to close file input stream", t);
							}
						}
					} 
				}	
				
				for(String missingResource : missingResources) {
					osw.write("/** Cannot merge content for '" + missingResource + "': resource not found */\n");
				}
				
				for(String nonTextualResource : nonTextualResources) {
					osw.write("/** Cannot merge content for '" + nonTextualResource + "': resource is non-textual */\n");
				}
			}
			
			osw.flush();
			osw.close();
		} catch (Throwable t) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR /* 500 - Internal Server Error */, t.getMessage());
		}
	}
}