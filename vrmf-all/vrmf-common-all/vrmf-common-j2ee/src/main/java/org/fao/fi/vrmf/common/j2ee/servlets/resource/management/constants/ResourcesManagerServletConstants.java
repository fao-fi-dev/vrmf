/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.servlets.resource.management.constants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Mar 2011
 */
public interface ResourcesManagerServletConstants {
	String BASE_URI_PARAMETER 		 = "baseURI";
	String RESOURCES_PARAMETER 		 = "resources";
	String RESOURCES_SPLITTER_REGEXP = "\\|";
}
