/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.servlets.resource.management;

import java.io.IOException;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.vrmf.common.j2ee.servlets.CommonServlet;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 16 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 16 Mar 2011
 */
abstract public class AbstractResourcesManagerServlet extends CommonServlet {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4372921907994128218L;
	
	protected MimetypesFileTypeMap MIME_TYPES = new MimetypesFileTypeMap();

	/**
	 * Class constructor
	 */
	public AbstractResourcesManagerServlet() {
		super();
		
		String CUSTOM_MIME_TYPES = "" +
			"text/plain txt text TXT\n" +
			"text/css css CSS\n" +
			"text/javascript js JS\n" +
			"text/html html htm HTML HTM\n" + 
			"image/gif gif GIF\n" + 
			"image/png png PNG\n" + 
			"text/xml xml XML\n" +			
		"";
	
		this.MIME_TYPES.addMimeTypes(CUSTOM_MIME_TYPES);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fvr.web.servlets.BasicServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	final protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		throw new ServletException("POST method not allowed");
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doDelete(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	final protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		throw new ServletException("DELETE method not allowed");
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doOptions(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	final protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		throw new ServletException("OPTIONS method not allowed");
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPut(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	final protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		throw new ServletException("PUT method not allowed");
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doTrace(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	final protected void doTrace(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		throw new ServletException("TRACE method not allowed");
	}
}
