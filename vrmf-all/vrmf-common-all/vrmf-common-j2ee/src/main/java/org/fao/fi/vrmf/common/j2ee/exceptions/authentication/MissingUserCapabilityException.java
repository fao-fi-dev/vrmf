/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.exceptions.authentication;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Mar 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Mar 2011
 */
public class MissingUserCapabilityException extends LoggedUserFilterException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1730242057665347662L;

	/**
	 * Class constructor
	 */
	public MissingUserCapabilityException() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public MissingUserCapabilityException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public MissingUserCapabilityException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public MissingUserCapabilityException(Throwable cause) {
		super(cause);
	}	
}