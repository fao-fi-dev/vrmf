/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.servlets.constants;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Feb 2013
 */
public interface MIMETypeConstants {
	String XML_MIME_TYPE  = "text/xml";
	String HTML_MIME_TYPE = "text/html";
	String JSON_MIME_TYPE = "application/json";
	String CSV_MIME_TYPE  = "text/csv"; 
	String XLS_MIME_TYPE  = "application/vnd.ms-excel";
	String XLSX_MIME_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	String TXT_MIME_TYPE  = "text/plain";
	String BYTES_MIME_TYPE= "application/octet-stream";
	String JS_MIME_TYPE   = "text/javascript";
	String PDF_MIME_TYPE  = "application/pdf";
	String MDB_MIME_TYPE  = "application/vnd.ms-access";
	
	String EFFECTIVE_XML_MIME_TYPE  = XML_MIME_TYPE + "; " + OutputEncodingConstants.DEFAULT_CHARSET_SPECIFICATION;
	String EFFECTIVE_HTML_MIME_TYPE = HTML_MIME_TYPE + "; " + OutputEncodingConstants.DEFAULT_CHARSET_SPECIFICATION;
	String EFFECTIVE_JSON_MIME_TYPE = JSON_MIME_TYPE + "; " + OutputEncodingConstants.DEFAULT_CHARSET_SPECIFICATION;
	String EFFECTIVE_CSV_MIME_TYPE  = CSV_MIME_TYPE + "; " + OutputEncodingConstants.DEFAULT_CHARSET_SPECIFICATION;
	String EFFECTIVE_TXT_MIME_TYPE  = TXT_MIME_TYPE + ";" + OutputEncodingConstants.DEFAULT_CHARSET_SPECIFICATION;
	String EFFECTIVE_JS_MIME_TYPE   = JS_MIME_TYPE + ";" + OutputEncodingConstants.DEFAULT_CHARSET_SPECIFICATION;
}