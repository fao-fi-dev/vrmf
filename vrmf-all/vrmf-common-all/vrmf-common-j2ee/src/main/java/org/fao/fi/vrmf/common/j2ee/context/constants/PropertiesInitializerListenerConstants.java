/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.context.constants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Mar 2013
 */
public interface PropertiesInitializerListenerConstants {
	String PROPERTIES_RESOURCES_INIT_PARAMETER = "vrmf.context.property.resources";
}
