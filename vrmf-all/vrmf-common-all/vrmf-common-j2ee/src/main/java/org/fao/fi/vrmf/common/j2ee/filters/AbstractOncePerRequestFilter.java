/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-j2ee)
 */
package org.fao.fi.vrmf.common.j2ee.filters;

/**
 * Base class for custom once-per-request by default J2EE filters.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Nov 2012
 */
abstract public class AbstractOncePerRequestFilter extends AbstractFilter {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.j2ee.filters.AbstractFilter#doOncePerRequest()
	 */
	@Override
	protected boolean doOncePerRequest() {
		return FiltersConstants.VRMF_DO_FILTER_ONCE_PER_REQUEST;
	}
}
