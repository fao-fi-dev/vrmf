/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-lexical)
 */
package org.fao.fi.vrmf.common.tools.lexical.processors.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.lexical.processors.base.AbstractLexicalProcessor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2013
 */
public class VesselNameNumberPrefixRemoverProcessor extends AbstractLexicalProcessor {
	static final private Pattern NO_PATTERN = Pattern.compile("(.*)(^NO\\s?|" +
																   	"^N0\\s?|" +
																   	"\\sNO\\s?|" +
																   	"\\sN0\\s?|" +
																   	"\\sN°\\s?|" +
																   	"\\sNº\\s?|" + 
																   	"^N([O|0|°|º])?\\s?|" +
																   	"\\sN\\s?)" +
																   "(\\d+)(.*)");
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.tools.lexical.processors.AbstractLexicalProcessor#doProcess(java.lang.String)
	 */
	@Override
	protected String doProcess(String toProcess) {
		Matcher m = NO_PATTERN.matcher(toProcess);
		
		if(m.matches()) {
			toProcess = ( m.group(1) == null ? "" : m.group(1) ) + " " + m.group(4) + " " + ( m.group(5) == null ? "" : m.group(5) );
			
			toProcess = StringsHelper.removeMultipleSpaces(toProcess);
		}
		
		return StringsHelper.trim(toProcess);	
	}
}
