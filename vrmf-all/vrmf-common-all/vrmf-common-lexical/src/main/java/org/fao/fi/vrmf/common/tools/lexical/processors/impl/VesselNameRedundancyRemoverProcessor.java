/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-lexical)
 */
package org.fao.fi.vrmf.common.tools.lexical.processors.impl;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.lexical.processors.base.AbstractLexicalProcessor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2013
 */
public class VesselNameRedundancyRemoverProcessor extends AbstractLexicalProcessor {
	static final private String[] WORDS_TO_REMOVE_PATTERNS = {
		"^KM ",
		"^\\s?\"?F\\/V|^\\s?\"?F\\\\V", //Stands for 'Fishing Vessel', usually
		"^\\s?\"?F\\/B|^\\s?\"?F\\\\B", //Stands for 'Fishing Boat', usually
		"^\\s?\"?L\\/B|^\\s?\"?L\\\\B", //Stands for 'Leisure Boat', usually
		"DESCONHECIDO",
		"NOT NAMED",		
		"NO NAME GIVEN",
		"NO NAME REPORTED",
		"UNKNOWN NAME",
		"NAME UNKNOWN",
		"NO NAME",
		"UNKNOWN",		
		"FRITIDSFARTØJ|FRITIDSFARTOJ|FRITIDSFARTØY|FRITIDSFARTOY|FRITIDSFARTØ|FRITIDSFARTO|FRITIDSFAR" //Stands for 'Recreational Vessels' in Norwegian
	};
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.tools.lexical.processors.AbstractLexicalProcessor#doProcess(java.lang.String)
	 */
	@Override
	protected String doProcess(String toProcess) {
		for(String pattern : VesselNameRedundancyRemoverProcessor.WORDS_TO_REMOVE_PATTERNS) {
			toProcess = toProcess.replaceAll(pattern, StringsHelper.EMPTY_REPLACER);
		}
		
		return StringsHelper.trim(toProcess);
	}
}