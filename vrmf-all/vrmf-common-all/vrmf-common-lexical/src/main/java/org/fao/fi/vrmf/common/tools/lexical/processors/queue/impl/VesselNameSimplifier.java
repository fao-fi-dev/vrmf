/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-lexical)
 */
package org.fao.fi.vrmf.common.tools.lexical.processors.queue.impl;

import java.util.Arrays;
import java.util.Collection;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.lexical.processors.LexicalProcessor;
import org.fao.fi.sh.utility.lexical.processors.base.AbstractMultipartLexicalProcessorsQueue;
import org.fao.fi.sh.utility.lexical.processors.impl.DiacriticsProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.MultipleSpacesRemoverProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.NumberShifterProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.RomanNumbersProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.SymbolsRemoverProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.UppercaseProcessor;
import org.fao.fi.sh.utility.lexical.processors.queue.impl.LanguageTransliteratorProcessor;
import org.fao.fi.vrmf.common.tools.lexical.processors.impl.VesselNameNumberPrefixRemoverProcessor;
import org.fao.fi.vrmf.common.tools.lexical.processors.impl.VesselNameRedundancyRemoverProcessor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Feb 2013
 */
@Named @Singleton
public class VesselNameSimplifier extends AbstractMultipartLexicalProcessorsQueue {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.tools.lexical.processors.AbstractLexicalProcessorsQueue#doGetActualProcessors()
	 */
	@Override
	final protected Collection<LexicalProcessor> doGetActualProcessors() {
		return Arrays.asList(new LexicalProcessor[] {
			new UppercaseProcessor(),
			new VesselNameRedundancyRemoverProcessor(),
			new LanguageTransliteratorProcessor()
		});
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.tools.lexical.processors.AbstractMultipartLexicalProcessorsQueue#doGetActualPartProcessors()
	 */
	@Override
	final protected Collection<LexicalProcessor> doGetActualPartProcessors() {
		return Arrays.asList(new LexicalProcessor[] {
			new UppercaseProcessor(),
			new SymbolsRemoverProcessor(),
			new MultipleSpacesRemoverProcessor(),
			new LanguageTransliteratorProcessor(),
			new DiacriticsProcessor(),
			new VesselNameNumberPrefixRemoverProcessor(),
			new VesselNameRedundancyRemoverProcessor(),
			new RomanNumbersProcessor(),
			new NumberShifterProcessor()
		});
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.tools.lexical.processors.AbstractMultipartLexicalProcessorsQueue#doGetActualPostProcessors()
	 */
	@Override
	final protected Collection<LexicalProcessor> doGetActualPostProcessors() {
		return Arrays.asList(new LexicalProcessor[] {
			new MultipleSpacesRemoverProcessor()
		});
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.tools.lexical.processors.MultipartLexicalProcessorQueue#extract(java.lang.String)
	 */
	@Override
	final public String[] extract(String toProcess) {
		return toProcess == null ? null : toProcess.split("\\/");
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.tools.lexical.processors.MultipartLexicalProcessorQueue#join(java.lang.String[])
	 */
	@Override
	final public String join(String[] parts) {
		return parts == null || parts.length == 0 ? null : CollectionsHelper.join(parts, " ");
	}
}
