/**
 * (c) 2010-2013 FIPS / FAO of the UN (project: vrmf-common-lexical)
 */
package org.fao.vrmf.test.common.tools.lexical.processors;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.fao.fi.sh.utility.lexical.soundex.impl.ExtendedPhraseSoundexGenerator;
import org.fao.fi.vrmf.common.tools.lexical.processors.queue.impl.VesselNameSimplifier;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Feb 2013
 */
public class VesselNameSimplifierTest {
	private String[][] _vesselNamesData;
	
	private Logger _logger = LoggerFactory.getLogger(this.getClass().getSimpleName());
	
	@Before
	public void initialize() throws Throwable {
		StringBuilder result = new StringBuilder();
		Charset UTF8 = Charset.forName("UTF-8");
		
		this._logger.info("Reading data file...");
		
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("vessel_names_test.csv");
		byte[] buffer = new byte[8192];
		int len = -1;

		while((len = is.read(buffer)) != -1)
			result.append(new String(buffer, 0, len, UTF8));
		
		this._logger.info("Data file read");
		
		this._logger.info("Converting data file...");
		
		List<String[]> data = new ArrayList<String[]>();
		
		for(String line : result.toString().split("\\n"))
			data.add(line.split("\\,"));
		
		this._logger.info("Data file converted");
		
		data = data.subList(1, data.size());
		
		this._vesselNamesData = new String[data.size()][];
		
		int recordCounter = 0;
		int columnCounter;
		
		String[] updated;
		for(String[] record : data) {
			columnCounter = 0;
			updated = new String[record.length];
			
			for(String part : record) {
				updated[columnCounter++] = part.replaceAll("\"\"", "\"").replaceAll("^\"", "").replaceAll("\"$", "");
			}
			
			this._vesselNamesData[recordCounter++] = updated;
		}
		
		System.out.println(this._vesselNamesData.length + " lines read from test data file");
		
		try {
			is.close();
		} catch (Throwable t) {
			//Suffocate
		}
	}
	
	@Test
	public void testVesselNameSimplifier() {
		VesselNameSimplifier simplifier = new VesselNameSimplifier();
		
		for(String[] data : this._vesselNamesData)
			Assert.assertEquals("Wrong simplified name for " + data[2] + " / " + data[3], data[3], simplifier.process(data[2]));
	}
	
	@Test
	public void testVesselNameSimplifierSoundex() {
		ExtendedPhraseSoundexGenerator soundexGenerator = new ExtendedPhraseSoundexGenerator();
		
		for(String[] data : this._vesselNamesData)
			Assert.assertEquals("Wrong soundex for " + data[2] + " / " + data[3], data[4], soundexGenerator.getFullPhraseSoundex(data[3]));
	}
}
