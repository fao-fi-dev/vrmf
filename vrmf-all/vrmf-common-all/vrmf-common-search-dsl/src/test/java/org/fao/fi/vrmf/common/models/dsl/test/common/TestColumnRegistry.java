/**
 * (c) 2015 FAO / UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.models.dsl.test.common;

import javax.inject.Inject;

import org.fao.fi.vrmf.common.search.dsl.ColumnRegistry;
import org.fao.fi.vrmf.common.search.dsl.impl.Column;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 3, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 3, 2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
	"/config/spring/vrmf-common-search-dsl-spring-context.xml"
})
public class TestColumnRegistry {
	@Inject private ColumnRegistry _registry;
	
	@Test
	public void test() {
		Assert.assertNotNull(_registry);
		Assert.assertNotNull(_registry.availableColumns());
		Assert.assertFalse(_registry.availableColumns().isEmpty());
		
		for(Column in : _registry.availableColumns()) {
			System.out.println("Found " + in.getMnemonic() + " : " + in.getColumn() + " ( of type " + in.getClass().getName() + " ) among available columns");
		}
	}
}
