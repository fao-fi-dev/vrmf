/**
 * (c) 2014 FAO / UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.models.dsl.test.common;

import static org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns.ID;
import static org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns.NUM_SOURCES;
import static org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns.SOURCE;
import static org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns.UID;
import static org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns.UPD_DATE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.meta.AuthorizedVesselRecordDataColumns.AUTHORIZATION_END;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.meta.AuthorizedVesselRecordDataColumns.AUTHORIZATION_ISSUER;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.meta.AuthorizedVesselRecordDataColumns.AUTHORIZATION_START;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.AUTHORIZATION_AT_DATE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.AUTHORIZATION_ISSUERS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.AUTHORIZATION_STATUS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.CALLSIGNS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.CALLSIGNS_LIKE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.NAMES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.NAME_LIKE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.NO_LENGTH;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.TONNAGE_TYPES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.GEAR_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.GT;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.LENGTH_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.LENGTH_VALUE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.LOA;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.NAME;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.PARENT_VESSEL_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.TONNAGE_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.TONNAGE_VALUE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns.VESSEL_TYPE;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.fao.fi.vrmf.common.search.dsl.exceptions.MultipleConditionsForSameClauseRuntimeException;
import org.fao.fi.vrmf.common.search.dsl.exceptions.WrongConditionCardinalityRuntimeException;
import org.fao.fi.vrmf.common.search.dsl.exceptions.WrongConditionValueRuntimeException;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.AuthorizedVesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.support.AuthorizationStatus;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 5 Feb 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 5 Feb 2014
 */
public class TestDSL {
	static final private String DEFAULT_VESSELS_TABLE = "VESSELS";
	
	private AuthorizedVesselRecordSearchFilter getAuthorizedVesselsFilter() {
		Calendar authorizedAtDate = GregorianCalendar.getInstance();
		authorizedAtDate.set(Calendar.YEAR, 2011);
		authorizedAtDate.set(Calendar.MONTH, Calendar.DECEMBER);
		authorizedAtDate.set(Calendar.DAY_OF_MONTH, 31);

		AuthorizedVesselRecordSearchFilter filter = new AuthorizedVesselRecordSearchFilter();
		
		filter.
		FROM(DEFAULT_VESSELS_TABLE).
		SELECT(
			filter.columns().
				include(
					ID,
					UID,
					SOURCE,
					VesselRecordDataColumns.IMO,
					NAME,
					VesselRecordDataColumns.FLAG_ID,
					VesselRecordDataColumns.FLAG,
					LOA,
					LENGTH_TYPE,
					LENGTH_VALUE,
					GT,
					TONNAGE_TYPE,
					TONNAGE_VALUE,
					VESSEL_TYPE,
					PARENT_VESSEL_TYPE,
					GEAR_TYPE,
					UPD_DATE,
					AUTHORIZATION_ISSUER,
					AUTHORIZATION_START,
					AUTHORIZATION_END
				)
		).
		WHERE(
			filter.clauses().
				and(NO_LENGTH, Boolean.TRUE).
				and(TONNAGE_TYPES, "NRT").
				and(VesselRecordClauses.FLAG, 110, 117).
				and(NAME_LIKE, Boolean.TRUE).
				and(NAMES, "K%", "%F%").
				and(AUTHORIZATION_ISSUERS, "ICCAT").
				and(AUTHORIZATION_STATUS, AuthorizationStatus.ENFORCED).
				and(AUTHORIZATION_AT_DATE, authorizedAtDate.getTime()).
				and(CALLSIGNS_LIKE, Boolean.TRUE ).
				and(CALLSIGNS, "WDC%" )
		).
		ORDER_BY(
			filter.sortableColumns().
				descending(UID).
				descending(VesselRecordDataColumns.FLAG).
				descending(NUM_SOURCES).
				ascending(SOURCE).
				ascending(NAME).
				ascending(LENGTH_VALUE).
				ascending(LENGTH_TYPE)
		).
		AMONG(new String[] { "CCSBT", "IATTC", "ICCAT", "IOTC", "WCPFC", "FFA", "SPRFMO", "ISSF", "CTMFM", "SICA", "UNGA61", "CCAMLR", "EU" }).
		GROUP_DUPLICATES().
		LIMIT(30).
		OFFSET(1000).
		lang("FG").
		numberOfSourcesBetween(6, 13).
		linkToSearch().
		linkToSources().
		retrieveAdditionalMetadata().
		duplicatesOnly().
		current().
		nop();
		
		return filter;
	}
	
	@Test
	public void testFromXML() throws Throwable {
		new AuthorizedVesselRecordSearchFilter().fromXML(getAuthorizedVesselsFilter().toXML());
	}
	
	@Test
	public void testToXML() throws Exception {
		System.out.println(getAuthorizedVesselsFilter().toXML());
	}
	
	@Test(expected=WrongConditionCardinalityRuntimeException.class)
	public void testWrongCardinality() {
		VesselRecordSearchFilter filter = new VesselRecordSearchFilter();
		
		filter.
			FROM(DEFAULT_VESSELS_TABLE).
			WHERE(
				filter.clauses().
				and(VesselRecordClauses.IMO, "1234567", "7654321")
			);
	}
	
	@Test(expected=WrongConditionValueRuntimeException.class)
	public void testWrongType() {
		VesselRecordSearchFilter filter = new VesselRecordSearchFilter();
		
		filter.
			FROM(DEFAULT_VESSELS_TABLE).
			WHERE(
				filter.clauses().
				and(VesselRecordClauses.IMO, 1234)
			);
	}
	
	@Test(expected=MultipleConditionsForSameClauseRuntimeException.class)
	public void testMultipleSameClauses() {
		VesselRecordSearchFilter filter = new VesselRecordSearchFilter();
		
		filter.
			FROM(DEFAULT_VESSELS_TABLE).
			WHERE(
				filter.clauses().
				and(VesselRecordClauses.IMO, "1234").
				and(VesselRecordClauses.IMO, "5678")
			);
	}
}