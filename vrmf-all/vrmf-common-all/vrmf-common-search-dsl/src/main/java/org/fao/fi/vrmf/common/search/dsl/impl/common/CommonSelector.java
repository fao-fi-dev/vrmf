/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.common;

import org.fao.fi.vrmf.common.search.dsl.impl.Selector;
import org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
public class CommonSelector extends Selector implements CommonColumnFilter {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -945428495203715288L;
	
	final public boolean getSelectID() {
		return this._selectedColumns.contains(CommonDataColumns.ID);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Selector#getSelectNumberOfSources()
	 */
	
	final public boolean getSelectNumberOfSources() {
		return this._selectedColumns.contains(CommonDataColumns.NUM_SOURCES);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Selector#getSelectSource()
	 */
	
	final public boolean getSelectSource() {
		return this._selectedColumns.contains(CommonDataColumns.SOURCE);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Selector#getSelectUID()
	 */
	
	final public boolean getSelectUID() {
		return this._selectedColumns.contains(CommonDataColumns.UID);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Selector#getSelectLastReferenceDate()
	 */
	
	final public boolean getSelectUpdateDate() {
		return this._selectedColumns.contains(CommonDataColumns.UPD_DATE);
	}

}
