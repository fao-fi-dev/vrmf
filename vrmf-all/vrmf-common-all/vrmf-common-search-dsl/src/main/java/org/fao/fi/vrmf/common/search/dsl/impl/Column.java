/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl;

import javax.inject.Named;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.fao.fi.sh.utility.core.annotations.di.InheritedComponent;
import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
@XmlRootElement(name="column")
@XmlAccessorType(XmlAccessType.FIELD)
@Named @InheritedComponent 
public class Column extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7443245378807125363L;
	
	@XmlTransient private String _mnemonic;
	@XmlAttribute(name="name") private String _column;
	
	/**
	 * Class constructor
	 *
	 */
	public Column() {
		super(); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param mnemonic
	 * @param column
	 */
	public Column(String mnemonic, String column) {
		super();
		this._mnemonic = mnemonic;
		this._column = column;
	}
	
	/**
	 * @return the 'mnemonic' value
	 */
	public String getMnemonic() {
		return this._mnemonic;
	}
	
	/**
	 * @return the 'column' value
	 */
	public String getColumn() {
		return this._column;
	}

	/**
	 * @param mnemonic the 'mnemonic' value to set
	 */
	public void setMnemonic(String mnemonic) {
		this._mnemonic = mnemonic;
	}

	/**
	 * @param column the 'column' value to set
	 */
	public void setColumn(String column) {
		this._column = column;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this._column == null ) ? 0 : this._column.hashCode() );
		result = prime * result + ( ( this._mnemonic == null ) ? 0 : this._mnemonic.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		Column other = (Column) obj;
		if(this._column == null) {
			if(other._column != null) return false;
		} else if(!this._column.equals(other._column)) return false;
		if(this._mnemonic == null) {
			if(other._mnemonic != null) return false;
		} else if(!this._mnemonic.equals(other._mnemonic)) return false;
		return true;
	}
}
