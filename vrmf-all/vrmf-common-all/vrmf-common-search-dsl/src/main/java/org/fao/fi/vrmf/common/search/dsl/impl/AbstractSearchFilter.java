/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;
import org.fao.fi.vrmf.common.core.model.GenericData;
import org.fao.fi.vrmf.common.search.dsl.impl.meta.GroupableDataColumns;
import org.fao.fi.vrmf.common.search.dsl.impl.meta.Stage;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Jul 2011   Fiorellato     Creation.
 *
 * @version 1.0
 *
 * @param <SELECTOR> the generic SELECTOR type
 * @param <WHERE> the generic WHERE type
 * @param <SORTER> the generic SORTER type
 * @param <SELF> the generic SELF type
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
abstract public class AbstractSearchFilter<SELECTOR extends Selector, WHERE extends Where, SORTER extends Sorter, SELF extends AbstractSearchFilter<SELECTOR, WHERE, SORTER, SELF>> extends GenericData {
	
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3272175013525246700L;

	/**
	 * The _stage.
	 */
	@XmlAttribute(name="stage")
	protected Stage _stage;

	/**
	 * The _selector.
	 */
	@XmlElement(name="select")
	protected SELECTOR _selector;

	@XmlElement(name="from")
	private String _from;

	/**
	 * The _where.
	 */
	@XmlElement(name="where")
	protected WHERE _where;

	/**
	 * The _sorter.
	 */
	@XmlElement(name="sortBy")
	protected SORTER _sorter;

	@XmlAttribute(name="offset")
	private Integer _offset = 0;

	@XmlAttribute(name="limit")
	private Integer _limit = 30;

	@XmlElement(name="atDate")
	private Date _atDate;

	@XmlElement(name="groupBy")
	private String _groupBy = "ID";

	@XmlElement(name="duplicatesOnly")
	private Boolean _searchDuplicatesOnly = Boolean.FALSE;

	@XmlElementWrapper(name="sourcedBy")
	@XmlElement(name="source")
	private String[] _availableSourceSystems;

	@XmlElement(name="filteredSources")
	private String[] _sourceSystems;

	@XmlElement(name="minSources")
	private Integer _minSources;

	@XmlElement(name="maxSources")
	private Integer _maxSources;

	@XmlElement(name="updatedFrom")
	private Date _updatedFrom;

	@XmlElement(name="updatedTo")
	private Date _updatedTo;

	@XmlAttribute(name="lang")
	private String _lang;
	
	@XmlTransient
	private Boolean _retrieveAdditionalMetadata = Boolean.FALSE;

	@XmlTransient
	private Boolean _linkSourcesToFilters = Boolean.FALSE;

	@XmlTransient
	private Boolean _showTimeRelatedMetadata = Boolean.FALSE;

	@XmlTransient
	private Boolean _showSystemsRelatedMetadata = Boolean.FALSE;

	@XmlTransient
	private Boolean _linkMetadataToSearchCriteria = Boolean.FALSE;

	/**
	 * Class constructor.
	 */
	public AbstractSearchFilter() {
		super();

		this._stage = Stage.COUNT;
	}
	
	/**
	 * Thi$.
	 *
	 * @return the self
	 */
	abstract protected SELF thi$();

	/**
	 * Selector.
	 *
	 * @return the selector
	 */
	abstract protected SELECTOR selector();
	
	/**
	 * Where.
	 *
	 * @return the where
	 */
	abstract protected WHERE where();
	
	/**
	 * Sorter.
	 *
	 * @return the sorter
	 */
	abstract protected SORTER sorter();
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.search.dsl.Filter#count()
	 */

	/**
	 * Switch to data counting.
	 */
	public void switchToDataCounting() {
		this._stage = Stage.COUNT;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Filter#select()
	 */

	/**
	 * Switch to data selection.
	 */
	public void switchToDataSelection() {
		this._stage = Stage.SELECT;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Filter#retrieve()
	 */

	/**
	 * Switch to metadata retrievement.
	 */
	public void switchToMetadataRetrievement() {
		this._stage = Stage.RETRIEVE_METADATA;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.search.dsl.Filter#retrieveMetadata()
	 */

	/**
	 * Switch to additional metadata retrievement.
	 */
	public void switchToAdditionalMetadataRetrievement() {
		this._stage = Stage.RETRIEVE_ADDITIONAL_METADATA;
	}

	/**
	 * Gets the stage.
	 *
	 * @return the stage
	 */
	public Stage getStage() {
		return this._stage;
	}

	/**
	 * Gets the empty column selector.
	 *
	 * @return the empty column selector
	 */
	protected SELECTOR getEmptyColumnSelector() {
		SELECTOR selector = SerializationHelper.clone(selector());
		
		if(selector != null) selector.reset();

		return selector;
	}

	/**
	 * Gets the column selector.
	 *
	 * @return the column selector
	 */
	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Filter#columnSelector()
	 */
	public ColumnFilter getColumnSelector() {
		if(Stage.COUNT.equals(this._stage))
			return this.getEmptyColumnSelector();

		if(Stage.SELECT.equals(this._stage))
			return this._sorter;

		return this._selector;
	}

	/**
	 * Gets the from.
	 *
	 * @return the 'from' value
	 */
	public String getFrom() {
		return this._from;
	}

	/**
	 * Sets the from.
	 *
	 * @param from the 'from' value to set
	 */
	public void setFrom(String from) {
		this._from = from;
	}

	/**
	 * Gets the at date.
	 *
	 * @return the 'atDate' value
	 */
	public Date getAtDate() {
		return this._atDate;
	}

	/**
	 * Sets the at date.
	 *
	 * @param atDate the 'atDate' value to set
	 */
	public void setAtDate(Date atDate) {
		this._atDate = atDate;
	}

	/**
	 * From.
	 *
	 * @param vesselsTable the vessels table
	 * @return the self
	 */
	public SELF FROM(String vesselsTable) {
		this._from = vesselsTable;
		
		return thi$();
	}

	/**
	 * Among.
	 *
	 * @param availableSources the available sources
	 * @return the self
	 */
	public SELF AMONG(String[] availableSources) {
		this._availableSourceSystems = availableSources;
		
		return thi$();
	}

	/**
	 * Sourced by.
	 *
	 * @param sources the sources
	 * @return the self
	 */
	public SELF SOURCED_BY(String[] sources) {
		this._sourceSystems = sources;
		
		return thi$();
	}

	/**
	 * Sourced by.
	 *
	 * @param source the source
	 * @return the self
	 */
	public SELF SOURCED_BY(String source) {
		if(source == null)
			this._sourceSystems = null;
		else
			this._sourceSystems = new String[] { source };

		return thi$();
	}

	/**
	 * Offset.
	 *
	 * @param offset the offset
	 * @return the self
	 */
	public SELF OFFSET(Integer offset) {
		this._offset = offset;
		
		return thi$();
	}

	/**
	 * Rewind.
	 *
	 * @return the self
	 */
	public SELF rewind() {
		return this.OFFSET(new Integer(0));
	}

	/**
	 * Limit.
	 *
	 * @param limit the limit
	 * @return the self
	 */
	public SELF LIMIT(Integer limit) {
		this._limit = limit;
		
		return thi$();
	}

	/**
	 * Unlimited.
	 *
	 * @return the self
	 */
	public SELF unlimited() {
		return this.LIMIT(null);
	}

	/**
	 * Group duplicates.
	 *
	 * @return the self
	 */
	public SELF GROUP_DUPLICATES() {
		return this.GROUP_BY(GroupableDataColumns.UID);
	}


	/**
	 * Dont group duplicates.
	 *
	 * @return the self
	 */
	public SELF DONT_GROUP_DUPLICATES() {
		return this.GROUP_BY(GroupableDataColumns.ID);
	}
	
	/**
	 * Group by.
	 *
	 * @param groupBy the group by
	 * @return the self
	 */
	public SELF GROUP_BY(GroupableDataColumns groupBy) {
		this._groupBy = ( groupBy == null ? GroupableDataColumns.ID : groupBy ).getColumn();

		return thi$();
	}

	/**
	 * Retrieve additional metadata.
	 *
	 * @return the self
	 */
	public SELF retrieveAdditionalMetadata() {
		this._retrieveAdditionalMetadata = true;
		
		return thi$();
	}

	/**
	 * Duplicates only.
	 *
	 * @return the self
	 */
	public SELF duplicatesOnly() {
		this._searchDuplicatesOnly = true;
		
		return thi$();
	}

	/**
	 * At.
	 *
	 * @param atDate the at date
	 * @return the self
	 */
	public SELF AT(Date atDate) {
		this._atDate = atDate;
		
		return thi$();
	}

	/**
	 * Historical.
	 *
	 * @return the self
	 */
	public SELF historical() {
		return this.AT(null);
	}

	/**
	 * Current.
	 *
	 * @return the self
	 */
	public SELF current() {
		return this.AT(new Date());
	}

	/**
	 * Link to search.
	 *
	 * @return the self
	 */
	public SELF linkToSearch() {
		this._linkMetadataToSearchCriteria = true;
		
		return thi$();
	}

	/**
	 * Link to sources.
	 *
	 * @return the self
	 */
	public SELF linkToSources() {
		this._showSystemsRelatedMetadata = true;
		
		return thi$();
	}

	/**
	 * Link to date.
	 *
	 * @return the self
	 */
	public SELF linkToDate() {
		this._showTimeRelatedMetadata = true;
		
		return thi$();
	}

	/**
	 * Updated from.
	 *
	 * @param date the date
	 * @return the self
	 */
	public SELF updatedFrom(Date date) {
		this._updatedFrom = date;
		
		return thi$();
	}

	/**
	 * Updated to.
	 *
	 * @param date the date
	 * @return the self
	 */
	public SELF updatedTo(Date date) {
		this._updatedTo = date;
		
		return thi$();
	}

	/**
	 * Updated between.
	 *
	 * @param from the from
	 * @param to the to
	 * @return the self
	 */
	public SELF updatedBetween(Date from, Date to) {
		this._updatedFrom = from;
		this._updatedTo = to;
		
		return thi$();
	}

	/**
	 * Number of sources from.
	 *
	 * @param number the number
	 * @return the self
	 */
	public SELF numberOfSourcesFrom(int number) {
		this._minSources = number;
		return thi$();
	}

	/**
	 * Number of sources to.
	 *
	 * @param number the number
	 * @return the self
	 */
	public SELF numberOfSourcesTo(int number) {
		this._maxSources = number;
		
		return thi$();
	}

	/**
	 * Number of sources between.
	 *
	 * @param min the min
	 * @param max the max
	 * @return the self
	 */
	public SELF numberOfSourcesBetween(int min, int max) {
		this._minSources = min;
		this._maxSources = max;
		
		return thi$();
	}

	/**
	 * Nop.
	 *
	 * @return the self
	 */
	public SELF nop() {
		return thi$();
	}

	/**
	 * Gets the offset.
	 *
	 * @return the 'offset' value
	 */
	public Integer getOffset() {
		return this._offset;
	}

	/**
	 * Sets the offset.
	 *
	 * @param offset the 'offset' value to set
	 */
	public void setOffset(Integer offset) {
		this._offset = offset;
	}

	/**
	 * Gets the limit.
	 *
	 * @return the 'limit' value
	 */
	public Integer getLimit() {
		return this._limit;
	}

	/**
	 * Sets the limit.
	 *
	 * @param limit the 'limit' value to set
	 */
	public void setLimit(Integer limit) {
		this._limit = limit;
	}

	/**
	 * Gets the group by.
	 *
	 * @return the 'groupBy' value
	 */
	public String getGroupBy() {
		//System.out.println("groupBy");
		return this._groupBy;
	}

	/**
	 * Sets the group by.
	 *
	 * @param groupBy the 'groupBy' value to set
	 */
	public void setGroupBy(String groupBy) {
		this._groupBy = groupBy;
	}

	/**
	 * Gets the retrieve additional metadata.
	 *
	 * @return the 'retrieveSources' value
	 */
	public Boolean getRetrieveAdditionalMetadata() {
		return this._retrieveAdditionalMetadata;
	}

	/**
	 * Sets the retrieve additional metadata.
	 *
	 * @param retrieveSources the 'retrieveSources' value to set
	 */
	public void setRetrieveAdditionalMetadata(Boolean retrieveSources) {
		this._retrieveAdditionalMetadata = retrieveSources;
	}

	/**
	 * Gets the link sources to filters.
	 *
	 * @return the 'linkSourcesToFilters' value
	 */
	public Boolean getLinkSourcesToFilters() {
		return this._linkSourcesToFilters;
	}

	/**
	 * Sets the link sources to filters.
	 *
	 * @param linkSourcesToFilters the 'linkSourcesToFilters' value to set
	 */
	public void setLinkSourcesToFilters(Boolean linkSourcesToFilters) {
		this._linkSourcesToFilters = linkSourcesToFilters;
	}

	/**
	 * Gets the link metadata to search criteria.
	 *
	 * @return the 'linkMetadataToSearchCriteria' value
	 */
	public Boolean getLinkMetadataToSearchCriteria() {
		return this._linkMetadataToSearchCriteria;
	}

	/**
	 * Sets the link metadata to search criteria.
	 *
	 * @param linkMetadataToSearchCriteria the 'linkMetadataToSearchCriteria' value to set
	 */
	public void setLinkMetadataToSearchCriteria(Boolean linkMetadataToSearchCriteria) {
		this._linkMetadataToSearchCriteria = linkMetadataToSearchCriteria;
	}

	/**
	 * Gets the show time related metadata.
	 *
	 * @return the 'showTimeRelatedMetadata' value
	 */
	public Boolean getShowTimeRelatedMetadata() {
		return this._showTimeRelatedMetadata;
	}

	/**
	 * Sets the show time related metadata.
	 *
	 * @param showTimeRelatedMetadata the 'showTimeRelatedMetadata' value to set
	 */
	public void setShowTimeRelatedMetadata(Boolean showTimeRelatedMetadata) {
		this._showTimeRelatedMetadata = showTimeRelatedMetadata;
	}

	/**
	 * Gets the show systems related metadata.
	 *
	 * @return the 'showSystemsRelatedMetadata' value
	 */
	public Boolean getShowSystemsRelatedMetadata() {
		return this._showSystemsRelatedMetadata;
	}

	/**
	 * Sets the show systems related metadata.
	 *
	 * @param showSystemsRelatedMetadata the 'showSystemsRelatedMetadata' value to set
	 */
	public void setShowSystemsRelatedMetadata(Boolean showSystemsRelatedMetadata) {
		this._showSystemsRelatedMetadata = showSystemsRelatedMetadata;
	}

	/**
	 * Gets the available source systems.
	 *
	 * @return the 'availableSourceSystems' value
	 */
	public String[] getAvailableSourceSystems() {
		return this._availableSourceSystems;
	}

	/**
	 * Sets the available source systems.
	 *
	 * @param availableSourceSystems the 'availableSourceSystems' value to set
	 */
	public void setAvailableSourceSystems(String[] availableSourceSystems) {
		this._availableSourceSystems = availableSourceSystems;
	}

	/**
	 * Gets the source systems.
	 *
	 * @return the 'sourceSystems' value
	 */
	public String[] getSourceSystems() {
		return this._sourceSystems;
	}

	/**
	 * Sets the source systems.
	 *
	 * @param sourceSystems the 'sourceSystems' value to set
	 */
	public void setSourceSystems(String[] sourceSystems) {
		this._sourceSystems = sourceSystems;
	}

	/**
	 * Gets the min sources.
	 *
	 * @return the 'minSources' value
	 */
	public Integer getMinSources() {
		return this._minSources;
	}

	/**
	 * Sets the min sources.
	 *
	 * @param minSources the 'minSources' value to set
	 */
	public void setMinSources(Integer minSources) {
		this._minSources = minSources;
	}

	/**
	 * Gets the max sources.
	 *
	 * @return the 'maxSources' value
	 */
	public Integer getMaxSources() {
		return this._maxSources;
	}

	/**
	 * Sets the max sources.
	 *
	 * @param maxSources the 'maxSources' value to set
	 */
	public void setMaxSources(Integer maxSources) {
		this._maxSources = maxSources;
	}

	/**
	 * Gets the updated from.
	 *
	 * @return the 'updatedFrom' value
	 */
	public Date getUpdatedFrom() {
		return this._updatedFrom;
	}

	/**
	 * Sets the updated from.
	 *
	 * @param updatedFrom the 'updatedFrom' value to set
	 */
	public void setUpdatedFrom(Date updatedFrom) {
		this._updatedFrom = updatedFrom;
	}

	/**
	 * Gets the updated to.
	 *
	 * @return the 'updatedTo' value
	 */
	public Date getUpdatedTo() {
		return this._updatedTo;
	}

	/**
	 * Sets the updated to.
	 *
	 * @param updatedTo the 'updatedTo' value to set
	 */
	public void setUpdatedTo(Date updatedTo) {
		this._updatedTo = updatedTo;
	}

	/**
	 * Gets the search duplicates only.
	 *
	 * @return the 'searchDuplicatesOnly' value
	 */
	public Boolean getSearchDuplicatesOnly() {
		//System.out.println("searchDuplicatesOnly");
		return this._searchDuplicatesOnly;
	}

	/**
	 * Sets the search duplicates only.
	 *
	 * @param searchDuplicatesOnly the 'searchDuplicatesOnly' value to set
	 */
	public void setSearchDuplicatesOnly(Boolean searchDuplicatesOnly) {
		this._searchDuplicatesOnly = searchDuplicatesOnly;
	}

	/**
	 * Use like.
	 *
	 * @param value the value
	 * @return true, if successful
	 */
	protected boolean useLike(String value) {
		return value == null ? false : ( value.contains("%") || value.contains("_") );
	}

	/**
	 * Select.
	 *
	 * @param selector the selector
	 * @return the self
	 */
	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Filter#select(org.fao.vrmf.common.services.request.search.dsl.interfaces.ColumnSelector)
	 */
	public SELF SELECT(SELECTOR selector) {
		this._selector = selector;

		return thi$();
	}

	/**
	 * Order by.
	 *
	 * @param sorter the sorter
	 * @return the self
	 */
	public SELF ORDER_BY(SORTER sorter) {
		this._sorter = sorter;

		return thi$();
	}

	/**
	 * Where.
	 *
	 * @param where the where
	 * @return the self
	 */
	public SELF WHERE(WHERE where) {
		this._where = where;

		return thi$();
	}

	/**
	 * Gets the conditions.
	 *
	 * @return the conditions
	 */
	public Map<Clause, Condition> getConditions() {
		return this.clauses().getConditions();
	}

	/**
	 * Gets the mnemonic conditions.
	 *
	 * @return the mnemonic conditions
	 */
	public Map<String, Object> getMnemonicConditions() {
		return this.clauses().getMnemonicConditions();
	}

	/**
	 * Gets the filter duplicates.
	 *
	 * @return the filter duplicates
	 */
	public boolean getFilterDuplicates() {
		return GroupableDataColumns.UID.name().equals(this._groupBy) && (
			   this._searchDuplicatesOnly ||
			   this.getFilterNumberOfSources() ||
			   this.getFilterUpdateDate() ||
			   ( this._sorter != null &&
			   	 ( this._sorter.getSelectNumberOfSources() ||
			   	   this._sorter.getSelectUpdateDate() ) )
		);
	}

	/**
	 * Gets the filter source.
	 *
	 * @return the filter source
	 */
	public boolean getFilterSource() {
		return this._sorter.getSelectSource();
	}

	/**
	 * Gets the filter number of sources.
	 *
	 * @return the filter number of sources
	 */
	public boolean getFilterNumberOfSources() {
		return   this.getSearchDuplicatesOnly() ||
			     this._minSources != null ||
			     this._maxSources != null ||
			   ( this._sorter != null && this._sorter.getSelectNumberOfSources() );
	}

	/**
	 * Gets the filter update date.
	 *
	 * @return the filter update date
	 */
	public boolean getFilterUpdateDate() {
		return this._updatedFrom != null ||
			   this._updatedTo != null ||
			 ( this._sorter != null && this._sorter.getSelectUpdateDate() );
	}

	/**
	 * Columns.
	 *
	 * @return the selector
	 */
	public SELECTOR columns() {
		if(this._selector == null)
			this._selector = selector();
		
		return (SELECTOR)this._selector;
	}
	
	/**
	 * Sortable columns.
	 *
	 * @return the sorter
	 */
	public SORTER sortableColumns() {
		if(this._sorter == null)
			this._sorter = sorter();
		
		return (SORTER)this._sorter;
	}

	/**
	 * Clauses.
	 *
	 * @return the where
	 */
	public WHERE clauses() {
		if(this._where == null)
			this._where = where();
		
		return (WHERE)this._where;
	}
	
	/**
	 * Gets the clauses.
	 *
	 * @return the clauses
	 */
	public WHERE getClauses() {
		return this.clauses();
	}
	
	/**
	 * Gets the columns.
	 *
	 * @return the columns
	 */
	public SELECTOR getColumns() {
		return this.columns();
	}
	
	/**
	 * Gets the sortable columns.
	 *
	 * @return the sortable columns
	 */
	public SORTER getSortableColumns() {
		return this.sortableColumns();
	}
	
	/**
	 * Lang.
	 *
	 * @param lang the lang
	 * @return the self
	 */
	public SELF lang(String lang) {
		this._lang = lang;
		
		return thi$();
	}
	
	/**
	 * Gets the lang.
	 *
	 * @return the 'lang' value
	 */
	public String getLang() {
		return _lang;
	}

	/**
	 * Context.
	 *
	 * @return the list
	 */
	@SuppressWarnings("rawtypes")
	abstract public List<Class> context();
	
	/**
	 * To xml.
	 *
	 * @return the string
	 */
	final public String toXML() {
		try {
			return JAXBHelper.toXML(this.context().toArray(new Class[0]), this);
		} catch(Exception e) {
			throw new RuntimeException("Cannot serialize filter to XML: " + e.getMessage(), e);
		}
	}
	
	/**
	 * From xml.
	 *
	 * @param xml the xml
	 * @return the self
	 */
	@SuppressWarnings("unchecked")
	final public SELF fromXML(String xml) {
		try {
			return (SELF)JAXBHelper.fromXML(this.getClass(), this.context().toArray(new Class[0]), xml);
		} catch(Exception e) {
			throw new RuntimeException("Cannot deserialize filter from XML: " + e.getMessage(), e);
		}
	}
}