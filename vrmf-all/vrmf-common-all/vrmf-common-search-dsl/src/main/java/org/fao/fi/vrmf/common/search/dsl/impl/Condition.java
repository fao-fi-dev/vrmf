/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Feb 2013
 */
@XmlRootElement(name="condition")
@XmlAccessorType(XmlAccessType.FIELD)
public class Condition extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1332650815906159857L;
	
	@XmlElement(name="clause")
	private Clause _clause;
	
	@XmlElementWrapper(name="values")
	@XmlElement(name="value")
	private List<Object> _values;
	
	/**
	 * Class constructor
	 */
	public Condition() {
	}
	
	public Condition(Clause clause, List<Object> value) {
		this._clause = clause;
		this._values = value == null ? null : value;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Condition#getClause()
	 */
	public Clause getClause() {
		return this._clause;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Condition#getValue()
	 */
	public List<Object> getValues() {
		return this._values;
	}
	
	public Object getValue() {
		if(_clause._multiple) return _values;
		
		return _values == null ? null : _values.get(0);
	}
	
	final public String toString() {
		return "'" + _clause._mnemonic + "' -> [ " + _clause.getExpectedType() + " ]" + ( _clause._multiple ? "[]" : "") + " @ " + this.getClass().getName();
	}
	
	final public String asString() {
		return new StringBuilder().
						append(_clause.asString()).append("|").
						append(CollectionsHelper.join(_values, "|")).
					toString();
	}
}