/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.vrmf.common.models.extended.ExtendedAuthorizations;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToInspection;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToIuuList;
import org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToPortEntryDenial;
import org.fao.fi.vrmf.common.models.generated.Vessels;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Nov 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Nov 2010
 */
@XmlType(name="vessel")
@XmlAccessorType(XmlAccessType.FIELD)
public class VesselRecord extends Vessels {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7801806468193779051L;

	@XmlElement(name="imo")
	protected String _IMO;
	
	@XmlElement(name="euCfr")
	protected String _EUCFR;
	
	@XmlElement(name="flag")
	protected String _flag;
	
	@XmlElement(name="flagCode")
	protected Integer _flagID;
	
	@XmlElement(name="name")
	protected String _name;
	
	@XmlElement(name="simplifiedName")
	protected String _simplifiedName;
	
	@XmlElement(name="callsign")
	protected String _callsign;

	@XmlElement(name="callsignCountry")
	protected String _callsignCountry;
	
	@XmlElement(name="callsignCountryCode")
	protected Integer _callsignCountryID;
	
	@XmlElement(name="registrationNumber")
	protected String _registrationNumber;
	
	@XmlElement(name="registrationPort")
	protected String _registrationPort;
	
	@XmlElement(name="registrationPortCode")
	protected Integer _registrationPortID;

	@XmlElement(name="registrationCountry")
	protected String _registrationCountry;
	
	@XmlElement(name="registrationCountryCode")
	protected Integer _registrationCountryID;
	
	@XmlElement(name="vesselTypeCode")
	protected Integer _vesselTypeID;
	
	@XmlElement(name="vesselType")
	protected String _vesselType;
	
	@XmlElement(name="parentVesselTypeCode")
	protected Integer _parentVesselTypeID;
	
	@XmlElement(name="parentVesselType")
	protected String _parentVesselType;
	
	@XmlElement(name="gearTypeCode")
	protected Integer _gearTypeID;
	
	@XmlElement(name="gearType")
	protected String _gearType;
	
	@XmlElement(name="currentLengthType")
	protected String _currentLengthType;
	
	@XmlElement(name="currentLengthValue")
	protected Float _currentLengthValue;
	
	@XmlElement(name="LOA")
	protected Float _LOA;
	
	@XmlElement(name="currentTonnageType")
	protected String _currentTonnageType;
	
	@XmlElement(name="currentTonnageValue")
	protected Float _currentTonnageValue;
	
	@XmlElement(name="GT")
	protected Float _GT;
	
	@XmlElement(name="numberOfSources")
	protected Integer _numberOfSources;
	
	@XmlElement(name="duplicateSources")
	protected Collection<String> _sourceSystems;
	
	@XmlElementWrapper(name="authorizations", required=false, nillable=true)
	@XmlElement(name="authorization")
	protected List<ExtendedAuthorizations> authorizations;

	@XmlElementWrapper(name="inspections", required=false, nillable=true)
	@XmlElement(name="inspection")
	protected List<ExtendedVesselToInspection> inspections;
	
	@XmlElementWrapper(name="portEntryDenials", required=false, nillable=true)
	@XmlElement(name="portEntryDenial")
	protected List<ExtendedVesselToPortEntryDenial> portDenials;
	
	@XmlElementWrapper(name="iuuListings", required=false, nillable=true)
	@XmlElement(name="iuuListing")
	protected List<ExtendedVesselToIuuList> iuuListings;
	/**
	 * Class constructor
	 */
	public VesselRecord() {
		super();
	}

	/**
	 * @return the 'iMO' value
	 */
	public String getIMO() {
		return this._IMO;
	}

	/**
	 * @param iMO the 'iMO' value to set
	 */
	public void setIMO(String iMO) {
		this._IMO = iMO;
	}

	/**
	 * @return the 'eUCFR' value
	 */
	public String getEUCFR() {
		return this._EUCFR;
	}

	/**
	 * @param eUCFR the 'eUCFR' value to set
	 */
	public void setEUCFR(String eUCFR) {
		this._EUCFR = eUCFR;
	}

	/**
	 * @return the 'flag' value
	 */
	public String getFlag() {
		return this._flag;
	}

	/**
	 * @param flag the 'flag' value to set
	 */
	public void setFlag(String flag) {
		this._flag = flag;
	}

	/**
	 * @return the 'flagID' value
	 */
	public Integer getFlagID() {
		return this._flagID;
	}

	/**
	 * @param flagID the 'flagID' value to set
	 */
	public void setFlagID(Integer flagID) {
		this._flagID = flagID;
	}

	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return this._name;
	}

	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		this._name = name;
	}

	/**
	 * @return the 'simplifiedName' value
	 */
	public String getSimplifiedName() {
		return this._simplifiedName;
	}

	/**
	 * @param simplifiedName the 'simplifiedName' value to set
	 */
	public void setSimplifiedName(String simplifiedName) {
		this._simplifiedName = simplifiedName;
	}

	/**
	 * @return the 'callsign' value
	 */
	public String getCallsign() {
		return this._callsign;
	}

	/**
	 * @param callsign the 'callsign' value to set
	 */
	public void setCallsign(String callsign) {
		this._callsign = callsign;
	}

	/**
	 * @return the 'registrationNumber' value
	 */
	public String getRegistrationNumber() {
		return this._registrationNumber;
	}

	/**
	 * @param registrationNumber the 'registrationNumber' value to set
	 */
	public void setRegistrationNumber(String registrationNumber) {
		this._registrationNumber = registrationNumber;
	}

	/**
	 * @return the 'registrationPort' value
	 */
	public String getRegistrationPort() {
		return this._registrationPort;
	}

	/**
	 * @param registrationPort the 'registrationPort' value to set
	 */
	public void setRegistrationPort(String registrationPort) {
		this._registrationPort = registrationPort;
	}

	/**
	 * @return the 'registrationPortID' value
	 */
	public Integer getRegistrationPortID() {
		return this._registrationPortID;
	}

	/**
	 * @param registrationPortID the 'registrationPortID' value to set
	 */
	public void setRegistrationPortID(Integer registrationPortID) {
		this._registrationPortID = registrationPortID;
	}

	/**
	 * @return the 'registrationCountryID' value
	 */
	public Integer getRegistrationCountryID() {
		return this._registrationCountryID;
	}

	/**
	 * @param registrationCountryID the 'registrationCountryID' value to set
	 */
	public void setRegistrationCountryID(Integer registrationCountryID) {
		this._registrationCountryID = registrationCountryID;
	}

	/**
	 * @return the 'vesselTypeID' value
	 */
	public Integer getVesselTypeID() {
		return this._vesselTypeID;
	}

	/**
	 * @param vesselTypeID the 'vesselTypeID' value to set
	 */
	public void setVesselTypeID(Integer vesselTypeID) {
		this._vesselTypeID = vesselTypeID;
	}

	/**
	 * @return the 'vesselType' value
	 */
	public String getVesselType() {
		return this._vesselType;
	}

	/**
	 * @param vesselType the 'vesselType' value to set
	 */
	public void setVesselType(String vesselType) {
		this._vesselType = vesselType;
	}

	/**
	 * @return the 'gearTypeID' value
	 */
	public Integer getGearTypeID() {
		return this._gearTypeID;
	}

	/**
	 * @param gearTypeID the 'gearTypeID' value to set
	 */
	public void setGearTypeID(Integer gearTypeID) {
		this._gearTypeID = gearTypeID;
	}

	/**
	 * @return the 'gearType' value
	 */
	public String getGearType() {
		return this._gearType;
	}

	/**
	 * @param gearType the 'gearType' value to set
	 */
	public void setGearType(String gearType) {
		this._gearType = gearType;
	}

	/**
	 * @return the 'currentLengthType' value
	 */
	public String getCurrentLengthType() {
		return this._currentLengthType;
	}

	/**
	 * @param currentLengthType the 'currentLengthType' value to set
	 */
	public void setCurrentLengthType(String currentLengthType) {
		this._currentLengthType = currentLengthType;
	}

	/**
	 * @return the 'currentLengthValue' value
	 */
	public Float getCurrentLengthValue() {
		return this._currentLengthValue;
	}

	/**
	 * @param currentLengthValue the 'currentLengthValue' value to set
	 */
	public void setCurrentLengthValue(Float currentLengthValue) {
		this._currentLengthValue = currentLengthValue;
	}

	/**
	 * @return the 'lOA' value
	 */
	public Float getLOA() {
		return this._LOA;
	}

	/**
	 * @param lOA the 'lOA' value to set
	 */
	public void setLOA(Float lOA) {
		this._LOA = lOA;
	}

	/**
	 * @return the 'currentTonnageType' value
	 */
	public String getCurrentTonnageType() {
		return this._currentTonnageType;
	}

	/**
	 * @param currentTonnageType the 'currentTonnageType' value to set
	 */
	public void setCurrentTonnageType(String currentTonnageType) {
		this._currentTonnageType = currentTonnageType;
	}

	/**
	 * @return the 'currentTonnageValue' value
	 */
	public Float getCurrentTonnageValue() {
		return this._currentTonnageValue;
	}

	/**
	 * @param currentTonnageValue the 'currentTonnageValue' value to set
	 */
	public void setCurrentTonnageValue(Float currentTonnageValue) {
		this._currentTonnageValue = currentTonnageValue;
	}

	/**
	 * @return the 'gT' value
	 */
	public Float getGT() {
		return this._GT;
	}

	/**
	 * @param gT the 'gT' value to set
	 */
	public void setGT(Float gT) {
		this._GT = gT;
	}

	/**
	 * @return the 'numberOfSources' value
	 */
	public Integer getNumberOfSources() {
		return this._numberOfSources;
	}

	/**
	 * @param numberOfSources the 'numberOfSources' value to set
	 */
	public void setNumberOfSources(Integer numberOfSources) {
		this._numberOfSources = numberOfSources;
	}

	/**
	 * @return the 'sourceSystems' value
	 */
	public Collection<String> getSourceSystems() {
		return this._sourceSystems;
	}

	/**
	 * @param sourceSystems the 'sourceSystems' value to set
	 */
	public void setSourceSystems(Collection<String> sourceSystems) {
		this._sourceSystems = sourceSystems;
	}

	/**
	 * @return the 'registrationCountry' value
	 */
	public String getRegistrationCountry() {
		return this._registrationCountry;
	}

	/**
	 * @param registrationCountry the 'registrationCountry' value to set
	 */
	public void setRegistrationCountry(String registrationCountry) {
		this._registrationCountry = registrationCountry;
	}

	/**
	 * @return the 'callsignCountry' value
	 */
	public String getCallsignCountry() {
		return this._callsignCountry;
	}

	/**
	 * @param callsignCountry the 'callsignCountry' value to set
	 */
	public void setCallsignCountry(String callsignCountry) {
		this._callsignCountry = callsignCountry;
	}

	/**
	 * @return the 'callsignCountryID' value
	 */
	public Integer getCallsignCountryID() {
		return this._callsignCountryID;
	}

	/**
	 * @param callsignCountryID the 'callsignCountryID' value to set
	 */
	public void setCallsignCountryID(Integer callsignCountryID) {
		this._callsignCountryID = callsignCountryID;
	}

	/**
	 * @return the 'parentVesselTypeID' value
	 */
	public Integer getParentVesselTypeID() {
		return _parentVesselTypeID;
	}

	/**
	 * @param parentVesselTypeID the 'parentVesselTypeID' value to set
	 */
	public void setParentVesselTypeID(Integer parentVesselTypeID) {
		_parentVesselTypeID = parentVesselTypeID;
	}

	/**
	 * @return the 'parentVesselType' value
	 */
	public String getParentVesselType() {
		return _parentVesselType;
	}

	/**
	 * @param parentVesselType the 'parentVesselType' value to set
	 */
	public void setParentVesselType(String parentVesselType) {
		_parentVesselType = parentVesselType;
	}
	
	/**
	 * @return the 'authorizations' value
	 */
	public final List<ExtendedAuthorizations> getAuthorizations() {
		return authorizations;
	}

	/**
	 * @param authorizations the 'authorizations' value to set
	 */
	public final void setAuthorizations(List<ExtendedAuthorizations> authorizations) {
		this.authorizations = authorizations;
	}

	/**
	 * @return the 'inspections' value
	 */
	public final List<ExtendedVesselToInspection> getInspections() {
		return inspections;
	}

	/**
	 * @param inspections the 'inspections' value to set
	 */
	public final void setInspections(List<ExtendedVesselToInspection> inspections) {
		this.inspections = inspections;
	}

	/**
	 * @return the 'portDenials' value
	 */
	public final List<ExtendedVesselToPortEntryDenial> getPortDenials() {
		return portDenials;
	}

	/**
	 * @param portDenials the 'portDenials' value to set
	 */
	public final void setPortDenials(List<ExtendedVesselToPortEntryDenial> portDenials) {
		this.portDenials = portDenials;
	}

	/**
	 * @return the 'iuuListings' value
	 */
	public final List<ExtendedVesselToIuuList> getIuuListings() {
		return iuuListings;
	}

	/**
	 * @param iuuListings the 'iuuListings' value to set
	 */
	public final void setIuuListings(List<ExtendedVesselToIuuList> iuuListings) {
		this.iuuListings = iuuListings;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _EUCFR == null ) ? 0 : _EUCFR.hashCode() );
		result = prime * result + ( ( _GT == null ) ? 0 : _GT.hashCode() );
		result = prime * result + ( ( _IMO == null ) ? 0 : _IMO.hashCode() );
		result = prime * result + ( ( _LOA == null ) ? 0 : _LOA.hashCode() );
		result = prime * result + ( ( _callsign == null ) ? 0 : _callsign.hashCode() );
		result = prime * result + ( ( _callsignCountry == null ) ? 0 : _callsignCountry.hashCode() );
		result = prime * result + ( ( _callsignCountryID == null ) ? 0 : _callsignCountryID.hashCode() );
		result = prime * result + ( ( _currentLengthType == null ) ? 0 : _currentLengthType.hashCode() );
		result = prime * result + ( ( _currentLengthValue == null ) ? 0 : _currentLengthValue.hashCode() );
		result = prime * result + ( ( _currentTonnageType == null ) ? 0 : _currentTonnageType.hashCode() );
		result = prime * result + ( ( _currentTonnageValue == null ) ? 0 : _currentTonnageValue.hashCode() );
		result = prime * result + ( ( _flag == null ) ? 0 : _flag.hashCode() );
		result = prime * result + ( ( _flagID == null ) ? 0 : _flagID.hashCode() );
		result = prime * result + ( ( _gearType == null ) ? 0 : _gearType.hashCode() );
		result = prime * result + ( ( _gearTypeID == null ) ? 0 : _gearTypeID.hashCode() );
		result = prime * result + ( ( _name == null ) ? 0 : _name.hashCode() );
		result = prime * result + ( ( _numberOfSources == null ) ? 0 : _numberOfSources.hashCode() );
		result = prime * result + ( ( _parentVesselType == null ) ? 0 : _parentVesselType.hashCode() );
		result = prime * result + ( ( _parentVesselTypeID == null ) ? 0 : _parentVesselTypeID.hashCode() );
		result = prime * result + ( ( _registrationCountry == null ) ? 0 : _registrationCountry.hashCode() );
		result = prime * result + ( ( _registrationCountryID == null ) ? 0 : _registrationCountryID.hashCode() );
		result = prime * result + ( ( _registrationNumber == null ) ? 0 : _registrationNumber.hashCode() );
		result = prime * result + ( ( _registrationPort == null ) ? 0 : _registrationPort.hashCode() );
		result = prime * result + ( ( _registrationPortID == null ) ? 0 : _registrationPortID.hashCode() );
		result = prime * result + ( ( _simplifiedName == null ) ? 0 : _simplifiedName.hashCode() );
		result = prime * result + ( ( _sourceSystems == null ) ? 0 : _sourceSystems.hashCode() );
		result = prime * result + ( ( _vesselType == null ) ? 0 : _vesselType.hashCode() );
		result = prime * result + ( ( _vesselTypeID == null ) ? 0 : _vesselTypeID.hashCode() );
		result = prime * result + ( ( authorizations == null ) ? 0 : authorizations.hashCode() );
		result = prime * result + ( ( inspections == null ) ? 0 : inspections.hashCode() );
		result = prime * result + ( ( iuuListings == null ) ? 0 : iuuListings.hashCode() );
		result = prime * result + ( ( portDenials == null ) ? 0 : portDenials.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		VesselRecord other = (VesselRecord) obj;
		if(_EUCFR == null) {
			if(other._EUCFR != null) return false;
		} else if(!_EUCFR.equals(other._EUCFR)) return false;
		if(_GT == null) {
			if(other._GT != null) return false;
		} else if(!_GT.equals(other._GT)) return false;
		if(_IMO == null) {
			if(other._IMO != null) return false;
		} else if(!_IMO.equals(other._IMO)) return false;
		if(_LOA == null) {
			if(other._LOA != null) return false;
		} else if(!_LOA.equals(other._LOA)) return false;
		if(_callsign == null) {
			if(other._callsign != null) return false;
		} else if(!_callsign.equals(other._callsign)) return false;
		if(_callsignCountry == null) {
			if(other._callsignCountry != null) return false;
		} else if(!_callsignCountry.equals(other._callsignCountry)) return false;
		if(_callsignCountryID == null) {
			if(other._callsignCountryID != null) return false;
		} else if(!_callsignCountryID.equals(other._callsignCountryID)) return false;
		if(_currentLengthType == null) {
			if(other._currentLengthType != null) return false;
		} else if(!_currentLengthType.equals(other._currentLengthType)) return false;
		if(_currentLengthValue == null) {
			if(other._currentLengthValue != null) return false;
		} else if(!_currentLengthValue.equals(other._currentLengthValue)) return false;
		if(_currentTonnageType == null) {
			if(other._currentTonnageType != null) return false;
		} else if(!_currentTonnageType.equals(other._currentTonnageType)) return false;
		if(_currentTonnageValue == null) {
			if(other._currentTonnageValue != null) return false;
		} else if(!_currentTonnageValue.equals(other._currentTonnageValue)) return false;
		if(_flag == null) {
			if(other._flag != null) return false;
		} else if(!_flag.equals(other._flag)) return false;
		if(_flagID == null) {
			if(other._flagID != null) return false;
		} else if(!_flagID.equals(other._flagID)) return false;
		if(_gearType == null) {
			if(other._gearType != null) return false;
		} else if(!_gearType.equals(other._gearType)) return false;
		if(_gearTypeID == null) {
			if(other._gearTypeID != null) return false;
		} else if(!_gearTypeID.equals(other._gearTypeID)) return false;
		if(_name == null) {
			if(other._name != null) return false;
		} else if(!_name.equals(other._name)) return false;
		if(_numberOfSources == null) {
			if(other._numberOfSources != null) return false;
		} else if(!_numberOfSources.equals(other._numberOfSources)) return false;
		if(_parentVesselType == null) {
			if(other._parentVesselType != null) return false;
		} else if(!_parentVesselType.equals(other._parentVesselType)) return false;
		if(_parentVesselTypeID == null) {
			if(other._parentVesselTypeID != null) return false;
		} else if(!_parentVesselTypeID.equals(other._parentVesselTypeID)) return false;
		if(_registrationCountry == null) {
			if(other._registrationCountry != null) return false;
		} else if(!_registrationCountry.equals(other._registrationCountry)) return false;
		if(_registrationCountryID == null) {
			if(other._registrationCountryID != null) return false;
		} else if(!_registrationCountryID.equals(other._registrationCountryID)) return false;
		if(_registrationNumber == null) {
			if(other._registrationNumber != null) return false;
		} else if(!_registrationNumber.equals(other._registrationNumber)) return false;
		if(_registrationPort == null) {
			if(other._registrationPort != null) return false;
		} else if(!_registrationPort.equals(other._registrationPort)) return false;
		if(_registrationPortID == null) {
			if(other._registrationPortID != null) return false;
		} else if(!_registrationPortID.equals(other._registrationPortID)) return false;
		if(_simplifiedName == null) {
			if(other._simplifiedName != null) return false;
		} else if(!_simplifiedName.equals(other._simplifiedName)) return false;
		if(_sourceSystems == null) {
			if(other._sourceSystems != null) return false;
		} else if(!_sourceSystems.equals(other._sourceSystems)) return false;
		if(_vesselType == null) {
			if(other._vesselType != null) return false;
		} else if(!_vesselType.equals(other._vesselType)) return false;
		if(_vesselTypeID == null) {
			if(other._vesselTypeID != null) return false;
		} else if(!_vesselTypeID.equals(other._vesselTypeID)) return false;
		if(authorizations == null) {
			if(other.authorizations != null) return false;
		} else if(!authorizations.equals(other.authorizations)) return false;
		if(inspections == null) {
			if(other.inspections != null) return false;
		} else if(!inspections.equals(other.inspections)) return false;
		if(iuuListings == null) {
			if(other.iuuListings != null) return false;
		} else if(!iuuListings.equals(other.iuuListings)) return false;
		if(portDenials == null) {
			if(other.portDenials != null) return false;
		} else if(!portDenials.equals(other.portDenials)) return false;
		return true;
	}
}