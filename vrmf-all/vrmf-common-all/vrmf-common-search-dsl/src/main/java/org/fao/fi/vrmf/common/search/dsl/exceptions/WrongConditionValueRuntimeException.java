/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.exceptions;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;

import org.fao.fi.vrmf.common.search.dsl.impl.Clause;
import org.fao.fi.vrmf.common.search.dsl.impl.Condition;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Mar 2013
 */
public class WrongConditionValueRuntimeException extends DSLRuntimeException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2337560345134158866L;

	private Condition _condition;
	private Clause _clause;
	
	public WrongConditionValueRuntimeException(Condition condition, Clause clause) {
		super("Wrong condition value type for " + condition + ": " +
			  "expected type: [ " + clause.getExpectedType().getCanonicalName() + (clause.isMultiple() ? "[]" : "" ) + " ], " +
			  "actual type: [ " + getActualType(condition, clause).getCanonicalName() + (clause.isMultiple() ? "[]" : "" ) + " ], " +
			  "actual value: [ " + condition.getValues() + " ]");
		
		this._condition = condition;
		this._clause = clause;
	}

	/**
	 * @return the 'condition' value
	 */
	public Condition getCondition() {
		return this._condition;
	}

	/**
	 * @return the 'clause' value
	 */
	public Clause getClause() {
		return this._clause;
	}
	
	static private Class<?> getActualType(Condition condition, Clause clause) {
		return clause.isMultiple() ? getMultipleClauseActualType(condition, clause)	: getClauseActualType(condition, clause);	
	}
	
	static private Class<?> getMultipleClauseActualType(Condition condition, Clause clause) {
		Class<?> valueClass = condition.getValue().getClass();
		
		boolean isArray = valueClass.isArray();
		boolean isCollection = Collection.class.isAssignableFrom(valueClass);
		boolean isGenericCollection = isCollection && valueClass.getGenericSuperclass() instanceof ParameterizedType;
		
		Object value = null;
		
		return isArray ? valueClass.getComponentType() : isGenericCollection ? ( ( value = getActualValue(condition) ) == null ? Void.class : value.getClass()) : valueClass;
	}
	
	static private Class<?> getClauseActualType(Condition condition, Clause clause) {
		Object value = null;
		
		return ( value = getActualValue(condition) ) == null ? Void.class : value.getClass();
	}
	
	static private Object getActualValue(Condition condition) {
		Object value = condition.getValue();
		
		if(value == null) return null;
		if(value instanceof Collection)
			return ((Collection<?>)condition.getValue()).toArray(new Object[0])[0];
		if(value.getClass().isArray())
			return ((Object[])value)[0];
		
		return condition.getValue();
	}
}
