/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels;

import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.search.dsl.impl.common.CommonSorter;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns;

@XmlRootElement
public class VesselRecordSorter extends CommonSorter implements VesselRecordColumnFilter {

	private static final long serialVersionUID = 8088117384533564889L;

	@Override
	public boolean getSelectIMO() {
		return this.mustSelect(VesselRecordDataColumns.IMO);
	}

	@Override
	public boolean getSelectEUCFR() {
		return this.mustSelect(VesselRecordDataColumns.EU_CFR);
	}

	@Override
	public boolean getSelectName() {
		return this.mustSelect(VesselRecordDataColumns.NAME);
	}

	@Override
	public boolean getSelectFlag() {
		return this.mustSelect(VesselRecordDataColumns.FLAG);
	}

	@Override
	public boolean getSelectCallsign() {
		return this.mustSelect(VesselRecordDataColumns.CALLSIGN);
	}

	@Override
	public boolean getSelectRegistrationNumber() {
		return this.mustSelect(VesselRecordDataColumns.REG_NO);
	}

	@Override
	public boolean getSelectRegistrationCountry() {
		return this.mustSelect(VesselRecordDataColumns.REG_COUNTRY);
	}

	@Override
	public boolean getSelectRegistrationPort() {
		return this.mustSelect(VesselRecordDataColumns.REG_PORT);
	}

	@Override
	public boolean getSelectLOA() {
		return this.mustSelect(VesselRecordDataColumns.LOA);
	}

	@Override
	public boolean getSelectGT() {
		return this.mustSelect(VesselRecordDataColumns.GT);
	}

	@Override
	public boolean getSelectType() {
		return this.mustSelect(VesselRecordDataColumns.VESSEL_TYPE);
	}

	@Override
	public boolean getSelectGearType() {
		return this.mustSelect(VesselRecordDataColumns.GEAR_TYPE);
	}

	@Override
	public boolean getSelectFlagID() {
		return this.mustSelect(VesselRecordDataColumns.FLAG_ID);
	}

	@Override
	public boolean getSelectRegistrationCountryID() {
		return this.mustSelect(VesselRecordDataColumns.REG_COUNTRY_ID);
	}

	@Override
	public boolean getSelectRegistrationPortID() {
		return this.mustSelect(VesselRecordDataColumns.REG_PORT_ID);
	}

	@Override
	public boolean getSelectTypeID() {
		return this.mustSelect(VesselRecordDataColumns.VESSEL_TYPE_ID);
	}

	@Override
	public boolean getSelectGearTypeID() {
		return this.mustSelect(VesselRecordDataColumns.GEAR_TYPE_ID);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.vessels.VesselRecordColumnFilter#getSelectParentTypeID()
	 */
	@Override
	public boolean getSelectParentTypeID() {
		return this.mustSelect(VesselRecordDataColumns.PARENT_VESSEL_TYPE_ID);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.vessels.VesselRecordColumnFilter#getSelectParentType()
	 */
	@Override
	public boolean getSelectParentType() {
		return this.mustSelect(VesselRecordDataColumns.PARENT_VESSEL_TYPE);
	}

	@Override
	public boolean getSelectVesselFlag() {
		return this.mustSelect(VesselRecordDataColumns.FLAG) || 
			   this.mustSelect(VesselRecordDataColumns.FLAG_ID);
	}

	@Override
	public boolean getSelectVesselCallsign() {
		return this.mustSelect(VesselRecordDataColumns.CALLSIGN) || 
			   this.mustSelect(VesselRecordDataColumns.CALLSIGN_COUNTRY) || 
			   this.mustSelect(VesselRecordDataColumns.CALLSIGN_COUNTRY_ID);
	}

	@Override
	public boolean getSelectCallsignCountry() {
		return this.mustSelect(VesselRecordDataColumns.CALLSIGN_COUNTRY);
	}

	@Override
	public boolean getSelectCallsignCountryID() {
		return this.mustSelect(VesselRecordDataColumns.CALLSIGN_COUNTRY_ID);
	}

	@Override
	public boolean getSelectVesselRegistration() {
		return this.mustSelect(VesselRecordDataColumns.REG_NO) || 
			   this.mustSelect(VesselRecordDataColumns.REG_PORT_ID) || 
			   this.mustSelect(VesselRecordDataColumns.REG_PORT) || 
			   this.mustSelect(VesselRecordDataColumns.REG_COUNTRY_ID) || 
			   this.mustSelect(VesselRecordDataColumns.REG_COUNTRY);
	}

	@Override
	public boolean getSelectVesselLength() {
		return this.mustSelect(VesselRecordDataColumns.LENGTH_TYPE) || this.mustSelect(VesselRecordDataColumns.LENGTH_VALUE);
	}

	@Override
	public boolean getSelectVesselTonnage() {
		return this.mustSelect(VesselRecordDataColumns.TONNAGE_TYPE) || this.mustSelect(VesselRecordDataColumns.TONNAGE_VALUE);
	}

	@Override
	public boolean getSelectVesselGearType() {
		return this.mustSelect(VesselRecordDataColumns.GEAR_TYPE) || this.mustSelect(VesselRecordDataColumns.GEAR_TYPE_ID);
	}

	@Override
	public boolean getSelectVesselType() {
		return this.mustSelect(VesselRecordDataColumns.VESSEL_TYPE) || this.mustSelect(VesselRecordDataColumns.VESSEL_TYPE_ID);
	}

	@Override
	public boolean getSelectLengthType() {
		return this.mustSelect(VesselRecordDataColumns.LENGTH_TYPE);
	}

	@Override
	public boolean getSelectLengthValue() {
		return this.mustSelect(VesselRecordDataColumns.LENGTH_VALUE);
	}

	@Override
	public boolean getSelectTonnageType() {
		return this.mustSelect(VesselRecordDataColumns.TONNAGE_VALUE);
	}

	@Override
	public boolean getSelectTonnageValue() {
		return this.mustSelect(VesselRecordDataColumns.TONNAGE_TYPE);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.vessels.VesselRecordColumnFilter#getSelectVesselParentType()
	 */
	@Override
	public boolean getSelectVesselParentType() {
		return this.mustSelect(VesselRecordDataColumns.PARENT_VESSEL_TYPE) || this.mustSelect(VesselRecordDataColumns.PARENT_VESSEL_TYPE_ID);
	}
}