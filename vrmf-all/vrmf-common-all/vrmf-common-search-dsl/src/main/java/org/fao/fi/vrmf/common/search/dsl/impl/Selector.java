/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.utility.model.extensions.collections.ListSet;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Feb 2013
 */
@XmlRootElement(name="select")
@XmlAccessorType(XmlAccessType.FIELD)
abstract public class Selector extends ColumnFilter {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5841569165934318231L;
	
	@XmlElementWrapper(name="columns")
	@XmlElement(name="column")
	protected Collection<Column> _selectedColumns;
	
	public Collection<Column> getSelectedColumns() {
		return this._selectedColumns;
	}
	
	public Selector() {
		this._selectedColumns = new ListSet<>();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.search.dsl.Filter#reset()
	 */
	
	public void reset() {
		this._selectedColumns.clear();
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Selector#select(org.fao.vrmf.common.services.request.search.dsl.interfaces.Data)
	 */
	
	@SuppressWarnings("unchecked")
	public <S extends Selector> S include(Column column) {
		this._selectedColumns.add(column);
		
		return (S)this;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Selector#includeIfTrue(boolean, org.fao.vrmf.common.services.request.search.dsl.interfaces.Data)
	 */
	
	@SuppressWarnings("unchecked")
	public <S extends Selector> S includeIfTrue(boolean condition, Column column) {
		if(condition)
			return this.include(column);
		
		return (S)this;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Selector#includeIfNotTrue(boolean, org.fao.vrmf.common.services.request.search.dsl.interfaces.Data)
	 */
	
	public <S extends Selector> S includeIfFalse(boolean condition, Column column) {
		return this.includeIfTrue(!condition, column);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Selector#include(C[])
	 */
	
	@SuppressWarnings("unchecked")
	public <S extends Selector> S include(Column... columns) {
		for(Column column : columns)
			this.include(column);
		
		return (S)this;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Selector#includeIfTrue(boolean, C[])
	 */
	
	@SuppressWarnings("unchecked")
	public <S extends Selector> S includeIfTrue(boolean condition, Column... columns) {
		if(condition) {
			this.include(columns);
		}
		
		return (S)this;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Selector#includeIfFalse(boolean, C[])
	 */
	
	public <S extends Selector> S includeIfFalse(boolean condition, Column... columns) {
		return this.includeIfTrue(!condition, columns);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.ColumnFilter#mustSelect(org.fao.fi.vrmf.common.core.search.dsl.impl.Column)
	 */
	@Override
	protected boolean mustSelect(Column column) {
		if(column == null || this._selectedColumns == null || this._selectedColumns.isEmpty()) return false; 
		
		for(Column in : this._selectedColumns)
			if(in.getColumn().equals(column.getColumn()))
				return true;
			
		return false;
	}
}