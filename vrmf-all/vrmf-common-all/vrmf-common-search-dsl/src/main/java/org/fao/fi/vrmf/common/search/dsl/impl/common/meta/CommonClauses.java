/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.common.meta;

import java.util.Date;

import org.fao.fi.vrmf.common.search.dsl.impl.Clause;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Feb 2013
 */
public class CommonClauses extends Clause {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3503174702284411427L;
	
	final static public CommonClauses VRMF_IDS = new CommonClauses("ids", Integer.class, true);
	final static public CommonClauses VRMF_UIDS = new CommonClauses("uids", Integer.class, true);
	final static public CommonClauses SOURCES = new CommonClauses("sources", String.class, true); 						//CURRENTLY UNUSED
	final static public CommonClauses AVAILABLE_SOURCES = new CommonClauses("availableSources", String.class, true);	//CURRENTLY UNUSED
	final static public CommonClauses MIN_SOURCES = new CommonClauses("minSources", Integer.class, false);				//CURRENTLY UNUSED
	final static public CommonClauses MAX_SOURCES = new CommonClauses("maxSources", Integer.class, false);				//CURRENTLY UNUSED
	final static public CommonClauses UPD_FROM = new CommonClauses("updatedFrom", Date.class, false);					//CURRENTLY UNUSED
	final static public CommonClauses UPD_TO = new CommonClauses("updatedTo", Date.class, false);						//CURRENTLY UNUSED
	
	public CommonClauses() {
		super();
	}
	
	protected CommonClauses(String mnemonic, Class<?> expectedType, boolean isMultiple) { 
		super(mnemonic, expectedType, isMultiple);
	}
}