/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized;

import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSelector;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.meta.AuthorizedVesselRecordDataColumns;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
@XmlRootElement
public class AuthorizedVesselRecordSelector extends VesselRecordSelector implements AuthorizedVesselRecordColumnFilter {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -945428495203715288L;

	@Override
	public boolean getSelectAuthorizationStart() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_START);
	}

	@Override
	public boolean getSelectAuthorizationEnd() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_END);
	}

	@Override
	public boolean getSelectAuthorizationType() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_TYPE);
	}

	@Override
	public boolean getSelectAuthorizationIssuingCountry() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_TYPE);
	}

	@Override
	public boolean getSelectAuthorizationIssuer() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_ISSUER);
	}

	@Override
	public boolean getSelectAuthorizationTerminationDate() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_TERMINATION_DATE);
	}

	@Override
	public boolean getSelectAuthorizationTerminationCode() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_TERMINATION_CODE);
	}
}
