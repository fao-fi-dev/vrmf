/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized;

import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordWhere;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
@XmlRootElement
public class AuthorizedVesselRecordWhere extends VesselRecordWhere {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3455707049619249378L;
}
