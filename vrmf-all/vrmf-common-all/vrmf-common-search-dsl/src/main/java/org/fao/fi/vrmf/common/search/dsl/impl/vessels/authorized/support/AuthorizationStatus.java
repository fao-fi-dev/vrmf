/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.support;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Feb 2013
 */
@XmlEnum
@XmlRootElement(name="authorizationStatus")
public enum AuthorizationStatus {
	ENFORCED("EN"),
	EXPIRED("EX"),
	TERMINATED("TE");
	
	private String _status;
	
	AuthorizationStatus(String status) {
		this._status = status;
	}
	
	public String getStatus() {
		return this._status;
	}
	
	static public AuthorizationStatus fromStatus(String status) {
		for(AuthorizationStatus current : AuthorizationStatus.values())
			if(current._status.equals(status))
				return current;
		
		return null;
	}
}
