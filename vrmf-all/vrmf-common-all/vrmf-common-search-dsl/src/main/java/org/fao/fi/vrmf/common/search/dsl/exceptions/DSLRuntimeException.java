/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Mar 2013
 */
public class DSLRuntimeException extends RuntimeException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6231538995119619991L;

	/**
	 * Class constructor
	 *
	 */
	public DSLRuntimeException() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public DSLRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public DSLRuntimeException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public DSLRuntimeException(Throwable cause) {
		super(cause);
	}
	
}
