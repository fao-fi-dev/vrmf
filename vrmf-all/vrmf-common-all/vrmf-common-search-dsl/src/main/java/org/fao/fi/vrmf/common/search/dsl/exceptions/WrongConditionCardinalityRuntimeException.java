/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.exceptions;

import java.util.Collection;

import org.fao.fi.vrmf.common.search.dsl.impl.Clause;
import org.fao.fi.vrmf.common.search.dsl.impl.Condition;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Mar 2013
 */
public class WrongConditionCardinalityRuntimeException extends DSLRuntimeException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2337560345134158866L;

	private Condition _condition;
	private Clause _clause;
	
	public WrongConditionCardinalityRuntimeException(Condition condition, Clause clause) {
		super("Wrong condition cardinality for " + condition + ": " +
			  "expected cardinality: " + ( clause.isMultiple() ? "multiple" : "single" ) + ", " +
			  "actual cardinality: " + getCardinality(condition) + ", " +
			  "actual value: " + condition.getValue());
		
		this._condition = condition;
		this._clause = clause;
	}
	
	@SuppressWarnings("unchecked")
	static private int getCardinality(Condition condition) {
		Object value = condition.getValue();
		
		if(value == null) return 0;
		if(value instanceof Collection) { return ((Collection<Object>)value).size(); } 
		
		return 1;
	}

	/**
	 * @return the 'condition' value
	 */
	public Condition getCondition() {
		return this._condition;
	}

	/**
	 * @return the 'clause' value
	 */
	public Clause getClause() {
		return this._clause;
	}
}
