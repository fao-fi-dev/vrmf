/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 3, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 3, 2015
 */
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.support.FieldFilter;
import org.fao.fi.vrmf.common.search.dsl.impl.Column;

@Named @Singleton final public class ColumnRegistry extends AbstractLoggingAwareClient {
	@Inject private List<Column> COLUMN_HOLDERS;
	
	protected Map<String, Column> COLUMNS_MAP = new HashMap<String, Column>();

	@PostConstruct void initializeColumnsMap() throws Exception {
		final FieldFilter filter = new FieldFilter() {
			@Override
			public boolean include(Field field) {
				return ((field.getModifiers() & Modifier.STATIC) == Modifier.STATIC) &&
						 Column.class.isAssignableFrom(field.getType());
			}
		};
		
		Column current, overwritten;
		
		List<Column> uniqueHolders = COLUMN_HOLDERS.stream().distinct().collect(Collectors.toList());
		
		for(Column holder : uniqueHolders) {
			for(Field in : ObjectsHelper.listFieldsForClass(holder.getClass(), ObjectsHelper.DONT_TRAVERSE_OBJECT_HIERARCHY, filter)) {
				_log.info("Analyzing field {} of type {} from class {}...", in.getName(), in.getType().getName(), holder.getClass().getName());
				
				current = $nN((Column)in.get(holder), "Column constant field is not initialized");
				
				if(current.getMnemonic() != null) {
					overwritten = COLUMNS_MAP.put(current.getMnemonic(), current);
					
					$n(overwritten, "Column {} from {} overwrites an existing column with the same mnemonic ({})", overwritten == null ? null : overwritten.getColumn(), holder.getClass().getName(), overwritten == null ? null : overwritten.getMnemonic());
				}
			}
		}
	}
	
	public Column byMnemonic(String mnemonic) {
		return COLUMNS_MAP.get(mnemonic);
	}
	
	public Collection<Column> availableColumns() {
		return COLUMNS_MAP.values();
	}
}
