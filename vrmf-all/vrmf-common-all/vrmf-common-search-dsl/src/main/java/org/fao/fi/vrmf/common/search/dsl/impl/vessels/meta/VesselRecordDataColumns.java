/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta;

import org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Feb 2013
 */
public class VesselRecordDataColumns extends CommonDataColumns {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4632701729602944464L;
	
	final static public VesselRecordDataColumns IMO = new VesselRecordDataColumns("l", "IMO");
	final static public VesselRecordDataColumns EU_CFR = new VesselRecordDataColumns("eu", "EU_CFR");
	
	final static public VesselRecordDataColumns FLAG = new VesselRecordDataColumns("f", "FLAG_COUNTRY_NAME");
	final static public VesselRecordDataColumns FLAG_ID	= new VesselRecordDataColumns("fid", "FLAG_COUNTRY_ID");
	
	final static public VesselRecordDataColumns NAME = new VesselRecordDataColumns("n", "NAME");
	
	final static public VesselRecordDataColumns CALLSIGN = new VesselRecordDataColumns("c", "CALLSIGN");
	final static public VesselRecordDataColumns CALLSIGN_COUNTRY = new VesselRecordDataColumns("cc", "CALLSIGN_COUNTRY_NAME");
	final static public VesselRecordDataColumns CALLSIGN_COUNTRY_ID = new VesselRecordDataColumns("ccid","CALLSIGN_COUNTRY_ID");
	
	final static public VesselRecordDataColumns REG_COUNTRY = new VesselRecordDataColumns("rc", "REG_PORT_COUNTRY");
	final static public VesselRecordDataColumns REG_COUNTRY_ID = new VesselRecordDataColumns("rcid","REG_PORT_COUNTRY_ID");
	final static public VesselRecordDataColumns REG_PORT = new VesselRecordDataColumns("rp", "REG_PORT_NAME");
	final static public VesselRecordDataColumns REG_PORT_ID = new VesselRecordDataColumns("rpid","REG_PORT_ID");
	final static public VesselRecordDataColumns REG_NO = new VesselRecordDataColumns("rn", "REG_NO");
	
	final static public VesselRecordDataColumns PARENT_VESSEL_TYPE = new VesselRecordDataColumns("pt", 	"PARENT_VESSEL_TYPE");
	final static public VesselRecordDataColumns PARENT_VESSEL_TYPE_ID = new VesselRecordDataColumns("ptid", "PARENT_VESSEL_TYPE_ID");
	
	final static public VesselRecordDataColumns VESSEL_TYPE = new VesselRecordDataColumns("t", "VESSEL_TYPE");
	final static public VesselRecordDataColumns VESSEL_TYPE_ID = new VesselRecordDataColumns("tid", "VESSEL_TYPE_ID");
	
	final static public VesselRecordDataColumns GEAR_TYPE = new VesselRecordDataColumns("g", "PRIMARY_GEAR_TYPE");
	final static public VesselRecordDataColumns GEAR_TYPE_ID = new VesselRecordDataColumns("gid", "PRIMARY_GEAR_TYPE_ID");
	
	final static public VesselRecordDataColumns LOA = new VesselRecordDataColumns("loa", "LOA");
	final static public VesselRecordDataColumns GT = new VesselRecordDataColumns("gt", "GT");
	
	final static public VesselRecordDataColumns LENGTH_TYPE = new VesselRecordDataColumns("lt", "LENGTH_TYPE");
	final static public VesselRecordDataColumns LENGTH_VALUE = new VesselRecordDataColumns("lv", "LENGTH");
	
	final static public VesselRecordDataColumns TONNAGE_TYPE = new VesselRecordDataColumns("tt", "TONNAGE_TYPE");
	final static public VesselRecordDataColumns TONNAGE_VALUE = new VesselRecordDataColumns("tv", "TONNAGE");

	public VesselRecordDataColumns() {
		super();
	}
	
	protected VesselRecordDataColumns(String mnemonic, String column) {
		super(mnemonic, column);
	}
}
