/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels;

import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.search.dsl.impl.common.CommonWhere;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
@XmlRootElement
public class VesselRecordWhere extends CommonWhere {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3455707049619249378L;

	public boolean getFilterIdentifiers() {
		return this.getFilterBy(VesselRecordClauses.IDENTIFIERS, 
								VesselRecordClauses.IDENTIFIER_TYPE);
	}
	
	public boolean getFilterBuildingYear() {
		return this.getFilterBy(VesselRecordClauses.BUILDING_YEAR_MIN, 
								VesselRecordClauses.BUILDING_YEAR_MAX);
	}
	
	public boolean getFilterHullMaterial() {
		return this.getFilterBy(VesselRecordClauses.HULL_MATERIAL);
	}
	
	public boolean getFilterFlag() {
		return this.getFilterBy(VesselRecordClauses.FLAG, 
								VesselRecordClauses.NO_FLAG);
	}
	
	public boolean getFilterName() {
		return this.getFilterBy(VesselRecordClauses.NAMES);
	}
	
	public boolean getFilterType() {
		return this.getFilterBy(VesselRecordClauses.TYPE, 
								VesselRecordClauses.TYPE_CATEGORY,
								VesselRecordClauses.NO_TYPE);
	}
	
	public boolean getFilterParentType() {
		return this.getFilterBy(VesselRecordClauses.PARENT_TYPE, 
								VesselRecordClauses.NO_PARENT_TYPE);
	}
	
	public boolean getFilterPrimaryGear() {
		return this.getFilterBy(VesselRecordClauses.PRIMARY_GEAR_TYPE, 
								VesselRecordClauses.PRIMARY_GEAR_TYPE_CATEGORY);
	}
	
	public boolean getFilterSecondaryGear() {
		return this.getFilterBy(VesselRecordClauses.SECONDARY_GEAR_TYPE, 
								VesselRecordClauses.SECONDARY_GEAR_TYPE_CATEGORY);
	}
	
	public boolean getFilterGears() {
		return this.getFilterPrimaryGear() || 
			   this.getFilterSecondaryGear();
	}
	
	public boolean getFilterRegistrations() {
		return this.getFilterBy(VesselRecordClauses.REGISTRATION_COUNTRIES, 
								VesselRecordClauses.REGISTRATION_PORTS,
								VesselRecordClauses.REGISTRATION_NUMBERS, 
								VesselRecordClauses.REGISTRATION_COUNTRY_VALID_FROM,
								VesselRecordClauses.REGISTRATION_COUNTRY_VALID_TO);
	}
	
	public boolean getFilterFishingLicenses() {
		return this.getFilterBy(VesselRecordClauses.FISHING_LICENSE_ISSUING_COUNTRIES, 
								VesselRecordClauses.FISHING_LICENSE_NUMBERS);
	}
	
	public boolean getFilterVms() {
		return this.getFilterBy(VesselRecordClauses.HAS_VMS_INDICATOR);
	}
		
	public boolean getFilterLength() {
		return this.getFilterBy(VesselRecordClauses.LENGTH_TYPES, 
								VesselRecordClauses.LENGTH_RANGES,
								VesselRecordClauses.NO_LENGTH);
	}
	
	public boolean getFilterTonnage() {
		return this.getFilterBy(VesselRecordClauses.TONNAGE_TYPES, 
								VesselRecordClauses.TONNAGE_RANGES,
								VesselRecordClauses.NO_TONNAGE);
	}
	
	public boolean getFilterMainEngine() {
		return this.getFilterBy(VesselRecordClauses.MAIN_ENGINE_POWER_TYPES, 
								VesselRecordClauses.MAIN_ENGINE_POWER_RANGES);
	}
	
	public boolean getFilterAuxiliaryEngine() {
		return this.getFilterBy(VesselRecordClauses.AUX_ENGINE_POWER_TYPES, 
								VesselRecordClauses.AUX_ENGINE_POWER_RANGES);
	}

	public boolean getFilterNonCompliance() {
		return this.getFilterBy(VesselRecordClauses.NON_COMPLIANCE_DATE_RANGE_FROM, 
								VesselRecordClauses.NON_COMPLIANCE_DATE_RANGE_TO,
								VesselRecordClauses.NON_COMPLIANCE_INFRINGEMENT_TYPES,
								VesselRecordClauses.NON_COMPLIANCE_ISSUERS,
								VesselRecordClauses.NON_COMPLIANCE_OUTCOME_TYPES,
								VesselRecordClauses.NON_COMPLIANCE_SOURCE_TYPES);
	}
	
	public boolean getFilterInspections() {
		boolean mustHaveInspections = this.getFilterBy(VesselRecordClauses.HAS_INSPECTIONS) && Boolean.TRUE.equals(_conditions.get(VesselRecordClauses.HAS_INSPECTIONS).getValue());
		
		return mustHaveInspections ||
				this.getFilterBy(VesselRecordClauses.INSPECTIONS_DATE_RANGE_FROM,
								 VesselRecordClauses.INSPECTIONS_DATE_RANGE_TO,
								 VesselRecordClauses.INSPECTIONS_INFRINGEMENT_TYPES,
								 VesselRecordClauses.INSPECTIONS_ORIGINATING_COUNTRIES);
	}
	
	public boolean getFilterPortEntryDenials() {
		boolean mustHaveDenials = this.getFilterBy(VesselRecordClauses.HAS_PORT_ENTRY_DENIALS) && Boolean.TRUE.equals(_conditions.get(VesselRecordClauses.HAS_PORT_ENTRY_DENIALS).getValue());

		return mustHaveDenials ||
				this.getFilterBy(VesselRecordClauses.PORT_ENTRY_DENIALS_DATE_RANGE_FROM,
								 VesselRecordClauses.PORT_ENTRY_DENIALS_DATE_RANGE_TO,
								 VesselRecordClauses.PORT_ENTRY_DENIALS_ORIGINATING_COUNTRIES,
								 VesselRecordClauses.PORT_ENTRY_DENIALS_ORIGINATING_PORTS,
								 VesselRecordClauses.PORT_ENTRY_DENIALS_REASONS);
	}
	
	public boolean getFilterIUULists() {
		boolean mustHaveIUULists = this.getFilterBy(VesselRecordClauses.HAS_IUU_LISTS) && Boolean.TRUE.equals(_conditions.get(VesselRecordClauses.HAS_IUU_LISTS).getValue());
		
		return mustHaveIUULists ||
				this.getFilterBy(VesselRecordClauses.IUU_LISTS_DATE_RANGE_FROM,
								 VesselRecordClauses.IUU_LISTS_DATE_RANGE_TO,
								 VesselRecordClauses.IUU_LISTS_ORIGINATING_LISTS);
	}
	
	public boolean getFilterAuthorizations() {
		return this.getFilterBy(VesselRecordClauses.AUTHORIZATION_HOLDER,
								VesselRecordClauses.AUTHORIZATION_END_FROM, 
								VesselRecordClauses.AUTHORIZATION_END_TO,
								VesselRecordClauses.AUTHORIZATION_ISSUERS,
								VesselRecordClauses.AUTHORIZATION_ISSUING_COUNTRIES,
								VesselRecordClauses.AUTHORIZATION_START_FROM,
								VesselRecordClauses.AUTHORIZATION_START_TO,
								VesselRecordClauses.AUTHORIZATION_STATUS,
								VesselRecordClauses.AUTHORIZATION_TERMINATION_FROM,
								VesselRecordClauses.AUTHORIZATION_TERMINATION_TO,
								VesselRecordClauses.AUTHORIZATION_TYPES);
	}
	
	public boolean getFilterOwners() {
		return this.getFilterBy(VesselRecordClauses.OWNERS);
	}
	
	public boolean getFilterOperators() {
		return this.getFilterBy(VesselRecordClauses.OPERATORS);
	}
}
