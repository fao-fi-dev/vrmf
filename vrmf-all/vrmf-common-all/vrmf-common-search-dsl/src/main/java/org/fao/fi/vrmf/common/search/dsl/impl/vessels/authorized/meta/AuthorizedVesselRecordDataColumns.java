/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.meta;

import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Feb 2013
 */
public class AuthorizedVesselRecordDataColumns extends VesselRecordDataColumns {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2781690697697956549L;
	
	final static public AuthorizedVesselRecordDataColumns AUTHORIZATION_START = new AuthorizedVesselRecordDataColumns("as", "AUTHORIZATION_START");
	final static public AuthorizedVesselRecordDataColumns AUTHORIZATION_END	 = new AuthorizedVesselRecordDataColumns("ae", "AUTHORIZATION_END");
	final static public AuthorizedVesselRecordDataColumns AUTHORIZATION_TYPE = new AuthorizedVesselRecordDataColumns("at", "AUTHORIZATION_TYPE");
	final static public AuthorizedVesselRecordDataColumns AUTHORIZATION_ISSUER = new AuthorizedVesselRecordDataColumns("ai", "AUTHORIZATION_ISSUER");
	final static public AuthorizedVesselRecordDataColumns AUTHORIZATION_ISSUING_COUNTRY = new AuthorizedVesselRecordDataColumns("aic", "AUTHORIZATION_ISSUING_COUNTRY");
	final static public AuthorizedVesselRecordDataColumns AUTHORIZATION_TERMINATION_DATE = new AuthorizedVesselRecordDataColumns("atd", "AUTHORIZATION_TERMINATION_DATE");
	final static public AuthorizedVesselRecordDataColumns AUTHORIZATION_TERMINATION_CODE = new AuthorizedVesselRecordDataColumns("atc", "AUTHORIZATION_TERMINATION_CODE");
	
	public AuthorizedVesselRecordDataColumns() {
		super();
	}
	
	protected AuthorizedVesselRecordDataColumns(String mnemonic, String column) {
		super(mnemonic, column);
	}
}
