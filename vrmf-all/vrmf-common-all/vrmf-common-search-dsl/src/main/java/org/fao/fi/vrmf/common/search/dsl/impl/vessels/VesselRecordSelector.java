/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.search.dsl.impl.common.CommonSelector;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns;

@XmlRootElement
public class VesselRecordSelector extends CommonSelector implements VesselRecordColumnFilter {
	private static final long serialVersionUID = -945428495203715288L;

	private @XmlAttribute boolean includeAuthorizations = false;
	private @XmlAttribute boolean includePortDenials = false;
	private @XmlAttribute boolean includeIUUListings = false;
	private @XmlAttribute boolean includeInspections = false;
	
	final public VesselRecordSelector withAuthorizations() {
		includeAuthorizations = true;
		return this;
	}
	
	final public VesselRecordSelector withoutAuthorizations() {
		includeAuthorizations = false;
		return this;
	}
	
	final public VesselRecordSelector withPortDenials() {
		includePortDenials = true;
		return this;
	}
	
	final public VesselRecordSelector withoutPortDenials() {
		includePortDenials = false;
		return this;
	}
	
	final public VesselRecordSelector withIUUListings() {
		includeIUUListings = true;
		return this;
	}
	
	final public VesselRecordSelector withoutIUUListings() {
		includeIUUListings = false;
		return this;
	}
	
	final public VesselRecordSelector withInspections() {
		includeInspections = true;
		return this;
	}
	
	final public VesselRecordSelector withoutInspections() {
		includeInspections = false;
		return this;
	}
	
	/**
	 * @return the 'includeAuthorizations' value
	 */
	final public boolean getIncludeAuthorizations() {
		return includeAuthorizations;
	}

	/**
	 * @param includeAuthorizations the 'includeAuthorizations' value to set
	 */
	final public void setIncludeAuthorizations(boolean includeAuthorizations) {
		this.includeAuthorizations = includeAuthorizations;
	}

	/**
	 * @return the 'includePortDenials' value
	 */
	final public boolean getIncludePortDenials() {
		return includePortDenials;
	}

	/**
	 * @param includePortDenials the 'includePortDenials' value to set
	 */
	final public void setIncludePortDenials(boolean includePortDenials) {
		this.includePortDenials = includePortDenials;
	}

	/**
	 * @return the 'includeIUUListings' value
	 */
	final public boolean getIncludeIUUListings() {
		return includeIUUListings;
	}

	/**
	 * @param includeIUUListings the 'includeIUUListings' value to set
	 */
	final public void setIncludeIUUListings(boolean includeIUUListings) {
		this.includeIUUListings = includeIUUListings;
	}

	/**
	 * @return the 'includeInspections' value
	 */
	final public boolean getIncludeInspections() {
		return includeInspections;
	}

	/**
	 * @param includeInspections the 'includeInspections' value to set
	 */
	final public void setIncludeInspections(boolean includeInspections) {
		this.includeInspections = includeInspections;
	}

	@Override
	final public boolean getSelectIMO() {
		return this._selectedColumns.contains(VesselRecordDataColumns.IMO);
	}

	@Override
	final public boolean getSelectEUCFR() {
		return this._selectedColumns.contains(VesselRecordDataColumns.EU_CFR);
	}

	@Override
	final public boolean getSelectName() {
		return this._selectedColumns.contains(VesselRecordDataColumns.NAME);
	}

	@Override
	final public boolean getSelectFlag() {
		return this._selectedColumns.contains(VesselRecordDataColumns.FLAG);
	}

	@Override
	final public boolean getSelectCallsign() {
		return this._selectedColumns.contains(VesselRecordDataColumns.CALLSIGN);
	}

	@Override
	final public boolean getSelectRegistrationNumber() {
		return this._selectedColumns.contains(VesselRecordDataColumns.REG_NO);
	}

	@Override
	final public boolean getSelectRegistrationCountry() {
		return this._selectedColumns.contains(VesselRecordDataColumns.REG_COUNTRY);
	}

	@Override
	final public boolean getSelectRegistrationPort() {
		return this._selectedColumns.contains(VesselRecordDataColumns.REG_PORT);
	}

	@Override
	final public boolean getSelectLOA() {
		return this._selectedColumns.contains(VesselRecordDataColumns.LOA);
	}

	@Override
	final public boolean getSelectLengthType() {
		return this._selectedColumns.contains(VesselRecordDataColumns.LENGTH_TYPE);
	}

	@Override
	final public boolean getSelectLengthValue() {
		return this._selectedColumns.contains(VesselRecordDataColumns.LENGTH_VALUE);
	}

	@Override
	final public boolean getSelectGT() {
		return this._selectedColumns.contains(VesselRecordDataColumns.GT);
	}

	@Override
	final public boolean getSelectTonnageType() {
		return this._selectedColumns.contains(VesselRecordDataColumns.TONNAGE_TYPE);
	}

	@Override
	final public boolean getSelectTonnageValue() {
		return this._selectedColumns.contains(VesselRecordDataColumns.TONNAGE_VALUE);
	}

	@Override
	final public boolean getSelectType() {
		return this._selectedColumns.contains(VesselRecordDataColumns.VESSEL_TYPE);
	}

	@Override
	final public boolean getSelectParentType() {
		return this._selectedColumns.contains(VesselRecordDataColumns.PARENT_VESSEL_TYPE);
	}

	@Override
	final public boolean getSelectParentTypeID() {
		return this._selectedColumns.contains(VesselRecordDataColumns.PARENT_VESSEL_TYPE_ID);
	}
	
	@Override
	final public boolean getSelectGearType() {
		return this._selectedColumns.contains(VesselRecordDataColumns.GEAR_TYPE);
	}

	@Override
	final public boolean getSelectFlagID() {
		return this._selectedColumns.contains(VesselRecordDataColumns.FLAG_ID);
	}

	@Override
	final public boolean getSelectRegistrationCountryID() {
		return this._selectedColumns.contains(VesselRecordDataColumns.REG_COUNTRY_ID);
	}

	@Override
	final public boolean getSelectRegistrationPortID() {
		return this._selectedColumns.contains(VesselRecordDataColumns.REG_PORT_ID);
	}

	@Override
	final public boolean getSelectTypeID() {
		return this._selectedColumns.contains(VesselRecordDataColumns.VESSEL_TYPE_ID);
	}

	@Override
	final public boolean getSelectGearTypeID() {
		return this._selectedColumns.contains(VesselRecordDataColumns.GEAR_TYPE_ID);
	}

	@Override
	final public boolean getSelectCallsignCountry() {
		return this._selectedColumns.contains(VesselRecordDataColumns.CALLSIGN_COUNTRY);
	}

	@Override
	final public boolean getSelectCallsignCountryID() {
		return this._selectedColumns.contains(VesselRecordDataColumns.CALLSIGN_COUNTRY_ID);
	}

	@Override
	final public boolean getSelectVesselFlag() {
		return this.getSelectFlag() || this.getSelectFlagID();
	}

	@Override
	final public boolean getSelectVesselCallsign() {
		return this.getSelectCallsign() || this.getSelectCallsignCountryID() || this.getSelectCallsignCountry();
	}

	@Override
	final public boolean getSelectVesselRegistration() {
		return this.getSelectRegistrationNumber() || 
			   this.getSelectRegistrationPortID() || 
			   this.getSelectRegistrationPort() || 
			   this.getSelectRegistrationCountryID() || 
			   this.getSelectRegistrationCountry();
	}

	@Override
	final public boolean getSelectVesselLength() {
		return this.getSelectLengthType() || this.getSelectLengthValue();
	}

	@Override
	final public boolean getSelectVesselTonnage() {
		return this.getSelectTonnageType() || this.getSelectTonnageValue();
	}

	@Override
	final public boolean getSelectVesselGearType() {
		return this.getSelectGearType() || this.getSelectGearTypeID();
	}

	@Override
	final public boolean getSelectVesselType() {
		return this.getSelectType() || this.getSelectTypeID();
	}
	
	@Override
	public boolean getSelectVesselParentType() {
		return this.getSelectParentType() || 
			   this.getSelectParentTypeID();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( includeAuthorizations ? 1231 : 1237 );
		result = prime * result + ( includeIUUListings ? 1231 : 1237 );
		result = prime * result + ( includePortDenials ? 1231 : 1237 );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		VesselRecordSelector other = (VesselRecordSelector) obj;
		if(includeAuthorizations != other.includeAuthorizations) return false;
		if(includeIUUListings != other.includeIUUListings) return false;
		if(includePortDenials != other.includePortDenials) return false;
		return true;
	}
}
