/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.fao.fi.vrmf.common.core.behavior.vessels.AuthorizationAware;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Feb 2013
 */
@XmlRootElement(name="authorizedVesselRecord")
public class AuthorizedVesselRecord extends VesselRecord implements AuthorizationAware {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2573937547436330871L;

	@XmlTransient
	private Long _maxTimeDelta;
	
	@XmlElement(name="authorizationType")
	private String _authorizationTypeID;
	
	@XmlElement(name="authorizationIssuer")
	private String _authorizationIssuer;
	
	@XmlElement(name="authorizationIssuingCountry")
	private String _authorizationIssuingCountry;
	
	@XmlElement(name="authorizationIssuingCountryCode")
	private Integer _authorizationIssuingCountryID;
	
	@XmlElement(name="authorizationStartDate")
	private Date _authorizationStartDate;
	
	@XmlElement(name="authorizationEndDate")
	private Date _authorizationEndDate;
	
	@XmlElement(name="authorizationTerminationDate")
	private Date _authorizationTerminationDate;

	@XmlElement(name="authorizationTerminationReasonCode")
	private Integer _authorizationTerminationReasonCode;
	
	@XmlElement(name="authorizationTerminationReason")
	private String _authorizationTerminationReason;
	
	@XmlElement(name="authorizationReferenceDate")
	private Date _authorizationReferenceDate;

	/**
	 * @return the 'maxTimeDelta' value
	 */
	public Long getMaxTimeDelta() {
		return this._maxTimeDelta;
	}

	/**
	 * @param maxTimeDelta the 'maxTimeDelta' value to set
	 */
	public void setMaxTimeDelta(Long maxTimeDelta) {
		this._maxTimeDelta = maxTimeDelta;
	}

	/**
	 * @return the 'authorizationTypeId' value
	 */
	public String getAuthorizationTypeID() {
		return this._authorizationTypeID;
	}

	/**
	 * @param authorizationTypeId the 'authorizationTypeId' value to set
	 */
	public void setAuthorizationTypeID(String authorizationTypeId) {
		this._authorizationTypeID = authorizationTypeId;
	}

	/**
	 * @return the 'authorizationIssuer' value
	 */
	public String getAuthorizationIssuer() {
		return this._authorizationIssuer;
	}

	/**
	 * @param authorizationIssuer the 'authorizationIssuer' value to set
	 */
	public void setAuthorizationIssuer(String authorizationIssuer) {
		this._authorizationIssuer = authorizationIssuer;
	}

	/**
	 * @return the 'authorizationIssuingCountry' value
	 */
	public Integer getAuthorizationIssuingCountryID() {
		return this._authorizationIssuingCountryID;
	}

	/**
	 * @param authorizationIssuingCountry the 'authorizationIssuingCountry' value to set
	 */
	public void setAuthorizationIssuingCountryID(Integer authorizationIssuingCountry) {
		this._authorizationIssuingCountryID = authorizationIssuingCountry;
	}

	/**
	 * @return the 'authorizationStartDate' value
	 */
	public Date getAuthorizationStartDate() {
		return this._authorizationStartDate;
	}

	/**
	 * @param authorizationStartDate the 'authorizationStartDate' value to set
	 */
	public void setAuthorizationStartDate(Date authorizationStartDate) {
		this._authorizationStartDate = authorizationStartDate;
	}

	/**
	 * @return the 'authorizationEndDate' value
	 */
	public Date getAuthorizationEndDate() {
		return this._authorizationEndDate;
	}

	/**
	 * @param authorizationEndDate the 'authorizationEndDate' value to set
	 */
	public void setAuthorizationEndDate(Date authorizationEndDate) {
		this._authorizationEndDate = authorizationEndDate;
	}

	/**
	 * @return the 'authorizationTerminationDate' value
	 */
	public Date getAuthorizationTerminationDate() {
		return this._authorizationTerminationDate;
	}

	/**
	 * @param authorizationTerminationDate the 'authorizationTerminationDate' value to set
	 */
	public void setAuthorizationTerminationDate(Date authorizationTerminationDate) {
		this._authorizationTerminationDate = authorizationTerminationDate;
	}

	/**
	 * @return the 'authorizationTerminationReasonCode' value
	 */
	public Integer getAuthorizationTerminationReasonCode() {
		return this._authorizationTerminationReasonCode;
	}

	/**
	 * @param authorizationTerminationReasonCode the 'authorizationTerminationReasonCode' value to set
	 */
	public void setAuthorizationTerminationReasonCode(Integer authorizationTerminationReasonCode) {
		this._authorizationTerminationReasonCode = authorizationTerminationReasonCode;
	}

	/**
	 * @return the 'authorizationTerminationReason' value
	 */
	public String getAuthorizationTerminationReason() {
		return this._authorizationTerminationReason;
	}

	/**
	 * @param authorizationTerminationReason the 'authorizationTerminationReason' value to set
	 */
	public void setAuthorizationTerminationReason(String authorizationTerminationReason) {
		this._authorizationTerminationReason = authorizationTerminationReason;
	}

	/**
	 * @return the 'authorizationReferenceDate' value
	 */
	public Date getAuthorizationReferenceDate() {
		return this._authorizationReferenceDate;
	}

	/**
	 * @param authorizationReferenceDate the 'authorizationReferenceDate' value to set
	 */
	public void setAuthorizationReferenceDate(Date authorizationReferenceDate) {
		this._authorizationReferenceDate = authorizationReferenceDate;
	}

	/**
	 * @return the 'authorizationIssuingCountry' value
	 */
	public String getAuthorizationIssuingCountry() {
		return this._authorizationIssuingCountry;
	}

	/**
	 * @param authorizationIssuingCountry the 'authorizationIssuingCountry' value to set
	 */
	public void setAuthorizationIssuingCountry(String authorizationIssuingCountry) {
		this._authorizationIssuingCountry = authorizationIssuingCountry;
	}
}