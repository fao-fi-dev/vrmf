/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Feb 2013
 */
@XmlRootElement(name="sortingCriterion")
@XmlAccessorType(XmlAccessType.FIELD)
public class SortCriteria extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7022912644995331763L;
	
	@XmlElement(name="column")
	private Column _column;
	
	@XmlAttribute(name="ascending")
	private boolean _ascending;
	
	/**
	 * Class constructor.
	 */
	public SortCriteria() {
	}
	
	/**
	 * Instantiates a new sort criteria.
	 *
	 * @param column the column
	 * @param ascending the ascending
	 */
	public SortCriteria(Column column, boolean ascending) {
		this._column = column;
		this._ascending = ascending;
	}
	
	/**
	 * Gets the column.
	 *
	 * @return the column
	 */
	public Column getColumn() {
		return this._column;
	}

	/**
	 * Ascending.
	 *
	 * @return true, if successful
	 */
	public boolean ascending() {
		return this._ascending;
	}

	/**
	 * Descending.
	 *
	 * @return true, if successful
	 */
	public boolean descending() {
		return !this._ascending;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this._column.getColumn() + " " + ( this._ascending ? "ASC" : "DESC" );
	}
	
	public String asString() {
		return new StringBuilder().
						append(this._column.getMnemonic()).append("|").
						append(this._ascending).
					toString();
	}
}