/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
public interface AuthorizedVesselRecordColumnFilter {
	boolean getSelectAuthorizationStart();
	boolean getSelectAuthorizationEnd();
	boolean getSelectAuthorizationType();
	boolean getSelectAuthorizationIssuer();
	boolean getSelectAuthorizationIssuingCountry();
	boolean getSelectAuthorizationTerminationDate();
	boolean getSelectAuthorizationTerminationCode();
}
