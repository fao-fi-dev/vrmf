/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.core.behavior.other.TimeframeDeltaAware;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Feb 2013
 */
@XmlRootElement(name="authorizedTimeframedVesselRecord")
public class AuthorizedTimeframedVesselRecord extends AuthorizedVesselRecord implements TimeframeDeltaAware {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8806695457531661015L;
	
	@XmlElement(name="maxTimeDelta")
	private Long _maxTimeDelta;
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.models.behaviours.TimeframedData#setMaxTimeDelta(long)
	 */
	@Override
	public void setMaxTimeDelta(Long maxTimeDelta) {
		this._maxTimeDelta = maxTimeDelta;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.models.behaviours.TimeframedData#getMaxTimeDelta()
	 */
	@Override
	public Long getMaxTimeDelta() {
		return this._maxTimeDelta;
	}
}