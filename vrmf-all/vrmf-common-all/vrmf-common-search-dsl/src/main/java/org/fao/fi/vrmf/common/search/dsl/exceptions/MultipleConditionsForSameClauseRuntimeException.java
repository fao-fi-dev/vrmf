/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.exceptions;

import org.fao.fi.vrmf.common.search.dsl.impl.Clause;
import org.fao.fi.vrmf.common.search.dsl.impl.Condition;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Mar 2013
 */
public class MultipleConditionsForSameClauseRuntimeException extends DSLRuntimeException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2337560345134158866L;

	private Clause _clause;
	private Condition _existingCondition;
	private Condition _currentCondition;
	
	public MultipleConditionsForSameClauseRuntimeException(Clause clause, Condition existingCondition, Condition currentCondition) {
		super("Multiple conditions for clause " + clause + " ( existing condition: " + existingCondition + ", current condition: " + currentCondition + " )");
		
		this._clause = clause;
		this._existingCondition = existingCondition;
		this._currentCondition = currentCondition;
	}

	/**
	 * @return the 'clause' value
	 */
	public Clause getClause() {
		return this._clause;
	}

	/**
	 * @return the 'existingCondition' value
	 */
	public Condition getExistingCondition() {
		return this._existingCondition;
	}

	/**
	 * @return the 'currentCondition' value
	 */
	public Condition getCurrentCondition() {
		return this._currentCondition;
	}
}