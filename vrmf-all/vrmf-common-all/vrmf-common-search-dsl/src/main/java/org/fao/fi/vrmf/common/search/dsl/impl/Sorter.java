/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.search.dsl.impl.common.CommonColumnFilter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Feb 2013
 */
@XmlRootElement(name="sort")
@XmlAccessorType(XmlAccessType.FIELD)
abstract public class Sorter extends ColumnFilter implements CommonColumnFilter {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3699895886008393173L;
	
	/**
	 * The _sorting criteria.
	 */
	@XmlElementWrapper(name="criteria")
	@XmlElement(name="criterion")
	protected Collection<SortCriteria> _sortingCriteria;
	
	/**
	 * Instantiates a new sorter.
	 */
	public Sorter() {
		this._sortingCriteria = this.initializeCriteria(); 
	}
	
	/**
	 * Reset.
	 */
	public void reset() {
		this._sortingCriteria.clear();
	}
	
	/**
	 * Initialize criteria.
	 *
	 * @return the collection
	 */
	protected Collection<SortCriteria> initializeCriteria() {
		return new ArrayList<SortCriteria>();
	}
	
	/**
	 * Gets the sorting criteria.
	 *
	 * @return the sorting criteria
	 */
	public Collection<SortCriteria> getSortingCriteria() {
		return this._sortingCriteria;
	}
	
	/**
	 * Sort.
	 *
	 * @param <S> the generic type
	 * @param column the column
	 * @param ascending the ascending
	 * @return the s
	 */
	@SuppressWarnings("unchecked")
	public <S extends Sorter> S sort(Column column, boolean ascending) {
		this._sortingCriteria.add(new SortCriteria(column, ascending));
		
		return (S)this;
	}

	/**
	 * Ascending.
	 *
	 * @param <S> the generic type
	 * @param column the column
	 * @return the s
	 */
	@SuppressWarnings("unchecked")
	public <S extends Sorter> S ascending(Column column) {
		this._sortingCriteria.add(new SortCriteria(column, true));
		
		return (S)this;
	}

	/**
	 * Descending.
	 *
	 * @param <S> the generic type
	 * @param column the column
	 * @return the s
	 */
	@SuppressWarnings("unchecked")
	public <S extends Sorter> S descending(Column column) {
		this._sortingCriteria.add(new SortCriteria(column, false));
		
		return (S)this;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.ColumnFilter#mustSelect(org.fao.fi.vrmf.common.core.search.dsl.impl.Column)
	 */
	protected boolean mustSelect(Column column) {
		if(column == null || this._sortingCriteria == null || this._sortingCriteria.isEmpty()) return false; 
		
		for(SortCriteria criterion : this._sortingCriteria) {
			if(column.equals(criterion.getColumn()))
				return true;
		}
		
		return false;
	}
	
	public String asString() {
		StringBuilder asString = new StringBuilder();
		
		if(_sortingCriteria != null) {
			for(SortCriteria in : _sortingCriteria) {
				asString.append(in.asString()).append("|");
			}
		}
		
		return asString.toString();
	}
}