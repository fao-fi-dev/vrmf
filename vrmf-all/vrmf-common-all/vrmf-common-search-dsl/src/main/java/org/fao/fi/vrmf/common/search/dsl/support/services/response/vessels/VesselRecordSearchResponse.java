/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.support.services.response.vessels;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableArrayList;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.fao.fi.vrmf.common.services.search.response.PaginatedSearchResponse;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Dec 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Dec 2010
 */
@XmlRootElement(name="vesselsSearchResults")
final public class VesselRecordSearchResponse<RECORD extends VesselRecord> extends PaginatedSearchResponse<RECORD> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7888146661459982865L;
			
	/**
	 * Class constructor
	 *
	 */
	public VesselRecordSearchResponse() {
		this(0, 0, 0, new ArrayList<RECORD>());
	}
	
	 /**
	  * Class constructor
	 *
	 * @param offset
	 * @param pageSize
	 * @param fullResultsSize
	 * @param data
	 */
	public VesselRecordSearchResponse(Integer offset, Integer pageSize, Integer fullResultsSize, Collection<RECORD> dataSet) {
		super(offset, pageSize, fullResultsSize, dataSet == null ? null : new SerializableArrayList<RECORD>(dataSet));
	}
}