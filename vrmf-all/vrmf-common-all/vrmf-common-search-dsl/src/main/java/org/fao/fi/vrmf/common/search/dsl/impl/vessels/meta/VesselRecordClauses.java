/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta;

import java.util.Date;

import org.fao.fi.vrmf.common.models.search.DoubleRange;
import org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonClauses;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.support.AuthorizationStatus;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Feb 2013
 */
public class VesselRecordClauses extends CommonClauses {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7346142116148802353L;
	
	final static public VesselRecordClauses IMO = single("imo", String.class);
	final static public VesselRecordClauses EU_CFR = single("euCfr", String.class);
	
	final static public VesselRecordClauses IDENTIFIER_TYPE = single("identifierType", String.class);
	final static public VesselRecordClauses IDENTIFIERS = multiple("identifiers", String.class);
	final static public VesselRecordClauses IDENTIFIER_LIKE = single("identifierLike", Boolean.class);
	
	final static public VesselRecordClauses BUILDING_YEAR_MIN = single("buildingYearMin", Integer.class);
	final static public VesselRecordClauses BUILDING_YEAR_MAX = single("buildingYearMax", Integer.class);

	final static public VesselRecordClauses HULL_MATERIAL = multiple("hullMaterial", String.class);

	final static public VesselRecordClauses NO_PARENT_TYPE = single("noParentType", Boolean.class);
	final static public VesselRecordClauses PARENT_TYPE = multiple("parentType", Integer.class);
	
	final static public VesselRecordClauses NO_TYPE = single("noType", Boolean.class);
	final static public VesselRecordClauses TYPE = multiple("type", Integer.class);
	final static public VesselRecordClauses TYPE_CATEGORY = single("typeCategory", String.class);
	final static public VesselRecordClauses TYPE_CATEGORY_LIKE = single("typeCategoryLike", Boolean.class);
	
	final static public VesselRecordClauses PRIMARY_GEAR_TYPE = multiple("primaryGearType", Integer.class);
	final static public VesselRecordClauses PRIMARY_GEAR_TYPE_CATEGORY = single("primaryGearCategory", String.class);
	final static public VesselRecordClauses PRIMARY_GEAR_TYPE_CATEGORY_LIKE = single("primaryGearCategoryLike", Boolean.class);
	
	final static public VesselRecordClauses SECONDARY_GEAR_TYPE = multiple("secondaryGearType", Integer.class);
	final static public VesselRecordClauses SECONDARY_GEAR_TYPE_CATEGORY = single("secondaryGearCategory", String.class);
	final static public VesselRecordClauses SECONDARY_GEAR_TYPE_CATEGORY_LIKE = single("secondaryGearCategoryLike", Boolean.class);
	
	final static public VesselRecordClauses STATUS = multiple("status", String.class);
	
	final static public VesselRecordClauses NO_FLAG = single("noFlag", Boolean.class);
	final static public VesselRecordClauses FLAG = multiple("flag", Integer.class);
	
	final static public VesselRecordClauses NAMES = multiple("names", String.class);
	final static public VesselRecordClauses NAME_VALID_FROM = single("nameValidFrom", Date.class);
	final static public VesselRecordClauses NAME_VALID_TO = single("nameValidTo", Date.class);
	final static public VesselRecordClauses NAME_LIKE = single("nameLike", Boolean.class);
	
	final static public VesselRecordClauses EXTERNAL_MARKINGS = multiple("externalMarkings", String.class);
	final static public VesselRecordClauses EXTERNAL_MARKING_LIKE = single("externalMarkingsLike", Boolean.class);
	
	final static public VesselRecordClauses CALLSIGNS = multiple("IRCSs", String.class);
	final static public VesselRecordClauses CALLSIGNS_LIKE = single("IRCSLike", Boolean.class);
	
	final static public VesselRecordClauses MMSIS = multiple("MMSIs", String.class);
	final static public VesselRecordClauses MMSI_LIKE = single("MMSILike", Boolean.class);

	final static public VesselRecordClauses HAS_VMS_INDICATOR = single("hasVmsIndicator", Boolean.class);

	final static public VesselRecordClauses REGISTRATION_NUMBERS = multiple("registrationNumbers", String.class);
	final static public VesselRecordClauses REGISTRATION_NUMBER_LIKE = single("registrationNumberLike", Boolean.class);
	final static public VesselRecordClauses REGISTRATION_COUNTRIES = multiple("registrationCountries", Integer.class);
	final static public VesselRecordClauses REGISTRATION_COUNTRY_VALID_FROM = single("registrationCountryValidFrom", Date.class);
	final static public VesselRecordClauses REGISTRATION_COUNTRY_VALID_TO = single("registrationCountryValidTo", Date.class);
	final static public VesselRecordClauses REGISTRATION_PORTS = multiple("registrationPorts", Integer.class);
	
	final static public VesselRecordClauses FISHING_LICENSE_NUMBERS = multiple("fishingLicenseNumbers", String.class);
	final static public VesselRecordClauses FISHING_LICENSE_NUMBER_LIKE = single("fishingLicenseNumberLike", Boolean.class);
	final static public VesselRecordClauses FISHING_LICENSE_ISSUING_COUNTRIES = multiple("fishingLicenseIssuingCountries", Integer.class);
	
	final static public VesselRecordClauses NO_LENGTH = single("noLength", Boolean.class);
	final static public VesselRecordClauses LENGTH_TYPES = multiple("lengthTypes", String.class);
	final static public VesselRecordClauses LENGTH_RANGES = multiple("lengthRanges", DoubleRange.class);
	
	final static public VesselRecordClauses NO_TONNAGE = single("noTonnage", Boolean.class);
	final static public VesselRecordClauses TONNAGE_TYPES = multiple("tonnageTypes", String.class);
	final static public VesselRecordClauses TONNAGE_RANGES = multiple("tonnageRanges", DoubleRange.class);
	
	final static public VesselRecordClauses MAIN_ENGINE_POWER_TYPES = multiple("mainEnginePowerTypes", String.class);
	final static public VesselRecordClauses MAIN_ENGINE_POWER_RANGES = multiple("mainEnginePowerRanges", DoubleRange.class);
	
	final static public VesselRecordClauses AUX_ENGINE_POWER_TYPES = multiple("auxEnginePowerTypes", String.class);
	final static public VesselRecordClauses AUX_ENGINE_POWER_RANGES = multiple("auxEnginePowerRanges", DoubleRange.class);
	
	final static public VesselRecordClauses NON_COMPLIANCE_DATE_RANGE_FROM = single("nonComplianceRangeFrom", Date.class);
	final static public VesselRecordClauses NON_COMPLIANCE_DATE_RANGE_TO = single("nonComplianceRangeTo", Date.class);
	final static public VesselRecordClauses NON_COMPLIANCE_SOURCE_TYPES = multiple("nonComplianceSourceTypes", String.class);
	final static public VesselRecordClauses NON_COMPLIANCE_OUTCOME_TYPES = multiple("nonComplianceOutcomeTypes", String.class);
	final static public VesselRecordClauses NON_COMPLIANCE_INFRINGEMENT_TYPES = multiple("nonComplianceInfringementTypes", String.class);
	final static public VesselRecordClauses NON_COMPLIANCE_ISSUERS = multiple("nonComplianceIssuers", String.class);
	
	final static public VesselRecordClauses HAS_INSPECTIONS = single("hasInspections", Boolean.class);
	final static public VesselRecordClauses INSPECTIONS_DATE_RANGE_FROM = single("inspectionsDateRangeFrom", Date.class);
	final static public VesselRecordClauses INSPECTIONS_DATE_RANGE_TO = single("inspectionsDateRangeTo", Date.class);
	final static public VesselRecordClauses INSPECTIONS_ORIGINATING_COUNTRIES = multiple("inspectionsOriginatingCountries", Integer.class);
	final static public VesselRecordClauses INSPECTIONS_INFRINGEMENT_TYPES = multiple("inspectionsInfringementTypes", Integer.class);
	
	final static public VesselRecordClauses HAS_IUU_LISTS = single("hasIUULists", Boolean.class);
	final static public VesselRecordClauses IUU_LISTS_DATE_RANGE_FROM = single("IUUListsDateRangeFrom", Date.class);
	final static public VesselRecordClauses IUU_LISTS_DATE_RANGE_TO = single("IUUListsDateRangeTo", Date.class);
	final static public VesselRecordClauses IUU_LISTS_ORIGINATING_LISTS = multiple("IUUListsOriginatingLists", Integer.class);
	
	final static public VesselRecordClauses HAS_PORT_ENTRY_DENIALS = single("hasPortEntryDenials", Boolean.class);
	final static public VesselRecordClauses PORT_ENTRY_DENIALS_DATE_RANGE_FROM = single("portEntryDenialsDateRangeFrom", Date.class);
	final static public VesselRecordClauses PORT_ENTRY_DENIALS_DATE_RANGE_TO = single("portEntryDenialsDateRangeTo", Date.class);
	final static public VesselRecordClauses PORT_ENTRY_DENIALS_ORIGINATING_COUNTRIES = multiple("portEntryDenialsOriginatingCountries", Integer.class);
	final static public VesselRecordClauses PORT_ENTRY_DENIALS_ORIGINATING_PORTS = multiple("portEntryDenialsOriginatingPorts", Integer.class);	
	final static public VesselRecordClauses PORT_ENTRY_DENIALS_REASONS = multiple("portEntryDenialsReasons", Integer.class);	
	
	final static public VesselRecordClauses AUTHORIZATION_HOLDER = single("authorizationHolder", Date.class);
	final static public VesselRecordClauses AUTHORIZATION_AT_DATE = single("authorizationAtDate", Date.class);
	final static public VesselRecordClauses AUTHORIZATION_TYPES = multiple("authorizationTypes", String.class);
	final static public VesselRecordClauses AUTHORIZATION_STATUS = single("authorizationStatus", AuthorizationStatus.class);
	final static public VesselRecordClauses AUTHORIZATION_START_FROM = single("authorizationStartFrom", Date.class);
	final static public VesselRecordClauses AUTHORIZATION_START_TO = single("authorizationStartTo", Date.class);
	final static public VesselRecordClauses AUTHORIZATION_END_FROM = single("authorizationEndFrom", Date.class);
	final static public VesselRecordClauses AUTHORIZATION_END_TO = single("authorizationEndTo", Date.class);
	final static public VesselRecordClauses AUTHORIZATION_TERMINATION_FROM = single("authorizationTerminationFrom", Date.class);
	final static public VesselRecordClauses AUTHORIZATION_TERMINATION_TO = single("authorizationTerminationTo", Date.class);
	final static public VesselRecordClauses AUTHORIZATION_ISSUERS = multiple("authorizationIssuers", String.class);
	final static public VesselRecordClauses AUTHORIZATION_ISSUING_COUNTRIES = multiple("authorizationIssuingCountries", Integer.class);
	final static public VesselRecordClauses AUTHORIZED_AT_DATE = single("authorizedAtDate", Date.class);
	
	final static public VesselRecordClauses OWNERS = multiple("owners", Integer.class);
	final static public VesselRecordClauses OPERATORS = multiple("operators", Integer.class);
	
	public VesselRecordClauses() {
		super();
	}
	
	protected VesselRecordClauses(String mnemonic, Class<?> expectedType, boolean isArray) { 
		super(mnemonic, expectedType, isArray);
	}
	
	static private VesselRecordClauses multiple(String mnemonic, Class<?> expectedType) {
		return new VesselRecordClauses(mnemonic, expectedType, true);
	}
	
	static private VesselRecordClauses single(String mnemonic, Class<?> expectedType) {
		return new VesselRecordClauses(mnemonic, expectedType, false);
	}
}
