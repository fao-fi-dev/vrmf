/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.math.DoubleRange;
import org.fao.fi.vrmf.common.search.dsl.impl.AbstractSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.support.AuthorizationStatus;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
@XmlRootElement
public class VesselRecordSearchFilter extends AbstractSearchFilter<VesselRecordSelector, VesselRecordWhere, VesselRecordSorter, VesselRecordSearchFilter> {
	 /** Field serialVersionUID */
	private static final long serialVersionUID = 7813829858455732137L;

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#thi$()
	 */
	@Override
	protected VesselRecordSearchFilter thi$() {
		return this;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#selector()
	 */
	@Override
	protected VesselRecordSelector selector() {
		return new VesselRecordSelector();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#sorter()
	 */
	@Override
	protected VesselRecordSorter sorter() {
		return new VesselRecordSorter();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#where()
	 */
	@Override
	protected VesselRecordWhere where() {
		return new VesselRecordWhere();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#context()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List<Class> context() {
		return new ArrayList<Class>(Arrays.asList(new Class[] { 
			VesselRecordSelector.class, 
			VesselRecordSorter.class, 
			VesselRecordWhere.class, 
			DoubleRange.class,
			AuthorizationStatus.class 
		}));
	}
	
	public boolean includeAuthorizations() {
		return _selector != null && _selector.getIncludeAuthorizations();
	}
	
	public boolean includePortDenials() {
		return _selector != null && _selector.getIncludePortDenials();
	}
	
	public boolean includeInspections() {
		return _selector != null && _selector.getIncludeInspections();
	}
	
	public boolean includeIUUListings() {
		return _selector != null && _selector.getIncludeIUUListings();
	}
}
