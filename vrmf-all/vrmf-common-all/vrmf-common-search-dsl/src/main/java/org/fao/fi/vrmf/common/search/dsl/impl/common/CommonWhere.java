/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.common;

import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.search.dsl.impl.Where;
import org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonClauses;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
@XmlRootElement
public class CommonWhere extends Where {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3455707049619249378L;

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.WhereCondition#getIDs()
	 */
	public Integer[] getIDs() {
		return (Integer[])this.doGetConditionValue(CommonClauses.VRMF_IDS);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.Where#getSourceSystems()
	 */
	
	public String[] getSources() {
		return (String[])this.doGetConditionValue(CommonClauses.SOURCES);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.common.services.request.search.dsl.interfaces.WhereCondition#getUIDs()
	 */
	
	public Integer[] getUIDs() {
		return (Integer[])this.doGetConditionValue(CommonClauses.VRMF_UIDS);
	}
}
