/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.search.dsl.impl.AbstractSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.support.AuthorizationStatus;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
@XmlRootElement
public class AuthorizedVesselRecordSearchFilter extends AbstractSearchFilter<AuthorizedVesselRecordSelector, AuthorizedVesselRecordWhere, AuthorizedVesselRecordSorter, AuthorizedVesselRecordSearchFilter> {
	 /** Field serialVersionUID */
	private static final long serialVersionUID = 7813829858455732137L;

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#thi$()
	 */
	@Override
	protected AuthorizedVesselRecordSearchFilter thi$() {
		return this;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#selector()
	 */
	@Override
	protected AuthorizedVesselRecordSelector selector() {
		return new AuthorizedVesselRecordSelector();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#sorter()
	 */
	@Override
	protected AuthorizedVesselRecordSorter sorter() {
		return new AuthorizedVesselRecordSorter();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#where()
	 */
	@Override
	protected AuthorizedVesselRecordWhere where() {
		return new AuthorizedVesselRecordWhere();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#context()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List<Class> context() {
		return new ArrayList<Class>(Arrays.asList(new Class[] { AuthorizedVesselRecordSelector.class, AuthorizedVesselRecordSorter.class, AuthorizedVesselRecordWhere.class, AuthorizationStatus.class })); // TODO Auto-generated method stub
	}
}