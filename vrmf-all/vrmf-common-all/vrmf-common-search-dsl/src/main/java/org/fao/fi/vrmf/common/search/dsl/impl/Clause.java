/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
@XmlRootElement(name="clause")
@XmlAccessorType(XmlAccessType.FIELD)
public class Clause implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6800097082440612336L;
	
	@XmlAttribute protected String _mnemonic;
	@XmlAttribute protected Class<?> _expectedType;
	@XmlAttribute protected boolean _multiple;
	
	/**
	 * Class constructor
	 *
	 */
	public Clause() {
	}
	
	/**
	 * Class constructor
	 *
	 * @param mnemonic
	 * @param expectedType
	 */
	public Clause(String mnemonic, Class<?> expectedType, boolean multiple) {
		super();
		this._mnemonic = mnemonic;
		this._expectedType = expectedType;
		this._multiple = multiple;
	}

	/**
	 * @return the 'mnemonic' value
	 */
	public String getMnemonic() {
		return this._mnemonic;
	}
	
	/**
	 * @param mnemonic the 'mnemonic' value to set
	 */
	public void setMnemonic(String mnemonic) {
		this._mnemonic = mnemonic;
	}
	
	/**
	 * @return the 'expectedType' value
	 */
	public Class<?> getExpectedType() {
		return this._expectedType;
	}
	
	/**
	 * @param expectedType the 'expectedType' value to set
	 */
	public void setExpectedType(Class<?> expectedType) {
		this._expectedType = expectedType;
	}

	/**
	 * @return the 'multiple' value
	 */
	public boolean isMultiple() {
		return this._multiple;
	}

	/**
	 * @param multiple the 'multiple' value to set
	 */
	public void setMultiple(boolean multiple) {
		this._multiple = multiple;
	}
	
	public String asString() {
		return new StringBuilder().
						append(_mnemonic).append("|").
						append(_multiple).append("|").
						append(_expectedType.getName()).
				   toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this._expectedType == null ) ? 0 : this._expectedType.hashCode() );
		result = prime * result + ( ( this._mnemonic == null ) ? 0 : this._mnemonic.hashCode() );
		result = prime * result + ( this._multiple ? 1231 : 1237 );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		Clause other = (Clause) obj;
		if(this._expectedType == null) {
			if(other._expectedType != null) return false;
		} else if(!this._expectedType.equals(other._expectedType)) return false;
		if(this._mnemonic == null) {
			if(other._mnemonic != null) return false;
		} else if(!this._mnemonic.equals(other._mnemonic)) return false;
		if(this._multiple != other._multiple) return false;
		return true;
	}
}