/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.meta;

import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Feb 2013
 */
public class AuthorizedVesselRecordClauses extends VesselRecordClauses {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7336581103495806489L;
	
	/**
	 * Class constructor
	 */
	public AuthorizedVesselRecordClauses() {
		super();
	}
	
	protected AuthorizedVesselRecordClauses(String mnemonic, Class<?> expectedType, boolean isArray) { 
		super(mnemonic, expectedType, isArray);
	}
	
	@SuppressWarnings("unused")
	static private AuthorizedVesselRecordClauses multiple(String mnemonic, Class<?> expectedType) {
		return new AuthorizedVesselRecordClauses(mnemonic, expectedType, true);
	}
	
	@SuppressWarnings("unused")
	static private AuthorizedVesselRecordClauses single(String mnemonic, Class<?> expectedType) {
		return new AuthorizedVesselRecordClauses(mnemonic, expectedType, false);
	}
}
