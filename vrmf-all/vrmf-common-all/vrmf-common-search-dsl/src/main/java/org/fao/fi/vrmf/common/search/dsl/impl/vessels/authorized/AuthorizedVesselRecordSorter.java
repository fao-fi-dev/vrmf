/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized;

import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSorter;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.meta.AuthorizedVesselRecordDataColumns;

@XmlRootElement
public class AuthorizedVesselRecordSorter extends VesselRecordSorter implements AuthorizedVesselRecordColumnFilter {
	private static final long serialVersionUID = 8088117384533564889L;

	@Override
	public boolean getSelectAuthorizationStart() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_START);
	}

	@Override
	public boolean getSelectAuthorizationEnd() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_END);
	}

	@Override
	public boolean getSelectAuthorizationType() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_TYPE);
	}

	@Override
	public boolean getSelectAuthorizationIssuingCountry() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_TYPE);
	}

	@Override
	public boolean getSelectAuthorizationIssuer() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_ISSUER);
	}

	@Override
	public boolean getSelectAuthorizationTerminationDate() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_TERMINATION_DATE);
	}

	@Override
	public boolean getSelectAuthorizationTerminationCode() {
		return this.mustSelect(AuthorizedVesselRecordDataColumns.AUTHORIZATION_TERMINATION_CODE);
	}
}