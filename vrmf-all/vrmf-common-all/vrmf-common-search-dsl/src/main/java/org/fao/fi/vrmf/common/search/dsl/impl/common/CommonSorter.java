/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.common;

import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.search.dsl.impl.Sorter;
import org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
@XmlRootElement
public class CommonSorter extends Sorter implements CommonColumnFilter {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8088117384533564889L;

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.ICommonSorter#getSelectUID()
	 */
	@Override
	public boolean getSelectUID() {
		return this.mustSelect(CommonDataColumns.UID);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.ICommonSorter#getSelectID()
	 */
	@Override
	public boolean getSelectID() {
		return this.mustSelect(CommonDataColumns.ID);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.ICommonSorter#getSelectSource()
	 */
	@Override
	public boolean getSelectSource() {
		return this.mustSelect(CommonDataColumns.SOURCE);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.ICommonSorter#getSelectNumberOfSources()
	 */
	@Override
	public boolean getSelectNumberOfSources() {
		return this.mustSelect(CommonDataColumns.NUM_SOURCES);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.ICommonSorter#getSelectUpdateDate()
	 */
	@Override
	public boolean getSelectUpdateDate() {
		return this.mustSelect(CommonDataColumns.UPD_DATE);
	}
}