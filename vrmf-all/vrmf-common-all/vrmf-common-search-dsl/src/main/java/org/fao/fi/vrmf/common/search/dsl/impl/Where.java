/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl;

import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.fao.fi.vrmf.common.core.model.GenericData;
import org.fao.fi.vrmf.common.search.dsl.exceptions.DSLRuntimeException;
import org.fao.fi.vrmf.common.search.dsl.exceptions.MultipleConditionsForSameClauseRuntimeException;
import org.fao.fi.vrmf.common.search.dsl.exceptions.WrongConditionCardinalityRuntimeException;
import org.fao.fi.vrmf.common.search.dsl.exceptions.WrongConditionValueRuntimeException;
import org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonClauses;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;
// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Feb 2013
 */
@XmlRootElement(name="where")
@XmlAccessorType(XmlAccessType.FIELD)
public class Where extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5639528293251700600L;
	
	/**
	 * The _conditions.
	 */
	@XmlTransient
	protected Map<Clause, Condition	> _conditions;
	
	/**
	 * The _conditions list.
	 */
	@XmlElementWrapper(name="conditions")
	@XmlElement(name="and")
	protected Collection<Condition> _conditionsList;
	
	/**
	 * Instantiates a new where.
	 */
	public Where() {
		this._conditions = new HashMap<Clause, Condition>();
		this._conditionsList = new ArrayList<Condition>();
	}
	
	/**
	 * Nop.
	 *
	 * @param <W> the generic type
	 * @return the w
	 */
	@SuppressWarnings("unchecked")
	public <W extends Where> W nop() {
		return (W)this;
	}

	/**
	 * Gets the conditions.
	 *
	 * @return the conditions
	 */
	public Map<Clause, Condition> getConditions() {
		return this._conditions;
	}
	
	/**
	 * Gets the conditions list.
	 *
	 * @return the 'conditionsList' value
	 */
	public Collection<Condition> getConditionsList() {
		return this._conditionsList;
	}

	/**
	 * Gets the mnemonic conditions.
	 *
	 * @return the mnemonic conditions
	 */
	public Map<String, Object> getMnemonicConditions() {
		Map<String, Object> mnemonics = new HashMap<String, Object>();
		
		Clause clause;
		boolean isValueSet;
		for(Condition condition : this._conditions.values()) {
			clause = condition.getClause();
			isValueSet = this.isConditionValueSet(condition);
			
			if(isValueSet)
				mnemonics.put(clause.getMnemonic(), condition.getValue());
		}
		
		return mnemonics;
	}
	
	/**
	 * Checks if is condition value set.
	 *
	 * @param condition the condition
	 * @return true, if is condition value set
	 */
	protected boolean isConditionValueSet(Condition condition) {
		$nN($nN(condition, "Cannot check condition value for a <NULL> condition").getClause(), "Cannot check condition value for a <NULL> clause within a condition");
		
		Object value = condition.getValues();
		
		if(value == null)
			return false;
		
		Clause clause = condition.getClause();
		
		return !clause.isMultiple() || !this.isMultipleValueEmpty(value);
	}
	
	/**
	 * Do get condition value.
	 *
	 * @param clause the clause
	 * @return the object
	 */
	protected Object doGetConditionValue(Clause clause) {
		if(this._conditions.containsKey(clause))
			return this._conditions.get(clause);
		
		return null;
	}
	
	/**
	 * Checks if is clause set.
	 *
	 * @param clause the clause
	 * @return the object
	 */
	protected Object isClauseSet(Clause clause) {
		return this.doGetConditionValue(clause) != null;
	}
	
	/**
	 * Gets the filter by.
	 *
	 * @param clauses the clauses
	 * @return the filter by
	 */
	protected boolean getFilterBy(Clause... clauses) {
		Set<String> conditions = this.getMnemonicConditions().keySet();
		
		for(Clause clause : clauses) {
			if(conditions.contains(clause.getMnemonic()))
				if(this.hasConditionForClause(this._conditions, clause))
					return true;
		}
		
		return false;
	}
	
	/**
	 * Checks for condition for clause.
	 *
	 * @param conditions the conditions
	 * @param clause the clause
	 * @return true, if successful
	 */
	protected boolean hasConditionForClause(Map<Clause, Condition> conditions, Clause clause) {
		Condition condition;
		
		if(clause == null || 
		   conditions == null || 
		   conditions.isEmpty() || 
		 ( condition = conditions.get(clause)) == null )
			return false;
		
		List<?> values = condition.getValues();
		
		return values != null && !values.isEmpty() && ( values.size() > 1 || values.get(0) != null );
	}
	
	protected boolean isValueSet(Object value) {
		if(value == null) return false;
		
		final Class<?> valueClass = value.getClass();
		
		if(valueClass.isArray()) {
			Object[] asArray = (Object[])value;
			
			return asArray.length > 1 || ( asArray.length == 1 && asArray[0] != null );
		} else if(value instanceof Collection) {
			Collection<?> asCollection = (Collection<?>)value;
			
			return asCollection.size() > 1 || ( asCollection.size() == 1 && asCollection.toArray(new Object[0])[0] != null );
		}
		
		return true;
	}
	
	/**
	 * Checks if is value properly set.
	 *
	 * @param value the value
	 * @param expectedType the expected type
	 * @return true, if is value properly set
	 */
	protected boolean isValueProperlySet(Object value, Class<?> expectedType) {
		$nEm((List<?>)value, "The provided value cannot be null");
		$nN(expectedType, "Expected type should not be null in order to check for its proper initialization");
		
		return expectedType.isAssignableFrom(((List<?>)value).get(0).getClass());
	}
	
	/**
	 * Checks if is array value properly set.
	 *
	 * @param value the value
	 * @param expectedType the expected type
	 * @return true, if is array value properly set
	 */
	protected boolean isMultipleValueProperlySet(Object value, Class<?> expectedType) {
		$nN(value, "Value should not be null in order to check for its proper initialization");
		$nN(expectedType, "Expected type should not be null in order to check for its proper initialization");
		
		Class<?> valueClass = value.getClass();
		
		boolean isArray = valueClass.isArray();
		boolean isCollection = Collection.class.isAssignableFrom(valueClass);
		boolean isGenericCollection = isCollection && valueClass.getGenericSuperclass() instanceof ParameterizedType;
		
		if(isArray || isCollection) {
			Class<?> elementClass = isArray ? 
										valueClass.getComponentType() : 
											!isGenericCollection ?
												valueClass :
													((Collection<?>)value).toArray(new Object[0])[0].getClass(); //This really sucks
			
			if(!expectedType.isAssignableFrom(elementClass))
				return false;
			
			return !isMultipleValueEmpty(value);
		}
		
		return false;
	}
	
	private boolean isMultipleValueEmpty(Object value) {
		if(value == null)
			return true;
		
		Class<?> valueClass = value.getClass();
		
		boolean isArray = valueClass.isArray();
		boolean isCollection = Collection.class.isAssignableFrom(valueClass);

		if(!isArray && !isCollection)
			throw new DSLRuntimeException(value + " is neither an array nor a collection");
		
		return isArray ? Array.getLength(value) == 0 : ((Collection<?>)value).isEmpty();
	}
	
	/**
	 * Do add condition.
	 *
	 * @param condition the condition
	 */
	@SuppressWarnings("unchecked")
	protected void doAddCondition(Condition condition) {
		if(condition == null)
			throw new DSLRuntimeException("Cannot add a <NULL> condition");

		Condition overwritten;
		Clause clause;

		if(( clause = condition.getClause() ) == null)
			throw new DSLRuntimeException("Cannot add a condition with a <NULL> clause");

		Object value;
		boolean valueProperlySet;
		
		if(isValueSet(value = condition.getValues())) {
			//Check that value is correct according to type
			valueProperlySet = clause.isMultiple() ?
							   this.isMultipleValueProperlySet(value, clause.getExpectedType()) :
							   this.isValueProperlySet(value, clause.getExpectedType());

			if(!valueProperlySet)
				throw new WrongConditionValueRuntimeException(condition, clause);
			
			if(!clause.isMultiple() && value != null && ((List<Object>)value).size() > 1)
				throw new WrongConditionCardinalityRuntimeException(condition, clause);
			
			this._conditionsList.add(condition);
			overwritten = this._conditions.put(clause, condition);
			
			if(overwritten != null)
				throw new MultipleConditionsForSameClauseRuntimeException(clause, overwritten, condition);

		} else {
			//Conditions with null-equivalent values are silently dropped...
		}
	}
	
	/**
	 * And.
	 *
	 * @param <W> the generic type
	 * @param condition the condition
	 * @return the w
	 */
	@SuppressWarnings("unchecked")
	public <W extends Where> W and(Condition condition) {
		this.doAddCondition(condition);
		
		return (W)this;
	}
	
	/**
	 * And.
	 *
	 * @param <W> the generic type
	 * @param clause the clause
	 * @param values the values
	 * @return the w
	 */
	@SuppressWarnings("unchecked")
	public <W extends Where> W and(Clause clause, Object... values) {
		this.doAddCondition(this.doBuildCondition(clause, values));
		
		return (W)this;
	}

	/**
	 * Do build condition.
	 *
	 * @param clause the clause
	 * @param values the values
	 * @return the condition
	 */
	protected Condition doBuildCondition(Clause clause, Object... values) {
		return new Condition(clause, values == null || values.length == 0 ? null : Arrays.asList(values));
	}
	
	/**
	 * Gets the available sources.
	 *
	 * @return the available sources
	 */
	public String[] getAvailableSources() {
		return (String[])this.doGetConditionValue(CommonClauses.AVAILABLE_SOURCES);
	}
	
	public String asString() {
		StringBuilder asString = new StringBuilder();
		
		if(_conditionsList != null) {
			for(Condition in : _conditionsList) {
				asString.append(in.asString()).append("|");
			}
		}
		
		return asString.toString();
	}
}