/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.common.meta;

import org.fao.fi.vrmf.common.search.dsl.impl.Column;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Feb 2013
 */
public class CommonDataColumns extends Column {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4962863293368310152L;
	
	final static public CommonDataColumns UID = new CommonDataColumns("uid", "META_UID");
	final static public CommonDataColumns ID = new CommonDataColumns("id", 	"META_ID");
	final static public CommonDataColumns UPD_DATE = new CommonDataColumns("ud", 	"UPDATE_DATE");
	final static public CommonDataColumns SOURCE = new CommonDataColumns("s", 	"SOURCE_SYSTEM");
	final static public CommonDataColumns NUM_SOURCES = new CommonDataColumns("ns", "NUMBER_OF_SYSTEMS");
	final static public CommonDataColumns NOP = new CommonDataColumns(null, null);
	
	/**
	 * Class constructor
	 */
	public CommonDataColumns() {
		super(null, null);
	}

	/**
	 * Class constructor
	 *
	 * @param mnemonic
	 * @param column
	 */
	public CommonDataColumns(String mnemonic, String column) {
		super(mnemonic, column); // TODO Auto-generated constructor block
	}
};