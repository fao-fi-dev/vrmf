/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.meta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Feb 2013
 */
@XmlRootElement(name="groupableColumn")
@XmlAccessorType(XmlAccessType.FIELD)
public class GroupableDataColumns {
	final static public GroupableDataColumns ID = new GroupableDataColumns("ID");
	final static public GroupableDataColumns UID = new GroupableDataColumns("UID");
	
	@XmlAttribute private final String _column;
	
	/**
	 * Class constructor
	 *
	 */
	public GroupableDataColumns() {
		this(null);
	}
	
	private GroupableDataColumns(String column) {
		this._column = column;
	}

	/**
	 * @return the 'column' value
	 */
	public String getColumn() {
		return this._column;
	}
	
	public String name() {
		return this.getColumn();
	}
}
