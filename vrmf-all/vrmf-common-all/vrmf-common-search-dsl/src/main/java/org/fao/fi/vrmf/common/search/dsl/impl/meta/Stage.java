/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.meta;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Feb 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Feb 2013
 */
@XmlEnum
@XmlRootElement
public enum Stage {
	COUNT("count"),
	SELECT("select"),
	RETRIEVE_METADATA("retrieveMetadata"),
	RETRIEVE_ADDITIONAL_METADATA("retrieveAdditionalMetadata");
	
	private String _stage;
	
	Stage(String stage) {
		this._stage = stage;
	}
	
	public String getStage() {
		return this._stage;
	}
}