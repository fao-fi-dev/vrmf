/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.vessels;

import org.fao.fi.vrmf.common.search.dsl.impl.common.CommonColumnFilter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
public interface VesselRecordColumnFilter extends CommonColumnFilter {
	boolean getSelectIMO();
	boolean getSelectEUCFR();
	
	boolean getSelectName();
	
	boolean getSelectFlag();
	boolean getSelectFlagID();
	
	boolean getSelectCallsign();
	boolean getSelectCallsignCountry();
	boolean getSelectCallsignCountryID();
	
	boolean getSelectRegistrationNumber();
	boolean getSelectRegistrationCountry();
	boolean getSelectRegistrationCountryID();
	boolean getSelectRegistrationPort();
	boolean getSelectRegistrationPortID();
	
	boolean getSelectLOA();
	boolean getSelectLengthType();
	boolean getSelectLengthValue();
	
	boolean getSelectGT();
	boolean getSelectTonnageType();
	boolean getSelectTonnageValue();
	
	boolean getSelectType();
	boolean getSelectTypeID();
	
	boolean getSelectParentType();
	boolean getSelectParentTypeID();
	
	boolean getSelectGearType();
	boolean getSelectGearTypeID();
	
	boolean getSelectVesselFlag();
	boolean getSelectVesselCallsign();
	boolean getSelectVesselRegistration();
	boolean getSelectVesselLength();
	boolean getSelectVesselTonnage();
	boolean getSelectVesselType();
	boolean getSelectVesselParentType();
	boolean getSelectVesselGearType();
}
