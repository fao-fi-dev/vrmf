/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-common-search-dsl)
 */
package org.fao.fi.vrmf.common.search.dsl.impl.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fao.fi.vrmf.common.search.dsl.impl.AbstractSearchFilter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 27, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 27, 2015
 */
public class CommonSearchFilter extends AbstractSearchFilter<CommonSelector, CommonWhere, CommonSorter, CommonSearchFilter> {
	 /** Field serialVersionUID */
	private static final long serialVersionUID = 7813829858455732137L;

	/**
	 * Class constructor
	 *
	 */
	public CommonSearchFilter() {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#thi$()
	 */
	@Override
	protected CommonSearchFilter thi$() {
		return this;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#selector()
	 */
	@Override
	protected CommonSelector selector() {
		return new CommonSelector();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#sorter()
	 */
	@Override
	protected CommonSorter sorter() {
		return new CommonSorter();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#where()
	 */
	@Override
	protected CommonWhere where() {
		return new CommonWhere();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.common.core.search.dsl.impl.AbstractSearchFilter#context()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List<Class> context() {
		return new ArrayList<Class>(Arrays.asList(new Class[] { CommonSelector.class, CommonSorter.class, CommonWhere.class })); // TODO Auto-generated method stub
	}
}
